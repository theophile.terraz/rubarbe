##!/bin/bash
#Options de compilation
F90 = gfortran -cpp
DEBUG_FLAG = -Og -g -Wall -Wextra -fbounds-check -fimplicit-none -fcheck=all -fbacktrace
OPTIM_FLAG = -O3
USUAL_FLAG =
OMP_FLAG = -fopenmp
MPI_FLAG = mpifort -cpp -DWITH_MPI
# change to debug_flag to enable debug features
COMPILE_FLAG = $(OPTIM_FLAG)

#Fichiers compilés
MPI = _MPI
OMP = _OpenMP
EC = 3
TS = be
PROG = rubar
VERSION =
MODULES =
COUPLAGE =
SRC = rubarbe.f90
SRC_OLD = rubarbe.f

#Ci-dessous des options de calcul prédéfinis pour effectuer la vérification automatique
#en eau claire et en transport solide
define AUTO_EC
-D TRANSPORT_SOLIDE=0
endef

define AUTO_TS
-D TRANSPORT_SOLIDE=1
endef

# YMPD: Intercell Values Limitation
# QMPD: More Limitations on Q
# CONFLUE: Conflu2 Instead of Conflue

define OPTIONS
-D QMPD=0 -D YMPD=0 -D CONFL2=0
endef


#Compilation

all : rubarbe rubar3

rubar3 :
	$(F90) $(COUPLAGE) $(OPTIONS) $(AUTO_EC) $(COMPILE_FLAG) $(MODULES) $(SRC) -o $(PROG)$(EC)

rubarbe :
	$(F90) $(COUPLAGE) $(OPTIONS) $(AUTO_TS) $(COMPILE_FLAG) $(MODULES) $(SRC) -o $(PROG)$(TS)

.PHONY: clean

clean :
	@rm *~ *.mod
	@echo "On a fait du nettoyage"
