# RubarBE: PROGRAMME DE CALCUL HYDRODYNAMIQUE AVEC FOND MOBILE

Le code numérique utilise un schéma de type Godounov et le solveur de Roe permettant de résoudre le problème de Riemann approché aux intermailles. Il détermine les variables conservatives des équations de Saint Venant 1D au temps TNP1 en fonction de celles calculées au temps TN aux centremailles et des flux numériques FLS de section et FLQ de débit à l'amont et l'aval immédiat des intermailles. Les variables calculées Y et Q sont supposées linéaires autour de la moyenne située au centremaille.

