      INTEGER NMAXcou,ncouplage,nacoumax
      parameter(nmaxcou=10000,nacoumax=50)
      INTEGER I2D(NMAXcou),I1D(NMAXcou) 
      character*1 typcou(nmaxcou)
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax),iM1D(NAMAX)
      logical GCOUPLAGE(NAMAX),COUPLAGE(NOUMAX),COUPLAGE1D(NAMAX)
      
      DOUBLE PRECISION SMBS4(LMAX),SMBQ4(LMAX)
     :,xangle1d(lmax),yangle1d(lmax)
     :,hcouplage(namax),vxcouplage(namax),vycouplage(namax)
      logical condlim6 
CConc
Cliq      DOUBLE PRECISION CCOUPLTS(NAMAX),DCOUPLTS(NAMAX),SCOUPLTS(NAMAX)      
Cliq      DOUBLE PRECISION SMBQS4(LMAX),SMBD4(LMAX),SMBET4(LMAX)
Cliq      DOUBLE PRECISION EXPVERTICALEC,EXPHORIZONTALEC
Cliq      LOGICAL VERTICALEC,VERTICALECVAR,HORIZONTALEC
CConcf
   
      COMMON/OUVCOUPLE/COUPLAGE
      COMMON/GCOUPLAGE/GCOUPLAGE
      COMMON/COUPLAGE1D/COUPLAGE1D
      common/hvcouplage/Hcouplage,vxcouplage,vycouplage
      COMMON/XYANGLE1D/XANGLE1D,YANGLE1D
      COMMON/smbS4/SMBS4 
      COMMON/smbq4/SMBq4 
      common/I1D2D/Ncouplage,I1D,I2D
      common/typcou/typcou
      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/NM1D/IM1D
      common/condlim6/condlim6    
CConc
Cliq      COMMON/smbQS4/SMBQS4,SMBD4,SMBET4 
Cliq      COMMON/CCOUPTS/CCOUPLTS,DCOUPLTS,SCOUPLTS      
Cliq      COMMON/EXPOSANTSC/EXPVERTICALEC,EXPHORIZONTALEC
Cliq      COMMON/VARC/VERTICALEC,VERTICALECVAR,HORIZONTALEC
CConcf
C*********************************************************      
      Subroutine initcouplage
C lancement de l'initialisation du couplage
c*********************************************************    
       call lecrefcouplage
C       call compdt
       call projec
       return
       end
C*********************************************************      
      Subroutine tran2den1ddemi0
C transformation variables 2d en variables 1D 
C mise a zero avant ou apres couplage pour les apports lateraux
C conditions limites traitees dans cl6sanscouplage
c*********************************************************    
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,NBB
c      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
c     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
c      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
c     :,nat(lmax)
      
C      DOUBLE PRECISION XTMAIL(LMAX)
      DOUBLE PRECISION SMBS4(LMAX),smbq4(lmax)
      INTEGER J,M
CConc
Cliq      DOUBLE PRECISION SMBQS4(LMAX),SMBD4(LMAX),SMBET4(LMAX)
CConcf

c       COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
c       COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/smbS4/SMBS4
      COMMON/SMBQ4/smbq4
C      COMMON/XGEOMT/XTMAIL
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
CConc
Cliq      COMMON/smbQS4/SMBQS4,SMBD4,SMBET4 
CConcf
      
      
      DO M=1,NBB
       DO J=LM(m-1)+2,LM(M)-1
         SMBS4(J)=0.
         smbq4(j)=0.
CConc
Cliq         smbqs4(j)=0.
Cliq         smbd4(j)=1.
Cliq         smbet4(j)=1.
CConcf
C fin boucle sur j
       enddo
c fin boucle sur bief
      enddo
      return
      end

C*********************************************************      
      Subroutine tran2den1ddemi
C transformation variables 2d en variables 1D 
C on prend les valeurs du 2D au demi pas de temps du 2D precedent
C logique uniquement si les pas de temps sont identiques
c*********************************************************    
       call calculflsq4
       call calculsmbq4
       call calculsmbs4
CConc
Cliq       call calculsmbqs4
CConcf
       return
       end
C*********************************************************      
      Subroutine lecrefcouplage
C lecture des correspondances      
c*********************************************************    
       integer N,I,NMAXCOU,ncouplage,j,m,nbb
     :,jrg,jrd,jce,jam,jav
      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NAMAX,NACOUMAX
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300
     :,NBMAX=150,NAMAX=185000)
      parameter(nmaxcou=10000,nacoumax=50)
      INTEGER LM(0:NBMAX),LL,IA
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      integer I1D(nmaxcou),I2D(nmaxcou)
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax),im1D(namax)
      integer ne,na,nn
      double precision xa(namax),ya(namax)
      character*200 ligne
      character*1 typcou(nmaxcou)
      double precision x1(nmaxcou),x2(nmaxcou),Y1(nmaxcou),y2(nmaxcou)
      logical finlec,TROUVE,CONDLIM6,lcontinue
      CHARACTER ETUDE*20
CConc
Cliq      DOUBLE PRECISION EXPVERTICALEC,EXPHORIZONTALEC
Cliq      LOGICAL VERTICALEC,VERTICALECVAR,HORIZONTALEC
Cliq      COMMON/EXPOSANTSC/EXPVERTICALEC,EXPHORIZONTALEC
Cliq      COMMON/VARC/VERTICALEC,VERTICALECVAR,HORIZONTALEC
CConcf
      
      common/I1D2D/Ncouplage,I1D,I2D
      common/typcou/typcou
      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/NM1D/IM1D      
      COMMON/condlim6/condlim6
    
      COMMON/NOMETU/ETUDE
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      
      COMMON/NOMB/NE,NA,NN
      COMMON/COOA/XA,YA

C lcontinue variable logique permettant le calcul
C si lcontinue faux on ecrit 1D2Dtrouve puis on s arrete
      lcontinue=.TRUE.
      do i=1,nmaxcou
            i1D(I)=0
            i2d(i)=0
      enddo    
CConc
Cliq      EXPHORIZONTALEC=1.
Cliq      HORIZONTALEC=.FALSE.
Cliq      EXPVERTICALEC=0.
Cliq      VERTICALEC=.FALSE.
Cliq      VERTICALECVAR=.FALSE.
CConcf
C initialisation du nombre de couplages
C a priori egal au nombre d'aretes 2D dans les couplages
      ncouplage=0
      N=20       
      WRITE(*,*)'##################################################'
      WRITE(*,*)'#            Couplage Rubar3/Rubar20             #'
      WRITE(*,*)'#        Simulation d''ecoulements 1D/2D         #'
      WRITE(*,*)'#                                                #'
      WRITE(*,*)'#          INRAE   8  avril 2021                 #'
      WRITE(*,*)'#                                                #'
      WRITE(*,*)'##################################################'
      OPEN(N,FILE='1D2D.'//ETUDE,STATUS='OLD',ERR=1)
      write(*,*)'lecture en cours fichier 1D2D.',ETUDE
      FINLEC=.false.
      DO I=1,nmaxcou
       If(.NOT.FINLEC)then
          READ (N,'(A200)',END=3)LIGNE
          Read (LIGNE(1:1),'(A1)')TYPCOU(I)
          IF(TYPCOU(I).EQ.' ')THEN
            finlec=.TRUE.
            NCOUplage=I-1
CConc
Cliq         ELSEIF(TYPCOU(I).EQ.'P'.OR.TYPCOU(I).EQ.'p')then
Cliq            TYPCOU(I)='P'
C lecture des param�tres pour le calcul des concentrations   
Cliq           READ (LIGNE(2:200),*,END=2)EXPVERTICALEC,EXPHORIZONTALEC
Cliq           IF(EXPVERTICALEC.GT.EPS2)then
Cliq              VERTICALEC=.TRUE. 
Cliq           elseIF(EXPVERTICALEC.LT.-EPS2)then
Cliq              VERTICALEC=.TRUE. 
CliqC calcul de l'exposant des concentrations
Cliq              VERTICALECVAR=.TRUE. 
CliqC coefficient non utilise
Cliq              EXPVERTICALEC=0.
Cliq           else
CliqC coefficient non utilise
Cliq              EXPVERTICALEC=0.
Cliq           endif
Cliq           IF(EXPHORIZONTALEC.GT.1.+EPS2)then
Cliq              HORIZONTALEC=.TRUE. 
Cliq           else
CliqC coefficient non utilise
Cliq              EXPHORIZONTALEC=1.
Cliq           endif
CConcf
         elseIF(TYPCOU(I).EQ.'G'.OR.TYPCOU(I).EQ.'g')then
           TYPCOU(I)='G'
         ELSEIF(TYPCOU(I).EQ.'D'.OR.TYPCOU(I).EQ.'d')then
           TYPCOU(I)='D'
         ELSEIF(TYPCOU(I).EQ.'C'.OR.TYPCOU(I).EQ.'c')then
           TYPCOU(I)='C'
         ELSEIF(TYPCOU(I).EQ.'M'.OR.TYPCOU(I).EQ.'m')then
           TYPCOU(I)='M'
         ELSEIF(TYPCOU(I).EQ.'V'.OR.TYPCOU(I).EQ.'v')then
           TYPCOU(I)='V'
          ELSE
            write(*,*)'fichier 1D2D.'//ETUDE//' faux'
            write(*,*)'premier caractere ligne ',I
            stop
          endif
        IF(.NOT.FINLEC)THEN
Cliq          if(typcou(i).NE.'P')then
          READ (LIGNE(2:200),*,END=2)I1D(I),I2D(I)
          IF(I1D(i).EQ.0)THEN
            IF(I2D(I).EQ.0)THEN
C cas I1D=0 I2D=0
      READ (LIGNE(2:200),*,END=2)I1D(I),I2D(I),X1(I),Y1(I),X2(I),Y2(I)
      TROUVE=.FALSE.
      IF(TYPCOU(I).EQ.'G')then
         call TRIMXYG(I1D(I),X1(i),Y1(I),TROUVE)
       ELSEIF(TYPCOU(I).EQ.'D')then
          call TRIMXYD(I1D(I),X1(i),Y1(I),TROUVE)
       ELSEIF(TYPCOU(I).EQ.'C')then
          call TRIMXYC(I1D(I),X1(i),Y1(I),TROUVE)
       ELSEIF(TYPCOU(I).EQ.'M')then
          call TRIMXYM(I1D(I),X1(i),Y1(I),TROUVE)
       ELSEIF(TYPCOU(I).EQ.'V')then
          call TRIMXYV(I1D(I),X1(i),Y1(I),TROUVE)
       endif
       IF(.NOT.TROUVE)THEN
          write(*,*)I,' eme ligne coordonnees 1D fausses'
          lcontinue=.false.
C                   stop
       endif
       TROUVE=.FALSE.
       call TRIAXY(I2D(I),X2(i),Y2(I),TROUVE)
       IF(.NOT.TROUVE)THEN
       write(*,*)I,' eme ligne coordonnees 2D fausses'
          lcontinue=.false.
C                    stop
       endif
              ELSE
C cas I1D=0 I2D non nul
                 If(I2D(I).GT.NA.or.I2D(I).LT.0)THEN
                   write(*,*)'numero arete ',I2D(I),' impossible'
                   write(*,*)'pour couplage ',i
                   lcontinue=.false.
C          stop
                 endif    
      READ (LIGNE(2:200),*,END=2)I1D(I),I2D(I),X1(I),Y1(I)
      TROUVE=.FALSE.
      IF(TYPCOU(I).EQ.'G')then
         call TRIMXYG(I1D(I),X1(i),Y1(I),TROUVE)
      ELSEIF(TYPCOU(I).EQ.'D')then
         call TRIMXYD(I1D(I),X1(i),Y1(I),TROUVE)
      ELSEIF(TYPCOU(I).EQ.'C')then
         call TRIMXYC(I1D(I),X1(i),Y1(I),TROUVE)
      ELSEIF(TYPCOU(I).EQ.'M')then
          call TRIMXYM(I1D(I),X1(i),Y1(I),TROUVE)
      ELSEIF(TYPCOU(I).EQ.'V')then
           call TRIMXYV(I1D(I),X1(i),Y1(I),TROUVE)
      endif
      IF(.NOT.TROUVE)THEN
      write(*,*)I,' eme ligne coordonnees 1D fausses'
          lcontinue=.false.
C                    stop
       endif
               ENDIF
          ELSEIF(I2D(I).EQ.0)THEN
C cas I1D  non nul I2D=0
             If(I1D(I).GT.LM(NBB).or.I1D(I).LT.0)THEN
               write(*,*)'numero maille 1D ',I1D(I),' impossible'
               write(*,*)'pour couplage ',i
               lcontinue=.false.
C          stop
             endif    
      READ (LIGNE(2:200),*,END=2)I1D(I),I2D(I),X2(I),Y2(I)
      call TRIAXY(I2D(I),X2(i),Y2(I),TROUVE)
      IF(.NOT.TROUVE)THEN
         write(*,*)I,' eme ligne coordonnees 2D fausses'
         lcontinue=.false.
C                  stop
      endif
         ELSE
C cas I1D non nul I2D non nul
C          READ (LIGNE(2:200),*)I1D(I),I2D(I)
           If(I2D(I).GT.NA.or.I2D(I).LT.0)THEN
              write(*,*)'numero arete ',I2D(I),' impossible'
              write(*,*)'pour couplage ',i
              lcontinue=.false.
c          stop
            endif    
            If(I1D(I).GT.LM(NBB).or.I1D(I).LT.0)THEN
              write(*,*)'numero maille 1D ',I1D(I),' impossible'
        write(*,*)'pour couplage ',i
              lcontinue=.false.
C          stop
            endif    
C fin du if sur id1d et i2d nuls
           ENDIF
C fin du if sur typcou=P
Cliq           endif
C fin du if sur finlec
        ENDIF
C fin du premier if sur finlec
      ENDIF
C fin boucle sur I
      enddo
C fermeture fichier de correspondance entre 1D et 2D    
 3    CLOSE(N)
      write(*,*)'fin de lecture du fichier 1D2D.',ETUDE
      IF(NCOUPLAGE.EQ.0)NCOUplage=I-1
C on fait la correspondance 1D vers 2D
      do ia=1,na
          im1D(IA)=0
      enddo
      DO M=1,NBB
        DO J=LM(M-1)+1,LM(M)
          NARG(J)=0
          NARD(J)=0
          NACE(J)=0
          NAam(J)=0
          NAav(J)=0
          JRG=1
          JRD=1
          JCE=1
          Jam=1
          Jav=1
          DO I=1,NCOUPLAGE
            IF(I1D(I).EQ.J)then
              IF(TYPCOU(I).EQ.'G')then
                  NARG(J)=NARG(J)+1
                  IARG(J,JRG)=I2D(I)
                  JRG=JRG+1
                  IF(IM1D(I2D(I)).EQ.0)THEN
                    IM1D(I2D(I))=J
                  else
      write(*,*)'arete ',I2D(I),' avec deux couplages'
                   lcontinue=.false.
C                    stop
                  endif 
              elseIF(TYPCOU(I).EQ.'D')then
                  NARd(J)=NARd(J)+1
                  IARd(J,JRd)=I2D(I)
                  JRd=JRd+1
                  IF(IM1D(I2D(I)).EQ.0)THEN
                    IM1D(I2D(I))=J
                  else
      write(*,*)'arete ',I2D(I),' avec deux couplages'
                   lcontinue=.false.
C                    stop
                  endif 
              elseIF(TYPCOU(I).EQ.'C')then
                  NAce(J)=NAce(J)+1
                  IAce(J,Jce)=I2D(I)
                  Jce=Jce+1
                  IF(IM1D(I2D(I)).EQ.0)THEN
                    IM1D(I2D(I))=J
                  else
      write(*,*)'arete ',I2D(I),' avec deux couplages'
                   lcontinue=.false.
C                    stop
                  endif 
              elseIF(TYPCOU(I).EQ.'M')then
                  NAam(J)=NAam(J)+1
                  IAam(J,Jam)=I2D(I)
                  Jam=Jam+1
                  IF(IM1D(I2D(I)).EQ.0)THEN
                    IM1D(I2D(I))=J
                  else
      write(*,*)'arete ',I2D(I),' avec deux couplages'
                   lcontinue=.false.
C                    stop
                  endif 
              elseIF(TYPCOU(I).EQ.'V')then
                  NAav(J)=NAav(J)+1
                  IAav(J,Jav)=I2D(I)
                  Jav=Jav+1
                  IF(IM1D(I2D(I)).EQ.0)THEN
                    IM1D(I2D(I))=J
                  else
      write(*,*)'arete ',I2D(I),' avec deux couplages'
                   lcontinue=.false.
C                    stop
                  endif 
C fin du if sur typcou
              ENDIF 
C fin du if sur i1D=J
            endif
C fin boucle sur i
          enddo
          nat(j)=narg(j)+nard(j)+nace(j)+naAM(j)+naav(j)
C fin boucle sur j
        enddo
C fin boucle sur m
      enddo
C on controle la coherence pour le couplage enextremite de bief ou pas
      condlim6=.FALSE.  
      DO M=1,NBB
        DO J=LM(M-1)+2,LM(M)-1
          if(naam(j)+naav(j).ne.0)then
            write(*,*)'couplage extremite en maille courante'
            write(*,*)'maille ',j,'(',j-lm(m-1),') du bief ',m
            lcontinue=.false.
C            stop
          endif
        enddo
        J=LM(M-1)+1
          if(naam(j).ne.nat(j))then
            write(*,*)'couplage courant en extremite'
            write(*,*)'amont du bief ',m
            lcontinue=.false.
C            stop
          endif
          IF(nat(j).NE.0)then
            if(condam(m).NE.6)then
              write(*,*)'incoherence entre couplage et condition limite'
              write(*,*)'amont du bief ',m
              lcontinue=.false.
C              stop
             ELSE
               condlim6=.TRUE.  
             endif
           endif
           J=LM(M)
          if(naav(j).ne.nat(j))then
            write(*,*)'couplage courant en extremite'
            write(*,*)'aval du bief ',m
            lcontinue=.false.
C            stop
          endif
          IF(nat(j).NE.0)then
            if(condav(m).NE.6)then
              write(*,*)'incoherence entre couplage et condition limite'
              write(*,*)'aval du bief ',m
              lcontinue=.false.
C              stop
             ELSE
               condlim6=.TRUE.  
            endif
          endif
C fin boucle sur bief
      enddo
C ecriture des correspondances trouvees      
      OPEN(N,FILE='1D2DTROUVE.'//ETUDE,STATUS='UNKNOWN')
      CALL ecrcoord(N)
      if(.NOT.Lcontinue)stop
        CLOSE(N)
        return
C retour si le fichier de couplage n'existe pas
  1     ncouplage=0
        return
C retour si le fichier est mal lu
  2   write(*,*)'fichier 1D2D.'//ETUDE//' mal lu ligne ',I
      stop
      end
      
C*************************************************************************C 
      SUBROUTINE ECRCOORD(N)
C ecriture pour controle de la correspondance 1D/2D      
C*************************************************************************C
      INTEGER NMAXcou,ncouplage,n,j,nbb
      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NAMAX,NEVAMX,NOUMAX
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150
     :,NOUMAX=3500,NAMAX=185000)
      parameter(nmaxcou=10000,NEVAMX=2)
      INTEGER LM(0:NBMAX),LL
      DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,X2,y1,Y2
      INTEGER I,I2D(NMAXcou),I1D(NMAXcou),IOUV
      LOGICAL GCOUPLAGE(NAMAX),COUPLAGE(NOUMAX),COUPLAGE1D(NAMAX)
      character*1 typcou(nmaxcou)
      CHARACTER ETUDE*20
      integer xnc(0:LMAX),NC(0:lmax)
      double precision xa(namax),ya(namax)
      INTEGER IE1(NOUMAX),IE2(NOUMAX),IA1(NOUMAX),IA2(NOUMAX)
     :     ,J1(NOUMAX),J2(NOUMAX),NOUV(NOUMAX),NBOUV
     :,ne,na,nn,ia
      INTEGER IEVA(NAMAX,NEVAMX)
      INTEGER NREFA(NAMAX) 
Cliq      DOUBLE PRECISION EXPVERTICALEC,EXPHORIZONTALEC

Cliq      COMMON/EXPOSANTSC/EXPVERTICALEC,EXPHORIZONTALEC
      common/I1D2D/Ncouplage,I1D,I2D
      common/typcou/typcou
      COMMON/COUPLAGE1D/COUPLAGE1D
      COMMON/GCOUPLAGE/GCOUPLAGE
      COMMON/OUVCOUPLE/COUPLAGE
      common/XYPLANI/XPLANI,YPLANI
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
      COMMON/NOMETU/ETUDE
      COMMON/COOA/XA,YA
      COMMON/IOUVRA/IE1,IE2,IA1,IA2,J1,J2,NOUV,NBOUV
      COMMON/IELA/IEVA
      COMMON/NARF/NREFA 
      COMMON/NOMB/NE,NA,NN
      
      DO IA=1,na
       couplage1D(ia)=.FALSE.
      enddo
       DO Iouv=1,NBOUV
         couplage(iouv)=.FALSE.
       enddo
       DO I=1,ncouplage
Cliq       IF(TYPCOU(I).EQ.'P')then
Cliq       write(N,'(A1,2F13.4)')TYPCOU(I),EXPVERTICALEC,EXPHORIZONTALEC
CCliq cas ou typcou different de P
Cliq       ELSE
         if(I2D(i).EQ.0)then
           x2=0.
           y2=0.
         else    
           X2=xa(i2D(I))
           y2=YA(I2D(I))
           IF(NREFA(I2D(I)).LT.0)THEN
          DO Iouv=1,NBOUV
            if(ia1(iouv).EQ.I2D(I))THEN
C gcouplage permet de savoir quelle maille 2D(gauche ou droite) 
C             IECE(I2D(I))=IE1(IOUV)
             IF(IE1(iouv).EQ.IEVA(IA1(iouv),1))THEN
               gcouplage(i2D(I))=.TRUE.
               j2(iouv)=2
             else 
               gcouplage(i2D(I))=.FALSE.
               J2(iouv)=1
            endif 
C            ia2(iouv)=ia1(iouv)
            COUPLAGE(IOUV)=.TRUE.
C            COUPLAGE1D(I2D(I))=.TRUE.
            endif
          enddo  
C nrefa positif ou nul
         ELSE
        IF(TYPCOU(I).EQ.'C')THEN
          write(*,*)'ouvrage manquant pour un couplage central'
          write(*,*)'arete ',i2D(I),' couplage ',i
          stop
C pour les autres types, une des deux mailles est exterieure au 2D
        ELSE
          IF(IEVA(I2D(I),1).EQ.0)THEN
            gcouplage(i2D(I))=.FALSE.
          ELSE
            gcouplage(i2D(I))=.TRUE.
          ENDIF           
C fin du if sur le type couplage
        endif 
C fin du if sur nrefa
        ENDIF
C fin du if sur i2D(i)=0
        endif 
        if(I1D(i).EQ.0)then
          x1=0.
          y1=0.
        else    
          J=I1D(I)
          IF(TYPCOU(I).EQ.'G')then
            x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
            Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
           elseIF(TYPCOU(I).EQ.'D')then
             x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
             Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
            elseIF(TYPCOU(I).EQ.'C')then
              x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
     :+xplani(xnc(j-1))+xplani(xnc(j)))
              Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
     :+yplani(xnc(j-1))+yplani(xnc(j)))
             elseIF(TYPCOU(I).EQ.'M')then
          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
             elseIF(TYPCOU(I).EQ.'V')then
          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
C fin du if sur typcou        
             endif
C fin du if sur i1D=0
        endif
      COUPLAGE1D(I2D(I))=.TRUE.
      write(N,'(A1,2I7,4F13.4)')TYPCOU(I),I1D(I),I2D(I),X1,Y1,X2,Y2
CCliq   fin du if sur typcou=P
Cliq      ENDIF
C fin boucle sur I
      enddo
      return
      end
C*************************************************************************C 
      SUBROUTINE COMPDT
C*************************************************************************C

C DONNE UNE VALEUR DE PAS DE TEMPS
C si le pas de temps 1D est plus petit, le pas de temps 2D est reduit

  
      DOUBLE PRECISION DT,TN,TNP1,DTN,CFL,DT0,T
      COMMON/CFLR/DT,CFL,DT0,T
      COMMON/TREEL/TN,DTN,TNP1
       
      
        
        IF (DT.GT.DTN) THEN
            DT=DTN
C        ELSE
C            DTN=DT
        ENDIF

        END 

C*************************************************************************C
            SUBROUTINE INTERMAIL 
C*************************************************************************C
C DONNE LEs VALEURS 1D AU 2D A T N+1/2
C remplit les tableax hcouplage (ia), vxcouplage,vycouplage
C modification le 5/11/20 pour definir
C ccouplts,dcouplts et scouplts par groupe d aretes au lieu d'1 arete
C 13/04/2021 remplacement qsr par abs(qsr) au cas ou probleme en limite
C pour que concentration positive

      INTEGER NAMAX,LMAX
      PARAMETER (NAMAX  = 185000,
     :           LMAX=3000)
c      INTEGER NMAXcou,ncouplage
c      parameter(nmaxcou=10000)
      INTEGER nacoumax
      parameter(nacoumax=50)
      integer nbmax
      parameter(nbmax=150)
      INTEGER M,NBB 
      INTEGER IA,JA
      INTEGER J  
      INTEGER LM(0:NBMAX),LL
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax)
      DOUBLE PRECISION YMD(LMAX),YPD(LMAX),VMD(LMAX),VPD(LMAX)
     :,SMD(LMAX),SPD(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX),
     :     ZFA(NAMAX) 
     :,xangle1d(lmax),yangle1d(lmax)
     :,s1D,v1D,eps1,eps2,paray
     :,hcouplage(namax),vxcouplage(namax),vycouplage(namax)
CConc
Cliq      DOUBLE PRECISION CCOUPLTS(NAMAX),DCOUPLTS(NAMAX),SCOUPLTS(NAMAX)      
Cliq      DOUBLE PRECISION QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
Cliq     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX),qmax
Cliq      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
Cliq      DOUBLE PRECISION VITCHUT
Cliq      DOUBLE PRECISION JRH(LMAX),KS1(LMAX)   
Cliq      DOUBLE PRECISION G
Cliq      DOUBLE PRECISION HSECTION,CMOYEN,VITFRO,ALFAVERTICALEC
Cliq      DOUBLE PRECISION EXPVERTICALEC,EXPHORIZONTALEC
Cliq      DOUBLE PRECISION COEFC,COEFS
Cliq      LOGICAL Horizontale(nacoumax)
Cliq      LOGICAL VERTICALEC,VERTICALECVAR,HORIZONTALEC
Cliq      INTEGER NEMAX
Cliq      PARAMETER(NEMAX=94000)
Cliq       DOUBLE PRECISION SE(NEMAX),LA(NAMAX)
Cliqcvar       LOGICAL SEDVAR
CConcf

c      common/I1D2D/Ncouplage,I1D,I2D
c      common/typcou/typcou
      common/hvcouplage/Hcouplage,vxcouplage,vycouplage
      COMMON/XYANGLE1D/XANGLE1D,YANGLE1D
      COMMON/YMPD/YMD,YPD
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/ZARE/ZFA  
      COMMON/PARAM/EPS1,EPS2,PARAY
      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
CConc
Cliq       COMMON/CCOUPTS/CCOUPLTS,DCOUPLTS,SCOUPLTS      
Cliq       COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
Cliq      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
Cliqcvar       COMMON/SEDVAR/SEDVAR
Cliq        COMMON/SOLIDE/JRH,KS1
Cliq      COMMON/GRAV/G
Cliq      COMMON/EXPOSANTSC/EXPVERTICALEC,EXPHORIZONTALEC
Cliq      COMMON/VARC/VERTICALEC,VERTICALECVAR,HORIZONTALEC
Cliq      COMMON/DOSE/SE,LA
Cliq        EXTERNAL VITCHUT
CConcf

      DO M=1,NBB
        DO J=LM(M-1)+1,LM(M)
          IF(NAT(J).GT.0)THEN
            IF(NARG(J).GT.0)THEN
             do JA=1,NARG(J)
              IA=IARG(j,ja)
c             hcouplage(ia)=0.5*(ymd(j)+ypd(j-1))+ctdf(j)-zfa(ia)
               hsection=0.5*(ymd(j)+ypd(j-1))
               hcouplage(ia)=hsection+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=0.5*(smd(j)+spd(j-1))
               IF(S1D.gt.eps1)then
                 v1d=0.5*(smd(j)*vmd(j)+spd(j-1)*vpd(j-1))/s1D
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
C pour eviter grosses concentrations on prend le debit maximal
Cliq                qmax=max(smd(j)*vmd(j),spd(j-1)*vpd(j-1))
Cliq                IF(qmax.gt.EPS1)then
CConc            
Cliq              if(verticalec)then
Cliq               if(verticalecvar)then
C calcul du coefficient expverticalec
Cliq                if(JRH(J).GT.EPS1)then
Cliq             vitfro=SQRT(G*JRH(J))
Cliq             expverticalec=min(2.5*vitchut(dmob(j-1))/vitfro,1.)
Cliq               else
Cliq             expverticalec=1.
Cliq               endif
C fin du if sur verticalecvar
Cliq               endif
Cliq                 cmoyen=abs(qsr(j-1))/(ros*qmax)
Cliq                 alfaverticalec=(hcouplage(ia)/hsection)**expverticalec
Cliq       ccouplts(ia)=cmoyen*alfaverticalec                     
Cliqcvar           if(sedvar)then
Cliqcvar           scouplts(ia)=smob(j-1)**alfaverticalec
Cliqcvar           dcouplts(ia)=dmob(j-1)/smob(j-1)*scouplts(ia)
Cliqcvar           else
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
CliqC fin du if sur sedvar
Cliqcvar           endif
C else du if sur verticalec
Cliq               else 
Cliq                 ccouplts(ia)=abs(qsr(j-1))/(ros*qmax)
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
C fin du if sur verticalec
Cliq               endif 
Cliq               else
Cliq           ccouplts(ia)=0.
Cliq           dcouplts(ia)=1.
Cliq           scouplts(ia)=1.
Cliq         endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C fin du do sur ia
             enddo
C si narg  = 0
C          else
C fin du if sur narg(j)
          endif
            IF(NARD(J).GT.0)THEN
             do JA=1,NARD(J)
              IA=IARD(j,ja)
c             hcouplage(ia)=0.5*(ymd(j)+ypd(j-1))+ctdf(j)-zfa(ia)
               hsection=0.5*(ymd(j)+ypd(j-1))
               hcouplage(ia)=hsection+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=0.5*(smd(j)+spd(j-1))
               IF(S1D.gt.eps1)then
                 v1d=0.5*(smd(j)*vmd(j)+spd(j-1)*vpd(j-1))/s1D
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
C pour eviter grosses concentrations on prend le debit maximal
Cliq                qmax=max(smd(j)*vmd(j),spd(j-1)*vpd(j-1))
Cliq                IF(qmax.gt.EPS1)then
CConc            
Cliq              if(verticalec)then
Cliq               if(verticalecvar)then
C calcul du coefficient expverticalec
Cliq             vitfro=SQRT(G*JRH(J))
Cliq             expverticalec=2.5*vitchut(dmob(j-1))/vitfro
C fin du if sur verticalecvar
Cliq               endif
Cliq                 cmoyen=abs(qsr(j-1))/(ros*qmax)
Cliq                 alfaverticalec=(hcouplage(ia)/hsection)**expverticalec
Cliq       ccouplts(ia)=cmoyen*alfaverticalec                     
Cliqcvar           if(sedvar)then
Cliqcvar           scouplts(ia)=smob(j-1)**alfaverticalec
Cliqcvar           dcouplts(ia)=dmob(j-1)/smob(j-1)*scouplts(ia)
Cliqcvar           else
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
CliqC fin du if sur sedvar
Cliqcvar           endif
C else du if sur verticalec
Cliq               else 
Cliq                 ccouplts(ia)=abs(qsr(j-1))/(ros*qmax)
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
C fin du if sur verticalec
Cliq               endif 
Cliq               else
Cliq           ccouplts(ia)=0.
Cliq           dcouplts(ia)=1.
Cliq           scouplts(ia)=1.
Cliq         endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C fin du do sur ia
             enddo
C si nard  = 0
C          else
C fin du if sur nard(j)
          endif
            IF(NAce(J).GT.0)THEN
             do JA=1,NACE(J)
              IA=IACE(j,ja)
c             hcouplage(ia)=0.5*(ymd(j)+ypd(j-1))+ctdf(j)-zfa(ia)
               hsection=0.5*(ymd(j)+ypd(j-1))
               hcouplage(ia)=hsection+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=0.5*(smd(j)+spd(j-1))
               IF(S1D.gt.eps1)then
                 v1d=0.5*(smd(j)*vmd(j)+spd(j-1)*vpd(j-1))/s1D
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
C pour eviter grosses concentrations on prend le debit maximal
Cliq                qmax=max(smd(j)*vmd(j),spd(j-1)*vpd(j-1))
Cliq                IF(qmax.gt.EPS1)then
CConc            
Cliq              if(verticalec)then
Cliq               if(verticalecvar)then
C calcul du coefficient expverticalec
Cliq             vitfro=SQRT(G*JRH(J))
Cliq             expverticalec=2.5*vitchut(dmob(j-1))/vitfro
C fin du if sur verticalecvar
Cliq               endif
Cliq                 cmoyen=abs(qsr(j-1))/(ros*qmax)
Cliq                 alfaverticalec=(hcouplage(ia)/hsection)**expverticalec
Cliq       ccouplts(ia)=cmoyen*alfaverticalec                     
Cliqcvar           if(sedvar)then
Cliqcvar           scouplts(ia)=smob(j-1)**alfaverticalec
Cliqcvar           dcouplts(ia)=dmob(j-1)/smob(j-1)*scouplts(ia)
Cliqcvar           else
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
CliqC fin du if sur sedvar
Cliqcvar           endif
C else du if sur verticalec
Cliq               else 
Cliq                 ccouplts(ia)=abs(qsr(j-1))/(ros*qmax)
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
C fin du if sur verticalec
Cliq               endif 
Cliq               else
Cliq           ccouplts(ia)=0.
Cliq           dcouplts(ia)=1.
Cliq           scouplts(ia)=1.
Cliq         endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C fin du do sur ia
             enddo
C si nace  = 0
C          else
C fin du if sur nace(j)
          endif
            IF(NAAM(J).GT.1)THEN
CConc
Cliq             if(horizontalec)then
Cliq               coefc=0.
Cliq               coefs=0.
Cliq               cmoyen=0.
Cliq             endif  
CConcf
             do JA=1,NAAM(J)
              IA=IAAM(j,ja)
CConc
Cliq              horizontale(ja)=.FALSE.
CConcf
               hsection=ypd(j)
               hcouplage(ia)=hsection+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=spd(j)
               IF(S1D.gt.eps1)then
                 v1d=vpd(j)
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
Cliq                IF(v1D.gt.EPS1)then
Cliq              if(horizontalec)then
Cliq                horizontale(ja)=.TRUE.
Cliq                coefs=coefs+hcouplage(ia)*la(ia)
Cliq                coefc=coefc+la(ia)*(hcouplage(ia)**exphorizontalec)
Cliq                cmoyen=abs(qsr(j))/(ros*V1D*S1D)
C else du if sur horizontalec
Cliq               else 
Cliq                 ccouplts(ia)=abs(qsr(j))/(ros*V1D*S1D)
C fin du if sur horizontalec
Cliq               endif 
Cliq           dcouplts(ia)=dmob(j)
Cliq           scouplts(ia)=smob(j)
Cliq               else
Cliq           ccouplts(ia)=0.
Cliq           dcouplts(ia)=1.
Cliq           scouplts(ia)=1.
Cliq         endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C fin du do sur ia
             enddo
CConc
Cliq             if(horizontalec)then
Cliq               if(cmoyen.GT.EPS2)then
Cliq               cmoyen=cmoyen*coefs/coefc
Cliq             do JA=1,NAAM(J)
Cliq              IA=IAAM(j,ja)
Cliq             IF(horizontale(ja))then
Cliq                 ccouplts(ia)=cmoyen*hcouplage(ia)**(exphorizontalec-1.)                    
C fin if sur horizontale                
Cliq             endif 
C fin du do sur ia
Cliq             enddo
CliqC else cmoyen=0
Cliq             else
Cliq             do JA=1,NAAM(J)
Cliq              IA=IAAM(j,ja)
Cliq             IF(horizontale(ja))then
Cliq                 ccouplts(ia)=0.                    
C fin if sur horizontale                
Cliq             endif 
C fin du do sur ia
Cliq             enddo
C fin du if sur cmoyen=0             
Cliq             endif  
C fin du if sur horizontalec             
Cliq             endif  
CConcf
            elseif (naAM(j).eq.1)then
              IA=IAAM(j,1)
C cas ou typcou amont             
             hcouplage(ia)=ypd(j)+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=spd(j)
               IF(S1D.gt.eps1)then
                 v1d=vpd(j)
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
Cliq               IF(v1D.gt.eps1)then
Cliq                 ccouplts(ia)=abs(qsr(j))/(ros*v1d*s1d)
Cliq                 dcouplts(ia)=dmob(j)
Cliq                 scouplts(ia)=smob(j)
Cliq               else
Cliq                 ccouplts(ia)=0.
Cliq                 dcouplts(ia)=1.
Cliq                 scouplts(ia)=1.
Cliq               endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq                 ccouplts(ia)=0.
Cliq                 dcouplts(ia)=1.
Cliq                 scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C si naam  = 0
C          else
C fin du if sur naAM(j)
          endif
            IF(NAAV(J).GT.1)THEN
CConc
Cliq             if(horizontalec)then
Cliq               coefc=0.
Cliq               coefs=0.
Cliq               cmoyen=0.
Cliq             endif  
CConcf
             do JA=1,NAAV(J)
              IA=IAAV(j,ja)
CConc
Cliq              horizontale(ja)=.FALSE.
CConcf
               hsection=ymd(j-1)
               hcouplage(ia)=hsection+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=smd(j-1)
               IF(S1D.gt.eps1)then
                 v1d=vmd(j-1)
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
Cliq                IF(v1D.gt.EPS1)then
Cliq              if(horizontalec)then
Cliq                horizontale(ja)=.TRUE.
Cliq                coefs=coefs+hcouplage(ia)*la(ia)
Cliq                coefc=coefc+la(ia)*(hcouplage(ia)**exphorizontalec)
Cliq                cmoyen=abs(qsr(j-1))/(ros*V1D*S1D)
C else du if sur horizontalec
Cliq               else 
Cliq                 ccouplts(ia)=abs(qsr(j-1))/(ros*V1D*S1D)
C fin du if sur horizontalec
Cliq               endif 
Cliq           dcouplts(ia)=dmob(j-1)
Cliq           scouplts(ia)=smob(j-1)
Cliq               else
Cliq           ccouplts(ia)=0.
Cliq           dcouplts(ia)=1.
Cliq           scouplts(ia)=1.
Cliq         endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq               ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C fin du do sur ia
             enddo
CConc
Cliq             if(horizontalec)then
Cliq               if(cmoyen.GT.EPS2)then
Cliq               cmoyen=cmoyen*coefs/coefc
Cliq             do JA=1,NAAV(J)
Cliq              IA=IAAV(j,ja)
Cliq             IF(horizontale(ja))then
Cliq                 ccouplts(ia)=cmoyen*hcouplage(ia)**(exphorizontalec-1.)                    
C fin if sur horizontale                
Cliq             endif 
C fin du do sur ia
Cliq             enddo
CliqC else cmoyen=0
Cliq             else
Cliq             do JA=1,NAAV(J)
Cliq              IA=IAAV(j,ja)
Cliq             IF(horizontale(ja))then
Cliq                 ccouplts(ia)=0.                    
C fin if sur horizontale                
Cliq             endif 
C fin du do sur ia
Cliq             enddo
C fin du if sur cmoyen=0             
Cliq             endif  
C fin du if sur horizontalec             
Cliq             endif  
CConcf
            elseif (naAV(j).eq.1)then
              IA=IAAV(j,1)
C cas ou typcou aval            
             hcouplage(ia)=ymd(j-1)+ctdf(j)-zfa(ia)
             IF(hcouplage(ia).lt.paray)then
               hcouplage(ia)=0.
               vxcouplage(ia)=0.
               vycouplage(ia)=0.
CConc
Cliq             ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
             else
               S1D=smd(j-1)
               IF(S1D.gt.eps1)then
                 v1d=vmd(j-1)
                 vxcouplage(ia)=v1D*xangle1d(j)
                 vycouplage(ia)=v1D*yangle1d(j)
CConc
Cliq               IF(v1D.gt.eps1)then
Cliq                 ccouplts(ia)=abs(qsr(j-1))/(ros*v1d*s1d)
Cliq                 dcouplts(ia)=dmob(j-1)
Cliq                 scouplts(ia)=smob(j-1)
Cliq               else
Cliq                 ccouplts(ia)=0.
Cliq                 dcouplts(ia)=1.
Cliq                 scouplts(ia)=1.
Cliq               endif
CConcf
               else
                 vxcouplage(ia)=0.
                 vycouplage(ia)=0.
CConc
Cliq             ccouplts(ia)=0.
Cliq               dcouplts(ia)=1.
Cliq               scouplts(ia)=1.
CConcf
C fin if sur s1D
                 endif
C fin if sur hcouplage=0                 
             endif 
C si naav  = 0
C          else
C fin du if sur naav(j)
          endif

C fin du if sur nat(j)
          endif
C fin du do sur j
         enddo
C fin du do sur M
       enddo
       
      RETURN
      END

C************************************************************************C
      SUBROUTINE PROJEC
C************************************************************************C

      INTEGER LMAX,LNCMAX,NBMAX,NBB,M,J,NCMAX
      PARAMETER (LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
      DOUBLE PRECISION x1,X2,Y1,Y2,DELTA,eps1,eps2,paray,dx,dy 
      integer xnc(0:LMAX),NC(0:lmax),LM(0:NBMAX),LL
      DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,xangle1D(lmax),yangle1d(lmax)
      
      common/XYPLANI/XPLANI,YPLANI
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB 
      COMMON/PARAM/EPS1,EPS2,PARAY
      COMMON/XYANGLE1D/XANGLE1D,YANGLE1D

C CALCUL DES DIRECTIONS DE PROJECTION         
       
      DO 10 M=1,NBB
      DO 1 j=LM(M-1)+2,LM(M)-1
        
          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
          x2=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
          Y2=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
          dx=x2-x1
          dy=y2-y1
          delta=sqrt(dx**2+dy**2)
          if(delta.gt.EPS1)then
            xangle1d(j)=dx/delta
            yangle1d(j)=dy/delta
          else
            write(*,*)'centres de sections confondues'
      write(*,*)'bief ',m,' sections ',j-1-lm(m-1),' et ',j-lm(m-1)
             xangle1D(j)=0.
             yangle1d(j)=0.
          endif  

 1      CONTINUE
        xangle1D(lm(m-1)+1)=xangle1d(lm(m-1)+2)
        yangle1D(lm(m-1)+1)=yangle1d(lm(m-1)+2)
        xangle1D(lm(m))=xangle1d(lm(m)-1)
        yangle1D(lm(m))=yangle1d(lm(m)-1)
 10     CONTINUE
         RETURN    
         END

C*************************************************************************C
       SUBROUTINE calculFLSQ4
CALCUL Des conditiones limites en couplage
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL De fls,flq et sc en extremite bief POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER LMAX,NBMAX,NACOUMAX
      PARAMETER(LMAX=3000,NBMAX=150,NACOUMAX=50)
      INTEGER LM(0:NBMAX),LL,NBB,m,j,ja,ia
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax),ij1
      
      DOUBLE PRECISION SMBFLS,SMBFLQ,FLS(LMAX),FLQ(LMAX),SN(LMAX)
     :,SNP1(lmax),QN(lmax),qnp1(lmax),SMBS,SC,vc
     :,CHEZY,GRAV,EPS,EPSY,EPSM,qs2,d2,s2
CConc
Cliq      DOUBLE PRECISION QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
Cliq     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
CConcf

      integer LDETSJ
      double precision detfl
      external LDETSJ,detfl

      COMMON/DDFLUX/FLS,FLQ
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
CConc
Cliq       COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
CConcf
      
      DO M=1,NBB
        J=LM(m-1)+1
         IF(NAT(J).GT.0)then
           FLS(J)=0.
           FLQ(j)=0.
           SC=0.
CConc
Cliq           QSR(j)=0.
Cliq           DMOB(j)=1.
Cliq           SMOB(j)=1.
CConcf     
           do ja=1,NAAM(j)
            IA=IAAM(j,JA)
      CALL smbflsq(IA,SMBS,SMBFLS,SMBFLQ,qs2,d2,s2)
C cette remise a zero  est erronne car signe de fls a controler
            if(smbfls.lt.-eps)then
              sc=sc+smbs
              fls(j)=fls(j)-smbfls
              flq(j)=flq(j)-smbflq
CConc
Cliq         call mixage(qsr(j),dmob(j),smob(j),-qs2,d2,s2)
CConcf
            elseif(smbfls.lt.eps)then
c               write(*,*)'incoherence amont bief ',M
c               write(*,*)'debit sortant du 1D mis a 0'
c               write(*,*)'mais debit entre dans 2D'
               smbfls=0.
               smbflq=0.
Cliq               qs2=0.
C fin du if sur smbfls >0
c             endif
            else
              sc=sc+smbs
              fls(j)=fls(j)+smbfls
              flq(j)=flq(j)+smbflq
CConc
Cliq         call mixage(qsr(j),dmob(j),smob(j),qs2,d2,s2)
CConcf
C fin du if sur smbfls >0
        endif  
          enddo
          if(sc.lt.eps)then
            snp1(j)=0.
            fls(j)=0.
            flq(j)=0.
            qnp1(j)=0.
C si sc non nul
          else
c120121            snp1(j)=2.*sc-sn(j)
c120121            if(snp1(j).lt.eps)then
c120121              snp1(j)=0.
c120121              sc=0.5*sn(j)
c120121              qnp1(j)=0.
c120121              fls(j)=0.5*qn(j)
c120121              VC=FLS(j)/SC
c120121              IJ1=LDETSJ(1,SC,J)
c120121              FLQ(J) = DETFL(SC,VC,IJ1,J)
c120121            else
              VC=FLS(j)/SC
c120121              qnp1(j)=vc*snp1(j)
c120121 on fait une erreur temporelle de dt/2
             qnp1(j)=fls(j)
             snp1(j)=sc
C        qnp1(j)=2.*fls(j)-qn(j)
C modification provisoire de flq car probleme de signe le 5/8/2011:
C on recalcule flq issu du 2D par la fonction detfl (methode 1D)
              IJ1=LDETSJ(1,SC,J)
              FLQ(J) = DETFL(SC,VC,IJ1,J)
C fin du if sur snp1
c120121            endif
C fin du if sur sc
          endif
C fin du if sur nat
        endif
        J=LM(M)
        IF(NAT(J).GT.0)then
           FLS(J-1)=0.
           FLQ(j-1)=0.
           SC=0.
C a priori inutile en aval  
CConc
Cliq           QSR(j-1)=0.
Cliq           DMOB(j-1)=1.
Cliq           SMOB(j-1)=1.
CConcf     
          do ja=1,NAAV(j)
            IA=IAAV(j,JA)
      CALL smbflsq(IA,SMBS,SMBFLS,SMBFLQ,qs2,d2,s2)
            if(smbfls.gt.eps)then
            sc=sc+smbs
c45              fls(j)=fls(j)+smbfls
c45              flq(j)=flq(j)+smbflq
            fls(j-1)=fls(j-1)+smbfls
            flq(j-1)=flq(j-1)+smbflq
CConc
C serait utile si d�bits n�gatifs (entrants par extremite aval mais pas prevu 
C dans le 1D avec TS voire le 1D)
c45          call mixage(qsr(j-1),dmob(j-1),smob(j-1),qs2,d2,s2)
Cliq          call mixage(qsr(j-1),dmob(j-1),smob(j-1),qs2,d2,s2)
CConcf
            elseif(smbfls.gt.-eps)then
c               write(*,*)'incoherence aval bief ',M
c               write(*,*)'debit entrant dans le 1D mis a 0'
c               write(*,*)'mais debit sort du 2D'
               smbfls=0.
               smbflq=0.
Cliq               qs2=0.
        else 
            sc=sc+smbs
c45              fls(j)=fls(j)+smbfls
c45              flq(j)=flq(j)+smbflq
            fls(j-1)=fls(j-1)-smbfls
            flq(j-1)=flq(j-1)-smbflq
CConc
C serait utile si d�bits n�gatifs (entrants par extremite aval mais pas prevu 
C dans le 1D avec TS voire le 1D)
c45          call mixage(qsr(j-1),dmob(j-1),smob(j-1),qs2,d2,s2)
Cliq          call mixage(qsr(j-1),dmob(j-1),smob(j-1),-qs2,d2,s2)
CConcf
          endif 
          enddo
          if(sc.lt.eps)then
            snp1(j)=0.
            fls(j-1)=0.
            flq(j-1)=0.
            qnp1(j)=0.
C si sc non nul
          else
C100813            snp1(j)=2.*sc-sn(j)
             snp1(j)=sc
C100813            if(snp1(j).lt.eps)then
C100813              snp1(j)=0.
C100813              sc=0.5*sn(j)
C100813                 qnp1(j)=0.
C100813              fls(j-1)=0.5*qn(j)
C100813              VC=FLS(j-1)/SC
C100813              IJ1=LDETSJ(1,SC,J-1)
C100813              FLQ(J-1) = DETFL(SC,VC,IJ1,J-1)
C100813            else
              VC=FLS(j-1)/SC
              qnp1(j)=vc*snp1(j)
c                qnp1(j)=2.*fls(j-1)-qn(j)
C modification provisoire de flq car probleme de signe le 5/8/2011:
C on recalcule flq issu du 2D par la fonction detfl (methode 1D)
              IJ1=LDETSJ(1,SC,J-1)
              FLQ(J-1) = DETFL(SC,VC,IJ1,J-1)
C fin du if sur snp1
C100813            endif
C fin du if sur sc
          endif
C fin du if sur nat
        endif
c fin boucle sur bief
      enddo
      return
      end

C*************************************************************************C
       SUBROUTINE cl6sanscouplage
CALCUL Des conditiones limites en couplage si pas de couplage
C calcul fait comme si reflexion
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL De fls,flq et sc en extremite bief POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER LMAX,NBMAX,NACOUMAX,m,j,ij,niter
      PARAMETER(LMAX=3000,NBMAX=150,NACOUMAX=50)
      INTEGER LM(0:NBMAX),LL,NBB
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax),ij1,ij2
      
      DOUBLE PRECISION FLS(LMAX),FLQ(LMAX),SN(LMAX)
     :,SNP1(lmax),QN(lmax),qnp1(lmax),SC,vc,S2,V2
     :,smd(lmax),spd(lmax),vmd(lmax),vpd(lmax)
     :,s1,V1,y1,H1,ylm,ylp,yl2
     :,CHEZY,GRAV,EPS,EPSY,EPSM

CConc
Cliq      DOUBLE PRECISION QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
Cliq     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
CConcf
      INTEGER LDETSJ,LDETYJ
      DOUBLE PRECISION DETFL,DETYN,DETSN,DETH
      EXTERNAL DETFL,DETYN,DETSN,DETH,LDETSJ,LDETYJ

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DDFLUX/FLS,FLQ
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
CConc
Cliq       COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
CConcf
      
      DO M=1,NBB
        J=LM(m-1)+1
         IF(NAT(J).GT.0)then
CConc
Cliq           QSR(j)=0.
Cliq           DMOB(j)=1.
Cliq           SMOB(j)=1.
CConcf     
           S2=SPD(J)
           V2=VPD(J)
           IJ2=LDETSJ(1,S2,J)
           IF(V2.EQ.0.)THEN
             SC=S2
           ELSE
             CALL DCHAMV(SC,S2,V2,IJ2,J)
           ENDIF
           if(sc.lt.eps)then
             snp1(j)=0.
             fls(j)=0.
             flq(j)=0.
             qnp1(j)=0.
C si sc non nul
          else
            snp1(j)=sc
C100813            snp1(j)=2.*sc-sn(j)
C100813            if(snp1(j).lt.eps)then
C100813              snp1(j)=0.
C100813              sc=0.5*sn(j)
C fin du if sur snp1
C100813            endif
              qnp1(j)=0.
              fls(j)=0.
              VC=0.
              IJ1=LDETSJ(1,SC,J)
              FLQ(J) = DETFL(SC,VC,IJ1,J)
C fin du if sur sc
          endif
C fin du if sur nat
        endif
        J=LM(M)
        IF(NAT(J).GT.0)then
C a priori inutile en aval  
CConc
Cliq          QSR(j-1)=0.
Cliq          DMOB(j-1)=1.
Cliq          SMOB(j-1)=1.
CConcf     
      niter=0
C smd et vmd sont a tnp1    
       S1 = SMD(J-1)
       V1 = VMD(J-1)
       IJ1=LDETSJ(1,S1,J-1)
C SI V1<0 ON A UNE DETENTE ; SI V1>0 UN CHOC
       IF(ABS(V1).LT.EPS)THEN
C             IF(V1.EQ.0.)THEN
          SC=S1
       ELSEIF(V1.GT.0.)THEN
         CALL DCHAMV(SC,S1,V1,IJ1,J-1)
       ELSE
C RESOLUTION DE H(SC)=H1+V1
       Y1=DETYN(1,S1,IJ1)
       H1 = DETH(Y1,IJ1,J-1)
       IF(H1+V1.LE.0.)THEN
                 SC=0.
       ELSE
                 YLM=0.
                 YLP=Y1
 1100            YL2=.5*(YLP+YLM)
                 IJ=LDETYJ(1,YL2,J-1)
                 V2=V1+H1-DETH(YL2,IJ,J-1)
                 IF(V2.LT.0.)THEN
                   YLP=YL2
                 ELSE
                   YLM=YL2
                 ENDIF
                 IF(ABS(YLP-YLM).LT.EPSY)GO TO 1101
                 NITER=NITER+1
                 IF(NITER.GT.50)THEN
      IF(M.GT.1)THEN
          WRITE(*,*)'CONDITION AVAL NON VERIFIEE ',ABS(YLP-YLM),
     :' BIEF ',M
      else
          WRITE(*,*)'CONDITION AVAL NON VERIFIEE ',ABS(YLP-YLM)
      endif 
          WRITE(*,*)'DELTA OBTENU SUR V=',V2,
     :'  Y MAX=',YLP,'Y MIN=',YLM
                 ELSE
                   GO TO 1100
                 ENDIF
 1101            IF(YL2.GT.0.)THEN

                   YL2=.5*(YLP+YLM)
                   IJ=LDETYJ(1,YL2,J-1)
                   SC=DETSN(1,YL2,IJ)

                 ELSE
                   SC=0.
                 ENDIF
               ENDIF
             ENDIF
             VC=0.
             if(sc.lt.eps)then
               snp1(j)=0.
               fls(j-1)=0.
               flq(j-1)=0.
               qnp1(j)=0.
C si sc non nul
          else
C100813            snp1(j)=2.*sc-sn(j)
            snp1(j)=sc
C100813            if(snp1(j).lt.eps)then
C100813              snp1(j)=0.
C100813              sc=0.5*sn(j)
C fin du if sur snp1
C100813            endif
              qnp1(j)=0.
              fls(j-1)=0.
              VC=0.
              IJ1=LDETSJ(1,SC,J-1)
              FLQ(J-1) = DETFL(SC,VC,IJ1,J-1)
C fin du if sur sc
          endif
C fin du if sur nat
        endif
c fin boucle sur bief
      enddo
      return
      end

C*************************************************************************C
       SUBROUTINE SMBFLSQ(IA,SMBS,SMBFLS,SMBFLQ,QS2,D2,S2)
CALCUL Des FLUX en extremite du 1D
C*************************************************************************C
      INTEGER NAMAX,NEVAMX,NEMAX
      PARAMETER (NAMAX=185000,NEVAMX = 2,NEMAX=94000)
      INTEGER IEVA(NAMAX,NEVAMX)
      INTEGER IA,IED,IEG 
      DOUBLE PRECISION FHA(NAMAX),FQUA(NAMAX),FQVA(NAMAX) 
      DOUBLE PRECISION XNA(NAMAX),YNA(NAMAX),XTA(NAMAX),YTA(NAMAX)
      DOUBLE PRECISION SE(NEMAX),LA(NAMAX),HAE(NAMAX)
      DOUBLE PRECISION HOUV(0:NAMAX,NEVAMX),QOUV(0:NAMAX,NEVAMX)
      DOUBLE PRECISION VOUV(0:NAMAX,NEVAMX),VTOUV(0:NAMAX,NEVAMX)
      INTEGER NREFA(NAMAX) 
      DOUBLE PRECISION SMBS,SMBFLS,SMBFLQ,QS2,D2,S2
      LOGICAL GCOUPLAGE(NAMAX)
CConc
Cliq      DOUBLE precision DIAMA(NAMAX),SIGMAA(NAMAX)
Cliq     :,SIGMAouv(0:naMAX,nevamx),DIAMouv(0:NAMAX,NEVAMX)
Cliq      DOUBLE PRECISION FHCA(NAMAX),COOUV(0:NAMAX,NEVAMX)
Cliq      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1

CConcf

      COMMON/HALI/HAE
      COMMON/IELA/IEVA
      COMMON/FLUX/FHA,FQUA,FQVA 
      COMMON/VARE/XNA,YNA,XTA,YTA
      COMMON/DOSE/SE,LA   
      COMMON/HQOUV/HOUV,QOUV 
      COMMON/VOUVH/VOUV,VTOUV
      COMMON/NARF/NREFA 
      COMMON/GCOUPLAGE/GCOUPLAGE

CConc
Cliq      COMMON/FLUX1/FHCA
Cliq      COMMON/HCOUV/COOUV
Cliq      COMMON/DSIGMAouv/DIAMouv,SIGMAouv
Cliq      COMMON/DSIGMAA/DIAMA,SIGMAA
Cliq      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
CConcf
      
      
      SMBs=0.
      smbfls=0.
      smbflq=0.
      QS2=0.
      D2=0.
      S2=0.
      IEG=IEVA(IA,1) 
      IED=IEVA(IA,2)

      IF (IED.EQ.0) THEN
         IF(NREFA(IA).EQ.-2) THEN
c           IF(HG(IA).GT.PARAY)THEN
C on prend la valeur de la vitesse normale C�t� 2D (toujours d�finie)
            SMBflS=(QOUV(IA,1))    
            SMBflQ=(QOUV(IA,1)*VOUV(IA,1)) 
            smbs=HOUV(IA,1)*la(ia)
CConc
Cliq            QS2=ROS*QOUV(IA,1)*COOUV(IA,1)
C condition supprimee car qs2 du meme signe que QOUV
C qouv positif  si va dans le 1D mais negatif sinon
CliqC            IF(QS2.LT.0.)QS2=0.
Cliq            D2=DIAMouv(IA,1)
Cliq            S2=SIGMAouv(IA,1)
CConcf
C           ELSE
C             SMBQ=0.
c           ENDIF   
         ELSEIF(NREFA(IA).EQ.-1) THEN
C           IF(HG(IA).GT.PARAY)THEN
C  ce cas existe t il? non car on ne permet pas couplage type C en extremite 1D
            SMBFLQ=(QOUV(IA,1)*VOUV(IA,1))  
     :      +(FQUA(IA)*XnA(IA)+FQVA(IA)*YnA(IA))*LA(IA)    
            SMBFLS=(QOUV(IA,1)+FHA(IA)*LA(IA)) 
            smbs=houv(ia,1)*la(ia)
CConc
Cliq            QS2=ROS*QOUV(IA,1)*COOUV(IA,1)
C condition supprimee car qs2 du meme signe que QOUV
C qouv positif  si va dans le 1D mais negatif sinon
CliqC            IF(QS2.LT.0.)QS2=0.
Cliq      D2=DIAMouv(IA,1)
Cliq      S2=SIGMAouv(IA,1)
Cliq      IF(FHCA(IA).GT.0.)THEN
Cliq        CALL MIXAGE(QS2,D2,S2,ROS*FHCA(IA)*LA(IA)
Cliq     :,DIAMA(IA),SIGMAA(IA))
Cliq      ELSE
Cliq        QS2=MAX(QS2-ROS*FHCA(IA)*LA(IA),0.)
Cliq      ENDIF
CConcf
C           ELSE
C             SMBQ=0.
C           ENDIF   
         ELSE
           SMBFLQ=(FQUA(IA)*XNA(IA)+FQVA(IA)*YNA(IA))*LA(IA)
           SMBFLS=FHA(IA)*LA(IA) 
           smbs=haE(ia)*la(ia)
CConc
Cliq            QS2=ROS*FHCA(IA)*LA(IA)
C condition supprimee car qs2 du meme signe que QOUV
C qouv positif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq        D2=DIAMA(IA)
Cliq        S2=SIGMAA(IA)
CConcf
         ENDIF 
       ELSEIF(IEG.EQ.0)then
         IF(NREFA(IA).EQ.-2) THEN
c           IF(HD(IA).GT.PARAY)THEN
C on prend la valeur de la vitesse normale C�t� 2D (toujours d�finie)
           SMBFLQ=-QOUV(IA,2)*VOUV(IA,2)    
           SMBFLS=-QOUV(IA,2) 
           SMBS=HOUV(IA,2)*LA(IA)
CConc
Cliq            QS2=-ROS*QOUV(IA,2)*COOUV(IA,2)
C condition supprimee car qs2 du meme signe que QOUV
C qouv negatif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq      D2=DIAMouv(IA,2)
Cliq      S2=SIGMAouv(IA,2)
CConcf
C           ELSE
C             SMBQ=0.
c           ENDIF   
         ELSEIF(NREFA(IA).EQ.-1) THEN
c           IF(HD(IA).GT.PARAY)THEN
C  ce cas existe t il? normalement non
           SMBFLQ=-(QOUV(IA,2)*VOUV(IA,2))  
     :      -(FQUA(IA)*XnA(IA)+FQVA(IA)*YnA(IA))*LA(IA)    
           SMBFLS=-QOUV(IA,2)-FHA(IA)*LA(IA) 
           SMBS=HOUV(IA,2)*LA(IA)
CConc
Cliq            QS2=-ROS*QOUV(IA,2)*COOUV(IA,2)
C condition supprimee car qs2 du meme signe que QOUV
C qouv negatif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq      D2=DIAMouv(IA,2)
Cliq      S2=SIGMAouv(IA,2)
Cliq      IF(FHCA(IA).LT.0.)THEN
Cliq              CALL MIXAGE(QS2,D2,S2,-ROS*FHCA(IA)*LA(IA)
Cliq     :,DIAMA(IA),SIGMAA(IA))
Cliq      ELSE
Cliq        QS2=MAX(QS2-ROS*FHCA(IA)*LA(IA),0.)
Cliq      ENDIF
CConcf
C           ELSEC
c             SMBQ=0.
c           ENDIF   
         ELSE
           SMBFLQ=-(FQUA(IA)*XNA(IA)+FQVA(IA)*YNA(IA))*LA(IA)
           SMBFLS=-FHA(IA)*LA(IA) 
           smbs=haE(ia)*la(ia)
CConc
Cliq            QS2=-ROS*FHCA(IA)*LA(IA)
C condition supprimee car qs2 du meme signe que QOUV
C qouv negatif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq      D2=DIAMA(IA)
Cliq      S2=SIGMAA(IA)
CConcf
         ENDIF 
C cas ou ieg et ied non nuls : on est forcement en central avec un ouvrage
C on prend la vitesse cote 1D
C ce cas ne semble pas exister non plus
       ELSE
         IF(GCOUPLAGE(IA))THEN
            SMBflS=(QOUV(IA,1))    
            SMBflQ=(QOUV(IA,1)*VOUV(IA,1)) 
            smbs=HOUV(IA,1)*la(ia)
CConc
Cliq            QS2=ROS*QOUV(IA,1)*COOUV(IA,1)
C condition supprimee car qs2 du meme signe que QOUV
C qouv negatif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq        D2=DIAMouv(IA,1)
Cliq        S2=SIGMAouv(IA,1)
CConcf
        else
           SMBFLQ=-QOUV(IA,2)*VOUV(IA,2)    
           SMBFLS=-QOUV(IA,2) 
           SMBS=HOUV(IA,2)*LA(IA)
CConc
Cliq            QS2=-ROS*QOUV(IA,2)*COOUV(IA,2)
C condition supprimee car qs2 du meme signe que QOUV
C qouv negatif  si va dans le 1D mais negatif sinon
CliqC      IF(QS2.LT.0.)QS2=0.
Cliq        D2=DIAMouv(IA,2)
Cliq        S2=SIGMAouv(IA,2)
CConcf
         endif 
C fin du if sur ieg et ied nuls         
       ENDIF

       RETURN
       END

C*************************************************************************C
       SUBROUTINE calculSMBQ4
CALCUL DU FLUX DE QUANTITE DE MOUVEMENT POUR LE 1D
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL DU SECOND MEMBRE (QV) POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER LMAX,NBMAX,NACOUMAX
      PARAMETER(LMAX=3000,NBMAX=150,NACOUMAX=50)
      INTEGER LM(0:NBMAX),LL,NBB
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax)
      
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION SMBQ,SMBQ4(LMAX)
      INTEGER JA,J,M,IA

       COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
       COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/smbq4/SMBQ4 
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PHYS/LM,LL
       COMMON/NBIEF/NBB
      
      external SMBQ
      
      DO M=1,NBB
       DO J=LM(m-1)+2,LM(M)-1
        SMBQ4(J)=0.
c      ENDDO  
c      DO I=1,NCOUPLAGE 
c        J=I1D(I)
        IF(NAT(J).GT.0)then
          do ja=1,NARG(j)
            IA=IARG(j,JA)
C le signe - car smbq calcule pour rive droite      
            smbq4(j)=smbq4(j)-smbq(IA)
          enddo
          do ja=1,NARD(j)
            IA=IARD(j,JA)
            smbq4(j)=smbq4(j)+smbq(IA)
          enddo
C en central : pas de quantit� de mouvement transmise          
c          do ja=1,NACE(j)
c            IA=IACE(j,JA)
c            smbq4(j)=smbq4(j)+smbq(IA)
c           enddo
          SMBQ4(J)=SMBQ4(J)/(XTMAIL(J)-XTMAIL(J-1))
C fin du if sur nat
        endif
C fin boucle sur j
       enddo
c fin boucle sur bief
      enddo
      return
      end

C*************************************************************************C
       DOUBLE PRECISION FUNCTION SMBQ(IA)
CALCUL DU FLUX DE QUANTITE DE MOUVEMENT POUR LE 1D
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL DU SECOND MEMBRE (QV) POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER NAMAX,NEVAMX,NEMAX
      PARAMETER (NAMAX=185000,NEVAMX = 2,NEMAX=94000)
      INTEGER IEVA(NAMAX,NEVAMX)
      INTEGER IA,IED,IEG 
      DOUBLE PRECISION FHA(NAMAX),FQUA(NAMAX),FQVA(NAMAX) 
      DOUBLE PRECISION XNA(NAMAX),YNA(NAMAX),XTA(NAMAX),YTA(NAMAX)
      DOUBLE PRECISION SE(NEMAX),LA(NAMAX)
      DOUBLE PRECISION HOUV(0:NAMAX,NEVAMX),QOUV(0:NAMAX,NEVAMX)
      DOUBLE PRECISION VOUV(0:NAMAX,NEVAMX),VTOUV(0:NAMAX,NEVAMX)
      INTEGER NREFA(NAMAX) 

      COMMON/IELA/IEVA
      COMMON/FLUX/FHA,FQUA,FQVA 
      COMMON/VARE/XNA,YNA,XTA,YTA
      COMMON/DOSE/SE,LA   
      COMMON/HQOUV/HOUV,QOUV 
      COMMON/VOUVH/VOUV,VTOUV
      COMMON/NARF/NREFA 

        SMBQ=0.
        IEG=IEVA(IA,1) 
        IED=IEVA(IA,2)


C BERT ON TEST LA MAILLE RIVE GAUCHE / L'ARETE RIVE GAUCHE (IED OU IAG) 


       IF (IED.EQ.0) THEN
C le signe + correspond a arete en meme sens que 1D donc rive droite 1D       
         IF(NREFA(IA).EQ.-2) THEN
c           IF(HG(IA).GT.PARAY)THEN
C on prend la valeur de la vitesse tangentielle C�t� 1D (toujours d�finie)
            SMBQ=+(QOUV(IA,1)*VTOUV(IA,2))    
C           ELSE
C             SMBQ=0.
c           ENDIF   
         ELSEIF(NREFA(IA).EQ.-1) THEN
C           IF(HG(IA).GT.PARAY)THEN
C  ce cas existe t il?
            SMBQ=(QOUV(IA,1)*VTOUV(IA,2))  
     :      +(FQUA(IA)*XTA(IA)+FQVA(IA)*YTA(IA))*LA(IA)    
C           ELSE
C             SMBQ=0.
C           ENDIF   
         ELSE
           SMBQ=(FQUA(IA)*XTA(IA)+FQVA(IA)*YTA(IA))*LA(IA)
         ENDIF 
       ELSEIF(IEG.EQ.0)then
         IF(NREFA(IA).EQ.-2) THEN
c           IF(HD(IA).GT.PARAY)THEN
C on prend la valeur de la vitesse tangentielle C�t� 1D (toujours d�finie)
C le signe - correspond a arete en sens inverse du 1D donc rive droite 1D       
            SMBQ=QOUV(IA,2)*VTOUV(IA,1)    
C           ELSE
C             SMBQ=0.
c           ENDIF   
         ELSEIF(NREFA(IA).EQ.-1) THEN
c           IF(HD(IA).GT.PARAY)THEN
C  ce cas existe t il?
            SMBQ=(QOUV(IA,2)*VTOUV(IA,1))  
     :      +(FQUA(IA)*XTA(IA)+FQVA(IA)*YTA(IA))*LA(IA)    
C           ELSEC
c             SMBQ=0.
c           ENDIF   
         ELSE
           SMBQ=(FQUA(IA)*XTA(IA)+FQVA(IA)*YTA(IA))*LA(IA)
         ENDIF 
C cas ou ieg et ied non nuls : on est forcement en central avec un ouvrage       
c       ELSE
C on ne fait pas transiter de vitesse transversale
c          SMBQ=0.
C fin du if sur ieg et ied nuls         
       ENDIF

       RETURN
       END

C*************************************************************************C
       SUBROUTINE calculSMBS4
CALCUL DU FLUX DE masse (debit lateral) POUR LE 1D
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL DU SECOND MEMBRE (QV) POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER LMAX,NBMAX,NACOUMAX
      PARAMETER(LMAX=3000,NBMAX=150,NACOUMAX=50)
      INTEGER LM(0:NBMAX),LL,NBB
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax)
      
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION SMBS2,SMBS4(LMAX)
      INTEGER JA,J,M,IA

       COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
       COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/smbS4/SMBS4 
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PHYS/LM,LL
       COMMON/NBIEF/NBB
      
      external SMBS2
      
      DO M=1,NBB
       DO J=LM(m-1)+2,LM(M)-1
        SMBS4(J)=0.
c      ENDDO  
c      DO I=1,NCOUPLAGE 
c        J=I1D(I)
        IF(NAT(J).GT.0)then
          do ja=1,NARG(j)
            IA=IARG(j,JA)
            smbs4(j)=smbs4(j)+smbs2(IA)
          enddo
          do ja=1,NARD(j)
            IA=IARD(j,JA)
            smbs4(j)=smbs4(j)+smbs2(IA)
          enddo
          do ja=1,NACE(j)
            IA=IACE(j,JA)
            smbs4(j)=smbs4(j)+smbs2(IA)
           enddo
          SMBs4(J)=SMBs4(J)/(XTMAIL(J)-XTMAIL(J-1))
C fin du if sur nat
        endif
C fin boucle sur j
       enddo
c fin boucle sur bief
      enddo
      return
      end

C*************************************************************************C
       DOUBLE PRECISION FUNCTION SMBS2(IA)
CALCUL DU FLUX DE QUANTITE DE MOUVEMENT POUR LE 1D
C*************************************************************************C

C-----------------------------------------------------
C        CALCUL DU SECOND MEMBRE (QV) POUR TRANSFERT 1D/2D
C-----------------------------------------------------
      INTEGER NAMAX,NEVAMX,NEMAX
      PARAMETER (NAMAX=185000,NEVAMX = 2,NEMAX=94000)
      INTEGER IEVA(NAMAX,NEVAMX)
      INTEGER IA,IED,IEG 
      DOUBLE PRECISION FHA(NAMAX),FQUA(NAMAX),FQVA(NAMAX) 
C      DOUBLE PRECISION XNA(NAMAX),YNA(NAMAX),XTA(NAMAX),YTA(NAMAX)
      DOUBLE PRECISION SE(NEMAX),LA(NAMAX)
      DOUBLE PRECISION HOUV(0:NAMAX,NEVAMX),QOUV(0:NAMAX,NEVAMX)
      INTEGER NREFA(NAMAX) 
      LOGICAL GCOUPLAGE(NAMAX)

      COMMON/IELA/IEVA
      COMMON/FLUX/FHA,FQUA,FQVA 
C      COMMON/VARE/XNA,YNA,XTA,YTA
      COMMON/DOSE/SE,LA   
      COMMON/HQOUV/HOUV,QOUV 
      COMMON/NARF/NREFA 
      COMMON/GCOUPLAGE/GCOUPLAGE
      
        SMBS2=0.
        IEG=IEVA(IA,1) 
        IED=IEVA(IA,2)


C BERT ON TEST LA MAILLE RIVE GAUCHE / L'ARETE RIVE GAUCHE (IED OU IAG) 


       IF (IED.EQ.0) THEN
         IF(NREFA(IA).EQ.-2) THEN
            SMBS2=QOUV(IA,1)    
         ELSEIF(NREFA(IA).EQ.-1) THEN
            SMBS2=QOUV(IA,1)+FHA(IA)*LA(IA)    
         ELSE
           SMBS2=FHA(IA)*LA(IA)
         ENDIF 
       ELSEIF(IEG.EQ.0)then
         IF(NREFA(IA).EQ.-2) THEN
           SMBS2=-QOUV(IA,2)    
         ELSEIF(NREFA(IA).EQ.-1) THEN
           SMBS2=-QOUV(IA,2)-FHA(IA)*LA(IA)    
         ELSE
           SMBS2=-FHA(IA)*LA(IA)
         ENDIF 
C cas ou ieg et ied non nuls : on est forcement en central avec un ouvrage       
       ELSE
         IF(GCOUPLAGE(IA))THEN
           SMBS2=QOUV(IA,1)    
         else
           SMBS2=-QOUV(IA,2)    
         endif 
C fin du if sur ieg et ied nuls         
       ENDIF

       RETURN
       END


C*************************************************************************C
       SUBROUTINE calculSMBQS4
CALCUL DU FLUX DE masse (debit lateral) POUR LE 1D
C*************************************************************************C
      INTEGER LMAX,NBMAX,NACOUMAX
      PARAMETER(LMAX=3000,NBMAX=150,NACOUMAX=50)
      INTEGER LM(0:NBMAX),LL,NBB
      integer iarg(lmax,nacoumax),iard(lmax,nacoumax)
     :,iace(lmax,nacoumax),iaam(lmax,nacoumax),iaav(lmax,nacoumax)
      integer narg(lmax),nard(lmax),nace(lmax),naam(lmax),naav(lmax)
     :,nat(lmax)
      
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION SMBQS2,DS2,SS2
      DOUBLE PRECISION SMBQS4(LMAX),SMBD4(LMAX),SMBET4(LMAX)
      INTEGER JA,J,M,IA
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1

      COMMON/NA1D/NARG,NARD,NACE,NAAM,NAAV,NAT
      COMMON/IA1D/IARG,IARD,IACE,IAAM,IAAV
      COMMON/smbQS4/SMBQS4,SMBD4,SMBET4 
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      
      DO M=1,NBB
       DO J=LM(m-1)+2,LM(M)-1
        SMBQS4(J)=0.
        smbd4(j)=1.
        smbet4(j)=1.
c      ENDDO  
c      DO I=1,NCOUPLAGE 
c        J=I1D(I)
        IF(NAT(J).GT.0)then
          do ja=1,NARG(j)
            IA=IARG(j,JA)
      call SMBQS(SMBQS2,DS2,SS2,IA)
      if(smbqs4(j).lT.0.)THEN
        if(smbqs2.lt.0.)then
	  smbqs4(j)=-smbqs4(j)
	  smbqs2=-smbqs2
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
	  smbqs4(j)=-smbqs4(j)
        else
C smbqs2 positif et on suppose smbqs4 quasi nul
          smbqs4(j)=SMBQS4(j)+smbqs2
          smbd4(j)=ds2
          smbet4(j)=ss2
        endif
      elseif(smbqs2.lt.0.)then
C smbqs2 negatif et on suppose smbqs4 quasi nul
        smbqs4(j)=SMBQS4(j)+smbqs2
        smbd4(j)=ds2
        smbet4(j)=ss2
      else
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
      endif
          enddo
          do ja=1,NARD(j)
            IA=IARD(j,JA)
      call SMBQS(SMBQS2,DS2,SS2,IA)
      if(smbqs4(j).lT.0.)THEN
        if(smbqs2.lt.0.)then
	  smbqs4(j)=-smbqs4(j)
	  smbqs2=-smbqs2
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
	  smbqs4(j)=-smbqs4(j)
        else
C smbqs2 positif et on suppose smbqs4 quasi nul
          smbqs4(j)=SMBQS4(j)+smbqs2
          smbd4(j)=ds2
          smbet4(j)=ss2
        endif
      elseif(smbqs2.lt.0.)then
C smbqs2 negatif et on suppose smbqs4 quasi nul
        smbqs4(j)=SMBQS4(j)+smbqs2
        smbd4(j)=ds2
        smbet4(j)=ss2
      else
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
      endif
          enddo
          do ja=1,NACE(j)
            IA=IACE(j,JA)
      call SMBQS(SMBQS2,DS2,SS2,IA)
      if(smbqs4(j).lT.0.)THEN
        if(smbqs2.lt.0.)then
	  smbqs4(j)=-smbqs4(j)
	  smbqs2=-smbqs2
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
	  smbqs4(j)=-smbqs4(j)
        else
C smbqs2 positif et on suppose smbqs4 quasi nul
          smbqs4(j)=SMBQS4(j)+smbqs2
          smbd4(j)=ds2
          smbet4(j)=ss2
        endif
      elseif(smbqs2.lt.0.)then
C smbqs2 negatif et on suppose smbqs4 quasi nul
        smbqs4(j)=SMBQS4(j)+smbqs2
        smbd4(j)=ds2
        smbet4(j)=ss2
      else
C les deux sont positifs ou nuls      
            call mixage(smbqs4(j),smbd4(j),smbet4(j),smbqs2,ds2,ss2)
      endif
          enddo
C smbqs4 est en m3/s
C     SMBqs4(J)=SMBqs4(J)/(XTMAIL(J)-XTMAIL(J-1))
           smbqs4(j)=SMBQS4(j)*ROS
C dans 1D QSR est en kg/s donc smbqs4 idem
C fin du if sur nat
        endif
C fin boucle sur j
       enddo
c fin boucle sur bief
      enddo
      return
      end

C*************************************************************************C
       SUBROUTINE SMBQS(SMBQS2,DS2,SS2,IA)
CALCUL DU FLUX DE concentration lateral POUR LE 1D
C*************************************************************************C

      INTEGER NAMAX,NEVAMX,NEMAX
      PARAMETER (NAMAX=185000,NEVAMX = 2,NEMAX=94000)
      INTEGER IEVA(NAMAX,NEVAMX)
      INTEGER IA,IED,IEG 
      DOUBLE PRECISION FHA(NAMAX),FQUA(NAMAX),FQVA(NAMAX) 
C      DOUBLE PRECISION XNA(NAMAX),YNA(NAMAX),XTA(NAMAX),YTA(NAMAX)
      DOUBLE PRECISION SE(NEMAX),LA(NAMAX)
      DOUBLE PRECISION HOUV(0:NAMAX,NEVAMX),QOUV(0:NAMAX,NEVAMX)
      DOUBLE PRECISION SMBQS2,DS2,SS2
      INTEGER NREFA(NAMAX) 
      LOGICAL GCOUPLAGE(NAMAX)
      DOUBLE precision DIAMA(NAMAX),SIGMAA(NAMAX)
     :,SIGMAouv(0:naMAX,nevamx),DIAMouv(0:NAMAX,NEVAMX)
      DOUBLE PRECISION FHCA(NAMAX),COOUV(0:NAMAX,NEVAMX)

      COMMON/FLUX1/FHCA
      COMMON/HCOUV/COOUV
      COMMON/DSIGMAouv/DIAMouv,SIGMAouv
      COMMON/DSIGMAA/DIAMA,SIGMAA
      COMMON/IELA/IEVA
      COMMON/FLUX/FHA,FQUA,FQVA 
C      COMMON/VARE/XNA,YNA,XTA,YTA
      COMMON/DOSE/SE,LA   
      COMMON/HQOUV/HOUV,QOUV 
      COMMON/NARF/NREFA 
      COMMON/GCOUPLAGE/GCOUPLAGE
      
        SMBqS2=0.
        IEG=IEVA(IA,1) 
        IED=IEVA(IA,2)


C BERT ON TEST LA MAILLE RIVE GAUCHE / L'ARETE RIVE GAUCHE (IED OU IAG) 


       IF (IED.EQ.0) THEN
         IF(NREFA(IA).EQ.-2) THEN
C c�t� 2D on a toujours la bonne concentration car chang�e si du 1D vers 2D         
            SMBQS2=QOUV(IA,1)*COOUV(IA,1)    
      DS2=DIAMOUV(IA,1)
      SS2=SIGMAOUV(IA,1)
         ELSEIF(NREFA(IA).EQ.-1) THEN
            SMBQS2=QOUV(IA,1)*COOUV(IA,1)    
      DS2=DIAMOUV(IA,1)
      SS2=SIGMAOUV(IA,1)
      if(fhca(ia).lT.0.)THEN
        smbqs2=FHCA(IA)*LA(IA)+smbqs2
      elseif(smbqs2.lt.0.)then
        smbqs2=FHCA(IA)*LA(IA)+smbqs2
              DS2=DIAMA(IA)
              SS2=SIGMAA(IA)
      else
C les deux sont positifs ou nuls      
      CALL MIXAGE(SMBQS2,DS2,SS2,FHCA(IA)*LA(IA)
     :,DIAMA(IA),SIGMAA(IA))
      endif
         ELSE
           SMBQS2=FHCA(IA)*LA(IA)
      DS2=DIAMA(IA)
      SS2=SIGMAA(IA)
         ENDIF 
       ELSEIF(IEG.EQ.0)then
         IF(NREFA(IA).EQ.-2) THEN
C c�t� 2D on a toujours la bonne concentration car chang�e si du 1D vers 2D         
           SMBQS2=-QOUV(IA,2)*COOUV(IA,2)    
      DS2=DIAMOUV(IA,2)
      SS2=SIGMAOUV(IA,2)
         ELSEIF(NREFA(IA).EQ.-1) THEN
C           if(qouv(ia,2).GT.0.)then
           SMBQS2=-QOUV(IA,2)*COOUV(IA,2)    
      DS2=DIAMOUV(IA,2)
      SS2=SIGMAOUV(IA,2)
c         else
c           SMBQS2=-QOUV(IA,2)*COOUV(IA,2)    
c      DS2=DIAMOUV(IA,2)
c      SS2=SIGMAOUV(IA,2)
c         endif
      if(fhca(ia).gT.0.)THEN
        smbqs2=-FHCA(IA)*LA(IA)+smbqs2
      elseif(smbqs2.lt.0.)then
        smbqs2=-FHCA(IA)*LA(IA)+smbqs2
              DS2=DIAMA(IA)
              SS2=SIGMAA(IA)
      else
C les deux sont positifs ou nuls      
      CALL MIXAGE(SMBQS2,DS2,SS2,-FHCA(IA)*LA(IA)
     :,DIAMA(IA),SIGMAA(IA))
      endif
         ELSE
           SMBQS2=-FHCA(IA)*LA(IA)
      DS2=DIAMA(IA)
      SS2=SIGMAA(IA)
         ENDIF 
C cas ou ieg et ied non nuls : on est forcement en central avec un ouvrage       
       ELSE
         IF(GCOUPLAGE(IA))THEN
           SMBQS2=QOUV(IA,1)*COOUV(IA,1)    
      DS2=DIAMOUV(IA,1)
      SS2=SIGMAOUV(IA,1)
         else
           SMBQS2=-QOUV(IA,2)*COOUV(IA,2)    
      DS2=DIAMOUV(IA,2)
      SS2=SIGMAOUV(IA,2)
         endif 
C fin du if sur ieg et ied nuls         
       ENDIF

       RETURN
       END


C**********************************************************************
      SUBROUTINE TRIMXYG(IM,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES D'UNE maille SONT BONNES
C     ENTREE: X, Y De la maille
C     SORTIE: NUMERO DE La maille
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C

      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NBB,IM,M,J,IDEC
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
       DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE
      INTEGER LM(0:NBMAX),LL
       DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,y1
      integer xnc(0:LMAX),NC(0:lmax)
 
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
       common/XYPLANI/XPLANI,YPLANI
      
C la distance limite maximale est 60 m pour presque toujours trouver
      DO IDEC=1,-3,-1
        DIST=0.06*10.**(-IDEC)
        DO M=1,NBB
         DO J=Lm(m-1)+2,LM(M)-1
c        IF(TYPCOU(I).EQ.'G')then
          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
c        elseIF(TYPCOU(I).EQ.'D')then
c          x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'C')then
c          x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
c          +xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
c          +yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'M')then
c          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'V')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
cC fin du if sur typcou        
c        endif
          IF(ABS(X-X1).LT.DIST)THEN
           IF(ABS(Y-Y1).LT.DIST)THEN
             TROUVE=.TRUE.
             IM=J
             X=X1
             Y=Y1
             RETURN
           ENDIF
          ENDIF
         ENDDO
        ENDDO
      ENDDO
      RETURN
      END
C**********************************************************************
      SUBROUTINE TRIMXYD (IM,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES D'UNE maille SONT BONNES
C     ENTREE: X, Y De la maille
C     SORTIE: NUMERO DE La maille
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C

      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NBB,IM,M,J,IDEC
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
       DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE
      INTEGER LM(0:NBMAX),LL
       DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,y1
      integer xnc(0:LMAX),NC(0:lmax)
 
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
       common/XYPLANI/XPLANI,YPLANI
      
C la distance limite maximale est 60 m pour presque toujours trouver
      DO IDEC=1,-3,-1
        DIST=0.06*10.**(-IDEC)
        DO M=1,NBB
         DO J=Lm(m-1)+2,LM(M)-1
c        IF(TYPCOU(I).EQ.'G')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
c        elseIF(TYPCOU(I).EQ.'D')then
          x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
          Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'C')then
c          x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
c          +xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
c          +yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'M')then
c          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'V')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
cC fin du if sur typcou        
c        endif
          IF(ABS(X-X1).LT.DIST)THEN
           IF(ABS(Y-Y1).LT.DIST)THEN
             TROUVE=.TRUE.
             IM=J
         X=X1
             Y=Y1
             RETURN
           ENDIF
          ENDIF
         ENDDO
        ENDDO
      ENDDO
      RETURN
      END
C**********************************************************************
      SUBROUTINE TRIMXYC (IM,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES D'UNE maille SONT BONNES
C     ENTREE: X, Y De la maille
C     SORTIE: NUMERO DE La maille
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C

      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NBB,IM,M,J,IDEC
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
       DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE
      INTEGER LM(0:NBMAX),LL
       DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,y1
      integer xnc(0:LMAX),NC(0:lmax)
 
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
       common/XYPLANI/XPLANI,YPLANI
      
C la distance limite maximale est 60 m pour presque toujours trouver
      DO IDEC=1,-3,-1
        DIST=0.06*10.**(-IDEC)
        DO M=1,NBB
         DO J=Lm(m-1)+2,LM(M)-1
c        IF(TYPCOU(I).EQ.'G')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
c        elseIF(TYPCOU(I).EQ.'D')then
c          x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'C')then
          x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
     :+xplani(xnc(j-1))+xplani(xnc(j)))
          Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
     :+yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'M')then
c          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'V')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
C fin du if sur typcou        
c        endif
          IF(ABS(X-X1).LT.DIST)THEN
           IF(ABS(Y-Y1).LT.DIST)THEN
             TROUVE=.TRUE.
             IM=J
             X=X1
             Y=Y1
             RETURN
           ENDIF
          ENDIF
         ENDDO
        ENDDO
      ENDDO
      RETURN
      END
C**********************************************************************
      SUBROUTINE TRIMXYM (IM,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES D'UNE maille SONT BONNES
C     ENTREE: X, Y De la maille
C     SORTIE: NUMERO DE La maille
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C

      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NBB,IM,M,J,IDEC
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
       DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE
      INTEGER LM(0:NBMAX),LL
       DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,y1
      integer xnc(0:LMAX),NC(0:lmax)
 
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
       common/XYPLANI/XPLANI,YPLANI
      
C la distance limite maximale est 60 m pour presque toujours trouver
      DO IDEC=1,-3,-1
        DIST=0.06*10.**(-IDEC)
        DO M=1,NBB
         J=Lm(m-1)+1
c        IF(TYPCOU(I).EQ.'G')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
c        elseIF(TYPCOU(I).EQ.'D')then
c          x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'C')then
c          x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
c          +xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
c          +yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'M')then
          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'V')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
cC fin du if sur typcou        
c        endif
          IF(ABS(X-X1).LT.DIST)THEN
           IF(ABS(Y-Y1).LT.DIST)THEN
             TROUVE=.TRUE.
             IM=J
             X=X1
             Y=Y1
             RETURN
           ENDIF
          ENDIF
c         ENDDO
        ENDDO
      ENDDO
      RETURN
      END
C**********************************************************************
      SUBROUTINE TRIMXYV (IM,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES D'UNE maille SONT BONNES
C     ENTREE: X, Y De la maille
C     SORTIE: NUMERO DE La maille
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C

      INTEGER LMAX,LNCMAX,NCMAX,NBMAX,NBB,IM,M,J,IDEC
      PARAMETER(LMAX=3000,LNCMAX=103000,NCMAX=300,NBMAX=150)
       DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE
      INTEGER LM(0:NBMAX),LL
       DOUBLE PRECISION XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,x1,y1
      integer xnc(0:LMAX),NC(0:lmax)
 
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NBIEF/NBB
       common/XYPLANI/XPLANI,YPLANI
      
C la distance limite maximale est 60 m pour presque toujours trouver
      DO IDEC=1,-3,-1
        DIST=0.06*10.**(-IDEC)
        DO M=1,NBB
         J=LM(M)
c        IF(TYPCOU(I).EQ.'G')then
c          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1))
c          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1))
c        elseIF(TYPCOU(I).EQ.'D')then
c          x1=0.5*(xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'C')then
c          x1=0.25*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)+1)
c          +xplani(xnc(j-1))+xplani(xnc(j)))
c          Y1=0.25*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)+1)
c          +yplani(xnc(j-1))+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'M')then
c          x1=0.5*(xplani(xnc(j-1)+1)+xplani(xnc(j)))
c          Y1=0.5*(yplani(xnc(j-1)+1)+yplani(xnc(j)))
c        elseIF(TYPCOU(I).EQ.'V')then
          x1=0.5*(xplani(xnc(j-2)+1)+xplani(xnc(j-1)))
          Y1=0.5*(yplani(xnc(j-2)+1)+yplani(xnc(j-1)))
C fin du if sur typcou        
c        endif
          IF(ABS(X-X1).LT.DIST)THEN
           IF(ABS(Y-Y1).LT.DIST)THEN
             TROUVE=.TRUE.
             IM=J
             X=X1
             Y=Y1
             RETURN
           ENDIF
          ENDIF
c         ENDDO
        ENDDO
      ENDDO
      RETURN
      END
C**********************************************************************
      SUBROUTINE TRIAXY (IA,X,Y,TROUVE)
C---------------------------------------------------------------------C
C     VERIFIE QUE LES COORDONNEES DES ARETES D'UN OUVRAGE SONT BONNES
C     ENTREE: X, Y DU MILIEU DE L'ARETE
C     SORTIE: NUMERO DE L'ARETE
C     LA VARIABLE LOGIQUE TROUVE INDIQUE SI L'ARETE EST TROUVEE
C=====================================================================C
      INTEGER NAMAX

      PARAMETER (NAMAX  = 185000)

      INTEGER NE,NA,NN
      DOUBLE PRECISION XA(NAMAX),YA(NAMAX)
      INTEGER IA,IDEC
      DOUBLE PRECISION X,Y,DIST
      LOGICAL TROUVE

      COMMON/NOMB/NE,NA,NN
      COMMON/COOA/XA,YA
C la distance limite maximale est tres grande pour toujours trouver
      DO IDEC=3,-10,-1
         DIST=0.06*10.**(-IDEC)
         DO 1 IA=1,NA
         IF(ABS(X-XA(IA)).LT.DIST)THEN
           IF(ABS(Y-YA(IA)).LT.DIST)THEN
             TROUVE=.TRUE.
             X=XA(IA)
             Y=YA(IA)
             RETURN
           ENDIF
         ENDIF
   1     CONTINUE
      ENDDO
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE SEPARATION(M,D,S,M1,D1,S1,M2,D2,S2)
C-----------------------------------------------------------------------
C D�mixe les s�diments des compartiments 1 et 2 
C quand le diametre du compartiment 1 est connu
C on rentre avec les 3 masses, 
C et les caractristiques du tout et du compartiment 1(d1<d)
C et les autres carcteristiques identisques et egales a celles du tout
C on sort les carcteristiques du compartiment 2 (d2>d)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER DEMIX
      DOUBLE PRECISION M1,D1,S1,M2,D2,S2,s20 
      DOUBLE PRECISION DX,a,a2,b,b2
C compartiment d'origine
      DOUBLE PRECISION M,D,S
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION DCHARD,DCHARS
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/DCHARSED/DCHARD,DCHARS
      COMMON/DEMIX/DEMIX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
c       write(*,*)'demixage1',m,d,s,dx

C Contr�le des entr�es
C-----------------------------------------------------------------------
       IF(M.GT.EPSM)THEN
        IF(S.LT.1.)then
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur S lors de l''appel a SEPARATION'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
          S=1.000000
        endif  
        IF(D.LT.EPS)then
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur D lors de l''appel a SEPARATION'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
          D=0.000001
        endif  
        elseIF(M.LT.-EPSM)THEN
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur M lors de l''appel a SEPARATION'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
c        PAUSE
        STOP
C cas ou masse proche de 0
        ELSE
        IF(S.LT.1.)then
          S=1.000000
        endif  
        IF(D.LT.EPSY)then
          D=EPSY
        endif  
        IF(M.lT.0.)THEN
          M=0.
          M1=0.
          M2=0.
          D2=D
          S2=S 
          D1=D
          S1=S
          RETURN
	  ENDIF
C fin if sur M
        ENDIF
      IF(M1.LT.0.)THEN
        IF(M1.LT.-EPSM)THEN
        WRITE(*,*)'PROBLEME sur M1 lors de l''appel a SEPARATION'
     &    ,M,D,S,' masse 1 ',M1,' masse 2 ',M2
        WRITE(*,*)' '
c        PAUSE
        STOP
        ELSE
          M1=0.
          M2=M
          D2=D
          S2=S 
          RETURN
        ENDIF
C fin du if sur m1
              ENDIF
      IF(M2.LT.0.)THEN
        IF(M2.LT.-EPSM)THEN
        WRITE(*,*)'PROBLEME sur M2 lors de l''appel a SEPARATION'
     &    ,M,D,S,' masse 1 ',M1,' masse 2 ',M2
        WRITE(*,*)' '
c        PAUSE
        STOP
        ELSE
          M2=0.
          D2=D
          S2=S 
          RETURN
        ENDIF
C fin du if sur m2
              ENDIF
C DEMIX=0, diam�tre identique pour les deux compartiments 1 et 2
      IF(DEMIX.EQ.0)THEN
        D2=D
        S2=S
      ELSEIF(DEMIX.EQ.1)THEN      
c cas ou la masse du compartiment M2 est n�gligeable devant la masse de M1
        IF(M2.LE.EPSY*M1) THEN
c on suppose que le compartiment 2 garde les caract�ristiques du compartiment d'origine
c c'est uniquement pour des raisons num�riques, la masse M2 est nulle 
          D2=D   
          S2=S  
c cas ou la masse du compartiment M1 est n�gligeable devant la masse de M2
        ELSEIF(M1.LE.EPSY*M2) THEN
          D2=D
          S2=S 
c cas ou le materiau est deja uniforme
        ELSEIF(S.LE.1.+EPSY) THEN
          D2=D
          S2=S 
C on traite le cas ou les deux masses ne sont pas nulles
        ELSE
          d2=d+M1/M2*(d-d1)
          S2=S+M1/M2*(s-s1)
          if(d2.gt.s*d)then
            d2=d*s
            s2=1.
          elseif(s2.lt.1.)then
             s2=1.   
             if(d2*s2.gt.s*d)then
              s2=d*s/d2
             endif 
           endif 
C fin du if sur masses trop petites
      endif  
C fin du if sur demix=0
      endif  
      RETURN
      END

