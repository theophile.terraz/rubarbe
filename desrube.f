CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C       PROGRAMME DE TRACE DES RESULTATS DE RUBARBE               C
C                                                                 C
C       LECTURE DES FICHIERS                                      C
C              ENVLOP.ETUDE                                       C
C              TRAJEC.ETUDE                                       C
C              HYDLIM.ETUDE                                       C
C              PROFIL.ETUDE                                       C
C et ecriture de fichiers colonnes pour dessin par excel ou autre C
C        LE 07/01/98   
C eclairci le 31 juillet 2007   
C lecture geometrie par geomac-i le 15 octobre 2007               C
C ajout variables sedimentaires et plusieurs biefs le 8 juin 2009
C correction format fichier geomac-i le 30/11/2015 et nnbb de 20 a 150
C le 2/12/2015 dim de 20000 a 10000 et5 mis a ncourb(soit 10)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC


        INTEGER DIM,NMHYD,I,ICHX,ICHY,IH,IMAX,J,LM,II,IB
        INTEGER N,NBSSAV,NBPROF,NM,NUMHYD,NUMLIM,NUMVIT
        INTEGER NUMPRH,NUMPRQ,NUMPRV,IDPARS,NC,IOECH
        INTEGER REP,INDI,NUMPRC,J1
        INTEGER NUMHY2,NUMLI2,NUMVI2
        INTEGER NUMPR2,IOSOR,JH,NBF,NBP(5),K,KP,CHOIX
        INTEGER NCOURB,NUMCOU,NBPRO,IREP,NBMAX

C DIMENSION MAXIMALE DES TABLEAUX : DIM-2
C NOMBRE MAXIMAL DE COURBES DANS HYDLIM ET DANS PROFIL : 100 
C NOMBRE MAXIMAL DE COURBES TRACEES : 10 (SEULES 5 PREVUES)

        PARAMETER (DIM=10000,NMHYD=100,NCOURB=10,NBMAX=150)

        DOUBLE PRECISION A(DIM),B(DIM),B2(DIM,4)
        DOUBLE PRECISION CFX,CFN,TFN1,TFX3
C         DOUBLE PRECISION XMIN,XMAX,YMIN,YMAX,CFX,CFN,TFN1,TFX3
C        DOUBLE PRECISION XMIN,XMAX,YMIN,YMAX,STEPX,STEPY,YMIN2
        DOUBLE PRECISION CTDF(DIM),FDEB1,FDEB2,FDEB3,CTDF2(DIM)
        DOUBLE PRECISION POUB1,POUB2,POUB3,POUB4,POUB5
        DOUBLE PRECISION MAILLE(NMHYD),TSOR(NMHYD)
C 5 FICHIERS HYDLIM DIFFERENTS PEUVENT ETRE LUS
      DOUBLE PRECISION Q0(ncourb*NMHYD+1,DIM),Q00(DIM)
     :,Q0N(ncourb*NMHYD+1),Q0X(ncourb*NMHYD+1)
        DOUBLE PRECISION V0(ncourb*NMHYD+1,DIM),V00(DIM) 
     :,V0N(ncourb*NMHYD+1),V0X(ncourb*NMHYD+1)
        DOUBLE PRECISION H0(ncourb*NMHYD+1,DIM),H00(DIM)
     :,H0N(ncourb*NMHYD+1),H0X(ncourb*NMHYD+1)
        DOUBLE PRECISION QM(DIM),QMN,QMX
        DOUBLE PRECISION VM(DIM),VMN,VMX
        DOUBLE PRECISION YM(DIM),YMN,YMX,YM1
C  fichiers profils peuvent etre lus
        DOUBLE PRECISION QNP(ncourb*NMHYD,DIM),QNP0(DIM)
     :,QNX(ncourb*NMHYD),QNN(ncourb*NMHYD)
        DOUBLE PRECISION VNP(ncourb*NMHYD,DIM),VNP0(DIM)
     :,VNX(ncourb*NMHYD),VNN(ncourb*NMHYD)
        DOUBLE PRECISION YNP(ncourb*NMHYD,DIM),YNP0(DIM)
     :,YNX(ncourb*NMHYD),YNN(ncourb*NMHYD)
        DOUBLE PRECISION ZNP(ncourb*NMHYD,DIM),ZNP0(DIM)
     :,ZNX(ncourb*NMHYD),ZNN(ncourb*NMHYD)

        DOUBLE PRECISION T(DIM),TFDEB1(DIM),TFDEB2(DIM),TFDEB3(DIM)
     :,TFN,TFX
        DOUBLE PRECISION TQ(DIM),TQN,TQX,TV(DIM),TVN,TVX,TY(DIM)
     :,TYN,TYX,TX
C nouvelles variables
        DOUBLE PRECISION TZM(DIM),TZFMAX(DIM),TZFMIN(DIM),ZFMAX(DIM)
     :,ZFMIN(DIM)
        DOUBLE PRECISION TZMX,TZFMAXX,TZFMINX,ZFMAXX,ZFMINX
        DOUBLE PRECISION TZMN,TZFMAXN,TZFMINN,ZFMAXN,ZFMINN
        DOUBLE PRECISION z0(ncourb*NMHYD+1,DIM),z00(DIM)
     :,z0N(ncourb*NMHYD+1),z0X(ncourb*NMHYD+1)
        DOUBLE PRECISION d0(ncourb*NMHYD+1,DIM),d00(DIM)
     :,d0N(ncourb*NMHYD+1),d0X(ncourb*NMHYD+1)
        DOUBLE PRECISION s0(ncourb*NMHYD+1,DIM),s00(DIM)
     :,s0N(ncourb*NMHYD+1),s0X(ncourb*NMHYD+1)
        DOUBLE PRECISION QsP(ncourb*NMHYD,DIM),qsP0(DIM)
     :,QsX(ncourb*NMHYD),QsN(ncourb*NMHYD)
        DOUBLE PRECISION dmP(ncourb*NMHYD,DIM),dmP0(DIM)
     :,dmX(ncourb*NMHYD),dmN(ncourb*NMHYD)
        DOUBLE PRECISION smP(ncourb*NMHYD,DIM),smP0(DIM)
     :,smX(ncourb*NMHYD),smN(ncourb*NMHYD)

        DOUBLE PRECISION X(DIM),XN,XX,TN,TNX(ncourb*NMHYD),X2(DIM)
        DOUBLE PRECISION ZM(DIM),ZMN,ZMX,ZM0(DIM)
        DOUBLE PRECISION XPO(ncourb,DIM),VAL(ncourb,DIM)
        CHARACTER ETUDE*20,NOMFIC*38,ETUD2*20,AREP*1,PARLEG*9
        CHARACTER CHAINE*30,CPOUB(3)*10,TIT*1,XCCOU*1,LIGNE*630
        LOGICAL TESTCH
	DOUBLE PRECISION XMIN,XMAX,YMIN,YMAX
	INTEGER NBPCOU(NCOURB)
        DOUBLE PRECISION XINIT(NCOURB,DIM),YINIT(NCOURB,DIM)
C Itmep vaut 1 si temps exprime en secondes,2 si minutes , 3 si heures
      INTEGER ITEMP,IBB,NBB,NSM,m,ILM(0:NBMAX)
	
	CHARACTER*15 TITREX,TITREY(ncourb),NOMFICH*27,NOMFIC2*27
C variables de presence de fichiers hydlims,envlops, profils
	LOGICAL PHYDLIMS,PENVLOPS,PPROFILS
	
	COMMON /AXE_X/ TitreX
	COMMON /AXE_Y/ TitreY
	COMMON/VALEUR/XINIT,YINIT
        COMMON/NBVALE/NBPCOU
        COMMON/NOMETU/ETUDE
        COMMON/NSECT/NM,NBPROF
        COMMON/SECT/X,TNX,ZNP 
        
        PHYDLIMS=.TRUE.
        PPROFILS=.TRUE.
        PENVLOPS=.TRUE.
        
C        NBCASTOR=0
        NBF=0

C-- RECUPERATION DES PARAMETRES DANS LES FICHIERS :
C==================================================

C100     WRITE(*,101)
C101     FORMAT(1H1)
        WRITE(*,*)' Programme d''extraction  des resultats de RUBARBE '
		write(*,*)'        version du 18 octobre 2021 '
		write(*,*)'            Irstea, HHLY, France        '
		write(*,*)
c 100    WRITE(*,*)'          ENTREZ LE NOM DE L''ETUDE : '
        WRITE(*,*)'          ENTREZ LE NOM DE L''ETUDE : '
        READ(*,702) ETUDE
 702    FORMAT(A20)
        ILM(0)=0
        NBB=1

      OPEN(70,FILE = 'confl.'//ETUDE,STATUS='OLD',ERR=111)
      READ(70,*,ERR=111)NBB
      CLOSE(70)

 111   NOMFIC='mail.'//ETUDE
        OPEN(65,FILE=NOMFIC,STATUS='UNKNOWN')
        DO 1000 M=1,NBB
        READ(65,*) ILM(M)
         ILM(M)=ILM(M-1)+ILM(M)
        READ(65,*) (X(I),I=ILM(M-1)+1,ILM(M))
        READ(65,*) (X2(I),I=ILM(M-1)+1,ILM(M)-1)
		X2(ILM(m))=X2(ILM(m)-1)
 1000   CONTINUE 
        CLOSE(65)
        NM=ILM(NBB)
C      GO TO 112

C    NM = NOMBRE DE MAILLES
C NSM nombre de sections
C Ouverture du fichier de g�om�trie abscisse-cote aux intermailles
C geomac-i 
        NOMFIC='geomac-i.'//ETUDE
        OPEN(24,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=98)
        GO TO 980
 98     WRITE(*,*)'FICHIER geomac-i INEXISTANT'   
        Stop 
 980  DO M=1,NBB  
        read(24,*)nsm
        IF(NSM.NE.ILM(M)-1-ILM(M-1))THEN
            write(*,*)'erreur en tete bief geomac-i ou fichier mail'
            write(*,*)'nombre sections bief = ',m
            Stop
          ENDIF

      DO I=ILM(M-1)+1,ILM(M)-1
        READ(24,*) II,X2(I),NC
        CTDF2(I)=9999.99
        DO J=1,NC
C Lecture des couples abscisse-cote
          READ(24,'(A630)') LIGNE
          READ(LIGNE,9994) XCCOU,POUB1,POUB2
          IF(POUB2.LT.CTDF2(I))THEN
            CTDF2(I)=POUB2
          ENDIF
        ENDDO 
C        write(*,*)i,ctdf2(I),x2(I)
C enddo sur I numero section
      ENDDO
C enddo sur M numero bief
      ENDDO
      CLOSE(24)

 9994 FORMAT(A1,1X,F11.5,F13.5)

       DO iB=1,NBB 
       CTDF(ILM(IB-1)+1)=CTDF2(ILM(IB-1)+1-IB+1)
       X(ILM(IB-1)+1)=X2(ILM(IB-1)+1-IB+1)
       CTDF(ILM(IB))=CTDF2(ILM(IB)-1-IB+1)
       X(ILM(IB))=X2(ILM(IB)-1-IB+1)
       DO 2123 I=ILM(IB-1)+2,ILM(IB)-1
          CTDF(I)=0.5*(CTDF2(I-IB+1-1)+CTDF2(I-IB+1))
          X(I)=0.5*(X2(I-1-IB+1)+X2(I-IB+1))
 2123  CONTINUE
       ENDDO

C -- MENU
 2300 WRITE(*,*)'  '
      WRITE(*,*) '---------------------------------------------------'
      WRITE(*,*) 'CHOIX 1 : LECTURE D''UN FICHIER ENVLOP (ET TRAJEC)'
      WRITE(*,*) 'CHOIX 2 : LECTURE D''UN FICHIER HYDLIM'
      WRITE(*,*) 'CHOIX 3 : LECTURE DE HYDLIM D''UNE AUTRE ETUDE'
      WRITE(*,*) 'CHOIX 4 : LECTURE D''UN FICHIER PROFIL'
      WRITE(*,*) 'CHOIX 5 : SUITE DU PROGRAMME  '
      WRITE(*,*) 'CHOIX 0 : FIN DU PROGRAMME '
      WRITE(*,*) '---------------------------------------------------'
      WRITE(*,*) '  '
      WRITE(*,*) 'DONNEZ VOTRE CHOIX'
      READ (*,*) CHOIX
      IF(CHOIX.EQ.0)choix=6
C -- TEST DU CHOIX
      TESTCH=.FALSE.
      DO 2040 I=1,6
          TESTCH=TESTCH.OR.(CHOIX.EQ.I)
 2040 CONTINUE
      IF (.NOT.TESTCH) THEN
          WRITE(*,*) 'VOUS AVEZ DONNE UN MAUVAIS CHOIX'
          WRITE(*,*) 'RECOMMENCEZ'
          GOTO 2300
      ENDIF

      GOTO(2301,2302,2303,2304,2305,2306),CHOIX

C---- LECTURE DE ENVLOP.ETUDE
 2301    WRITE(*,*)'          VOULEZ-VOUS LES VALEURS CTPB ?'
          WRITE(*,*)'        OUI ------> 1    NON --------> 0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  iosor=1
		  else
			  iosor=0
		  endif

       DO 2500 I=1,NM
           ZM(I)=0.
           YM(I)=0.
           QM(I)=0.
           VM(I)=0.
           TY(I)=0.
           TV(I)=0.
           TQ(I)=0.
 2500  CONTINUE
          NOMFIC='envlop.'//ETUDE
          OPEN(10,FILE=NOMFIC,STATUS='OLD',ERR=7110)
          NOMFIC='envlops.'//ETUDE
          OPEN(20,FILE=NOMFIC,STATUS='OLD',ERR=17110)
          GO TO 110 
 7110      WRITE(*,*)'PAS DE FICHIER envlop '  
           go to 2300
17110     PENVLOPS=.FALSE.
          WRITE(*,*)'PAS DE FICHIER envlops '  
  110      READ(10,'(A1)')TIT
          IF(PENVLOPS)THEN 
          READ(20,'(A1)')TIT
          ENDIF
          
         IF(IOSOR.EQ.1)THEN
          DO 1003 I=1,NM
            READ(10,*) J,TY(I),YM1,TV(I),VM(I),TQ(I),QM(I)
            TY(I)=TY(I)*.87
            TV(I)=TV(I)*.87
            TQ(I)=TQ(I)*.87
            YM(I)=MAX(YM1*1.15,YM1+1.)
            ZM(I)=YM(I)+CTDF(I)
1003      CONTINUE
          ELSE
          DO 703 I=1,NM
            READ(10,*) J,TY(I),YM(I),TV(I),VM(I),TQ(I),QM(I)
            ZM(I)=YM(I)+CTDF(I)
 703      CONTINUE
             if(penvlops)then
		 DO M=1,NBB		 
           DO I=ILM(m-1)+1,ILM(M)-1
C           write(*,*)'nm=',nm
       READ(20,*) J,Tzm(I),zm0(i),Tzfmax(I),zfmax(I),Tzfmin(I),zfmin(I)
            ENDDO
			ZM0(ilm(m))=ZM0(ILM(m)-1)
C on transforme zm au centre de maille
            ZM(ILM(M))=ZM0(ILM(m)-1)
            ZM(ILM(M-1)+1)=ZM0(ILM(m-1)+1)
			DO i=ILM(m-1)+2,ILM(m)-1
			  ZM(I)=0.5*(zm0(i-1)+Zm0(i))
			ENDDO
         ENDDO  
            ENDIF     
C fin du if sur ctpb
          ENDIF
          CLOSE(10)
          IF(PENVLOPS)THEN 
          CLOSE(20)
          ENDIF 

C---- LECTURE DE TRAJEC.ETUDE

        NOMFIC='trajec.'//ETUDE
        OPEN(2,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(2,'(3(A7,F13.7,5X))',END=2400)CPOUB(1),FDEB1,CPOUB(2)
     +,FDEB2,CPOUB(3),FDEB3
        READ(2,'(A1)')TIT
        IF(IOSOR.EQ.1)THEN
        DO 1044 I=1,NM
        READ(2,*)J,TFDEB1(I),TFDEB2(I),TFDEB3(I)
        TFDEB1(I)=TFDEB1(I)*.87
        TFDEB2(I)=TFDEB2(I)*.87
        TFDEB3(I)=TFDEB3(I)*.87
 1044    CONTINUE
        ELSE
        DO 44 I=1,NM
        READ(2,*)J,TFDEB1(I),TFDEB2(I),TFDEB3(I)
 44     CONTINUE
        ENDIF
        CLOSE(2)
        GO TO 2300
C        ELSE
 2400   DO 144 I=1,NM
           TFDEB1(I)=0.
           TFDEB2(I)=0.
           TFDEB3(I)=0.
 144     CONTINUE
C        ENDIF
        GO TO 2300

C---- LECTURE DE HYDLIM.ETUDE
 2302   NOMFIC='hydlim.'//ETUDE
        OPEN(3,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=3)
        NOMFIC='hydlims.'//ETUDE
        OPEN(13,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=10003)
        GO TO 39
  3     WRITE(*,*)'PAS DE FICHIER hydlim'
C        Pause
        Stop 
10003   WRITE(*,*)'PAS DE FICHIER hydlims'
        Phydlims=.false.
  39    NOMFIC='abshyd.'//ETUDE
        OPEN(76,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=76)
        GO TO 760
 76     WRITE(*,*)'PAS DE FICHIER abshyd'  
 760    READ(76,*)NBSSAV
        IF (NBSSAV.NE.0) THEN
          DO 67 I=1,NBSSAV
             READ(76,*)J,MAILLE(I)
67        CONTINUE
        ENDIF
        I=0
5       CONTINUE
        I=I+1
        READ(3,*,END=55) T(I),(Q0(N,I),N=1,NBSSAV),
     :  (V0(N,I),N=1,NBSSAV),(H0(N,I),N=1,NBSSAV)
          IF(PHYDLIMS)THEN 
        READ(13,*,END=55) T(I),(Z0(N,I),N=1,NBSSAV),
     :  (D0(N,I),N=1,NBSSAV),(S0(N,I),N=1,NBSSAV)
          ENDIF 
        GOTO 5
55      IH=I-1
c        write(*,*)'IH=', ih 
        CLOSE(76)
        CLOSE(3)
          IF(PHYDLIMS)THEN 
        CLOSE(13)
        ENDIF
        CHAINE='       NOMBRE D''HYDROGRAMMES :'
        WRITE(*,99)CHAINE,NBSSAV
        JH=1
        GO TO 2300
 2303   WRITE(*,*)'ATTENTION : ON SUPPOSE MEME FICHIER ABSHYD '
        WRITE(*,*)'          ENTREZ LE NOM DE L''AUTRE ETUDE : '
        READ(*,702) ETUD2
        NOMFIC='hydlim.'//ETUD2
        OPEN(3,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=13)
        NOMFIC='hydlims.'//ETUD2
        OPEN(13,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=10013)
        GO TO 130 
 13     WRITE(*,*)'PAS DE FICHIER hydlim' 
10013   WRITE(*,*)'PAS DE FICHIER hydlims'
 130        I=0
 705    CONTINUE
        I=I+1
        READ(3,*,END=755) T(I),(Q0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV),
     :  (V0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV),
     :  (H0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV)
        READ(13,*,END=755) T(I),(Z0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV),
     :  (D0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV)
     :,(S0(N,I),N=JH*NBSSAV+1,(JH+1)*NBSSAV)
        GO TO 705
 755    CLOSE(3)
        close(13)
        JH=JH+1
        GO TO 2300

C---- LECTURE DANS PROFIL.ETUDE

 2304   NOMFIC='tnprof.'//ETUDE
        OPEN(77,FILE=NOMFIC,STATUS='OLD',ERR=77)
        GO TO 770
 77     WRITE(*,*)'PAS DE FICHIER tnprof'  
        GO TO 1437
 770    READ(77,*) NBPRO
        IF (NBPROF.NE.0) THEN
          DO 66 I=1,NBPRO
             READ(77,*)J,TSOR(I)
66        CONTINUE
        ENDIF
        CLOSE(77)

 1437   NBPROF=0
        NOMFIC='profil.'//ETUDE
        NOMFIC2='profils.'//ETUDE
 437    OPEN(42,FILE=NOMFIC,STATUS='OLD',ERR=42)
        OPEN(43,FILE=NOMFIC2,STATUS='OLD',ERR=43)
        GO TO 420
 42     WRITE(*,*)'PAS DE FICHIER profil' 
C        PAUSE
        GO TO 2300
 43     WRITE(*,*)'PAS DE FICHIER profils' 
        PPROFILS=.FALSE.
 420    DO 413 J=NBPROF+1,ncourb*NMHYD
          READ (42,*,END=149)TNX(J)
          IF(PPROFILS)THEN 
          READ (43,*,END=149)TNX(J)
          ENDIF
          DO 415 I=1,NM
            READ(42,*,END=149)YNP(J,I),VNP(J,I),QNP(J,I),ZNP(J,I)
          IF(PPROFILS)THEN 
            READ(43,*,END=149)qsP(J,I),dmP(J,I),smP(J,I)
          ENDIF
C            ZNP(J,I)=YNP(J,I)+CTDF(I)
415       CONTINUE
          J1=J
413     CONTINUE

149     NBPROF=J1
        CLOSE(42)
        CLOSE(43)
 436    WRITE(*,*)'voulez-vous lire un autre fichier profil ?'
        WRITE(*,*)'oui ----> 1       non ------> 0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  irep=1
		  else
			  irep=0
		  endif
        IF(IREP.EQ.1)THEN
          WRITE(*,*)'ATTENTION : ON SUPPOSE MEME FICHIER GEOMAC-I '
          WRITE(*,*)'          ENTREZ LE NOM DE L''AUTRE ETUDE : '
          READ(*,702) ETUD2
          NOMFIC='profil.'//ETUD2
        NOMFIC2='profils.'//ETUD2
          GO TO 437
        ELSEIF(IREP.NE.0)THEN
          write(*,*)'erreur de choix'
          GO TO 436
        ENDIF
        CHAINE='       NOMBRE DE PROFILS :'
        WRITE(*,99)CHAINE,NBPROF
C        ENDIF

99      FORMAT(A30,I3)
        GO TO 2300
 2306   STOP
C NOMBRE TOTAL D'HYDROGRAMMES
 2305   NBSSAV=JH*NBSSAV
C conversion des temps
 2316    WRITE(*,*)'Voulez-vous l''expression des temps en'
         WRITE(*,*)'secondes ------>1'
         WRITE(*,*)'minutes  ------>2'
         WRITE(*,*)'heures   ------>3'
         WRITE(*,*)'rentrez votre choix'
         READ(*,'(A1)')AREP
		 IF(AREP.EQ.'1')then
			 itemp=1
		 elseIF(AREP.EQ.'2')then
			 itemp=2
		 elseIF(AREP.EQ.'3')then
			 itemp=3
		 else
			 itemp=1
		 endif
		 
         IF(ITEMP.EQ.2)THEN
           DO J=1,NBPROF
             TNX(J)=TNX(J)/60.
           ENDDO
           DO I=1,DIM
             T(I)=T(I)/60.
           ENDDO                
           DO I=1,NM
             TFDEB1(I)=TFDEB1(I)/60.
             TFDEB2(I)=TFDEB2(I)/60.
             TFDEB3(I)=TFDEB3(I)/60.
             TY(I)=TY(I)/60.
             TV(I)=TV(I)/60.
             TQ(I)=TQ(I)/60.
           ENDDO  
           IF(PENVLOPS)THEN
           DO I=1,NM-1
             TZM(I)=TZM(I)/60.
             TZFMAX(I)=TZFMAX(I)/60.
             TZFMIN(I)=TZFMIN(I)/60.
           ENDDO
           ENDIF
                    ELSEIF(ITEMP.EQ.3)THEN
           DO J=1,NBPROF
             TNX(J)=TNX(J)/3600.
           ENDDO
           DO I=1,DIM
             T(I)=T(I)/3600.
           ENDDO                
           DO I=1,NM
             TFDEB1(I)=TFDEB1(I)/3600.
             TFDEB2(I)=TFDEB2(I)/3600.
             TFDEB3(I)=TFDEB3(I)/3600.
             TY(I)=TY(I)/3600.
             TV(I)=TV(I)/3600.
             TQ(I)=TQ(I)/3600.
           ENDDO  
           IF(PENVLOPS)THEN
           DO I=1,NM-1
             TZM(I)=TZM(I)/3600.
             TZFMAX(I)=TZFMAX(I)/3600.
             TZFMIN(I)=TZFMIN(I)/3600.
           ENDDO
           ENDIF
         ELSEIF(ITEMP.NE.1)THEN
C on repose la question car la reponse est fausse
           WRITE(*,*)'REPONSE ERRONEE'
           GO TO 2316
         ENDIF
         
C-- RECHERCHE DES MINS ET DES MAX
C=================================
        XX=X(1)
        TYX=TY(1)
        YMX=YM(1)
        ZMX=ZM(1)
        TVX=TV(1)
        VMX=VM(1)
        TQX=TQ(1)
        QMX=QM(1)
        XN=XX
        TYN=TYX
        TVN=TVX
        TQN=TQX
        YMN=YMX
        ZMN=ZMX
        VMN=VMX
        QMN=QMX
        TFX=TFDEB1(1)
        TFN1=TFX
        TFX3=TFDEB3(1)
        TFN=TFX3
        CFX=CTDF(1)
        CFN=CFX
        IMAX=NM
       IF(PENVLOPS)THEN
        TZMX=TZM(1)
        TZMN=TZMX
        TZFMAXX=TZFMAX(1)
        TZFMAXN=TZFMAXX
        TZFMINX=TZFMIN(1)
        TZFMINN=TZFMINX
        ZFMAXX=ZFMAX(1)
        ZFMAXN=ZFMAXX
        ZFMINX=ZFMIN(1)
        ZFMINN=ZFMINX
       ENDIF 
        CALL LIM(TY,IMAX,TYX,TYN)
        CALL LIM(TQ,IMAX,TQX,TQN)
        CALL LIM(TV,IMAX,TVX,TVN)
        CALL LIM(X,IMAX,XX,XN)
        CALL LIM(YM,IMAX,YMX,YMN)
        CALL LIM(ZM,IMAX,ZMX,ZMN)
        CALL LIM(QM,IMAX,QMX,QMN)
        CALL LIM(VM,IMAX,VMX,VMN)
        CALL LIM(TFDEB1,IMAX,TFX,TFN1)
        CALL LIM(TFDEB3,IMAX,TFX3,TFN)
        CALL LIM(CTDF,IMAX,CFX,CFN)
        TX=T(1)
        TN=TX
        IF(PENVLOPS)THEN
        CALL LIM(ZM,IMAX-1,ZMX,ZMN)
        CALL LIM(TZM,IMAX-1,TZMX,TZMN)
        CALL LIM(TZFMAX,IMAX-1,TZFMAXX,TZFMAXN)
        CALL LIM(TZFMIN,IMAX-1,TZFMINX,TZFMINN)
        CALL LIM(ZFMAX,IMAX-1,ZFMAXX,ZFMAXN)
        CALL LIM(ZFMIN,IMAX-1,ZFMINX,ZFMINN)
C else sur penvlops
                 ELSE
        CALL LIM(ZM,IMAX,ZMX,ZMN)
         ENDIF         
        DO 59 J=1,NBSSAV
          DO 591 N=1,IH
            Q00(N)=Q0(J,N)
            V00(N)=V0(J,N)
            H00(N)=H0(J,N)
          IF(PHYDLIMS)THEN 
            Z00(N)=Z0(J,N)
            D00(N)=D0(J,N)
            S00(N)=S0(J,N)
          ENDIF  
591       CONTINUE
          Q0X(J)=Q0(J,1)
          Q0N(J)=Q0X(J)
          H0X(J)=H0(J,1)
          H0N(J)=H0X(J)
          V0X(J)=V0(J,1)
          V0N(J)=V0X(J)
          CALL LIM(T,IH,TX,TN)
          CALL LIM(Q00,IH,Q0X(J),Q0N(J))
          CALL LIM(H00,IH,H0X(J),H0N(J))
          CALL LIM(V00,IH,V0X(J),V0N(J))
          IF(PHYDLIMS)THEN 
          z0X(J)=z0(J,1)
          z0N(J)=z0X(J)
          d0X(J)=d0(J,1)
          d0N(J)=d0X(J)
          s0X(J)=s0(J,1)
          s0N(J)=s0X(J)
          CALL LIM(z00,IH,z0X(J),z0N(J))
          CALL LIM(d00,IH,d0X(J),d0N(J))
          CALL LIM(s00,IH,s0X(J),s0N(J))
          ENDIF
59      CONTINUE

        DO 70 J=1,NBPROF
          DO 701 N=1,NM
            ZNP0(N) = ZNP(J,N)
            YNP0(N) = YNP(J,N)
            VNP0(N) = VNP(J,N)
            QNP0(N) = QNP(J,N)
          IF(PPROFILS)THEN 
            QSP0(N) = QSP(J,N)
            DMP0(N) = DMP(J,N)
            SMP0(N) = SMP(J,N)
           ENDIF
701       CONTINUE
          ZNX(J) = ZNP(J,1)
          ZNN(J) = ZNX(J)
          YNX(J) = YNP(J,1)
          YNN(J) = YNX(J)
          VNX(J) = VNP(J,1)
          VNN(J) = VNX(J)
          QNX(J) = QNP(J,1)
          QNN(J) = QNX(J)

          CALL LIM(ZNP0,NM,ZNX(J),ZNN(J))
          CALL LIM(YNP0,NM,YNX(J),YNN(J))
          CALL LIM(VNP0,NM,VNX(J),VNN(J))
          CALL LIM(QNP0,NM,QNX(J),QNN(J))
          IF(PPROFILS)THEN 
                    QSX(J) = QSP(J,1)
          QSN(J) = QSX(J)
          DMX(J) = DMP(J,1)
          DMN(J) = DMX(J)
          SMX(J) = SMP(J,1)
          SMN(J) = SMX(J)
          CALL LIM(QSP0,NM,QSX(J),QSN(J))
          CALL LIM(DMP0,NM,DMX(J),DMN(J))
          CALL LIM(SMP0,NM,SMX(J),SMN(J))
          ENDIF
70      CONTINUE

10      WRITE(*,*)'    CHOIX DE L''ABSCISSE (1ERE COLONNE) :'
        WRITE(*,*)
        WRITE(*,*)'HYDROGRAMME,ETC :     TEMPS -->1'
        WRITE(*,*)
        WRITE(*,*)'FRONTS :        ABSCISSE ----->4'
        WRITE(*,*)'                TEMPS -------->5'
        WRITE(*,*)
        WRITE(*,*)'MAXIMA DES DEBITS,VITESSES,HAUTEURS,COTES:'
        WRITE(*,*)'                 ABSCISSE ---->6'
        WRITE(*,*)
        WRITE(*,*)'TEMPS D''ARRIVEE DES MAXI :'
        WRITE(*,*)'                ABSCISSE  --->10'
        WRITE(*,*)'                TYMAX ------->13'
        WRITE(*,*)'                TVMAX ------->15'
        WRITE(*,*)'                TQMAX ------->16'
        WRITE(*,*)
        WRITE(*,*)'PROFIL :       ABSCISSE ----->17'
        WRITE(*,*)
        IF(PENVLops)then
        WRITE(*,*)'ENVLOPS        ABSCISSE ----->27'
        ENDIF
        WRITE(*,*)
        WRITE(*,*)'QUEL EST VOTRE CHOIX ?'
        READ(*,*) ICHX
        IF((ICHX.GT.27).OR.(ICHX.LT.1)) THEN
          WRITE (*,*) 'VOUS AVEZ FAIT UN MAUVAIS CHOIX'
          WRITE(*,*)'IL FAUT UN NOMBRE INFERIEUR OU EGAL A 27'
          GOTO 10
        ENDIF


C -- CHOIX DE L'ORDONNEE
C ======================

20      WRITE(*,*)
        WRITE(*,*)'      CHOIX DE L''ORDONNEE (2EME COLONNE) :'
        WRITE(*,*)

        IF(ICHX.EQ.1) THEN
			iF(ITEMP.EQ.1)then
		  TITREX= ' temps (s)'	
			elseiF(ITEMP.EQ.2)then
		  TITREX= ' temps (mn)'	
			elseiF(ITEMP.EQ.3)then
		  TITREX= ' temps (h)'
	        endif
          WRITE(*,*)'QUEL TYPE DE SORTIE VOULEZ VOUS ?'
          WRITE(*,*)' HYDRO- ]         :   DEBIT ---->1'
          WRITE(*,*)' LIMNI- ] -GRAMME :   HAUTEUR -->2'
          WRITE(*,*)' VITI-  ]         :   VITESSE -->3'
          IF(PHYDLIMS)THEN
          WRITE(*,*)' limni- ]         :   cote fond ->21'
          WRITE(*,*)' diami- ] -GRAMME :   DIAMETRE -->22'
          WRITE(*,*)' hydro- ]         :   QS -->23'
          ENDIF
        ELSE IF(ICHX.EQ.5) THEN
			iF(ITEMP.EQ.1)then
		  TITREX= ' temps (s)'	
			elseiF(ITEMP.EQ.2)then
		  TITREX= ' temps (mn)'	
			elseiF(ITEMP.EQ.3)then
		  TITREX= ' temps (h)'
	        endif
          WRITE(*,*)'FRONTS :             ABSCISSE -->4'
        ELSE IF(ICHX.EQ.4) THEN
		  TITREX= ' abscisse (m)'	
          WRITE(*,*)'FRONTS :             TEMPS ----->5'
        ELSE IF (ICHX.EQ.6) THEN
		  TITREX= ' abscisse (m)'	
          WRITE(*,*)'QUEL MAXI VOULEZ VOUS SORTIR ?'
          WRITE(*,*)'                     ZMAX ------>6'
          WRITE(*,*)'                     YMAX ------>7'
          WRITE(*,*)'                     VMAX ------>8'
          WRITE(*,*)'                     QMAX ------>9'
        ELSE IF(ICHX.EQ.10) THEN
		  TITREX= ' abscisse (m)'	
          WRITE(*,*)'                    TQMAX ----->10'
          WRITE(*,*)'                    TYMAX ----->11'
          WRITE(*,*)'                    TVMAX ----->12'
        ELSE IF(ICHX.EQ.13) THEN
			iF(ITEMP.EQ.1)then
		  TITREX= ' temps (s)'	
			elseiF(ITEMP.EQ.2)then
		  TITREX= ' temps (mn)'	
			elseiF(ITEMP.EQ.3)then
		  TITREX= ' temps (h)'
	        endif
          WRITE(*,*)'                     YMAX ----->13'
          WRITE(*,*)'                     ZMAX ----->14'
        ELSE IF(ICHX.EQ.15) THEN
			iF(ITEMP.EQ.1)then
		  TITREX= ' temps (s)'	
			elseiF(ITEMP.EQ.2)then
		  TITREX= ' temps (mn)'	
			elseiF(ITEMP.EQ.3)then
		  TITREX= ' temps (h)'
	        endif
          WRITE(*,*)'                     VMAX ----->15'
        ELSE IF(ICHX.EQ.16) THEN
			iF(ITEMP.EQ.1)then
		  TITREX= ' temps (s)'	
			elseiF(ITEMP.EQ.2)then
		  TITREX= ' temps (mn)'	
			elseiF(ITEMP.EQ.3)then
		  TITREX= ' temps (h)'
	        endif
          WRITE(*,*)'                     QMAX ----->16'
        ELSE IF(ICHX.EQ.17) THEN
		  TITREX= ' abscisse (m)'	
          WRITE(*,*)'QUEL PROFIL VOULEZ VOUS SORTIR ?'
          WRITE(*,*)
          WRITE(*,*)'                     HAUTEUR -->17'
          WRITE(*,*)'                     VITESSE -->18'
          WRITE(*,*)'                     DEBIT ---->19'
          WRITE(*,*)'                     COTE  ---->20'
          IF(PPROFILS)THEN
          WRITE(*,*)'                     ETENDUE -->24'
          WRITE(*,*)'                     DIAMETRE -->25'
          WRITE(*,*)'                     QS      -->26'
          ENDIF
        ELSE IF(ICHX.EQ.27) THEN
		  TITREX= ' abscisse (m)'	
          WRITE(*,*)'QUELLE ENVELOPPE VOULEZ VOUS SORTIR ?'
          WRITE(*,*)
          WRITE(*,*)'                     ZMAX   -->27'
          WRITE(*,*)'                     ZFMIN  -->28'
          WRITE(*,*)'                     ZFMAX --->29'
          WRITE(*,*)'                     TZM    -->30'
          WRITE(*,*)'                     TZFMIN -->31'
          WRITE(*,*)'                     TZFMAX -->32'
        ENDIF


30      WRITE(*,*)'  '
        WRITE(*,*)'QUEL EST VOTRE CHOIX ?'
        READ(*,*) ICHY
        IF((ICHY.GT.32).OR.(ICHY.LT.1)) THEN
          WRITE (*,*) 'VOUS AVEZ FAIT UN MAUVAIS CHOIX'
          GOTO 20
        ENDIF


C b) SUIVANT OY
C--------------

        IF(ICHY.EQ.1) THEN
          WRITE(*,*)'LISTE DES HYDROGRAMMES DISPONIBLES  :'
          DO 510 I=1,NBSSAV
             WRITE(*,*)'HYDROGRAMME N',I,'ABSCISSE :',MAILLE(I),' m'
510       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DE L''HYDROGRAMME CHOISI :'
          READ(*,*) NUMHYD
          WRITE(*,*)
          TITREY(1)=' Debit (m3/s) '
        ELSE IF(ICHY.EQ.2) THEN
          WRITE(*,*)'LISTE DES LIMNIGRAMMES DISPONIBLES  :'
          DO 511 I=1,NBSSAV
             WRITE(*,*)'LIMNIGRAMME N',I,'ABSCISSE :',MAILLE(I),' m'
511       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU LIMNIGRAMME CHOISI :'
          READ(*,*) NUMLIM
          WRITE(*,*)
          TITREY(1)=' Hauteur (m) '
        ELSE IF(ICHY.EQ.3) THEN
          WRITE(*,*)'LISTE DES VITIGRAMMES DISPONIBLES  :'
          DO 512 I=1,NBSSAV
             WRITE(*,*)'VITIGRAMME N',I,'ABSCISSE :',MAILLE(I),' m'
512       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU VITIGRAMME CHOISI :'
          READ(*,*) NUMVIT
          WRITE(*,*)
          TITREY(1)='Vitesse (m/s)'
        ELSE IF(ICHY.EQ.21) THEN
          WRITE(*,*)'LISTE DES LIMNIGRAMMES DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'LIMNIGRAMME N',I,'ABSCISSE :',MAILLE(I),' m'
             WRITE(*,*)
          ENDDO  
          WRITE(*,*)'ENTREZ LE NUMERO DU LIMNIGRAMME CHOISI :'
          READ(*,*) NUMHYD
          WRITE(*,*)
          TITREY(1)=' cote fond (m) '
        ELSE IF(ICHY.EQ.22) THEN
          WRITE(*,*)'LISTE DES diametres DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'diametre N',I,'ABSCISSE :',MAILLE(I),' m'
          ENDDO  
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU DIAMETRE CHOISI :'
          READ(*,*) NUMHYD
          WRITE(*,*)
          TITREY(1)=' Diametre (m) '
        ELSE IF(ICHY.EQ.23) THEN
          WRITE(*,*)'LISTE DES HYDROGRAMMES DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'HYDROGRAMME N',I,'ABSCISSE :',MAILLE(I),' m'
          ENDDO  
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DE HYDROGRAMME CHOISI :'
          READ(*,*) NUMHYD
          WRITE(*,*)
          TITREY(1)=' QS (m3/s) '
        ELSE IF(ICHY.EQ.17) THEN
          WRITE(*,*)'LISTE DES PROFILS DE HAUTEUR DISPONIBLES :'
          DO 520 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
520       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + HAUTEURS :'
          READ(*,*)NUMPRH
          TITREY(1)=' Hauteur (m) '
        ELSE IF(ICHY.EQ.18) THEN
          WRITE(*,*)'LISTE DES PROFILS DE VITESSE DISPONIBLES :'
          DO 521 I=1,NBPROF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
521       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + VITESSES :'
          READ(*,*) NUMPRV
          TITREY(1)='Vitesse (m/s)'
        ELSE IF(ICHY.EQ.19) THEN
          WRITE(*,*)'LISTE DES PROFILS DE DEBIT DISPONIBLES :'
          DO 522 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
522       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + DEBITS :'
          READ(*,*) NUMPRQ
          TITREY(1) = ' Debit (m3/s)'
        ELSE IF(ICHY.EQ.20) THEN
          WRITE(*,*)'LISTE DES PROFILS DE COTE DISPONIBLES :'
          DO I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + COTES :'
          READ(*,*) NUMPRC
          TITREY(1) = 'Cote fond (m)'
        ELSE IF(ICHY.EQ.24) THEN
          WRITE(*,*)'LISTE DES PROFILS DE ETENDUE DISPONIBLES :'
          DO  I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + ETENDUES :'
          READ(*,*) NUMPRC
          TITREY(1)= '   ETENDUE (m)'
        ELSE IF(ICHY.EQ.25) THEN
          WRITE(*,*)'LISTE DES PROFILS DE DIAMETRE DISPONIBLES :'
          DO I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + DIAMETRES :'
          READ(*,*) NUMPRC
          TITREY(1) = '   DIAMETRE (m)'
        ELSE IF(ICHY.EQ.26) THEN
          WRITE(*,*)'LISTE DES PROFILS DE QS DISPONIBLES :'
          DO I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + QSS :'
          READ(*,*) NUMPRC
          TITREY(1)= '   QS (m)'
        ENDIF

200     FORMAT(F3.1)

C NOMBRE DE VARIABLES
C--------------------

        IF (ICHY.LE.3.OR.(ICHY.GT.20.AND.ICHY.LT.24)) THEN
          LM=IH
        ELSEIF(ICHY.GT.26.AND.ICHY.LT.33)THEN
          LM=NM-1
        ELSE
          LM=NM
        ENDIF

C RECUPERATION TABLEAU DES ABSCISSES
C------------------------------------

        IF(ICHX.EQ.1) THEN
          DO 50 I=1,LM
            A(I)=T(I)
50        CONTINUE
          XMAX=TX
          XMIN=TN
        ELSE IF(ICHX.EQ.4.OR.ICHX.EQ.6.OR.ICHX.EQ.10) THEN
          DO 501 I=1,LM
            A(I)=X(I)
501       CONTINUE
          XMAX=XX
          XMIN=XN
        ELSE IF(ICHX.EQ.5) THEN
          DO 502 I=1,LM
            A(I)=TFDEB1(I)
502       CONTINUE
          XMAX=TFX
          XMIN=TFN1
        ELSE IF(ICHX.EQ.13) THEN
          DO 505 I=1,LM
            A(I)=TY(I)
505       CONTINUE
          XMAX=TYX
          XMIN=TYN
        ELSE IF(ICHX.EQ.15) THEN
          DO 506 I=1,LM
            A(I)=TV(I)
506      CONTINUE
          XMAX=TVX
          XMIN=TVN
        ELSE IF(ICHX.EQ.16) THEN
          DO 507 I=1,LM
            A(I)=TQ(I)
507       CONTINUE
          XMAX=TQX
          XMIN=TQN
        ELSE IF(ICHX.EQ.17) THEN
          DO 508 I=1,LM
            A(I)=X(I)
508       CONTINUE
          XMAX=XX
          XMIN=XN
        ELSE IF(ICHX.EQ.27) THEN
          DO I=1,LM
            A(I)=X2(I)
          ENDDO  
          XMAX=XX
          XMIN=XN
        ENDIF


C RECUPERATION TABLEAU DES ORDONNEES
C ----------------------------------

        INDI=1
C ICHY = 1
        IF(ICHY.EQ.1) THEN
          DO 60 I=1,LM
            B(I)=Q0(NUMHYD,I)
60        CONTINUE
          YMAX=Q0X(NUMHYD)
          YMIN=Q0N(NUMHYD)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMHYD)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5510      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE HYDROGRAMME?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
C          STITRG='HYDROGRAMMES'
          WRITE(*,*)'LISTE DES HYDROGRAMMES DISPONIBLES  :'
          DO 7510 I=1,NBSSAV
             WRITE(*,*)'HYDROGRAMME N ',I,'ABSCISSE :',MAILLE(I),' m'
7510       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DE L''HYDROGRAMME CHOISI :'
          READ(*,*) NUMHY2
          WRITE(*,*)
          DO 7060 I=1,LM
            B2(I,INDI)=Q0(NUMHY2,I)
7060        CONTINUE
          YMAX=MAX(Q0X(NUMHY2),YMAX)
          YMIN=MIN(Q0N(NUMHY2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhyd)
            TITREY(1)='QX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhy2)
            TITREY(INDI+1)='QX = '//PARLEG//'M'
c            WRITE(PARLEG,'(F9.1)')MAILLE(NUMHY2)
c            TXTLEG(INDI+1)='ABSCISSE= '//PARLEG//'M'
          INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5510
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5510
          ENDIF
C ICHY = 21
        ELSEIF(ICHY.EQ.21) THEN
          DO I=1,LM
            B(I)=z0(NUMHYD,I)
          ENDDO
          YMAX=z0X(NUMHYD)
          YMIN=z0N(NUMHYD)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMHYD)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5581      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE LIMNIGRAMME?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='HYDROGRAMMES'
          WRITE(*,*)'LISTE DES LIMNIGRAMMES DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'LIMNIGRAMME N ',I,'ABSCISSE :',MAILLE(I),' m'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU LIMNIGRAMME CHOISI :'
          READ(*,*) NUMHY2
          WRITE(*,*)
          DO  I=1,LM
            B2(I,INDI)=z0(NUMHY2,I)
          ENDDO
          YMAX=MAX(z0X(NUMHY2),YMAX)
          YMIN=MIN(z0N(NUMHY2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhyd)
            TITREY(1)='ZX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhy2)
            TITREY(INDI+1)='ZX = '//PARLEG//'M'
c            WRITE(PARLEG,'(F9.1)')MAILLE(NUMHY2)
c            TXTLEG(INDI+1)='ABSCISSE= '//PARLEG//'M'
          INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5581
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5581
          ENDIF
C ICHY = 22
        ELSEIF(ICHY.EQ.22) THEN
          DO I=1,LM
            B(I)=D0(NUMHYD,I)
          ENDDO
          YMAX=d0X(NUMHYD)
          YMIN=d0N(NUMHYD)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMHYD)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5582      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE DIAMETRE?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='HYDROGRAMMES'
          WRITE(*,*)'LISTE DES DIAMETRES DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'DIAMETRE N ',I,'ABSCISSE :',MAILLE(I),' m'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU DIAMETRE CHOISI :'
          READ(*,*) NUMHY2
          WRITE(*,*)
          DO  I=1,LM
            B2(I,INDI)=D0(NUMHY2,I)
          ENDDO
          YMAX=MAX(D0X(NUMHY2),YMAX)
          YMIN=MIN(D0N(NUMHY2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhyd)
            TITREY(1)='dX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhy2)
            TITREY(INDI+1)='dX = '//PARLEG//'M'
c            WRITE(PARLEG,'(F9.1)')MAILLE(NUMHY2)
c            TXTLEG(INDI+1)='ABSCISSE= '//PARLEG//'M'
          INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5582
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5582
          ENDIF
C ICHY = 23
        ELSEIF(ICHY.EQ.23) THEN
          DO I=1,LM
            B(I)=S0(NUMHYD,I)
          ENDDO
          YMAX=S0X(NUMHYD)
          YMIN=S0N(NUMHYD)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMHYD)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5580      WRITE(*,*)'VOULEZ-VOUS SORTIR UNE AUTRE ETENDUE?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='HYDROGRAMMES'
          WRITE(*,*)'LISTE DES ETENDUES DISPONIBLES  :'
          DO I=1,NBSSAV
             WRITE(*,*)'ETENDUE N ',I,'ABSCISSE :',MAILLE(I),' m'
          ENDDO
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DE L''ETENDUE CHOISIE :'
          READ(*,*) NUMHY2
          WRITE(*,*)
          DO  I=1,LM
            B2(I,INDI)=S0(NUMHY2,I)
          ENDDO
          YMAX=MAX(S0X(NUMHY2),YMAX)
          YMIN=MIN(S0N(NUMHY2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhyd)
            TITREY(1)='sX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMhy2)
            TITREY(INDI+1)='sX = '//PARLEG//'M'
c           WRITE(PARLEG,'(F9.1)')MAILLE(NUMHY2)
c            TXTLEG(INDI+1)='ABSCISSE= '//PARLEG//'M'
          INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5580
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5580
          ENDIF
C ICHY = 2
        ELSE IF(ICHY.EQ.2) THEN
          DO 601 I=1,LM
            B(I)=H0(NUMLIM,I)
601       CONTINUE
          YMAX=H0X(NUMLIM)
          YMIN=H0N(NUMLIM)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMLIM)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5511      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE LIMNIGRAMME ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='LIMNIGRAMMES'
          WRITE(*,*)'LISTE DES LIMNIGRAMMES DISPONIBLES  :'
          DO 7511 I=1,NBSSAV
             WRITE(*,*)'LIMNIGRAMME N ',I,'ABSCISSE :',MAILLE(I),' m'
7511       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU LIMNIGRAMME CHOISI :'
          READ(*,*) NUMLI2
          WRITE(*,*)
          DO 7601 I=1,LM
            B2(I,INDI)=H0(NUMLI2,I)
7601       CONTINUE
          YMAX=MAX(H0X(NUMLI2),YMAX)
          YMIN=MIN(H0N(NUMLI2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMlim)
            TITREY(1)='hX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMlI2)
            TITREY(INDI+1)='hX = '//PARLEG//'M'
c            WRITE(PARLEG,'(F9.1)')MAILLE(NUMLI2)
c            TXTLEG(INDI+1)='ABSCISSE= '//PARLEG//'M'
          INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5511
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5511
          ENDIF
C ICHY = 3
        ELSE IF(ICHY.EQ.3) THEN
          DO 602 I=1,LM
            B(I)=V0(NUMVIT,I)
602       CONTINUE
          YMAX=V0X(NUMVIT)
          YMIN=V0N(NUMVIT)
c          WRITE(PARLEG,'(F9.1)')MAILLE(NUMVIT)
c          TXTLEG(1)='ABSCISSE= '//PARLEG//'M'
5512      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE VITIGRAMME ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='VITESSES'
          WRITE(*,*)'LISTE DES VITIGRAMMES DISPONIBLES  :'
          DO 7512 I=1,NBSSAV
             WRITE(*,*)'VITIGRAMME N ',I,'ABSCISSE :',MAILLE(I),' m'
7512       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU VITIGRAMME CHOISI :'
          READ(*,*) NUMVI2
          WRITE(*,*)
          DO 7602 I=1,LM
            B2(I,INDI)=V0(NUMVI2,I)
7602       CONTINUE
          YMAX=MAX(V0X(NUMVI2),YMAX)
          YMIN=MIN(V0N(NUMVI2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMVIt)
            TITREY(1)='vX = '//PARLEG//'M'
            ENDIF
            WRITE(PARLEG,'(F9.1)')MAILLE(NUMVI2)
            TITREY(INDI+1)='vX = '//PARLEG//'M'
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5512
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5512
          ENDIF
C ICHY = 4
        ELSE IF(ICHY.EQ.4) THEN
          DO 603 I=1,LM
            B(I)=X(I)
603       CONTINUE
          YMAX=XX
          YMIN=XN
C ICHY = 5
        ELSE IF(ICHY.EQ.5) THEN
          DO 604 I=1,LM
            B(I)=TFDEB1(I)
            B2(I,1)=TFDEB2(I)
            B2(I,2)=TFDEB3(I)
            WRITE(PARLEG,'(F9.1)')FDEB1
            TITREY(1)='DEBIT= '//PARLEG//'M3/S'
            WRITE(PARLEG,'(F9.1)')FDEB2
            TITREY(2)='DEBIT= '//PARLEG//'M3/S'
            WRITE(PARLEG,'(F9.1)')FDEB3
            TITREY(3)='DEBIT= '//PARLEG//'M3/S'
            INDI=3
604       CONTINUE
          YMAX=TFX
          YMIN=TFN
          GO TO 5610
C ICHY = 6 OU 13
        ELSE IF(ICHY.EQ.6.OR.ICHY.EQ.13) THEN
          DO 608 I=1,LM
c            B2(I,1)=ZM(I)
c            B(I)=CTDF(I)
            B(I)=ZM(I)
608       CONTINUE
          YMAX=ZMX
          YMIN=CFN
           TITREY(1)='Zmax EAU (m)'
C           TITREY(1)='COTE DU FOND initiale '
c          TXTLEG(2)='COTE MAXIMALE'
          INDI=1
C ICHY = 7 OU 14
        ELSE IF(ICHY.EQ.7.OR.ICHY.EQ.14) THEN
          DO 605 I=1,LM
            B(I)=YM(I)
605       CONTINUE
          YMAX=YMX
          YMIN=YMN
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='ZE A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='ZE A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='ZE A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'H'
            ENDIF
C ICHY = 8 OU 15
        ELSE IF(ICHY.EQ.8.OR.ICHY.EQ.15) THEN
          DO 606 I=1,LM
            B(I)=VM(I)
606       CONTINUE
          YMAX=VMX
          YMIN=VMN
C ICHY = 9 OU 16
        ELSE IF(ICHY.EQ.9.OR.ICHY.EQ.16) THEN
          DO 607 I=1,LM
            B(I)=QM(I)
607       CONTINUE
          YMAX=QMX
          YMIN=QMN
C ICHY = 10
        ELSE IF(ICHY.EQ.10) THEN
          DO 611 I=1,LM
            B(I)=TQ(I)
611       CONTINUE
          YMAX=TQX
          YMIN=TQN
          GO TO 5610
C ICHY = 11
        ELSE IF(ICHY.EQ.11) THEN
          DO 609 I=1,LM
            B(I)=TY(I)
609       CONTINUE
          YMAX=TYX
          YMIN=TYN
          GO TO 5610
C ICHY = 12
        ELSE IF(ICHY.EQ.12) THEN
          DO 610 I=1,LM
            B(I)=TV(I)
610       CONTINUE
          YMAX=TVX
          YMIN=TVN
          GO TO 5610
C ICHY = 17
        ELSE IF(ICHY.EQ.17) THEN
          DO 612 I=1,LM
            B(I)=YNP(NUMPRH,I)
612       CONTINUE
          YMAX=YNX(NUMPRH)
          YMIN=YNN(NUMPRH)
c          WRITE(PARLEG,'(F9.1)')TNX(NUMPRH)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
5520      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='PROFILS DE HAUTEUR'
          WRITE(*,*)'LISTE DES PROFILS DE HAUTEUR DISPONIBLES :'
          DO 7520 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
7520       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + HAUTEURS :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO 7612 I=1,LM
            B2(I,INDI)=YNP(NUMPR2,I)
7612       CONTINUE
          YMAX=MAX(YNX(NUMPR2),YMAX)
          YMIN=MIN(YNN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRh)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='H A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='H A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='H A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='H A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='H A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='H A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
C            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5520
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5520
          ENDIF
C ICHY = 18
        ELSE IF(ICHY.EQ.18) THEN
          DO 613 I=1,LM
            B(I)=VNP(NUMPRV,I)
613       CONTINUE
          YMAX=VNX(NUMPRV)
          YMIN=VNN(NUMPRV)
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPRV)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
C            TXTLEG(1)='TEMPS = '//PARLEG//'S  '
5521      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='PROFILS DE VITESSE'
          WRITE(*,*)'LISTE DES PROFILS DE VITESSE DISPONIBLES :'
          DO 7521 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
7521       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + VITESSES :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO 7613 I=1,LM
            B2(I,INDI)=VNP(NUMPR2,I)
7613       CONTINUE
          YMAX=MAX(VNX(NUMPR2),YMAX)
          YMIN=MIN(VNN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRv)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='V A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='V A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='V A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='V A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='V A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='V A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
C            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5521
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5521
          ENDIF
C ICHY = 19
        ELSE IF(ICHY.EQ.19) THEN
          DO 614 I=1,LM
            B(I)=QNP(NUMPRQ,I)
614       CONTINUE
          YMAX=QNX(NUMPRQ)
          YMIN=QNN(NUMPRQ)
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPRQ)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
C            TXTLEG(1)='TEMPS = '//PARLEG//'S  '
5522      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='PROFILS DE DEBIT'
          WRITE(*,*)'LISTE DES PROFILS DE DEBIT DISPONIBLES :'
          DO 7522 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
7522       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + DEBITS :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO 7614 I=1,LM
            B2(I,INDI)=QNP(NUMPR2,I)
7614       CONTINUE
          YMAX=MAX(QNX(NUMPR2),YMAX)
          YMIN=MIN(QNN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRQ)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='Q A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='Q A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='Q A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='Q A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='Q A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='Q A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
C            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='TEMPS = '//PARLEG//'H  '
c            ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5522
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5522
          ENDIF
C ICHY = 20
        ELSE IF(ICHY.EQ.20) THEN
	      write(*,*)'entrer 1 si cote eau'
	      write(*,*)'entrer 0 si cote fond'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
		  If(rep.eq.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='ZE A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='ZE A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='ZE A '//PARLEG//'H'
            ENDIF
c          TITREY(1) = '   Cote eau (m)'
          DO  I=1,LM
            B(I)=ZNP(NUMPRC,I)+YNP(NUMPRC,I)
          enddo
          YMAX=ZNX(NUMPRC)+ynx(NUMPRC)
          YMIN=ZNN(NUMPRC)+ynn(numprc)
          else
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='ZF A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='ZF A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='ZF A '//PARLEG//'H'
            ENDIF
          DO 615 I=1,LM
            B(I)=ZNP(NUMPRC,I)
615       CONTINUE
          YMAX=ZNX(NUMPRC)
          YMIN=ZNN(NUMPRC)
          endif		  
5523      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='LIGNE D''EAU'
          WRITE(*,*)'LISTE DES PROFILS DE COTE DISPONIBLES :'
          DO 7523 I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
7523       CONTINUE
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + COTES :'
          READ(*,*)NUMPR2
          WRITE(*,*)
	      write(*,*)'entrer 1 si cote eau'
	      write(*,*)'entrer 0 si cote fond'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
		  If(rep.eq.1)THEN
          DO I=1,LM
            B2(I,INDI)=ZNP(NUMPR2,I)+YNP(NUMPR2,I)
          ENDDO
          YMAX=MAX(ZNX(NUMPR2)+ynx(numpr2),YMAX)
          YMIN=MIN(ZNN(NUMPR2)+ynn(numpr2),YMIN)
C            IF(INDI.EQ.1)THEN
C            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
C            IF(ITEMP.EQ.1)THEN
C              TITREY(1)='ZE A '//PARLEG//'S'
C            ELSEIF(ITEMP.EQ.2)THEN
C              TITREY(1)='ZE A '//PARLEG//'M'
C            ELSEIF(ITEMP.EQ.3)THEN
C              TITREY(1)='ZE A '//PARLEG//'H'
C            ENDIF
C            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='ZE A '//PARLEG//'H'
            ENDIF
c si cote fond
			else	  
          DO 7615 I=1,LM
            B2(I,INDI)=ZNP(NUMPR2,I)
7615       CONTINUE
          YMAX=MAX(ZNX(NUMPR2),YMAX)
          YMIN=MIN(ZNN(NUMPR2),YMIN)
C            IF(INDI.EQ.1)THEN
C            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
C            IF(ITEMP.EQ.1)THEN
C              TITREY(1)='ZF A '//PARLEG//'S'
C            ELSEIF(ITEMP.EQ.2)THEN
C              TITREY(1)='ZF A '//PARLEG//'M'
C            ELSEIF(ITEMP.EQ.3)THEN
C              TITREY(1)='ZF A '//PARLEG//'H'
C            ENDIF
C            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='ZF A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='ZF A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='ZF A '//PARLEG//'H'
            ENDIF
c            IF(INDI.EQ.1)THEN
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
C            TXTLEG(1)='COTE A  '//PARLEG//'S  '
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(1)='COTE A '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(1)='COTE A '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(1)='COTE A '//PARLEG//'H  '
c            ENDIF
c            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='COTE A '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='COTE A '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='COTE A '//PARLEG//'H  '
c            ENDIF
C            TXTLEG(INDI+1)='COTE A  '//PARLEG//'S  '
           ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5523
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5523
          ENDIF
C ICHY = 24
        ELSE IF(ICHY.EQ.24) THEN
          DO  I=1,LM
            B(I)=SMP(NUMPRC,I)
          ENDDO  
          YMAX=SMX(NUMPRC)
          YMIN=SMN(NUMPRC)
5623      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='LIGNE D''EAU'
          WRITE(*,*)'LISTE DES PROFILS DE ETENDUE DISPONIBLES :'
          DO  I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO  
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + ETENDUES :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO I=1,LM
            B2(I,INDI)=SMP(NUMPR2,I)
          ENDDO  
          YMAX=MAX(SMX(NUMPR2),YMAX)
          YMIN=MIN(SMN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='S A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='S A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='S A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='S A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='S A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='S A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='ETENDUE A '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='ETENDUE A '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='ETENDUE A '//PARLEG//'H  '
c            ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5623
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5623
          ENDIF
C ICHY = 25
        ELSE IF(ICHY.EQ.25) THEN
          DO  I=1,LM
            B(I)=DMP(NUMPRC,I)
          ENDDO  
          YMAX=DMX(NUMPRC)
          YMIN=DMN(NUMPRC)
5723      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
c          STITRG='LIGNE D''EAU'
          WRITE(*,*)'LISTE DES PROFILS DE DIAMETRE DISPONIBLES :'
          DO  I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO  
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + DIAMETRES :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO I=1,LM
            B2(I,INDI)=DMP(NUMPR2,I)
          ENDDO  
          YMAX=MAX(DMX(NUMPR2),YMAX)
          YMIN=MIN(DMN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='d A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='d A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='d A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='d A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='d A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='d A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='DIAMETRE A '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='DIAMETRE A '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='DIAMETRE A '//PARLEG//'H  '
c            ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5723
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5723
          ENDIF
C ICHY = 26
        ELSE IF(ICHY.EQ.26) THEN
          DO  I=1,LM
            B(I)=QSP(NUMPRC,I)
          ENDDO  
          YMAX=QSX(NUMPRC)
          YMIN=QSN(NUMPRC)
5823      WRITE(*,*)'VOULEZ-VOUS SORTIR UN AUTRE PROFIL ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
          WRITE(*,*)'LISTE DES PROFILS DE QS DISPONIBLES :'
          DO  I=1,NBPROF
            IF(ITEMP.EQ.1)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
            ELSEIF(ITEMP.EQ.2)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),'mn'
            ELSEIF(ITEMP.EQ.3)THEN
             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' h'
            ENDIF
C             WRITE(*,*)'PROFIL N ',I,' TEMPS :',TNX(I),' s'
          ENDDO  
          WRITE(*,*)
          WRITE(*,*)'ENTREZ LE NUMERO DU PROFIL CHOISI POUR LES
     + QSS :'
          READ(*,*)NUMPR2
          WRITE(*,*)
          DO I=1,LM
            B2(I,INDI)=QSP(NUMPR2,I)
          ENDDO  
          YMAX=MAX(QSX(NUMPR2),YMAX)
          YMIN=MIN(QSN(NUMPR2),YMIN)
            IF(INDI.EQ.1)THEN
            WRITE(PARLEG,'(F9.1)')TNX(NUMPRC)
            IF(ITEMP.EQ.1)THEN
              TITREY(1)='QS A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(1)='QS A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(1)='QS A '//PARLEG//'H'
            ENDIF
            ENDIF
            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
            IF(ITEMP.EQ.1)THEN
              TITREY(indi+1)='QS A '//PARLEG//'S'
            ELSEIF(ITEMP.EQ.2)THEN
              TITREY(indi+1)='QS A '//PARLEG//'M'
            ELSEIF(ITEMP.EQ.3)THEN
              TITREY(indi+1)='QS A '//PARLEG//'H'
            ENDIF
c            WRITE(PARLEG,'(F9.1)')TNX(NUMPR2)
c            IF(ITEMP.EQ.1)THEN
c            TXTLEG(INDI+1)='QS A '//PARLEG//'S  '
c            ELSEIF(ITEMP.EQ.2)THEN
c            TXTLEG(INDI+1)='QS A '//PARLEG//'MN '
c            ELSEIF(ITEMP.EQ.3)THEN
c            TXTLEG(INDI+1)='QS A '//PARLEG//'H  '
c            ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5823
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5823
          ENDIF
          WRITE(*,*)'                     ZM -->27'
          WRITE(*,*)'                     ZFMIN -->28'
          WRITE(*,*)'                     ZFMAX ---->29'
          WRITE(*,*)'                     TZM -->30'
          WRITE(*,*)'                     TZFMIN -->31'
          WRITE(*,*)'                     TZFMAX ---->32'
C ICHY = 27
        ELSE IF(ICHY.EQ.27) THEN
          DO  I=1,LM
            B(I)=ZM0(I)
          ENDDO
          YMAX=ZMX
          YMIN=ZMN
          GO TO 5610
C ICHY = 28
        ELSE IF(ICHY.EQ.28) THEN
          DO  I=1,LM
            B(I)=ZFMIN(I)
          ENDDO
          YMAX=ZFMINX
          YMIN=ZFMINN
          GO TO 5610
C ICHY = 29
        ELSE IF(ICHY.EQ.29) THEN
          DO  I=1,LM
            B(I)=ZFMAX(I)
          ENDDO
          YMAX=ZFMAXX
          YMIN=ZFMAXN
          GO TO 5610
C ICHY = 30
        ELSE IF(ICHY.EQ.30) THEN
          DO  I=1,LM
            B(I)=TZM(I)
          ENDDO
          YMAX=TZMX
          YMIN=TZMN
          GO TO 5610
C ICHY = 31
        ELSE IF(ICHY.EQ.31) THEN
          DO  I=1,LM
            B(I)=TZFMIN(I)
          ENDDO
          YMAX=TZFMINX
          YMIN=TZFMINN
          GO TO 5610
C ICHY = 32
        ELSE IF(ICHY.EQ.32) THEN
          DO  I=1,LM
            B(I)=TZFMAX(I)
          ENDDO
          YMAX=TZFMAXX
          YMIN=TZFMAXN
          GO TO 5610
        ENDIF
        GO TO 5611
C COMPLEMENT AU SORTI DES TEMPS D'ARRIVEE
5610      WRITE(*,*)'VOULEZ-VOUS SORTIR UNE AUTRE COURBE ?'
          WRITE(*,*)'OUI --->1    NON --->0'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  rep=1
		  else
			  rep=0
		  endif
          IF (REP.EQ.1)THEN
          WRITE(*,*)'LISTE DES COURBES POSSIBLES :'
             WRITE(*,*)'TEMPS D''ARRIVEE DU QMAX --->  1'
             WRITE(*,*)'TEMPS D''ARRIVEE DU YMAX --->  2'
             WRITE(*,*)'TEMPS D''ARRIVEE DU VMAX --->  3'
         WRITE(*,*)'TEMPS D''ARRIVEE DU FRONT DE DEBIT',FDEB1,'--->  4'
         WRITE(*,*)'TEMPS D''ARRIVEE DU FRONT DE DEBIT',FDEB2,'--->  5'
         WRITE(*,*)'TEMPS D''ARRIVEE DU FRONT DE DEBIT',FDEB3,'--->  6'
             WRITE(*,*)'TEMPS D''ARRIVEE DU zMAX --->  7'
             WRITE(*,*)'TEMPS D''ARRIVEE DU zfMAX --->  8'
             WRITE(*,*)'TEMPS D''ARRIVEE DU zfmin --->  9'
          READ(*,*)NUMPR2
          WRITE(*,*)
          IF(NUMPR2.EQ.1)THEN
          DO 5910 I=1,LM
            B2(I,INDI)=TQ(I)
 5910     CONTINUE
            YMAX=MAX(YMAX,TQX)
            YMIN=MIN(YMIN,TQN)
            TITREY(INDI+1)='TEMPS DE QMAX'
          ELSEIF(NUMPR2.EQ.2)THEN
          DO 5911 I=1,LM
            B2(I,INDI)=TY(I)
            YMAX=MAX(YMAX,TYX)
            YMIN=MIN(YMIN,TYN)
 5911     CONTINUE
            Titrey(INDI+1)='TEMPS DE YMAX'
          ELSEIF(NUMPR2.EQ.3)THEN
          DO 5912 I=1,LM
            B2(I,INDI)=TV(I)
            YMAX=MAX(YMAX,TVX)
            YMIN=MIN(YMIN,TVN)
 5912     CONTINUE
            Titrey(INDI+1)='TEMPS DE VMAX'
c
          ELSEIF(NUMPR2.EQ.4)THEN
          DO 5913 I=1,LM
            B2(I,INDI)=TFDEB1(I)
            YMAX=MAX(YMAX,TFX)
            YMIN=MIN(YMIN,TFN)
 5913     CONTINUE
           WRITE(PARLEG,'(F9.1)')FDEB1
           Titrey(INDI+1)='T '//PARLEG//'M3/S'
          ELSEIF(NUMPR2.EQ.5)THEN
          DO 5914 I=1,LM
            B2(I,INDI)=TFDEB2(I)
            YMAX=MAX(YMAX,TFX)
            YMIN=MIN(YMIN,TFN)
 5914     CONTINUE
            WRITE(PARLEG,'(F9.1)')FDEB2
            Titrey(INDI+1)='T '//PARLEG//'M3/S'
          ELSEIF(NUMPR2.EQ.6)THEN
          DO 5915 I=1,LM
            B2(I,INDI)=TFDEB3(I)
            YMAX=MAX(YMAX,TFX)
            YMIN=MIN(YMIN,TFN)
 5915     CONTINUE
            WRITE(PARLEG,'(F9.1)')FDEB3
            Titrey(INDI+1)='TEMPS '//PARLEG//'M3/S'
          ELSEIF(NUMPR2.EQ.7)THEN
          DO  I=1,LM
            B2(I,INDI)=TV(I)
            YMAX=MAX(YMAX,TVX)
            YMIN=MIN(YMIN,TVN)
           ENDDO
            Titrey(INDI+1)='TEMPS DE ZMAX'
          ELSEIF(NUMPR2.EQ.8)THEN
          DO  I=1,LM
            B2(I,INDI)=TV(I)
            YMAX=MAX(YMAX,TVX)
            YMIN=MIN(YMIN,TVN)
           ENDDO
            Titrey(INDI+1)='TEMPS DE ZFMAX'
          ELSEIF(NUMPR2.EQ.9)THEN
          DO  I=1,LM
            B2(I,INDI)=TV(I)
            YMAX=MAX(YMAX,TVX)
            YMIN=MIN(YMIN,TVN)
           ENDDO
            Titrey(INDI+1)='TEMPS DE zfmin'
          ENDIF
           INDI=INDI+1
          IF(INDI.LT.NCOURB)GO TO 5610
          ELSE IF(REP.NE.0)THEN
            WRITE(*,*)'MAUVAISE REPONSE'
            GO TO 5610
          ENDIF
C        ENDIF

C DEFINITION DES TABLEAUX DE COURBES A SORTIR
c 5611   IF((ICHY.GT.4.AND.ICHY.LT.13).OR.(ICHY.GT.16))THEN
c          WRITE(*,*)'VOULEZ-VOUS RAJOUTER DES POINTS ?'
c          WRITE(*,*)'DONNEZ LE NOMBRE DE FICHIERS FORMAT CASTOR '
c          WRITE(*,*)' NOMBRE ENTRE 0 ET 5'
c          READ(*,*)NBF
c           NBF=0
c          IF(NBF.GT.0)THEN
c            DO 330 KP=1,NBF
c 310        WRITE(*,*)'DONNEZ LE NOM DU FICHIER'
c            READ(*,'(A38)')NOMFIC
c            OPEN(10,FILE=NOMFIC,STATUS='OLD',ERR=350)
c            GO TO 340
c 350        WRITE(*,*)'FICHIER INEXISTANT'
c            GO TO 310
c 340        READ(10,*)NBP(KP)
c            DO 320 K=1,NBP(KP)
c              IF(ICHY.EQ.9.OR.ICHY.EQ.19)THEN
c                READ(10,*)XPO(KP,K),VAL(KP,K),POUB1,POUB2,POUB3,POUB4
c              ELSEIF(ICHY.EQ.7.OR.ICHY.EQ.17)THEN
c                READ(10,*)XPO(KP,K),POUB1,VAL(KP,K),POUB2,POUB3,POUB4
c              ELSEIF(ICHY.EQ.6.OR.ICHY.EQ.20)THEN
c                READ(10,*)XPO(KP,K),POUB1,POUB2,VAL(KP,K),POUB3,POUB4
c              ELSEIF(ICHY.EQ.8.OR.ICHY.EQ.18)THEN
c                READ(10,*)XPO(KP,K),POUB1,POUB2,POUB3,VAL(KP,K),POUB4
c              ELSE
c                READ(10,*)XPO(KP,K),POUB1,POUB2,POUB3,POUB4,VAL(KP,K)
c              ENDIF
c  320      CONTINUE
c           CLOSE(10)
c          DO 1333 K=1,NBP(KP)
c            YMIN=MIN(VAL(KP,K),YMIN)
c            YMAX=MAX(VAL(KP,K),YMAX)
c 1333      CONTINUE
c 330     CONTINUE
c        ENDIF
c        ENDIF
C DEFINITION DES TABLEAUX DE COURBES A TRACER
C LE NOMBRE TOTAL DE COURBES EST INDI + NBF
C ici nbf=0
 5611   NUMCOU=INDI+NBF
        NBPCOU(1)=LM
        DO 7249 I=1,LM
           XINIT(1,I)=A(I)
           YINIT(1,I)=B(I)
 7249   CONTINUE       
        DO 7247 K=2,INDI
          NBPCOU(K)=LM
          DO 7248 I=1,LM        
           XINIT(K,I)=A(I)
           YINIT(K,I)=B2(I,K-1)
 7248     CONTINUE
 7247   CONTINUE   
c        DO 7245 K=1,NBF
c          NBPCOU(K+INDI)=NBP(K)
c          DO 7246 I=1,NBP(K)       
c           XINIT(K+INDI,I)=XPO(K,I)
c           YINIT(K+INDI,I)=VAL(K,I)
c 7246     CONTINUE
c 7245   CONTINUE   

C MODIFICATION DES VALEURS PAR DEFAUT 
        NOMFICH='dessin.'//ETUDE
 1300   WRITE(*,*)'VOULEZ VOUS MODIFIER '
        WRITE(*,*)
        WRITE(*,*)'   RIEN                                 ----> 0'
        WRITE(*,*)'   LE TITRE DE L''AXE OX                ----> 1'
        WRITE(*,*)'   LE TITRE DE L''AXE OY                ----> 2'
        WRITE(*,*)'   LE NOM DU FICHIER COLONNES en Sortie ----> 3'
C QUADRILLAGE FOND ET TYPE D'AXE NON INSTALLE
        WRITE(*,*)
        WRITE(*,*)'DONNEZ VOTRE CHOIX'
          READ(*,'(A1)')AREP
		  IF(AREP.EQ.'1')then
			  ioech=1
		  elseIF(AREP.EQ.'2')then
			  ioech=2
		  elseIF(AREP.EQ.'3')then
			  ioech=3
		  else
			  ioech=0
		  endif
        IF (IOECH.EQ.1) THEN
          WRITE(*,*)'ENTREZ LE TITRE DE L''AXE OX',
     :' EN MOINS DE 15 CARACTERES'
          READ(*,'(A15)')TITREX
          GO TO 1300
        ELSEIF (IOECH.EQ.2)THEN
          WRITE(*,*)'ENTREZ LE TITRE DE L''AXE OY',
     :' EN MOINS DE 15 CARACTERES'
          READ(*,'(A15)')TITREY
          GO TO 1300
        ELSEIF (IOECH.EQ.3)THEN
          WRITE(*,*)'ENTREZ LE NOM DU FICHIER',
     :' EN MOINS DE 27 CARACTERES'
          READ(*,'(A27)')NOMFICH
          GO TO 1300
        ELSEIF (IOECH.NE.0) THEN
          WRITE(*,*)'MAUVAISE REPONSE'
          GO TO 1300
        ENDIF

C TRACE DU DESSIN
        CALL DESS(NUMCOU,NOMFICH)

 300  WRITE(*,*)'  '
      WRITE(*,*) '---------------------------------------------------'
      WRITE(*,*) 'CHOIX 1 : CHOIX D''UNE AUTRE COURBE DE LA MEME ETUDE'
c      WRITE(*,*) 'CHOIX 2 : PASSAGE A UNE AUTRE ETUDE'
      WRITE(*,*) 'CHOIX 0 : SORTIE DU PROGRAMME        '
      WRITE(*,*) '---------------------------------------------------'
      WRITE(*,*) '  '
      WRITE(*,*) 'DONNEZ VOTRE CHOIX'
      READ (*,'(A1)') AREP
	  IF(AREP.EQ.'1')THEN
		  choix=1
	  ELSE
          choix=2
	  ENDIF

C -- TEST DU CHOIX

        TESTCH=.FALSE.
        DO 40 I=1,2
          TESTCH=TESTCH.OR.(CHOIX.EQ.I)
40     CONTINUE
        IF (.NOT.TESTCH) THEN
          WRITE(*,*) 'VOUS AVEZ DONNE UN MAUVAIS CHOIX'
          WRITE(*,*) 'RECOMMENCEZ'
          GOTO 300
        ENDIF
        GOTO(10,4),CHOIX

 4      STOP
        END
C--------------------------------------------------------------------C
        SUBROUTINE DESS(NUMCOU,NOMFICH)
C--------------------------------------------------------------------C
C                                                                    C
C                     SORTI  GRAPHIQUE                               C
C                 remplace ici par l'ecriture d'un fichier colonne   C
C--------------------------------------------------------------------C


	INTEGER NUMCOU,IFILE
 	INTEGER NCOURB,DIM,I,J,K
	PARAMETER (NCOURB=10,DIM=10000)
	INTEGER NBPCOU(NCOURB)
        DOUBLE PRECISION X(NCOURB,DIM),Y(NCOURB,DIM),volume(ncourb,dim)
        CHARACTER*20 ETUDE
        CHARACTER*27 NOMFICH
	CHARACTER*15 TITREX,TITREY(ncourb)
	
	COMMON /AXE_X/ TitreX
	COMMON /AXE_Y/ TitreY

	COMMON/VALEUR/X,Y
        COMMON/NBVALE/NBPCOU
        COMMON/NOMETU/ETUDE

C ****************************************************************************
        IFILE=10
        OPEN(IFILE,FILE=NOMFICH,STATUS='UNKNOWN')
        J=nbpcou(1)
        DO 1 I=1,NUMCOU 
         IF(J.LT.NBPCOU(I))J=NBPCOU(I)
 1      CONTINUE          	
        DO 4 I=1,NUMCOU 
          volume(i,1)=0.
          do k=2,j
            volume(i,k)= volume(i,k-1)
     :    +0.5*(y(i,k-1)+y(i,k))*(x(i,k)-x(i,k-1))
          enddo
 4      CONTINUE          	
          WRITE(IFILE,'(20A15)')TITREX,(TITREY(i),I=1,NUMCOU)
        DO 2 K=1,J
          WRITE(IFILE,'(20F15.4)')X(1,K),(Y(I,K),I=1,NUMCOU)
 2      CONTINUE          
        write(ifile,*)' '
C        write(ifile,*)'volumes'
        write(ifile,'(20A15)') titrex,
     :('    volumes   ',I=1,NUMCOU)
        DO 5 K=2,J
          WRITE(IFILE,'(20F15.4)')X(1,K),(volume(I,K),I=1,NUMCOU)
 5      CONTINUE          
        CLOSE(IFILE)
        RETURN
        END

C----------------------------------------------------------------
        SUBROUTINE LIM(A,IM,AX,AN)
C----------------------------------------------------------------
C       CHERCHE LE MIN (AN) ET LE MAX (AX)
C       PARMI UNE SERIE DE NOMBRES (A(I))
C       AVEC I=1,IM
C----------------------------------------------------------------

C       IMPLICIT NONE

        INTEGER I,IM

        DOUBLE PRECISION A(IM),AN,AX

        DO 500 I=1,IM
          IF(A(I).LT.AN) THEN
            AN=A(I)
          ENDIF
          IF(A(I).GT.AX) THEN
            AX=A(I)
          ENDIF
500     CONTINUE

        RETURN

        END

C ****************************************************************************
	DOUBLE PRECISION FUNCTION Signe(x)
C SE: C Renvoie le signe (+/-) du parametre x
C SI: pour nombre reel uniquement
C RESULTAT:	 	
C		
C ****************************************************************************
          DOUBLE PRECISION X
	  IF (x.LT.0) THEN 
	    Signe=-1.
	  ELSE
	    Signe=+1.
	  ENDIF
	END




