C Version avec g�om�trie en abscisse-cote
C introduction lit mineur/moyen pour le transport solide
C***********************************************************************
C
C          PROGRAMME DE CALCUL HYDRODYNAMIQUE AVEC FOND MOBILE
C cou2D a supprimer si couplage avec 2D
C cou1D a supprimer si pas de couplage avec 2D
C ctrs a supprimer si rubarbe
C cliq a supprimer si rubar 3 1 bief
C cympd a supprimer pour introduire limiattions sur valeurs intermailles (version b)
C cqmpd a supprimer pour limitations supplementaires sur Q
C (limitations sur V alors � desactiver comme dans la version du 25/10/12)
C cconfl a supprimer traitement par conflue2 au lieu de conflue pour confluences
C
C            Version du 11/06/2002 par A.Paquier & P.BalaYN & K. EL Kadi
C par rapport a version fin septembre 2001, MPmuller modifie ajoute
C ainsi que evolution granulometrie etendue
C version du 15 octobre 2007 : plusieurs MPC
C version du 101207 : introduction HYDSAM pour gestion de condam=0 si TS
C version du 17 mars 2008 : introduction 6 formules de frottement en lit mobile
C version du 20 mai 2009 : multibief
C 29 juillet 2009 : plusieurs B et ouvrage T supprime,
C gestion apports lateraux modifiee (devers pas necessaire)
C 24 aout 2009 ajout par Kamal de la formule de Wu et wang pour la
C distance de chargement : common/odchar et modif SP INITS et DICHAR
C 30 septembre 2009 : ajout ouvrage O
c 27 OCTOBRE 2009 PARTAGE EN TROIS DU pp
C 18 novembre 2009 : appel � qouvr une fois par pas de temps
C 6 aout 2010 : correction des formules m�thode de Roe
C 30 aout 2010 : correction des quantites de mouvement en multibief et
C      abandon de la minimisation de qmd-qpd (cqmpd pour la version b) et
C                 limitation de vm et vp
C 16 septembre 2010 :  les conditions aux limites sont calculees
C a tn+1 aux confluences pour eviter oscillations
C janvier 2011 : introduction couplage avec  2D
C f�vrier 2011 ajout formules de capacit� de transport
C mars 2011 : conditions aux limites ramenees a tn+1/2
C Septembre 2011 : depots dans le lit actif au lieu du lit mineur
C avril 2012 : modification pour interreg Rhin sur couche active
C mai 2012 : fente de preisman, modif des fonctions detpm et detpmi
C avec introduction d'un iffente et dlfente et xncfente,ncfente
C aout 2012 : ajout des parametres puierosion,puidepot, methoderosion
C et des contraintes proportionnelles a hauteur eau (SP CONTHAUT)
C et methode depot 7 fonction de (tau -taucr)**pui
C 12 juin 2013: depsecn  (n>2) renvoi vers depsec2 au lieu de depsec
C si contrainte faible pour etre coherent avec erosion
C19 octobre 2021 correction pour ouvrir profils quand necessaire
C et ncmo pris au plus bas = min des Y des xncmo
C***********************************************************************
C
C TMAIL contient les abscisses des points de calcul ("centremailles")
C XTMAIL contient les abscisses des interfaces o� sont calcul�s les flux
C                        et o� est d�finie la g�om�trie ("intermailles")
C
C                TMAIL(I)               TMAIL(I+1)
C         ~P(I-1)   |       ~M(I) ~P(I)      |     ~M(I+1)
C        |---------U(I)----------|---------U(I+1)---------|
C        |                       |                        |
C   XTMAIL(I-1)             XTMAIL(I)                 XTMAIL(I+1)
C
C***********************************************************************
C
C Le code num�rique utilise un sch�ma de type Godounov et le solveur de
C Roe permettant de r�soudre le probl�me de Riemann approch� aux
C intermailles. Il d�termine les variables conservatives des �quations
C de Saint Venant 1D au temps TNP1 en fonction de celles calcul�es au
C temps TN aux centremailles et des flux num�riques FLS de section et
C FLQ de d�bit � l'amont et l'aval imm�diat des intermailles.
C Les variables calcul�es Y et Q sont suppos�es lin�aires autour de la
C moyenne situ�e au centremaille.
C
C***********************************************************************

      PROGRAM RUBARBE
      IMPLICIT NONE

      INTEGER LMAX,LNCMAX,NBHYPR,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,NBHYPR=100,CSMAX=10,NBMAX=150)
      INTEGER NTHMAX,NCLMAX,NOB1DMAX
      PARAMETER(NTHMAX=20000,NCLMAX=1000,NOB1DMAX=10)
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000)
      INTEGER LM(0:NBMAX),LL
      INTEGER IDPARS
      INTEGER METHODE,SOUSMETHODE,CHOIXC,DEPOT,CAPASOL,DEFOND

C Donn�es de base
C----------------
      CHARACTER ETUDEDD*20
      CHARACTER*1 IODEV,IOPDT,IOVISU,REP,TF(10)
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D
     &  ,PSAVE,TS,TIOPDT,FDEB1,FDEB2,FDEB3
      DOUBLE PRECISION TMPVISU
      INTEGER INDDEV(0:LMAX),NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
C        : ,ITSAV1
      LOGICAL DEBORD, STOCKAGE

C Variables g�n�rales
C EPSTETA: pr�cision sur TETA(J) (Kamal)
C--------------------
      DOUBLE PRECISION TN,TNP1,DTN
      DOUBLE PRECISION DDT,VE,VS,VOL
      DOUBLE PRECISION QMAX(LMAX),VMAX(LMAX),YMAX(LMAX)
     &  ,TQMAX(LMAX),TVMAX(LMAX),TYMAX(LMAX)
      DOUBLE PRECISION ZMAX(LMAX),ZFMAX(LMAX),ZFMIN(LMAX)
     &  ,TZMAX(LMAX),TZFMAX(LMAX),TZFMIN(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY,ECH
C        :,EPSTETA (mise dans la subroutine)
     & ,COEFC,COEFD,PUI,EPSS
      DOUBLE PRECISION VOLDEV(LMAX),VOLDE1(LMAX)
      DOUBLE PRECISION CRM,TSOR(NBHYPR),DELMAI(NBHYPR),DELIMAI(NBHYPR)
      DOUBLE PRECISION TFDEB1(LMAX),TFDEB2(LMAX),TFDEB3(LMAX)

C Variables aux centremailles, g�om�triques
C------------------------------------------
      INTEGER NC(0:LMAX),NCMO(0:LMAX)
      DOUBLE PRECISION TMAIL(LMAX),PEN(LMAX),CTDF(LMAX)
      DOUBLE PRECISION FR1(LMAX)
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)

C Variables aux centremailles, � TN
C----------------------------------
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)

C Variables aux centremailles, � TN+DTN/2
C----------------------------------------
      DOUBLE PRECISION SND(LMAX),YND(LMAX)

C Variables aux centremailles, � TNP1
C------------------------------------
      DOUBLE PRECISION QNP1(LMAX),SNP1(LMAX),VNP1(LMAX),YNP1(LMAX)
     & ,RHNP1(LMAX)

C Variables aux intermailles, g�om�triques
C-----------------------------------------
      INTEGER XNC(0:LMAX)
      INTEGER XNCMO(0:LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER XNCBINI(LMAX),XNCBDUR(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPECUM(LNCMAX)
     &  ,XPICUM(LNCMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XL(LMAX)
      DOUBLE PRECISION DZF(LMAX),DZF4(LMAX)
      DOUBLE PRECISION XZINI(LNCMAX),XZDUR(LNCMAX)
     &  ,XPLANI(LNCMAX),YPLANI(LNCMAX),COUR2G(LMAX)
     :, XSECMI(LNCMAX),SECMIN(LNCMAX)

C Variables aux intermailles, � TN
C---------------------------------
      DOUBLE PRECISION QM(LMAX),QP(LMAX),ZM(LMAX),ZP(LMAX)

C Variables aux intermailles, � TN+DTN/2
C---------------------------------------
      DOUBLE PRECISION SMD(LMAX),SPD(LMAX),VMD(LMAX),VPD(LMAX)
      DOUBLE PRECISION FLQ(LMAX),FLS(LMAX)

C Variables relatives aux conditions aux limites
C-----------------------------------------------
      INTEGER NT2(0:NBMAX),NT1(0:NBMAX),NT3(0:NBMAX)
      DOUBLE PRECISION QYH(NCLMAX),YH(NCLMAX)
      DOUBLE PRECISION TH(NTHMAX),QTH(NTHMAX),TA(NCLMAX),HIMP(NCLMAX)

C Variables relatives aux ouvrages
C---------------------------------
      CHARACTER*1 DDTYPOUV(nou1Dmax,noe1Dmax)
      INTEGER DDIA1(nou1Dmax),DDIA2(nou1Dmax),DDNOUV(nou1Dmax),DDNBOUV
      INTEGER DDNREFA(LMAX)
      INTEGER NMU(0:LMAX)
      DOUBLE PRECISION ZYD(LMAX),MU(LMAX),EXPOS(LMAX)
      DOUBLE PRECISION QMU(LMAX*20),TMU(LMAX*20)
      DOUBLE PRECISION DLAT(LMAX),SLAT(LMAX)
      DOUBLE PRECISION TSMU(LMAX*20),QSMU(LMAX*20)
      DOUBLE PRECISION DDQOUV(LMAX,2),DDHOUV(LMAX,2)
      DOUBLE PRECISION DDLONG(nou1Dmax,noe1Dmax)
     :,DDZDEV(nou1Dmax,noe1Dmax),DDHAUT(nou1Dmax,noe1Dmax)
     :,DDCOEF(nou1Dmax,noe1Dmax),DDVOLOUV(nou1Dmax)
     :  ,ZOUV(nou1Dmax,noe1Dmax),COEFIN(nou1Dmax,noe1Dmax)
     :  ,ZFERM(nou1Dmax,noe1Dmax)
      DOUBLE PRECISION QLAT(LMAX)
      DOUBLE PRECISION OUV(LMAX),LOND(LMAX),COEFA(LMAX),COEFB(LMAX)
     &  ,EFFPRI(LMAX)
      DOUBLE PRECISION DDQCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
     &  ,DDZCOUP(nou1Dmax*noe1Dmax*noe1Dmax),FRLM(LMAX)
         DOUBLE PRECISION FRII(LNCMAX)
      INTEGER DDNBCOU2(nou1Dmax,noe1Dmax),DDNBCOU1(nou1Dmax,noe1Dmax)

C Variables relatives aux s�diments
C----------------------------------
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),LACT(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX),COEFPROCHE
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF,ODCHAR
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION DCHARD,DCHARS
      INTEGER DEMIX
      INTEGER VARCONS
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
      INTEGER XNBCS(LNCMAX),NTHSAM(0:NBMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION THSAM(NCLMAX),QTHSAM(NCLMAX),DTHSAM(NCLMAX)
     &  ,STHSAM(NCLMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
     :  ,SENSI
      LOGICAL XMODSEC(LMAX),MODSEC
      LOGICAL BERGE
      DOUBLE PRECISION QSACT(LMAX)
      LOGICAL DEBUT1D
      INTEGER DEFORM(LNCMAX),JDEFMAX
      INTEGER PSOUE(LNCMAX),JSOUEMAX,NBB,COMPTEUR
      DOUBLE PRECISION CTDFTN(LMAX)
      LOGICAL HYDSAM(NBMAX)
      DOUBLE PRECISION OPTMAC,CDISPDEPO,CDISPEROS
          Integer methoderosion
      double precision puierosion,puidepot

C Variables relatives � une rupture progressive
C----------------------------------------------
c      INTEGER IT,NT
c      DOUBLE PRECISION NU
c      DOUBLE PRECISION KA,DT2
c      DOUBLE PRECISION ZC,ZPB,ZB,ZB0,Z0,ALP,ALC,RHO,PHI,DB0,DB,D50
c      DOUBLE PRECISION ETA,C1,C2,DBMAX,TRUP
c      DOUBLE PRECISION YM,SM,PM,RHM,ALM,ZFM
c      DOUBLE PRECISION QL(ntr1Dmax),QS(ntr1Dmax),R(ntr1Dmax),Z(ntr1Dmax)
c     &  ,ZAV(ntr1Dmax),TAU(ntr1Dmax)
c      LOGICAL KAPPA

      DOUBLE PRECISION DDZC(NOB1DMAX),ZPB(NOB1DMAX),DDZB(NOB1DMAX)
     :,DDZB0(NOB1DMAX),DDZ0(NOB1DMAX),DDALP(NOB1DMAX)
     :,DDALC(NOB1DMAX),DDRHO(NOB1DMAX),DDPHI(NOB1DMAX)
     :,DDDB0(NOB1DMAX),DDDB(NOB1DMAX),DDD50(NOB1DMAX)
     :,DDETA(NOB1DMAX),DDC1(NOB1DMAX),DDC2(NOB1DMAX)
     :,DBMAX(NOB1DMAX),TRUP(NOB1DMAX)
     :,DDKA(NOB1DMAX),DDDT2(NOB1DMAX)
     :,DDYM(NOB1DMAX),DDSM(NOB1DMAX),DDPM(NOB1DMAX)
     :,DDRHM(NOB1DMAX),DDALM(NOB1DMAX),DDNU(NOB1DMAX)
C     :,ZFM(NOB1DMAX)
     :,DDQL(ntr1Dmax,NOB1DMAX),DDQS(ntr1Dmax,NOB1DMAX)
     :,DDDBR(ntr1Dmax,NOB1DMAX)
     :,DDZ(ntr1Dmax,NOB1DMAX),DDZAV(ntr1Dmax,NOB1DMAX)
     :,DDZBR(ntr1Dmax,NOB1DMAX),DDTRECT(NOB1DMAX)
c     :,DBPREC(NOB1DMAX)

      INTEGER DDNT(NOB1DMAX),DDIT(NOB1DMAX),DDIOUB(nou1Dmax,noe1Dmax)
     :,DDNOB
      LOGICAL DDKAPPA(NOB1DMAX)
     :,DDELAP(NOB1DMAX)

      LOGICAL VOLQOUV

C variables pour le calcul du frottement au fond
      INTEGER OPTFPC
      DOUBLE PRECISION ROM1
C si vrai le frottement est defini point par point
         LOGICAL frotloc,frlfix,frotloccd

      DOUBLE PRECISION ALP1(NBMAX),ALP2(NBMAX),ALP3(NBMAX)
      INTEGER NCONF,CONFLU(NBMAX,3),NCONF2,NIB(NBMAX)
         LOGICAL RETOURARRIERE
         integer frplani(LNCMAX)
         double precision frlcl(LNCMAX),mfrloc
      DOUBLE PRECISION HII(LNCMAX),nbobst(LNCMAX),CoefDIi(LNCMAX)
     :,landaii(LNCMAX),landasurfII(LNCMAX),Hcl(LNCMAX),nbobstcl(LNCMAX)
     :,CoefDcl(LNCMAX),landacl(LNCMAX),landasurfcl(LNCMAX)
         DOUBLE PRECISION COEFA1(LMAX)

C variables pour fente de preisman
         logical iffente
         double precision dlfente
         integer ncfente(lmax),xncfente(lmax)

C***********************************************************************
C                          LISTE  DES  COMMONS
C***********************************************************************

C variables pour fente de preisman
         COMMON/IFFENTE/iffente
         COMMON/LFENTE/dlfente
         COMMON/NCXNCFENTE/ncfente,xncfente

C Variables contenues dans 'donnee.etude'
C----------------------------------------
      COMMON/NOMETU/ETUDEDD
      COMMON/TYPSAI/IDPARS
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
         COMMON/VES/VE,VS
C      COMMON/ITSAV1/ITSAV1 ! ajouter par kamal pour ecriture visu dans iterav
C debord = oui = utilisation formule debord
C stockage =oui : lit majeur au dessous berge basse lit mineur
C supprime de la geometrie
      COMMON/DEBORS/DEBORD,STOCKAGE


C Variables g�om�triques
C-----------------------
      COMMON/PHYS/LM,LL
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XL/XL
      COMMON/XGEOVISU/XNCBINI,XNCBDUR,XZINI,XZDUR
      COMMON/COURB/COUR2G

C Variables relatives au mod�le math�matique
C-------------------------------------------
      COMMON/FROTMT/FR1
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/HYDAV/YH,QYH
      COMMON/NHYDA/NT2
      COMMON/HYDAM/TH,QTH
      COMMON/NHYDM/NT1,NT3
      COMMON/HHAM/TA,HIMP

C Variables conservatives
C------------------------
      COMMON/MAILTN/SN,QN
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/XMALTD/SMD,SPD
      COMMON/MALTNP/SNP1,QNP1
      COMMON/DDFLUX/FLS,FLQ

C Variables non conservatives
C----------------------------
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/VITTD/VMD,VPD
      COMMON/VITTNP/VNP1,YNP1,RHNP1

C Variables de convergence
C-------------------------
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/TREEL/TN,DTN,TNP1

C Variables li�es aux s�diments / d�formation
C--------------------------------------------
      COMMON/CPROCHE/COEFPROCHE
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
         COMMON/ODCHAR/ODCHAR
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/XLACT/LACT
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/ROSRO/ROM1
      COMMON/DZF04/DZF,DZF4
      COMMON/LIQND/SND,YND
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/QSAM/THSAM,QTHSAM,DTHSAM,STHSAM
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/MODIFSEC/XMODSEC,MODSEC
      COMMON/SOUSMETHODE/SOUSMETHODE
      COMMON/CHOIXC/CHOIXC
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/METHODE/METHODE
      COMMON/DEPOT/DEPOT
      COMMON/CAPASOL/CAPASOL ! cette variable est pour le choix de la m�thode de caclul de la capacite solide
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/DEFOND/DEFOND   ! si on choisit de faire un calcul avec transport de sediment, on peut choisir apres de deformer ou pas le lit
      COMMON/DCHARSED/DCHARD,DCHARS
      COMMON/DEMIX/DEMIX
      COMMON/COEFC/COEFC ! cette variable sert a comparer la contrainte MPC avec la contrainte moyenne donn�e
                          ! par ROGRAVRHJ, si contrainte MPC sup�rieure � COEFC*ROGRAVRHJ, alors
                          ! contrainte MPC=COEFC*ROGRAVRHJ
                          ! valeur de COEFC=2; fix� dans "INITIALISATIONS, PREPARATION DU CALCUL"

      COMMON/COEFD/COEFD ! pour un d�pot en fonction de (1/contrainte)^(COEFD)
                         ! valeur de COEFD=1; fix� dans "INITIALISATIONS, PREPARATION DU CALCUL"
C  la d�formation peut �tre calcul�e en fonction de (TO-TC)^(PUI)
C                     ! la puissance depend de la formule de capacit� solide, PUI=1.5 si MPM ou engluend
C                     ! = 1 si bagnold
      COMMON/PUI/PUI
      COMMON/EROSION/methoderosion
      COMMON/perosion/puierosion
      common/pdepot/puidepot
C  utiliser dans erosec et depsec pour taux participation Sparticip
      COMMON/EPSS/EPSS
      COMMON/VARCONS/VARCONS
      COMMON/EBERGE/BERGE
C debit solide actif initial
      COMMON/QSACTI/QSACT
C nes eert que dans schema pour initialisation masse active
      COMMON/DEBUTS/DEBUT1D
C debit solide en amont (equivaut a condam non nul)
      COMMON/HSA/HYDSAM
      COMMON/NSAM/NTHSAM
C OPTMAC est le multiplicateur du D84 pour epaisseur couche active
      COMMON/OPTMAC/optMAC
C cdispdepo est le multiplicateur du D84 pour epaisseur couche superieure
C melangee avec la suivante lors d'un depot
      common/cdispdepo/cdispdepo
C cdisperos est le multiplicateur du D16 pour epaisseur couche superieure
C infiltree dans couche de dessous lors d'une erosion
      common/cdisperos/cdisperos

C Autres variables
C-----------------
      common/volqouv/volqouv
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/PARMAX/YMAX,VMAX,QMAX,TYMAX,TVMAX,TQMAX
      COMMON/PARSMAX/ZMAX,ZFMAX,ZFMIN,TZMAX,TZFMAX,TZFMIN
      COMMON/TFDEB/TFDEB1,TFDEB2,TFDEB3
      COMMON/TSTFIL/TF
      COMMON/TMPVISUC/TMPVISU
      COMMON/TQMU/TMU,QMU
      COMMON/NTMU/NMU
      COMMON/QTSM/QSMU,TSMU
      COMMON/DDNARF/DDNREFA
      COMMON/FROT2/FRLM
         COMMON/FRI/FRII
      COMMON/DDHQOUV/DDHOUV,DDQOUV
      COMMON/DDIOUVRA/DDIA1,DDIA2,DDNOUV,DDNBOUV
      COMMON/DDCOUVRA/DDLONG,DDZDEV,DDHAUT,DDCOEF
      COMMON/DDTOUVRA/DDTYPOUV
      COMMON/DDVOLOU/DDVOLOUV
      COMMON/DDNOUVRA/DDNBCOU1,DDNBCOU2
      COMMON/DDZOUVRA/DDQCOUP,DDZCOUP
      COMMON/YOUVRA/COEFIN,ZOUV,ZFERM
c variables rupture progressive
      COMMON/DDDIGUE/DDZC,ZPB,DDALP,DDALC,DDZ0,DDD50,
     :DDRHO,DDPHI,DDDB0,DDZB0
      COMMON/DDMOYEN/DDYM,DDSM,DDPM,DDRHM,DDALM,DDNU
      COMMON/DDCONRUP/DDETA,DDKA,DDC1,DDC2,DDDT2
      COMMON/DDNCONST/DDNT
      COMMON/DDBRECHE/DDZB,DDDB,DDIT
      COMMON/DDLAG/DDKAPPA
      COMMON/DDMAXBRE/DBMAX,DDTRECT
      COMMON/DDRESUL/DDQL,DDQS,DDDBR,DDZ,DDZAV,DDZBR
      COMMON/DDTINIB/TRUP
      COMMON/DDELAPPR/DDELAP
      COMMON/DDIOBREC/DDIOUB,DDNOB

C QLAT debit evacue par les prises
      COMMON/QL/QLAT
      COMMON/PRISE/OUV,LOND,COEFA,COEFB,EFFPRI
      COMMON/DSLAT/DLAT,SLAT
      COMMON/SENSM/SENSI
      COMMON/CTDFTN/CTDFTN
      COMMON/SPMM/XSECMI,SECMIN
      COMMON/FRMLPC/OptFPC
C 9)VARIABLES NBIEFS
C----------------------------------
      COMMON/ALP/ALP1,ALP2,ALP3
      COMMON/NBIEF/NBB
      COMMON/NCONFL/NCONF,NCONF2,CONFLU
         common/NIBIEF/nib

         common/RTARRIERE/RETOURARRIERE
         common/XYPLANI/XPLANI,YPLANI
C calcul de frottement point par point
         common/frl/frotloc,frlfix,frotloccd
         common/frlcl/frlcl
         common/frplani/frplani
         common/mfrloc/mfrloc
         Common/cdII/HII,nbobst,CoefDIi,landaii,landasurfII
         Common/cdcl/Hcl,nbobstcl,CoefDcl,landacl,landasurfcl

C coefa1: variable A de la formule de debord mis en common le 10/7/13
         COMMON/COEFA12/COEFA1

C 16/09/10 pour initialiser flq a 0        pour conflue
         data flq/lmax*0./
C initialisation a1 des coefficients debord
         data coefa1/lmax*1./
C fente de preisman
          IFFENTE=.FALSE.
C lecture dlfente dans le fichier donnee
C          DLFENTE=0.015
COU2DC COU2D a supprimer si couplage avec Rubar20
C on est au temps tn=tinit1D
      CALL INIT1D(VOL,compteur)
COU2DC initialisation du 2D        : on est au temps tm=tinit
COU2D             CALL INIT
COU2DC initialisation du couplage
COU2D        call initcouplage
      RETOURARRIERE=.FALSE.
C trois phases 1D seul puis 1D/2D puis 1D seul
C debut des iterations en temps : 1D seul
COU2D        call tran2Den1Ddemi0
COU2D      DO WHILE(TN.LT.tinit)
COU2D        call calcul1Ddemi(tinit)
COU2DC traitement conditions  limites de type 6 si pas de couplage
COU2D        if(condlim6)then
COU2D          call cl6sanscouplage
COU2D           endif
COU2D        call itera1D
COU2D        CALL calcul1Dfin(compteur)
C Fin des it�rations en temps 1D seul
C----------------------------
COU2D      ENDDO
C debut des iterations en temps         couplage
COU2D      DO WHILE(TN.LT.tmax)
COU2DC ramene le pas de temps du 2D au pas de temps 1D
COU2D        call compdt
COU2DC       call tran2Den1D
COU2D        call calcul1Ddemi(tmax)
COU2DC transforme ymd, etc en hcouplage affecte � hg1 du 2D
COU2D        call intermail
COU2DC on lance de tn=tm a tnp1 le 2D
COU2D        call calcdt(tnp1)
COU2D        call tran2Den1Ddemi
COU2D        call itera1D
COU2D        CALL calcul1Dfin(compteur)
COU2DC sorties pour le couplage
COU2DC        call sortiecouplage
C Fin des it�rations en temps couplage
C----------------------------
COU2D      ENDDO
COU2DC sorties du 2D
COU2D                CALL FINAL
COU2DC        CALL FINALCOUPLAGE
C debut des iterations en temps         1D seul final
COU2D        call tran2Den1Ddemi0
      DO WHILE(TN.LT.tmax1D)
        call calcul1Ddemi(tmax1D)
COU2DC traitement conditions  limites de tyep 6 si pas de couplage
COU2D        if(condlim6)then
COU2D          call cl6sanscouplage
COU2D           endif
        call itera1D
COU1DC le retour en arriere est deconnecte en cas de couplage
COU1DC cou1D a supprimer si pas de couplage avec le 2D
COU1D        IF(.NOT.RETOURARRIERE)THEN
          CALL calcul1Dfin(compteur)
COU1D        endif
C Fin des it�rations en temps final 1D seul
C----------------------------
      ENDDO
C FIN DU TRAITEMENT
C-----------------------------------------------------------------------
      CALL FINAL1D(VOL)
      END
C*******************************************************************
C initialisation variables
C***********************************************************************
      subroutine init1D(VOL,compteur)
C***********************************************************************

      IMPLICIT NONE

      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,NBB
      INTEGER J,IDPARS,COMPTEUR

C Donn�es de base
C----------------
      CHARACTER ETUDEDD*20
      CHARACTER*1 IOPDT,IOVISU,REP
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D
     &  ,PSAVE,TIOPDT
      LOGICAL TRASED,CDCHAR,CGEOM

C Variables g�n�rales
C EPSTETA: pr�cision sur TETA(J) (Kamal)
C--------------------
      DOUBLE PRECISION TN,TNP1,DTN
      DOUBLE PRECISION DDT,DTM
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY,ECH
     :,COEFC,COEFD,EPSS
     :,POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION VOL,VE,VS
      DOUBLE PRECISION CRM

C Variables aux centremailles, g�om�triques
C------------------------------------------
C Variables relatives aux ouvrages
C---------------------------------
      DOUBLE PRECISION DDQOUV(LMAX,2),DDHOUV(LMAX,2)

C Variables relatives aux s�diments
C----------------------------------
      LOGICAL DEBUT1D

C Variables relatives � une rupture progressive
C----------------------------------------------

C variables pour le calcul du frottement au fond
      DOUBLE PRECISION DM,SEGMA


C***********************************************************************
C                          LISTE  DES  COMMONS
C***********************************************************************

C Variables contenues dans 'donnee.etude'
C----------------------------------------
      COMMON/NOMETU/ETUDEDD
      COMMON/TYPSAI/IDPARS
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
c      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
         COMMON/VES/VE,VS


C Variables g�om�triques
C-----------------------
      COMMON/PHYS/LM,LL

C Variables de convergence
C-------------------------
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM

C Variables li�es aux s�diments / d�formation
C--------------------------------------------
      COMMON/DEBUTS/DEBUT1D
C  utiliser dans erosec et depsec pour taux participation Sparticip
      COMMON/COEFC/COEFC
      COMMON/COEFD/COEFD
      COMMON/EPSS/EPSS
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
C nes eert que dans schema pour initialisation masse active
      COMMON/DDHQOUV/DDHOUV,DDQOUV
      COMMON/NBIEF/NBB


C***********************************************************************
C                            DEBUT DU CALCUL
C***********************************************************************
      WRITE(*,*)
      WRITE(*,*)
      WRITE(*,*)
      WRITE(*,*)
      WRITE(*,*)'##################################################'
      WRITE(*,*)'#                                                #'
ctrs      WRITE(*,*)'#                    RubarBE                     #'
ctrs      WRITE(*,*)'#          Simulation d''ecoulements 1D           #'
ctrs      WRITE(*,*)'#                avec fond mobile                #'
cliq      WRITE(*,*)'#                    Rubar 3                     #'
cliq      WRITE(*,*)'#          Simulation d''ecoulements 1D           #'
cliq      WRITE(*,*)'#                                                #'
      WRITE(*,*)'#                                                #'
      WRITE(*,*)'#         INRAE    19  octobre  2021             #'
      WRITE(*,*)'#                                                #'
      WRITE(*,*)'##################################################'
      WRITE(*,*)
      call getarg(1,ETUDEDD)
      if (trim(ETUDEDD) == '') then
        WRITE(*,'(A)')'         ENTREZ LE NOM DE L ETUDE :'
        READ(*,'(A20)') ETUDEDD
      end if
      WRITE(*,*)

C INITIALISATIONS, PREPARATION DU CALCUL
C-----------------------------------------------------------------------
c coefficient de chezy minimal
      CHEZY=1.
c zero remplacant le zero machine
      EPS=0.00000000001
c precision sur les hauteurs
C utilise aussi pour les vitesses
      EPSY=0.000001
c precision sur angle des berges/fond (mise dans la subroutine)
C      EPSTETA=0.00001
c precision sur masse de sediments
      EPSM=0.00000001
c gravite
      GRAV=9.81
c masse volumique eau
      RO=1000.
c coefficient pour limitation de la contrainte MPC
      COEFC=2.
c coefficient pour calcul depot en 1/contrainte
      COEFD=1.
c coefficient pour calcul Sparticip dans erosec et depsec
C utilise aussi pour JRH
      EPSS=0.00000001
c ajouer par kamal mais dans initsauv
C supprime car on peut utiliser un seul itsav
C      ITSAV1=1
C cree en meme temps que subroutine inimact
      DEBUT1D=.TRUE.

C parametres des confluences
        CALL LECONF

C R�cup�ration des param�tres et initialisations
C-----------------------------------------------
      CALL INIT1(DM,SEGMA)
      CALL MAIL
C 32)LECTURE FROTTEMENT
C-------------------------------------------------------------

        CALL INIGEO


C 33) RECUPERATION geometrie et initialisation ouvrages :
C---------------------------------------------
        CALL LECGEOM
C initialisation hauteurs et debits ouvrages
        DO 7203 J=1,LM(NBB)
          DDHOUV(J,1)=0.
          DDHOUV(J,2)=0.
          DDQOUV(J,1)=0.
          DDQOUV(J,2)=0.
 7203   CONTINUE

C 33) RECUPERATION DES C.L ET DES DEVERSEMENTS :
C---------------------------------------------

        CALL INIT2
C        CLOSE(31)

        CALL LOUVR1D



C      CALL INIT3


C Lecture et pr�paration des conditions initiales
C------------------------------------------------
C      WRITE(*,*)'conditions initiales'
C      pause
      CALL CONDIN
      CALL INITS(DM,SEGMA)
C initialisation frottement local
         CALL inifrii
C recalcul frottement a partir frottement local
C mis dans SP godroe uniquement (apres calcul yinter)
C         if(frotloc)then
c                      call calcfr
C               endif
C        CALL EGAL
C calcule ordre bief pour SP canoge
      if(trased)then
        call ordrbief
         endif
      TN=tinit1D
      DTN=DT1D

C Pr�paration des donn�es et fichiers de sauvegarde
C--------------------------------------------------
      CALL INIENV
      CALL INITSAUV
C      TMPVISU=tinit1D
      IF(DTSAUV.EQ.0.) DTSAUV=EPS
      IF(PSAVE.EQ.0.) PSAVE=EPS

C Condition de Courant Friedrich Levy
C---------------------------------------
      CALL TESTCR
      DTM=DTN*CFL1D/CRM
      IF(IOPDT.EQ.'N')THEN
        IF(DT1D.LT.DTM)THEN
          DDT=0.
          DTN=DT1D
        ELSE
          DTN=DTM
          DDT=DTM
        ENDIF
      ELSEIF(IOPDT.EQ.'M'.AND.TN.LT.TIOPDT)THEN
        DTN=MIN(DT1D,DTM)
      ELSE
        DTN=DTM
      ENDIF
      CRM=CFL1D*DTN/DTM

C Calcul du volume initial
C-------------------------
      CALL VOLMOD(VOL)
      WRITE(*,*)
      WRITE(*,*)'Le volume initial de fluide dans le modele'
     &  ,' est de ',VOL,' m3'
      WRITE(*,*)


C ITERATIONS EN TEMPS
C-----------------------------------------------------------------------
      VE=0.
      VS=0.
      compteur=1
      RETURN
      END

C**********************************************************************
C retour apres validation du dt
      subroutine calcul1Dfin (compteur)
C*************************************************************************
      IMPLICIT NONE

      INTEGER LMAX,NBMAX,NBHYPR
      PARAMETER(LMAX=3000,NBHYPR=100,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,NBB
      INTEGER M,IDPARS,COMPTEUR,I

C Donn�es de base
C----------------
      CHARACTER*1 IOPDT,IOVISU,TF(10)
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D
     &  ,PSAVE,TS,TIOPDT,FDEB1,FDEB2,FDEB3
      DOUBLE PRECISION CRM,TSOR(NBHYPR),DELMAI(NBHYPR),DELIMAI(NBHYPR)
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
C        : ,ITSAV1

C Variables g�n�rales
C EPSTETA: pr�cision sur TETA(J) (Kamal)
C--------------------
      DOUBLE PRECISION TN,TNP1,DTN
      DOUBLE PRECISION DDT,DTA
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY,ECH
      DOUBLE PRECISION VE,VS

C Variables aux centremailles, g�om�triques
C------------------------------------------
      DOUBLE PRECISION FLQ(LMAX),FLS(LMAX)

      DOUBLE PRECISION VOLDEV(LMAX),VOLDE1(LMAX),EXPOS(LMAX)
     &  ,MU(LMAX),ZYD(LMAX)

C***********************************************************************
C                          LISTE  DES  COMMONS
C***********************************************************************

C Variables contenues dans 'donnee.etude'
C----------------------------------------
      COMMON/TYPSAI/IDPARS
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
C      COMMON/ITSAV1/ITSAV1 ! ajouter par kamal pour ecriture visu dans iterav
         COMMON/VES/VE,VS


C Variables g�om�triques
C-----------------------
      COMMON/PHYS/LM,LL
      COMMON/DDFLUX/FLS,FLQ

C Variables de convergence
C-------------------------
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/TSTFIL/TF
      COMMON/NBIEF/NBB

      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1

C Calcul des variables a Tn+1
C----------------------------
C        CALL ITERAV
C Volumes d�vers�s
      DO I=1,LM(NBB)
        VOLDEV(I)=VOLDE1(I)
      ENDDO
C on introduit dta parce que dtn a deja ete change
        DTA=TNP1-TN
        DO 8001 M=1,NBB
c          VE=VE+FLS(LM(M-1)+1)*DTN
          VE=VE+FLS(LM(M-1)+1)*DTA
8001    CONTINUE
        DO 8002 M=1,NBB
          VS=VS+FLS(LM(M)-1)*DTA
c          VS=VS+FLS(LM(M)-1)*DTN
 8002   CONTINUE
C        VE=VE+FLS(1)*DTA
C        VS=VS+FLS(LL)*DTA
C               write(92,*)'tn=',tn,' dtn=',dta,dtn
c         DO j=2,LM(1)
c          DX=XTMAIL(J)-XTMAIL(J-1)
c      write(92,*)'j=',j,sn(j),snp1(j),sn(j)-dta*(fls(j)-fls(j-1))/dx
c                write(92,*)'fls=',j,fls(j),fls(j-1),dta,dx
c               enddo
C �galisation des variables de Tn+1 � Tn
C---------------------------------------
        CALL SAVEVISU(TN)

        TN=TNP1
        if(compteur.eq.100)then
          compteur=0
        else
          compteur=compteur+1
        endif
        if(compteur.eq.0)then
          write(*,*)'temps =',tn
          compteur=compteur+1
        endif

c      print*,' entree egal apres iterav'
        CALL EGAL
c      print*,' sortie egal'
C         CALL VOLMOD(VOL2)
C      write(92,*)'vol=',vol+ve-vs,' ve= ',ve,' vs=',vs,'vol2',vol2


C Recherche des enveloppes et des fronts de d�bits
C-------------------------------------------------
        CALL RECENV
        CALL RECFRONT

C Sortie des fichiers de r�sultats
C---------------------------------
c        print*,' entree visu'
        CALL SAVEFH(DTA)
        IF(INT(TN/DTSAUV).GT.INT((TN-DTA)/DTSAUV))THEN
          CALL SORTIDS(TN)
          CALL ECRENV
        ENDIF

C Sauvegarde des variables pour une reprise �ventuelle
C-----------------------------------------------------
        IF(TF(1).EQ.'O')THEN
          IF((TS.NE.tinit1D).AND.(TS.GE.(TN-DTA)).AND.(TS.LE.TN))THEN
            CALL SAUVRE(TS)
                  call ecrenv
          ENDIF
        ENDIF

        RETURN
        END

C**********************************************************************
C     ecriture en fin de calcul
      SUBROUTINE FINAL1D(VOL)
C**********************************************************************
      IMPLICIT NONE

      INTEGER LMAX,NBMAX,NBHYPR
      PARAMETER(LMAX=3000,NBHYPR=100,NBMAX=150)
      INTEGER NOB1DMAX
      PARAMETER(NOB1DMAX=10)
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000)
      INTEGER LM(0:NBMAX),LL,NBB
      INTEGER I,J,K,L,N,IDPARS,IOUV

C Donn�es de base
C----------------
      CHARACTER ETUDEDD*20,NOMFIC*40
      CHARACTER*1 REP,IODEV,IOPDT,IOVISU
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D
     &  ,PSAVE,TS,TIOPDT,FDEB1,FDEB2,FDEB3
      INTEGER INDDEV(0:LMAX)
C        ,NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)

C Variables g�n�rales
C EPSTETA: pr�cision sur TETA(J) (Kamal)
C--------------------
      DOUBLE PRECISION TN,TNP1,DTN
      DOUBLE PRECISION DDT
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY,ECH
      DOUBLE PRECISION VOLENL(LMAX)
      DOUBLE PRECISION ZYD(LMAX),MU(LMAX),EXPOS(LMAX),VOLDEV(LMAX)
     & ,VOLDE1(LMAX)
      DOUBLE PRECISION VOL,VOLF,VE,VS
      DOUBLE PRECISION CRM,TSOR(NBHYPR),DELMAI(NBHYPR),DELIMAI(NBHYPR)

C Variables aux centremailles, g�om�triques
C------------------------------------------

C Variables relatives aux ouvrages
C---------------------------------
      INTEGER DDIA1(nou1Dmax),DDIA2(nou1Dmax),DDNOUV(nou1Dmax),DDNBOUV
      DOUBLE PRECISION DDVOLOUV(nou1Dmax)

C Variables relatives aux s�diments
C----------------------------------

      INTEGER DDNT(NOB1DMAX),DDIOUB(nou1Dmax,noe1Dmax)
     :,DDNOB,IBOUV
      DOUBLE PRECISION DDETA(NOB1DMAX),DDC1(NOB1DMAX),DDC2(NOB1DMAX)
     :,DDKA(NOB1DMAX),DDDT2(NOB1DMAX),TRUP(NOB1DMAX)



C***********************************************************************
C                          LISTE  DES  COMMONS
C***********************************************************************

C Variables contenues dans 'donnee.etude'
C----------------------------------------
      COMMON/NOMETU/ETUDEDD
      COMMON/TYPSAI/IDPARS
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
c      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
C      COMMON/ITSAV1/ITSAV1 ! ajouter par kamal pour ecriture visu dans iterav
         COMMON/VES/VE,VS


C Variables g�om�triques
C-----------------------
      COMMON/PHYS/LM,LL
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV

C Variables de convergence
C-------------------------
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/TREEL/TN,DTN,TNP1


      COMMON/DDIOUVRA/DDIA1,DDIA2,DDNOUV,DDNBOUV
      COMMON/DDVOLOU/DDVOLOUV
      COMMON/DDNCONST/DDNT
      COMMON/DDIOBREC/DDIOUB,DDNOB
      COMMON/DDCONRUP/DDETA,DDKA,DDC1,DDC2,DDDT2
      COMMON/DDTINIB/TRUP
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1

      COMMON/NBIEF/NBB

C FIN DU TRAITEMENT
C-----------------------------------------------------------------------
c      print*,' avant fin de traitement'
      CALL SORTIDS(tmax1D)
         CALL ECRENV
C         call savevisu(tmax1D)
c      print*,' apres sortids tmax1D'
      CALL FINSAUV
c      print*,' apres fermeture fichier sorties'

C Volumes d�vers�s
C-----------------
      IF(IODEV.EQ.'O')THEN
C volenl volume deverse par zone
          DO J=1,LM(NBB)
            VOLENL(J)=0.
          ENDDO
        IF(REP.EQ.'O'.OR.REP.EQ.'o')THEN
          NOMFIC='voldev.'//ETUDEDD
          OPEN(55,FILE=NOMFIC,STATUS='OLD',ERR=239)
                write(*,*)'volumes deverses precedents lus'
          DO J=1,LM(NBB)
            READ(55,*,END=239)K,VOLENL(J)
            VOL=VOL+VOLENL(J)
          ENDDO
          close (55)
        ENDIF
  239   NOMFIC='voldev.'//ETUDEDD
        OPEN(55,FILE=NOMFIC,STATUS='UNKNOWN')
C        ENDIF

         K=0
C 239    K=0
        J=1
C 234    CONTINUE
        DO WHILE(INDDEV(K+1).LT.LM(NBB)+1)
c modifi� par kamal janvier 2005
c      IF(INDDEV(K+1).EQ.LM+1)THEN
          K=K+1
          L=INDDEV(K)
          DO N=J,L
            VOLENL(K)=VOLENL(K)+VOLDEV(N)
          ENDDO
          J=INDDEV(K)+1
C          K=K+1
c          GOTO 234
c        ENDIF
        ENDDO
C235     WRITE(*,*)'Volumes liquides totaux deverses:'
        WRITE(*,*)'Volumes liquides totaux deverses:'
        DO I=1,K
          IF(I.EQ.1)THEN
            WRITE(*,*)I,'ier volume:',VOLENL(I)
          ELSE
            WRITE(*,*)I,'ieme volume:',VOLENL(I)
          ENDIF
          WRITE(55,*)I,VOLENL(I)
        ENDDO
        CLOSE(55)
      ENDIF

C Volume dans le mod�le
C----------------------
      CALL VOLMOD(VOLF)
      WRITE(*,*)
      WRITE(*,*)'Le volume final est de ',VOLF,' m3'
      IF(IODEV.EQ.'O')THEN
        DO I=1,K
          VOL=VOL-VOLENL(I)
        ENDDO
COU2D      ELSE
COU2D        DO I=1,LM(NBB)
COU2D          VOL=VOL-VOLDEV(I)
COU2D        ENDDO
      ENDIF
C Volume des ouvrages sortants
      DO IOUV=1,DDNBOUV
        IF(DDIA2(IOUV).EQ.0)THEN
          VOL=VOL-DDVOLOUV(IOUV)
        ENDIF
      ENDDO
      VOL=VOL+VE-VS
      WRITE(*,*)'Le volume calcule est de ',VOL,' m3'
      WRITE(*,*)'erreur conservation volume ',
     :abs(Vol-volf)/(100.*max(ve,vs,volf)),' %'

C �criture r�sultats rupture progressive
C---------------------------------------
        DO IBOUV=1,DDNOB
          I=INT((TNP1-TRUP(IBOUV))/DDDT2(IBOUV))+1
C pour limiter ecriture resultats aux valeurs enregistrees
          IF(I.LT.DDNT(IBOUV))DDNT(IBOUV)=I
          CALL IMPRIB(IBOUV)
        ENDDO
        RETURN
        END

C***********************************************************************
C                      LISTE  DES  SOUS-PROGRAMMES
C***********************************************************************

C-----------------------------------------------------------------------
      SUBROUTINE INIT1(DM,SEGMA)
C-----------------------------------------------------------------------
C R�cup�re les param�tres du fichier 'donnee.etude'
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,NBHYPR,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,NBHYPR=100,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
     &  ,ITSAV,NBMAIL(NBHYPR),INDDEV(0:LMAX),NBSSAV,NTSOR,NBB,I
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION CFL1D,CRM,DDT
      DOUBLE PRECISION TIOPDT,ECH
      CHARACTER*50 B
      CHARACTER ETUDE*20,NOMFIC*40
      CHARACTER IODEV*1,IOPDT*1,IOVISU*1,REP*1,TF(10)*1
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER OPTFPC
      DOUBLE PRECISION ROS,ROM1,DM,SEGMA
      CHARACTER*30 nomoptionf(0:7),options*3,iodebord*1,iostockage*1
      LOGiCAL DEBORD,STOCKAGE
C chaincfl : chaine de carct�res contenant le cflmax et la largeur de fente
C de preismann dlfente
       CHARACTER*74 chaincfl
       DOUBLE PRECISION DLFENTE

      COMMON/LFENTE/dlfente
      COMMON/NOMETU/ETUDE
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PHYS/LM,LL
      COMMON/TSTFIL/TF
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/ROSRO/ROM1
      COMMON/FRMLPC/OptFPC
      COMMON/NBIEF/NBB
      COMMON/DEBORS/DEBORD,STOCKAGE

      NOMOPTIONf(0)='Strickler utilisateur'
      NOMOPTIONf(1)='Brownlie 1983'
      NOMOPTIONf(2)='Griffith 1981 lit mobile'
      NOMOPTIONf(3)='Karim 1995'
      NOMOPTIONf(4)='Wu et Wang 1999'
      NOMOPTIONf(5)='Yu et Lim 2003'
      NOMOPTIONf(6)='Recking 2008'
      NOMOPTIONf(7)='Garde et Raju 1966'
         NOMFIC='donnee.'//ETUDE
      OPEN(20,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=25)

C==================================
C    PARAMETRES DE CALCUL
C==================================
C CFL1D = CONDITION DE COURANT
C CONDAM = CONDITION LIMITE AMONT
C CONDAV = CONDITION LIMITE AVAL
C----------------------------------
        READ(20,'(A74)')CHAINCFL
        READ(chaincfl(51:62),'(E12.5)')CFL1D
        READ(chaincfl(63:74),'(E12.5)')dlfente
        If (dlfente.lt.eps)then
          dlfente=0.015
        else
          write(*,*)'largeur fente de Preismann ',dlfente
        endif
        READ(20,101)B,(CONDAM(I),I=1,NBB)
        READ(20,101)B,(CONDAV(I),I=1,NBB)
        READ(20,101)B,(REGIME(I),I=1,NBB)
C        READ(20,101)B,CONDAM
        DO I=1,NBB
        IF(
COU2D     :CONDAM(I).GT.6.OR.CONDAM(I).EQ.5.OR.
COU1D     :CONDAM(I).GT.4.OR.
     :CONDAM(I).LT.0.OR.CONDAM(I).EQ.3)THEN
          WRITE(*,*)'BIEF',I,
     :' ERREUR DANS LE TYPE DE CONDITION LIMITE AMONT'
          STOP
        ENDIF
C        READ(20,101)B,CONDAV
        IF(CONDAV(I).GT.7.OR.CONDAV(I).LT.0
COU1D     :.OR.CONDAV(I).EQ.6
     :)THEN
          WRITE(*,*)'BIEF',I,
     :'ERREUR DANS LE TYPE DE CONDITION LIMITE AVAL'
          STOP
        ENDIF
              ENDDO
C        READ(20,101)B,REGIME
C verification des confluneces si NBB>1
        if(nbb.gt.1)then
                   call verifconfluence
                endif
C==================================
C    OPTIONS DE CALCUL
C==================================
C IODEV = DEVERSEMENT LATERAL
C IOPDT = PAS DE TEMPS AUTOMATIQUE
C VISU EN TEMPS REEL
C----------------------------------
        READ(20,103)B,options
                READ(OPTIONS(1:1),'(A1)')IODEV
        IF(IODEV.EQ.'O'.OR.IODEV.EQ.'o')THEN
          IODEV='O'
        ELSE IF(IODEV.EQ.'N'.OR.IODEV.EQ.'n')THEN
          IODEV='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION DEVERSEMENT(IODEV)'
        ENDIF
                READ(OPTIONS(2:2),'(A1)')IODEBORD
        IF(IODEBORD.EQ.'O'.OR.IODEbord.EQ.'o')THEN
          DEBORD=.TRUE.
        ELSE IF(IODEBORD.EQ.'N'.OR.IODEBORD.EQ.'n')THEN
          DEBORD=.FALSE.
          WRITE(*,*)'FORMULE DEBORD NON APPLIQUEE'
        ELSE
          DEBORD=.TRUE.
        ENDIF
                READ(OPTIONS(3:3),'(A1)')IOstockage
        IF(IOstockage.EQ.'O'.OR.IOstockage.EQ.'o')THEN
          STOCKAGE=.TRUE.
        ELSE IF(IOSTOCKAGE.EQ.'N'.OR.IOSTOCKAGE.EQ.'n')THEN
          STOCKAGE=.FALSE.
          WRITE(*,*)'geometrie lit majeur sans stockage'
        ELSE
          STOCKAGE=.TRUE.
        ENDIF
        READ(20,102)B,IOPDT
        IF(IOPDT.EQ.'O'.OR.IOPDT.EQ.'o')THEN
          IOPDT='O'
        ELSE IF(IOPDT.EQ.'N'.OR.IOPDT.EQ.'n')THEN
          IOPDT='N'
        ELSE IF(IOPDT.EQ.'M'.OR.IOPDT.EQ.'m')THEN
          IOPDT='M'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION PAS DE TEMPS(IOPDT)'
        ENDIF
C remplacement de l'option visualisation par l'option fichier largeur
        READ(20,102) B,TF(7)
        IF(TF(7).EQ.'O'.OR.TF(7).EQ.'o')THEN
          TF(7)='O'
        ELSE IF(TF(7).EQ.'N'.OR.TF(7).EQ.'n')THEN
          TF(7)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER LARGEUR'
        ENDIF
C        READ(20,102)B,IOVISU
C        IF(IOVISU.EQ.'O'.OR.IOVISU.EQ.'o')THEN
C          IOVISU='O'
C        ELSE IF(IOVISU.EQ.'N'.OR.IOVISU.EQ.'n')THEN
          IOVISU='N'
C        ELSE
C          WRITE(*,*)'ERREUR DANS L''OPTION VISUALISATION(IOVISU)'
C        ENDIF

C==================================
C    PARAMETRES TEMPORELS
C==================================
C REP = REPRISE OU NON DU PROGRAMME
C tinit1D = TEMPS DEBUT DE CALCUL
C tmax1D = TEMPS DE CALCUL
C TIOPDT = TEMPS OU DT=CONSTANT
C DT = PAS DE TEMPS INITIAL
C----------------------------------
        READ(20,102) B,REP
        READ(20,100) B,tinit1D
c        IF (((REP.EQ.'N').OR.(REP.EQ.'n')).AND.(tinit1D.NE.0.))
c     +  THEN
c       WRITE(*,*)'PAS DE REPRISE : LE TEMPS INITIAL DOIT ETRE NUL'
c          WRITE(*,*)'CORRIGEZ LE FICHIER DONNEE'
c          STOP
c        ENDIF
        READ(20,100) B,tmax1D
        IF ((tmax1D-tinit1D).LT.0.001) THEN
          WRITE(*,*)'DUREE DE CALCUL TROP PETITE'
          WRITE(*,*)'CORRIGEZ LE FICHIER DONNEE'
          STOP
        ENDIF
        READ(20,100) B,TIOPDT
        READ(20,100) B,DT1D

        IF(IOPDT.EQ.'M') THEN
          IF((TIOPDT.LE.tinit1D).OR.(TIOPDT.GE.tmax1D)) THEN
            WRITE(*,*) '********* ATTENTION **********'
            WRITE(*,*) 'LE TEMPS OU DT CONSTANT NE CONVIENT PAS'
          ENDIF
        ENDIF

C============================================================
C       SORTIES
C============================================================
C TS = TEMPS DE SAUVEGARDE
C PSAVE=PAS DE TEMPS DE SAUVEGARDE DES FRONTS ET HYDROGRAMMES
C DTSAUV = PAS DE TEMPS DE SAUVEGARDE DES LIGNES D'EAU
C FDEB1,FDEB2,FDEB3 = FRONTS DES DEBITS
C------------------------------------------------------------
        READ(20,100) B,TS
        READ(20,100) B,DTSAUV
        READ(20,100) B,PSAVE
              IF(PSAVE.LT.EPS)PSAVE=EPS
        READ(20,100) B,FDEB1
        READ(20,100) B,FDEB2
        READ(20,100) B,FDEB3

C=======================================================
C       INDICATEURS DE CREATION DE FICHIERS DE RESULTATS
C=======================================================

        READ(20,102) B,TF(1)
        IF(TF(1).EQ.'O'.OR.TF(1).EQ.'o')THEN
          TF(1)='O'
        ELSE IF(TF(1).EQ.'N'.OR.TF(1).EQ.'n')THEN
          TF(1)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER TPS'
        ENDIF
        READ(20,102) B,TF(2)
        IF(TF(2).EQ.'O'.OR.TF(2).EQ.'o')THEN
          TF(2)='O'
        ELSE IF(TF(2).EQ.'N'.OR.TF(2).EQ.'n')THEN
          TF(2)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER HYDLIM'
        ENDIF
        READ(20,102) B,TF(3)
        IF(TF(3).EQ.'O'.OR.TF(3).EQ.'o')THEN
          TF(3)='O'
        ELSE IF(TF(3).EQ.'N'.OR.TF(3).EQ.'n')THEN
          TF(3)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER ENVLOP'
        ENDIF
        READ(20,102) B,TF(4)
        IF(TF(4).EQ.'O'.OR.TF(4).EQ.'o')THEN
          TF(4)='O'
        ELSE IF(TF(4).EQ.'N'.OR.TF(4).EQ.'n')THEN
          TF(4)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER PROFIL'
        ENDIF
        READ(20,102) B,TF(5)
        IF(TF(5).EQ.'O'.OR.TF(5).EQ.'o')THEN
          TF(5)='O'
        ELSE IF(TF(5).EQ.'N'.OR.TF(5).EQ.'n')THEN
          TF(5)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER LINDO'
        ENDIF
        READ(20,102) B,TF(6)
        IF(TF(6).EQ.'O'.OR.TF(6).EQ.'o')THEN
          TF(6)='O'
        ELSE IF(TF(6).EQ.'N'.OR.TF(6).EQ.'n')THEN
          TF(6)='N'
        ELSE
          WRITE(*,*)'ERREUR DANS L''OPTION FICHIER TRAJEC'
        ENDIF

C-----------------------------------------------
C       FACTEUR D'ECHELLE POUR LA VISUALISATION
C-----------------------------------------------
C        IF(IOVISU.EQ.'O')THEN
C          READ(20,100) B,ECH
C        ENDIF

100     FORMAT(A50,E12.5)
101     FORMAT(A50,150I1)
102     FORMAT(A50,A1)
103     FORMAT(A50,A3)

      READ(20,102,END=1005) B,TF(8)
ctrs      IF(TF(8).EQ.'O'.OR.TF(8).EQ.'o')THEN
ctrs        TRASED=.TRUE.
ctrs      ELSE
        TRASED=.FALSE.
ctrs      ENDIF
      DM=0.001
      SEGMA=1.
      READ(20,101,END=1000) B,OPTFPC
         IF (OPTFPC.GT.7)THEN
               write(*,*)'option de frottement non disponible'
         ENDIF
C      GO TO 1001
       WRITE(*,*)'option de frottement ',nomoptionf(OPTFPC)
c      IF(OPTFPC.NE.0)THEN
        IF(.NOT.TRASED)THEN
          ROM1=1.65
          READ(20,100,END=1101)B,ROS
          ROM1=(ROS-1000.)/1000.
          READ(20,100,END=1101)B,DM
          READ(20,100,END=1101)B,SEGMA
        ENDIF
c      ENDIF
1101  CLOSE(20)
      WRITE(*,'(A,A)')'Parametres de simulation: ',NOMFIC
      RETURN
1005  TRASED=.FALSE.
1000  OPTFPC=0
      ROM1=1.65
      DM=0.001
      SEGMA=1.
      CLOSE(20)
      WRITE(*,'(A,A)')'Parametres de simulation: ',NOMFIC
      RETURN
25    write(*,*)'fichier ',NOMFIC,' introuvable'
      stop
      END


C**********************************************************************
            SUBROUTINE MAIL
C----------------------------------------------------------------------
C                                                                     C
C     SOUS PROGRAMME D INITIALISATION DU MAILLAGE                     C
C                                                                     C
C     SON ROLE:CALCUL DES INTERMAILLES A PARTIR DES ABSCISSES DES     C
C              POINTS DE CALCUL CONTENUS DANS LE FICHIER DE MAILLAGE  C
C                                                                     C
C     ENTREES : TMAIL(FICHIER LU)      SORTIES : TMAIL,XTMAIL         C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C



        INTEGER NBMAX,NBB,LL,I,LMAX,LNCMAX,M

        PARAMETER(LMAX=3000,NBMAX=150,LNCMAX=130000)
        INTEGER LM(0:NBMAX)

        DOUBLE PRECISION TMAIL(LMAX)
     +      ,XTMAIL(LMAX)
     +      ,CTDF(LMAX),XCTDF(LMAX)
     +      ,PEN(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)

        CHARACTER ETUDE*20,NOMFIC*40

        COMMON/NOMETU/ETUDE
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XLGEO/DXMAIL,XDYA
        COMMON/PHYS/LM,LL
        COMMON/NBIEF/NBB

        LM(0)=0
        NOMFIC='mail.'//ETUDE
        OPEN(55,FILE=NOMFIC,STATUS='UNKNOWN')
        DO 1000 M=1,NBB
        READ(55,*) LM(M)
         LM(M)=LM(M-1)+LM(M)
         WRITE(*,*)
        READ(55,*) (TMAIL(I),I=LM(M-1)+1,LM(M))
        READ(55,*) (XTMAIL(I),I=LM(M-1)+1,LM(M)-1)
 1000   CONTINUE
        CLOSE(55)
        IF(LM(NBB).GT.LMAX) THEN
              WRITE(*,*)'LM VAUT ',LM(NBB)
              WRITE(*,*)'EXECUTION IMPOSSIBLE'
         WRITE(*,*) 'LE NOMBRE DE MAILLES DOIT ETRE INFERIEUR A ',LMAX
              STOP
        ENDIF
        LL=LM(NBB)-1
        DO M=1,NBB
          DO I=LM(m-1)+1,LM(M)-1
            IF(TMAIL(I).GE.TMAIL(I+1))THEN
              WRITE(*,*)'BIEF ',M
              WRITE(*,*) ' ABSCISSES NON CROISSANTES A ',TMAIL(I+1)
              STOP
            ENDIF
          ENDDO
          DO I=LM(m-1)+1,LM(M)-2
            IF(XTMAIL(I).GE.XTMAIL(I+1))THEN
              WRITE(*,*)'BIEF ',M
              WRITE(*,*) ' ABSCISSES NON CROISSANTES A ',XTMAIL(I+1)
              STOP
            ENDIF
          ENDDO
        ENDDO
      DO I=1,LL
        DXMAIL(I)=TMAIL(I+1)-TMAIL(I)
      ENDDO
        RETURN
        END



C-----------------------------------------------------------------------
      SUBROUTINE LECGEOM
C-----------------------------------------------------------------------
C R�cup�re la g�om�trie et les param�tres s�dimentaires
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I,J,II,NUMCS,JMININI1,JMINDUR1
      LOGICAL EXMOAG,EXMOAD,EXMMAG,EXMMAD
      DOUBLE PRECISION XX
C         ,YMOG,YMOD
     :,ZMININI,ZMINDUR,TN,ZDER
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
c      INTEGER XNCMOG(0:LMAX),XNCMOD(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER XNBCSP1(LNCMAX)
      CHARACTER ETUDE*20,NOMFIC*40
      CHARACTER LIGNE*630,FINLIGNE*600,MORCLIGNE*58
      CHARACTER XCCOU*1
      CHARACTER REP*1
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
     :  ,XSECMI(LNCMAX),SECMIN(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      INTEGER XNCBINI(LMAX),XNCBDUR(LMAX),N0,N1
      DOUBLE PRECISION XZINI(LNCMAX),XZDUR(LNCMAX)
     &  ,XPLANI(LNCMAX),YPLANI(LNCMAX),COUR2G(LMAX)
c      DOUBLE PRECISION XLU,YLU,ZLU,ZDER
      LOGICAL XMODSEC(LMAX),MODSEC
      LOGICAL nouvpt
C         ,moyen,MOYENG

      COMMON/ITEM/REP
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/NOMETU/ETUDE
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOVISU/XNCBINI,XNCBDUR,XZINI,XZDUR
      COMMON/COURB/COUR2G
      COMMON/MODIFSEC/XMODSEC,MODSEC
      COMMON/SPMM/XSECMI,SECMIN
         COMMON/NBIEF/NBB
         common/XYPLANI/XPLANI,YPLANI

C R�cup�ration et pr�paration de la g�om�trie aux intermailles
C-----------------------------------------------------------------------
C Ouverture du fichier de g�om�trie abscisse-cote aux intermailles
C geomac-i (g�om�trie initiale) ou geomac-r (si reprise)
      IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
              REP='O'
        NOMFIC='geomac-r.'//ETUDE
        OPEN(24,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED')
        READ(24,*) II,TN
        IF(ABS(TN-tinit1D).GE.0.1)THEN
          WRITE(*,*)'PROBLEME DE TEMPS INITIAL DANS geomac-r TN=',TN
          STOP
        ENDIF
        WRITE(*,'(A,F15.3,A,A)')'Geometrie du lit (reprise a'
     &    ,tinit1D,'s): ',NOMFIC
      ELSE
        NOMFIC='geomac-i.'//ETUDE
        OPEN(24,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED')
        READ(24,*) II
        WRITE(*,'(A,A)')'Geometrie du lit: ',NOMFIC
C fin du if sur reprise ou pas
      ENDIF
      XNCMOAG(0)=0
      XNCMOAD(0)=0
      XNC(0)=0
      DO IB=1,NBB
           IF(IB.NE.1)THEN
                     READ(24,*)II
                  I=LM(IB)-LM(IB-1)-1
              ELSE
C dans le cas du bief 1 II a d�ja �t� lu
                     I=LM(IB)-1
              ENDIF
        IF(II.NE.I)THEN
C        IF(II.NE.LL)THEN
         IF(REP.EQ.'O')THEN
          WRITE(*,*)'LE NOMBRE DE SECTIONS DANS geomac-r ',II
     &      ,'N''EST PAS EGAL AU NOMBRE D''INTERMAILLES DANS mail ',I
            ELSE
          WRITE(*,*)'LE NOMBRE DE SECTIONS DANS geomac-i ',II
     &      ,'N''EST PAS EGAL AU NOMBRE D''INTERMAILLES DANS mail ',I
            ENDIF
         IF(NBB.NE.1)THEN
                       write(*,*)'DANS LE BIEF ',IB
               ENDIF
         STOP
        ENDIF
      DO I=LM(IB-1)+1,LM(IB)-1
C      DO I=1,LL
C Contr�le des en-t�tes de sections
        READ(24,*,END=240) II,XX,XNC(I)
        XNC(I)=XNC(I)+XNC(I-1)
        IF(II.NE.I-lm(ib-1))THEN
          WRITE(*,*)'ATTENTION: dans geomac initial, la ',I-lm(ib-1)
     &      ,'ieme section est numerotee ',II
           IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
              endif
C fin du if sur II
        ENDIF
        IF((XX.GE.XTMAIL(I)+EPSY).OR.(XX.LE.XTMAIL(I)-EPSY))THEN
          WRITE(*,*)'PROBLEME DANS geomac initial: LA ',I-lm(ib-1)
     &      ,'IEME SECTION EST A UNE MAUVAISE ABSCISSE '
           IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
              endif
          STOP
C fin du if sur xx
        ENDIF
C Initialisation pour le traitement des limites min/Moy
        EXMOAG=.FALSE.
        EXMOAD=.FALSE.
        EXMMAG=.FALSE.
        EXMMAD=.FALSE.
        DO J=XNC(I-1)+1,XNC(I)
C Lecture des couples abscisse-cote
          READ(24,'(A630)') LIGNE
          READ(LIGNE,9994) XCCOU,XYCOU(J),FINLIGNE
                IF(J.NE.XNC(I-1)+1)THEN
                     IF(XYCOU(J-1).gt.xycou(J)+EPS)then
C                     IF(XYCOU(J-1).gt.xycou(J))then
C rajoute pour le cas d'egalite
C                       IF(abs(xycou(j)-xycou(j-1)).gt.epsy)then
      write(*,*)'probleme eventuel geometrie section', i
      write(*,*)'abscisse point', j-xnc(i-1),
     :'< abscisse point ', j-xnc(i-1)-1
      IF(NBB.NE.1)THEN
         write(*,*)'dans le bief ',IB
      endif
C                       endif
C 26/05/09 correction supprimee car correspond a section se fermant
C et le triatement ulterieur devrait etre bon
c                       XYCOU(J+1)=XYCOU(j)
c                       xycou(j)=xycou(j-1)
c                       xycou(j-1)=xycou(j+1)
                     ENDIF
                ENDIF
C Traitement des limites min/Moy
C XNCMOAG(I),XNCMOAD(I),EXMOAG,EXMOAG relatifs aux limites lit min / lit Moy de la section I 'G' et 'D'
C XNCMMAG(I),XNCMMAD(I),EXMMAG,EXMMAG relatifs aux limites pour le calcul de la cote moyenne du fond 'g' et 'd'
          IF(XCCOU.EQ.' ')THEN
          ELSEIF((.NOT.EXMOAG).AND.(XCCOU.EQ.'G'))THEN
            EXMOAG=.TRUE.
            XNCMOAG(I)=J
          ELSEIF((.NOT.EXMOAD).AND.(XCCOU.EQ.'D'))THEN
            EXMOAD=.TRUE.
            XNCMOAD(I)=J
          ELSEIF(XCCOU.EQ.'g')THEN
            EXMMAG=.TRUE.
            XNCMMAG(I)=J
          ELSEIF(XCCOU.EQ.'d')THEN
            EXMMAD=.TRUE.
            XNCMMAD(I)=J
          ELSE
            WRITE(*,*)'PROBLEME DANS geomac initial: '
     &        ,'LIMITE MINEUR/MOYEN SECTION ',I-lm(ib-1)
               IF(NBB.NE.1)THEN
                        write(*,*)'dans le bief ',IB
                  endif

            STOP
          ENDIF
C D�codage s�dimentaire (description point par point)
          DO NUMCS=1,CSMAX
            MORCLIGNE=FINLIGNE(1+58*(NUMCS-1):58*NUMCS)
            READ(MORCLIGNE,9996,END=200) XZCS(J,NUMCS),XDCS(J,NUMCS)
     &        ,XSCS(J,NUMCS),XTMCS(J,NUMCS)
c            IF((XZCS(J,NUMCS).EQ.0.).AND.(XDCS(J,NUMCS).EQ.0.)
c     &        .AND.(XSCS(J,NUMCS).EQ.0.)
c     &        .AND.(XTMCS(J,NUMCS).EQ.0.)) GOTO 200
            IF((XDCS(J,NUMCS).EQ.0.)
     &        .AND.(XSCS(J,NUMCS).EQ.0.)
     &        .AND.(XTMCS(J,NUMCS).EQ.0.)) then
                if(XZCS(J,NUMCS).EQ.0.)then
                   go to 200
                else
          WRITE(*,*)'PROBLEME GEOMETRIQUE OU SEDIMENTAIRE AU POINT ',
     &j-xnc(i-1),' SECTION ',I-lm(ib-1)
           IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
              endif
                endif
             endif
          ENDDO
 200      CONTINUE
          XNBCS(J)=NUMCS-1
C controle que cotes decroissantes
          n1=0
          DO NUMCS=XNBCS(J),2,-1
            if(xzcs(j,NUMCS).GT.xzcs(j,numcs-1))then
C pour ne mettre message que si non identiques
              if(abs(xzcs(j,numcs)-xzcs(j,numcs-1)).gt.eps)then
                write(*,*)'probleme de couches non superposees'
                write(*,*)'section ',i-lm(ib-1),' point ',j-xnc(i-1)
                IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
                endif
              endif
C ajout du 2 avril 2012
               do n0=numcs,xnbcs(j)
              xzcs(j,n0-1)=xzcs(j,n0)
              xdcs(j,n0-1)=xdcs(j,n0)
              xscs(j,n0-1)=xscs(j,n0)
              xtmcs(j,n0-1)=xtmcs(j,n0)
                            enddo
              n1=n1+1
C              xzcs(j,numcs)=xzcs(j,numcs-1)
            endif
          ENDDO
                  xnbcs(j)=xnbcs(j)-n1
          XZCOU(J)=XZCS(J,1)
                  IF(ABS(XZCOU(J)-XZCOU(j-1)).LT.EPS)THEN
                    IF(ABS(XYCOU(J)-XYCOU(j-1)).LT.EPS)THEN
            WRITE(*,*)'POINTS CONFONDUS ',j-xnc(i-1)-1,' ET ',
     &j-xnc(i-1),' SECTION ',I-lm(ib-1)
           IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
              endif
                        ENDIF
          ENDIF
          IF(XNBCS(J).EQ.0)THEN
            WRITE(*,*)'PROBLEME GEOMETRIQUE OU SEDIMENTAIRE AU POINT ',
     &j-xnc(i-1),' SECTION ',I-lm(ib-1)
           IF(NBB.NE.1)THEN
                     write(*,*)'dans le bief ',IB
              endif
            XNBCS(J)=1
C            STOP
          ENDIF
        ENDDO
C Ajout des limites min/Moy si absentes
        IF(.NOT.EXMOAG) XNCMOAG(I)=XNC(I-1)+1
        IF(.NOT.EXMOAD) XNCMOAD(I)=XNC(I)
        IF(.NOT.EXMMAG) XNCMMAG(I)=XNCMOAG(I)
        IF(.NOT.EXMMAD) XNCMMAD(I)=XNCMOAD(I)
                IF(xncmmad(i).LT.XNCMMAG(I)+2)then
                   write(*,*)'section ',i,' lit actif mal defini'
                endif
                IF(xncmoad(i).LT.XNCMoAG(I)+2)then
                   write(*,*)'section ',i,' lit mineur mal defini'
                endif
C fin boucle sur I
      ENDDO
      XNC(LM(IB))=XNC(LM(IB)-1)
C fin boucle sur IB

      ENDDO
 240  CLOSE(24)
c kamal: mars 2003; c'etait dans INITS
      DO I=1,LL
          DO J=XNC(I-1)+1,XNC(I)
          XZCOUP1(J)=XZCS(J,1)
          XNBCSP1(J)=XNBCS(J)
          DO NUMCS=1,XNBCSP1(J)
            XZCSP1(J,NUMCS)=XZCS(J,NUMCS)
          ENDDO
        ENDDO
      ENDDO

C Initialisation des tableaux de g�om�tries initiale et dure
C-----------------------------------------------------------------------
C [abscisse-cote aux intermailles] utile pour les sorties graphiques
C "g�o dure" signifie le toit du compartiment s�dimentaire inf�rieur
C XNCBINI,XNCBDUR indice du point bas de la section
C XZINI,XZDUR cote des points de la section
      DO I=1,LL
        ZMININI=999999.
        ZMINDUR=999999.
        JMININI1=XNC(I-1)+1
        JMINDUR1=XNC(I-1)+1
        DO J=XNC(I-1)+1,XNC(I)
          XZINI(J)=XZCS(J,1)
          IF(XZINI(J).LT.ZMININI+EPS)THEN
C On choisit le centre des points qui sont dans un voisinage de la cote minimale
            IF(XZINI(J).LT.ZMININI-EPS)THEN
              ZMININI=XZINI(J)
              JMININI1=J
              XNCBINI(I)=J
            ELSE
              XNCBINI(I)=(J+JMININI1)/2
C division enti�re: je veux la partie enti�re de la moyenne des indices
            ENDIF
          ENDIF

          XZDUR(J)=XZCS(J,XNBCS(J))
          IF(XZDUR(J).LT.ZMINDUR+EPS)THEN
C On choisit le centre des points qui sont dans un voisinage de la cote minimale
            IF(XZDUR(J).LT.ZMINDUR-EPS)THEN
              ZMINDUR=XZDUR(J)
              JMINDUR1=J
              XNCBDUR(I)=J
            ELSE
C division enti�re: je veux la partie enti�re de la moyenne des indices
              XNCBDUR(I)=(J+JMINDUR1)/2
            ENDIF
          ENDIF
        ENDDO
      ENDDO

C (Premi�re) �criture de geomac:
C g�om�trie courante abscisse-cote aux intermailles (description sedimentaire point par point)
C-----------------------------------------------------------------------
C      NOMFIC='geomac.'//ETUDE
C      OPEN(24,FILE=NOMFIC,STATUS='UNKNOWN',FORM='FORMATTED')
C      WRITE(24,'(I4,1X,F12.3)')LL,tinit1D
C      DO I=1,LL
C        WRITE(24,9991)I,XTMAIL(I),XNC(I)-XNC(I-1)
C        DO J=XNC(I-1)+1,XNC(I)
C          IF(J.EQ.XNCMOAG(I))THEN
C            XCCOU='G'
C          ELSEIF(J.EQ.XNCMOAD(I))THEN
C            XCCOU='D'
C          ELSE
C            XCCOU=' '
C          ENDIF
C          WRITE(FINLIGNE,9995)(XZCS(J,NUMCS),XDCS(J,NUMCS)
C     &      ,XSCS(J,NUMCS),XTMCS(J,NUMCS),NUMCS=1,XNBCS(J))
C          WRITE(24,9994)XCCOU,XYCOU(J),FINLIGNE
C        ENDDO
C      ENDDO
C      CLOSE(24)
C 9991 FORMAT(I4,1X,F11.3,1X,I2)
C 9995 FORMAT(10(1X,F11.5,1X,F14.10,1X,F14.10,1X,F14.10,1X))
 9994 FORMAT(A1,1X,F11.5,A600)
 9996 FORMAT(F13.5,2F15.10,F15.5)

C Passage en largeur-cote aux intermailles
C-----------------------------------------------------------------------
C XMODSEC(I)=.TRUE. signifie que la lar-cot � l'intermaille I doit �tre recalcul�e
C TRACL met � jour les tableaux XCTDF, ceux de XTBGEO et ceux de MODIFSEC
      DO I=1,LL
        XMODSEC(I)=.TRUE.
      ENDDO
      CALL TRACLC
C la suite integree dans TRACLC
C Limite mineur/Moyen Droite/Gauche aux intermailles
C XNCMOAG,XNCMOAD limites en g�om�trie Abs-Cot
C XNCMOG,XNCMOD   limites en g�om�trie Lar-Cot
c      XNCMOG(0)=0
c      XNCMOD(0)=0
c      XNCMO(0)=0
c      MOYENG=.FALSE.
c      DO IB=1,NBB
c      DO I=LM(IB-1)+1,LM(IB)-1
c        YMOG=XZCOU(XNCMOAG(I))-XCTDF(I)
c        YMOD=XZCOU(XNCMOAD(I))-XCTDF(I)
c        XNCMOG(I)=XNC(I)
c        XNCMOD(I)=XNC(I)
c        Moyen=.false.
c        DO J=XNC(I-1)+2,XNC(I)
c          If(.NOT.moyen)THEN
c            IF(XYISEC(J).GE.YMOG)THEN
c              MOYEn=.TRUE.
c              MOYENG=.TRUE.
cC  ! On prend la valeur la plus proche
c              IF(ABS(XYISEC(J-1)-YMOG).LT.ABS(XYISEC(J)-YMOG))THEN
c                XNCMOG(I)=J-1
c              ELSE
c                XNCMOG(I)=J
c              ENDIF
c            ENDIF
c          ENDIF
c        ENDDO
c        Moyen=.false.
c        DO J=XNC(I-1)+2,XNC(I)
c          If(.not.moyen)THEN
c            IF(XYISEC(J).GE.YMOD)THEN
c              MOYEn=.TRUE.
c              MOYENG=.TRUE.
cC ! On prend la valeur la plus proche
c              IF(ABS(XYISEC(J-1)-YMOD).LT.ABS(XYISEC(J)-YMOD))THEN
c                XNCMOD(I)=J-1
c              ELSE
c                XNCMOD(I)=J
c              ENDIF
c            ENDIF
c          ENDIF
c        ENDDO
c        XNCMO(I)=MIN(XNCMOG(I),XNCMOD(I))
cC fin boucle sur I
c      ENDDO
c      XNCMO(LM(IB))=XNCMO(LM(IB)-1)
cC fin boucle sur IB
c      ENDDO
C detrmination de sections et perimetre mouille du plein bord
c         DO I=1,LL
c            DO J=XNC(I-1)+1,XNC(I)
c              IF(XNCMO(I).GE.J)THEN
c                XSECMI(J)=XSECUM(J)
c              ELSE
c                XSECMI(J)=XSECMI(J-1)+(XYISEC(J)-XYISEC(J-1))
c     :*XLISEC(XNCMO(I))
c              ENDIF
c            ENDDO
c          ENDDO
C recuperation frottements en lit moyen si au moins un lit moyen
c      IF(MOYENG)THEN
        CALL LMOYEN
c      ENDIF

C Interpolation de la g�om�trie aux centremailles
C-----------------------------------------------------------------------
C INCMLC remplit les tableaux NC,NCMO,CTDF,PEN et ceux de TABGEO
      CALL INCMLC
C      WRITE(*,'(A,A)')'interpolation de la geo aux centremailles: '

C Donn�es planim�triques: lecture si elles existent (fichier 'm.etude')
C                         sinon on donne des valeurs 1D standard
C-----------------------------------------------------------------------
      NOMFIC='m.'//ETUDE
      OPEN(25,FILE=NOMFIC,STATUS='OLD',ERR=250)
 251  CONTINUE
      READ(25,'(A630)') LIGNE
      IF(LIGNE(1:1).EQ.'#') GOTO 251
      J=1
c      XLU=999.9990
c      YLU=999.9990
c      ZLU=0.0000
      DO WHILE(J.LE.XNC(LL))
        NOUVPT=.TRUE.
        READ(25,'(A630)') LIGNE
        READ(LIGNE,*) XPLANI(J),YPLANI(J),ZDER
C NOUVELLE SECTION
        IF(ABS(XPLANI(J)-999.9990).LT.0.001)THEN
          IF(ABS(YPLANI(J)-999.9990).LT.0.001)THEN
            NOUVPT=.FALSE.
C lecture entete section
C on suppose pas de commentaires entre sections
            READ(25,'(A630)') LIGNE
          ENDIF
        ENDIF
c mis en commentaire car des points peuvent etre confondus
c de toute facon m et geomac-i doivent avoir les memes points
c        IF(ABS(XPLANI(J)-XLU).LE.EPSY)THEN
c          IF(ABS(YPLANI(J)-YLU).LE.EPSY)THEN
c            IF(ABS(ZDER-ZLU).LT.EPSY)THEN
C points identiques qui ont ete supprimes
c              NOUvPT=.FALSE.
c            ENDIF
c          ENDIF
c        ENDIF
        IF(NOUVPT)THEN
c          XLU=XPLANI(J)
c          YLU=YPLANI(J)
c          ZLU=ZDER
          J=J+1
        ENDIF
      ENDDO
      WRITE(*,'(A,A)')'Donnees geometriques planimetriques: ',NOMFIC
      CLOSE(25)
C calcul des rayons de courbure
C renvoie cour2G qui vaut 1/(2*G*rayon de courbure)
      CALL COURBU
      RETURN
C le fichier m.etude non defini ou mauvais
 250  CONTINUE
      CLOSE(25)
      DO I=1,LL
        DO J=XNC(I-1)+1,XNC(I)
          XPLANI(J)=XTMAIL(I)
          YPLANI(J)=XYCOU(J)
        ENDDO
        COUR2G(I)=0.
      ENDDO
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE COURBU
C-----------------------------------------------------------------------
C calcule le rayon de courbure dans chaque section
C on prend le plus fort entre section i-1 et I et entre i et i+1
C on ne prend pas en compte un rayon au dessus de 10000.
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,I
      INTEGER LMAX,LNCMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
         INTEGER LM(0:NBMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XZINI(LNCMAX),XZDUR(LNCMAX)
     &  ,XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,COUR2G(LMAX)
C cour2G contient 1/ (2*Grav*RC)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER XNCBINI(LMAX),XNCBDUR(LMAX)
C rayons de courbure respectivement i/i-1 et i/i+1
      DOUBLE PRECISION RC,RCD
     :,X1,Y1,X2,Y2,X3,Y3,X4,Y4,X5,Y5,A,B

      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOVISU/XNCBINI,XNCBDUR,XZINI,XZDUR
      COMMON/COURB/COUR2G
         common/XYPLANI/XPLANI,YPLANI

      RC=0.
      DO I=2,LL
C on calcule le point (x3,Y3) intersection du segment i-1 et i
        X1=XPLANI(XNCMMAG(I-1))
        Y1=YPLANI(XNCMMAG(I-1))
        X2=XPLANI(XNCMMAD(I-1))
        Y2=YPLANI(XNCMMAD(I-1))
        X4=XPLANI(XNCMMAG(I))
        Y4=YPLANI(XNCMMAG(I))
        X5=XPLANI(XNCMMAD(I))
        Y5=YPLANI(XNCMMAD(I))
        IF(X1.EQ.X2)THEN
          IF(X4.EQ.X5)THEN
C segments paralleles
             RCD=0.
             COUR2G(I-1)=0.
          ELSE
             X3=X1
             Y3=Y4+(X3-X4)*(Y4-Y5)/(X4-X5)
C on calcule la courbure pour la section i-1
C on est dans le cas x1=X2 donc Y1 et Y2 differents
             IF(Y1.GT.Y2)THEN
               IF(Y3.GT.Y1)THEN
C centre a gauche donc distance prise par rapport au point de droite
                 RCD=Y3-Y2
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSEIF(Y3.LT.Y2)THEN
                 RCD=Y3-Y1
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSE
C point au milieu : probleme definition
                 COUR2G(I-1)=0.
               ENDIF
C cas ou y1<Y2
             ELSE
               IF(Y3.GT.Y2)THEN
C centre a droite donc distance prise par rapport au point de gauche
                 RCD=Y3-Y1
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSEIF(Y3.LT.Y1)THEN
                 RCD=Y3-Y2
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSE
C point au milieu : probleme definition
                 COUR2G(I-1)=0.
               ENDIF
C endif sur Y1>Y2
             ENDIF
             IF(X4.GT.X5)THEN
               IF(X3.GT.X4)THEN
C centre a gauche donc distance prise par rapport au point de droite
                 RCD=SQRT((X3-X5)**2+(Y3-Y5)**2)
               ELSEIF(X3.LT.X5)THEN
                 RCD=-SQRT((X3-X4)**2+(Y3-Y4)**2)
               ELSE
C point au milieu : probleme definition
                 RCD=0.
               ENDIF
C cas ou x4<X5
             ELSE
               IF(X3.GT.X5)THEN
C centre a drote donc distance prise par rapport au point de gauche
                 RCD=-SQRT((X3-X4)**2+(Y3-Y4)**2)
               ELSEIF(X3.LT.X4)THEN
                 RCD=SQRT((X3-X5)**2+(Y3-Y5)**2)
               ELSE
C point au milieu : probleme definition
                 RCD=0.
               ENDIF
C endif sur x4>X5
             ENDIF
C endif sur X4 egal a X5
           ENDIF
C ELSE si x1 different de x2
         ELSE
           IF(X4.EQ.X5)THEN
             X3=X4
             Y3=Y2+(X3-X2)*(Y2-Y1)/(X2-X1)
C on calcule la courbure pour la section i-1
C on est dans le cas x1 different de X2
             IF(X1.GT.X2)THEN
               IF(X3.GT.X1)THEN
C centre a gauche donc distance prise par rapport au point de droite
                 RCD=SQRT((X3-X2)**2+(Y3-Y2)**2)
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSEIF(X3.LT.X2)THEN
                 RCD=-SQRT((X3-X1)**2+(Y3-Y1)**2)
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSE
C point au milieu : probleme definition
                 COUR2G(I-1)=0.
               ENDIF
C cas ou x1<X2
             ELSE
               IF(X3.GT.X2)THEN
C centre a droite donc distance prise par rapport au point de gauche
                 RCD=-SQRT((X3-X1)**2+(Y3-Y1)**2)
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSEIF(X3.LT.X1)THEN
                 RCD=SQRT((X3-X2)**2+(Y3-Y2)**2)
                 IF(RC*RCD.GT.0.)THEN
                   RC=MAX(RC,RCD)
                   IF(RC.GT.10000.)THEN
                     COUR2G(I-1)=0.
                   ELSE
                     COUR2G(I-1)=1./(RC*2.*GRAV)
                   ENDIF
                 ELSE
                   COUR2G(I-1)=0.
                 ENDIF
               ELSE
C point au milieu : probleme definition
                 COUR2G(I-1)=0.
               ENDIF
C endif sur X1>X2
             ENDIF
C on est dans le cas ou x4=X5 donc y4 different de Y5
             IF(Y4.GT.Y5)THEN
               IF(Y3.GT.Y4)THEN
C centre a gauche donc distance prise par rapport au point de droite
                 RCD=Y3-Y5
               ELSEIF(Y3.LT.Y5)THEN
                 RCD=Y3-Y4
               ELSE
C point au milieu : probleme definition
                 RCD=0.
               ENDIF
C cas ou y4<y5
             ELSE
               IF(y3.GT.y5)THEN
C centre a drote donc distance prise par rapport au point de gauche
                 RCD=Y3-Y4
               ELSEIF(Y3.LT.Y4)THEN
                 RCD=Y3-Y5
               ELSE
C point au milieu : probleme definition
                 RCD=0.
               ENDIF
C endif sur y4>y5
             ENDIF

C else du if sur X4=X5
C correspond a x1 et X2 differentes, X4 et X5 differents
           ELSE
             a=(Y4-Y5)/(X4-X5)
             b=(Y2-Y1)/(X2-X1)
             if(A.EQ.B)THEN
C segments paralleles
               RCD=0.
               COUR2G(I-1)=0.
C a different de B
             ELSE
               X3=(Y2-Y4+X4*A-X2*B)/(A-B)
               Y3=Y4+(X3-X4)*A
C on calcule la courbure pour la section i-1
C on est dans le cas x1 different de X2 mais Y1 et Y2 differents ou egaux
               IF(X1.GT.X2)THEN
                 IF(X3.GT.X1)THEN
C centre a gauche donc distance prise par rapport au point de droite
                   RCD=SQRT((X3-X2)**2+(Y3-Y2)**2)
                   IF(RC*RCD.GT.0.)THEN
                     RC=MAX(RC,RCD)
                     IF(RC.GT.10000.)THEN
                       COUR2G(I-1)=0.
                     ELSE
                       COUR2G(I-1)=1./(RC*2.*GRAV)
                     ENDIF
                   ELSE
                     COUR2G(I-1)=0.
                   ENDIF
                 ELSEIF(X3.LT.X2)THEN
                   RCD=-SQRT((X3-X1)**2+(Y3-Y1)**2)
                   IF(RC*RCD.GT.0.)THEN
                     RC=MAX(RC,RCD)
                     IF(RC.GT.10000.)THEN
                       COUR2G(I-1)=0.
                     ELSE
                       COUR2G(I-1)=1./(RC*2.*GRAV)
                     ENDIF
                   ELSE
                     COUR2G(I-1)=0.
                   ENDIF
                 ELSE
C point au milieu : probleme definition
                   COUR2G(I-1)=0.
                 ENDIF
C cas ou x1<X2
               ELSE
                 IF(X3.GT.X2)THEN
C centre a droite donc distance prise par rapport au point de gauche
                   RCD=-SQRT((X3-X1)**2+(Y3-Y1)**2)
                   IF(RC*RCD.GT.0.)THEN
                     RC=MAX(RC,RCD)
                     IF(RC.GT.10000.)THEN
                       COUR2G(I-1)=0.
                     ELSE
                       COUR2G(I-1)=1./(RC*2.*GRAV)
                     ENDIF
                   ELSE
                     COUR2G(I-1)=0.
                   ENDIF
                 ELSEIF(X3.LT.X1)THEN
                   RCD=SQRT((X3-X2)**2+(Y3-Y2)**2)
                   IF(RC*RCD.GT.0.)THEN
                     RC=MAX(RC,RCD)
                     IF(RC.GT.10000.)THEN
                       COUR2G(I-1)=0.
                     ELSE
                       COUR2G(I-1)=1./(RC*2.*GRAV)
                     ENDIF
                   ELSE
                     COUR2G(I-1)=0.
                   ENDIF
                 ELSE
C point au milieu : probleme definition
                   COUR2G(I-1)=0.
                 ENDIF
C endif sur X1>X2
               ENDIF
               IF(X4.GT.X5)THEN
                 IF(X3.GT.X4)THEN
C centre a gauche donc distance prise par rapport au point de droite
                   RCD=SQRT((X3-X5)**2+(Y3-Y5)**2)
                 ELSEIF(X3.LT.X5)THEN
                   RCD=-SQRT((X3-X4)**2+(Y3-Y4)**2)
                 ELSE
C point au milieu : probleme definition
                   RCD=0.
                 ENDIF
C cas ou x4<X5
               ELSE
                 IF(X3.GT.X5)THEN
C centre a drote donc distance prise par rapport au point de gauche
                   RCD=-SQRT((X3-X4)**2+(Y3-Y4)**2)
                 ELSEIF(X3.LT.X4)THEN
                   RCD=SQRT((X3-X5)**2+(Y3-Y5)**2)
                 ELSE
C point au milieu : probleme definition
                   RCD=0.
                 ENDIF
C endif sur x4>X5
               ENDIF
C endif sur a=b
             ENDIF
C endif sur X4 egal a X5
           ENDIF
C ENDIF si x1 different de x2
         ENDIF
         RC=RCD
C boucle sur I
       ENDDO
C on suppose amont et aval en portion droite
       COUR2G(1)=0.
       COUR2G(LL)=0.
       RETURN
       END

C***********************************************************************
        SUBROUTINE INIFRII
C***********************************************************************
C
C     SON ROLE:  il d�finit
C     les parametres de frottement   point par point
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



      INTEGER NBMAX,LL,I,J,LMAX,NBB,ib,K,LNCMAX

      PARAMETER(LMAX=3000,NBMAX=150,LNCMAX=130000)
      INTEGER LM(0:NBMAX)

      DOUBLE PRECISION FRII(LNCMAX)
     +    ,GRAV,EPS,EPSY,CHEZY,EPSM
     :, FR1(lmax),FRLM(LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
     :,NC(0:LMAX),XNC(0:LMAX)
      CHARACTER*20 NOMFIC*40,ETUDE
         integer clfr(LNCMAX),nloc,frplani(LNCMAX)
     :,nbclfr,itypfrl
         double precision xloc(LNCMAX),yloc(LNCMAX)
     :,frlcl(LNCMAX),XPLANI(LNCMAX),YPLANI(LNCMAX)
     :,mfrloc
      DOUBLE PRECISION Hcl(LNCMAX),nbobstcl(LNCMAX)
     :,CoefDcl(LNCMAX),landacl(LNCMAX),landasurfcl(LNCMAX)

         LOGICAL FROTLOC,frlfix,frotloccd,trouve


          COMMON/NOMETU/ETUDE
      COMMON/FRI/FRII
      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NBIEF/NBB
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/NC/NC,XNC
         common/frl/frotloc,frlfix,frotloccd
         common/frlcl/frlcl
         common/frplani/frplani
         common/mfrloc/mfrloc
         common/XYPLANI/XPLANI,YPLANI
         Common/cdcl/Hcl,nbobstcl,CoefDcl,landacl,landasurfcl

C LECTURE DU FICHIER DE FROTTEMENT local:
C-------------------------------------
C format frotloc simple
      NOMFIC='frotloc.'//ETUDE
      OPEN(60,FILE=NOMFIC,STATUS='OLD',ERR=10)
      write(*,*)'lecture frottement local dans fichier ',nomfic
      FROTLOC=.TRUE.
      DO K=1,LNCMAX
                read(60,*,err=11,end=11)xloc(k),yloc(k),clfr(k)
      enddo
 11   close(60)
      nloc=K-1
      do ib=1,nbb
       DO I=lm(ib-1)+1,LM(iB)-1
        DO J=XNC(I-1)+1,XNC(I)
           trouve=.false.
           do k=1,nloc
             if(.not.trouve)then
                if(abs(xplani(j)-xloc(k)).LT.eps)then
                  if(abs(yplani(j)-yloc(k)).LT.eps)then
                    trouve=.true.
                    frplani(j)=clfr(k)
                  endif
                endif
              endif
c fin boucle sur k
          enddo
          If(.not.trouve)then
             write(*,*)'un point de section sans frottement local'
             write(*,*)'x= ',xplani(j),' y= ',yplani(j)
             write(*,*)'bief ',ib,' section ',i,' point ',j
             stop
          endif
C fin boucle sur j
         ENDDO
C fin boucle sur i
       ENDDO
C fin boucle sur ib
      ENDDO
      NOMFIC='frotform.'//ETUDE
      OPEN(60,FILE=NOMFIC,STATUS='OLD',ERR=12)
      write(*,*)'lecture formule frottement dans fichier ',nomfic
      read(60,*,err=12)nbclfr,itypfrl,mfrloc
      if(abs(mfrloc).lT.EPS)then
         write(*,*)'exposant nul non autorise'
          stop
      endif
      if(itypfrl.eq.1)then
        FRLFIX=.TRUE.
        FROTLOCCD=.FALSE.
        do k=1,nbclfr
          read(60,*,err=12)frlcl(k)
        enddo
      elseif(itypfrl.eq.2)then
        FRLFIX=.TRUE.
        FRotloccd=.TRUE.
        do k=1,nbclfr
          read(60,*,err=12)frlcl(k),hcl(k),nbobstcl(k),coefdcl(k)
     :,landacl(k),landasurfcl(k)
         enddo
      else
        FRLFIX=.FALSE.
        FRotLOCCD=.FALSE.
      endif
      close(60)
C remplit frii : frottement local si donne en strickler fixe
      call calcfrloc
      RETURN
C si fichier formule pas l� mais fichier frotloc present
  12    write(*,*)'erreur dans le fichier ',nomfic
        stop
 10     FROTLOC=.FALSE.
C on definit frii en fonction des stricklers par lit
              do ib=1,nbb
        J=lm(ib-1)+1
               DO K=xnc(j-1)+1,xncmoag(j)-1
                 FRII(K)=FRlm(j)
               ENDDO
               DO K=xncmoag(j),xncmoad(j)
                 FRII(K)=FR1(j)
               ENDDO
               DO K=xncmoad(j)+1,xnc(j)
                 FRII(K)=FRlm(j)
               ENDDO
        DO J=lm(ib-1)+2,LM(iB)-2
               DO K=xnc(j-1)+1,xncmoag(j)-1
                 FRII(K)=0.5*(FRlm(j+1)+frlm(j))
               ENDDO
               DO K=xncmoag(j),xncmoad(j)
                 FRII(K)=0.5*(FR1(j+1)+fr1(j))
               ENDDO
               DO K=xncmoad(j)+1,xnc(j)
                 FRII(K)=0.5*(FRlm(j+1)+frlm(j))
               ENDDO
C fin boucle sur maille
        ENDDO
        J=LM(iB)-1
               DO K=xnc(j-1)+1,xncmoag(j)-1
                 FRII(K)=FRlm(j+1)
               ENDDO
               DO K=xncmoag(j),xncmoad(j)
                 FRII(K)=FR1(j+1)
               ENDDO
               DO K=xncmoad(j)+1,xnc(j)
                 FRII(K)=FRlm(j+1)
               ENDDO

c fin boucle sur ib
        ENDDO
        RETURN
        END
C***********************************************************************
         subroutine calcfrloc

C     SON ROLE:  il d�finit
C     les parametres de frottement   point par point

C***********************************************************************
       INTEGER NBMAX,LL,J,LMAX,NBB,ib,K,LNCMAX

      PARAMETER(LMAX=3000,NBMAX=150,LNCMAX=130000)
      INTEGER LM(0:NBMAX)

      DOUBLE PRECISION FRII(LNCMAX)
     :, FR1(lmax),FRLM(LMAX)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
         integer frplani(LNCMAX)
         double precision frlcl(LNCMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      DOUBLE PRECISION HII(LNCMAX),nbobst(LNCMAX),CoefDIi(LNCMAX)
     :,landaii(LNCMAX),landasurfII(LNCMAX),Hcl(LNCMAX),nbobstcl(LNCMAX)
     :,CoefDcl(LNCMAX),landacl(LNCMAX),landasurfcl(LNCMAX)

         LOGICAL FROTLOC,frlfix,frotloccd

         COMMON/FRI/FRII
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/NC/NC,XNC
         common/frl/frotloc,frlfix,frotloccd
         common/frlcl/frlcl
         common/frplani/frplani
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
         Common/cdII/HII,nbobst,CoefDIi,landaii,landasurfII
         Common/cdcl/Hcl,nbobstcl,CoefDcl,landacl,landasurfcl

         if(frlfix)then
          if(frotloccd)then
C cas force de trainee
       do ib=1,nbb
        DO J=lm(ib-1)+1,LM(iB)-1
               DO K=xnc(j-1)+1,xnc(j)
           FRII(K)=frlcl(frplani(k))
           hII(K)=hcl(frplani(k))
           nbobst(K)=nbobstcl(frplani(k))
           coefdII(K)=coefdcl(frplani(k))
           landaII(K)=landacl(frplani(k))
           landasurfII(K)=landasurfcl(frplani(k))
               ENDDO
             enddo
c fin boucle sur ib
        ENDDO
C cas sans forcede trainee
        else
        do ib=1,nbb
        DO 5 J=lm(ib-1)+1,LM(iB)-1
               DO K=xnc(j-1)+1,xnc(j)
           FRII(K)=frlcl(frplani(k))
                 if (frII(K).LT.2.)then
                      if(K.lt.xncmoad(j).and.k.gt.xncmoag(j))then
                         frii(k)=frii(k)*fr1(J)
                      else
                         frii(k)=frii(k)*frlm(J)
                      endif
           endif
               ENDDO
5       CONTINUE
c fin boucle sur ib
        ENDDO
C fin du if sur frotloccd
              endif
C fin du if sur frlfix
              endif
              return
              end
C***********************************************************************
        SUBROUTINE CALCFR
C***********************************************************************
C
C     SON ROLE:  il d�finit
C     les frottements par lit a partir frottements par point
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



      INTEGER NBMAX,LL,I,J,LMAX,NBB,ib,K,LNCMAX

      PARAMETER(LMAX=3000,NBMAX=150,LNCMAX=130000)
      INTEGER LM(0:NBMAX)

      DOUBLE PRECISION FRII(LNCMAX)
     +    ,GRAV,EPS,EPSY,CHEZY,EPSM
     :, FR1(lmax),FRLM(LMAX),pm,pmm,fr,frm,zsurf
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
     :,NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION mfrloc
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XZCOU(LNCMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION FR1s(lmax),FRLMs(LMAX)
C         LOGICAL FROTLOC,frlfix,trouve

         COMMON/FRI/FRII
      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NBIEF/NBB
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/NC/NC,XNC
C         common/frl/frotloc,frlfix
      common/mfrloc/mfrloc
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACZ/XZCOU
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER

      Do ib=1,nbb
       do j=lm(ib-1)+1,lm(ib)-1
         pm=0.
         pmm=0.
         fr=0.
         frm=0.
         zsurf=xctdf(j)+yinter(j)
         do i=xnc(j-1)+1,xncmoag(j)
           if(xzcou(i)+eps.lt.zsurf)then
             pmm=pmm+xdya(i)
             frm=frm+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         do i=xncmoag(j)+1,xncmoad(j)-1
           if(xzcou(i)+eps.lt.zsurf)then
              pm=pm+xdya(i)
              fr=fr+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         do i=xncmoad(j),xnc(j)
          if(xzcou(i)+eps.lt.zsurf)then
             pmm=pmm+xdya(i)
             frm=frm+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         if(pmm.gt.eps)then
           frlms(j)=(frm/pmm)**(1./mfrloc)
                  else
                   frlms(j)=frlm(j)
         endif
         if(pm.gt.eps)then
            fr1s(j)=(fr/pm)**(1./mfrloc)
                 else
                   fr1s(j)=fr1(j)
         endif
c fin boucle sur j
        enddo
c fin boucle sur ib
        enddo
        Do ib=1,nbb
          j=lm(ib-1)+1
          fr1(j)=fr1s(j)
          frlm(j)=frlms(j)
          j=lm(ib)
          fr1(j)=fr1s(j-1)
          frlm(j)=frlms(j-1)
          do j=lm(ib-1)+2,lm(ib)-1
            fr1(j)=0.5*(fr1s(j-1)+fr1s(j))
            frlm(j)=0.5*(frlms(j-1)+frlms(j))
c fin boucle sur j
         enddo
c fin boucle sur ib
      enddo
      return
      end

C***********************************************************************
        SUBROUTINE CALCFRCD
C***********************************************************************
C
C     SON ROLE:  il d�finit
C     les frottements par lit a partir frottements par point
C en cas d'utilisation de coefficient de trainee
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



      INTEGER NBMAX,LL,I,J,LMAX,NBB,ib,K,LNCMAX

      PARAMETER(LMAX=3000,NBMAX=150,LNCMAX=130000)
      INTEGER LM(0:NBMAX)

      DOUBLE PRECISION FRII(LNCMAX)
     +,HII(LNCMAX),nbobst(LNCMAX),CoefDIi(LNCMAX)
     +,landaii(LNCMAX),landasurfII(LNCMAX)
     +    ,GRAV,EPS,EPSY,CHEZY,EPSM
     : ,FR1(lmax),FRLM(LMAX),pm,pmm,fr,frm,zsurf
     : ,h,h2,fr2,frii2,freq
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
     :,NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION mfrloc
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XZCOU(LNCMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION FR1s(lmax),FRLMs(LMAX)
C         LOGICAL FROTLOC,frlfix,trouve

         COMMON/FRI/FRII
      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NBIEF/NBB
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/NC/NC,XNC
C         common/frl/frotloc,frlfix
      common/mfrloc/mfrloc
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACZ/XZCOU
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      Common/cdII/HII,nbobst,CoefDIi,landaii,landasurfII

      Do ib=1,nbb
       do j=lm(ib-1)+1,lm(ib)-1
         pm=0.
         pmm=0.
         fr=0.
         frm=0.
         zsurf=xctdf(j)+yinter(j)
         do i=xnc(j-1)+1,xncmoag(j)
           if(xzcou(i)+eps.lt.zsurf)then
            if(hII(i).LT.EPS)THEN
             freq=frii(i)
             Else
             h=zsurf-xzcou(i)
             h2=min(h,hii(i))
C fr strickler        equivalent local issu de cd
             fr2=((1.-landasurfii(i))*2.*grav)/
     +(h**0.333*coefdii(i)*landaii(i)*h2*nbobst(i))
             IF(h.GT.h2)then
               fr2=fr2*(h/h2)**1.2
              endif
             frii2=frii(i)**2
             freq=sqrt(frii2*fr2/(frii2+fr2))
             endif
             pmm=pmm+xdya(i)
             frm=frm+xdya(i)*freq**mfrloc
C             frm=frm+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         do i=xncmoag(j)+1,xncmoad(j)-1
           if(xzcou(i)+eps.lt.zsurf)then
            if(hII(i).LT.EPS)THEN
             freq=frii(i)
             Else
             h=zsurf-xzcou(i)
             h2=min(h,hii(i))
C fr strickler        equivalent local issu de cd
             fr2=((1.-landasurfii(i))*2.*grav)/
     +(h**0.333*coefdii(i)*landaii(i)*h2*nbobst(i))
              IF(h.GT.h2)then
               fr2=fr2*(h/h2)**1.2
              endif
            frii2=frii(i)**2
             freq=sqrt(frii2*fr2/(frii2+fr2))
            endif
             pm=pm+xdya(i)
              fr=fr+xdya(i)*freq**mfrloc
C              fr=fr+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         do i=xncmoad(j),xnc(j)
          if(xzcou(i)+eps.lt.zsurf)then
            if(hII(i).LT.EPS)THEN
             freq=frii(i)
             Else
             h=zsurf-xzcou(i)
             h2=min(h,hii(i))
C fr strickler        equivalent local issu de cd
             fr2=((1.-landasurfii(i))*2.*grav)/
     +(h**0.333*coefdii(i)*landaii(i)*h2*nbobst(i))
             IF(h.GT.h2)then
               fr2=fr2*(h/h2)**1.2
              endif
             frii2=frii(i)**2
             freq=sqrt(frii2*fr2/(frii2+fr2))
            endif
             pmm=pmm+xdya(i)
             frm=frm+xdya(i)*freq**mfrloc
C             frm=frm+xdya(i)*frii(i)**mfrloc
           endif
c fin boucle sur i
         enddo
         if(pmm.gt.eps)then
           frlms(j)=(frm/pmm)**(1./mfrloc)
                  else
                   frlms(j)=frlm(j)
         endif
         if(pm.gt.eps)then
            fr1s(j)=(fr/pm)**(1./mfrloc)
                 else
                   fr1s(j)=fr1(j)
         endif
c fin boucle sur j
        enddo
c fin boucle sur ib
        enddo
        Do ib=1,nbb
          j=lm(ib-1)+1
          fr1(j)=fr1s(j)
          frlm(j)=frlms(j)
C          write(*,*)'fr=',fr1(j),frlm(j)
          j=lm(ib)
          fr1(j)=fr1s(j-1)
          frlm(j)=frlms(j-1)
          do j=lm(ib-1)+2,lm(ib)-1
            fr1(j)=0.5*(fr1s(j-1)+fr1s(j))
            frlm(j)=0.5*(frlms(j-1)+frlms(j))
c fin boucle sur j
         enddo
c fin boucle sur ib
      enddo
      return
      end
C***********************************************************************
        SUBROUTINE INIGEO
C***********************************************************************
C
C     SON ROLE:  il ne r�cupere que
C     les parametres de frottement
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



      INTEGER NBMAX,LL,I,J,LMAX,NBB,ib

      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX)

C      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION FR1(LMAX)
     +    ,GRAV,EPS,EPSY,CHEZY,EPSM

      CHARACTER*20 NOMFIC*40,ETUDE
      CHARACTER TF(10)*1

      COMMON/NOMETU/ETUDE
      COMMON/FROTMT/FR1
C      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PHYS/LM,LL
      COMMON/TSTFIL/TF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NBIEF/NBB

C LECTURE DU FICHIER DE FROTTEMENT :
C-------------------------------------

        NOMFIC='frot.'//ETUDE
        OPEN(60,FILE=NOMFIC,STATUS='UNKNOWN')
              do ib=1,nbb
        READ(60,*)I
        DO 5 J=lm(ib-1)+1,LM(iB)
c           I=J
           READ(60,*) I,FR1(J)
           IF(FR1(J).LT.0.001)THEN
             FR1(J)=1.D10
           ENDIF
5       CONTINUE
c fin boucle sur ib
        ENDDO
        CLOSE(60)

        RETURN
        END

C***********************************************************************
        SUBROUTINE LMOYEN
C***********************************************************************
C
C     SON ROLE:  il r�cupere                                       C
C     les parametres de frottement DU LIT MOYEN
C
C+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



      INTEGER NBMAX,NBB,LL,I,J,LMAX,ib

      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX)
      INTEGER XNC(0:LMAX)
      INTEGER XNCMO(0:LMAX)
      INTEGER NC(0:LMAX),NCMO(0:LMAX)

      DOUBLE PRECISION FRLM(LMAX),FR1(LMAX)

      CHARACTER*20 NOMFIC*40,ETUDE
         LOGICAL MOYEN

      COMMON/NOMETU/ETUDE
      COMMON/FROT2/FRLM
      COMMON/FROTMT/FR1
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO

C LECTURE DU FICHIER DE FROTTEMENT :
C-------------------------------------

        NOMFIC='frot2.'//ETUDE
        OPEN(60,FILE=NOMFIC,STATUS='OLD',ERR=1)
              DO IB=1,NBB
          READ(60,*,END=1)I
C        WRITE(*,*)'LECTURE FROTTEMENT DE LIT MOYEN'
         DO 5 J=lm(ib-1)+1,LM(ib)
c           I=J
           READ(60,*,ERR=1) I,FRLM(J)
           IF(FRLM(J).LT.0.001)THEN
             FRLM(J)=1.D10
           ENDIF
5        CONTINUE
C FIN BOUCLE SUR IB
        ENDDO
        CLOSE(60)
        RETURN

 1      MOYEN=.FALSE.
        DO J=1,LM(NBB)
          IF(ncmo(j).NE.nc(j))THEN
             MOYEN=.TRUE.
                ENDIF
        ENDDO
        IF(MOYEN)THEN
        WRITE(*,*)'PAS DE FROTTEMENT DE LIT MOYEN'
           ENDIF
C le test ne servait qu a ecrire message
C dans tous les cas on met meme frottement
         DO 7 J=1,LM(NBB)
          FRLM(J)=FR1(J)
 7      CONTINUE
        RETURN
        END


C-----------------------------------------------------------------------
      SUBROUTINE INIT2
C-----------------------------------------------------------------------
C Lit les conditions aux limites et les param�tres de sauvegarde
C Sorties: TH,QTH (HYDROGRAMME AMONT)
C          YH,QYH (C.L AVAL)
C          HIMP
C          YD,MU
C          ABS,CTDF,PEN
C          DE 1 A 10  Y,L,S,P,B,dl/dx,PP,DLDY
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBHYPR,NTHMAX,NCLMAX,NBMAX
      PARAMETER(LMAX=3000,NBHYPR=100,NTHMAX=20000,NCLMAX=1000,NBMAX=150)
      INTEGER I,J,K,N,N1,N2,N3,N20,TYP,NBB,M,J2,IB
      INTEGER LM(0:NBMAX),LL
      INTEGER NT1(0:NBMAX),NT3(0:NBMAX)
      INTEGER NT2(0:NBMAX)
      INTEGER NMU(0:LMAX)
      INTEGER INDDEV(0:LMAX)
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      INTEGER NBSSAV,NTSOR,NBMAIL(NBHYPR),ITSAV
      CHARACTER NOMFIC*40
      CHARACTER IODEV*1
      CHARACTER TF(10)*1
      CHARACTER ETUDE*20,CHAINE*31,LIGNE*80
      DOUBLE PRECISION MAILLE,XTS,DX
      DOUBLE PRECISION TH(NTHMAX),QTH(NTHMAX)
      DOUBLE PRECISION TA(NCLMAX),HIMP(NCLMAX)
      DOUBLE PRECISION YH(NCLMAX),QYH(NCLMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION ZYD(LMAX),MU(LMAX),EXPOS(LMAX),VOLDEV(LMAX)
     & ,VOLDE1(LMAX)
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION TMU(LMAX*20),QMU(LMAX*20)
      DOUBLE PRECISION TSMU(LMAX*20),QSMU(LMAX*20)
      DOUBLE PRECISION OUV(LMAX),LOND(LMAX),COEFA(LMAX),COEFB(LMAX)
     &  ,EFFPRI(LMAX)
     : ,DLAT(LMAX),SLAT(LMAX),DMU,SMU
     : ,CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION X,ZD,MUD,HP,LD,CFA,CFB,EFF
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
         INTEGER NMIN(LMAX),NMAX(LMAX)
         LOGICAL LECHYDRO,LECHAMONT,LECCONDAV

C         DATA INDDEV/LMAX*0/

      COMMON/NBIEF/NBB
      COMMON/NOMETU/ETUDE
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PHYS/LM,LL
      COMMON/TSTFIL/TF
      COMMON/HYDAM/TH,QTH
      COMMON/NHYDM/NT1,NT3
      COMMON/HHAM/TA,HIMP
      COMMON/HYDAV/YH,QYH
      COMMON/NHYDA/NT2
      COMMON/TQMU/TMU,QMU
      COMMON/NTMU/NMU
      COMMON/QTSM/QSMU,TSMU
      COMMON/PRISE/OUV,LOND,COEFA,COEFB,EFFPRI
      COMMON/DSLAT/DLAT,SLAT
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

C R�cup�ration des param�tres de d�versement
C-------------------------------------------
C Initialisation de NMU, tableau des pointeurs de couples li�s
C aux apports lat�raux
      DO I=0,LM(NBB)
        NMU(I)=0
      ENDDO
      DO I=1,LM(NBB)
                VOLDEV(I)=0.
      ENDDO
         DO I=1,LMAX
           INDDEV(i)=0
         ENDDO

C Ouverture du fichier devers.etude
C----------------------------------
      IF(IODEV.EQ.'O')THEN
        NOMFIC='devers.'//ETUDE
        OPEN(26,FILE=NOMFIC,FORM='FORMATTED',STATUS='OLD',ERR=231)
        WRITE(*,'(A,A)')'Deversements lateraux: ',NOMFIC
              DO IB=1,NBB
        READ(26,*) K
        DO I=LM(IB-1)+1,LM(IB)
          READ(26,*)N,ZYD(I),MU(I),EXPOS(I)
                IF(EXPOS(i).lt.0.)then
                       write(*,*)'maille ',i,' abscisse ',tmail(i),
     :'exposant de deversement negatif'
                 stop
            elseif(expos(i).lt.eps)then
                     if(mu(i).lt.0.)then
                       write(*,*)'maille ',i,' abscisse ',tmail(i),
     :'exposant de deversement nul et coefficient negatif',
     :'apport lateral constant a rentrer dans hydevs'
              stop
               else
C on transforme mu de debit en debit lineaire
                 mu(i)=mu(i)/(XTMAIL(i)-XTMAIL(i-1))
           endif
         endif
C               VOLDEV(I)=0.
C en cas de deversement hors prises eau claire
          EFFPRI(I)=0.
C diametre et etendue des apports
          DLAT(I)=0.
          SLAT(I)=0.
        ENDDO
C fin boucle sur IB
        enddo
        CLOSE(26)
        DO IB=1,NBB
                IF(ABS(MU(LM(IB-1)+1)).GT.EPS)THEN
                                        MU(LM(IB-1)+1)=0.
C            write(*,*)'pas de deversement possible en debut de bief ',IB
C                  stop
                ENDIF
                IF(abs(MU(LM(IB))).GT.EPS)THEN
                                        MU(LM(IB))=0.
C            write(*,*)'pas de deversement possible en fin de bief ',IB
C                  stop
                ENDIF
        ENDDO

C Ouverture du fichier hydevs.etude
C----------------------------------
C QSMU(I) peut �tre un d�bit solide ou une concentration...
C mais seul le cas debit solide (unisol=1) est traite de facon sure

        J=1
              J2=1
        NOMFIC='hydevs.'//ETUDE
        OPEN(26,FILE=NOMFIC,FORM='FORMATTED',STATUS='OLD',ERR=232)
        NMU(0)=0
        N20=0
        IF(TRASED)THEN
 234    CONTINUE
        READ(26,*,END=228)N1,N2,N3,DMU,SMU
              IF(N2.lt.N1)THEN
                     write(*,*)'apport lateral mal affecte'
                     write(*,*)'maille amont ',n1,' < maille aval ',n2
                     stop
              endif
        DO IB=1,NBB
                IF(LM(IB-1)+1.EQ.N1)THEN
            write(*,*)'pas d''apport possible en debut de bief ',IB
                  stop
                ENDIF
                IF(LM(IB).EQ.N2)THEN
            write(*,*)'pas d''apport possible en fin de bief ',IB
                  stop
                ENDIF
        ENDDO
              NMIN(J2)=N1
        NMAX(J2)=N2
              J2=J2+1
        DO I=N1,N2
          DLAT(I)=DMU
          SLAT(I)=SMU
        ENDDO
        DX=XTMAIL(N2)-XTMAIL(N1-1)
        DO I=J,J+N3-1
          READ(26,*)QMU(I),TMU(I),QSMU(I),TSMU(I)
          QMU(I)=QMU(I)/DX
C si debit solide
          IF(UNISOL.EQ.1)THEN
            QSMU(I)=QSMU(I)/DX
C            QSMU(I)=QSMU(I)/FLOAT(N2-N1+1)
          ENDIF
        ENDDO
        DO I=N20+1,N1-1
          NMU(I)=J-1
        ENDDO
        NMU(N1)=J+N3-1
        DO K=N1+1,N2
          J=J+N3
          NMU(K)=J+N3-1
          DO I=J,J+N3-1
            QMU(I)=QMU(I-N3)
            TMU(I)=TMU(I-N3)
            QSMU(I)=QSMU(I-N3)
            TSMU(I)=TSMU(I-N3)
          ENDDO
        ENDDO
        J=J+N3
        N20=N2
        GOTO 234
 228    CONTINUE

C si pas de transport solide
        ELSE
 1234    CONTINUE
        READ(26,*,END=1228)N1,N2,N3
              IF(N2.lt.N1)THEN
                     write(*,*)'apport lateral mal affecte'
                     write(*,*)'maille amont ',n1,' < maille aval ',n2
                     stop
              endif
        DO IB=1,NBB
                IF(LM(IB-1)+1.EQ.N1)THEN
            write(*,*)'pas d''apport possible en debut de bief ',IB
                  stop
                ENDIF
                IF(LM(IB).EQ.N2)THEN
            write(*,*)'pas d''apport possible en fin de bief ',IB
                  stop
                ENDIF
        ENDDO
              NMIN(J2)=N1
        NMAX(J2)=N2
              J2=J2+1
        DO I=N1,N2
          DLAT(I)=1.
          SLAT(I)=1.
        ENDDO
        DX=XTMAIL(N2)-XTMAIL(N1-1)
        DO I=J,J+N3-1
          READ(26,*)QMU(I),TMU(I)
          QMU(I)=QMU(I)/DX
          QSMU(I)=0.
        ENDDO
        DO I=N20+1,N1-1
          NMU(I)=J-1
        ENDDO
        NMU(N1)=J+N3-1
        DO K=N1+1,N2
          J=J+N3
          NMU(K)=J+N3-1
          DO I=J,J+N3-1
            QMU(I)=QMU(I-N3)
            TMU(I)=TMU(I-N3)
            QSMU(I)=0.
            TSMU(I)=TMU(I)
          ENDDO
        ENDDO
        J=J+N3
        N20=N2
        GOTO 1234
 1228   CONTINUE
C fin du if sur trased
        ENDIF
        DO I=N20,LM(NBB)
          NMU(I)=J-1
        ENDDO
        CLOSE(26)
        WRITE(*,'(A,A)')'                   et: ',NOMFIC
 232    NMIN(J2)=LM(NBB)+1
        NMax(J2)=LM(NBB)+1
C 231    CONTINUE

C modification du 29 juillet 2009
C on permet des deversements et des apports au meme endroit
C donc hydevs independant de devers
C mais necessite de redefinir les zones de deversements
C en tenant compte des apports
c--------------------------------
        I=0
        K=1
              J=1
223     CONTINUE
        IF(MU(K).NE.0.)THEN
          IF(INDDEV(I).GE.K)THEN
C dans ce cas c'est un nmax
c            IF(nmin(j-1).lt.k)then
                       INDDEV(i)=k-1
c                  endif
                  I=I+1
               INDDEV(I)=K
                  K=K+1
                  j=j+1
            GOTO 223
                ENDIF
          I=I+1
224       CONTINUE
          INDDEV(I)=K
          K=K+1
          IF(K.GT.LM(NBB))GOTO 227
          IF(K.EQ.NMin(j))then
                  I=I+1
            INDDEV(I)=nmax(j)
                     j=j+1
            K=K+1
            GOTO 223
          ELSEIF(K.EQ.NMAX(j))THEN
C dans ce cas k ne peut etre plus grand que lm(nbb)
            I=I+1
               INDDEV(I)=K
                  K=K+1
                  j=j+1
            GOTO 223
          elseIF(MU(K).EQ.0.)THEN
            K=K+1
            IF(K.GT.LM(NBB))GOTO 227
            GOTO 223
          ELSE
            GOTO 224
          ENDIF
        ELSEIF(K.EQ.NMin(j))THEN
C dans ce cas k ne peut etre plus grand que lm(nbb)
                I=I+1
             INDDEV(I)=NMAX(J)
                K=K+1
                j=j+1
          GOTO 223
              ELSE
          K=K+1
          IF(K.GT.LM(NBB))GOTO 227
          GOTO 223
        ENDIF
227     CONTINUE
        INDDEV(I+1)=LM(NBB)+1

C Ouverture du fichier prises.etude
C----------------------------------
        NOMFIC='prises.'//ETUDE
        OPEN(26,FILE=NOMFIC,FORM='FORMATTED',STATUS='OLD',ERR=241)
        K=0
        DO WHILE(.TRUE.)
          READ(26,*,END=303) X,TYP,ZD,MUD,HP,LD,CFA,CFB,EFF
C          IF((X.LT.TMAIL(1)).OR.(X.GT.TMAIL(LM))) GOTO 304
          K=K+1
          I=0
          DO WHILE(TMAIL(I).GT.X.AND.TMAIL(I+1).LE.X)
            I=I+1
          ENDDO
          ZYD(I)=ZD
          MU(I)=MUD
          OUV(I)=HP
          LOND(I)=LD/(XTMAIL(I)-XTMAIL(I-1))
          COEFA(I)=CFA*LD**CFB
          COEFB(I)=CFB
          EFFPRI(I)=EFF
          IF(TYP.EQ.1)THEN
            EXPOS(I)=-1.000
          ELSEIF(TYP.EQ.2)THEN
            EXPOS(I)=-2.000
          ELSE
            WRITE(*,*)'PROBLEME AVEC LE TYPE DE PRISE DANS prises'
            STOP
          ENDIF
C        Write(*,'(2I3,8F7.3)')K,I,ZYD(I),MU(I),OUV(I),LOND(I)
C     &   ,COEFA(I),COEFB(I),EFFPRI(I),EXPOS(I)
          CONTINUE
C 304      CONTINUE
        ENDDO
 303    CONTINUE
        CLOSE(26)
        WRITE(*,'(A,A)')'Prises d''irrigation: ',NOMFIC
 241    CONTINUE

      ENDIF


C 2)---->ON LIT SUR LE FICHIER 23 L HYDROGRAMME AMONT :
C------------------------------------------------------
          CHAINE='CONDITION LIMITE AMONT '
         LECHYDRO=.FALSE.
          NT1(0)=0

C        IF (CONDAM.EQ.1) THEN
        DO 9530 M=1,NBB
          IF (CONDAM(M).EQ.1)then
                       LECHYDRO=.TRUE.
                ELSEIF (CONDAM(M).EQ.4)then
                       IF(NBB.GT.1)THEN
            WRITE(*,*) 'BIEF ',M,' ',CHAINE//' INTERNE '
                    else
            WRITE(*,*) CHAINE//' INTERNE '
                    endif
COU2D                ELSEIF (CONDAM(M).EQ.6)then
COU2D                       IF(NBB.GT.1)THEN
COU2D            WRITE(*,*) 'BIEF ',M,' ',CHAINE//' COUPLAGE AVEC 2D'
COU2D                    else
COU2D            WRITE(*,*) CHAINE//' COUPLAGE AVEC 2D'
COU2D                    endif
                ELSEIF (CONDAM(M).EQ.0)then
                       IF(NBB.GT.1)THEN
            WRITE(*,*) 'BIEF ',M,' ',CHAINE//' REFLEXION '
                    else
            WRITE(*,*) CHAINE//' REFLEXION '
                    endif
C fin du if sur condam
           endif
 9530   CONTINUE
C passage au suivant (hamont)
        IF(LECHYDRO)THEN
         NOMFIC='hydro.'//ETUDE
C          NOMFIC='hydro.'//ETUDE
        OPEN(23,FILE=NOMFIC,STATUS='OLD',ERR=9756)
        NT1(0)=0

        M=1
        IF (CONDAM(M).NE.1) THEN
          NT1(1)=0
        ELSE
          READ(23,*) NT1(M)
          NT1(M)=NT1(M-1)+NT1(M)
          IF(NT1(M).GT.NTHMAX) THEN
      WRITE(*,*) 'LE NOMBRE DE DEBITS AMONT DOIT ETRE INFERIEUR A ',
     :NTHMAX
              STOP
          ENDIF
          IF(NT1(M).GT.NT1(M-1))THEN
            READ(23,*)QTH(NT1(M-1)+1),TH(NT1(M-1)+1)
                  IF(NBB.GT.1)THEN
            WRITE(*,*) 'BIEF ',M,' ',CHAINE//' EN DEBIT '
                  Else
            WRITE(*,*) CHAINE//' EN DEBIT '
                  endif
            DO 1 I=NT1(M-1)+2,NT1(M)
              READ(23,*)QTH(I),TH(I)
              IF(TH(I).LT.TH(I-1))THEN
                 WRITE(*,*)'TEMPS DEBITS AMONT NON CROISSANTS BIEF ',M
                 STOP
              ENDIF
1           CONTINUE
         ENDIF
         ENDIF
         DO 1105 M=2,NBB
          IF (CONDAM(M).NE.1) THEN
           NT1(M)=NT1(M-1)
          ELSE
           READ(23,*) NT1(M)
          NT1(M)=NT1(M-1)+NT1(M)
           IF(NT1(M).GT.NTHMAX) THEN
      WRITE(*,*) 'LE NOMBRE DE DEBITS AMONT DOIT ETRE INFERIEUR A ',
     :NTHMAX
              STOP
           ENDIF

           IF(NT1(M).GT.NT1(M-1))THEN
                  IF(NBB.GT.1)THEN
            WRITE(*,*) 'BIEF ',M,' ',CHAINE//' EN DEBIT '
                  Else
            WRITE(*,*) CHAINE//' EN DEBIT '
                  endif
             READ(23,*)QTH(NT1(M-1)+1),TH(NT1(M-1)+1)
             DO 9001 I=NT1(M-1)+2,NT1(M)
               READ(23,*)QTH(I),TH(I)
               IF(TH(I).LT.TH(I-1))THEN
                 WRITE(*,*)'TEMPS DEBITS AMONT NON CROISSANTS BIEF',M
                 STOP
               ENDIF
 9001        CONTINUE
           ENDIF
          ENDIF
 1105    CONTINUE
         GO TO 2001
 9756    WRITE(*,*)'LE FICHIER hydro N''EXISTE PAS'
         STOP
 2001     CLOSE(23)
C fin du if sur lechydro
        ENDIF
C PROV POUR REGIME
         LECHAMONT=.FALSE.
         DO 9540 M=1,NBB
            IF (REGIME(M).EQ.1) THEN
                            lechamont=.true.
                     endif
 9540     CONTINUE
C passage au suivant
         If(lechamont)then
         NOMFIC='hamont.'//ETUDE
         NT3(0)=0
         OPEN(56,FILE=NOMFIC,FORM='FORMATTED',STATUS='OLD',ERR=1756)
         DO 9550 M=1,NBB
C           READ(90,'(A15)',END=1756) CHAIN
C           READ(90,'(A31)',END=1000) CHAIN2
C           DO 7520 J=2,15
C             IF(CHAIN(J-1:J-1).NE.' '.AND.CHAIN(J:J).EQ.' ')THEN
C               WRITE(CHAIN3,'(I2)')J-1
C               CHAIN3='(I'//CHAIN3(1:2)//')'
C               READ(CHAIN,CHAIN3)NT3(1)
C               GO TO 7521
C             ENDIF
C 7520      CONTINUE
C         READ(CHAIN,'(I15)')NT3(M)
C 7521    READ(CHAIN2,'(F15.6,1X,F15.6)')
C     :    HIMP(NT3(M-1)+1),TA(NT3(M-1)+1)
          IF (REGIME(M).NE.1)THEN
            NT3(M)=NT3(M-1)
          ELSE
           READ(56,*)NT3(M)
          NT3(M)=NT3(M-1)+NT3(M)
           IF(NT3(M).GT.NCLMAX) THEN
      WRITE(*,*)'LE NOMBRE DE COTES AMONT DOIT ETRE INFERIEUR A',
     :NCLMAX
             STOP
           ENDIF
           IF(NT3(M).GT.NT3(M-1))THEN
                  IF(NBB.GT.1)THEN
             WRITE(*,*) 'BIEF ',M,' ',CHAINE//' EN COTE'
                  Else
             WRITE(*,*) CHAINE//' EN COTE'
                  endif
             READ(56,*)HIMP(NT3(M-1)+1),TA(NT3(M-1)+1)
             DO 1003 I=NT3(M-1)+2,NT3(M)
               READ(56,'(F15.6,1X,F15.6)')HIMP(I),TA(I)
               IF(TA(I).LT.TA(I-1))THEN
                 WRITE(*,*)'TEMPS COTES AMONT NON CROISSANTS BIEF ',M
                 STOP
               ENDIF
 1003        CONTINUE
           ENDIF
          ENDIF
 9550    CONTINUE
         GO TO 1001
 1756    WRITE(*,*)'LE FICHIER hamont N''EXISTE PAS'
         STOP
 1001    CLOSE(56)
C fin du if sur lechamont
        endif
C 3)---->ON LIT SUR LE FICHIER 51 LA CONDITION A LA LIMITE AVAL
C---------------------------------------------------------------
      CHAINE='CONDITION LIMITE AVAL '
         LECCONDAV=.FALSE.

      NOMFIC='condav.'//ETUDE
      DO 9570 M=1,NBB
          IF (CONDAV(M).EQ.1.OR.CONDAV(M).EQ.3) then
                  leccondav=.true.
          elseIF (CONDAV(M).EQ.0) THEN
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
            WRITE(*,*) CHAINE//'FLUX LIBRE'
          ELSE IF (CONDAV(M).EQ.2)THEN
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
            WRITE(*,*) CHAINE//'REFLEXION'
          ELSE IF (CONDAV(M).EQ.5) THEN
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
            WRITE(*,*) CHAINE//'REGIME UNIFORME'
            IF(PEN(LM(M)-1).LE.EPS)THEN
               PEN(LM(M)-1)=EPS
               WRITE(*,*)'REGIME UNIFORME AVAL IMPOSSIBLE'
               WRITE(*,*)'CAR PENTE NEGATIVE ou NULLE'
            ENDIF
          ELSE IF (CONDAV(M).EQ.7) THEN
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
            WRITE(*,*) CHAINE//'REGIME CRITIQUE'
          ELSE if (CONDAV(M).EQ.4) then
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
            WRITE(*,*) CHAINE//'INTERNE'
COU2D          ELSE if (CONDAV(M).EQ.6) then
COU2D                  IF(NBB.GT.1)THEN
COU2D               WRITE(*,*) 'BIEF ',M
COU2D                  ENDIF
COU2D            WRITE(*,*) CHAINE//'COUPLAGE AVEC 2D'
C fin du if sur condav
                     ENDIF

 9570 CONTINUE
C passage au suivant
      if(leccondav)then
       OPEN(51,FILE=NOMFIC,STATUS='OLD',ERR=9757)

      NT2(0)=0
      DO 1111 M=1,NBB
        IF(CONDAV(M).NE.1.AND.CONDAV(M).NE.3) THEN
          NT2(M)=NT2(M-1)
        ELSE
          READ(51,*) NT2(M)
          NT2(M)=NT2(M-1)+NT2(M)
          IF(CONDAV(M).EQ.1)THEN
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
             WRITE(*,*) CHAINE//'COURBE DE TARAGE '
          ELSE
                  IF(NBB.GT.1)THEN
               WRITE(*,*) 'BIEF ',M
                  ENDIF
             WRITE(*,*) CHAINE//'HAUTEUR IMPOSEE '
          ENDIF
          IF(NT2(M).GT.NCLMAX) THEN
      WRITE(*,*) 'LE NOMBRE DE DONNEES AVAL DOIT ETRE INFERIEUR A ',
     :NCLMAX
              STOP
          ENDIF

          IF((NT2(M).GT.NT2(M-1)))THEN
             I=NT2(M-1)+1
             READ(51,*)QYH(I),YH(I)
c             YH(I)=YH(I)-CTDF(LM(M))
             DO 2 I=NT2(M-1)+2,NT2(M)
               READ(51,*)QYH(I),YH(I)
c               YH(I)=YH(I)-CTDF(LM(M))

               IF(CONDAV(M).EQ.1)THEN
                IF(YH(I).LT.YH(I-1)) THEN
                 WRITE(*,*)'COTES DONNEES AVAL NON CROISSANTES BIEF ',M
                 STOP
                ENDIF
               ENDIF

2            CONTINUE
           ENDIF

        ENDIF
1111  CONTINUE
      GO TO 3001
 9757 WRITE(*,*)'LE FICHIER condav N''EXISTE PAS'
      STOP
 3001 CLOSE(51)
c fin du if sur leccondav
      endif
C 4)---->RECUPERATION DES ABSCISSES DES HYDROGRAMMES
C     DE SORTIE ET DES TEMPS DE SAUVEGARDE DES PROFILS
C-------------------------------------------------------

      IF(TF(2).EQ.'O')THEN
        NOMFIC='abshyd.'//ETUDE
        OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(56,*,ERR=706,END=706)NBSSAV
              DO N=1,NBSSAV
                NBMAIL(N)=0
              ENDDO
        IF (NBSSAV.NE.0) THEN
          DO 1005 N=1,NBSSAV
              READ(56,'(A80)',ERR=706,END=706)LIGNE
                 M=0
           READ(LIGNE,*,ERR=701,END=701)J,MAILLE,M
                 go to 705
 701       READ(LIGNE,*,ERR=702,END=702)J,MAILLE
           go to 705
 702       READ(LIGNE,'(I4,1X,F11.3,1X,I4)',ERR=703,END=703)J,MAILLE,M
           go to 705
 703       READ(LIGNE,'(I4,1X,F11.3)',ERR=704,END=704)J,MAILLE
           go to 705
 704       READ(LIGNE,'(I12,1X,F24.3)')J,MAILLE
C si numero bief errone renvoi vers le passage sans numero bief
 705       IF(M.LT.1.OR.M.GT.NBB)GO TO 700
           DO J=LM(M-1)+2,LM(M)
             IF(TMAIL(J).GE.MAILLE.AND.TMAIL(J-1).LE.MAILLE)THEN
               NBMAIL(N)=J-1
               DELMAI(N)=(MAILLE-TMAIL(J-1))/(TMAIL(J)-TMAIL(J-1))
C renvoi vers l'abscisse suivante
               GOTO 1005
             ENDIF
          ENDDO
                WRITE(*,*)'abscisse hydrogramme stockage numero ',n
     :,' erronee'
C renvoi vers l'abscisse suivante
                GO TO 1005
 700      DO 9904 M=1,NBB
           DO 1004 J=LM(M-1)+2,LM(M)
             IF(TMAIL(J).GE.MAILLE.AND.TMAIL(J-1).LE.MAILLE)THEN
               NBMAIL(N)=J-1
               DELMAI(N)=(MAILLE-TMAIL(J-1))/(TMAIL(J)-TMAIL(J-1))
               GOTO 1005
             ENDIF
1004       CONTINUE
 9904     CONTINUE
c fin boucle surle nombre d' abscisses
1005      CONTINUE
        ENDIF
 706     CLOSE(56)
       ENDIF
C       IF(TF(4).EQ.'O')THEN
         NOMFIC='tnprof.'//ETUDE
         OPEN(56,FILE=NOMFIC,STATUS='OLD',ERR=1232)
         READ(56,*,END=1232)NTSOR
         IF (NTSOR.NE.0) THEN
          DO 11 I=1,NTSOR
             READ(56,*)J,TSOR(I)
11        CONTINUE

         IF(NTSOR.EQ.1)GO TO 410
C.....REARRANGEMENT DES TEMPS
         DO 41 I=2,NTSOR
          IF(TSOR(I).LT.TSOR(I-1))THEN
            IF(I.EQ.2)THEN
              XTS=TSOR(2)
              TSOR(2)=TSOR(1)
              TSOR(1)=XTS
            ELSE
              DO 42 J=I,2,-1
              IF(TSOR(I).LT.TSOR(I-J))THEN
                XTS=TSOR(I)
                DO 43 K=1,J
                  TSOR(I-K+1)=TSOR(I-K)
43              CONTINUE
                TSOR(I-J)=XTS
                GO TO 41
              ENDIF
42            CONTINUE
            ENDIF
          ENDIF
41       CONTINUE
         ENDIF
 410     CLOSE(56)
C       ENDIF


      RETURN
 231  write(*,*)'le fichier devers.',ETUDE,' n''existe pas'
      stop
         return
 1232  NTSOR=0
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE LOUVR1D
C-----------------------------------------------------------------------
C Initialise les variables relatives aux ouvrages
C Lit leurs caract�ristiques dans le fichier 'ouvrag.etude'
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,NBB,NOB1DMAX
      PARAMETER(LMAX=3000,NBMAX=150,NOB1DMAX=10)
      INTEGER LM(0:NBMAX),LL
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000)
      DOUBLE PRECISION QOUV(LMAX,2),HOUV(LMAX,2)
      DOUBLE PRECISION XIA1,XIA2,ZORI
      INTEGER IOUV,IDON,NREF,I,J,K,IBOUV,NOB,IDON2
      CHARACTER ETUDE*20,NOMFIC*40,RIEN*9
      LOGICAL TROUVE
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax),NBOUV
     :,NREFA(LMAX),NBCOUP,NBCOU2(nou1Dmax,noe1Dmax),NBCOUM
     :,NBCOU1(nou1Dmax,noe1Dmax),nb1(nou1Dmax),NB2(nou1Dmax)
      DOUBLE PRECISION LONG(nou1Dmax,noe1Dmax),ZDEV(nou1Dmax,noe1Dmax)
     &  ,HAUT(nou1Dmax,noe1Dmax),COEF(nou1Dmax,noe1Dmax)
     &  ,ZOUV(nou1Dmax,noe1Dmax),COEFIN(nou1Dmax,noe1Dmax)
     &  ,ZFERM(nou1Dmax,noe1Dmax)
     &  ,QCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
     :,ZCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
      CHARACTER*1 TYPOUV(nou1Dmax,noe1Dmax)
      INTEGER IT(NOB1DMAX),NT(NOB1DMAX),IOUB(nou1Dmax,noe1Dmax)
      DOUBLE PRECISION NU(NOB1DMAX)
      DOUBLE PRECISION KA(NOB1DMAX),DT2(NOB1DMAX)
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),C1(NOB1DMAX),C2(NOB1DMAX)
     :,DBMAX(NOB1DMAX),TRUP(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX)
      DOUBLE PRECISION QL(ntr1Dmax,NOB1DMAX),QS(ntr1Dmax,NOB1DMAX)
     :,ZBR(ntr1Dmax,NOB1DMAX),DBR(ntr1Dmax,NOB1DMAX)
     :,Z(ntr1Dmax,NOB1DMAX)
     :  ,ZAV(ntr1Dmax,NOB1DMAX),TRECT(NOB1DMAX)
     :  ,tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
C si volqouv true ecriture fichier volqouv
c car nref=-1 pour au moins une arete
      LOGICAL ELAP(NOB1DMAX),VOLQOUV
         CHARACTER*1 rep,ligne*80,nomfic2*40

      COMMON/ITEM/REP
      COMMON/PHYS/LM,LL
      COMMON/NOMETU/ETUDE
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDCOUVRA/LONG,ZDEV,HAUT,COEF
      COMMON/DDTOUVRA/TYPOUV
      COMMON/DDNARF/NREFA
      COMMON/YOUVRA/COEFIN,ZOUV,ZFERM
      COMMON/DDNOUVRA/NBCOU1,NBCOU2
      COMMON/DDZOUVRA/QCOUP,ZCOUP
      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDRESUL/QL,QS,DBR,Z,ZAV,ZBR
      COMMON/DDTINIB/TRUP
      COMMON/DDELAPPR/ELAP
      COMMON/DDIOBREC/IOUB,NOB
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDBRECHE/ZB,DB,IT
C      COMMON/ZBAS/ZFM
      COMMON/NBIEF/NBB
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      common/volqouv/volqouv


C Initialisations
C-----------------------------------------------------------------------
C IBOUV indice du nombre d'ouvrages B
      IBOUV=0
      NOB=0
         volqouv=.FALSE.

      DO J=1,LM(NBB)
        HOUV(J,1)=0.
        HOUV(J,2)=0.
        QOUV(J,1)=0.
        QOUV(J,2)=0.
      ENDDO

C Lecture de 'ouvrag.etude'
C-----------------------------------------------------------------------
      NBOUV=0
      NBCOUM=0
      TROUVE=.FALSE.
      IDON=9
         IDON2=8
      NOMFIC='ouvrag.'//ETUDE
      OPEN(IDON,FILE=NOMFIC,STATUS='OLD',ERR=1)
C si reprise on essaie de lire ouvrag-r
      IF ((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
         NOMFIC2='ouvrag-r.'//ETUDE
      OPEN(IDON2,FILE=NOMFIC2,STATUS='OLD',ERR=2)
         CLOSE(idon2)
         close(idon)
         NOMFIC='ouvrag-r.'//ETUDE
      OPEN(IDON,FILE=NOMFIC,STATUS='OLD')
         endif
 2    WRITE(*,'(A,A)')'Ouvrages: ',NOMFIC
      DO IOUV=1,nou1Dmax
C NREF=0: pas d'ouvrage, le d�bit est donn� par Saint-Venant
C NREF=-1: le d�bit de l'ouvrage se rajoute au d�bit de Saint-Venant
C NREF=-2: le seul d�bit est le d�bit de l'ouvrage
        nb1(iouv)=0
              nb2(iouv)=0
        READ(IDON,'(A80)',ERR=5,END=8)ligne
        READ(LIGNE,*,ERR=115,END=115)
     :XIA1,NREF,NB1(IOUV)
           go to 15
 115    READ(LIGNE,'(F11.3,1X,I4,1X,I4)',ERR=215,END=215)
     :XIA1,NREF,NB1(IOUV)
           go to 15
 215    READ(LIGNE,'(F11.3,1X,I4)',ERR=315,END=315)
     :XIA1,NREF
           go to 15
 315    READ(LIGNE,'(F19.3,1X,I16)',ERR=415,END=415)
     :XIA1,NREF
           go to 15
 415    read(ligne,*)xia1,nref
 15    if(nb1(iouv).lt.1.or.nb1(iouv).gt.nbb)then
                 nb1(iouv)=0
              endif
               if(nref.EQ.-1)volqouv=.true.
        CALL TRXYI(XIA1,IA1(IOUV),NB1(IOUV),TROUVE)
        IF(TROUVE)THEN
          TROUVE=.FALSE.
        ELSE
          WRITE(*,*)'PROBLEME coordonnees de l''interface amont '
     &      ,'fausses ouvrage ',IOUV
          STOP
        ENDIF
C NOUV nombre d'ouvrages �l�mentaires
        READ(IDON,'(A80)',ERR=5)ligne
        READ(LIGNE,*,ERR=25,END=25)
     : XIA2,NOUV(IOUV),NB2(IOUV)
           go to 26
 25     READ(LIGNE,'(F11.3,1X,I4,1X,I4)',ERR=125,END=125)
     : XIA2,NOUV(IOUV),NB2(IOUV)
           go to 26
 125     READ(LIGNE,*,ERR=225,END=225)
     : XIA2,NOUV(IOUV)
           go to 26
 225     READ(LIGNE,'(F11.3,1X,I4)',ERR=325,END=325)
     : XIA2,NOUV(IOUV)
           go to 26
 325     READ(LIGNE,'(F19.3,1X,I16)')
     : XIA2,NOUV(IOUV),NB2(IOUV)
 26          if(nb2(iouv).lt.1.or.nb2(iouv).gt.nbb)then
            nb2(iouv)=0
       endif
       IF(NOUV(IOUV).GT.noe1Dmax)THEN
          WRITE(*,*)'PROBLEME trop d''ouvrages elementaires; NBMAX='
     &      ,noe1Dmax
          STOP
        ENDIF
        CALL TRXYI(XIA2,IA2(IOUV),NB2(iouv),TROUVE)
        IF(TROUVE)THEN
          TROUVE=.FALSE.
          WRITE(*,'(A,I2,A,I2,A)')'- ouvrage ',IOUV,' compose de '
     &      ,NOUV(IOUV),' ouvrage(s) elementaire(s)'
        ELSE
          WRITE(*,'(A,I2,A,I2,A,A,A)')'- ouvrage ',IOUV,' compose de '
     &      ,NOUV(IOUV),' ouvrage(s) elementaire(s); le debit est '
     &      ,'evacue du modele car les coordonnees de l''interface '
     &      ,'aval sont sans correspondance'
          IA2(IOUV)=0
        ENDIF
        DO I=IA1(IOUV),IA2(IOUV),1
          NREFA(I)=NREF
        ENDDO

C Lecture des caract�ristiques des ouvrages au format Fluvia
C Les cotes sont des cotes absolues...
        DO J=1,NOUV(IOUV)
          READ(IDON,'(A1,A9,F10.0,F10.0,F10.0,F10.0)',ERR=5)
     &   TYPOUV(IOUV,J),RIEN,LONG(IOUV,J),ZDEV(IOUV,J),ZORI,COEF(IOUV,J)

C Ouvrage de type D
          IF(TYPOUV(IOUV,J).EQ.'D'.OR.TYPOUV(IOUV,J).EQ.'d')THEN
            TYPOUV(IOUV,J)='D'
            HAUT(IOUV,J)=ZORI-ZDEV(IOUV,J)

C Ouvrage de type Y
          ELSEIF(TYPOUV(IOUV,J).EQ.'Y'.OR.TYPOUV(IOUV,J).EQ.'y')THEN
            TYPOUV(IOUV,J)='Y'
            READ(IDON,*)COEFIN(IOUV,J),ZOUV(IOUV,J),ZFERM(IOUV,J)
            HAUT(IOUV,J)=ZORI-ZDEV(IOUV,J)

C O correspond au meme ouvrage que D mais avec une longueur et en circulaire
          ELSEIF(TYPOUV(IOUV,J).EQ.'O'.OR.TYPOUV(IOUV,J).EQ.'o')THEN
            TYPOUV(IOUV,J)='O'
C haut contient le diametre
            HAUT(IOUV,J)=ZORI
C            ZDEV(IOUV,J)=ZDEV(IOUV,J)-ZFM

C Ouvrage de type Z
          ELSEIF(TYPOUV(IOUV,J).EQ.'Z'.OR.TYPOUV(IOUV,J).EQ.'z')THEN
            TYPOUV(IOUV,J)='Z'
            READ(RIEN,'(I9)')NBCOUP
            IF(NBCOUP.LT.1.OR.NBCOUP.GT.nou1Dmax*noe1Dmax)THEN
              WRITE(*,*)'PROBLEME erreur dans le nombre de couples '
     &  ,NBCOUP,' de l''ouvrage ',IOUV,'. NBMAX=',nou1Dmax*noe1Dmax
              STOP
            ENDIF
            NBCOU1(IOUV,J)=NBCOUM+1
            NBCOUM=NBCOUM+NBCOUP
            IF(NBCOUM.GT.nou1Dmax*noe1Dmax*noe1Dmax)THEN
              WRITE(*,*)'PROBLEME ',NBCOUM,' couples (Z,Q) ou (t,Q) '
     &   ,'ouvrage ',IOUV,' pour NBMAX=',nou1Dmax*noe1Dmax*noe1Dmax
              STOP
            ENDIF
            NBCOU2(IOUV,J)=NBCOUM
            DO K=NBCOU1(IOUV,J),NBCOU2(IOUV,J)
              READ(IDON,*,ERR=5)ZCOUP(K),QCOUP(K)
c              ZCOUP(K)=ZCOUP(K)-ZFM
              IF(K.NE.NBCOU1(IOUV,J))THEN
                IF(ZCOUP(K).LT.ZCOUP(K-1))THEN
                  WRITE(*,*)'ATTENTION: cotes non croissantes au '
     &              ,'couple (Z,Q) numero ',K,' d''un ouvrage de type Z'
                ENDIF
              ENDIF
            ENDDO

C Ouvrage de type Q
          ELSEIF(TYPOUV(IOUV,J).EQ.'Q'.OR.TYPOUV(IOUV,J).EQ.'q')THEN
            TYPOUV(IOUV,J)='Q'
            READ(RIEN,'(I9)')NBCOUP
            IF(NBCOUP.LT.1.OR.NBCOUP.GT.nou1Dmax*noe1Dmax)THEN
              WRITE(*,*)'PROBLEME erreur dans le nombre de couples '
     &    ,NBCOUP,' de l''ouvrage ',IOUV,'. NBMAX=',nou1Dmax*noe1Dmax
              STOP
            ENDIF
            NBCOU1(IOUV,J)=NBCOUM+1
            NBCOUM=NBCOUM+NBCOUP
            IF(NBCOUM.GT.nou1Dmax*noe1Dmax*noe1Dmax)THEN
              WRITE(*,*)'PROBLEME ',NBCOUM,' couples (Z,Q) ou (t,Q) '
     &    ,'ouvrage ',IOUV,' pour NBMAX=',nou1Dmax*noe1Dmax*noe1Dmax
              STOP
            ENDIF
            NBCOU2(IOUV,J)=NBCOUM
            DO K=NBCOU1(IOUV,J),NBCOU2(IOUV,J)
              READ(IDON,*,ERR=5)ZCOUP(K),QCOUP(K)
              IF(K.NE.NBCOU1(IOUV,J))THEN
                IF(ZCOUP(K).LT.ZCOUP(K-1))THEN
                  WRITE(*,*)'ATTENTION: temps non croissants au '
     &              ,'couple (T,Q) numero ',K,' d''un ouvrage de type Q'
                ENDIF
              ENDIF
            ENDDO

C Ouvrage de type B
          ELSEIF(TYPOUV(IOUV,J).EQ.'B'.OR.TYPOUV(IOUV,J).EQ.'b')THEN
         IF(IBOUV.LT.NOB1DMAX)THEN
           IBOUV=IBOUV+1
           NOB=IBOUV
           IOUB(IOUV,J)=IBOUV
         ELSE
           WRITE(*,*)'LE NOMBRE OUVRAGES B DEPASSE LE MAXIMUM'
           WRITE(*,*)'DE ',NOB1DMAX,' POSSIBLES'
c           PAUSE
           STOP
         ENDIF
       TYPOUV(IOUV,J)='B'
       ZC(IBOUV)=LONG(IOUV,J)
       ZP(IBOUV)=ZDEV(IOUV,J)
       ALC(IBOUV)=ZORI
       ALP(IBOUV)=COEF(IOUV,J)
       TRECT(ibouv)=-999999.
C si elap vrai alors contrainte reduite sur les berges et approfondissement avec elargissement
       READ(RIEN(1:1),'(I1)')NBCOUP

       IF(NBCOUP.EQ.1)THEN
         ELAP(IBOUV)=.TRUE.
       ELSE
         ELAP(IBOUV)=.FALSE.
       ENDIF
c       IF(ABS(TINI(IBOUV)).LT.0.05)THEN
       If(RIEN(2:9).EQ.'        ')THEN
         TRUP(IBOUV)=tinit1D
       ELSE
         READ(RIEN(2:9),'(F8.0)')TRUP(IBOUV)
       ENDIF

C ouverture du fichier pour resultats de ouvrage B
       CALL OUVERB(IBOUV)
C ouverture reportee a chaque stockage
C      SOUS-PROGRAMME DE LECTURE ET DE CONTROLE DES DONNEES
       CALL LECTESB(IOUV,IBOUV)
C ecriture dans le fichier de resultats pour B
       CALL IMPDONB(IOUV,IBOUV)
C inutile sauf peut etre pour ecriture
      Z(1,IBOUV)=Z0(IBOUV)
         ZB(IBOUV)=ZB0(IBOUV)
      ZAV(1,IBOUV)=Z0(IBOUV)
      DBR(1,IBOUV)=DB(IBOUV)
      ZBR(1,IBOUV)=ZB(IBOUV)
      QS(1,IBOUV)=0.
      QL(1,IBOUV)=0.



C Ouvrage de type T
c          ELSEIF(TYPOUV(IOUV,J).EQ.'T'.OR.TYPOUV(IOUV,J).EQ.'t')THEN
c            KOUVB=KOUVB+1
c            IF(KOUVB.GT.1)THEN
c              write(*,*) 'plus de 1 ouvrage de type B ou T'
c              write(*,*)'impossible dans la version actuelle'
c              STOP
c            ENDIF
c            TYPOUV(IOUV,J)='T'
c            ZC=LONG(IOUV,J)
c            ZP=ZDEV(IOUV,J)
c            ALC=ZORI
c            ALP=COEF(IOUV,J)
c            CALL OUVERB
c            CALL LECTESB(IOUV)
c            Z(1)=Z0
c            ZB=ZB0
c            DB=DB0
c            R(1)=DB
c            IF(IT.LT.0) R(1)=ZB

C Ouvrage de type inconnu
          ELSE
            WRITE(*,*)'ERREUR DANS LE TYPE D''OUVRAGE'
          ENDIF

        ENDDO
      ENDDO
      WRITE(*,*)'ATTENTION: le nombre maximal d''ouvrages ',nou1Dmax
     &  ,' a ete lu'
    8 NBOUV=IOUV-1
      CLOSE(IDON)
C      WRITE(*,'(A,A)')'ouvrag.',ETUDE
    1 CONTINUE
      RETURN
    5 WRITE (*,*)'ERREUR DANS LE FICHIER ouvrag.',ETUDE
      STOP

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CONDIN
C-----------------------------------------------------------------------
C Lecture des conditions initiales
C Sorties: YN1D,VN,SN,QN,YNP1,VNP1,SNP1,QNP1
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,II,IP
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN,T
      DOUBLE PRECISION SN(LMAX),VN(LMAX),QN(LMAX),YN1D(LMAX),RHN(LMAX)
     & ,RHNP1(LMAX)
      DOUBLE PRECISION SNP1(LMAX),QNP1(LMAX),VNP1(LMAX),YNP1(LMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      CHARACTER*40 NOMFIC,ETUDE*20,REP*1

      INTEGER LDETYJ
      DOUBLE PRECISION DETSN,DETCN,DETYRH
      EXTERNAL LDETYJ,DETSN,DETCN,DETYRH

      COMMON/NBIEF/NBB
      COMMON/NOMETU/ETUDE
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/DDNARF/NREFA
      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

C Lecture du fichier de conditions initiales
C-------------------------------------------
      IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
        NOMFIC='tps.'//ETUDE
        WRITE(*,'(A,F15.3,A,A)')
     &'Conditions initiales liquide (reprise a',tinit1D,'s): ',NOMFIC
        OPEN(32,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(32,*)TN
        IF(ABS( TN-tinit1D).LE.EPS)THEN
          TN=tinit1D
        ELSE
      WRITE(*,*)'TEMPS INITIAL DANS tps T = ',TN
      write(*,*)'DIFFERENT DU TEMPS INITIAL DANS donnee T =',tinit1D
          WRITE(*,*)'POURSUITE DU CALCUL IMPOSSIBLE'
          STOP
        ENDIF
c else di if sur reprise ou pas
      ELSE
        NOMFIC='condin.'//ETUDE
        WRITE(*,'(A,A)')'Conditions initiales liquide: ',NOMFIC
        OPEN(32,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(32,*)TN
        IF(ABS( TN-tinit1D).GT.EPS)THEN
      WRITE(*,*)'TEMPS INITIAL DANS condin T =',TN
      write(*,*)'DIFFERENT DE TEMPS INITIAL DANS donnee T =',tinit1D
c          WRITE(*,*)'POURSUITE DU CALCUL IMPOSSIBLE'
c          STOP
          ENDIF
          TN=tinit1D
C fin du if sur reprise ou pas
        ENDIF
              DO IB=1,NBB
          IF(IB.NE.1)THEN
C on ne controle pas que les biefs suivants soient aussi a tinit1D
            READ(32,*)T
          ENDIF
          DO I=LM(IB-1)+1,LM(IB)
            READ(32,*) II,YNP1(I),VNP1(I)
          ENDDO
              ENDDO
        CLOSE(32)

C Initialisation
C---------------
      DO I=1,LM(NBB)
        IF(I.NE.1)THEN
          IF(NREFA(I).EQ.-2.AND.NREFA(I-1).EQ.-2)THEN
            YNP1(I)=0.
            RHNP1(I)=0. !rayon hydraulique, va servir pour calculer les conditions solides initiales si l'on sp�cifie qu'elles sont �gales � la capacit� solide
            SNP1(I)=0.
            VNP1(I)=0.
            QNP1(I)=0.
            GOTO 20
          ENDIF
        ENDIF
        IF(YNP1(I).LT.EPSY)THEN
          RHNP1(I)=0.!rayon hydraulique, va servir pour calculer les conditions solides initiales si l'on sp�cifie qu'elles sont �gales � la capacit� solide
          SNP1(I)=0.
          QNP1(I)=0.
          YNP1(I)=0.
          VNP1(I)=0.
        ELSE
          IP=LDETYJ(0,YNP1(I),I)
          SNP1(I)=DETSN(0,YNP1(I),IP)
          RHNP1(I)=DETYRH(0,YNP1(I),I)
C rayon hydraulique, va servir pour calculer les conditions
C solides initiales si l'on sp�cifie qu'elles sont �gales
C � la capacit� solide
          IF(VNP1(I).GT.500.)THEN
            QNP1(I)=VNP1(I)-1000.
            VNP1(I)=QNP1(I)/SNP1(I)
          ELSE
            QNP1(I)=SNP1(I)*VNP1(I)
          ENDIF
        ENDIF
 20     CONTINUE
        YN1D(I)=YNP1(I)
        RHN(I)=RHNP1(I)
        SN(I)=SNP1(I)
        VN(I)=VNP1(I)
        QN(I)=QNP1(I)
      ENDDO

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE INITS(DM,SEGMA)
C-----------------------------------------------------------------------
C R�cup�re, calcule, initialise les grandeurs li�es au transport solide
C et les apports lateraux
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,NCLMAX,CSMAX,NBMAX,NBB
      PARAMETER(LMAX=3000,LNCMAX=130000,NCLMAX=1000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,II,J,NUMCS,IB
      INTEGER NC(0:LMAX),XNC(0:LMAX)
c      INTEGER XIP
      CHARACTER REP*1
      CHARACTER B*50,VERSION*26,optiontc*3,ligne4*40
      CHARACTER ETUDE*20,NOMFIC*40,LIGNE*20,LIGNE2*80,LIGNE3*50
      LOGICAL HYDSAM(NBMAX)
      INTEGER NTHSAM(0:NBMAX)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
c      INTEGER XJMIL
c      INTEGER LDETYJ
      DOUBLE PRECISION TN
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      DOUBLE PRECISION POR
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
c      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX),XVN,XRHN,XJRH,XCHR
c     &  ,XYN
c      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
c      DOUBLE PRECISION FR1(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XL(LMAX),XL3(LMAX),X1(LMAX)
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
      DOUBLE PRECISION THSAM(NCLMAX),QTHSAM(NCLMAX),DTHSAM(NCLMAX)
     &  ,STHSAM(NCLMAX)
      DOUBLE PRECISION COEFC
c      DOUBLE PRECISION DEBSOL1,DEBSOL2,DEBSOL3,SHIELDS,DETL
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),LACT(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      INTEGER SOUSMETHODE,CHOIXC,OPTION,METHODE,DEPOT,CAPASOL,DEFOND
      INTEGER ODCHAR
C         ,OGEOM
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX),QSACT(LMAX)
      DOUBLE PRECISION BMIU,PSTAB
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
     :  ,SENSI
     :,SOMLE(LMAX)
      DOUBLE PRECISION DCHARD,DCHARS
      INTEGER DEMIX
      INTEGER VARCONS
      LOGICAL BERGE
      LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER OPTFPC
      DOUBLE PRECISION DM,SEGMA,ROM1,PUI,T,OPTMAC
     :,CDISPDEPO,CDISPEROS,COEFPROCHE,puierosion,puidepot
         INTEGER LMA(0:NBMAX),IBL(0:LMAX),methoderosion
C      EXTERNAL DEBSOL1,DEBSOL2,DEBSOL3,SHIELDS,LDETYJ,DETL


      COMMON/NBIEF/NBB
      COMMON/XL/XL
      COMMON/BMIU/BMIU,PSTAB
      COMMON/NC/NC,XNC
      COMMON/NOMETU/ETUDE
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XLGEO/DXMAIL,XDYA
c      COMMON/VITTN/VN,YN1D,RHN
c      COMMON/MAILTN/SN,QN
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/HSA/HYDSAM
      COMMON/QSAM/THSAM,QTHSAM,DTHSAM,STHSAM
      COMMON/NSAM/NTHSAM
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
      COMMON/NCMM/NCMO,XNCMO
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/XLACT/LACT
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
c      COMMON/FROTMT/FR1
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/COEFC/COEFC
      COMMON/SOUSMETHODE/SOUSMETHODE
      COMMON/CHOIXC/CHOIXC
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/METHODE/METHODE
      COMMON/DEPOT/DEPOT
      COMMON/CAPASOL/CAPASOL
      COMMON/DEFOND/DEFOND
      COMMON/DCHARSED/DCHARD,DCHARS
      COMMON/DEMIX/DEMIX
      COMMON/VARCONS/VARCONS
      COMMON/SENSM/SENSI
      COMMON/EBERGE/BERGE
      COMMON/QSACTI/QSACT
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/FRMLPC/OptFPC
      COMMON/ROSRO/ROM1
      COMMON/ODCHAR/ODCHAR
      COMMON/PUI/PUI
C OPTMAC est le multiplicateur du D84 pour epaisseur couche active
      COMMON/OPTMAC/optMAC
C cdispdepo est le multiplicateur du D84 pour epaisseur couche superieure
C melangee avec la suivante lors d'un depot
          common/cdispdepo/cdispdepo
C cdisperos est le multiplicateur du D16 pour epaisseur couche superieure
C infiltree dans couche de dessous lors d'une erosion
      common/cdisperos/cdisperos
C coefproche intervient dans le choix de couches proches ou pas
      COMMON/CPROCHE/COEFPROCHE
      COMMON/EROSION/methoderosion
          COMMON/perosion/puierosion
          common/pdepot/puidepot


c      Write(*,*)'entree inits'

C Calcul des segments de largeur �l�mentaire
C utile pour frottement local et transport solide
C-------------------------------------------
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
C      DO I=1,LL
C 17/04/2012 : on traite cas segment negatif par 0.
C pour eviter
        J=XNC(I-1)+1
        XDYA(J)=max(0.5*(XYCOU(J+1)-XYCOU(J)),0.)
        DO J=XNC(I-1)+2,XNC(I)-1
          XDYA(J)=max(0.5*(XYCOU(J+1)-XYCOU(J-1)),0.)
        ENDDO
        J=XNC(I)
        XDYA(J)=max(0.5*(XYCOU(J)-XYCOU(J-1)),0.)
      ENDDO
      ENDDO

C Cas d'un calcul sans transport de sediment
C----------------------------
      IF(.NOT.TRASED)THEN
        WRITE(*,'(A)')'Calcul sans transport de sediment'
        IF(OPTFPC.NE.0)THEN
          DO i=1,LM(NBB)
            DACT(I)=DM
            SACT(I)=SEGMA
          ENDDO
         ENDIF
        RETURN
      ENDIF

C Lecture de 'ts.etude'
C----------------------
      NOMFIC='ts.'//ETUDE
      OPEN(32,FILE=NOMFIC,STATUS='OLD')
      READ(32,'(A50,F10.0,A20)')B,ROS,VERSION
      ROM1=(ROS-RO)/RO
      READ(32,'(A50,F10.0)')B,POR
      READ(32,'(A50,F10.0)')B,DCHARG
      READ(32,'(A50,F10.0)')B,HALFA
      READ(32,'(A50,A50)')B,LIGNE3
         read(ligne3(1:10),'(F10.0)')MUCASO
         read(ligne3(11:20),'(F10.0)')OPTMAC
                 if(optmac.gt.eps)then
        WRITE(*,'(A,F9.7,A)')'- epaisseur couche active ',
     :  OPTMAC,' D84'
             endif
         read(ligne3(21:30),'(F10.0)')CDISPEROS
C cdisperos est le multiplicateur du D16 pour epaisseur couche superieure
C infiltree dans couche de dessous lors d'une erosion
                 if(CDISPEROS.gt.eps)then
        WRITE(*,'(A,A,F9.7)')'- multiplicateur du D16',
     :  ' pour epaisseur infiltree en erosion',CDISPEROS
             endif
         read(ligne3(31:40),'(F10.0)')CDISPDEPO
                 if(CDISPDEPO.gt.eps)then
C cdispdepo est le multiplicateur du D84 pour epaisseur couche superieure
C melangee avec la suivante lors d'un depot
        WRITE(*,'(A,A,F9.7)')'- multiplicateur du D84',
     :  ' pour epaisseur melangee en depot ',CDISPDEPO
             endif
         read(ligne3(41:50),'(F10.0)')COEFPROCHE
                 if(coefproche.LT.EPS)then
                         COEFPROCHE=1.
                 else
        WRITE(*,'(A,F9.7)')'- coef proximite couches ',
     :  COEFPROCHE
             endif
c      READ(32,'(A50,F10.0)')B,MUCASO
c      READ(32,'(A50,2F10.0)')B,TCADIM,VISC
      READ(32,'(A50,A20)')B,LIGNE
         read(ligne(1:10),'(F10.0)')TCADIM
         read(ligne(11:20),'(F10.0)')visc
      READ(32,'(A50,I2)')B,OPTS
      READ(32,'(A50,I1)')B,ODCHAR
C      OGEOM=1
      READ(32,'(A50,I1)')B,UNISOL
      READ(32,'(A50,I1)')B,TYPDEF
      TYPREP=3
      READ(32,'(A50,A40)')B,LIGNE4
        READ(ligne4(1:10),'(I10)')DEPOT
        READ(ligne4(11:20),'(I10)')methoderosion
        READ(ligne4(21:30),'(F10.0)')puierosion
        READ(ligne4(31:40),'(F10.0)')puiDEPOT
C type de calcul de contrainte
      READ(32,'(A50,I1)')B,CHOIXC
      READ(32,'(A50,A3)')B,OPTIONTC
         If(optiontc(1:1).eq.'1')then
C calcul contrainte critique avec coef de pente transversale
                TCPente=.TRUE.
        WRITE(*,'(A)')'- contrainte critique corrigee Ikeda pour pente'
         ELSE
                TCPENTE=.FALSE.
        WRITE(*,'(A)')'- contrainte critique non corrig�e pour pente'
         ENDIF
         iF(optiontc(2:2).eq.'1')then
C calcul de la contrainte critique a partir de tcadim
          TCshields=.false.
         else
C calcul de la contrainte critique par diagramme de shields
                tcshields=.true.
         endif
         if(optiontc(3:3).eq.'1')then
                tauefficace=.true.
         else
                tauefficace=.false.
         endif

      READ(32,'(A50,I1)')B,CAPASOL
      READ(32,'(A50,F10.0)')B,BMIU
C compte tenu de son utilisation on devrait verifier que Bmiu est inferieur a 1/0.85
C on prend provisoirment
        PSTAB=BMIU
C la pente de stabilite a sec et sous eau egales
      READ(32,'(A50,I1)')B,DEMIX
      READ(32,'(A50,I1)')B,DEFOND
      READ(32,'(A50,I1)')B,VARCONS
      IF(DEMIX.EQ.1) THEN
      READ(32,'(A50,F10.0)')B,DCHARD
      READ(32,'(A50,F10.0)')B,DCHARS
      ENDIF
      CLOSE(32)

      WRITE(*,'(A,A)')'- calcul avec transport solide: ',NOMFIC
      IF(DEFOND.EQ.1) THEN
      WRITE(*,'(A,A)')'- calcul avec lit deformable: ',NOMFIC
      ELSE
                DEFOND=0
      WRITE(*,'(A,A)')'- calcul sans deformation du lit: ',NOMFIC
      ENDIF


C V�rification de la version du fichier
C--------------------------------------
C modif du 11/06/02
C      WRITE(*,'(A)') VERSION
C      PAUSE
c       BERGE=.FALSE.
C on utilise la version pour  mettre une erosion de berges
C les coefficients coef0 et coef 1 sont inutilises maintenant
c      IF(VERSION(1:6).EQ.'rube1b')THEN
c        READ(VERSION (7:13),'(F7.0)')COEF0
c        READ(VERSION (14:20),'(F7.0)')COEF1
c          WRITE(*,*)'premier coefficient de version =', coef0
c          WRITE(*,*)'second coefficient de version =', coef1

c      ELSEIF(VERSION(1:6).EQ.'eberge')THEN
      IF(VERSION(1:6).EQ.'eberge')THEN
          berge=.true.
        Write(*,'(A)')'- calcul avec erosion de berge'

C        COEF0=0.
C        COEF1=0.
      ELSEIF(VERSION(1:6).EQ.'      ')THEN
        BERGE=.FALSE.
        Write(*,'(A)')'- calcul sans erosion de berge'
c        COEF0=0.
c        COEF1=0.
      ELSE
        Write(*,*)'lecture de version erronee'
        write(*,*)'on lit ',        version(1:6)
c        pause
        stop
      ENDIF
C      Write(*,'(A)') version(1:6)
C      Pause
C        WRITE(*,*)'PROBLEME DE VERSION DU FICHIER ',NOMFIC
C        WRITE(*,*)'FORMAT ATTENDU: ''rube1'' '
C        STOP
C      ENDIF

C Traitement des options de transport solide
C-------------------------------------------
      IF(CHOIXC.EQ.1)THEN
        WRITE(*,'(A)')'- contrainte par la MPC'
      ELSEIF(CHOIXC.EQ.2)THEN
        WRITE(*,'(A)')'- contrainte uniforme dans la section'
      ELSEIF(CHOIXC.EQ.3)THEN
        WRITE(*,'(A)')'- contrainte par la MPC (moyenne sur 3)'
      ELSEIF(CHOIXC.EQ.4)THEN
        WRITE(*,'(A)')'- contrainte par la MPC (moyenne sur 3 pentes)'
      ELSEIF(CHOIXC.EQ.5)THEN
        WRITE(*,'(A)')'- contrainte par la MPC (moyenne sur h)'
      ELSEIF(CHOIXC.EQ.6)THEN
        WRITE(*,'(A)')'- contrainte par la MPC fonction de D'
      ELSEIF(CHOIXC.EQ.7)THEN
        WRITE(*,'(A)')'- contrainte par la MPC fonction de Ks local'
      ELSEIF(CHOIXC.EQ.8)THEN
      WRITE(*,'(A)')'- contrainte proportionnelle a hauteur eau locale'
      ELSE
        WRITE(*,*)'Choix de calcul de contrainte non disponible'
           stop
      ENDIF
      IF(OPTS.EQ.1)THEN
        WRITE(*,'(A)')'- capacite solide par Meyer-Peter et Muller'
      ELSEIF(OPTS.EQ.4)THEN
        WRITE(*,'(A)')'- capacite solide par Meyer-Peter et Muller'
        TAUEFFICACE=.TRUE.
              OPTS=1
              TCSHIELDS=.TRUE.
      ELSEIF(OPTS.EQ.2)THEN
        WRITE(*,'(A)')'- capacite solide par Engelund et Hansen'
      ELSEIF(OPTS.EQ.3)THEN
        WRITE(*,'(A)')'- capacite solide par Bagnold'
      ELSEIF(OPTS.EQ.5)THEN
        WRITE(*,'(A)')'- capacite solide par Meyer-Peter et Muller'
        TAUEFFICACE=.FALSE.
              OPTS=1
              TCSHIELDS=.FALSE.
      ELSEIF(OPTS.EQ.6)THEN
        WRITE(*,'(A)')'- capacite solide par Meyer-Peter et Muller'
        TAUEFFICACE=.TRUE.
              OPTS=1
              TCSHIELDS=.FALSE.
      ELSEIF(OPTS.EQ.7)THEN
        WRITE(*,'(A)')'- capacite solide par Meyer-Peter et Muller'
        WRITE(*,'(A)')' modifiee Pologne et correction Strickler'
      ELSEIF(OPTS.EQ.8)THEN
        WRITE(*,'(A)')'- capacite solide par Ackers et White (1973)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.9)THEN
        WRITE(*,'(A)')'- capacite solide par Smart'
      ELSEIF(OPTS.EQ.10)THEN
        WRITE(*,'(A)')'- capacite solide par van Rijn'
      ELSEIF(OPTS.EQ.11)THEN
        WRITE(*,'(A)')'- capacite solide par Rickenmann'
      ELSEIF(OPTS.EQ.12)THEN
        WRITE(*,'(A)')'- capacite solide par Camenen et Larson'
      ELSEIF(OPTS.EQ.13)THEN
        WRITE(*,'(A)')'- capacite solide par Schoklitsch'
      ELSEIF(OPTS.EQ.14)THEN
        WRITE(*,'(A)')'- capacite solide par Sato'
      ELSEIF(OPTS.EQ.15)THEN
        WRITE(*,'(A)')'- capacite solide par Recking'
      ELSEIF(OPTS.EQ.16)THEN
        WRITE(*,'(A)')'- capacite solide par Brownlie'
           CAPASOL=1
      ELSEIF(OPTS.EQ.17)THEN
        WRITE(*,'(A)')'- capacite solide par Laursen'
           CAPASOL=1
      ELSEIF(OPTS.EQ.18)THEN
        WRITE(*,'(A)')'- capacite solide par Ackers et White (1990)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.19)THEN
        WRITE(*,'(A)')'- capacite solide par Karim'
           CAPASOL=1
      ELSEIF(OPTS.EQ.20)THEN
        WRITE(*,'(A)')'- capacite solide par Karim et Kennedy'
      ELSEIF(OPTS.EQ.21)THEN
        WRITE(*,'(A)')'- capacite solide par Yang et Lim'
      ELSEIF(OPTS.EQ.22)THEN
        WRITE(*,'(A)')'- capacite solide par Yang (1984)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.23)THEN
        WRITE(*,'(A)')'- capacite solide par Yang (1979)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.24)THEN
        WRITE(*,'(A)')'- capacite solide par Yang (1973)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.25)THEN
        WRITE(*,'(A)')'- capacite solide par van Rijn suspension'
      ELSEIF(OPTS.EQ.26)THEN
        WRITE(*,'(A)')'- capacite solide par van Rijn charriage'
      ELSEIF(OPTS.EQ.27)THEN
        WRITE(*,'(A)')'- capacite solide par (Recking et al. 2016)'
      ELSEIF(OPTS.EQ.28)THEN
        WRITE(*,'(A)')'- capacite solide par (Piton et al. 2017)'
      ELSEIF(OPTS.EQ.29)THEN
        WRITE(*,'(A)')'- capacite solide par Lefort (2007)'
           CAPASOL=1
      ELSEIF(OPTS.EQ.30)THEN
        WRITE(*,'(A)')'- capacite solide par Lefort (2015)'
           CAPASOL=1
      ELSE
        WRITE(*,*)'LOI DE CAPACITE SOLIDE',OPTS,' NON DISPONIBLE'
        STOP
      ENDIF
C on corrige eventuellment des erreurs en entree
      IF(OPTS.EQ.2.OR.OPTS.EQ.3.OR.OPTS.EQ.13
     :.OR.OPTS.EQ.16.OR.OPTS.EQ.17)THEN
               TAUEFFICACE=.FALSE.
         ENDIF
         IF(TAUEFFICACE)then
        write(*,'(A)')'- contrainte efficace type Meyer Peter utilisee'
             else
        write(*,'(A)')'- contrainte totale utilisee'
      endif
      IF(.NOT.(OPTS.EQ.1.OR.OPTS.EQ.10.OR.OPTS.EQ.11
     :.OR.OPTS.EQ.12.OR.OPTS.EQ.14.OR.OPTS.EQ.25.OR.OPTS.EQ.26))THEN
            TCSHIELDS=.FALSE.
c      ELSEIF(opts.eq.17.OR.opts.eq.20.or.opts.EQ.21)then
c               TCSHIELDS=.TRUE.
      ENDIF
C cas ou la contrainte critique est calculee par le diagramme de shields
      IF(TCSHIELDS)then
       write(*,'(A)')'- contrainte critique par diagramme Shields'
       If(VISC.lt.EPS)then
         visc=tcadim
       endif
         WRITE(*,'(A,F9.7)')'- viscosite eau de ',
     :  VISC
      ELSEIF(OPTS.EQ.2.OR.OPTS.EQ.3.OR.OPTS.EQ.8.OR.OPTS.EQ.13.
     :.OR.OPTS.EQ.18.OR.OPTS.EQ.19.OR.OPTS.EQ.22
     :.OR.OPTS.EQ.23.OR.OPTS.EQ.24)THEN
C        WRITE(*,'(A,F9.6)')'- pas de contrainte critique donnee'
        WRITE(*,'(A,F9.6)')'- contrainte critique nulle'
            If(VISC.lt.EPS)then
                 visc=tcadim
            endif
        WRITE(*,'(A,F9.7)')'- viscosite eau de ',
     :  VISC
      ELSE
C si pas calcul par shields il faut tcadim
          If(VISC.lt.EPS)then
            visc=0.0000013
          endif
          IF(OPTS.NE.16)then
C pour Brownlie tc est calculee specifiquement
        WRITE(*,'(A,F9.6)')'- contrainte critique adimensionnelle de',
     :  TCADIM
          ENDIF
        WRITE(*,'(A,F9.7)')'- viscosite eau de ',
     :  VISC
C fin du if sur shields
      ENDIF
C pour la deformation coefficient utilise
      IF(OPTS.EQ.3.OR.opts.EQ.17)THEN
C  cas bagnold       ou laursen
       PUI=1.
      ELSEIF(OPTS.EQ.9)THEN
c cas Rickenmann
       PUI=1.6
      ELSEIF(OPTS.EQ.10.OR.OPTS.EQ.25.OR.OPTS.EQ.26)THEN
c cas van Rijn
       PUI=2.1
      ELSEIF(OPTS.EQ.15.OR.OPTS.EQ.21)THEN
c cas recking       ou yang et lim
       PUI=2.
      ELSEIF(OPTS.EQ.27.OR.OPTS.EQ.28)THEN
c cas recking       ou piton
       PUI=2.5
      ELSEIF(OPTS.EQ.16)THEN
c cas Brownlie
       PUI=1.65
      ELSEIF(OPTS.EQ.18)THEN
c cas ackers white 1990
       PUI=1.78
      ELSEIF(OPTS.EQ.19)THEN
c cas Karim
       PUI=2.22
      ELSEIF(OPTS.EQ.22.OR.OPTS.EQ.23.OR.OPTS.EQ.24)THEN
c cas Yang
       PUI=0.5
      else
       PUI=1.5
      ENDIF
      If(ligne4(21:30).eq.'          ')then
        puierosion=pui
      endif
      If(ligne4(31:40).eq.'          ')then
         puidepot=pui
      endif


      WRITE(*,'(A,A,F5.2)')'- coefficient multiplicateur de la capacite'
     &  ,' solide de ',MUCASO
      IF(capasol.eq.1)then
         write(*,'(A)')'- capacite solide calculee sur section totale'
      elseif(capasol.eq.2)then
         write(*,'(A)')'- capacite solide calculee sur largeur active'
      else
         capasol=3
         write(*,'(A)')'- capacite solide sommee sur segments'
      endif
      IF(ODCHAR.EQ.0)THEN
        CDCHAR=.FALSE.
        WRITE(*,'(A,F15.4,A)')'- distance de chargement fixe de '
     &    ,DCHARG,' m'
      ELSEIF(ODCHAR.EQ.1)THEN
        CDCHAR=.TRUE.
        WRITE(*,'(A,A,F15.4)')'- distance de chargement par formule'
     &    ,' de Han avec coefficient de ',HALFA
c        IF(OPTS.EQ.5.OR.OPTS.EQ.6.OR.OPTS.EQ.7)THEN
c          WRITE(*,'(A,A,A)')'- distance de chargement par formule'
c     &    ,' de Han impossible avec contrainte fixe'
c     &    ,'la distance de chargement est supposee fixe'
c          PAUSE
c            stop
c        ENDIF
      ELSEIF(ODCHAR.EQ.2)THEN
c        CDCHAR=.TRUE.
        WRITE(*,'(A,A,F15.4)')'- distance de chargement par formule'
     &    ,' de Wu and Wang avec distance de charriage ',DCHARG,' m'
       WRITE(*,'(A,A,F15.4)')'- coefficient de chargement pour la'
     &    ,' suspension de ',HALFA
      ELSE
      WRITE(*,*)'loi de chargement',ODCHAR,' NON DISPONIBLE'
      STOP
      ENDIF
      IF(DEMIX.EQ.1) THEN
        WRITE(*,'(A)')'- Demixage effectue'
        WRITE(*,'(A,F15.4,A)')'- distance de chargement pour diam�tre'
     &    ,DCHARD,' m'
        WRITE(*,'(A,F15.4,A)')'- distance de chargement pour �tendue '
     &    ,DCHARS,' m'
      ELSE
              DEMIX=0
        WRITE(*,'(A)')'- demixage non effectue'
      ENDIF
c      pause

C      IF(OGEOM.EQ.0)THEN
C        CGEOM=.FALSE.
C        WRITE(*,'(A)')'- geometrie modifiee en fin de simulation'
C      ELSEIF(OGEOM.EQ.1)THEN
C        CGEOM=.TRUE.
C        WRITE(*,'(A)')'- geometrie modifiee au cours du calcul'
C      ELSE
C        WRITE(*,*)'TYPE DE MODIFICATION DE LA GEOMETRIE',OGEOM
C     &    ,' NON DISPONIBLE'
C        STOP
C      ENDIF

      IF(TYPDEF.EQ.1)THEN
        WRITE(*,'(A)')'- sensibilite masse active = 0.1'
        SENSI=0.1
      ELSEIF(TYPDEF.EQ.2)THEN
        WRITE(*,'(A)')'- sensibilite masse active = 0.01'
        SENSI=0.01
      ELSEIF(TYPDEF.EQ.3)THEN
        WRITE(*,'(A)')'- sensibilite masse active = 0.001'
        SENSI=0.001
      ELSEIF(TYPDEF.EQ.4)THEN
        WRITE(*,'(A)')'- sensibilite masse active = 0.0001'
        SENSI=0.0001
      ELSEIF(TYPDEF.EQ.0)THEN
        WRITE(*,'(A)')'- masse active reactualisee toujours'
        SENSI=EPS
      ELSE
        write(*,*)'sensibilite masse active non disponible'
        STOP
      ENDIF
      WRITE(*,'(A)')'- Deformation de la section:'
      IF(methoderosion.EQ.2)THEN
        WRITE(*,'(A,A,F10.3)')'- EROSION:proportionnelle a '
     &  ,' contrainte ** ' ,puierosion
      ELSE
        methoderosion=1
        WRITE(*,'(A,A,F10.3)')'- EROSION:proportionnelle a '
     &  ,' (contrainte-contrainte critique) ** ',puierosion
      ENDIF
      IF(DEPOT.EQ.1)THEN
      WRITE(*,'(A,A)')'- DEPOT: couches horizontales  '
     &  ,' en partant du fond du lit '
      ELSEIF(DEPOT.EQ.2)THEN
      WRITE(*,'(A,A)')'- DEPOT:uniforme le long du perimetre mouille'
     &  ,'du lit actif '
      ELSEIF(DEPOT.EQ.3)THEN
      WRITE(*,'(A,A)')'- DEPOT:le long du perimetre mouille '
     &  ,' du lit actif en fonction de 1/contrainte '
      ELSEIF(DEPOT.EQ.4)THEN
      WRITE(*,'(A,A,F10.3)')'- DEPOT:proportionnel a '
     &  ,' (contrainte critique-Alpha*contrainte) ** ',
     :puidepot
      ELSEIF(DEPOT.EQ.5)THEN
      WRITE(*,'(A,A,F10.3)')'- DEPOT:proportionnel a '
     &  ,' (contrainte critique-Alpha1*contrainte) ** ',
     :puidepot
      ELSEIF(DEPOT.EQ.6)THEN
      WRITE(*,'(A,A,F10.3)')'- DEPOT:proportionnel a '
     &  ,' (contrainte) ** ',
     :puidepot
      ELSEIF(DEPOT.EQ.7)THEN
      WRITE(*,'(A,A,F10.3)')'- DEPOT:proportionnel a '
     &  ,' (contrainte - contrainte critique) ** ',
     :puidepot
      ELSE
        WRITE(*,*)'TYPE DE DEPOT ',DEPOT
     &    ,' NON DISPONIBLE'
      ENDIF
      IF(UNISOL.EQ.1)THEN
        WRITE(*,'(A)')'- debits solides exprimes en kg/s'
      ELSEIF(UNISOL.EQ.2)THEN
        WRITE(*,'(A)')'- concentrations exprimees en kg/m3'
      ELSE
       WRITE(*,*)'UNITE POUR LE TRANSPORT SOLIDE ',UNISOL
     &    ,' NON DISPONIBLE'
        STOP
      ENDIF

C      IF(TYPREP.EQ.1)THEN
C        WRITE(*,'(A)')'- report du bilan solide a l''amont de la maille'
c      ELSEIF(TYPREP.EQ.2)THEN
c        WRITE(*,'(A,A)')'- report du bilan solide mixte: '
c     &    ,'depot vers l''amont et erosion vers l''aval'
C      ELSEIF(TYPREP.EQ.3)THEN
C        WRITE(*,'(A)')'- report du bilan solide a l''aval de la maille'
C      ELSEIF(TYPREP.EQ.4)THEN
C        WRITE(*,'(A,A)')'- report du bilan solide vers l''aval'
C     &    ,', sauf en cas d''oscillations'
C      ELSE
c        WRITE(*,*)'TYPE DE REPORT DU BILAN SOLIDE',TYPREP
C     &    ,' NON DISPONIBLE'
C        STOP
C      ENDIF

C Lecture de 'litactif.etude'
C----------------------------
         DO IB=0,NBB
           LMA(IB)=0
      ENDDO
      NOMFIC='litactif.'//ETUDE
      OPEN(33,FILE=NOMFIC,STATUS='OLD',ERR=101)
      WRITE(*,'(A,A)')'- fichier de largeur active: ',NOMFIC
C on suppose que soit le numero de bief n'est jamais donne
C soit les numeros de biefs et abscisses sont donnees toujours et dans l'ordre
      DO I=1,LMAX
           IBL(I)=0
           READ(33,'(A80)',END=101)LIGNE2
        READ(LIGNE2,*,ERR=700,END=700)X1(I),XL3(I),IBL(I)
              GO TO 704
 700    READ(LIGNE2,'(F11.3,1X,F11.3,1X,I4)',ERR=701,END=701)
     :X1(I),XL3(I),IBL(I)
              GO TO 704
 701    READ(LIGNE2,*,ERR=702,END=702)X1(I),XL3(I)
              GO TO 704
 702    READ(LIGNE2,'(F11.3,1X,F11.3)',ERR=703,END=703)X1(I),XL3(I)
              GO TO 704
 703    READ(LIGNE2,'(2F12.3)',ERR=101)X1(I),XL3(I)
C on met ibl a 0 pour prendre les largeurs globalement
C car il a deja manque un numero de bief
 704    IF(IBL(I).NE.0.AND.LMA(0).NE.0)IBL(I)=0
C selon le cas on affecte la valeur au bief  ou globalement
              LMA(IBL(I))=LMA(IBL(I))+1
      ENDDO
 101  CLOSE(33)
      DO IB=1,NBB
           LMA(IB)=LMA(IB-1)+LMA(IB)
         ENDDO
C calcul de la somme des segments du lit actif deplace
C pour ne pas utiliser xlisec(xncmo)
C calcul des segments reporte au debut de inits
C on fait la somme des segments des points pouvant se deformer
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
        SOMLE(i)=0.
        DO J=XNCMMAG(I)+1,XNCMMAD(I)-1
         somle(i)=somle(i)+XDYA(J)
        ENDDO
        ENDDO
      ENDDO


C Calcul des limites du lit actif
C--------------------------------
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
C Calcul de XL(I), largeur active a partir de 'litactif.etude'
        IF(LMA(0).EQ.0)THEN
C on est dans le cas ou xl3 pas donne ou donne par bief
C on suppose que si pas donne par bief donne sur tout le bief
        IF(LMA(ib).EQ.lma(ib-1))THEN
C Cas o� la larg. act. n'a pas �t� donn�e dans 'litactif.etude'
C le 6 juin 2013 on remplace xlisec(xncmo) par somle
C somme des segments du lit actif
C          XL(I)=XLISEC(XNCMO(I))
          XL(I)=SOMLE(I)
C cas ou xl3 donne par bief
        ELSEIF(LMA(ib).EQ.lma(ib-1)+1)THEN
          XL(I)=XL3(LMA(IB-1)+1)
        ELSEIF(XTMAIL(I).LE.X1(LMA(IB-1)+1))THEN
          XL(I)=XL3(LMA(IB-1)+1)
        ELSEIF(XTMAIL(I).GT.X1(LMA(IB)))THEN
          XL(I)=XL3(LMA(IB))
        ELSE
          DO J=LMA(IB-1)+1,LMA(IB)-1
            IF(XTMAIL(I).GT.X1(J).AND.XTMAIL(I).LE.X1(J+1))THEN
            XL(I)=(XTMAIL(I)-X1(J))*XL3(J+1)+(-XTMAIL(I)+X1(J+1))*XL3(J)
              XL(I)=XL(I)/(X1(J+1)-X1(J))
            ENDIF
          ENDDO
C fin du if sur lma(ib)
        ENDIF
C on est maintenant dans le cas ou xl3 donne gloablement (pas de numero de bief)
        ELSEIF(LMA(0).EQ.1)THEN
          XL(I)=XL3(1)
        ELSEIF(XTMAIL(I).LE.X1(1))THEN
          XL(I)=XL3(1)
        ELSEIF(XTMAIL(I).GT.X1(LMA(0)))THEN
          XL(I)=XL3(LMA(0))
        ELSE
          DO J=1,LMA(0)-1
            IF(XTMAIL(I).GT.X1(J).AND.XTMAIL(I).LE.X1(J+1))THEN
            XL(I)=(XTMAIL(I)-X1(J))*XL3(J+1)+(-XTMAIL(I)+X1(J+1))*XL3(J)
              XL(I)=XL(I)/(X1(J+1)-X1(J))
            ENDIF
          ENDDO
        ENDIF
      ENDDO
      ENDDO

C calcul des segments reporte au debut de inits
C somme des segments des points pouvant se deformer reportee plus haut
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
        IF(SOMLE(I).LT.XL(I)-EPS)THEN
          XL(I)=SOMLE(I)
          WRITE(*,*)'ATTENTION: abscisse ',XTMAIL(I)
     &      ,' m, largeur active trop grande, reduite a ',XL(I)
        ENDIF
      ENDDO
      ENDDO

C Initialisations des constantes du transport solide
C---------------------------------------------------
      POR1=1./(1.-POR)
C  JRH=V^2/(Ch�zy^2)
C      DELTA=(ROS-RO)/RO
C          ROGRAV*JRH:Contrainte de cisaillement
      ROGRAV=RO*GRAV
      ALF=8./(GRAV*(ROS-RO)*SQRT(RO))

C Lecture de l'hydrogramme solide amont
C--------------------------------------
C QTHSAM(I) peut �tre un d�bit solide ou une concentration...
C si condam est nul reflexion a l'amont donc flux nul
      NOMFIC='hydros.'//ETUDE
      OPEN(23,FILE=NOMFIC,STATUS='UNKNOWN')
      NTHSAM(0)=0
      DO IB=1,NBB
        IF(CONDAM(IB).EQ.1.OR.CONDAM(IB).EQ.2)THEN
          HYDSAM(IB)=.TRUE.
          READ(23,*) NTHSAM(IB)
          NTHSAM(IB)=NTHSAM(IB-1)+NTHSAM(IB)
          IF(NTHSAM(IB).GT.NCLMAX)THEN
        WRITE(*,*)'PROBLEME nombre de donnees solides superieur a '
     &    ,NCLMAX
           STOP
         ENDIF
         DO I=NTHSAM(IB-1)+1,NTHSAM(IB)
c      Write(*,*)'I=',I
           READ(23,*)QTHSAM(I),THSAM(I),DTHSAM(I),STHSAM(I)
           IF((DTHSAM(I).EQ.0.).OR.(STHSAM(I).EQ.0.))THEN
              WRITE(*,*)'PROBLEME dans fichier hydros.',ETUDE
          WRITE(*,*)'diametre ou etendue numero ',I,' valant zero'
c          WRITE(*,*)'PROBLEME de version du fichier hydros.',ETUDE
c          WRITE(*,*)'Format attendu: ''ruts9'' ~ ''rube1'' '
              STOP
           ENDIF
           IF(NTHSAM(IB).GT.NTHSAM(IB-1)+1)THEN
             IF(I.GT.NTHSAM(IB-1)+1) THEN
               IF(THSAM(I).LT.THSAM(I-1))THEN
               WRITE(*,*)'TEMPS DONNEES SOLIDES AMONT NON CROISSANTS'
               STOP
               ENDIF
             ENDIF
           ENDIF
         ENDDO
         IF(UNISOL.EQ.1)THEN
        WRITE(*,'(A,A)')'Condition limite solide amont Qs(t): ',NOMFIC
         ELSEIF(UNISOL.EQ.2)THEN
        WRITE(*,'(A,A)')'Condition limite solide amont Cs(t): ',NOMFIC
         ENDIF
C si condam est nul
         ELSE
           HYDSAM(IB)=.FALSE.
           NTHSAM(IB)=NTHSAM(IB-1)
C fin du if sur condam
         ENDIF
C fin boucle sur IB
      ENDDO
      CLOSE(23)


C Lecture du fichier de conditions initiales solides
C---------------------------------------------------
      IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
        NOMFIC='tpss.'//ETUDE
        OPEN(40,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(40,*)TN
        IF(ABS(TN-tinit1D).GE.0.001)THEN
          WRITE(*,*)'PROBLEME DE TEMPS INITIAL DANS tpss T = ',TN
          WRITE(*,*)'CAR TEMPS INITIAL DANS donnee T = ',tinit1D
          STOP
        ENDIF
        WRITE(*,'(A,F15.3,A,A)')
     &    'Conditions initiales solide (reprise a',tinit1D,'s): ',NOMFIC
      ELSE
C      WRITE(*,*)'conditions initiales solides'
        NOMFIC='condins.'//ETUDE
        OPEN(40,FILE=NOMFIC,STATUS='UNKNOWN')
        READ(40,*)TN
        IF(ABS(TN-tinit1D).GE.0.001)THEN
          WRITE(*,*)'PROBLEME DE TEMPS INITIAL DANS condins T =',TN
          WRITE(*,*)'CAR TEMPS INITIAL DANS donnee T = ',tinit1D
C          STOP
        ENDIF
      WRITE(*,'(A,A)')'Conditions initiales solides: ',NOMFIC
      ENDIF
      DO IB=1,NBB
           IF(ib.ne.1)then
                read(40,*)T
              endif
        DO I=LM(IB-1)+1,LM(ib)-1
C QSACT peut �tre un d�bit solide ou une concentration
          READ(40,*) II,QSACT(I),DACT(I),SACT(I)
                if(sact(i).lt.1.)then
                        write(*,*)'section ',II,' etendue <1 : ',sact(i)
                     write(*,*)'etendue ramenee a 1'
                     sact(i)=1.
                endif
C calcul de la masse active initiale reportee dans la subroutine inimact

        ENDDO
              ENDDO

        CLOSE(40)

C Initialisation du calcul s�dimentaire
C--------------------------------------
C      DO I=1,LL
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
        DO J=XNC(I-1)+1,XNC(I)
          DO NUMCS=1,XNBCS(J)-1
            XTFCS(J,NUMCS)=0.
            XMCS(J,NUMCS)=(XZCS(J,NUMCS)-XZCS(J,NUMCS+1))
     &        *DXMAIL(I)*XDYA(J)*ROS/POR1
          ENDDO
          XTFCS(J,XNBCS(J))=0.
C la derniere couche est tres grande (1000m*1000m*1000m)
          XMCS(J,XNBCS(J))=1000000000.
        ENDDO
      ENDDO
      ENDDO

C faux: Initialisation des variables s�dimentaires pour retour en arri�re �ventuel
C---------------------------------------------------------------------------
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(ib)-1
C      DO I=1,LL
C        MACTP1(I)=MACT(I)
        DACTP1(I)=DACT(I)
        SACTP1(I)=SACT(I)
        TMACTP1(I)=TMACT(I)
        TFACTP1(I)=TFACT(I)
        DO J=XNC(I-1)+1,XNC(I)
          XZCOUP1(J)=XZCOU(J)
          XNBCSP1(J)=XNBCS(J)
          DO NUMCS=1,XNBCSP1(J)
            XZCSP1(J,NUMCS)=XZCS(J,NUMCS)
            XMCSP1(J,NUMCS)=XMCS(J,NUMCS)
            XDCSP1(J,NUMCS)=XDCS(J,NUMCS)
            XSCSP1(J,NUMCS)=XSCS(J,NUMCS)
            XTMCSP1(J,NUMCS)=XTMCS(J,NUMCS)
            XTFCSP1(J,NUMCS)=XTFCS(J,NUMCS)
          ENDDO
        ENDDO
      ENDDO
      ENDDO
c      Write(*,*)'sortie inits'

      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE INIMACT
C   initialise la masse active
C deplace de la subroutine INITS a schema avant appel a canoge
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION QSACT(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
     :,EPSS
            DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      DOUBLE PRECISION MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
C      INTEGER CAPASOL
      DOUBLE PRECISION JRH(LMAX),KS1(LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      DOUBLE PRECISION TFAC,CHR,FRPEAU
      INTEGER I,LL,LM(0:NBMAX)
      DOUBLE PRECISION FR1(LMAX),LACT(LMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE


      DOUBLE PRECISION LARACT,DEBSOL,SHIELDS
      EXTERNAL LARACT,DEBSOL,SHIELDS

      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/XLACT/LACT
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/QSACTI/QSACT
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PHYS/LM,LL
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
C      COMMON/CAPASOL/CAPASOL
      COMMON/SOLIDE/JRH,KS1
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/FROTMT/FR1
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/EPSS/EPSS
      COMMON/NBIEF/NBB

      DO ib=1,NBB
         DO I=LM(IB-1)+1,LM(IB)-1
        IF(QSACT(I).GE.0.)THEN
          IF(UNISOL.EQ.1)THEN
            IF(ABS(VINTER(I)).GT.EPSY)THEN
              MACT(I)=ABS(QSACT(I)*DXMAIL(I)/VINTER(I))
            ELSE
              MACT(I)=0.
            ENDIF
          ELSEIF(UNISOL.EQ.2)THEN
            MACT(I)=ABS(QSACT(I)*DXMAIL(I)*SINTER(I))
          ENDIF

        ELSE
          CHR=MAX(CHEZY**2,FR1(I)**2*RHINTER(I)**(1./3.))
          JRH(I)=(VINTER(I)**2)/CHR
          IF(JRH(I).LT.EPSS) JRH(I)=0.
          IF(TAUEFFICACE)THEN
C         IF(OPTS.EQ.4.OR.OPTS.EQ.6)THEN
C            JMIL=INT(0.5*(XNCMMAG(I)+XNCMMAD(I)))
C            FRPEAU=21.*(XDCSP1(JMIL,1))**(-0.1666666667)
            FRPEAU=21.*(DACT(I))**(-0.1666666667)
            KS1(I)=(FR1(I)/FRPEAU)**1.5
                     IF(KS1(i).GT.1.)KS1(I)=1.
c          ELSE
c            KS1(I)=1.
          ENDIF
          TFAC=SHIELDS(DACT(I),JRH(I),RHINTER(I))
          LACT(I)=LARACT(I)
c          IF(CAPASOL.EQ.1)THEN
            QSACT(I)=DEBSOL(I,DACT(I),TFAC,SACT(I),LACT(I))
c          ELSEIF(CAPASOL.EQ.2)THEN
c            QSACT(I)=DEBSOL2(I,DACT(I),TFAC,SACT(I))
c          ELSEIF(CAPASOL.EQ.3)THEN
c            QSACT(I)=DEBSOL3(I,DACT(I),TFAC,SACT(I))
c          ENDIF

C dans tous les cas debsol renvoie un debit solide
C       IF(UNISOL.EQ.1)THEN
            IF(ABS(VINTER(I)).GT.EPSY)THEN
              MACT(I)=ABS(QSACT(I)*DXMAIL(I)/VINTER(I))
            ELSE
              MACT(I)=0.
            ENDIF
c       ELSEIF(UNISOL.EQ.2)THEN
c            MACT(I)=ABS(QSACT*(SN(I)*(TMAIL(I+1)-TMAIL(I))
c     &        +(SN(I+1)-SN(I))*(XTMAIL(I)-TMAIL(I))))
c       ENDIF
C fin du if sur signe de QSACT
        ENDIF
C        write(*,*)'mactini',i,mact(i),qsact(i),jrh(i)
        MACTP1(I)=MACT(I)
      ENDDO
         ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE INITSAUV
C-----------------------------------------------------------------------
C Ouverture conditionnelle des fichiers de sortie
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBHYPR,LL,I,LI,NBLI,N,NBMAX,NBB
      PARAMETER(NBHYPR=100,NBMAX=150)
      CHARACTER ETUDE*20,NOMFIC*40
      CHARACTER REP*1,TF(10)*1
      INTEGER NBSSAV,NTSOR,NBMAIL(NBHYPR),ITSAV
C      DOUBLE PRECISION DTA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      LOGICAL TRASED,CDCHAR,CGEOM,VOLQOUV
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF,LM(0:NBMAX)

      COMMON/PHYS/LM,LL
      COMMON/NOMETU/ETUDE
      COMMON/ITEM/REP
      COMMON/TSTFIL/TF
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NBIEF/NBB
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
         COMMON/VOLQOUV/VOLQOUV

C Initialisations
C-----------------------------------------------------------------------
c      ITSAV=1
      ITSAV=0

      IF(REP.EQ.'O'.OR.REP.EQ.'o')THEN
C Ouverture des fichiers en cas de reprise
C-----------------------------------------------------------------------
        IF(TF(2).EQ.'O')THEN
C fichier 'hydlim.etude' pr�-existant
          LI=0
          NOMFIC='hydlim.'//ETUDE
          OPEN(54,FILE=NOMFIC,STATUS='OLD')
          DO WHILE(.TRUE.)
            READ(54,*,END=81)
            LI=LI+1
          ENDDO
81        CONTINUE
          NBLI=LI
          CLOSE(54)
          OPEN(54,FILE=NOMFIC,STATUS='OLD')
          DO LI=1,NBLI
            READ(54,*)
          ENDDO

         IF(TRASED)THEN
C fichier 'hydlims.etude' pr�-existant
          LI=0
          NOMFIC='hydlims.'//ETUDE
          OPEN(50,FILE=NOMFIC,STATUS='OLD')
          DO WHILE(.TRUE.)
            READ(50,*,END=82)
            LI=LI+1
          ENDDO
82        CONTINUE
          NBLI=LI
          CLOSE(50)
          OPEN(50,FILE=NOMFIC,STATUS='OLD')
          DO LI=1,NBLI
            READ(50,*)
          ENDDO
         ENDIF
        ENDIF

        IF(TF(4).EQ.'O')THEN
C fichier 'profil.etude' pr�-existant
          NOMFIC='profil.'//ETUDE
          OPEN(42,FILE=NOMFIC,STATUS='OLD')
          DO WHILE(.TRUE.)
            READ(42,*,END=91,ERR=91)
            DO I=1,LM(NBB)
              READ(42,*,END=91,ERR=91)
            ENDDO
            ITSAV=ITSAV+1
          ENDDO
91        CONTINUE
          CLOSE(42)
          OPEN(42,FILE=NOMFIC,STATUS='OLD')
C          DO N=1,ITSAV-1
          DO N=1,ITSAV
            READ(42,*)
            DO I=1,LM(NBB)
              READ(42,*)
            ENDDO
          ENDDO

         IF(TRASED)THEN
C fichier 'profils.etude' pr�-existant
          NOMFIC='profils.'//ETUDE
          OPEN(41,FILE=NOMFIC,STATUS='OLD')
          DO N=1,ITSAV
            READ(41,*)
            DO I=1,LM(NBB)
              READ(41,*)
            ENDDO
          ENDDO
         ENDIF
C        ENDIF

        ELSEIF(TF(5).EQ.'O')THEN
C fichier 'lindo.etude' pr�-existant
C transforme en 'profil.etude'
          LI=0
          NOMFIC='profil.'//ETUDE
          OPEN(42,FILE=NOMFIC,STATUS='OLD')
          DO WHILE(.TRUE.)
            READ(42,*,END=71)
            LI=LI+1
          ENDDO
71        CONTINUE
          NBLI=LI
          CLOSE(42)
          OPEN(42,FILE=NOMFIC,STATUS='OLD')
          DO LI=1,NBLI
            READ(42,*)
          ENDDO
         IF(TRASED)THEN
C fichier 'profils.etude' pr�-existant
          NOMFIC='profils.'//ETUDE
          OPEN(41,FILE=NOMFIC,STATUS='OLD')
          DO LI=1,NBLI
c          DO N=1,ITSAV
            READ(41,*)
c            DO I=1,LM(NBB)
c              READ(41,*)
c            ENDDO
          ENDDO
         ENDIF
c fin du if sur tf(4) ou tf(5)
               ENDIF

        IF(TF(7).EQ.'O')THEN
C fichier 'largeur.etude' pr�-existant ou non
          NOMFIC='largeur.'//ETUDE
          OPEN(43,FILE=NOMFIC,STATUS='UNKNOWN')
          CALL ECRLARGEUR(tinit1D,43)
C fichier volqouv
                  if(volqouv)then
              NOMFIC='volqouv.'//ETUDE
              OPEN(35,FILE=NOMFIC,STATUS='UNKNOWN')
                          call ecrvolqouv(tinit1D,35)
                     endif
        ENDIF

      ELSE
C Premi�re ouverture des fichiers
C-----------------------------------------------------------------------
        IF(TF(2).EQ.'O')THEN
          NOMFIC='hydlim.'//ETUDE
          OPEN(54,FILE=NOMFIC,STATUS='UNKNOWN')
          CALL ECRHYDLIM(tinit1D,54)

         IF(TRASED)THEN
          NOMFIC='hydlims.'//ETUDE
          OPEN(50,FILE=NOMFIC,STATUS='UNKNOWN')
          CALL ECRHYDLIMS(tinit1D,50)
         ENDIF
        ENDIF
        IF(TF(4).EQ.'O')THEN
          NOMFIC='profil.'//ETUDE
          OPEN(42,FILE=NOMFIC,STATUS='UNKNOWN')
c          CALL ECRPROFIL(tinit1D,42)
         IF(TRASED)THEN
          NOMFIC='profils.'//ETUDE
          OPEN(41,FILE=NOMFIC,STATUS='UNKNOWN')
c          CALL ECRPROFILS(tinit1D,41)
         ENDIF
C        ENDIF
        ELSEIF(TF(5).EQ.'O')THEN
          NOMFIC='profil.'//ETUDE
          OPEN(42,FILE=NOMFIC,STATUS='UNKNOWN')
C          CALL ECRLINDO(tinit1D,55)
C en commentaire car ecrit ailleurs
c          CALL ECRPROFIL(tinit1D,42)
         IF(TRASED)THEN
          NOMFIC='profils.'//ETUDE
          OPEN(41,FILE=NOMFIC,STATUS='UNKNOWN')
c          CALL ECRPROFILS(tinit1D,41)
         ENDIF

        ENDIF
        IF(TF(7).EQ.'O')THEN
          NOMFIC='largeur.'//ETUDE
          OPEN(43,FILE=NOMFIC,STATUS='UNKNOWN')
          CALL ECRLARGEUR(tinit1D,43)
C fichier volqouv
                  if(volqouv)then
              NOMFIC='volqouv.'//ETUDE
              OPEN(35,FILE=NOMFIC,STATUS='UNKNOWN')
                          call ecrvolqouv(tinit1D,35)
                     endif

        ENDIF
      ENDIF
C ITSAV devient le numero du profil atrouver
C modifie : on determine itsav a partir de tinit1D
      ITSAV=NTSOR+1
      DO LI=NTSOR,1,-1
           IF(TSOR(LI).GT.tinit1D-EPS)THEN
                     ITSAV=LI
           ENDIF
         ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE EGAL
C-----------------------------------------------------------------------
C Mise � �galit� des variables au temps Tn et Tn+1 pour le calcul au pas de temps suivant
C Entr�es: SNP1,QNP1,YNP1,VNP1     Sorties: SN,QN,YN1D,VN
C          XNBCSP1,XZCSP1,XMCSP1            XNBCS,XZCS,XMCS
C          XDCSP1,XSCSP1,XTMCSP1            XDCS,XSCS,XTMCS
C          XTFCSP1,   XZCOUP1               XTFCS,   XZCOU
C          MACTP1,DACTP1,SACTP1             MACT,DACT,SACT
C          TMACTP1,TFACTP1                  TMACT,TFACT
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J,NUMCS,NBB
      DOUBLE PRECISION SN(LMAX),VN(LMAX),QN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION SNP1(LMAX),VNP1(LMAX),QNP1(LMAX),YNP1(LMAX)
     & ,RHNP1(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NBIEF/NBB

C Variables hydrauliques
C-----------------------------------------------------------------------
      DO I=1,LM(NBB)
        IF(YNP1(I).LE.EPSY)THEN
          SN(I)=0.
          YN1D(I)=0.
          VN(I)=0.
          QN(I)=0.
        ELSEIF(ABS(VNP1(I)).LE.EPSY)THEN
          VN(I)=0.
          QN(I)=0.
          SN(I)=SNP1(I)
          YN1D(I)=YNP1(I)
        ELSE
          VN(I)=VNP1(I)
          QN(I)=QNP1(I)
          SN(I)=SNP1(I)
          YN1D(I)=YNP1(I)
        ENDIF
      ENDDO

C Variables g�om�triques et s�dimentaires
C-----------------------------------------------------------------------
      IF(.NOT.TRASED) RETURN
      DO I=1,LM(NBB)-1
        MACT(I)=MACTP1(I)
        If(MACT(I).LT.0.)THEN
           Write(*,*)'Egal MACT(',I,')=',MACT(I)
           MACT(I)=0.
        ENDIF
        DACT(I)=DACTP1(I)
        SACT(I)=SACTP1(I)
        TMACT(I)=TMACTP1(I)
        TFACT(I)=TFACTP1(I)
        DO J=XNC(I-1)+1,XNC(I)
          XZCOU(J)=XZCOUP1(J)
          XNBCS(J)=XNBCSP1(J)
          DO NUMCS=1,XNBCS(J)
            XZCS(J,NUMCS)=XZCSP1(J,NUMCS)
            XMCS(J,NUMCS)=XMCSP1(J,NUMCS)
            XDCS(J,NUMCS)=XDCSP1(J,NUMCS)
            XSCS(J,NUMCS)=XSCSP1(J,NUMCS)
            XTMCS(J,NUMCS)=XTMCSP1(J,NUMCS)
            XTFCS(J,NUMCS)=XTFCSP1(J,NUMCS)
          ENDDO
        ENDDO
      ENDDO

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE TESTCR
C-----------------------------------------------------------------------
C Calcule le nombre de courant maxi CRM
C
C Entr�es: XTMAIL,VNP1,YNP1,DTN    Sorties: CRM
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,LMAX,J,IJ,IDPARS,NBMAX,NBB,M
      PARAMETER(LMAX=3000,NBMAX=150)
         INTEGER LM(0:NBMAX)
      DOUBLE PRECISION VNP1(LMAX),RHNP1(LMAX),YNP1(LMAX),CNP1
     & ,QNP1(LMAX),SNP1(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION RKK(LMAX),RKA(LMAX),ALPHA(LMAX),BA(LMAX)
      DOUBLE PRECISION TN,DTN,TNP1,RN
      DOUBLE PRECISION CFL1D,CRM,DDT
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      INTEGER LDETYJ
      DOUBLE PRECISION DETCN
      EXTERNAL LDETYJ,DETCN

      COMMON/TYPSAI/IDPARS
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/MALTNP/SNP1,QNP1
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PHYS/LM,LL
      COMMON/PARASC/RKK,RKA,ALPHA,BA
      COMMON/NBIEF/NBB

      CRM=0.
              DO 1000 M=1,NBB
        DO 2 J=LM(M-1)+2,LM(M)-1
        IF(YNP1(J).GT.EPSY)THEN
           IJ=LDETYJ(0,YNP1(J),J)
           CNP1=DETCN(0,YNP1(J),IJ)
           RN=DTN/(XTMAIL(J)-XTMAIL(J-1))
           CRM=MAX(CRM,(ABS(VNP1(J))+CNP1)*RN)
        ENDIF
 2    CONTINUE
 1000 CONTINUE
      IF(CRM.EQ.0.) CRM=EPS
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE VOLMOD(VOL)
C-----------------------------------------------------------------------
C Calcule le volume contenu dans le modele hors ouvrages
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER I,LMAX,LL,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,NBMAX=150)
      DOUBLE PRECISION  SN(LMAX),QN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION VOL
      INTEGER NREFA(LMAX),LM(0:NBMAX)

      COMMON/DDNARF/NREFA
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/MAILTN/SN,QN
      COMMON/PHYS/LM,LL
         COMMON/NBIEF/NBB

      VOL=0.
         DO IB=1,NBB
      DO I=LM(IB-1)+2,LM(IB)-1
        IF(NREFA(I-1).NE.-2.OR.NREFA(I).NE.-2)THEN
          VOL=VOL+(XTMAIL(I)-XTMAIL(I-1))*SN(I)
        ENDIF
      ENDDO
         ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE INIENV
C-----------------------------------------------------------------------
C R�cup�rer les YMAX,VMAX et QMAX ainsi que les temps correspondants
C pour chaque point du maillage ainsi que les temps d'arriv�e des fronts
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,I,LMAX,N,NBB,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX)
      DOUBLE PRECISION YMAX(LMAX),VMAX(LMAX),QMAX(LMAX)
     &  ,TYMAX(LMAX),TVMAX(LMAX),TQMAX(LMAX)
      DOUBLE PRECISION ZMAX(LMAX),ZFMAX(LMAX),ZFMIN(LMAX)
     &  ,TZMAX(LMAX),TZFMAX(LMAX),TZFMIN(LMAX)
      DOUBLE PRECISION TFDEB1(LMAX),TFDEB2(LMAX),TFDEB3(LMAX)
      DOUBLE PRECISION YN1D(LMAX),RHN(LMAX),VN(LMAX),QN(LMAX)
      DOUBLE PRECISION SN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      CHARACTER REP*1,ETUDE*20,NOMFIC*40,TF(10)*1
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/ITEM/REP
      COMMON/PARMAX/YMAX,VMAX,QMAX,TYMAX,TVMAX,TQMAX
      COMMON/PARSMAX/ZMAX,ZFMAX,ZFMIN,TZMAX,TZFMAX,TZFMIN
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/PHYS/LM,LL
      COMMON/NOMETU/ETUDE
      COMMON/TFDEB/TFDEB1,TFDEB2,TFDEB3
      COMMON/TSTFIL/TF
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NBIEF/NBB

      IF(TF(3).EQ.'O')THEN
C Fichier 'envlop.etude'
        NOMFIC='envlop.'//ETUDE
        IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
          OPEN(52,FILE=NOMFIC,STATUS='OLD')
          READ(52,*)
          DO I=1,LM(NBB)
            READ(52,*)N,TYMAX(I),YMAX(I),TVMAX(I),VMAX(I)
     &        ,TQMAX(I),QMAX(I)
             IF(TYMAX(I).GT.tinit1D)THEN
               TYMAX(I)=0.
               YMAX(I)=0.
             ENDIF
             IF(TVMAX(I).GT.tinit1D)THEN
               TVMAX(I)=0.
               VMAX(I)=0.
             ENDIF
             IF(TQMAX(I).GT.tinit1D)THEN
               TQMAX(I)=0.
               QMAX(I)=0.
             ENDIF
          ENDDO
        ELSE
          OPEN(52,FILE=NOMFIC,STATUS='UNKNOWN')
          DO I=1,LM(NBB)
            TYMAX(I)=tinit1D
            YMAX(I)=YN1D(I)
            TVMAX(I)=tinit1D
            VMAX(I)=VN(I)
            TQMAX(I)=tinit1D
            QMAX(I)=QN(I)
          ENDDO
        ENDIF
        CLOSE(52)

        IF(TRASED)THEN
C Fichier 'envlops.etude'
        NOMFIC='envlops.'//ETUDE
        IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
          OPEN(49,FILE=NOMFIC,STATUS='OLD')
          READ(49,*)
          DO I=1,LM(NBB)-1
            READ(49,*)N,TZMAX(I),ZMAX(I),TZFMAX(I),ZFMAX(I)
     &        ,TZFMIN(I),ZFMIN(I)
             IF(TZMAX(I).GT.tinit1D)THEN
               TZMAX(I)=0.
               ZMAX(I)=-9999.99
             ENDIF
             IF(TZFMAX(I).GT.tinit1D)THEN
               TZFMAX(I)=0.
               ZFMAX(I)=-9999.99
             ENDIF
             IF(TZFMIN(I).GT.tinit1D)THEN
               TZFMIN(I)=0.
               ZFMIN(I)=9999.99
             ENDIF
          ENDDO
        ELSE
          OPEN(49,FILE=NOMFIC,STATUS='UNKNOWN')
          I=1
             ZMAX(I)=XCTDF(I)+YN1D(I)
                I=LM(NBB)-1
          ZMAX(I)=XCTDF(I)+YN1D(I)
          DO I=2,LM(NBB)-2
            ZMAX(I)=XCTDF(I)+0.5*(YN1D(I)+YN1D(I+1))
          ENDDO
          DO I=1,LM(NBB)-1
            TZMAX(I)=tinit1D
            TZFMAX(I)=tinit1D
            ZFMAX(I)=XCTDF(I)
            TZFMIN(I)=tinit1D
            ZFMIN(I)=XCTDF(I)
          ENDDO
        ENDIF
        CLOSE(49)
        ENDIF
        ENDIF

      IF(TF(6).EQ.'O')THEN
        NOMFIC='trajec.'//ETUDE
        IF((REP.EQ.'O').OR.(REP.EQ.'o'))THEN
          OPEN(53,FILE=NOMFIC,STATUS='OLD')
          READ(53,*)
          READ(53,*)
          DO 17 I=1,LM(NBB)
            READ(53,*)N,TFDEB1(I),TFDEB2(I),TFDEB3(I)
             IF(TFDEB1(I).GT.tinit1D)THEN
               TFDEB1(I)=0.
             ENDIF
             IF(TFDEB2(I).GT.tinit1D)THEN
               TFDEB2(I)=0.
             ENDIF
             IF(TFDEB3(I).GT.tinit1D)THEN
               TFDEB3(I)=0.
             ENDIF
17        CONTINUE
        ELSE
          OPEN(53,FILE=NOMFIC,STATUS='UNKNOWN')
          DO 15 I=1,LM(NBB)
            TFDEB1(I)=tinit1D
            TFDEB2(I)=tinit1D
            TFDEB3(I)=tinit1D
15        CONTINUE
        ENDIF
        CLOSE(53)
      ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CALCUL1DDEMI(tmax)
C-----------------------------------------------------------------------
C D�but des it�rations en temps.
C Appelle le sous programme SCHEMA qui d�termine les variables �
C l'instant Tn+1 � partir de celles � l'instant Tn.
C S'assure que la condition de stabilit� est bien verifi�e sinon
C divise le pas de temps par deux.
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J,NUMCS,NBB
      LOGICAL RETOURARRIERE
      INTEGER INDDEV(0:LMAX)
      DOUBLE PRECISION TIOPDT,ECH,CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D,CRM,PSAVE
      DOUBLE PRECISION TN,TNP1,DTN,DDT,DTM
      DOUBLE PRECISION VOLDEV(LMAX),VOLDE1(LMAX),EXPOS(LMAX)
     &  ,MU(LMAX),ZYD(LMAX)
      CHARACTER*1 IOPDT,IOVISU,IODEV
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER DEFOND
      LOGICAL XMODSEC(LMAX),MODSEC
          DOUBLE PRECISION TMAX

      COMMON/NBIEF/NBB
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/DEFOND/DEFOND
      COMMON/MODIFSEC/XMODSEC,MODSEC
         common/RTARRIERE/RETOURARRIERE

C It�ration: passage de Tn � Tn+1
C-----------------------------------------------------------------------
C Calcul des variables tabul�es aux centremailles (passage lar-cot aux intermailles et interpolation aux centremailles)
c      IF(TRASED)THEN
c        CALL TRACLC                                                     ! TRACL remplit les tableaux XCTDF et ceux de XTBGEO
c        CALL INCMLC                                                     ! INCMLC remplit les tableaux NC,NCMO,CTDF,PEN et ceux de TABGEO
c      ENDIF
c       print*,' entree iterav'

C En cas de retour en arri�re, on revient ici (pas de recalcul des variables tabul�es car inchang�es, mais on reprend l'ancienne g�om�trie)

      IF(RETOURARRIERE)THEN
                if(DEFOND.EQ.1)then
        DO I=1,LM(NBB)-1
          MACTP1(I)=MACT(I)
      If(MACTP1(I).LT.0.) Write(*,*)'Iterav MACTP1(',I,')=',MACTP1(I)
          DACTP1(I)=DACT(I)
          SACTP1(I)=SACT(I)
          TMACTP1(I)=TMACT(I)
          TFACTP1(I)=TFACT(I)
          DO J=XNC(I-1)+1,XNC(I)
            XZCOUP1(J)=XZCOU(J)
            XNBCSP1(J)=XNBCS(J)
            DO NUMCS=1,XNBCSP1(J)
              XZCSP1(J,NUMCS)=XZCS(J,NUMCS)
              XMCSP1(J,NUMCS)=XMCS(J,NUMCS)
              XDCSP1(J,NUMCS)=XDCS(J,NUMCS)
              XSCSP1(J,NUMCS)=XSCS(J,NUMCS)
              XTMCSP1(J,NUMCS)=XTMCS(J,NUMCS)
              XTFCSP1(J,NUMCS)=XTFCS(J,NUMCS)
            ENDDO
          ENDDO
C ajout du 13 juillet 2006 pour que recalcul complet par TRACLC
          XMODSEC(I)=.TRUE.
        ENDDO
c ajout par kamal mars 2004, si retour en arri�re, faut retrouver la g�om�trie a Tn
      CALL TRACLC                 ! TRACL remplit les tableaux XCTDF et ceux de XTBGEO
      CALL INCMLC
C fin du if sur defond
      ENDIF
         retourarriere=.FALSE.
C fin du if sur retourarriere
      endif
C Calcul de la valeur de Tn+1
      IF(DTN.LT.EPS)THEN
C      IF(DTN.LT.EPS**2)THEN
        WRITE(*,*)'PAS DE TEMPS DEVENU TROP PETIT A TN=',TN
        STOP
      ELSEIF(TN+3.*DTN.GT.tmax)THEN
          IF(TN+2.4*DTN.GT.tmax)THEN
            IF(TN+1.2*DTN.GT.tmax)THEN
              DTN=tmax-TN
            ELSE
              DTN=0.5*(tmax-TN)
            ENDIF
          ELSE
            DTN=0.3333*(tmax-TN)
          ENDIF
C      ENDIF
C fin du if sur eps
                ENDIF
      TNP1=TN+DTN

C Calcul des variables hydrauliques et de la d�formation du fond
      CALL PRESCHEMA

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE ITERA1D
C-----------------------------------------------------------------------
C fin des it�rations en temps.
C Appelle le sous programme SCHEMA qui d�termine les variables �
C l'instant Tn+1 � partir de celles � l'instant Tn+1/2
C S'assure que la condition de stabilit� est bien verifi�e sinon
C divise le pas de temps par deux.
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J,NUMCS,NBB
      LOGICAL RETOURARRIERE
      INTEGER INDDEV(0:LMAX)
      DOUBLE PRECISION TIOPDT,ECH,CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,CFL1D,CRM,PSAVE
      DOUBLE PRECISION TN,TNP1,DTN,DDT,DTM
      CHARACTER*1 IOPDT,IOVISU,IODEV
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER DEFOND
      LOGICAL XMODSEC(LMAX),MODSEC

      COMMON/NBIEF/NBB
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/OPTCAL/TIOPDT,ECH
      COMMON/IOPTCA/IOPDT,IOVISU
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/DEFOND/DEFOND
      COMMON/MODIFSEC/XMODSEC,MODSEC
         common/RTARRIERE/RETOURARRIERE

C Calcul des variables hydrauliques et de la d�formation du fond
        CALL SCHEMA

C Validation du pas de temps
C-----------------------------------------------------------------------
C Condition de courant de Friedrich-Levy maximale
      CALL TESTCR
C      Write(*,*)'Temps de calcul =',TN,TNP1
c      Write(*,*)'ITERAV apr�s TESTCR'

C Si brusque augmentation de vitesse, retour en arri�re avec r�duction du pas de temps
      IF(CRM.GT.CFL1D*2.)THEN
        WRITE(*,*)'ATTENTION, BRUSQUE AUGMENTATION DE VITESSE A Tn=',TN
COU1D        WRITE(*,*)'Retour en arriere avec reduction du pas de temps'
COU1D        DTN=0.5*DTN
COU1D        IF(IOPDT.EQ.'N')THEN
COU1D          DDT=DDT-DTN
COU1D          IF(DDT.LT.0.) DDT=DTN
COU1D        ENDIF
COU1D        RETOURARRIERE=.TRUE.
COU1D        RETURN
      ENDIF

C Calcul du prochain DTN
C (DDT permet de retrouver un nombre entier de DT)
      DTM=DTN*CFL1D/CRM
      IF(IOPDT.EQ.'N')THEN
        IF((DT1D-DDT).LT.EPS)THEN
          IF(DT1D.LT.DTM)THEN
            DDT=0.
            DTN=DT1D
          ELSE
            DTN=DTM
            DDT=DDT+DTN
          ENDIF
        ELSE
          DTN=MIN(DT1D-DDT,2.*DTN,DTM)
          DDT=DDT+DTN
        ENDIF
      ELSEIF(IOPDT.EQ.'M'.AND.TN.LT.TIOPDT)THEN
        DTN=MIN(DT1D,DTN*2.,DTM)
      ELSE
C        DTN=DTM
        DTN=MAX(0.5*DTN,MIN(DTM,2.*DTN))
      ENDIF
      CRM=CFL1D*DTN/DTM


      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE PRESCHEMA
C-----------------------------------------------------------------------
C D�termine le vecteur U au temps Tn+1/2 � partir de U au temps Tn
C-----------------------------------------------------------------------
C Entr�es: SN,QN                   Sorties: YMD,YPD,QMD,QPD
C
C ARCHITECTURE ----
C                  |
C                PENTE
C                  |
C                PENTEN
C                  |
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,J,LMAX,NBMAX,NBB
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX)
      DOUBLE PRECISION SND(LMAX),YND(LMAX)

      COMMON/LIQND/SND,YND
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB

C Initialisation
C---------------
      DO J=1,LM(NBB)
        SND(J)=0.
      ENDDO

C Calcul des variables aux intermailles au temps Tn+1/2
C-----------------------------------------------------------------------
c      Write(*,*)'SCHEMA: avant PENTE'
       CALL PENTE
c      Write(*,*)'SCHEMA: apr�s PENTE'
      CALL PENTEN
c      Write(*,*)'SCHEMA: apr�s PENTEN'

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE SCHEMA
C-----------------------------------------------------------------------
C D�termine le vecteur U au temps Tn+1 � partir de U au temps Tn+1/2
C-----------------------------------------------------------------------
C Entr�es: YMD,YPD,QMD,QPD                   Sorties: SNP1,QNP1
C
C ARCHITECTURE ----
C                  |
C                  |
C                GODROE ----
C                  |        |
C                  |      RIEAMS
C                  |        |
C                  |      RIEMAP
C                  |        |
C                  |      RIEMAN
C                  |
C                CANOGE
c                  |
c                TRACLC
c                  |
c                INCMLC
C                  |
C                 SC2
C
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,J,LMAX,NBHYPR,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,NBHYPR=100,NBMAX=150)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER DEFOND,LM(0:NBMAX)
      INTEGER INDDEV(0:LMAX),IJ,IJNTP1,NC(0:LMAX),XNC(0:LMAX)
     :,NREFA(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX),VN(LMAX),YN1D(LMAX),RHN(LMAX)
     +  ,SNP1(LMAX),QNP1(LMAX),YNP1(LMAX),RHNP1(LMAX),VNP1(LMAX),YNTP1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX),FR1(LMAX)
      DOUBLE PRECISION SND(LMAX),YND(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION DSMQ1(LMAX),DSMBS0,DSMBS2,SINP0(LMAX),DTN,TN,TNP1
     +  ,ZYD(LMAX),MU(LMAX),EXPOS(LMAX),VOLDEV(LMAX),VOLDE1(LMAX)
      DOUBLE PRECISION YF(LMAX),FLS(LMAX),FLQ(LMAX),CFL1D,CRM,DDT
     +  ,YMD(LMAX),YPD(LMAX),VMD(LMAX),VPD(LMAX)
     +  ,HOUV(LMAX,2),QOUV(LMAX,2),SMD(LMAX),SPD(LMAX)
COU2D       DOUBLE PRECISION SMBS4(LMAX),SMBQ4(LMAX)
         CHARACTER IODEV*1
      CHARACTER TF(10)*1
      INTEGER LDETYJ
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
C        :,ITSAV1
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      INTEGER VARCONS
      DOUBLE PRECISION CTDFTN(LMAX)
C        :,dsmq2(lmax)
      LOGICAL DEBUT1D
         INTEGER Icompteur (LMAX)

      DOUBLE PRECISION DETSN
      INTEGER LDETSJ
      DOUBLE PRECISION DETYN,DETCN,DETB,SMBQ2
     & ,SMBS,SGN
      EXTERNAL LDETSJ,DETYN,DETCN,DETB,SMBQ2
     &      ,SMBS,SGN,LDETYJ,DETSN
         SAVE ICOMPTEUR

      COMMON/LIQND/SND,YND
      COMMON/YMPD/YMD,YPD
      COMMON/DDFLUX/FLS,FLQ
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/ZYF/YF
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/FROTMT/FR1
      COMMON/NC/NC,XNC
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/CTDFTN/CTDFTN
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/NBCOUR/CFL1D,CRM,DDT
      COMMON/PHYS/LM,LL
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1
      COMMON/DEVERI/INDDEV
      COMMON/IDEVER/IODEV
      COMMON/VITTD/VMD,VPD
      COMMON/XMALTD/SMD,SPD
      COMMON/DSMQ1/DSMQ1
      COMMON/DDNARF/NREFA
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
C      COMMON/ITSAV1/ITSAV1
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/TSTFIL/TF
C AJOUT DE LA VARIABLE VARCONS DANS FICHIER TS.ETUDE: ON A TROIS CHOIX
C VARCONS=1: ON CONSERVE LA SECTION MOUILLEE SNP1 ET LE DEBIT QNP1 QUAND ON PASSE DU TEMPS Tn AU TEMPS Tn+1
C VARCONS=2: ON CONSERVE LE NIVEAU D EAU ET LE DEBIT QNP1 QUAND ON PASSE DU TEMPS Tn AU TEMPS Tn+1
C VARCONS=3: ON CONSERVE LE NIVEAU D EAU ET LA VITESSE VNP1 QUAND ON PASSE DU TEMPS Tn AU TEMPS Tn+1
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/DEFOND/DEFOND
      COMMON/VARCONS/VARCONS
      COMMON/DEBUTS/DEBUT1D
      COMMON/NBIEF/NBB
COU2D      COMMON/smbS4/SMBS4
COU2D      COMMON/smbq4/SMBq4


C Calcul des variables aux centremailles au temps Tn+1 sans second membre
C-----------------------------------------------------------------------
      CALL GODROE(SND)
c      Write(*,*)'SCHEMA: apr�s GODROE'


C Prise en compte du second membre
C-----------------------------------------------------------------------
C Correction sur les sections
C----------------------------
      IF(IODEV.EQ.'O')THEN
C Calcul des autres variables sans second membre
C-----------------------------------------------------------------------
      DO J=1,LM(NBB)
        IJ=LDETSJ(0,SNP1(J),J)
C        IF(IJ.EQ.NC(J))  WRITE(*,*)'Debordement maille ',J
c      Write(*,*)'SNP1(J)',SNP1(J)
c      Write(*,*)'IJ',IJ
c      Write(*,*)'SCHEMA: calcul sans scond membre'
c      pause
c      ENDIF
        YNP1(J)=DETYN(0,SNP1(J),IJ)
      ENDDO
              DO IB=1,NBB
        DO J=LM(IB-1)+2,LM(IB)-1
          DSMBS0=DTN*SMBS(MU(J),YN1D(J),ZYD(J),EXPOS(J),J,TN)
          DSMBS2=DTN*SMBS(MU(J),YNP1(J),ZYD(J),EXPOS(J),J,TNP1)
          SINP0(J)=SNP1(J)+0.5*(DSMBS0+DSMBS2)
          IF(SINP0(J).LT.EPS)THEN
            YNTP1=0.
          ELSE
            IJNTP1=LDETSJ(0,SINP0(J),J)
            YNTP1=DETYN(0,SINP0(J),IJNTP1)
          ENDIF
          DSMBS2=DTN*SMBS(MU(J),YNTP1,ZYD(J),EXPOS(J),J,TNP1)
          SINP0(J)=SNP1(J)+0.5*(DSMBS0+DSMBS2)
c080312          IF(SINP0(J).LT.EPS)THEN
c080312            SINP0(J)=0.
c080312            YNP1(J)=0.
c080312          ELSE
c080312            IJNTP1=LDETSJ(0,SINP0(J),J)
c            IF(IJNTP1.EQ.NC(J)) WRITE(*,*)'Apres deversement,'
c     &        ,' debordement maille ',J
c080312            YNP1(J)=DETYN(0,SINP0(J),IJNTP1)
c080312                     IF(YNP1(J).LT.EPSY)then
c080312                       SINP0(J)=0.
c080312                  endif
c080312          ENDIF
c          VOLDE1(J)=VOLDEV(J)-(XTMAIL(J)-XTMAIL(J-1))*(SINP0(J)-SNP1(J))
      VOLDE1(J)=VOLDEV(J)-(XTMAIL(J)-XTMAIL(J-1))*0.5*(DSMBS0+DSMBS2)
c          SND(J)=SND(J)+0.5*(SINP0(J)-SNP1(J))
          SNP1(J)=SINP0(J)
          SND(J)=SND(J)+0.25*(DSMBS0+DSMBS2)
          ENDDO
                ENDDO
      ENDIF
c      Write(*,*)'SCHEMA: apr�s prise en compte second membre'
C Calcul de YND au temps Tn+1/2
C----------------------------
              DO IB=1,NBB
        DO J=LM(IB-1)+2,LM(IB)-1
C      DO J=2,LL
COU2D              SND(J)=SND(J)+0.5*DTN*SMBS4(j)
        IF(SND(J).LT.EPS)THEN
          YND(J)=0.
        ELSE
          IJNTP1=LDETSJ(0,SND(J),J)
          YND(J)=DETYN(0,SND(J),IJNTP1)
        ENDIF
        ENDDO
              ENDDO
c fin du calcul du YND au temps Tn+1/2

C      DO J=2,LL
              DO IB=1,NBB
        DO J=LM(IB-1)+2,LM(IB)-1
COU2D        SNP1(J)=SNP1(J)+DTN*SMBS4(j)
COU2D      VOLDE1(J)=VOLDEV(J)-(XTMAIL(J)-XTMAIL(J-1))*DTN*SMBS4(j)
        IF(SNP1(J).LT.EPS)THEN
          SNP1(J)=0.
          YNP1(J)=0.
        ELSE
          IJNTP1=LDETSJ(0,SNP1(J),J)
          YNP1(J)=DETYN(0,SNP1(J),IJNTP1)
          IF(IJNTP1.EQ.NC(J)) THEN
                      IF(ICOMPTEUR(J).LT.20)THEN
                        ICOMPTEUR(J)=ICOMPTEUR(J)+1
                       WRITE(*,*)'Debordement maille ',J
     &      ,' (',TMAIL(J),' m) avec une hauteur de ',YNP1(J)
     &      ,' m a Tn+1=',TNP1
c            write(*,*)YND(j),snd(j),fls(j-1),fls(j),flq(j-1),flq(j)
c               write(*,*)sn(j),snp1(j),smd(j),spd(j-1),ymd(j),ypd(j-1)
c               write(*,*)qn(j),qnp1(j),vmd(j),vpd(j-1),smd(j),vpd(j)
                ENDIF
             endif
C fin du if sur snp1=0
        ENDIF
        IF(YND(J).LT.EPSY)THEN
          YF(J)=MIN(YMD(J)+XCTDF(J)-CTDF(J),YPD(J-1)+XCTDF(J-1)-CTDF(J))
        ELSE
          YF(J)=YND(J)
        ENDIF

C Calcul des pertes de charge dues � l'�largissement
C---------------------------------------------------
        IF(YPD(J-1).GT.EPSY.AND.YMD(J).GT.EPSY)THEN
          CALL ELARGI(J,YPD(J-1),YMD(J),SPD(J-1),SMD(J),VPD(J-1)
     &      ,VMD(J),DSMQ1(J))
          DSMQ1(J)=SND(J)*DTN*DSMQ1(J)
        ELSE
          DSMQ1(J)=0.
        ENDIF
C YF n'est pris en compte que si YND est nul (YF donc negatif)
        DSMQ1(J)=-DSMQ1(J)+DTN*DETB(YND(J),J,YF(J))
C modif de sc2 pour que le demi frottement soit pris avant changement geometrie
C erreur
C        DSMQ1(J)=DSMQ1(J)+0.5*DTN*SMBQ2(0,VN(J),J,YN1D(J))
C dsmq2 inutile car on calcule frottement en implicite dans sc2
C       DSMQ2(J)=0.5*DTN*SMBQ2(0,VN(J),J,YN1D(J))
        QNP1(J)=QNP1(J)+DSMQ1(J)
COU2D        QNP1(J)=QNP1(J)+DTN*SMBQ4(j)
      ENDDO
      ENDDO

c --------------------------------------------------------
c Kamal janvier 2005: cas ou pas de transport solide
c --------------------------------------------------------


c fin de calcul si pas de transport solide
c --------------------------------------------------------------
c      Write(*,*)'SCHEMA: avant transport solide'
      IF(TRASED) THEN
c dans le cas de conservation du niveau d'eau (VARCONS=2, ou =3), il faut garder en m�moire
c les cotes du fond au temps tn (avant d'effectuer la deformation), necessaire pour calculer le niveau d'eau
        IF(DEFOND.EQ.1) THEN
        IF(VARCONS.EQ.2.OR.VARCONS.EQ.3)THEN
          DO J=1,LM(NBB)
            CTDFTN(J)=CTDF(J)
          ENDDO
        ENDIF
      ENDIF


c  ----------------------------------------------------------------------------
c    on fait le bilan s�dimentaire et on d�forme la g�om�trie
c  ----------------------------------------------------------------------------
C la masse active est initialise ici car a besoin des variables a intermaille
C en cas de reprise, decalage de dt car sauve 1/2dt avant reprise et initialise 1/2 dt apres
        IF(DEBUT1D)THEN
          CALL INIMACT
        ENDIF
        CALL CANOGE
c  ----------------------------------------------------------------------------
c      Write(*,*)'SCHEMA: avant actualisation variable hydraulique'
c  la r�actualiasation des variables hydrauliques: on utilise le sous programme ACTVARHYD
c fin du if sur trased
      ENDIF
      IF(DEBUT1D)THEN
               DO J=1,LM(NBB)
                ICOMPTEUR (J)=0
            ENDDO
        DEBUT1D=.FALSE.
      ENDIF
      IF(.NOT.TRASED.OR.DEFOND.EQ.0)      THEN
C pour quoi recalcule t on YNP1 si la geometrie n'a pas change
          DO J=1,LM(NBB)
            IJ=LDETSJ(0,SNP1(J),J)
            YNP1(J)=DETYN(0,SNP1(J),IJ)
            IF(YNP1(J).LT.EPSY)  YNP1(J)=0.
          ENDDO
C if sur trased et defond
      ELSE
c cas defond = 1
          CALL ACTVARHYD
      ENDIF
C Calcule le terme de frottements (� Tn+1)
      DO IB=1,NBB
        DO J=LM(IB-1)+2,LM(IB)-1
C          DO J=2,LL
C            CALL SC2(DSMQ1(J),SNP1(J),YNP1(J),J,QNP1(J),DSMQ2(J))
            CALL SC2(SNP1(J),YNP1(J),QNP1(J),J)
        ENDDO
      ENDDO
C D�termination des vitesses corrig�es � Tn+1
      DO J=1,LM(NBB)
            IF(YNP1(J).LT.EPSY)THEN
              SNP1(J)=0.
              YNP1(J)=0.
              VNP1(J)=0.
            ELSE
              VNP1(J)=QNP1(J)/SNP1(J)
            ENDIF
      ENDDO
c      Write(*,*)'SCHEMA: sortie'
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE PENTE
C-----------------------------------------------------------------------
C Calcule les pentes aux intermailles sur les sections et les d�bits
C (connus aux centremailles au temps Tn), afin de d�terminer les d�bits
C et des sections aux intermailles
C
C Entr�es: SN,QN                   Sorties: ZM,ZP,QM,QP
C-----------------------------------------------------------------------
C DWM(LMAX): DS       DWP(LMAX): DQ       DX1: DX aval
C PM(LMAX): pente S   PD(LMAX): pente D   DX2,DX3,DX4: DX amont
      IMPLICIT NONE
      INTEGER LL,I,LMAX,LNCMAX,nou1Dmax,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,LNCMAX=130000,nou1Dmax=50,NBMAX=150)
      INTEGER LDETSJ,LDETYJ
      INTEGER NREFA(LMAX),LM(0:NBMAX)
         INTEGER REGIME(NBMAX),CONDAV(NBMAX),CONDAM(NBMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION ZM(LMAX),ZP(LMAX),QM(LMAX),QP(LMAX)
      DOUBLE PRECISION DWM(LMAX),DWP(LMAX),PM(LMAX),PD(LMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION DX,ZN(LMAX),DX1,DX2,DX3,DX4
C       ,A
     &,C,A1,A2,B1,B2,C1,C2
C        ,PY(LMAX)
     :,DWY(LMAX)
      DOUBLE PRECISION XCORR,XC1,XC2,XB1,XB2
      DOUBLE PRECISION SGN,DETYN,DETSN
      DOUBLE PRECISION Z1,Z2,BET
      LOGICAL FRON5,FRON6
      EXTERNAL SGN,DETYN,LDETSJ,LDETYJ,DETSN

      COMMON/DDNARF/NREFA
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB

      DO I=1,LM(NBB)
        ZN(I)=YN1D(I)+CTDF(I)
      ENDDO

C Calcul des pentes aux centremailles
C-----------------------------------------------------------------------
      DO I=1,LL
        DWM(I)=ZN(I+1)-ZN(I)
        DWP(I)=QN(I+1)-QN(I)
        DWY(I)=YN1D(I+1)-YN1D(I)
      ENDDO

      DO I=2,LL
        DX=TMAIL(I+1)-TMAIL(I-1)
        PM(I)=(ZN(I+1)-ZN(I-1))/DX
        PD(I)=(QN(I+1)-QN(I-1))/DX
C        PY(I)=(YN1D(I+1)-YN1D(I-1))/DX
      ENDDO

C Correction
C-----------------------------------------------------------------------
      XCORR=1.5
         DO IB=1,NBB
      FRON5=.FALSE.
      FRON6=.FALSE.
      DO I=LM(ib-1)+2,LM(IB)-1
        IF(YN1D(I).LT.EPSY)THEN
          QM(I)=0.
          QP(I-1)=0.
          ZP(I-1)=MIN(ZM(I-1),CTDF(I))
          ZM(I)=XCTDF(I)
          GOTO 777
        ENDIF
        DX1=TMAIL(I+1)-TMAIL(I)
        DX2=TMAIL(I)-TMAIL(I-1)
        C1=DWY(I)/DX1
        A1=DWM(I)/DX1
        B1=DWP(I)/DX1
        C2=DWY(I-1)/DX2
        B2=DWP(I-1)/DX2
        A2=DWM(I-1)/DX2
        IF(I.LT.LM(ib)-1)THEN
          DX3=TMAIL(I+2)-TMAIL(I+1)
          C=DWY(I+1)/DX3
C          A=DWM(I+1)/DX3
        ENDIF
        DX3=XTMAIL(I)-TMAIL(I)
        DX4=TMAIL(I)-XTMAIL(I-1)
        IF(.NOT.FRON6)THEN
          IF(ABS(C1).GT.EPS)THEN
            BET=MAX(ABS(C2),ABS(C))/ABS(C1)
            IF(BET.LT.0.98)FRON5=.TRUE.
          ELSE
            BET=10.
          ENDIF
          XC1=1.+(XCORR-1.)*(DX1-DX3)/DX3
          XC2=1.+(XCORR-1.)*(DX2-DX4)/DX4
        ENDIF
        IF(FRON5.OR.FRON6)THEN
          IF(BET.LT.0.95)THEN
            XC1=1.
            XC2=1.
          ELSEIF(BET.LT.0.98)THEN
            XC1=1.+(XCORR-1.)*(DX1-DX3)/DX3
            XC2=1.+(XCORR-1.)*(DX2-DX4)/DX4
            XC1=(BET*(XC1-1.)+0.98-0.95*XC1)/0.03
            XC2=(BET*(XC2-1.)+0.98-0.95*XC2)/0.03
          ENDIF
        ENDIF

        IF(YN1D(I+1).EQ.0.)THEN
          XC1=1.
        ENDIF
        IF(YN1D(I-1).EQ.0.)THEN
          XC2=1.
        ENDIF
        XB1=1.+(XCORR-1.)*(DX1-DX3)/DX3
        XB2=1.+(XCORR-1.)*(DX2-DX4)/DX4
        IF(I.EQ.LM(IB)-1)THEN
          XC1=1.
          XB1=1.
        ENDIF
        IF(I.EQ.LM(Ib-1)+2)THEN
          XC2=1.
          XB2=1.
        ENDIF
        IF(NREFA(I).EQ.-2)THEN
          IF(NREFA(I-1).EQ.-2)THEN
C On ne fait rien
            ZM(I)=ZN(I)
            ZP(I-1)=ZN(I)
          ELSE
C Pente sur Z uniquement
            ZM(I)=ZN(I)+(A2*DX3)
            ZP(I-1)=ZN(I)-(A2*DX4)
            IF(ZN(I-1).GT.ZN(I))THEN
              Z2=ZN(I)
              Z1=ZN(I-1)
            ELSE
              Z1=ZN(I)
              Z2=ZN(I-1)
            ENDIF
            IF(ZP(I-1).GT.Z1)THEN
              ZP(I-1)=Z1
              ZM(I)=2.*ZN(I)-Z1
            ELSEIF(ZP(I-1).LT.Z2)THEN
              ZP(I-1)=Z2
              ZM(I)=2.*ZN(I)-Z2
            ENDIF
C Fin du IF sur NREFA(I-1)=-2
          ENDIF
        ELSEIF(NREFA(I-1).EQ.-2)THEN
C Pente sur Z uniquement
          ZM(I)=ZN(I)+(A1*DX3)
          ZP(I-1)=ZN(I)-(A1*DX4)
          IF(ZN(I+1).GT.ZN(I))THEN
            Z2=ZN(I)
            Z1=ZN(I+1)
          ELSE
            Z1=ZN(I)
            Z2=ZN(I+1)
          ENDIF
          IF(ZM(I).GT.Z1)THEN
            ZM(I)=Z1
            ZP(I-1)=2.*ZN(I)-Z1
          ELSEIF(ZM(I).LT.Z2)THEN
            ZM(I)=Z2
            ZP(I-1)=2.*ZN(I)-Z2
          ENDIF
        ELSEIF(ZN(I).LT.XCTDF(I).AND.ZN(I+1).LT.XCTDF(I))THEN
C Interface I sommet
          ZM(I)=XCTDF(I)
          ZP(I-1)=ZN(I)-(A2*DX4)
          IF(YN1D(I-1).GT.YN1D(I))THEN
            Z2=YN1D(I)
            Z1=YN1D(I-1)
          ELSE
            Z1=YN1D(I)
            Z2=YN1D(I-1)
          ENDIF
          IF(ZP(I-1).GT.Z1+XCTDF(I-1))THEN
            ZP(I-1)=Z1+XCTDF(I-1)
          ELSEIF(ZP(I-1).LT.Z2+XCTDF(I-1))THEN
            ZP(I-1)=Z2+XCTDF(I-1)
          ENDIF
          IF(ZN(I-1).GT.ZN(I))THEN
            Z2=ZN(I)
            Z1=ZN(I-1)
          ELSE
            Z1=ZN(I)
            Z2=ZN(I-1)
          ENDIF
          IF(ZP(I-1).GT.Z1)THEN
             ZP(I-1)=Z1
          ELSEIF(ZP(I-1).LT.Z2)THEN
             ZP(I-1)=Z2
          ENDIF
        ELSEIF(ZN(I).LT.XCTDF(I-1).AND.ZN(I-1).LT.XCTDF(I-1))THEN
C Interface I-1 sommet
          ZP(I-1)=XCTDF(I-1)
          ZM(I)=ZN(I)+(A1*DX3)
          IF(YN1D(I+1).GT.YN1D(I))THEN
            Z2=YN1D(I)
            Z1=YN1D(I+1)
          ELSE
            Z1=YN1D(I)
            Z2=YN1D(I+1)
          ENDIF
          IF(ZM(I).GT.Z1+XCTDF(I))THEN
            ZM(I)=Z1+XCTDF(I)
          ELSEIF(ZM(I).LT.Z2+XCTDF(I))THEN
            ZM(I)=Z2+XCTDF(I)
          ENDIF
          IF(ZN(I+1).GT.ZN(I))THEN
            Z2=ZN(I)
            Z1=ZN(I+1)
          ELSE
            Z1=ZN(I)
            Z2=ZN(I+1)
          ENDIF
          IF(ZM(I).GT.Z1)THEN
            ZM(I)=Z1
          ELSEIF(ZM(I).LT.Z2)THEN
            ZM(I)=Z2
          ENDIF
        ELSEIF((SGN(PM(I)).EQ.SGN(A1)).AND.(SGN(A1).EQ.SGN(A2)))THEN
C Cas normal
          PM(I)=SGN(PM(I))*MIN(ABS(PM(I)),ABS(A1*XC1),ABS(A2*XC2))
          ZM(I)=ZN(I)+(PM(I)*DX3)
          ZP(I-1)=ZN(I)-(PM(I)*DX4)
          IF(YN1D(I+1).GT.YN1D(I))THEN
            Z2=YN1D(I)
            Z1=YN1D(I+1)
          ELSE
            Z1=YN1D(I)
            Z2=YN1D(I+1)
          ENDIF
          IF(ZM(I).GT.Z1+XCTDF(I))THEN
            ZM(I)=Z1+XCTDF(I)
            ZP(I-1)=2.*ZN(I)-ZM(I)
          ELSEIF(ZM(I).LT.Z2+XCTDF(I))THEN
            ZM(I)=Z2+XCTDF(I)
            ZP(I-1)=2.*ZN(I)-ZM(I)
          ENDIF
          IF(YN1D(I-1).GT.YN1D(I))THEN
            Z2=YN1D(I)
            Z1=YN1D(I-1)
          ELSE
            Z1=YN1D(I)
            Z2=YN1D(I-1)
          ENDIF
          IF(ZP(I-1).GT.Z1+XCTDF(I-1))THEN
            ZP(I-1)=Z1+XCTDF(I-1)
            ZM(I)=2.*ZN(I)-ZP(I-1)
          ELSEIF(ZP(I-1).LT.Z2+XCTDF(I-1))THEN
            ZP(I-1)=Z2+XCTDF(I-1)
            ZM(I)=2.*ZN(I)-ZP(I-1)
          ENDIF
          IF(ZN(I+1).GT.ZN(I))THEN
            Z2=ZN(I)
            Z1=ZN(I+1)
          ELSE
            Z1=ZN(I)
            Z2=ZN(I+1)
          ENDIF
          IF(ZM(I).GT.Z1)THEN
            ZM(I)=Z1
          ELSEIF(ZM(I).LT.Z2)THEN
            ZM(I)=Z2
          ENDIF
          IF(ZN(I-1).GT.ZN(I))THEN
            Z2=ZN(I)
            Z1=ZN(I-1)
          ELSE
            Z1=ZN(I)
            Z2=ZN(I-1)
          ENDIF
          IF(ZP(I-1).GT.Z1)THEN
            ZP(I-1)=Z1
          ELSEIF(ZP(I-1).LT.Z2)THEN
            ZP(I-1)=Z2
          ENDIF
C IF sur NREFA(I)=-2
        ELSE
C Cas o� les pentes sur Z sont de signe oppos� donc pente nulle sur Z
          ZM(I)=ZN(I)
          ZP(I-1)=ZN(I)
C          IF(YN1D(I+1).GT.YN1D(I))THEN
C            Z2=YN1D(I)
C            Z1=YN1D(I+1)
C          ELSE
C            Z1=YN1D(I)
C            Z2=YN1D(I+1)
C          ENDIF
C          IF(ZM(I).GT.Z1+XCTDF(I))THEN
C            ZM(I)=Z1+XCTDF(I)
C            ZP(I-1)=2.*ZN(I)-ZM(I)
C          ELSEIF(ZM(I).LT.Z2+XCTDF(I))THEN
C            ZM(I)=Z2+XCTDF(I)
C            ZP(I-1)=2.*ZN(I)-ZM(I)
C          ENDIF
C          IF(YN1D(I-1).GT.YN1D(I))THEN
C            Z2=YN1D(I)
C            Z1=YN1D(I-1)
C          ELSE
C            Z1=YN1D(I)
C            Z2=YN1D(I-1)
C          ENDIF
C          IF(ZP(I-1).GT.Z1+XCTDF(I-1))THEN
C            ZP(I-1)=Z1+XCTDF(I-1)
C            ZM(I)=2.*ZN(I)-ZP(I-1)
C          ELSEIF(ZP(I-1).LT.Z2+XCTDF(I-1))THEN
C            ZP(I-1)=Z2+XCTDF(I-1)
C            ZM(I)=2.*ZN(I)-ZP(I-1)
C          ENDIF
C            IF(ZN(I+1).GT.ZN(I))THEN
C              Z2=ZN(I)
C              Z1=ZN(I+1)
C            ELSE
C              Z1=ZN(I)
C              Z2=ZN(I+1)
C            ENDIF
C            IF(ZM(I).GT.Z1)THEN
C              ZM(I)=Z1
C            ELSEIF(ZM(I).LT.Z2)THEN
C              ZM(I)=Z2
C            ENDIF
C            IF(ZN(I-1).GT.ZN(I))THEN
C              Z2=ZN(I)
C              Z1=ZN(I-1)
C            ELSE
C              Z1=ZN(I)
C              Z2=ZN(I-1)
C            ENDIF
C            IF(ZP(I-1).GT.Z1)THEN
C              ZP(I-1)=Z1
C            ELSEIF(ZP(I-1).LT.Z2)THEN
C              ZP(I-1)=Z2
C            ENDIF
C Fin du IF sur NREFA(I)=-2
        ENDIF
        IF(NREFA(I).EQ.-2)THEN
          IF(NREFA(I-1).EQ.-2)THEN
C On ne fait rien
            QM(I)=QN(I)
            QP(I-1)=QN(I)
          ELSE
            QM(I)=QN(I)+(B2*DX3)
            QP(I-1)=QN(I)-(B2*DX4)
          ENDIF
        ELSEIF(NREFA(I-1).EQ.-2)THEN
          QM(I)=QN(I)+(B1*DX3)
          QP(I-1)=QN(I)-(B1*DX4)
        ELSEIF(ZN(I).LT.XCTDF(I).AND.ZN(I+1).LT.XCTDF(I))THEN
          QM(I)=QN(I)+(B2*DX3)
          QP(I-1)=QN(I)-(B2*DX4)
        ELSEIF(ZN(I).LT.XCTDF(I-1).AND.ZN(I-1).LT.XCTDF(I-1))THEN
          QM(I)=QN(I)+(B1*DX3)
          QP(I-1)=QN(I)-(B1*DX4)
        ELSEIF((SGN(PD(I)).EQ.SGN(B1)).AND.(SGN(B1).EQ.SGN(B2)))THEN
          PD(I)=SGN(PD(I))*MIN(ABS(PD(I)),ABS(B1*XB1),ABS(B2*XB2))
          QM(I)=QN(I)+(PD(I)*DX3)
          QP(I-1)=QN(I)-(PD(I)*DX4)
        ELSE
          QM(I)=QN(I)
          QP(I-1)=QN(I)
        ENDIF

C Pour compl�ter le traitement du cas YN1D(I)=0
        IF(YN1D(I-1).LT.EPSY)THEN
          ZM(I-1)=MIN(ZP(I-1),CTDF(I-1))
          QM(I-1)=0.
          QP(I-1)=0.
        ENDIF
 777    CONTINUE
        IF(FRON5)THEN
          FRON5=.FALSE.
          FRON6=.TRUE.
        ELSEIF(FRON6)THEN
          FRON6=.FALSE.
        ENDIF
C fin boucle sur i
        ENDDO
C rajout du 15/4/2010 deconnecte le 12/11/12
C si les deux valeurs interface meme cote
C alors on egalise
C12112              do i=LM(ib-1)+2,LM(IB)-2
C12112                Z1=0.5*(ZN(i+1)+zn(i))
C12112                IF(ZM(i).GT.Z1)then
C12112                     IF(ZP(i).GT.Z1)THEN
C12112                         ZP(I)=MIn(ZM(I),ZP(I))
C12112                         zm(i)=zp(i)
C12112                     ENDIF
C12112          ELSEIF(ZP(I).LT.Z1)THEN
C12112                         ZP(I)=Max(ZM(I),ZP(I))
C12112                         zm(i)=zp(i)
C12112                ENDIF
C12112                Z1=0.5*(QN(i+1)+Qn(i))
C12112                IF(QM(i).GT.Z1)then
C12112                     IF(QP(i).GT.Z1)THEN
C12112                         qP(I)=MIn(qM(I),qP(I))
C12112                         qm(i)=qp(i)
C12112                     ENDIF
C12112          ELSEIF(QP(I).LT.Z1)THEN
C12112                         qP(I)=Max(qM(I),qP(I))
C12112                         qm(i)=qp(i)
C12112                ENDIF
C12112C fin boucle sur i
C12112                ENDDO

C fin boucle sur ib
      ENDDO
      DO IB=1,NBB

c      IF((CONDAM(I).NE.4).and.(condam(i).ne.0))THEN
       IF(CONDAM(IB).NE.4)THEN
c141210        QP(LM(IB-1)+1)=QN(LM(IB-1)+1)
c        IF(CONDAM(I).NE.0)THEN
C26         ZP(LM(IB-1)+1)=ZN(LM(IB-1)+1)
c        ENDIF
c        QP(LM(I)-1)=QN(LM(I))
c        ZP(LM(I)-1)=ZN(LM(I))
      ENDIF
      IF(CONDAM(IB).EQ.4)THEN
c141210       IF(QN(LM(IB-1)+1).GE.0.)THEN
c141210        QP(LM(IB-1)+1)=QN(LM(IB-1)+1)
c141210        ZP(LM(IB-1)+1)=ZN(LM(IB-1)+1)
c141210       ENDIF
      ENDIF
      IF(CONDAV(IB).EQ.4)THEN
c141210       IF(QN(LM(IB)).LT.0.)THEN
c141210        QM(LM(IB)-1)=QN(LM(IB))
c141210        ZM(LM(IB)-1)=ZN(LM(IB))
c141210       ENDIF
      ENDIF
c      IF(CONDAV(IB).NE.4)THEN
c       IF(QN(LM(IB)).LT.0.)THEN
        QM(LM(Ib)-1)=QN(LM(Ib))
c        ZM(LM(IB)-1)=ZN(LM(IB))
c       ENDIF
c      ENDIF
C       IF(zp(lm(i-1)+1).le.ctdf2(lm(i-1)+1))then
C         write(*,*)'zp',i,zp(lm(i-1)+1),zn(lm(i-1)+1),zn(lm(i-1)+2)
C       endif
      ENDDO
c      write(*,*)'zn',zn(18),zn(19),zp(18),zm(19),YN1D(18)
c         write(*,*)'qn',qn(18),qn(19),qm(19),qp(18),YN1D(19)
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE PENTEN
C-----------------------------------------------------------------------
C Calcule les variables aux intermailles au temps Tn+1/2 �  partir des
C variables intermailles au temps Tn
C
C Entr�es: ZM,ZP,QM,QP             Sorties: SMD,SPD,VMD,VPD
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,J,LL,I,LDETYJ,LDETSJ,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,NBMAX=150)
         INTEGER LM(0:NBMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX),SN(LMAX),QN(LMAX)
     +  ,SM(LMAX),VM(LMAX),YM(LMAX),VP(LMAX),YP(LMAX),SP(LMAX)
     +  ,SMD(LMAX),QMD(LMAX),VMD(LMAX),SPD(LMAX), QPD(LMAX),VPD(LMAX)
     +  ,DWM(LMAX),DWP(LMAX),TN,DTN,TNP1,YMD(LMAX),YPD(LMAX)
      DOUBLE PRECISION VOLDE1(LMAX),EXPOS(LMAX),VOLDEV(LMAX)
     +  ,MU(LMAX),ZYD(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION YF(LMAX)
      DOUBLE PRECISION FQINP1,DETB
      DOUBLE PRECISION SMBQ2,SMBS,DETYN,DETSN,SGN
      DOUBLE PRECISION DQ(LMAX),dz,yMAX,yMIN,dq2,QMAX,QMIN,ds
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION ZM(LMAX),ZP(LMAX),QM(LMAX),QP(LMAX)
         INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
COU2D         DOUBLE PRECISION SMBQ4(LMAX),SMBS4(LMAX)
C100813      LOGICAL VDEVer(LMAX)
      CHARACTER *1 IODEV
      EXTERNAL LDETSJ,LDETYJ
      EXTERNAL DETSN,DETB
      EXTERNAL SMBQ2,FQINP1,SMBS,DETYN,SGN

      COMMON/YMPD/YMD,YPD
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/DEVERS/ZYD,MU,EXPOS,VOLDEV,VOLDE1
      COMMON/IDEVER/IODEV
      COMMON/PHYS/LM,LL
      COMMON/ZYF/YF
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/NBIEF/NBB
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
COU2D      COMMON/smbS4/SMBS4
COU2D      COMMON/smbq4/SMBq4

      DO J=1,LM(NBB)
        DWM(J)=0.
        DWP(J)=0.
        SM(J)=0.
        SP(J)=0.
        VM(J)=0.
        VP(J)=0.
        YM(J)=0.
        YP(J)=0.
      ENDDO

C Calcul de dU/dt = -dF(U)/dx sans second membre
C-----------------------------------------------------------------------
c      Write(*,*)'Penten: avant Pnn1'
      CALL PNN1(DWM,DWP,SM,SP,VM,VP,YM,YP)

C D�termination de S et Q � l'interface amont
C-----------------------------------------------------------------------
c      Write(*,*)'Penten: avant Pnn2'
      CALL PNN2(DWM,DWP,SM,SP,QMD,QPD)
c       write(*,*)vm(19),vp(18),ym(19),yp(18)
C Calcul avec le second membre
C-----------------------------------------------------------------------
C Correction sur les sections
C----------------------------
c      Write(*,*)'Penten: avant correction sur S'
      IF(IODEV.EQ.'O')THEN
              DO IB=1,nbb
        DO J=LM(IB-1)+2,LM(IB)-1
                DS=0.5*DTN*SMBS(MU(J),YN1D(J),ZYD(J),EXPOS(J),J,TN)
          SMD(J)=SMD(J)+DS
          SPD(J-1)=SPD(J-1)+DS
C100813          IF(ABS(DS).GT.EPS)THEN
C100813            vdever(j)=.TRUE.
C100813          else
C100813            vdever(j)=.FALSE.
C100813          endif
           ENDDO
c              J=LM(ib)-1
C160910              IF(CONDAV(IB).EQ.4)THEN
C160910        SMD(J)=SMD(J)+0.5*DTN*SMBS(MU(J),YN1D(J),ZYD(J),EXPOS(J),J,TN)
C160910           ELSE
c        SMD(J)=SMD(J)+DTN*SMBS(MU(J),YN1D(J),ZYD(J),EXPOS(J),J,TN)
C160910           ENDIFc
c              J=LM(IB-1)+1
C160910              IF(CONDAM(IB).EQ.4)THEN
C160910        SPD(J)=SPD(J)
C160910     :+0.5*DTN*SMBS(MU(J+1),YN1D(J+1),ZYD(J+1),EXPOS(J+1),J+1,TN)
C160910           ELSE
c        SPD(J)=SPD(J)
c     :+DTN*SMBS(MU(J+1),YN1D(J+1),ZYD(J+1),EXPOS(J+1),J+1,TN)
C160910           ENDIF
              ENDDO
      ENDIF
COU2D              DO IB=1,nbb
COU2D        DO J=LM(IB-1)+2,LM(IB)-1
COU2D                DS=0.5*dtn*SMBS4(j)
COU2DC100813          IF(ABS(DS).GT.EPS)THEN
COU2DC100813            vdever(j)=.TRUE.
COU2DC100813          endif
COU2D          SMD(J)=SMD(J)+DS
COU2D          SPD(J-1)=SPD(J-1)+DS
COU2D           ENDDO
COU2D              ENDDO

C Correction sur les d�bits
C--------------------------
c      Write(*,*)'Penten: avant correction sur Q'
      DO IB=1,NBB
      DO J=LM(ib-1)+2,LM(ib)-1
        I=LDETSJ(1,SM(J),J)
        YM(J)=DETYN(1,SM(J),I)
        I=LDETSJ(1,SP(J-1),J-1)
        YP(J-1)=DETYN(1,SP(J-1),I)

C Calcul de perte de charge due � l'�largissement
        IF(YP(J-1).GT.EPSY.AND.YM(J).GT.EPSY)THEN
          CALL ELARGI(J,YP(J-1),YM(J),SP(J-1),SM(J),VP(J-1),VM(J),DQ(J))
          DQ(J)=0.5*SN(J)*DTN*DQ(J)
        ELSE
          DQ(J)=0.
        ENDIF
        DQ(J)=-DQ(J)+0.5*DTN*DETB(YN1D(J),J,YF(J))
      ENDDO
         ENDDO

C On ne fait pas de modification g�ometrique � Tn+1/2
C      CALL CANOG2(QS,YS)

c      Write(*,*)'Penten: avant correction sur les hauteurs'
      DO IB=1,NBB
      DO J=LM(ib-1)+2,LM(ib)-1
C      DO J=2,LL
        IF(SMD(J).LT.EPS) SMD(J)=0.
        I=LDETSJ(1,SMD(J),J)
        YMD(J)=DETYN(1,SMD(J),I)
        IF(SPD(J-1).LT.EPS) SPD(J-1)=0.
        I=LDETSJ(1,SPD(J-1),J-1)
        YPD(J-1)=DETYN(1,SPD(J-1),I)
      ENDDO
         ENDDO
C on fait une correction pour que ecart ymd-ypd inferieur a ym-yp
cympd      DO IB=1,NBB
cympd      DO J=LM(ib-1)+2,LM(ib)-2
C      DO J=2,LL-1
cympd        IF(ABS(YPD(J)-YMD(J)).GT.EPSY)THEN
cympd          IF(YPD(J).GT.YMD(J))THEN
cympd            dz=0.5*(yp(j)-yM(j))
cympd            if(dz.GT.EPSY)THEN
cympd              yMax=0.5*(YPD(j)+YMD(J))+dz
cympd              yMin=0.5*(YPD(j)+YMD(J))-dz
cympd            ELSE
cympd              yMax=0.5*(YPD(j)+YMD(J))
cympd              ymin=ymax
cympd            ENDIF
cympd            If(YPD(j).GT.ymax) then
cympd              YPD(j)=ymax
cympd              I=LDETYJ(1,YPD(J),J)
cympd              SPD(J)=DETSN(1,YPD(J),I)
cympd            ENDIF
cympd            If(YMD(j).lT.ymin) then
cympd              YMD(j)=ymin
cympd              I=LDETYJ(1,YMD(J),J)
cympd              SMD(J)=DETSN(1,YMD(J),I)
cympd            ENDIF
C ymd superieur a Ypd
cympd          ELSE
cympd            dz=0.5*(ym(j)-yp(j))
cympd            if(dz.GT.EPSY)THEN
cympd              yMax=0.5*(YPD(j)+YMD(J))+dz
cympd              yMin=0.5*(YPD(j)+YMD(J))-dz
cympd            ELSE
cympd              yMax=0.5*(YPD(j)+YMD(J))
cympd              ymin=ymax
cympd            ENDIF
cympd            If(YPD(j).lT.ymin) then
cympd              YPD(j)=ymin
cympd              I=LDETYJ(1,YPD(J),J)
cympd              SPD(J)=DETSN(1,YPD(J),I)
cympd            ENDIF
cympd            If(YMD(j).gT.ymax) then
cympd              YMD(j)=ymax
cympd              I=LDETYJ(1,YMD(J),J)
cympd              SMD(J)=DETSN(1,YMD(J),I)
cympd            ENDIF
cympdC fin du if sur ymd/ypd
cympd          ENDIF
C fin du if sur difference faible
cympd        ENDIF
cympd      ENDDO
cympd         ENDDO

      DO IB=1,NBB
         DO J=LM(IB-1)+2,LM(IB)-1
        CALL PNN4(DQ(J),QMD,QPD,J,IB)
c160910        CALL PNN4(DQ(J),QMD,QPD,J,IB,CONDAM(IB),CONDAV(IB))
      ENDDO
         ENDDO
C correction du 10 avril 2013 pour couplage sur conduite Kyoto: a tester
C suppression le 10/08/2013 car pas efficace
C et cree discontinuite debit sur genissiat
C100813      DO IB=1,NBB
C100813      DO J=LM(ib-1)+2,LM(ib)-2
C100813               IF(vdever(j))then
C100813                             QPD(j-1)=QMD(j-1)
C100813                                 QMD(j)=QPD(J)
C100813                           endif
C100813      enddo
C100813      enddo
C on fait une correction pour que ecart qmd-qpd inferieur a qm-qp
cqmpd      DO IB=1,NBB
cqmpd      DO J=LM(ib-1)+2,LM(ib)-2
cqmpd        IF(ABS(QPD(J)-QMD(J)).GT.EPS)THEN
cqmpd          IF(QPD(J).GT.QMD(J))THEN
cqmpd            DQ2=0.5*(Qp(j)-QM(j))
cqymp        DQ2=min(DQ2,0.5*(ypd(j)-ymd(j))*max(abs(vm(j)),abs(vp(j))))
cqmpd            if(DQ2.GT.EPS)THEN
cqmpd              qMax=0.5*(qPD(j)+qMD(J))+DQ2
cqmpd              qMin=0.5*(qPD(j)+qMD(J))-DQ2
cqmpd              If(QPD(j).lT.Qmin) then
cqmpd                QPD(j)=Qmin
cqmpd              ENDIF
cqmpd              If(QMD(j).gT.Qmax) then
cqmpd                QMD(j)=Qmax
cqmpd              ENDIF
cqmpd            ELSE
cqmpd              QMax=0.5*(QPD(j)+QMD(J))
cqmpd              QPD(j)=Qmax
cqmpd              QMD(j)=Qmax
cqmpd            ENDIF
C Qmd superieur a Qpd
cqmpd          ELSE
cqmpd            DQ2=0.5*(Qm(j)-Qp(j))
cqymp        DQ2=min(DQ2,0.5*(ymd(j)-ypd(j))*max(abs(vm(j)),abs(vp(j))))
cqmpd            if(DQ2.GT.EPS)THEN
cqmpd              QMax=0.5*(QPD(j)+QMD(J))+DQ2
cqmpd              QMin=0.5*(QPD(j)+QMD(J))-DQ2
cqmpd              If(QPD(j).lT.Qmin) then
cqmpd                QPD(j)=Qmin
cqmpd              ENDIF
cqmpd              If(QMD(j).gT.Qmax) then
cqmpd                QMD(j)=Qmax
cqmpd              ENDIF
cqmpd            ELSE
cqmpd              QMax=0.5*(QPD(j)+QMD(J))
cqmpd              QPD(j)=Qmax
cqmpd              QMD(j)=Qmax
cqmpd            ENDIF
C fin du if sur qmd/qpd
cqmpd          ENDIF
C fin du if sur difference faible
cqmpd        ENDIF
cqmpd      ENDDO
cqmpd         ENDDO

C Calcul des autres variables aux interfaces
C-----------------------------------------------------------------------
c      Write(*,*)'Penten: avant calcul des autres variables'
      DO IB=1,NBB
      DO J=LM(ib-1)+2,LM(ib)-1
C      DO J=2,LL
        IF(YMD(J).LE.EPSY)THEN
          SMD(J)=0.
          VMD(J)=0.
          YMD(J)=0.
        ELSE
          VMD(J)=QMD(J)/SMD(J)
          IF(ABS(VMD(J)).LT.EPSY) VMD(J)=0.
        ENDIF
        IF(YPD(J-1).LE.EPSY)THEN
          SPD(J-1)= 0.
          VPD(J-1)=0.
          YPD(J-1)=0.
        ELSE
          VPD(J-1) = QPD(J-1)/SPD(J-1)
          IF(ABS(VPD(J-1)).LT.EPSY) VPD(J-1)=0.
        ENDIF
      ENDDO
         ENDDO

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE PNN1(DWM,DWP,SM,SP,VM,VP,YM,YP)
C-----------------------------------------------------------------------
C Calcul des variables aux intermailles au temps Tn
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,J,LL,I,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER nou1Dmax,LM(0:NBMAX)
      PARAMETER(nou1Dmax=50)
      INTEGER IOUV,NREFA(LMAX)
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax),NBOUV
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION ZM(LMAX),ZP(LMAX),QM(LMAX),QP(LMAX)
      DOUBLE PRECISION DWM(LMAX),DWP(LMAX),SM(LMAX),SP(LMAX)
     &  ,VM(LMAX),VP(LMAX),YM(LMAX),YP(LMAX)
      DOUBLE PRECISION VNM,VNP,SNM,SNP
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION YF(LMAX),vmax

      INTEGER LDETYJ
      DOUBLE PRECISION DETFL,DETSN
      EXTERNAL LDETYJ,DETFL,DETSN

      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/PHYS/LM,LL
      COMMON/ZYF/YF
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDNARF/NREFA
      COMMON/NBIEF/NBB

C      Write(*,*)'Pnn1: entr�e'
       vmax=20.
       DO IB=1,NBB
       DO J=LM(IB-1)+2,LM(IB)-1
          YM(J)=ZM(J)-XCTDF(J)
          YP(J-1)=ZP(J-1)-XCTDF(J-1)
        IF(NREFA(J-1).NE.-2.OR.NREFA(J).NE.-2)THEN
          IF(YP(J-1).LE.EPSY)THEN
            QP(J-1)=0.
            VP(J-1)=0.
            DWP(J-1)=0.
            SP(J-1)=0.
            YP(J-1)=0.
          ELSE
            I=LDETYJ(1,YP(J-1),J-1)
            SNP=DETSN(1,YP(J-1),I)
            VNP=QP(J-1)/SNP
                     IF(vnp.gt. max(vmax,2.*vn(j),2.*vn(j-1)))then
                            vnp=max(vmax,2.*vn(j),2.*vn(j-1))
                         qp(j-1)=vnp*snp
                     endif
c                     IF(vnp.lt. min(-vmax,0.5*vn(j),0.5*vn(j-1)))then
c                            vnp=min(-vmax,0.5*vn(j),0.5*vn(j-1))
                     IF(vnp.lt. min(-vmax,2.*vn(j),2.*vn(j-1)))then
                            vnp=min(-vmax,2.*vn(j),2.*vn(j-1))
                         qp(j-1)=vnp*snp
                     endif
            VP(J-1)=VNP
            DWP(J-1)=DETFL(SNP,VNP,I,J-1)
            SP(J-1)=SNP
          ENDIF
          IF(YM(J).LE.EPSY)THEN
            QM(J)=0.
            SM(J)=0.
            DWM(J)=0.
            VM(J)=0.
            YM(J)=0.
          ELSE
            I=LDETYJ(1,YM(J),J)
            SNM=DETSN(1,YM(J),I)
            SM(J)=SNM
            VNM=QM(J)/SNM
                     IF(vnm.gt. max(vmax,2.*vn(j),2.*vn(j+1)))then
                            vnm=max(vmax,2.*vn(j),2.*vn(j+1))
                         qm(j)=vnm*snm
                     endif
C                     IF(vnm.lt. min(-vmax,0.5*vn(j),0.5*vn(j+1)))then
C                            vnm=min(-vmax,0.5*vn(j),0.5*vn(j+1))
                     IF(vnm.lt. min(-vmax,2.*vn(j),2.*vn(j+1)))then
                            vnm=min(-vmax,2.*vn(j),2.*vn(j+1))
                         qm(j)=vnm*snm
                     endif
            VM(J)=VNM
            DWM(J)=DETFL(SNM,VNM,I,J)
          ENDIF
          IF(YN1D(J).LT.EPSY)THEN
            YF(J)=MIN(YM(J)+XCTDF(J)-CTDF(J),YP(J-1)+XCTDF(J-1)-CTDF(J))
          ELSE
            YF(J)=YN1D(J)
          ENDIF
C Fin du IF sur NREFA
        ENDIF
      ENDDO
C      Write(*,*)'Pnn1: deuxi�me boucle'
       DO J=LM(IB-1)+2,LM(IB)-1
C      DO J=2,LL-1
        IF(NREFA(J).LT.0)THEN
          DO IOUV=1,NBOUV
            IF(IA1(IOUV).EQ.J)THEN
              HOUV(J,1)=max(YM(J),0.D0)
            ENDIF
            IF(IA2(IOUV).EQ.J)THEN
              HOUV(J,2)=max(YP(J),0.D0)
            ENDIF
          ENDDO
        ENDIF
      ENDDO
C fin boucle sur IB
      ENDDO
C      Write(*,*)'Pnn1: avant Qouvr'
C Appel � QOUVR � Tn pour le calcul au demi pas de temps
      CALL QOUVR1D
C      Write(*,*)'Pnn1: sortie'

      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE PNN2(DWM,DWP,SM,SP,QMD,QPD)
C-----------------------------------------------------------------------
C Calcul des variables aux intermailles au temps Tn+1/2
C � partir des variables intermailles au temps Tn
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER J,IJ
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION ZM(LMAX),ZP(LMAX),QM(LMAX),QP(LMAX)
      DOUBLE PRECISION SMD(LMAX),SPD(LMAX)
      DOUBLE PRECISION SM(LMAX),SP(LMAX),QMD(LMAX),QPD(LMAX)
C      DOUBLE PRECISION DWM(LMAX),DWP(LMAX),S,Q,QJ,QJ2,DX1
      DOUBLE PRECISION DWM(LMAX),DWP(LMAX),QJ,QJ2,DX1
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
         INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)

      INTEGER LDETSJ,LDETYJ
      DOUBLE PRECISION DETYN,DETL,DETP
      EXTERNAL LDETSJ,LDETYJ,DETYN,DETL,DETP

      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/XMALTD/SMD,SPD
      COMMON/PHYS/LM,LL
      COMMON/DDNARF/NREFA
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/NBIEF/NBB
      COMMON/CONDLI/CONDAM,CONDAV,REGIME

       DO IB=1,NBB
       DO J=LM(IB-1)+2,LM(IB)-1
C      DO J=2,LL
        IF(NREFA(J).GT.-2.AND.NREFA(J-1).GT.-2)THEN
          DX1=XTMAIL(J)-XTMAIL(J-1)
          IF(HOUV(J,1).GT.EPSY)THEN
            QJ=QOUV(J,1)**2/SM(J)
          ELSE
            QJ=0.
          ENDIF
          IF(HOUV(J-1,2).GT.EPSY)THEN
            QJ2=QOUV(J-1,2)**2/SP(J-1)
          ELSE
            QJ2=0.
          ENDIF
          SMD(J)=SM(J)-(DTN*(QM(J)+QOUV(J,1)-QP(J-1)-QOUV(J-1,2))
     &      /(2.*DX1))
          SPD(J-1)=SP(J-1)-(DTN*(QM(J)+QOUV(J,1)-QP(J-1)-QOUV(J-1,2))
     &      /(2.*DX1))
          QMD(J)=QM(J)-(DTN*(DWM(J)+QJ-DWP(J-1)-QJ2)/(2.*DX1))
          QPD(J-1)=QP(J-1)-(DTN*(DWM(J)+QJ-DWP(J-1)-QJ2)/(2.*DX1))
          IF(SM(J).LT.EPS)THEN
            SMD(J)=0.
            QMD(J)=0.
          ENDIF
          IF(SP(J-1).LT.EPS)THEN
            SPD(J-1)=0.
            QPD(J-1)=0.
          ENDIF
        ELSEIF(NREFA(J-1).EQ.-2.AND.NREFA(J).NE.-2)THEN
          DX1=XTMAIL(J)-XTMAIL(J-1)
          IF(HOUV(J,1).GT.EPSY)THEN
            QJ=QOUV(J,1)**2/SM(J)
          ELSE
            QJ=0.
          ENDIF
          IF(HOUV(J-1,2).GT.EPSY)THEN
            IJ=LDETYJ(1,HOUV(J-1,2),J-1)
            QJ2=QOUV(J-1,2)**2/SP(J-1)+DETP(HOUV(J-1,2),IJ)
          ELSE
            QJ2=0.
          ENDIF
          SMD(J)=SM(J)-(DTN*(QM(J)+QOUV(J,1)-QOUV(J-1,2))/(2.*DX1))
          SPD(J-1)=SP(J-1)-(DTN*(QM(J)+QOUV(J,1)-QOUV(J-1,2))/(2.*DX1))
          QMD(J)=QM(J)-(DTN*(DWM(J)+QJ-QJ2)/(2.*DX1))
          QPD(J-1)=QP(J-1)-(DTN*(DWM(J)+QJ-QJ2)/(2.*DX1))
          IF(SM(J).LT.EPS)THEN
            SMD(J)=0.
            QMD(J)=0.
          ENDIF
          IF(SP(J-1).LT.EPS)THEN
            SPD(J-1)=0.
            QPD(J-1)=0.
          ENDIF
        ELSEIF(NREFA(J).EQ.-2.AND.NREFA(J-1).NE.-2)THEN
          DX1=XTMAIL(J)-XTMAIL(J-1)
          IF(HOUV(J,1).GT.EPSY)THEN
            IJ=LDETYJ(1,HOUV(J,1),J)
            QJ=QOUV(J,1)**2/SM(J)+DETP(HOUV(J,1),IJ)
          ELSE
            QJ=0.
          ENDIF
          IF(HOUV(J-1,2).GT.EPSY)THEN
            QJ2=QOUV(J-1,2)**2/SP(J-1)
          ELSE
            QJ2=0.
          ENDIF
          SMD(J)=SM(J)-(DTN*(QOUV(J,1)-QP(J-1)-QOUV(J-1,2))/(2.*DX1))
         SPD(J-1)=SP(J-1)-(DTN*(QOUV(J,1)-QP(J-1)-QOUV(J-1,2))/(2.*DX1))
          QMD(J)=QM(J)-(DTN*(QJ-DWP(J-1)-QJ2)/(2.*DX1))
          QPD(J-1)=QP(J-1)-(DTN*(QJ-DWP(J-1)-QJ2)/(2.*DX1))
          IF(SM(J).LT.EPS)THEN
            SMD(J)=0.
            QMD(J)=0.
          ENDIF
          IF(SP(J-1).LT.EPS)THEN
            SPD(J-1)=0.
            QPD(J-1)=0.
          ENDIF
C Si NREFA=-2
        ELSE
          DX1=XTMAIL(J)-XTMAIL(J-1)
          IF(HOUV(J,1).GT.EPSY)THEN
            IJ=LDETYJ(1,HOUV(J,1),J)
            QJ=QOUV(J,1)**2/SM(J)+DETP(HOUV(J,1),IJ)
          ELSE
            QJ=0.
          ENDIF
          IF(HOUV(J-1,2).GT.EPSY)THEN
            IJ=LDETYJ(1,HOUV(J-1,2),J-1)
            QJ2=QOUV(J-1,2)**2/SP(J-1)+DETP(HOUV(J-1,2),IJ)
          ELSE
            QJ2=0.
          ENDIF
          SMD(J)=SM(J)-(DTN*(QOUV(J,1)-QOUV(J-1,2))/(2.*DX1))
          SPD(J-1)=SP(J-1)-(DTN*(QOUV(J,1)-QOUV(J-1,2))/(2.*DX1))
          QMD(J)=QM(J)-(DTN*(QJ-QJ2)/(2.*DX1))
          QPD(J-1)=QP(J-1)-(DTN*(QJ-QJ2)/(2.*DX1))
          IF(SM(J).LT.EPS)THEN
            SMD(J)=0.
            QMD(J)=0.
          ENDIF
          IF(SP(J-1).LT.EPS)THEN
            SPD(J-1)=0.
            QPD(J-1)=0.
          ENDIF
C Fin du IF sur NREFA=-2
        ENDIF
C fin boucle sur J
              ENDDO
C160910              IF(CONDAM(IB).NE.4)THEN
c      SPD(LM(IB-1)+1)=2.*SPD(LM(IB-1)+1)-SP(LM(IB-1)+1)
c      QPD(LM(IB-1)+1)=2.*QPD(LM(IB-1)+1)-QP(LM(IB-1)+1)
C160910            ENDIF
C160910               IF(CONDAV(IB).NE.4)THEN
c      SMD(LM(IB)-1)=2.*SMD(LM(IB)-1)-SM(LM(IB)-1)
c      QMD(LM(IB)-1)=2.*QMD(LM(IB)-1)-QM(LM(IB)-1)
C160910            ENDIF
C fin boucle sur IB
      ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ELARGI(I,Y1,Y2,S1,S2,V1,V2,DQ)
C-----------------------------------------------------------------------
C Calcule la perte de charge singuli�re pour un �largissement
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I
      DOUBLE PRECISION S1,S2,Y1,Y2,V1,V2,DQ,DELX,TANALP,XIE
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)

      COMMON/XGEOMT/XTMAIL,XCTDF

      DELX=XTMAIL(I)-XTMAIL(I-1)
      TANALP=0.5*(S2/Y2-S1/Y1)/DELX
      IF(TANALP.LE.0.15)THEN
        XIE=0.
      ELSEIF(TANALP.GE.1.)THEN
        XIE=1.
      ELSE
        XIE=0.525*DLOG(TANALP)+1.
      ENDIF
      DQ=0.5*XIE*(V1-V2)**2/DELX

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE PNN4(DQ,QMD,QPD,J,IB)
C160910      SUBROUTINE PNN4(DQ,QMD,QPD,J,IB,CONDAM,CONDAV)
C-----------------------------------------------------------------------
C Calcul des frottements
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,J,LL,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,NBMAX=150)
         INTEGER LM(0:NBMAX)
C160910         INTEGER LM(0:NBMAX),CONDAM,CONDAV
      DOUBLE PRECISION SMD(LMAX),QMD(LMAX),SPD(LMAX),QPD(LMAX)
     +  ,TN,DTN,TNP1,YMD(LMAX),YPD(LMAX),YN1D(LMAX),RHN(LMAX),VN(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION YF(LMAX),DQ,V1
      DOUBLE PRECISION AD,CD,BD

      DOUBLE PRECISION SMBQ2,DETB
      EXTERNAL SMBQ2,DETB

      COMMON/YMPD/YMD,YPD
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/XMALTD/SMD,SPD
      COMMON/PHYS/LM,LL
      COMMON/ZYF/YF
         COMMON/NBIEF/NBB

      V1=1.
        IF(YMD(J).GT.EPSY)THEN
c      Write(*,*)'PNN4 marque 10'
          AD=-0.5*DTN*SMBQ2(1,V1,J,YMD(J))/SMD(J)**2
          CD=-(QMD(J)+DQ)
          BD=AD*CD
          IF(ABS(BD).LT.EPS)THEN
c      Write(*,*)'PNN4 marque 11'
            QMD(J)=-CD
          ELSEIF(BD.LT.0.)THEN
c      Write(*,*)'PNN4 marque 12'
            QMD(J)=(-1.+SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
          ELSE
c      Write(*,*)'PNN4 marque 13'
c      Write(*,*)'AD(',J,')=',AD
c      Write(*,*)'QMD(',J,')=',QMD(J)
c      Write(*,*)'QMD(',J,')=',DQ
c      Write(*,*)'CD(',J,')=',CD
            QMD(J)=(1.-SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
c      Write(*,*)'PNN4 marque 13 sortie'
          ENDIF
c      Write(*,*)'PNN4 marque 14'
        ELSE
c      Write(*,*)'PNN4 marque 15'
          QMD(J)=0.
        ENDIF
c      Write(*,*)'PNN4 marque 16'
        IF(YPD(J-1).GT.EPSY)THEN
c      Write(*,*)'PNN4 marque 17'
          AD=-0.5*DTN*SMBQ2(1,V1,J-1,YPD(J-1))/SPD(J-1)**2
          CD=-(QPD(J-1)+DQ)
          BD=AD*CD
          IF(ABS(BD).LT.EPS)THEN
c      Write(*,*)'PNN4 marque 18'
            QPD(J-1)=-CD
          ELSEIF(BD.LT.0.)THEN
c      Write(*,*)'PNN4 marque 19'
            QPD(J-1)=(-1.+SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
          ELSE
c      Write(*,*)'PNN4 marque 20'
            QPD(J-1)=(1.-SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
          ENDIF
        ELSE
c      Write(*,*)'PNN4 marque 21'
          QPD(J-1)=0.
        ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE GODROE(SND)
C-----------------------------------------------------------------------
C Calcule les variables au pas de temps Tn+1 a partir des flux FLS et FLQ
C aux intermailles determin�s � Tn+1/2 et des variables � Tn
C
C Constitu� des 3 sous-programmes:
C     1) RIEAMS
C     2) RIEAPS
C     3) RIEANS
C
C Entr�es: SN,QN
C Sorties: SNP1,QNP1
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL,J,LMAX,LDETSJ,IK,IJ,LDETYJ,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,NBMAX=150)
         INTEGER LM(0:NBMAX)
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX),SNP1(LMAX),QNP1(LMAX)
     +  ,FLS(LMAX),FLQ(LMAX)
      DOUBLE PRECISION SND(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION RN,DTN,DX,SCAM(NBMAX),SC(NBMAX)
     :   ,SGN,DETYN,TN,TNP1,CHEZY,GRAV,EPS,EPSY,EPSM,Y
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
     :  ,SMD(LMAX),SPD(LMAX),QJ,QJ2,DETP,DETSN
     :,vmd(lmax),vpd(lmax),qm(lmax),qp(lmax)
C ,sp,sm,ym,yp
     :,zm(lmax),zp(lmax)
C ,qmd,qpd
      INTEGER NREFA(LMAX)
      LOGICAL frotloc,frlfix,frotloccd

      EXTERNAL SGN,DETYN,LDETSJ,DETP,LDETYJ,DETSN

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/DDFLUX/FLS,FLQ
      COMMON/PHYS/LM,LL
      COMMON/DDNARF/NREFA
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/XMALTN/ZM,ZP,QM,QP
      COMMON/NBIEF/NBB
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      common/frl/frotloc,frlfix,frotloccd

C Initialisations
C----------------
c      Write(*,*)'Entr�e GODROE'
C les flux ont �t� calcul�s par le 2D en cas de couplage par les extr�mit�s
C on repasse les variables d'extr�mit�s de tn+1/2 a tn+1
C annule le 11 mars 2011
C vmd, vpd, etc pour toutes les extremites
c      DO IB=1,NBB
c        SC(IB)=0.
c        SCAM(IB)=0.
c              J=LM(IB-1)+1
c        QPD=VPD(J)*spd(j)
c              yp=zp(j)-xctdf(j)
c              if(yp.gt.epsy)then
c                IJ=LDETYJ(1,yp,J)
c             sp=detsn(1,yp,j)
c          SPD(J)=2.*SPD(J)-SP
c              else
c                spd(j)=2.*spd(j)
c           endif
c        IF(SPD(J).LT.EPS)THEN
c          SPD(J)= 0.
c          VPD(J)=0.
c        ELSE
c          QPD=2.*QPD-QP(J)
c          VPD(J) = QPD/SPD(J)
c          IF(ABS(VPD(J)).LT.EPSY) VPD(J)=0.
c        ENDIF
c        j=LM(IB)-1
c        QMD=VMD(J)*SMD(J)
c              ym=zm(j)-xctdf(j)
c              if(ym.gt.epsy)then
c                IJ=LDETYJ(1,ym,J)
c             sm=detsn(1,ym,j)
c          SmD(J)=2.*SmD(J)-Sm
c              else
c                smd(j)=2.*smd(j)
c           endif
c        IF(SMD(J).LT.EPS)THEN
c          SMD(J)=0.
c          VMD(J)=0.
c        ELSE
c          QMD=2.*QMD-QM(J)
c          VMD(J)=QMD/SMD(J)
c          IF(ABS(VMD(J)).LT.EPSY) VMD(J)=0.
c        ENDIF
C fin boucle sur bief
c              ENDDO
C traitement des confluences ou condam et condav = 4
      IF(NBB.GT.1)THEN
        CALL CONFLUE
cconfl        call conflue2
      endif
c         write(*,*)'apr�s conflue'
C Calcul de la condition limite amont
C------------------------------------
      DO IB=1,NBB
        IF(CONDAM(IB).NE.4
COU2D     :.AND.CONDAM(IB).NE.6
     :)THEN
          CALL RIEAMS(SCAM(IB),IB)
c      Write(*,*)'GODROE apr�s RIEAMS',IB,SCAM(IB)
c      IF(2.*SCAM(IB).GT.SN(LM(IB-1)+1))THEN
c        SNP1(LM(IB-1)+1)=2.*SCAM(IB)-SN(LM(IB-1)+1)
c      ELSE
c        SNP1(LM(IB-1)+1)=0.
c      ENDIF
c      QNP1(LM(IB-1)+1)=2.*FLS(LM(IB-1)+1)-QN(LM(IB-1)+1)
C else sur condam=4
        ELSE
          SCAM(IB)=0.5*(SN(LM(IB-1)+1)+SNP1(LM(IB-1)+1))
        ENDIF

C Calcul de la condition limite aval
C-----------------------------------
        IF(CONDAV(IB).NE.4
COU2D     :.AND.CONDAV(IB).NE.6
     :)THEN
         CALL RIEAPS(SC(IB),IB)
c      IF(2.*SC(IB).GT.SN(LM(IB)))THEN
c        SNP1(LM(IB))=2.*SC(IB)-SN(LM(IB))
c      ELSE
c        SNP1(LM(IB))=0.
c      ENDIF
c      QNP1(LM(IB)) =2.*FLS(LM(IB)-1)-QN(LM(IB))
C else sur condav=4
        ELSE
          SC(IB)=0.5*(SN(LM(IB))+SNP1(LM(IB)))
        ENDIF
C fin boucle sur IB
      ENDDO
C Calcul du flux aux intermailles
C--------------------------------
      CALL RIEANS(SCAM,SC)
c      Write(*,*)'GODROE apr�s RIEANS'

C recalcul frottement a partir frottement local
         if(frotloc)then
            if(frotloccd)then
C si force de trainee
              call calcfrcd
            else
              call calcfr
            endif
          endif

C Calcul des variables conservatives � Tn+1 � partir des flux
C------------------------------------------------------------
      DO IB=1,NBB
      DO J=LM(IB-1)+2,LM(IB)-1

C Cas "normal" o� on n'utilise pas la loi d'ouvrage
        IF(NREFA(J).GT.-2.AND.NREFA(J-1).GT.-2)THEN
c          Write(*,*)'GODROE "cas normal" J=',J
          DX=XTMAIL(J)-XTMAIL(J-1)
          RN=DTN/DX
          SNP1(J)=SN(J)-RN*(FLS(J)+QOUV(J,1)-FLS(J-1)-QOUV(J-1,2))
c                write(92,*)'js=',j,sn(j),snp1(j),dtn,dx
c                write(92,*)'fls=',j,fls(j),fls(j-1),qouv(j,1),qouv(j-1,2)
          IF(SNP1(J).GT.EPS)THEN
            IK=LDETSJ(0,SNP1(J),J)
C      Write(*,*)'SNP1(',J,')=',SNP1(J)
C      Write(*,*)'IK=',IK
            Y=DETYN(0,SNP1(J),IK)
C      Write(*,*)'GODROE Y=',Y
          ELSE
            Y=0.
               ENDIF
          IF(Y.LT.EPSY)THEN
                  IF(SNP1(J-1).GT.EPS)THEN
                       SNP1(J-1)=snp1(j-1)+SNp1(J)
                  else
C correction du 6 aout 2010
C                       FLS(J)=FLS(J)+SNP1(J)*DTN
                       FLS(J)=FLS(J)+SNP1(J)/RN
                  endif
            SNP1(J)=0.
            SND(J)=0.
            QNP1(J)=0.
C            FLS(J)=FLS(J-1)+SN(J)/RN
C            FLQ(J)=FLQ(J-1)+QN(J)/RN
          ELSE
            IF(HOUV(J,1).GT.EPSY)THEN
              QJ=QOUV(J,1)**2/SMD(J)
            ELSE
              QJ=0.
            ENDIF
            IF(HOUV(J-1,2).GT.EPSY)THEN
              QJ2=QOUV(J-1,2)**2/SPD(J-1)
            ELSE
              QJ2=0.
            ENDIF
            QNP1(J)=QN(J)-RN*(FLQ(J)+QJ-FLQ(J-1)-QJ2)
            SND(J)=SN(J)-0.5*RN*(FLS(J)+QOUV(J,1)-FLS(J-1)-QOUV(J-1,2))
          ENDIF
C      If(J.EQ.58) Write(*,*)'GR1 SNP1(',J,')=',SNP1(J),' SN(',J,')='
C     &  ,SN(J),' QNP1(',J,')=',QNP1(J)

C Cas o� on utilise la seule loi d'ouvrage sur la maille courante mais pas sur la pr�c�dente
        ELSEIF(NREFA(J).EQ.-2.AND.NREFA(J-1).NE.-2)THEN
C      Write(*,*)'GODROE "premi�re maille ouvrage" J=',J
          DX=XTMAIL(J)-XTMAIL(J-1)
          RN=DTN/DX
          SNP1(J)=SN(J)-RN*(QOUV(J,1)-FLS(J-1)-QOUV(J-1,2))
          IF(SNP1(J).GT.EPS)THEN
            IK=LDETSJ(0,SNP1(J),J)
            Y=DETYN(0,SNP1(J),IK)
          ELSE
            Y=0.
          ENDIF
          IF(Y.LT.EPSY)THEN
                  IF(SNP1(J-1).GT.EPS)THEN
                       SNP1(J-1)=snp1(j-1)+SNp1(J)
                  else
C correction du 6 aout 2010
c                       QOUV(J,1)=QOUV(J,1)+SNP1(J)*DTN
                       QOUV(J,1)=QOUV(J,1)+SNP1(J)/RN
                  endif
            SNP1(J)=0.
            SND(J)=0.
            QNP1(J)=0.
          ELSE
            IF(HOUV(J,1).GT.EPSY)THEN
              IJ=LDETYJ(1,HOUV(J,1),J)
              QJ=QOUV(J,1)**2/SMD(J)+DETP(HOUV(J,1),IJ)
            ELSE
              QJ=0.
            ENDIF
            IF(HOUV(J-1,2).GT.EPSY)THEN
              QJ2=QOUV(J-1,2)**2/SPD(J-1)
            ELSE
              QJ2=0.
            ENDIF
            QNP1(J)=QN(J)-RN*(QJ-FLQ(J-1)-QJ2)
            SND(J)=SN(J)-0.5*RN*(QOUV(J,1)-FLS(J-1)-QOUV(J-1,2))
          ENDIF
C      If(J.EQ.56) Write(*,*)'GR2 SNP1(',J,')=',SNP1(J),' SN(',J,')='
C     &  ,SN(J),' QNP1(',J,')=',QNP1(J)

C Cas o� on a utilis� la seule loi d'ouvrage sur la maille pr�c�dente, mais plus sur la maille courante
C On est donc dans la premi�re maille � l'aval de l'ouvrage
        ELSEIF(NREFA(J-1).EQ.-2.AND.NREFA(J).NE.-2)THEN
C      Write(*,*)'GODROE "premi�re maille apr�s ouvrage" J=',J
          DX=XTMAIL(J)-XTMAIL(J-1)
          RN=DTN/DX
          SNP1(J)=SN(J)-RN*(FLS(J)+QOUV(J,1)-QOUV(J-1,2))
C      Write(*,*)'GR3 SNP1(',J,')=',SNP1(J),' SN(',J,')=',SN(J)
C     &  ,' FLS(',J,')=',FLS(J),' QOUV(',J,',1)=',QOUV(J,1)
C     &  ,' QOUV(',J-1,',2)=',QOUV(J-1,2)
          IF(SNP1(J).GT.EPS)THEN
            IK=LDETSJ(0,SNP1(J),J)
            Y=DETYN(0,SNP1(J),IK)
          ELSE
            Y=0.
          ENDIF
C      Write(*,*)'GR3 Y=',Y
          IF(Y.LT.EPSY)THEN
                  IF(SNP1(J-1).LT.EPS)THEN
C correction du 6 aout 2010
C                       FLS(J)=FLS(J)+SNP1(J)*DTN
                       FLS(J)=FLS(J)+SNP1(J)/RN
                  endif
            SNP1(J)=0.
            SND(J)=0.
            QNP1(J)=0.
          ELSE
            IF(HOUV(J,1).GT.EPSY)THEN
              QJ=QOUV(J,1)**2/SMD(J)
            ELSE
              QJ=0.
            ENDIF
            IF(HOUV(J-1,2).GT.EPSY)THEN
              IJ=LDETYJ(1,HOUV(J-1,2),J-1)
              QJ2=QOUV(J-1,2)**2/SPD(J-1)+DETP(HOUV(J-1,2),IJ)
            ELSE
              QJ2=0.
            ENDIF
            QNP1(J)=QN(J)-RN*(FLQ(J)+QJ-QJ2)
            SND(J)=SN(J)-0.5*RN*(FLS(J)+QOUV(J,1)-QOUV(J-1,2))
          ENDIF
C      Write(*,*)'GR3 SNP1(',J,')=',SNP1(J),' SN(',J,')='
C     &  ,SN(J),' QNP1(',J,')=',QNP1(J),' SND(',J,')=',SND(J)
C      Write(*,*)'GR3 SNP1(',J,')=',SNP1(J),' FLS(',J,')=',FLS(J)

C Cas o� on utilise la seule loi d'ouvrage sur la maille courante et sur la maille pr�c�dente (NREFA=-2)
        ELSE
C      Write(*,*)'GODROE "cas loi ouvrage seule" J=',J
c          DX=XTMAIL(J)-XTMAIL(J-1)
c          RN=DTN/DX
c          SNP1(J)=SN(J)-RN*(QOUV(J,1)-QOUV(J-1,2))
c          IF(SNP1(J).GT.EPS)THEN
c            IK=LDETSJ(0,SNP1(J),J)
c            Y=DETYN(0,SNP1(J),IK)
c          ELSE
c            Y=0.
c          ENDIF
          QNP1(J)=0.
          SNP1(J)=0.
          SND(J)=0.
        ENDIF
C fin boucle sur J
      ENDDO
C fin boucle sur IB
      ENDDO

C      Write(*,*)'GODROE fin boucle'

c      Write(*,*)'Sortie GODROE'
C      ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE RIEAMS(SC,IB)
C-----------------------------------------------------------------------
C Calcule la condition limite amont
C on effectue le calcul a tn+1
C sauf depuis le 11/3
C mais sc est a tn+1/2
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NTHMAX,NCLMAX,NBMAX,IB
      PARAMETER(LMAX=3000,NTHMAX=20000,NCLMAX=1000,NBMAX=150)
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
     +  ,SMD(LMAX),SPD(LMAX),VMD(LMAX),VPD(LMAX),FLS(LMAX),FLQ(LMAX)
     +  ,QINP1,C2,S2,V2,CC,VC,TN,DTN,TNP1,Y,T,H,Y2
     +  ,TH(NTHMAX),QTH(NTHMAX),HIMP(NCLMAX),TA(NCLMAX)
         INTEGER NT1(0:NBMAX),NT3(0:NBMAX),REGIME(NBMAX)
     +  ,CONDAV(NBMAX),CONDAM(NBMAX),I,IJ1,IJ2
      DOUBLE PRECISION SC,SN(LMAX),QN(LMAX),PC,P2
C         ,RACIN
     + ,SNP1(LMAX),QNP1(LMAX)

      INTEGER LL,LM(0:NBMAX),LL2
      INTEGER LDETSJ,LDETYJ
      EXTERNAL LDETSJ,LDETYJ
      DOUBLE PRECISION DETFL,DETSN,FQINP1,DETYN,DETCN,DETH,DETP
      EXTERNAL DETFL,DETSN,FQINP1,DETYN,DETCN,DETH,DETP
      DOUBLE PRECISION TMAIL(LMAX),PEN(LMAX),CTDF(LMAX)
C compteur pour ecriture avertissements
          INTEGER IECR

      COMMON/HYDAM/TH,QTH
      COMMON/NHYDM/NT1,NT3
      COMMON/HHAM/TA,HIMP
      COMMON/DDFLUX/FLS,FLQ
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/MAILTN/SN,QN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PHYS/LM,LL

          DATA iecr/0/
          Save iecr

C Initialisation
C---------------
        LL2=LM(IB-1)+1
C spd et vpd sont a tnp1/2
        S2=SPD(LL2)
        V2=VPD(LL2)
c        write(*,*)'ib',ib,s2,v2
        IF (CONDAM(IB).EQ.1)THEN

          QINP1=FQINP1(0.5*(TN+TNP1),IB)
c          QINP1=FQINP1(TNP1,IB)
c                write(*,*)qinp1
C CONDAM=2 SIGNIFIE H DONNEE
        ELSEIF(CONDAM(IB).EQ.2)THEN
           T=0.5*(TN+TNP1)
c           T=TNP1
           DO 200 I=NT3(IB-1)+1,NT3(IB)
              IF(T.LT.TA(I))THEN
                IF(I.NE.NT3(IB-1)+1)THEN
                H=HIMP(I)+(T-TA(I))*(HIMP(I-1)-HIMP(I))/(TA(I-1)-TA(I))
                ELSE
                  H=HIMP(NT3(IB-1)+1)
                ENDIF
                GO TO 201
              ENDIF
 200       CONTINUE
           H=HIMP(NT3(IB))
 201       H=H-CTDF(LL2)
           IF(H.LE.EPSY)THEN
             SC=0.
           ELSE
             IJ1=LDETYJ(1,H,LL2)
             SC = DETSN(1,H,IJ1)
             IJ2=LDETSJ(1,S2,LL2)
             Y2=DETYN(1,S2,IJ2)
      QINP1=SC*(V2-DETH(Y2,IJ2,LL2)+DETH(H,IJ1,LL2))
             VC=QINP1/SC
             CC=DETCN(1,H,IJ1)
             PC=DETP(H,IJ1)
             C2=DETCN(1,Y2,IJ2)
             P2=DETP(Y2,IJ2)
             IF(VC+CC.GT.V2+C2)THEN
C ON CALCULE UN CHOC PAR RANKINE-HUGONIOT AVEC SC DONNEE
               If(SC*(SC-S2)*(PC-P2)/S2.gt.eps)then
                QINP1=SC*V2+SQRT(SC*(SC-S2)*(PC-P2)/S2)
                            endif
              ENDIF
C fin du if sur H
           ENDIF
        ELSE
C on a condam=0
          QINP1=0.
C fin du if sur condam
        ENDIF
C on effectue les calculs maintenant si condam=0 ou 1
        IF(CONDAM(IB).NE.2)THEN
         IF(QINP1.LT.0.) THEN
                 if(iecr.lt.100)then
          WRITE(*,*)'LA CONDITION AMONT EST UNE CONDITION AVAL'
                  iecr=iecr+1
                  endif
          IF(REGIME(IB).EQ.1)THEN
                 if(iecr.lt.100)then
            WRITE(*,*)'CONDITION AMONT ERRONEE'
            WRITE(*,*)'ON NE TIENT PAS COMPTE DE LA HAUTEUR'
                  iecr=iecr+1
                  endif
          ENDIF
C fin du if sur qinp1
         ENDIF


         IJ2=LDETSJ(1,S2,LL2)
         Y2=DETYN(1,S2,IJ2)
         C2=DETCN(1,Y2,IJ2)
c         write(*,*)'Qinp',QINP1,Y2,C2,V2

         IF(V2.GT.C2.OR.Y2.LE.EPSY)THEN
C REGIME = 1 : Q ET H DONNES
          IF(REGIME(IB).EQ.1)THEN
c           T=TNP1
           T=0.5*(TN+TNP1)
           DO 100 I=NT3(IB-1)+1,NT3(IB)
              IF(T.LT.TA(I))THEN
                IF(I.NE.NT3(IB-1)+1)THEN
                H=HIMP(I)+(T-TA(I))*(HIMP(I-1)-HIMP(I))/(TA(I-1)-TA(I))
                ELSE
                  H=HIMP(NT3(IB-1)+1)
                ENDIF
                GO TO 101
              ENDIF
 100       CONTINUE
           H=HIMP(NT3(IB))
 101       H=H-CTDF(LL2)
           IF(H.LE.EPSY)THEN
             SC=0.
           ELSE

             IJ1=LDETYJ(1,H,LL2)
             SC = DETSN(1,H,IJ1)
           ENDIF
          ELSEIF(Y2.GT.EPSY)THEN
c           IF(CONDAM(IB).EQ.1)THEN
c            WRITE(*,*)'FROUDE AMONT SUPERIEUR A 1, =',V2/C2
c            WRITE(*,*)'IL MANQUE UNE CONDITION AMONT'
c            IF(IB.GT.1)THEn
c              WRITE(*,*)'bief ',IB
c            ENDIF
C fin du if sur condam=1
C           ENDIF
C FIN DU IF SUR LE REGIME
          ENDIF
C FIN DU IF SUR v2.gt.c2
          ENDIF

          IF (REGIME(IB).EQ.0.OR.V2.LE.C2) THEN
            IF(REGIME(IB).EQ.1.AND.Y2.GT.EPSY)THEN
                 if(iecr.lt.100)then
              WRITE(*,*)'REGIME FLUVIAL AMONT : FROUDE, =',V2/C2
              WRITE(*,*)'ON ABANDONNE LA HAUTEUR FIXEE'
                  iecr=iecr+1
                  endif
            ENDIF
C V2-H2=V-H  :CALCUL DE SC A PARTIR QINP1 DONNE PAR EGALITE INVARIANT

c            IF(ABS(QINP1).LT.EPS.AND.ABS(V2).LT.EPS)THEN
c              SC=S2
c            ELSE
              CALL DCTMMS(QINP1,S2,V2,SC,LL2)
c            ENDIF
            IF(SC.LE.0.) THEN
              SC =0.
              VC=0.
            ELSE
              VC=QINP1/SC
            ENDIF
            IJ1=LDETSJ(1,SC,LL2)
            Y=DETYN(1,SC,IJ1)
            CC=DETCN(1,Y,IJ1)
C ON VERIFIE QU'IL PEUT Y AVOIR DETENTE

         IF(VC+CC.GT.V2+C2)THEN
          IF(QINP1.LT.-EPS)THEN
                 if(iecr.lt.100)then
            WRITE(*,*)'ERREUR:A L''AMONT ON A UNE CONDITION AVAL'
            WRITE(*,*)'TRAITEE DE FACON APPROXIMATIVE'
                  iecr=iecr+1
                  endif
C          ENDIF
C          ELSEIF(REGIME.EQ.1)THEN
C             WRITE(*,*)'CONDITION AMONT INCOMPATIBLE AVEC CALCUL'
C             WRITE(*,*)'LA CONDITION AMONT EST MAINTENUE MAIS'
C             WRITE(*,*)'LE CALCUL EST APPROXIMATIF'
          ELSEif(ABS(QINP1).LT.EPS)THEN
C CAS OU CONDAM=0 (REFLEXION):CORRESPOND A V2<0 ET CHOC
C ou Q momentanement nul
C            IF(Y2.GT.EPSY)THEN
              IF(V2.EQ.0.)THEN
                              SC=S2
                       ELSE
                 CALL DCHAMV(SC,S2,V2,IJ2,LL2)
                    ENDIF
c              write(*,*)'sc',sc,s2,v2
              IF(SC.LE.0.) THEN
                SC =0.
              ENDIF
C            ELSE
C              SC=0.
C            ENDIF
C           ELSEIF(CONDAM(IB).EQ.1)THEN
            ELSE
             IF(V2.LT.-eps)THEN
                 if(iecr.lt.100)then
               WRITE(*,*)'CONDITION AMONT APPROCHEE'
                  iecr=iecr+1
                  endif
c             ELSEIF(V2.EQ.0..AND.QINP1.EQ.0.)THEN
c               SC=S2
             ELSEIF(Y2.GT.EPSY)THEN
C ON CALCULE UN CHOC PAR RANKINE-HUGONIOT
C        WRITE(*,*)'ON DEVRAIT AVOIR UN CHOC AMONT:SOLUTION APPROCHEE'
              CALL QAMONT(S2,V2,QINP1,SC,IB)
C             ELSE
C CAS D'UN MODELE VIDE : LA SOLUTION DU PROBLEME DE RIEMANN EST UNE DETENTE
C LA HAUTEUR NORMALEMENT DONNEE EST ICI DETERMINEE PAR V-H=0
C FIN DU IF SUR V2
             ENDIF
C FIN DU IF SUR QINP1=0
          ENDIF
C FIN UD IF SUR CHOC
         ENDIF

C FIN DU IF SUR REGIME=0
          ENDIF
C FIN DU IF SUR CONDAM=2
         ENDIF
C DANS TOUS LES CAS ON A MAINTENANT QINP1 DONNE ET SC CALCULE
C BERT

C        IF((CONDAM(IB).EQ.0).AND.(ABS(V2).LT.EPS))SC=S2
C         WRITE(*,*)'S2C',S2,SC
C modification du 11 mars pour repasser a tn+1/2
c        SC =0.5*(SN(LL2)+SC)
C                write(*,*)'fls',ll2,tn,qinp1,qn(ll2),fls(ll2)

       IF(SC.LE.EPS) THEN
              SC =0.
              SNP1(LL2)=0.
              QNP1(LL2)=0.
              FLS(LL2)=0.
              FLQ(LL2) = 0.
       ELSE
C on fait une petite erreur pour eviter des oscillations en gardant le VC a tnp1
         vc=qinp1/sc
                 If(vc.gt.eps)then
                         fls(ll2)=vc*sc
C on suppose que si sc-SN grand, sn est mauvais donc on garde sc
c120413   on met snp1 et qnp1 a tn+1/2
c120413             if(abs(S2-SC).GT.ABS(SC-SN(LL2)))THEN
c120413                         SNP1(LL2) =2.*SC-SN(LL2)
c120413                     ELSE
             SNP1(LL2)=SC
c120413                     ENDIF
             if(snp1(ll2).lt.eps)THEN
                snp1(ll2)=0.
                qnp1(LL2)=0.
             else
               qnp1(LL2)=VC*Snp1(LL2)
             endif
C si vc=0
C                  elseIF(2.*SC.GT.SN(LL2)+EPS)THEN
                  else
C on suppose que si sc-SN grand, sn est mauvais donc on garde sc
c120413             if(abs(S2-SC).GT.ABS(SC-SN(LL2)))THEN
c120413                         SNP1(LL2) =2.*SC-SN(LL2)
c120413                     ELSE
             SNP1(LL2)=SC
c120413                     ENDIF
                    fls(ll2)=0.
            QNP1(LL2)=0.
c         ELSE
c           SNP1(LL2)=0.
c           QNP1(LL2)=0.
c           FLS(LL2)=0.
C fin du if sur vc=0
         ENDIF
c         FLS(LL2) = SC*VC
         VC=FLS(LL2)/SC
         IJ1=LDETSJ(1,SC,LL2)
         FLQ(LL2) = DETFL(SC,VC,IJ1,LL2)
C fin du if sur sc=0
        ENDIF
        RETURN
        END


C-----------------------------------------------------------------------
      SUBROUTINE RIEAPS(SC,IB)
C-----------------------------------------------------------------------
C Calcule la condition limite aval
C   CONDAV = 0 sortie libre
C   CONDAV = 1 Q(Z) � la sortie
C   CONDAV = 2 r�flexion au bout du domaine
C   CONDAV = 3 Z(T) � la sortie
C   CONDAV = 4 confluence non traite ici
C   CONDAV = 5 r�gime uniforme
C   CONDAV = 7 regime critique
C on effectue le calcul a tn+1/2depuis le 11 mars 2011
C mais sc est a tn+1/2
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,IJ1,IJ,NITER,I,NCLMAX,NBMAX,IB,LL2
      PARAMETER(LMAX=3000,NCLMAX=1000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,NT2(0:NBMAX)
     : ,CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION SNP1(LMAX),QNP1(LMAX)
      DOUBLE PRECISION SMD(LMAX),SPD(LMAX),VMD(LMAX),VPD(LMAX)
      DOUBLE PRECISION FLS(LMAX),FLQ(LMAX),YH(NCLMAX),QYH(NCLMAX)
      DOUBLE PRECISION S1,V1,H1,SC,VC,V2,F1,YLP,YLM,YL2,C1,CC,Y1,S2
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM,TN,TNP1,DTN,T
     +  ,TMAIL(LMAX),PEN(LMAX),CTDF(LMAX)
      INTEGER LDETSJ,LDETYJ
      DOUBLE PRECISION DETFL,DETYN,DETSN,DETH,DETCN,DETP
      EXTERNAL DETFL,DETYN,DETSN,DETH,LDETSJ,LDETYJ,DETCN,DETP

      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PHYS/LM,LL
      COMMON/HYDAV/YH,QYH
      COMMON/NHYDA/NT2
      COMMON/DDFLUX/FLS,FLQ
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/MAILTN/SN,QN
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/MALTNP/SNP1,QNP1

       NITER=0
C       IF (CONDAV(IB).EQ.4)RETURN

C BERT:PAS SUR DES INDICES
       LL2=LM(IB)-1
C smd et vmd sont a tnp1/2
       S1 = SMD(LL2)
       V1 = VMD(LL2)
       IJ1=LDETSJ(1,S1,LL2)
       Y1=DETYN(1,S1,IJ1)
       H1 = DETH(Y1,IJ1,LL2)
C initialisation de variables utilisees en sortie de sosu programmes
        S2=0.
        V2=0.
        IJ=0
        YL2=0.

C------------------------------------------------C
C    TYPE DE CONDITION A LA LIMITE AVAL :        C
C      SI CONDAV = 0 CONDITION DE SORTIE LIBRE   C
C      SI CONDAV = 1 CONDITION Q(Y) A LA SORTIE  C
C      SI CONDAV = 2 CONDITION DE REFLEXION      C
C      SI CONDAV = 3 Z(T) CONNUE
C      SI CONDAV = 4 CONFLUENCE
C      SI CONDAV = 5 regime uniforme
C      SI CONDAV = 7 regime critique
C------------------------------------------------C
C remarque  condav= 4 pas traite ici

C 1)---->CONDITION LIMITE :Q(Y)
C----------------------------------------------

      IF (CONDAV(IB).EQ.1) THEN
       IF(S1.GT.0.)THEN
       C1 = DETCN(1,Y1,IJ1)
       F1 = V1/C1
       IF (F1.GT.1.) THEN
               IF(IB.GT.1)THEN
           WRITE(*,*)'FROUDE AVAL= ',F1,'  >1  BIEF ',IB
               else
           WRITE(*,*)'FROUDE AVAL= ',F1,'  >1'
               endif
C           WRITE(*,*)'ON PASSE EN SORTIE LIBRE'
C           GO TO 1200
        ENDIF
       ELSE
       C1=0.
       ENDIF
C calcul pour detente
C       IDS=1
       CALL QYAV(Y1,S1,V1,H1,V2,S2,YL2,IJ,LL2,IB)
C TRAITEMENT DES RESULTATS: ON VERIFIE QU 'IL Y A BIEN DETENTE
       IF(YL2.GT.EPSY)THEN
          VC=V2
          SC=S2
          CC = DETCN(1,YL2,IJ)
c010710          IF(VC-CC.LT.V1-C1)THEN
C H1 EST LA VITESSE DU CHOC MAINTENANT
C            H1=-.5*(V1-C1+VC-CC)
C            IDS=0
C            CALL QYAV(Y1,S1,V1,H1,V2,S2,YL2,IJ,IDS,IB)
c010710                     if(IB.GT.1) Then
c010710              WRITE(*,*)'RESULTAT AVAL APPROCHE BIEF ',IB
c010710                     ELSE
c010710              WRITE(*,*)'RESULTAT AVAL APPROCHE'
c010710                  ENDIF
C            IF(YL2.GT.EPSY)THEN
C              VC=V2
C              SC=S2
C            ELSE
C              SC=0.
C              VC=0.
C            ENDIF
C FIN DU IF SUR CHOC
c010710          ENDIF
        ELSE
          SC=0.
          VC=0.
        ENDIF
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C condition limite = regime uniforme
      ELSEIF (CONDAV(IB).EQ.5) THEN
C calcul pour detente
C       IDS=1
       CALL QYAVUN(Y1,V1,H1,V2,S2,YL2,IJ,LL2)
C TRAITEMENT DES RESULTATS
       IF(YL2.GT.EPSY)THEN
          VC=V2
          SC=S2
        ELSE
          SC=0.
          VC=0.
        ENDIF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C condition limite = regime critique
      ELSEIF (CONDAV(IB).EQ.7) THEN
C calcul pour detente
C       IDS=1
       CALL QYAVCR(Y1,V1,H1,V2,S2,YL2,IJ,LL2)
C TRAITEMENT DES RESULTATS
       IF(YL2.GT.EPSY)THEN
          VC=V2
          SC=S2
        ELSE
          SC=0.
          VC=0.
        ENDIF

      ELSEIF(CONDAV(IB).EQ.3)THEN
C 2) CONDITION LIMITE Z(T) CONVERTIE EN Y(T):
C---------------------------------------------
       IF(S1.GT.0.)THEN
         C1 = DETCN(1,Y1,IJ1)
         F1 = V1/C1
         IF (F1.GT.1) THEN
               IF(IB.GT.1)THEN
           WRITE(*,*)'FROUDE AVAL= ',F1,'  >1  BIEF ',IB
               else
           WRITE(*,*)'FROUDE AVAL= ',F1,'  >1'
               endif
C           WRITE(*,*)'ON PASSE EN SORTIE LIBRE'
C           GO TO 1200
         ENDIF
       ELSE
         C1=0.
       ENDIF
c       T=TNP1
       T=0.5*(TN+TNP1)

       DO 100 I=NT2(IB-1)+1,NT2(IB)
C dans la variable QYH se trouve le temps de la loi Z(T)
           IF(T.LT.QYH(I))THEN
              IF(I.NE.NT2(IB-1)+1)THEN
               YL2=YH(I)+(T-QYH(I))*(YH(I-1)-YH(I))/(QYH(I-1)-QYH(I))
                     YL2=YL2-CTDF(LM(IB))
              ELSE
               YL2=YH(NT2(IB-1)+1)
                     YL2=YL2-CTDF(LM(IB))
              ENDIF
              GO TO 101
           ENDIF
 100    CONTINUE
        YL2=YH(NT2(IB))
        YL2=YL2-CTDF(LM(IB))
 101    IF(YL2.GT.epsy)THEN
c 101    IF(YL2.GT.0.)THEN
C         IF(ABS(YL2-Y1).GT.EPSY)THEN
          IJ=LDETYJ(1,YL2,LL2)
          SC=DETSN(1,YL2,IJ)
          VC=V1+H1-DETH(YL2,IJ,LL2)
          CC = DETCN(1,YL2,IJ)
          IF(VC-CC.LT.V1-C1)THEN
C ON CALCULE LE CHOC PAR RANKINE-HUGONIOT
            IF(S1.GT.0.)THEN
             CALL QAVAL(SC,YL2,IJ,VC,S1,Y1,IJ1,V1)
c on garde sc et vc de la detente
c            ELSE
c             SC=0.
            ENDIF
          ENDIF
C         ELSE
C           YL2=Y1
C           SC=S1
C           VC=V1
C          ENDIF
        ELSE
          SC=0.
          VC=0.
        ENDIF

C 3)---->AUTRE CONDITION LIMITE :REFLEXION
C------------------------------------------
         ELSEIF(CONDAV(IB).EQ.2)THEN
C SI V1<0 ON A UNE DETENTE ; SI V1>0 UN CHOC
             IF(ABS(V1).LT.EPS)THEN
C             IF(V1.EQ.0.)THEN
               SC=S1
             ELSE IF(V1.GT.0.)THEN
               CALL DCHAMV(SC,S1,V1,IJ1,LL2)
               IJ=LDETSJ(1,SC,LL2)
             ELSE
C RESOLUTION DE H(SC)=H1+V1
               IF(H1+V1.LE.0.)THEN
                 SC=0.
               ELSE
                 YLM=0.
                 YLP=Y1
 1100            YL2=.5*(YLP+YLM)
                 IJ=LDETYJ(1,YL2,LL2)
                 V2=V1+H1-DETH(YL2,IJ,LL2)
                 IF(V2.LT.0.)THEN
                   YLP=YL2
                 ELSE
                   YLM=YL2
                 ENDIF
                 IF(ABS(YLP-YLM).LT.EPSY)GO TO 1101
                 NITER=NITER+1
                 IF(NITER.GT.50)THEN
               IF(IB.GT.1)THEN
          WRITE(*,*)'CONDITION AVAL NON VERIFIEE ',ABS(YLP-YLM),
     :' BIEF ',IB
               else
          WRITE(*,*)'CONDITION AVAL NON VERIFIEE ',ABS(YLP-YLM)
               endif
          WRITE(*,*)'DELTA OBTENU SUR V=',V2,
     :'  Y MAX=',YLP,'Y MIN=',YLM
                 ELSE
                   GO TO 1100
                 ENDIF
 1101            IF(YL2.GT.0.)THEN

                   YL2=.5*(YLP+YLM)
                   IJ=LDETYJ(1,YL2,LL2)
                   SC=DETSN(1,YL2,IJ)

                 ELSE
                   SC=0.
                 ENDIF
               ENDIF
             ENDIF
             VC=0.
C 4)---->AUTRE CONDITION LIMITE :FLUX LIBRE (CONDAV=0)
C------------------------------------------
       ELSE
         IF(S1.GT.0.)THEN
           C1 = DETCN(1,Y1,IJ1)
           F1 = V1/C1
C           IF (F1.LT.1..AND.F1.NE.0.) THEN
C            WRITE(*,*)'FROUDE AVAL = ',F1,'  <1'
C             WRITE(*,*)'IL MANQUE UNE CONDITION AVAL'
C           ENDIF
         ELSE
           S1=0.
           V1=0.
         ENDIF
         SC=S1
         VC=V1
c         GO TO 1200
       ENDIF
C TRAITEMENT DES CAS DE SORTIE FLUVIALE
C mis en commentaire le 11 mars
c       SC=.5*(SC+SN(LM(IB)))
       IF(SC.LE.EPS) THEN
         SC = 0.
         FLS(LL2) = 0.
         FLQ(LL2) = 0.
         SNP1(LM(IB))=0.
         QNP1(LM(IB))=0.
       ELSE
C on fait une petite erreur pour eviter des oscillations en gardant le VC a tnp1
C marche pour une courbe de tarage
                 If(vc.gt.eps)then
                   fls(ll2)=sc*vc
C08           QNP1(LM(IB)) =2.*FLS(LL2)-QN(LM(IB))
c08           IF(QNP1(LM(IB))*fls(ll2).Le.0.)THEN
c08             QNP1(LM(IB))=0.
c08             FLS(LL2)=0.5*QN(LM(IB))
c120413   on met snp1 et qnp1 a tn+1/2
c120413              IF(ABS(SC-SN(LM(IB))).LT.ABS(SC-S1))THEN
c120413                         SNP1(LM(IB)) =2.*SC-SN(LM(IB))
c120413                         else
                          SNP1(LM(IB))=SC
c120413                          ENDIF
                         if(snp1(lm(ib)).lt.eps)snp1(lm(ib))=0.
                         QNP1(LM(IB))=SNp1(lm(ib))*vc
c08                   else
c08                         snp1(lm(ib))=qnp1(lm(ib))/vc
c08           ENDIF
C si vc=0
c120413                  elseIF(2.*SC.GT.SN(LM(IB)))THEN
c120413           SNP1(LM(IB)) =2.*SC-SN(LM(IB))
c120413                   fls(ll2)=0.
c120413             QNP1(LM(IB))=0.
         ELSE
c120413           SNP1(LM(IB))=0.
           SNP1(LM(IB))=SC
           QNP1(LM(IB))=0.
                 FLS(LL2)=0.
C fin du if sur vc=0
         ENDIF
c         FLS(LL2) = SC*VC
         VC=FLS(LL2)/SC
         IJ=LDETSJ(1,SC,LL2)
         FLQ(LL2) = DETFL(SC,VC,IJ,LL2)
C fin du if sur sc=0
                endif
       RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE RIEANS(SCAM,SC)
C-----------------------------------------------------------------------
C Calcul des flux aux intermailles � Tn+1/2
C par la r�solution du probl�me de Rieman (par Roe et correction Leveque)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,nou1Dmax,noe1Dmax,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000
     :,nou1Dmax=50,noe1Dmax=10,NBMAX=150)
      INTEGER J,IJ,IJ1,IJ2,IOUV,NBB,M
      DOUBLE PRECISION S1,V1,S2,V2,Y1,Y2,Y,V,S,VC,CC,CC1,CC2
     +  ,MA11,MA12,MA21,MA22,L1,L2,L1C,L2C,XL2,B,B1,B2
      INTEGER LM(0:NBMAX),LL
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax)
     :,NBOUV,NREFA(LMAX)
      DOUBLE PRECISION SMD(LMAX),SPD(LMAX),VMD(LMAX),VPD(LMAX)
      DOUBLE PRECISION FLS(LMAX),FLQ(LMAX)
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
      DOUBLE PRECISION SN(LMAX),QN(LMAX),VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
     :, RACIN,VMAX,SC(NBMAX),SCAM(NBMAX)

      INTEGER LDETSJ,LDETYJ
      DOUBLE PRECISION DETL,DETFL,DETP,DETYN,DETBET,DETPM
     &  ,DETLMI,DETPMI,DETSMI,DETVMI,DETSN,SGN
      EXTERNAL LDETSJ,LDETYJ
      EXTERNAL DETL,DETFL,DETP,DETYN,DETBET,DETPM
     &  ,DETLMI,DETPMI,DETSMI,DETVMI,DETSN,SGN

      COMMON/NBIEF/NBB
      COMMON/XMALTD/SMD,SPD
      COMMON/VITTD/VMD,VPD
      COMMON/DDFLUX/FLS,FLQ
      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDNARF/NREFA
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER

C utilisee pour limiter vinter pour calcul debit solide
C limite uniquement si instabilite numerique
      VMAX=20.

      DO M=1,NBB
C Premi�re intermaille
      IJ=LDETSJ(1,SCAM(M),LM(M-1)+1)
      Y=DETYN(1,SCAM(M),IJ)
      IF(Y.LE.EPSY)THEN
        XL1(LM(M-1)+1)=0.
        VINTER(LM(M-1)+1)=0.
        SINTER(LM(M-1)+1)=0.
        YINTER(LM(M-1)+1)=0.
        RHINTER(LM(M-1)+1)=0.
      ELSE
        SINTER(LM(M-1)+1)=DETSMI(1,Y,IJ,LM(M-1)+1)
        V=FLS(LM(M-1)+1)/SCAM(M)
C        VINTER(LM(M-1)+1)=MIN(DETVMI(1,V,Y,IJ,LM(M-1)+1),2.*V)
        VINTER(LM(M-1)+1)=DETVMI(1,V,Y,IJ,LM(M-1)+1)
C le facteur 2 est arbitraire : a reconsiderer pour de grands d�bordements
           IF(abs(vinter(LM(M-1)+1)).gt.2.*abs(v))then
                vinter(LM(M-1)+1)=2.*sgn(vinter(LM(M-1)+1))*abs(v)
              endif
        YINTER(LM(M-1)+1)=Y
        RHINTER(LM(M-1)+1)=SINTER(LM(M-1)+1)/DETPMI(1,Y,IJ,LM(M-1)+1)
        XL1(1)=DETLMI(1,Y,IJ,LM(M-1)+1)
      ENDIF

C Derni�re intermaille
      IJ=LDETSJ(1,SC(M),LM(M)-1)
      Y=DETYN(1,SC(M),IJ)
      IF(Y.LE.EPSY)THEN
        XL1(LM(M)-1)=0.
        VINTER(LM(M)-1)=0.
        SINTER(LM(M)-1)=0.
        YINTER(LM(M)-1)=0.
        RHINTER(LM(M)-1)=0.
      ELSE
        SINTER(LM(M)-1)=DETSMI(1,Y,IJ,LM(M)-1)
        V=FLS(LM(M)-1)/SC(M)
C        VINTER(LM(M)-1)=MIN(DETVMI(1,V,Y,IJ,LM(M)-1),2.*V)
        VINTER(LM(M)-1)=DETVMI(1,V,Y,IJ,LM(M)-1)
C le facteur 2 est arbitraire : a reconsiderer pour de grands d�bordements
           IF(abs(vinter(LM(M)-1)).gt.2.*abs(v))then
                vinter(LM(M)-1)=2.*sgn(vinter(LM(M)-1))*abs(v)
              endif
        YINTER(LM(M)-1)=Y
        RHINTER(LM(M)-1)=SINTER(LM(M)-1)/DETPMI(1,Y,IJ,LM(M)-1)
        XL1(LM(M)-1)=DETLMI(1,Y,IJ,LM(M)-1)
      ENDIF
      ENDDO

C Traitement des ouvrages sur les mailles courantes
      DO 1002 M=1,NBB
      DO 1000 J=LM(M-1)+2,LM(M)-2
        IF(NREFA(J).LT.0)THEN
          DO 1001 IOUV=1,NBOUV
            IF(IA1(IOUV).EQ.J)THEN
              IJ1=LDETSJ(1,SMD(J),J)
              HOUV(J,1)=DETYN(1,SMD(J),IJ1)
            ENDIF
            IF(IA2(IOUV).EQ.J)THEN
              IJ1=LDETSJ(1,SPD(J),J)
              HOUV(J,2)=DETYN(1,SPD(J),IJ1)
            ENDIF
 1001     CONTINUE
      ENDIF
 1000   CONTINUE
 1002 CONTINUE
C Appel � QOUVR � Tn+1/2 pour le calcul au pas de temps complet
c     CALL QOUVR

C Intermailles courantes
      DO 100 M=1,NBB
      DO 101 J=LM(M-1)+2,LM(M)-2
C      DO J=2,LL-1
        IF(NREFA(J).GT.-2)THEN
C Cas "normal" o� le d�bit n'est pas donn� par la seule loi d'ouvrage
          S1=SMD(J)
          V1=VMD(J)
          S2=SPD(J)
          V2=VPD(J)
          IJ1=LDETSJ(1,S1,J)
          IJ2=LDETSJ(1,S2,J)
          Y1=DETYN(1,S1,IJ1)
          Y2=DETYN(1,S2,IJ2)
          B1=DETBET(1,Y1,IJ1,J)
          B2=DETBET(1,Y2,IJ2,J)
          IF(Y1.LE.EPSY)THEN
            CC1=0.
            XL1(J)=0.
            IF(Y2.LE.EPSY)THEN
              CC=0.
              XL2=0.
            ELSE
              XL2=DETL(1,Y2,IJ2)
              CC2=SQRT(GRAV*S2/XL2)
              VC=B2*V2
              RACIN=DETP(Y2,IJ2)/S2-VC*(1.-B2)*V2
              If (RACIN.GT.EPS)THEN
                CC=SQRT(RACIN)
              ELSE
                CC=0.
              ENDIF
            ENDIF
          ELSE
            XL1(J)=DETL(1,Y1,IJ1)
            CC1=SQRT(GRAV*S1/XL1(J))
            IF(Y2.LE.EPSY)THEN
              XL2=0.
              CC2=0.
              VC=B1*V1
              RACIN=DETP(Y1,IJ1)/S1-VC*(1.-B1)*V1
              If (RACIN.GT.EPS)THEN
                CC=SQRT(RACIN)
              ELSE
                CC=0.
              ENDIF
            ELSE
              XL2=DETL(1,Y2,IJ2)
              CC2=SQRT(GRAV*S2/XL2)
              VC=(V1*S1**.5*B1**.5+V2*S2**.5*B2**.5)
     &          /((S1**.5/B1**.5)+(S2**.5/B2**.5))
              IF(ABS(S1-S2).LT.EPSY)THEN
                S=0.5*(S1+S2)
                IJ=LDETSJ(1,S,J)
                Y=DETYN(1,S,IJ)
                B=DETBET(1,Y,IJ,J)
C modif du 06 aout 2010
c                RACIN=B1**2*V1**2+CC1**2-B1*V1**2
c     &            *(1.-S1*(B1-B)/(B1*EPS))
                RACIN=CC1**2+VC**2*(1.-
     :(1.-S*(B1-B)/(B*EPSY))/B)
                If (RACIN.GT.EPS)THEN
                  CC=SQRT(RACIN)
                ELSE
                  CC=0.
                ENDIF
              ELSE
                IF((S1.GT.S2.AND.DETP(Y1,IJ1).LT.DETP(Y2,IJ2))
     &            .OR.(S1.LT.S2.AND.DETP(Y1,IJ1).GT.DETP(Y2,IJ2)))THEN
                  CC=CC1
                ELSE
C modif du 06 aout 2010
C                  RACIN=(DETP(Y2,IJ2)-DETP(Y1,IJ1))/(S2-S1)
C     &        +VC**2*(1.-(SQRT(S1/B1)-SQRT(S2/B2))/(SQRT(S1)-SQRT(S2)))
                   racin=(DETP(Y2,IJ2)-DETP(Y1,IJ1))/(S2-S1)
     :+VC**2*(1.-((S1/B1-S2/B2)/(S1-S2)))
                  If (RACIN.GT.EPS)THEN
                    CC=SQRT(RACIN)
                  ELSE
                    CC=0.
                  ENDIF
                ENDIF
              ENDIF
            ENDIF
          ENDIF

          IF(CC.LT.EPS)THEN
            FLS(J)=0.5*(V2*S2+V1*S1)
            FLQ(J)=0.5*(DETFL(S1,V1,IJ1,J)+DETFL(S2,V2,IJ2,J))
C            Y=0.25*(SQRT(Y1)+SQRT(Y2))**2
             S=0.25*(SQRT(S1)+SQRT(S2))**2
             IJ=LDETSJ(1,S,J)
             Y=DETYN(1,S,IJ)
            IF(Y.GT.EPSY)THEN
c              IJ=LDETYJ(1,Y,J)
c              S=DETSN(1,Y,IJ)
              SINTER(J)=DETSMI(1,Y,IJ,J)
              V=FLS(J)/S
C              VINTER(J)=MIN(DETVMI(1,V,Y,IJ,J),2.*V)
              VINTER(J)=DETVMI(1,V,Y,IJ,J)
                       IF(abs(vinter(j)).gt.2.*abs(v))then
                              vinter(j)=2.*sgn(vinter(j))*abs(v)
                       endif
              YINTER(J)=Y
              RHINTER(J)=SINTER(J)/DETPMI(1,Y,IJ,J)
              XL1(J)=DETLMI(1,Y,IJ,J)
            ELSE
              VINTER(J)=0.
              SINTER(J)=0.
              YINTER(J)=0.
              RHINTER(J)=0.
              XL1(J)=0.
            ENDIF
          ELSE
            L1=VC-CC
            L2=VC+CC
            IF(((B1*V1-CC1).LT.0.).AND.(0.LT.(B2*V2-CC2)))THEN
              L1C=(B2*V2-CC2)*(L1-B1*V1+CC1)/(B2*V2-CC2-B1*V1+CC1)
              IF(((B1*V1+CC1).LT.0.).AND.(0.LT.(B2*V2+CC2)))THEN
                L2C=(B2*V2+CC2)*(L2-B1*V1-CC1)/(B2*V2+CC2-B1*V1-CC1)
              ELSE
                L2C=.5*(ABS(L2)+L2)
              ENDIF
              MA11=L1C*L2-L2C*L1
              MA12=L2C-L1C
              MA22=L2*L2C-L1*L1C
            ELSE
              IF(((V1+CC1).LT.0.).AND.(0.LT.(V2+CC2)))THEN
                L1C=0.5*(ABS(L1)+L1)
                L2C=(B2*V2+CC2)*(L2-B1*V1-CC1)/(B2*V2+CC2-B1*V1-CC1)
                MA11=L1C*L2-L2C*L1
                MA12=L2C-L1C
                MA22=L2*L2C-L1*L1C
              ELSE
                MA12=.5*(L2+ABS(L2)-L1-ABS(L1))
                MA22=.5*((L2+ABS(L2))*L2-(L1+ABS(L1))*L1)
                IF(L1*L2.GT.0.)THEN
                  MA11=0.
                ELSE
                  MA11=ABS(L1)*L2
                ENDIF
              ENDIF
            ENDIF
            MA21=-L1*L2*MA12
            FLS(J)=V2*S2-(MA11*(S2-S1)+MA12*(V2*S2-V1*S1))/(2.*CC)
            FLQ(J)=DETFL(S2,V2,IJ2,J)
     &        -(MA21*(S2-S1)+MA22*(V2*S2-V1*S1))/(2.*CC)
C      If(J.EQ.57) Write(*,*)'Rieans2 Tn=',TN,' FLS(',J,')=',FLS(J)
C     &  ,' FLQ(',J,')=',FLQ(J),' V1=',V1,' S1=',S1,' V2=',V2,' S2=',S2
C            S=0.5*(S1+S2)
C            IJ=LDETSJ(1,S,J)
C            Y=DETYN(1,S,IJ)
C            Y=0.25*(SQRT(Y1)+SQRT(Y2))**2
             S=0.25*(SQRT(S1)+SQRT(S2))**2
             IJ=LDETSJ(1,S,J)
             Y=DETYN(1,S,IJ)
            IF(Y.GT.EPSY)THEN
c              IJ=LDETYJ(1,Y,J)
c              S=DETSN(1,Y,IJ)
              SINTER(J)=DETSMI(1,Y,IJ,J)
              V=FLS(J)/S
c              VINTER(J)=MIN(DETVMI(1,V,Y,IJ,J),2.*V)
              VINTER(J)=DETVMI(1,V,Y,IJ,J)
                       IF(abs(vinter(j)).gt.2.*abs(v))then
                              vinter(j)=2.*sgn(vinter(j))*abs(v)
                       endif
              YINTER(J)=Y
              RHINTER(J)=SINTER(J)/DETPMI(1,Y,IJ,J)
              XL1(J)=DETLMI(1,Y,IJ,J)
            ELSE
              VINTER(J)=0.
              SINTER(J)=0.
              YINTER(J)=0.
              RHINTER(J)=0.
              XL1(J)=0.
            ENDIF
          ENDIF

        ELSE
C Si NREFA=-2 (cas o� l'on n'utilise pas St Venant, mais la loi d'ouvrage seule),
C les variables ne servent pas au calcul, on les conserve d'une intermaille � l'autre
          FLS(J)=0.
          XL1(J)=XL1(J-1)
          VINTER(J)=VINTER(J-1)
          SINTER(J)=SINTER(J-1)
          YINTER(J)=YINTER(J-1)
          RHINTER(J)=RHINTER(J-1)
C Fin du IF sur NREFA>-2
        ENDIF
101   CONTINUE
100   CONTINUE

      DO J=1,LL
        IF(abs(vinter(j)).GT.vmax)vinter(j)=sgn(vinter(j))*vmax
      enddo

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CANOGE
C-----------------------------------------------------------------------
C Calcul du transport solide et modification de la g�om�trie � Tn+1/2
C Entr�es: LM,LL,TN,DTN,TNP1,DXMAIL,QLAT,NMU,TSMU,QSMU,VINTER,RHINTER,FR1,MACTP1,DACTP1,SACTP1 (+VN,YND si calcul centremailles)
C Sorties: QSLAT,LACT,JRH,CAPSOL,QSR,DMOB,SMOB,MACTP1,DACTP1,SACTP1,XMODSEC (+modif. g�o. faites dans EROSEC et DEPSEC)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER I,LL,LM(0:NBMAX),IB,NBB
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER METHODE,DEPOT
C      DOUBLE PRECISION MBADEM,MBAOBT,MBASTO,DBAOBT,SBAOBT,MACIND,SENSI
      DOUBLE PRECISION MBADEM,MBASTO,MACIND,SENSI
     &  ,CHR,CONCENTRATION
      DOUBLE PRECISION FRPEAU
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,COEFC
     :,EPSS
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION FR1(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION VNP1(LMAX),YNP1(LMAX),RHNP1(LMAX)
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),LACT(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX),VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION SND(LMAX),YND(LMAX)
      DOUBLE PRECISION QLAT(LMAX)
      DOUBLE PRECISION QSLAT(LMAX)
      DOUBLE PRECISION QSMU(LMAX*20),TSMU(LMAX*20)
      INTEGER NMU(0:LMAX)
      INTEGER INDDEV(0:LMAX)
      DOUBLE PRECISION OUV(LMAX),LOND(LMAX),COEFA(LMAX),COEFB(LMAX)
     &  ,EFFPRI(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER CHOIXC,DEFOND
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION MAM,DAM,SAM
      DOUBLE PRECISION MAC,DAC,SAC
      DOUBLE PRECISION MAV,DAV,SAV,TFAV
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
     : ,DZMAX
     : ,DLAT(LMAX),SLAT(LMAX)
     :,M1
COU2D     :,mrest,drest,srest
C     :,M2,D2,S2
      INTEGER NREFA(LMAX),NIB(NBMAX),NUM


      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      LOGICAL XMODSEC(LMAX),MODSEC
COU2D      DOUBLE PRECISION SMBQS4(LMAX),SMBD4(LMAX),SMBET4(LMAX)

      DOUBLE PRECISION DETYRH,QSA,DEBSOL,SHIELDS,LARACT
      EXTERNAL DETYRH,QSA,DEBSOL,SHIELDS,LARACT

      COMMON/PHYS/LM,LL
      COMMON/METHODE/METHODE
      COMMON/DEPOT/DEPOT
      COMMON/CHOIXC/CHOIXC
C si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
C si tcpente, calcul de contrainte critique par CK*shields, sinon contrainte par fct shields
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/FROTMT/FR1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/XLACT/LACT
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/LIQND/SND,YND
      COMMON/QL/QLAT
      COMMON/QTSM/QSMU,TSMU
      COMMON/NTMU/NMU
      COMMON/DEVERI/INDDEV
      COMMON/PRISE/OUV,LOND,COEFA,COEFB,EFFPRI
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/MODIFSEC/XMODSEC,MODSEC
C      COMMON/CAPASOL/CAPASOL
      COMMON/DEFOND/DEFOND
      COMMON/COEFC/COEFC
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/DSLAT/DLAT,SLAT
      COMMON/SENSM/SENSI
      COMMON/XNCMOA/XNCMMAG,XNCMMAD
      COMMON/EPSS/EPSS
      COMMON/DDNARF/NREFA
         COMMON/NBIEF/NBB
         common/NIBIEF/nib
COU2D      COMMON/smbQS4/SMBQS4,SMBD4,SMBET4


c      Write(*,*)'entree canoge'
C Cas d'un calcul sans transport de sediment
C-----------------------------------------------------------------------
C inutile car deja avant appel
C      IF(.NOT.TRASED) RETURN

c      Write(*,*)'entree canoge'
C Calcul de la pseudo contrainte (sans RO.GRAV) au fond J.Rh (par Jfrot)
C-----------------------------------------------------------------------
C [Calcul de JRH � l'intermaille]
C FR1 (centremailles ou intermailles): Strickler hydraulique donn� K
C FRPEAU: Strickler de grain (du centre du lit!) calcul� K' = 21 / D^(1/6)
C KS1: coefficient de correction pour le calcul de la contrainte efficace KS1 = (K / K')^(3/2) et Toeff = KS1 * To
      DO IB=1,NBB
      DO I=LM(IB-1)+1,LM(IB)-1
c        FRHYD=FR1(I)+(FR1(I+1)-FR1(I))*(XTMAIL(I)-TMAIL(I))
c     &    /(TMAIL(I+1)-TMAIL(I))
c modif ap 3 juin 2005 car fr1 est applique a la fois a l'intermaille et au centre maille dans smbq2
        CHR=MAX(CHEZY**2,FR1(I)**2*RHINTER(I)**(1./3.))
        JRH(I)=(VINTER(I)**2)/CHR
C correction du 11 mars 2005
        IF(JRH(I).LT.EPSS) JRH(I)=0.
C        IF(JRH(I).LT.EPS) JRH(I)=EPS
C        IF(OPTS.EQ.4.OR.OPTS.EQ.6)THEN
        IF(TAUEFFICACE)THEN
c          JMIL=INT(0.5*(XNCMMAG(I)+XNCMMAD(I)))
C          FRPEAU=21.*(XDCSP1(JMIL,1))**(-0.1666666667)
          FRPEAU=21.*(DACTP1(I))**(-0.1666666667)
          KS1(I)=(FR1(I)/FRPEAU)**1.5
                IF(KS1(I).GT.1.)KS1(i)=1.
c        ELSE
c          KS1(I)=1.
        ENDIF
        LACT(I)=LARACT(I)
C fin des boucles sur I et Ib
      ENDDO
         ENDDO


C D�bit solide entrant dans le mod�le et pr�-initialisation des compartiments
C-----------------------------------------------------------------------
C D�bit solide � l'amont du mod�le: initialisation de Mav qui deviendra le prochain Mam
C si Hydros.etude contient un Qs<0, on calcule la capacit� solide � l'�quilibre
      DO num=1,NBB
         IB=NIB(NUM)
C on chnage pour que dans debsam biefs amont deja calcules
      CALL DEBSAM(0.5*(TN+TNP1),IB)
      DAV=DMOB(LM(IB-1)+1)
      SAV=SMOB(LM(IB-1)+1)
      IF(QSR(LM(IB-1)+1).LT.0.)THEN
        TFAV=SHIELDS(DAV,JRH(LM(IB-1)+1),RHINTER(LM(ib-1)+1))
c      IF(CAPASOL.EQ.1)THEN
        QSR(LM(IB-1)+1)=
     :     DEBSOL(LM(IB-1)+1,DAV,TFAV,SAV,LACT(LM(IB-1)+1))
c      ELSEIF(CAPASOL.EQ.2)THEN
c        QSR(LM(IB-1)+1)=DEBSOL2(LM(IB-1)+1,DAV,TFAV,SAV)
c      ELSEIF(CAPASOL.EQ.3)THEN
c        QSR(LM(IB-1)+1)=DEBSOL3(LM(IB-1)+1,DAV,TFAV,SAV)
c      ENDIF
C fin du if sur QSR<0
      ENDIF
      MAV=QSR(LM(IB-1)+1)*DTN
      IF(MAV.LT.EPSM)THEN
        If(mav.lt.-epsm)then
          write(*,*)'amont : probleme de calcul de la masse active'
        endif
        MAV=0.
      ENDIF
c      DMOB(LM(IB-1)+1)=DAV
c      SMOB(LM(IB-1)+1)=SAV

      DO I=LM(IB-1)+1,LM(IB)-1
C      DO I=1,LL
C Initialisation des compartiments s�dimentaires Mam et Ac
C-----------------------------------------------------------------------
C Le compartiment Ac repose sur l'intermaille I, i.e. o� est d�finie la g�om�trie. Il y a donc LL compartiments Ac.
        MAM=MAV
        DAM=DAV
        SAM=SAV
        MAC=MACTP1(I)
        DAC=DACTP1(I)
        SAC=SACTP1(I)
        IF(NREFA(I).EQ.-2.AND.NREFA(I+1).EQ.-2)THEN
c on suppose un ouvrage entre les deux
          MAV=MAM
          DAV=DAM
          SAV=SAM
          QSR(I+1)=MAV/DTN
          DMOB(I+1)=DAV
          SMOB(I+1)=SAV
        ELSE
C Apports solides lat�raux (>0 apport; <0 soutirage)
C-----------------------------------------------------------------------
C Le centremaille I correspond � l'amont de la maille s�dimentaire I
C QLAT d�bit lin�ique de soutirage (<0) par les prises d'irrigation
C SMBS et QSA semblent plut�t concerner les autres types d'ouvrages
        IF(QLAT(I).GT.EPS)THEN
c si le diametre est negatif ou nul, diametre identique � l'amont
          IF(DLAT(I).LT.EPS)THEN
            QSLAT(I)=QSA(I,0.5*(TNP1+TN),DAM,LACT(I),QLAT(I),SAM)
            QSLAT(I)=QSLAT(I)*(XTMAIL(I)-XTMAIL(I-1))
C unisol=2 apports solides en concentration : QSA renvoie toujours un debit solide par m
c          IF(UNISOL.EQ.2)THEN
c            QSLAT(I)=QSLAT(I)*QLAT(I)
c          ENDIF
            IF(MAM.GT.EPSM)then
              MAM=MAM+QSLAT(I)*DTN
               elseif (mam.lt.-EPSM)then
                 mam=mam-qslat(i)*dtn
            else
                 mam=qslat(i)*dtn
            endif
          ELSE
           QSLAT(I)=QSA(I,0.5*(TNP1+TN),DLAT(I),LACT(I),QLAT(I),SLAT(I))
            QSLAT(I)=QSLAT(I)*(XTMAIL(I)-XTMAIL(I-1))
C unisol=2 apports solides en concentration : QSA renvoie toujours un debit solide
c          IF(UNISOL.EQ.2)THEN
c            QSLAT(I)=QSLAT(I)*QLAT(I)
c          ENDIF
            IF(MAM.GT.EPSM)then
              CALL MIXAGE(MAM,DAM,SAM,QSLAT(I)*DTN,DLAT(I),SLAT(I))
                elseif (mam.lt.-EPSM)then
                 m1=-mam
              CALL MIXAGE(M1,DAM,SAM,QSLAT(I)*DTN,DLAT(I),SLAT(I))
                 mam=-m1
            else
                 mam=qslat(i)*dtn
            endif
          ENDIF
        ELSEIF(QLAT(I).LT.-EPS)THEN
C Attention, CONCENTRATION est calcul� avec le d�bit � Tn et non � Tn+1/2
C comme le voudrait la logique, car QND non calcul�
          IF(QN(I).GT.EPS)THEN
            CONCENTRATION=ABS(EFFPRI(I)*QSR(I)/QN(I))
          ELSE
            CONCENTRATION=0.
          ENDIF
          QSLAT(I)=QLAT(I)*(XTMAIL(I)-XTMAIL(I-1))*CONCENTRATION
          IF(MAM.GT.EPSM)then
            MAM=MAM+QSLAT(I)*DTN
               IF(MAM.LT.EPSM)MAM=0.
             elseif (mam.lt.-EPSM)then
               mam=mam-qslat(i)*dtn
            IF(MAM.GT.-EPSM)MAM=0.
C si mam est nul on ne fait rien car mam reste nul et rien n'est pr�lev�
C           else
C              mam=qslat(i)*dtn
          endif
C          MAM=MAM+QSLAT(I)*DTN
        ELSE
C ligne suivante inutile ?
          QSLAT(I)=0.
        ENDIF
COU2D        IF(MAM.GT.EPSM)THEN
COU2D           IF(SMBQS4(I).GT.EPSM)THEN
COU2D              Call mixage(mam,dam,sam,smbqs4(I)*DTN,SMBD4(I),SMBET4(I))
COU2D           elseif(SMBQS4(I).LT.-EPSM)THEN
COU2D               MREST=MAM+smbqs4(I)*DTN
COU2D               IF(Mrest.LT.EPSM)then
COU2D                 MAM=0.
COU2D               else
COU2D                 call separation(mam,dam,sam,
COU2D     :-smbqs4(I)*DTN,SMBD4(I),SMBET4(I),mrest,drest,srest)
COU2D                 mam=mrest
COU2D		 dam=drest
COU2D		 sam=srest
COU2D               endif
COU2D           endif
COU2D         ELSEIF(MAM.LT.-EPSM)THEN
COU2D           M1=-MAM
COU2D           IF(SMBQS4(I).LT.-EPSM)THEN
COU2D              Call mixage(m1,dam,sam,-smbqs4(I)*DTN,SMBD4(I),SMBET4(I))
COU2D              MAM=-M1
COU2D           elseif(SMBQS4(I).GT.EPSM)THEN
COU2D               MAM=MAM+smbqs4(I)*DTN
COU2D               IF(MAM.GT.EPSM)THEN
COU2DC on suppose MAM petit devant smbqs4
COU2D                  DAM=SMBD4(I)
COU2D                  SAM=SMBET4(I)
COU2D               ELSE
COU2D                  MAM=0.
COU2D               ENDIF
COU2D           endif
COU2D         ELSE
COU2DC  cas de MAM=0
COU2D           MAM=SMBQS4(I)*DTN
COU2D           DAM=SMBD4(I)
COU2D           SAM=SMBET4(I)
COU2DC fin du if sur signe de mam
COU2D          ENDIF


C Bilan sur les compartiments Mam, Ac et Mav d'une maille s�dimentaire
C-----------------------------------------------------------------------
C Pour l'instant: Mam centremaille amont; Ac intermaille I; Mav centremaille aval (I+1)
        CALL CACOSE(I,DXMAIL(I),DTN,LACT(I)
     &    ,MAM,DAM,SAM,CAPSOL(I),DCHAR(I),DDCHAR(I),MACIND,MAV,DAV,SAV
     &    ,MAC,DAC,SAC)


c KAMAL:
c certes on fait le bilan sur toutes les intermailles mais on sait que pour les intermailles
c ou l'eau est absente, le bilan s�idmentaire ne donnerait pas de modification de g�om�trie
c donc c'est pas la peine de rentrer dans erosec ou depsec
c KAMAL: mars 2004: d�formation dans une intermaille I si la hauteur d'eau dans
c                   l'intermaille n'est pas nulle et si la hauteur d'eau dans
c                   le centremaill aval n'est pas nulle aussi
c j' n'utilise ici que les variables hydrauliques au temps tn+1/2: YINTER(IM) et YND(CM)
c YND(I+1) est la hauteur d'eau dans le CM a l'aval de l'IM I

c      IF(I.LT.LL)THEN ! ajouter uniquement pour impact: pas de d�formation de la derni�re section
c      IF(YINTER(I).GT.EPSY.AND.YND(I+1).GT.EPSY)THEN
C Gestion des compartiments de base Ba avec d�formation de la g�om�trie
C-----------------------------------------------------------------------
C A un instant donn� et pour une section donn�e, soit on �rode, soit on d�pose (soit on ne fait rien)...
C La marge de tol�rance de MAC autour de MACIND est r�gl�e par SENSI
C XMODSEC commande le recalcul des sections en largeur-cote aux intermailles
c Kamal: mars 2004: pas de d�formation dans la derni�re section (aval)
c        IF(I.LT.LL)THEN
C calcule a partir de TYPDEF lu dans ts.etude
C        SENSI=0.01
c        IF(ABS(MAC).LT.(1.-SENSI)*MACIND)THEN
        IF(MAC.LT.(1.-SENSI)*MACIND)THEN
C      Write(*,*)'entree pour deformation'
C D�stockage �ventuel d'une partie de Ba pour renflouer Ac
C---------------------------------------------------------
C MBADEM masse du compartiment Ba demand�e
C MBAOBT masse du compartiment Ba obtenue (=disponible)
C DBAOBT,SBAOBT caract�ristiques des s�diments obtenus
c          MBADEM=MACIND-ABS(MAC)
          MBADEM=MACIND-MAC
c          IF((MBADEM.LT.0.).OR.(MACIND.LT.0.))THEN
          IF((MBADEM.LE.0.).OR.(MACIND.LT.0.).OR.(MAC.LT.0.))THEN
            WRITE(*,*)'PROBLEME Canoge erosion MBADEM=',MBADEM,'MACIND='
     &        ,MACIND,' MAC=',MAC
            STOP
          ENDIF
c      IF(DEFOND.EQ.0)RETURN
C on limite � 1/10 de la hauteur d'eau
C          DZMAX=MIn(0.1*YINTER(I),DAC)
           DZMAX=MIN(0.1*ABS(VINTER(I))*DTN,DAC)
C           DZMAX=ABS(VINTER(I))*DTN
C on limite l'erosion a n fois l'erosion moyenne
C avec n=2
          IF(LACT(I).GT.EPS)THEN
            DZMAX=MIN(DZMAX,2.*MBADEM*POR1/(ROS*DXMAIL(I)*LACT(I)))
          ENDIF
c           IF(MAC.GT.0.)THEN
C test sur max mis dans erosec
             CALL EROSEC(I,MBADEM,MAC,DAC,SAC,DZMAX)
C reporte dans erosec
C          CALL MIXAGE(MAC,DAC,SAC,MBAOBT,DBAOBT,SBAOBT)
          XMODSEC(I)=.TRUE.

        ELSEIF(MAC.GT.(1.+SENSI)*MACIND)THEN
c        ELSEIF(ABS(MAC).GT.(1.+SENSI)*MACIND)THEN
C Stockage d'une partie de Ac dans les compartiments de base Ba
C--------------------------------------------------------------
C MABSTO masse du compartiment Ba � stocker dans la g�om�trie
c          DZMAX=ABS(VINTER(I))*DTN
C on limite � 1/10 de la hauteur d'eau
C          DZMAX=0.1*YINTER(I)
C           DZMAX=MIN(0.1*ABS(VINTER(I))*DTN,DAC)
C depot limite par une vitesse de chute maximale arbitraire de 1 mm/s
           DZMAX=0.001*DTN
          MBASTO=MAC-MACIND
c          MBASTO=ABS(MAC)-MACIND
C on suppose que tout sera depose
c          If(mac.gt.0.)THEN
             mac=macind
c          elseif(mac.lt.0.)then
c             mac=-macind
C cas ou mac et macind sont nuls
c          else
c             mbasto=0.
c          endif
C on limite le depot a n fois le depot moyen
C avec n=2
          IF(LACT(I).GT.EPS)THEN
            DZMAX=MIN(DZMAX,2.*MBASTO*POR1/(ROS*DXMAIL(I)*LACT(I)))
          ENDIF
C on ne doit reactualiser Mac que si tout a ete depose
C reporte en fin
C          MAC=MACIND
C          IF((MBASTO.LT.0.).OR.(MACIND.LT.0.))THEN
          IF((MBASTO.LE.0.).OR.(MAC.LT.0.).OR.(MACIND.LT.0.))THEN
            WRITE(*,*)'Probleme Canoge depot MBASTO=',MBASTO,'MACIND='
     &        ,MACIND,' MAC=',MAC
            STOP
          ENDIF
C      IF(DEFOND.EQ.0)RETURN
c        if(JRH(I).gT.epss)THEn
        IF(DEPOT.EQ.1) THEN
            CALL DEPSEC(I,MBASTO,DAC,SAC,DZMAX)
        ELSEIF(DEPOT.EQ.2) THEN
            CALL DEPSEC2(I,MBASTO,DAC,SAC,DZMAX)
        ELSEIF(DEPOT.EQ.3) THEN
            CALL DEPSEC3(I,MBASTO,DAC,SAC,DZMAX)
          ELSEIF(DEPOT.EQ.4) THEN
            CALL DEPSEC4(I,MBASTO,DAC,SAC,DZMAX)
        ELSEIF(DEPOT.EQ.5) THEN
            CALL DEPSEC5(I,MBASTO,DAC,SAC,DZMAX)
        ELSEIF(DEPOT.EQ.6) THEN
            CALL DEPSEC6(I,MBASTO,DAC,SAC,DZMAX)
        ELSEIF(DEPOT.EQ.7) THEN
            CALL DEPSEC7(I,MBASTO,DAC,SAC,DZMAX)
        ENDIF
C       write(*,*)'depots',i,mac,macind,dac,mbasto,dzmax
c        ENDIF
C Mbasto contient la masse restante
C modif : la masse restante est ajoutee a la masse aval
          IF(MAC.GE.EPSM)THEN
            MAC=MAC+MBASTO
            IF (MAC.LT.EPSM)MAC=0.
C          ELSEIF(MAC.lt.0.)then
c          ELSEIF(MAC.LT.-EPSM)THEN
c            MAC=MAC-MBASTO
c            IF (MAC.GT.0.)MAC=0.
          ELSE
            MAC=MBASTO
            IF (MAC.LT.EPSM)MAC=0.
          ENDIF
c          IF(MBASTO.LT.0.)THEN
cc            IF(MBASTO.LT.-EPSM)THEN
cc            write(*,*)'trop de sediments deposes'
cc            write(*,*)'MBASTO',mbasto
cc            ENDIF
c            IF(MAv.GT.EPSM)THEN
cC              CALL MIXAGE(MAV,DAV,SAV,MBASTO,DAC,SAC)
c                MAV=MAX(0.D0,MAV-MBASTO)
c          ELSEIF(MAV.lt.-EPSM)then
cc            M1=-MAV
cc            CALL MIXAGE(M1,DAV,SAV,MBASTO,DAC,SAC)
cc            MAV=-M1
cc             mav=min(0.,mav-mbasto)
c             mav=mav+mbasto
c          ELSE
cC MAV valait zero : on la laisse a zero malgre erreur sur masse
c               MAV=0.
c          ENDIF
C si MBASTO positif ce qui doit etre le cas
c          ELSEIF(MBASTO.GT.EPSM)THEN
c            IF(MAv.GT.EPSM)THEN
c              CALL MIXAGE(MAV,DAV,SAV,MBASTO,DAC,SAC)
c            ELSEIF(MAV.lt.-EPSM)then
cc              M1=-MAV
cc              CALL MIXAGE(M1,DAV,SAV,MBASTO,DAC,SAC)
cc              MAV=-M1
c              M1=-MAV
c          CALL DEMIXAGE(M1,DAV,SAV,MBASTO,DAC,SAC,M2,D2,S2,DXMAIL(I))
c              MAV=-M2
c              DAV=D2
c              SAV=S2
c cas ou mav=0c
c            ELSE
C on devrait peut etre donner le signe de la vitesse a MAV
c              MAV=MBASTO
c              DAV=DAC
c              SAV=SAC
c            ENDIF
C cas ou mbasto nul donc on garde mav
C          ELSE
C fin du if sur mbasto positif
c          ENDIF
          XMODSEC(I)=.TRUE.
C pas de defomration de la section
        ELSE
          XMODSEC(I)=.FALSE.
C fin du if sur comparaison mac et macind
        ENDIF

C Pr�paration des it�rations suivantes
C-----------------------------------------------------------------------
        QSR(I+1)=MAV/DTN
        DMOB(I+1)=DAV
        SMOB(I+1)=SAV
        MACTP1(I)=MAC
        DACTP1(I)=DAC
        SACTP1(I)=SAC
C fin du if sur nrefa
       ENDIF
C fin boucles sur I et IB
          ENDDO
          ENDDO
c ajout par kamal mars 2004
C apr�s d�formation, on r�actualise la g�om�trie largeur cote
C mais attention les couches de sediments ont deja ete reactualisees
c       write(*,*)'traclc'
      IF(DEFOND.EQ.0)RETURN
      CALL TRACLC
      CALL INCMLC
c       write(*,*)'fin incmlc'
c kamal: quand on sort de ce sous programme, on sort avec une g�om�trie d�ja r�actualis�
c les variables hydrauliques ne sont pas encore r�actualis�s
c la r�actualisation des variables hydrauliques (YNP1) se trouve apr�s canoge

      RETURN
      END



C-----------------------------------------------------------------------
      SUBROUTINE CACOSE(I,DX,DTN,LACT,MAM,DAM,SAM
     &  ,CAPSOL,DCHAR,DDCHAR,MACIND,MAV,DAV,SAV,MAC,DAC,SAC)
C-----------------------------------------------------------------------
C Bilan sur les compartiments d'une maille s�dimentaire
C Entr�es: JRH,KS1,DX,DTN,LACT,VITLIQ,MAM,DAM,SAM
C Sorties: CAPSOL,DCHAR,DDCHAR,MACIND,MAV,DAV,SAV
C Modifs:  MAC,DAC,SAC
C-----------------------------------------------------------------------
C Toutes les grandeurs sont prises au centre de la maille s�dimentaire consid�r�e
C JRH: pseudo contrainte [m]  KS1: correction contrainte efficace []
C DX: longeur maille s�d.[m]  DTN: pas de temps [s]
C LACT: largeur active [m]    VITLIQ: vitesse eau [m/s]   CAPSOL: capacit� de TS [kg/s]
C DCHAR: dist chargement [m]  DDCHAR: dist d�chargement [m]
C MACIND: masse solide indicative de l'intensit� d'interaction lit-�coulement (sert � r�gler la masse du compartiment Ac) [kg]
C MAM,DAM,SAM,MAC,DAC,SAC,MAV,DAV,SAV: masse [kg], diam. [m], �tendue des compart. s�d. [0] Mam (amont), Ac (actif), Mav (aval)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,I,LL,NBMAX,NBB,IB
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
      INTEGER CHOIXC,CAPASOL,LM(0:NBMAX)
      DOUBLE PRECISION DX,DTN,LACT,MACIND
      DOUBLE PRECISION MAM,DAM,SAM,MAC,DAC,SAC,MAV,DAV,SAV
      DOUBLE PRECISION CONTRAINTE,TFAM,TFAC,TMAC
      DOUBLE PRECISION MAC1,DAC1,SAC1,MAV1,DAV1,SAV1,MAC2,DAC2,SAC2
      DOUBLE PRECISION QSAM,CAPSOL,DCHAR,DDCHAR
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION VNP1(LMAX),YNP1(LMAX),RHNP1(LMAX)
      DOUBLE PRECISION SND(LMAX),YND(LMAX)
     :,M1,M2,D1,S1
     :,JRH(LMAX),KS1(LMAX)
      DOUBLE PRECISION OPTMAC

         LOGICAL DERNIER

      DOUBLE PRECISION DICHAR,LCHARG,SHIELDS,TOCMM
     &,DEBSOL,COEFC
      EXTERNAL DICHAR,LCHARG,SHIELDS,TOCMM,DEBSOL

      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/LIQND/SND,YND
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PHYS/LM,LL
      COMMON/CAPASOL/CAPASOL
      COMMON/COEFC/COEFC
      COMMON/SOLIDE/JRH,KS1
      COMMON/NBIEF/NBB
      COMMON/OPTMAC/optMAC

C dernier sert  a eviter les intermailles sans aval
         DERNIER=.FALSE.
         DO IB=1,NBB
          IF(.NOT.DERNIER)THEN
           IF(I.EQ.LM(IB)-1)THEN
                DERNIER=.TRUE.
           ENDIF
          ENDIF
      ENDDO

C Pr�liminaire: grandeurs communes
C-----------------------------------------------------------------------
      CONTRAINTE=ROGRAV*JRH(I)
      IF(CONTRAINTE.LT.0.) CONTRAINTE=0.

C 1-Transit des s�diments de Mam vers Mav
C decroissance exponentielle
C-----------------------------------------------------------------------
      QSAM=MAM/DTN
      CAPSOL=0.
      DDCHAR=DICHAR(I,DAM)
      TFAM=SHIELDS(DAM,JRH(I),RHINTER(I))
      IF(CONTRAINTE.LT.TFAM) DDCHAR=DDCHAR*(CONTRAINTE/TFAM)
         IF(QSAM.GT.0.)THEN
           MAV=LCHARG(QSAM,CAPSOL,DDCHAR,DX)*DTN
           IF(MAV.LT.EPSM)MAV=0.
           IF(MAV.GT.MAM)THEN
              MAV=MAM
           ENDIF
C ajout par kamal mars pour projet impact
c pas de s�diment qui passe de la maille I a la maille I+1 si y a pas d'eau dans cette derni�re
c ici, on fait le bilan pour une maille I represent� par l'intermaille I
c YND (I+1) est la hauteur d'eau au centremaille I+1 au temps tn+1/2, qui est l'interface aval de la maille s�dimentaire I
c YINTER(I+1) est la hauteur d'eau a l'intermaille suivante I+1
           IF(.NOT.DERNIER)THEN
C           IF(I.LT.LL)THEN
C je ne  traite pas la derni�re intermaille LL car YINTER(LL+1) n'existe pas
             IF(YND(I+1).LE.EPSY.OR.YINTER(I+1).LE.EPSY)THEN
C pour intermaille I donnee, je regarde si y a de l'eau dans le centraimlle aval (I+1) et dans l'interamille suivante I+1
               MAV=0
             ENDIF
C else pour dernier
           ELSE
             IF(YINTER(I).LE.EPSY)THEN
C pour intermaille I donnee, je regarde si y a de l'eau dans le centraimlle aval (I+1) et dans l'interamille suivante I+1
               MAV=0
             ENDIF
           ENDIF
         ELSEIF(QSAM.EQ.0.)THEN
           MAV=0.
         ELSE
C calcul faux mais impose par le fait qu'on calcule de la maille 1  a la maille lm
C meme si le courant est inverse
           MAV=LCHARG(QSAM,CAPSOL,DDCHAR,DX)*DTN
           IF(MAV.GT.-EPSM)MAV=0.
         ENDIF
c      ENDIF
c      ELSEIF(I.EQ.LL)THEN ! ici je traite le cas de la derni�re intermaille LL
c      IF(YINTER(I).LE.EPSY)THEN
c      MAV=0
c      ELSE
c      MAV=LCHARG(QSAM,CAPSOL,DDCHAR,DX)*DTN
c      ENDIF
c      ENDIF

C 2-D�p�t de Mam vers Ac
C-----------------------------------------------------------------------
C 3-Mixage interm�diaire de Ac
C-----------------------------------------------------------------------
          MAC1=ABS(MAM-MAV)
C on considere deux cas soit mam positif soit mam negatif
C par construction la valeur absolue de mav est inferieure a mam
        IF(MAM.GT.MAV)THEN
C cas ou MAM positif et MAV positif ou nul
C          write(*,*)'demixage1'
          CALL DEMIXAGE(MAM,DAM,SAM,MAV,DAV,SAV,MAC1,DAC1,SAC1,DX)
          CALL MIXAGE(MAC,DAC,SAC,MAC1,DAC1,SAC1)
C possible uniquement si Mam negatif
C on met les masses negatives si le courant est en sens inverse
        ELSEIF(MAV.GT.MAM)THEN
cC cas ou MAM negatif et MAV negatif ou nul
          M1=-MAM
          M2=-MAV
          CALL DEMIXAGE(M1,DAM,SAM,M2,DAV,SAV,MAC1,DAC1,SAC1,DX)
          IF(MAC.GT.MAC1)THEN
            M1=MAC-MAC1
            CALL DEMIXAGE(MAC,DAC,SAC,MAC1,DAC1,SAC1,M1,D1,S1,DX)
            MAC=M1
            DAC=D1
            SAC=S1
          ELSE
            MAV=MAV+MAC1-MAC
            MAC=0.
          ENDIF
C egalite de mam et mav
        ELSE
c Mac inchange
c          MAC1=0.
c          DAC1=DAM
c          SAC1=SAM
          DAV=DAM
          SAV=SAM
        ENDIF
      TFAC=SHIELDS(DAC,JRH(I),RHINTER(I))
      TMAC=TOCMM(DAC,SAC,JRH(I),RHINTER(I))

C 4-Erosion de Ac vers Mav
C-----------------------------------------------------------------------
      QSAM=0.
C Capsol a le signe de vinter donc mav1 a le signe de vinter
c      IF(CAPASOL.EQ.1)THEN
        CAPSOL=DEBSOL(I,DAC,TFAC,SAC,LACT)
c      ELSEIF(CAPASOL.EQ.2)THEN
c        CAPSOL=DEBSOL2(I,DAC,TFAC,SAC)
c      ELSEIF(CAPASOL.EQ.3)THEN
c        CAPSOL=DEBSOL3(I,DAC,TFAC,SAC)
c      ENDIF
      DCHAR=DICHAR(I,DAC)
      IF(TMAC.GT.TFAC) DCHAR=DCHAR*(TMAC/TFAC)
c      IF(I.LT.LL)THEN ! je traite pas la derni�re intermaille LL car Y(LL+1) n'existe pas
c      IF(YND(I+1).LE.EPSY.OR.YINTER(I+1).LE.EPSY)THEN ! pour intermaille I donnee, je regarde si y a deux dans le centraimlle aval (I+1) et dans l'interamille suivante I+1
c      MAV1=0
c      ELSE
      MAV1=LCHARG(QSAM,CAPSOL,DCHAR,DX)*DTN
c      ENDIF
c      ELSEIF(I.EQ.LL)THEN
c      IF(YINTER(LL).LE.EPSY)THEN ! pour intermaille I donnee, je regarde si y a deux dans le centraimlle aval (I+1) et dans l'interamille suivante I+1
c      MAV1=0
c      ELSE
c      MAV1=LCHARG(QSAM,CAPSOL,DCHAR,DX)*DTN
c      ENDIF
c      ENDIF

C 5-Bilan s�dimentaire de Ac
C-----------------------------------------------------------------------
      IF(MAV1.GT.EPSM)THEN
        MAC2=MAC-MAV1
        IF(MAC2.GT.EPSM)THEN
          CALL DEMIXAGE(MAC,DAC,SAC,MAV1,DAV1,SAV1,MAC2,DAC2,SAC2,DX)
          MAC=MAC2
          DAC=DAC2
          SAC=SAC2
        ELSE
          MAV1=MAC
          DAV1=DAC
          SAV1=SAC
          MAC=0.
        ENDIF
      ELSEIF(MAV1.LT.-EPSM)THEN
c hypothese fausse
          DAV1=DAC
          SAV1=SAC
          MAC=MAC-MAV1
C le cas MAV1 nul n'a pas besoin d'etre traite car traite juste apres
      ELSE
         MAV1=0.
      ENDIF


C 6-Mixage des s�diments de Mav
C-----------------------------------------------------------------------
      IF(MAV*MAV1.GT.0.)THEN
        IF(MAV.GT.0.)THEN
          CALL MIXAGE(MAV,DAV,SAV,MAV1,DAV1,SAV1)
        ELSE
          M1=-MAV
          M2=-MAV1
          CALL MIXAGE(M1,DAV,SAV,M2,DAV1,SAV1)
          MAV=-M1
        ENDIF
      ELSEIF(MAV*MAV1.LT.0.)THEN
        MAV=MAV+MAV1
        IF(ABS(MAV1).GT.ABS(MAV))THEN
          DAV=DAV1
          SAV=SAV1
        ENDIF
      ELSE
C une des masses est nulle
        IF(MAV1.NE.0.)THEN
          MAV=MAV1
          DAV=DAV1
          SAV=SAV1
        ENDIF
c sinon mav1 est nul donc mav inchange
      ENDIF


C Fin: �valuation de l'interaction lit-�coulement
C-----------------------------------------------------------------------
      IF(ABS(VINTER(I)).GT.EPSY)THEN
           if(OPTMAC.GT.EPS)THEn
            if (tmac.gt.eps)then
C epaisseur de optmac fois le D84 au maximum
          MACIND=ROS*LACT*DAC*SAC*OPTMAC*DX*min(contrainte/tmac,1.D0)
                 else
          MACIND=ROS*LACT*DAC*SAC*OPTMAC*DX
                 endif
       ELSE
        MACIND=CAPSOL*DX/VINTER(I)
       ENDIF
      ELSE
C si vitesse faible la capacite solide faible mise a zero
        MACIND=0.
C        MACIND=CAPSOL*DTN
      ENDIF
      IF(MACIND.LT.0.)THEN
        WRITE(*,*)'PROBLEME Cacose MACIND=',MACIND,' CAPSOL=',CAPSOL
     &    ,' VITLIQ=',VINTER(I)
      ENDIF
C      MACIND=CAPSOL*DX/VITLIQ               ruts9j
C      MACIND=CAPSOL*DTN                     ruts9j (abandonn�)
C      MACIND=MAX(QSDEP,QSERO)*DX/VITLIQ     ruts9h
C      MACIND=MAX(QSDEP,QSERO)*DTN           ruts9g
C      MACIND=ABS(MAV-MAM)                   ruts9g (abandonn�)

      RETURN
      END



C-----------------------------------------------------------------------
      SUBROUTINE EROSEC(I,MBADEM,MAC,DAC,SAC,DZMAX)
C VERSION DE EROSEC AVEC EROSION DE TOUS LES POINTS ERODABLES
C (PROPORTIONNELLE A L'ECART ENTRE CONTRAINTE ET CONTRAINTE CRITIQUE)
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C si la contrainte critique n'ets pas fix�e par l'utilisateur, elle est calcul�e pour la couche de base soit par Shields soit par CK*Shields.
C l'�rosion a lieu dans les le lit actif uniquement.
C-----------------------------------------------------------------------
C Tentative d'�rosion du lit de la section, i.e. d�stockage de Ba.
C Entr�e: I,JRH,RH,MBADEM,DZMAX             Sortie: MAC,DAC,SAC
C Modifie: g�om�trie abscisse-cote � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C Rh, rayon hydraulique
C JRH pseudo-contrainte (J.Rh)
C MBADEM masse du compartiment Ba demand�e (tentative de destockage) >0
C MBAOBT masse du compartiment Ba obtenue (=disponible � ce niveau de contrainte) >0
C DBAOBT,SBAOBT caract�ristiques des s�diments �rod�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
      LOGICAL ENCORE
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J,JMIL
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &      ,PSOUE(LNCMAX)
      INTEGER CHOIXC
         logical TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION MBADEM,MBAOBT,DBAOBT,SBAOBT,ZABS,TMBA ! contrainte de mise en mouvement
     &  ,DZ,DM,DMCUM,CONTRAINTE,SPARTICIP,PARTICIP(NCMAX),RH,ROGRAVJ
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION COEFC,EPSS,TN,DTN,TNP1
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TO(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &,TOMPC(LNCMAX),MAC,DAC,SAC,DZMAX
     :,JRH(LMAX),KS1(LMAX),CDISPEROS,puierosion
      LOGICAL BERGE
          integer methoderosion

      DOUBLE PRECISION TOCMM
      EXTERNAL TOCMM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC !si contrainte MPC est sup�rieur � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
c      COMMON/PUI/PUI
      COMMON/EPSS/EPSS
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/EBERGE/BERGE
      COMMON/SOLIDE/JRH,KS1
C cdisperos est le multiplicateur du D16 pour epaisseur couche superieure
C infiltree dans couche de dessous lors d'une erosion
      common/cdisperos/cdisperos
      COMMON/EROSION/methoderosion
          COMMON/perosion/puierosion

      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      JMIL=INT(0.5*(XNCMMAG(I)+XNCMMAD(I)))
C On initialise les caract�ristiques des s�diments obtenus
C aux valeurs des s�diments du milieu de la section.
C Purement conventionnel...
      MBAOBT=0.
      DBAOBT=XDCSP1(JMIL,1)
      SBAOBT=XSCSP1(JMIL,1)
      IF(MBADEM.LE.EPSM) RETURN
      CONTRAINTE=ROGRAV*JRH(I)
C encore est mis a false des qu'on considere que
C meme si mbadem n'est pas obtenu, il ne faut pas plus eroder
C do while a supprimer si on veut eviter toute erosion excessive
      DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau, on suppose que l'�rosion s'effecutera
C sur des points situ�s sous la surface de l'eau, � l'int�rieur du lit actif (limites exclues).
C le[s] points situ�s sous l'eau sont mis dans DEFORM(JDEF)
c      IF(CHOIXC.NE.2) THEN
C        J=XNCMMAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section : debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
C        DO WHILE(J.LT.XNCMMAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau, on traite toute la section lit majeur+litmineur
         IF(J.GT.XNCMMAG(I).AND.J.LT.XNCMMAD(I))THEN
          JDEF=JDEF+1
          DEFORM(JDEF)=J
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
c      ELSEIF(CHOIXC.EQ.2)THEN
c         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
c         JSOUE=0
c        DO WHILE(J.LT.XNCMMAD(I))
c          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
c          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
c          PSOUE(JSOUE)=J
c          JDEF=JSOUE
c          DEFORM(JDEF)=PSOUE(JSOUE) ! variable introduite uniquement pour coh�rence avec la MPC
c 110      CONTINUE
c          J=J+1
c        ENDDO
c        JSOUEMAX=JSOUE
c        JDEFMAX=JSOUEMAX
c      ENDIF
c      Write(*,*)'Nbpoints sous eau(',I,')=',JSOUEMAX
c      pause
C      DO JDEF=1,JDEFMAX
C     J=DEFORM(JDEF)
C      Write(*,*)'DEFORM(',JDEF,')=',J
C      pause
C      ENDDO


      IF(JSOUEMAX.gt.0)THEN
C rajout du glissement de berges
c on commence par calculer l'erosion des berges
c puis ensuite on fera celle du fond si necessaire
C il y aura erosion de la berge completementjusqu'� l'angle de stabilite
C le surplus eventuel passe dans la couche active pour un depot ulterieur
C modification du 21 aout 2008 on limite erosion berges a mbadem
C donc jamais de surplus

        If(berge)then
          call erosionberge(I,mbaobt,dbaobt,sbaobt,mbadem)
          mbadem=mbadem-mbaobt
          if(mbadem.lt.epsm)then
            encore=.false.
C            return
          endif
        endif

C
C apres glissement on teste de nouveau si il faut continuer erosion
        IF(ENCORE)THEN
      IF(CHOIXC.NE.2) THEN

c calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
      ROGRAVJ=CONTRAINTE/RH
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONThaut(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
        DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
           TO(J)=TOMPC(J)
         ELSE
           TO(J)=COEFC*CONTRAINTE
         ENDIF
       ENDDO

      ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
        ENDDO
      ENDIF
      IF(TCpente) THEN   ! calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*TOCMM(XDCSP1(J,1),XSCSP1(J,1),JRH(I),RH)
        ENDDO
C si tcpente faux : pas de correction avec pente
              ELSE
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          TC(J)=TOCMM(XDCSP1(J,1),XSCSP1(J,1),JRH(I),RH)
       ENDDO
      ENDIF

C De plus, calcul de la r�partition de l'�rosion, selon r�sultats de Saeed Khodashenas,
C i.e. formule inspir�e de M-P&M, proportionnelle � l'�cart entre contrainte et contrainte critique
C si un point n'ets pas �rodable, PARTICP= 0, donc pas de d�formation a ce point la
      SPARTICIP=0.
C intorduction de methodeerosion pour choisir entre tau  et tau-taucr
C la puissance est maintenant PUIerosion
          If(methoderosion.eq.1)then
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         TMBA=XTMCSP1(J,1)
         IF(TMBA.GT.999.)THEN
            PARTICIP(JDEF)=0.            ! Cas o� on est � la g�om�trie dure
         ELSE
           IF(TMBA.EQ.0.) TMBA=TC(J)
           IF(TO(J).LE.TMBA)THEN
             PARTICIP(JDEF)=0.                          ! Cas o� contrainte insuffisante
           ELSE
             PARTICIP(JDEF)=XDYA(J)*(TO(J)-TMBA)**PUIEROSION
             SPARTICIP=SPARTICIP+PARTICIP(JDEF)
           ENDIF
         ENDIF
      ENDDO
      ELSE
C          If(methoderosion.eq.2)then
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         TMBA=XTMCSP1(J,1)
         IF(TMBA.GT.999.)THEN
            PARTICIP(JDEF)=0.            ! Cas o� on est � la g�om�trie dure
         ELSE
             PARTICIP(JDEF)=XDYA(J)*TO(J)**PUIEROSION
             SPARTICIP=SPARTICIP+PARTICIP(JDEF)
         ENDIF
      ENDDO
C fin du if sur methoderosion
      ENDIF
C si contrainte trop faible on erode a epaisseur constante
      IF(SPARTICIP.LE.EPSS) THEN
C                  IF (MBADEM.GT.EPSM)THEN
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         TMBA=XTMCSP1(J,1)
         IF(TMBA.GT.999.)THEN
            PARTICIP(JDEF)=0.            ! Cas o� on est � la g�om�trie dure
         ELSE
             PARTICIP(JDEF)=XDYA(J)
             SPARTICIP=SPARTICIP+PARTICIP(JDEF)
           ENDIF
C        ENDIF
      ENDDO
C fin du if sur mbadem =0
C          ENDIF
C fin du if sur sparticip =0
          ENDIF
C application de sparticip
      IF(SPARTICIP.GT.EPSS) THEN
C on ne fairien si sparticip nulle
C Tentative d'application de la variation de cote
        DMCUM=0.
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
C xdya=0implique particip=0
           IF(PARTICIP(JDEF).LT.EPS)THEN
C           IF(PARTICIP(JDEF).EQ.0.)THEN
             DM=0.
             DZ=0.
           ELSE
          DM=MBADEM*PARTICIP(JDEF)/SPARTICIP
        IF(DM.GT.0.)THEN
          DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
c           IF(I.GE.398.AND.I.LE.402)THEN
c              IF(DZ.GE.0.03) THEN
c               DZ=0.03
c               ENCORE=.FALSE.
c               DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c             ENDIF
c         ELSE
          IF(DZ.GT.DZMAX)THEN
c            write(*,*)'forte erosion',' section ',I,' temps ',TN
c             write(*,*)'dz',DZ
c             write(*,*)'dzmax',DZMAX
c       pause
             DZ=DZMAX
             ENCORE=.FALSE.
             DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c          ENDIF
C fin du if sur dz>dzmax
       ENDIF
              IF(MBAOBT.LT.0.)THEN
                  write(*,*)'erosecmbaobt',mbaobt
                 mbaobt=0.
c                 pause
              endif
          IF(XNBCSP1(J).GT.1)THEN
C si couche assez epaisse pour etre erodee en partie
            IF((XZCSP1(J,1)-DZ).GT.XZCSP1(J,2))THEN
              CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM,XDCSP1(J,1)
     :,XSCSP1(J,1))
              XZCSP1(J,1)=XZCSP1(J,1)-DZ
              XMCSP1(J,1)=XMCSP1(J,1)-DM
c              IF(xmcsp1(j,1).lt.0.)then
c                write(*,*)'j',i,j,xmcsp1(j,1),dm,dz,xzcsp1(j,1)
c     :,xzcsp1(j,2)
c              pause
c              endif
C si couche peu epaisse <d16 et infiltrable d84<d16sous jacent
C on infiltre =on supprime couche et on melange a sous jacent
            IF((XZCSP1(J,1)-XZCSP1(J,2).LT.
     :CDISPEROS*XDCSP1(J,1)/XSCSP1(J,1)).AND.
C modif du 06/11/12 d50<d16 au lieu de d84<d16
c     :(XDCSP1(J,1)*XSCSP1(J,1).LT.XDCSP1(J,2)/XSCSP1(J,2)))THEN
     :(XDCSP1(J,1).LT.XDCSP1(J,2)/XSCSP1(J,2)))THEN
              XZCSP1(J,2)=XZCSP1(J,1)
C contrainte sera calculee a partir nouveau diametre
                          XTMCSP1(j,2)=0.
              CALL MIXAGE(XMCSP1(J,2),XDCSP1(J,2),XSCSP1(J,2),
     :XMCSP1(J,1),XDCSP1(J,1),XSCSP1(J,1))
              CALL SUPPBA1(J)
C fin du if sur infiltration
            ENDIF
C si couche erodee en totalite
          ELSE
              DM=XMCSP1(J,1)
              IF(DM.LT.0.)THEN
C cela ne doit jamais arriver!
                  write(*,*)'erosecdm',dm
c                  pause
                  dm=0.
              endif
            CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM,XDCSP1(J,1),XSCSP1(J,1))
              CALL SUPPBA1(J)
C fin du if sur compartiment totalement ou partiellementerodable
            ENDIF
C cas ou un seul compartiment
          ELSE
C      Write(*,*)'Erosec I=',I,' avant Mixage'
            CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM,XDCSP1(J,1),XSCSP1(J,1))
            XZCSP1(J,1)=XZCSP1(J,1)-DZ
C on devrait faire mais comme la masse est tres grande on la laisse identique
C              XMCSP1(J,1)=XMCSP1(J,1)-DM
C fin du if sur un seul compartiment
          ENDIF
          DMCUM=DMCUM+DM
C fin du if sur DM=0
      ENDIF
C fin du if sur particip=0
      ENDIF
      XZCOUP1(J)=XZCSP1(J,1)
C fin boucle sur jdef
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
        MBADEM=MBADEM-DMCUM
                IF(MBADEM.LT.EPSM)ENCORE=.FALSE.
C si sparticip nul
      ELSE
        ENCORE=.FALSE.
C fin du if sur Spartcip nul
      ENDIF
C fin du if sur encore apres glissement berges
      ENDIF
C 200    CONTINUE
C fin du if sur Jsouemax egal a 0
        ENDIF
        IF(JSOUEMAX.EQ.0) ENCORE=.FALSE.
C Cas o� plus aucun point �rodable
        IF(MBADEM.LE.EPSM) ENCORE=.FALSE.
C Cas o� <presque> tout a pu �tre �rod�
C        IF(SPARTICIP.LE.0.) ENCORE=.FALSE.                              ! Cas o� rien � �roder
C reclacul de la masse indicative possible mais on pr�f�re arreter l'�rosion
C si diamtre plus grand que prevu
        IF(DBAOBT.GT.DAC)ENCORE=.FALSE.
        ENCORE=.FALSE.
C fin du do while (encore)
                ENDDO
C melange final pour obtenir la couche active
      IF(MAC.GT.0.)THEN
        CALL MIXAGE(MAC,DAC,SAC,MBAOBT,DBAOBT,SBAOBT)
c      ELSEIF(MAC.LT.0.)THEN
c        M1=-MAC
c        CALL MIXAGE(M1,DAC,SAC,MBAOBT,DBAOBT,SBAOBT)
c        MAC=-M1
C mac=0
      ELSE
        MAC=MBAOBT
        DAC=DBAOBT
        SAC=SBAOBT
      ENDIF
      IF(MAC.LT.EPSM)MAC=0.
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE EROSIONBERGE(I,MBAOBT,DBAOBT,SBAOBT,MBADEM)
C calcule erosion des berges au dessus niveau eau
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER J,K,I,JG,JD,L
      DOUBLE PRECISION TMBA,MBAOBT,DBAOBT,SBAOBT,MBADEM
      LOGICAL RG,RD,EROSION
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNBCSP1(LNCMAX),DEFORM(LNCMAX)
      INTEGER DEBRG(NCMAX),FINRG(NCMAX),DEBRD(NCMAX),FINRD(NCMAX)
      DOUBLE PRECISION BMIU,PSTAB
     :,DM(LNCMAX),DZ(LNCMAX),MBAOBT0,COEF
      DOUBLE PRECISION XYCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      INTEGER JDEFMAX,JLIM


      COMMON/BMIU/BMIU,PSTAB
      COMMON/XGEOMACY/XYCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/DEFORM/DEFORM
      COMMON/PDEFOR/JDEFMAX
      COMMON/NC/NC,XNC
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XLGEO/DXMAIL,XDYA

C normalement on rentre avec mbaobt=0
C on utilise mbaobt0 pour comparer a mbadem
      MBAOBT0=MBAOBT
C on determine les zones de berge
C on determine les zones deberge gauche et droite en partant des zones du lit actif
C        write(*,*)'berges'

      JG=1
      J=DEFORM(1)-1
      JLIM=XNC(i-1)+2
      IF(J.GT.JLIM)then
      TMBA=XTMCSP1(J,1)
      IF(TMBA.LT.999.)THEN
        DEBRG(JG)=J
        JG=JG+1
        RG=.TRUE.
        DO K=J-1,JLIM,-1
          IF(RG)THEN
            FINRG(JG)=K
            TMBA=XTMCSP1(K,1)
            IF(TMBA.GT.999.)THEN
              FINRG(JG)=K+1
              RG=.FALSE.
            ENDIF
C fin du if sur Rg
          ENDIF
C fin boucle sur K
        ENDDO
C fin if sur TMBA premier point
      ENDIF
      elseif(J.eq.JLIM)then
C seul ce point est potentiellement erodable
      TMBA=XTMCSP1(J,1)
      IF(TMBA.LT.999.)THEN
        DEBRG(JG)=J
        finrg(jg)=j
        JG=JG+1
C fin if sur TMBA premier point
      ENDIF
C j inferieur au premier point
C donc rien a faire
C      else
      endif
      JD=1
      J=DEFORM(JDEFMAX)+1
      JLIM=XNC(i)-1
      IF(J.lT.JLIM)then
      TMBA=XTMCSP1(J,1)
      IF(TMBA.LT.999.)THEN
        DEBRD(JD)=J
        JD=JD+1
        RD=.TRUE.
        DO K=J+1,JLIM
          IF(RD)THEN
            FINRD(JD)=K
            TMBA=XTMCSP1(K,1)
            IF(TMBA.GT.999.)THEN
              FINRD(JD)=K-1
              RD=.FALSE.
            ENDIF
C fin du if sur RD
          ENDIF
C fin boucle sur K
        ENDDO
C fin if sur TMBA premier point
      ENDIF
      elseif(J.eq.JLIM)then
C seul ce point est potentiellement erodable
      TMBA=XTMCSP1(J,1)
      IF(TMBA.LT.999.)THEN
        DEBRd(Jd)=J
        finrd(jd)=j
        Jd=Jd+1
C fin if sur TMBA premier point
      ENDIF
C j superieur au dernier point
C donc rien a faire
C      else
      endif
c RECHERCHE DES EVENTUELLES AUTRES ZONES DE BERGE
      do J=1,JDEFMAX
        IF(DEFORM(j+1).GT.DEFORM(J)+1)THEN
          L=DEFORM(J+1)-1
          TMBA=XTMCSP1(L,1)
          IF(TMBA.LT.999.)THEN
            DEBRG(JG)=L
            JG=JG+1
            RG=.TRUE.
            DO K=L-1,DEFORM(J)+1,-1
              IF(RG)THEN
                FINRG(JG)=K
C la berge rive gauche s'arrete au point haut
                IF(XZCSP1(K,1).LT.XZCSP1(K+1,1))THEN
                  FINRG(JG)=K+1
                  RG=.FALSE.
                ENDIF
C fin du if sur Rg
              ENDIF
              IF(RG)THEN
                TMBA=XTMCSP1(K,1)
                IF(TMBA.GT.999.)THEN
                  FINRG(JG)=K+1
                  RG=.FALSE.
                ENDIF
C fin du if sur Rg
              ENDIF
C fin boucle sur K
            ENDDO
C fin if sur TMBA premier point
          ENDIF
          L=DEFORM(J)+1
          TMBA=XTMCSP1(L,1)
          IF(TMBA.LT.999.)THEN
            DEBRD(JD)=L
            JD=JD+1
            RD=.TRUE.
            DO K=L+1,DEFORM(J+1)-1
              IF(RD)THEN
                FINRD(JD)=K
C la berge rive droite s'arrete au point haut
                IF(XZCSP1(K,1).LT.XZCSP1(K-1,1))THEN
                  FINRD(JD)=K-1
                  RD=.FALSE.
                ENDIF
C fin du if sur RD
              ENDIF
              IF(RD)THEN
                TMBA=XTMCSP1(K,1)
                IF(TMBA.GT.999.)THEN
                  FINRD(JD)=K-1
                  RD=.FALSE.
                ENDIF
C fin du if sur RD
              ENDIF
C fin boucle sur K
            ENDDO
C fin if sur TMBA premier point
          ENDIF
C fin du if sur deform (j) et deform (j+1)
        ENDIF
C fin boucle sur j zone hors deformation
      ENDDO
C on met dans JG et JD le nombre de zones de berges
      JG=JG-1
      JD=JD-1
C on calcule maintenant le volume de berges erodees
C independamment du niveau eau
      DO K=1,JG
C la variable erosion sert a arreter erosion quand en un point on a equilibre
C eventuellement a remettre en cause
        EROSION=.TRUE.
        DO J=DEBRG(K),FINRG(K),-1
          IF(EROSION)THEN
c le cas des berges verticales ne semble pas necessiter de traitement specifique
C pstab est la pente de stabilite a sec
c            IF(XYCOU(J+1)-XYCOU(J).LE.EPS)THEN
            DZ(J)=XZCSP1(J,1)-XZCSP1(J+1,1)-(XYCOU(J+1)-XYCOU(J))*PSTAB
            IF(DZ(J).GT.0.)THEN
C on a une pente trop forte donc on cherche a eroder
              IF(XNBCSP1(J).GT.1)THEN
                IF((XZCSP1(J,1)-DZ(j)).GT.XZCSP1(J,2))THEN
C 'Erosion partielle du comp. sup. au point ',J
                  DM(J)=DZ(J)*ROS*XDYA(J)*DXMAIL(I)/POR1
                  MBAOBT0=MBAOBT0+DM(J)
c                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                  XZCSP1(J,1)=XZCSP1(J,1)-DZ
c                  XMCSP1(J,1)=XMCSP1(J,1)-DM
                ELSE
C 'Erosion totale du comp. sup. au point ',J
                  DM(J)=XMCSP1(J,1)
                  MBAOBT0=MBAOBT0+DM(J)
c                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                  CALL SUPPBA1(J)
                ENDIF
              ELSE
C 'Erosion partielle du comp. unique au point ',J
                DM(J)=DZ(j)*ROS*XDYA(J)*DXMAIL(I)/POR1
                  MBAOBT0=MBAOBT0+DM(J)
c                CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                XZCSP1(J,1)=XZCSP1(J,1)-DZ
              ENDIF
C else sur dz positif
            ELSE
c on arrete erosion berge si on a atteint la pente de stabilite en un point
C hypothese pouvant etre remise en cause
              EROSION=.FALSE.
C fin du if sur dz positif
            ENDIF
C fin du if sur erosion possible
          ENDIF
C fin boucle sur point d'une berge J
        ENDDO
C fin boucle sur K berges gauches
      ENDDO
      DO K=1,JD
C la variable erosion sert a arreter erosion quand en un point on a equilibre
C eventuellement a remettre en cause
        EROSION=.TRUE.
        DO J=DEBRD(K),FINRD(K)
          IF(EROSION)THEN
c le cas des berges verticales ne semble pas necessiter de traitement specifique
C pstab est la pente de stabilite a sec
c            IF(XYCOU(J)-XYCOU(J-1).LE.EPS)THEN
            DZ(j)=XZCSP1(J,1)-XZCSP1(J-1,1)-(XYCOU(J)-XYCOU(J-1))*PSTAB
            IF(DZ(j).GT.0.)THEN
C on a une pente trop forte donc on cherche a eroder
              IF(XNBCSP1(J).GT.1)THEN
                IF((XZCSP1(J,1)-DZ(j)).GT.XZCSP1(J,2))THEN
C 'Erosion partielle du comp. sup. au point ',J
                  DM(j)=DZ(j)*ROS*XDYA(J)*DXMAIL(I)/POR1
                  MBAOBT0=MBAOBT0+DM(J)
c                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                  XZCSP1(J,1)=XZCSP1(J,1)-DZ
c                  XMCSP1(J,1)=XMCSP1(J,1)-DM
                ELSE
C 'Erosion totale du comp. sup. au point ',J
                  DM(j)=XMCSP1(J,1)
                  MBAOBT0=MBAOBT0+DM(J)
c                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                  CALL SUPPBA1(J)
                ENDIF
              ELSE
C 'Erosion partielle du comp. unique au point ',J
                DM(j)=DZ(j)*ROS*XDYA(J)*DXMAIL(I)/POR1
                  MBAOBT0=MBAOBT0+DM(J)
c                CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM
c     :,XDCSP1(J,1),XSCSP1(J,1))
c                XZCSP1(J,1)=XZCSP1(J,1)-DZ
              ENDIF
C else sur dz positif
            ELSE
c on arrete erosion berge si on a atteint la pente de stabilite en un point
C hypothese pouvant etre remise en cause
              EROSION=.FALSE.
C fin du if sur dz positif
            ENDIF
C fin du if sur erosion possible
          ENDIF
C fin boucle sur point d'une berge J
          ENDDO
C fin boucle sur K berges droites
         ENDDO
C on calcule maintenant la variation de geometrie
C en appliquant une coefficient pour que MBAOBT<ou =MBADEM
         IF(MBAOBT0.GT.MBADEM)THEN
           IF(MBADEM.GT.0.)THEN
              coef=MBADEM/Mbaobt0
           ELSE
              coef=0.
           endif
         else
           coef=1.
         endif
C meme boucle une deuxieme fois
      DO K=1,JG
C la variable erosion sert a arreter erosion quand en un point on a equilibre
C eventuellement a remettre en cause
        EROSION=.TRUE.
        DO J=DEBRG(K),FINRG(K),-1
          IF(EROSION)THEN
c le cas des berges verticales ne semble pas necessiter de traitement specifique
C pstab est la pente de stabilite a sec
c            IF(XYCOU(J+1)-XYCOU(J).LE.EPS)THEN
c            DZ=XZCSP1(J,1)-XZCSP1(J+1,1)-(XYCOU(J+1)-XYCOU(J))*PSTAB
            DZ(J)=coef*DZ(J)
            IF(DZ(j).GT.0.)THEN
C on a une pente trop forte donc on cherche a eroder
              IF(XNBCSP1(J).GT.1)THEN
                IF((XZCSP1(J,1)-DZ(j)).GT.XZCSP1(J,2))THEN
C 'Erosion partielle du comp. sup. au point ',J
                  DM(J)=COEF*DM(J)
                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                  XZCSP1(J,1)=XZCSP1(J,1)-DZ(j)
                  XMCSP1(J,1)=XMCSP1(J,1)-DM(j)
                ELSE
C 'Erosion totale du comp. sup. au point ',J
                  DM(J)=COEF*DM(J)
                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                  CALL SUPPBA1(J)
                ENDIF
              ELSE
C 'Erosion partielle du comp. unique au point ',J
                DM(J)=COEF*DM(J)
                CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                XZCSP1(J,1)=XZCSP1(J,1)-DZ(j)
              ENDIF
C else sur dz positif
            ELSE
c on arrete erosion berge si on a atteint la pente de stabilite en un point
C hypothese pouvant etre remise en cause
              EROSION=.FALSE.
C fin du if sur dz positif
            ENDIF
C fin du if sur erosion possible
          ENDIF
C fin boucle sur point d'une berge J
        ENDDO
C fin boucle sur K berges gauches
      ENDDO
      DO K=1,JD
C la variable erosion sert a arreter erosion quand en un point on a equilibre
C eventuellement a remettre en cause
        EROSION=.TRUE.
        DO J=DEBRD(K),FINRD(K)
          IF(EROSION)THEN
c le cas des berges verticales ne semble pas necessiter de traitement specifique
C pstab est la pente de stabilite a sec
c            IF(XYCOU(J)-XYCOU(J-1).LE.EPS)THEN
c            DZ=XZCSP1(J,1)-XZCSP1(J-1,1)-(XYCOU(J)-XYCOU(J-1))*PSTAB
            DZ(J)=coef*DZ(J)
            IF(DZ(j).GT.0.)THEN
C on a une pente trop forte donc on cherche a eroder
              IF(XNBCSP1(J).GT.1)THEN
                IF((XZCSP1(J,1)-DZ(j)).GT.XZCSP1(J,2))THEN
C 'Erosion partielle du comp. sup. au point ',J
                  DM(J)=COEF*DM(J)
                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                  XZCSP1(J,1)=XZCSP1(J,1)-DZ(j)
                  XMCSP1(J,1)=XMCSP1(J,1)-DM(j)
                ELSE
C 'Erosion totale du comp. sup. au point ',J
                  DM(J)=COEF*DM(J)
                  CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                  CALL SUPPBA1(J)
                ENDIF
              ELSE
C 'Erosion partielle du comp. unique au point ',J
                DM(J)=COEF*DM(J)
                CALL MIXAGE(MBAOBT,DBAOBT,SBAOBT,DM(j)
     :,XDCSP1(J,1),XSCSP1(J,1))
                XZCSP1(J,1)=XZCSP1(J,1)-DZ(j)
              ENDIF
C else sur dz positif
            ELSE
c on arrete erosion berge si on a atteint la pente de stabilite en un point
C hypothese pouvant etre remise en cause
              EROSION=.FALSE.
C fin du if sur dz positif
            ENDIF
C fin du if sur erosion possible
          ENDIF
C fin boucle sur point d'une berge J
          ENDDO
C fin boucle sur K berges droites
         ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT PAR STRATES HORIZONTALES
C-----------------------------------------------------------------------
C D�p�t de s�diments dans la section du lit (limite entre Ba et Ac)
C Entr�e: I,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER I,J,JDEP,JDEPMAX,DEPOSAB(NCMAX)
      DOUBLE PRECISION Z,ZMIN1,ZMIN2,MBADEP,DDEP,SDEP,ZBA,MBA,DBA,SBA
     &  ,DYTOT,DZ,DM,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,TN,DTN,TNP1
      INTEGER NC(0:LMAX),XNC(0:LMAX)
C      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX),DZMAX
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)

      LOGICAL PROCHE
      EXTERNAL PROCHE

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
c      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/TREEL/TN,DTN,TNP1
c      print*,' entree depsec1',I
c      print*,'Tn+1/2=',0.5*(TNP1+TN)
C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.TRUE.
c      DO WHILE(ENCORE)
C Recherche du [des] point[s] de d�p�t: on le[s] met dans DEPOSAB(JDEP)
C ZMIN1 cote du point le plus bas  ZMIN2 cote du point juste au-dessus
C le 17 avril 2012 modif du lit total au lit actif
        ZMIN1=XZCSP1(XNC(I-1)+1,1)
        ZMIN2=ZMIN1
C        J=XNC(I-1)+2
        J=XNCMMAG(I)+1
        JDEP=0
C        DO WHILE(J.LE.XNC(I))
        DO WHILE(J.LT.XNCMMAD(I))
          Z=XZCSP1(J,1)
          IF(Z.LT.ZMIN1-EPSY)THEN
C          IF(Z.LE.ZMIN1-EPSY)THEN
C Changement du point le plus bas
C            IF(ZMIN2.NE.ZMIN1) ZMIN2=ZMIN1
            ZMIN2=ZMIN1
            ZMIN1=Z
            JDEP=1
            DEPOSAB(JDEP)=J
          ELSEIF(Z.LT.ZMIN1+EPSY)THEN
C          ELSEIF(Z.LE.ZMIN1+EPSY)THEN
C Ajout � la liste des points bas
            ZMIN1=MIN(ZMIN1,Z)
            JDEP=JDEP+1
            DEPOSAB(JDEP)=J
          ELSEIF(Z.LT.ZMIN2)THEN
C Changement du point juste au-dessus
            ZMIN2=Z
          ENDIF
          J=J+1
        ENDDO
        JDEPMAX=JDEP
c      print*,'JDEPMAX=',JDEPMAX
c      pause

C Calcul de la variation moyenne attendue de cote des points concern�s: DZ
        DYTOT=0.
        DO JDEP=1,JDEPMAX
          J=DEPOSAB(JDEP)
          DYTOT=DYTOT+XDYA(J)
        ENDDO
        DZ=(MBADEP*POR1/ROS)/(DXMAIL(I)*DYTOT)
        IF(DZ.GT.ZMIN2-ZMIN1) THEN
           DZ=ZMIN2-ZMIN1
C          print*,'DZ=',DZ,ZMIN1,ZMIN2
        ENDIF
        IF(DZ.GT.DZMAX)THEN
          DZ=DZMAX
        ENDIF
c      print*,'DYTOT=',DYTOT
c      pause
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEP=1,JDEPMAX
          J=DEPOSAB(JDEP)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
          DM=DZ*XDYA(J)*DXMAIL(I)*ROS/POR1
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsecmba',mba
                 mba=0.
c                 pause
              endif
              IF(DM.LT.0.)THEN
                  write(*,*)'depsecdm',dm,dz,XDYA(J),DXMAIL(I)
                 dm=0.
c                 pause
              endif

            CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)
            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
            ZBA=XZCSP1(J,1)+DZ
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MBADEP=MBADEP-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C on fait toujours un seul passage = la variable encore pas utilisee
c        IF(MBADEP.LE.EPSM) ENCORE=.FALSE.                            ! Tout est d�pos� (ou presque, pour �viter les pbs d'arrondi num�rique)
c        ENCORE=.FALSE.
c      ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie uniform�mment sur le pourcours de la section:
C tous les points de section (intermaille I)o� le depot sera possible vont monter
C de la m�me d�formation
C-----------------------------------------------------------------------
C D�p�t dans le lit actif uniquement
C Entr�e: I,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      INTEGER I,J,JDEP,JDEPMAX,DEPOSAB(NCMAX)
      DOUBLE PRECISION MBADEP,DDEP,SDEP,ZBA,MBA,DBA,SBA,TBA
     &  ,DYTOT,DZ,DM,ZABS
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
     :  ,DZMAX
      LOGICAL PROCHE
      EXTERNAL PROCHE

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
C      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1

C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.TRUE.
      ZABS=XCTDF(I)+YINTER(I)
C on fait toujours un seul passage = la variable encore pas utilisee
c      DO WHILE(ENCORE)
      IF(YINTER(I).GT.EPSY) THEN
C Recherche du [des] point[s] de d�p�t, on suppose que le dep�t s'effecutera
Csur tous les points situ�s sous la surface de l'eau: on le[s] met dans
C DEPOSAB(JDEP): � l'int�rieur du lit actif (limites exclues),
C on le[s] met dans DEPOSAB(JDEP)
        J=XNCMMAG(I)+1
        JDEP=0
        DO WHILE(J.LT.XNCMMAD(I))
C Cas o� point hors de l'eau
          IF(XZCSP1(J,1).LT.ZABS) THEN
          JDEP=JDEP+1
          DEPOSAB(JDEP)=J
c 100      CONTINUE
          J=J+1
         ELSE
         J=J+1
      ENDIF
        ENDDO
        JDEPMAX=JDEP
      ELSE
      JDEPMAX=0
      ENDIF

c      print*,'JDEPMAX=',JDEPMAX
c      pause
c modif kamal f�vrier 2005: pour gerer le cas ou l'on a peu de points sous la surface d'eau
C si on a peu de points sous la surface d'eau, on d�pose par couche horizontale
      IF(JDEPMAX.LE.1)THEN
c       Write(*,*)'peu de points sous la surface d eau, intermaille ', I
c       write(*,*)'nombre de points  ',JDEPMAX
c       print*,'Tn+1/2=',0.5*(TNP1+TN)
       CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c       ENCORE=.FALSE.
c       pause
C       ENDIF
c      MRESTE=MBADEP
C      IF(JDEPMAX.GT.3) THEN
        ELSE
C Calcul de la variation moyenne attendue de cote des points concern�s: DZ
C cette variation sera appliqu�e a tous les points mis dans DEPOSAB(JDEP)
        DYTOT=0.
        DO JDEP=1,JDEPMAX
          J=DEPOSAB(JDEP)
          DYTOT=DYTOT+XDYA(J)
        ENDDO
        DZ=MBADEP*POR1/(ROS*DXMAIL(I)*DYTOT)
          IF(DZ.GT.DZMAX)THEN
C            write(*,*)'fort depot',' section ',I,' temps ',TN
             DZ=DZMAX
             MBADEP=DZ*ROS*DYTOT*DXMAIL(I)/POR1
          ENDIF
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la
C strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts
C incidents dans une nouvelle strate.
        DO JDEP=1,JDEPMAX
          J=DEPOSAB(JDEP)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
C limitation de dz � la hauteur d'eau
          ZBA=XZCSP1(J,1)+DZ
          IF(ZBA.GT.ZABS)THEN
            DZ=ZABS-XZCSP1(J,1)
c            ZBA=ZABS
          ENDIF

          DM=DZ*XDYA(J)*DXMAIL(I)*ROS/POR1
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
C      Write(*,*)'Depsec I=',I,' avant Mixage'
                          IF(MBA.LT.0.)THEN
                  write(*,*)'depsec2mba',mba
                 mba=0.
              endif
              CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)
            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
            ZBA=XZCSP1(J,1)+DZ
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
            ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MBADEP=MBADEP-DM
        ENDDO
      ENDIF

C Fin du traitement avec �ventuellement un passage suppl�mentaire
c 300   CONTINUE
c      IF(JDEPMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
c      IF(MBADEP.LE.EPSM) ENCORE=.FALSE.
c      ENCORE=.FALSE.
c      ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC3(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie sur le pourcours de la section:
C la d�formation est PROPORTIONNELLE A 1/CONTRAINTE(m) si
C CONTRAINTE<COEF*contrainte critique de fin de mouvement de la couche active.
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C la contrainte critique est calcul�e pour la couche active soit par Shields
C soit par CK*Shields.
C le depot a lieu dans les le lit actif uniquement.
C COEF un coefficient pris �gal � 0.5
C COEFD puissance prise �gale � 1
C-----------------------------------------------------------------------
C D�p�t de s�diments dans le lit actif
C Entr�e: I,JRH,RH,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C RH,rayon hydraulique
C JRH, pente energie*rayon hydraulique
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
C hypoth�se: contrainte de de de mouvement est �gale � la contrainte de mise en mouvement

      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      INTEGER I,J
      DOUBLE PRECISION MBADEP,MRESTE,DDEP,SDEP,ZBA,MBA,DBA,SBA,ZABS
     &  ,DZ,DM,SPARTICIP,PARTICIP(NCMAX),TFAC !TFAC contrainte de fin de mouvement
     &  ,CONTRAINTE,ROGRAVJ,RH,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER CHOIXC
      logical TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION COEFC,COEFD,EPSS,TN,DTN,TNP1
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &  ,PSOUE(LNCMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &,TO(LNCMAX)
     :,DZMAX
     :,JRH(LMAX),KS1(LMAX)
      DOUBLE PRECISION SHIELDS,TOCMM

      LOGICAL PROCHE
      EXTERNAL PROCHE
      EXTERNAL TOCMM,SHIELDS
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC ! si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/COEFD/COEFD
      COMMON/EPSS/EPSS
      COMMON/SOLIDE/JRH,KS1


C on fait toujours un seul passage = la variable encore pas utilisee
C      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      CONTRAINTE=ROGRAV*JRH(I)

C on fait toujours un seul passage = la variable encore pas utilisee
c      DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau si MPC: sont mis dans PSOUE(JSOUE)
C si choixc = 2 (contrainte uniforme RO*GRAV*RH*J)on recherche les points que
C dans le lit actif: sont mis dans DEFORM(JDEF)
C on suppose que le dep�t s'effecutera sur des points � l'int�rieur du lit actif
C (limites exclues).
C si MPC est choisie, on recherche, apr�s calcul de contrainte,
C les points situ�s dans le lit actif
      IF(YINTER(I).GT.EPSY) THEN
      IF(CHOIXC.NE.2) THEN
C        J=XNCMOAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section :
C debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0
C        DO WHILE(J.LT.XNCMOAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J
         IF(J.GT.XNCMMAG(I))THEN
                   iF(J.LT.XNCMMAD(I))THEN
             JDEF=JDEF+1
             DEFORM(JDEF)=J
           ENDIF
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
      ELSEIF(CHOIXC.EQ.2)THEN
C on recherche les points sous l'eau directement dans le lit actif
         J=XNCMMAG(I)+1
         JSOUE=0
         DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
      ENDIF
      ELSE
      JSOUEMAX=0
      JDEFMAX=0
      ENDIF
C ap
C si bief sec on ne doit pas venir dans ce sous programme
C       IF(JSOUEMAX.LE.3)THEN
       IF(JDEFMAX.LE.1)THEN
c       Write(*,*)'peu de points sous la surface d eau, intermaille ', I
c       write(*,*)'nombre de points  ',JSOUEMAX
       CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c       ENCORE=.FALSE.
C      ENDIF
C      IF(JSOUEMAX.GT.3)THEN
C plusieurs points sous l'eau
C mais si contrainte faible, appel a depot uniforme
        ELSEIf(contrainte.lt.eps)THEN
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
        ELSE
      IF(CHOIXC.NE.2) THEN
      ROGRAVJ=CONTRAINTE/RH      ! calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
C      CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC) ! pour tous les points de la section I situ�s sous l'eau
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
            TO(J)=TOMPC(J)
         ELSE
            TO(J)=COEFC*CONTRAINTE !si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
         ENDIF
      ENDDO
        ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
       ENDDO
C fin du if sur choixc
      ENDIF
      IF(TCPENTE) THEN
C calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)  ! pour tous les points de la section I situ�s sous l'eau
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*SHIELDS(DDEP,JRH(I),RH) ! s�diments couche active seront depos�s
        ENDDO
C si pas de correction pente sur tc
      ELSE
       DO JDEF=1,JDEFMAX
       J=DEFORM(JDEF)
       TC(J)=SHIELDS(DDEP,JRH(I),RH)
       ENDDO
      ENDIF


C calcul de la r�partition du depot en fonction de (1/contrainte)^(m)

      MRESTE=MBADEP
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TFAC=TC(J)
C TFAC ici est la contrainte de fin de mouvement, suppos�e �gale � la contrainte de mise en mouvement
C      IF(TC(J).EQ.0.)THEN !pas de d�pot sur les points ou la contrainte critique est nulle (i.e paroies verticales, points instables)
c      IF(TFAC.EQ.0.)THEN
c        PARTICIP(JDEF)=0.
c      ELSE
C      IF(TC(J).NE.0.)THEN
        IF(TO(J).GT.TFAC)THEN
          PARTICIP(JDEF)=XDYA(J)

        ELSEIF(TO(J).GT.0.5*TFAC)THEN ! COEF=0.5
          PARTICIP(JDEF)=XDYA(J)*(TFAC/TO(J))**COEFD ! COEFD est pris �gal � 1
        ELSE
C si To(j) nul ou faible
          PARTICIP(JDEF)=XDYA(J)*2.**COEFD !2=1/COEF
        ENDIF
C fin du if sur tc(j) nul
c      ENDIF
      SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO
      IF(SPARTICIP.GT.EPSS)THEN
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          MBA=XMCSP1(J,1)
c          If(mba.lt.0.)then
c             write(*,*)'depsec3mba',mba
c          endif
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
                IF(PARTICIP(JDEF).LT.EPS)THEN
                     dm=0.
                  dz=0.
                ELSE
            DM=MBADEP*PARTICIP(JDEF)/SPARTICIP

C          DZ=(DM*POR1/ROS)/(DXMAIL(I)*XDYA(J))
C modif du 24 avril 2003 par Kamal pour ne pas faire de mixage si DM=0
C rappel: DM=0 signifie que le point ne subit pas de d�formation, donc pas besoin de faire le mixage
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
C AP
C          IF(DM.EQ.0.) GOTO 300
            IF(DM.GT.0.)THEN
              DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
C limitation de dz � la hauteur d'eau
              ZBA=XZCSP1(J,1)+DZ
              IF(ZBA.GT.ZABS)THEN
                DZ=ZABS-XZCSP1(J,1)
                DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
              ENDIF
             IF(DZ.GT.DZMAX)THEN
c            write(*,*)'fort depot',' section ',I,' temps ',TN
               DZ=DZMAX
               DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
             ENDIF

             IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsec3mba',mba
c                  pause
                 mba=0.
              endif
              CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)
              XZCSP1(J,1)=XZCSP1(J,1)+DZ
              XMCSP1(J,1)=MBA
              XDCSP1(J,1)=DBA
              XSCSP1(J,1)=SBA
            ELSE
C creation d une nouvelle couche si pas encore au nombre maximal
C de couches et sediemnst pas proches
C ou si le nombre de couches est 1
              ZBA=XZCSP1(J,1)+DZ
              CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
            ENDIF
C AP
C 300      CONTINUE
C fin du if sur dm=0
          ENDIF
C fin du if sur sparticip=0
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MRESTE=MRESTE-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C fin du if sur sparticip nul
      ENDIF
C 400   CONTINUE
C      write(*,*)'mbadep',mbadep
CAP inutile      IF(JDEFMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
c      IF(MRESTE.LE.EPSM*MBADEP) ENCORE=.FALSE.
c      MBADEP=MRESTE
      IF(SPARTICIP.LE.0.)THEN
           Write(*,*)'erreur: depot 3 impossible, intermaille ', I
           write(*,*)'abscisse  ',xtmail(i)
c           pause
c             stop
      ENDIF
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.FALSE.
c      ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC4(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie sur le pourtour de la section:
C la d�formation est PROPORTIONNELLE A L'ECART (CONTRAINTE CRITIQUE-gamma*CONTRAINTE)^(PUI)
c PUI d�pend de la formule de capacit� solide: =1.( si MPM ou englund, =1 si Bagnold
C ou donne par utilisateur
C Gamma est calcul� dans sous programme en d�treminant la valeur minimale du rapport CONTRAINTE CRITIQUE/CONTRAINTE
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C la contrainte critique est calcul�e pour la couche ACTIVE soit par Shields soit par CK*Shields.
C le depot a lieu dans les le lit actif uniquement.
C-----------------------------------------------------------------------
C D�p�t de s�diments dans le lit actif
C Entr�e: I,JRH,RH,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C RH,rayon hydraulique
C JRH, pente energie*rayon hydraulique
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
C hypoth�se: contrainte de de de mouvement est �gale � la contrainte de mise en mouvement

      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J,J1
      DOUBLE PRECISION MBADEP,MRESTE,DDEP,SDEP,ZBA,MBA,DBA,SBA,ZABS
     &  ,DZ,DM,SPARTICIP,PARTICIP(NCMAX),TFAC !TFAC contrainte de fin de mouvement
     &  ,CONTRAINTE,ROGRAVJ,RH,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,TN,DTN,TNP1
      DOUBLE PRECISION B1,B2,C ! deux variables utiliser pour calculer min(TC/TO)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER CHOIXC
      LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION COEFC,GAMA,PUIdepot,EPSS ! GAMA est un coefficient que j'utilise pour que le terme (contraintre critique-GAMA*contrainte) soit toujours strictement positif
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &    ,PSOUE(LNCMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &,TO(LNCMAX)
     : ,DZMAX
     :,JRH(LMAX),KS1(LMAX)
      DOUBLE PRECISION TOCMM,SHIELDS

      LOGICAL PROCHE
      EXTERNAL PROCHE
      EXTERNAL TOCMM,SHIELDS

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC ! si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/PDEPOT/PUIdepot
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/EPSS/EPSS
      COMMON/SOLIDE/JRH,KS1

      IF(JRH(I).LT.EPSS)THEN
            CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
            RETURN
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      CONTRAINTE=ROGRAV*JRH(I)

C on fait toujours un seul passage = la variable encore pas utilisee
c       DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau si MPC: sont mis dans PSOUE(JSOUE)
C si choixc = 2 (contrainte uniforme RO*GRAV*RH*J)on recherche les points que dans le lit actif: sont mis dans DEFORM(JDEF)
C on suppose que le dep�t s'effecutera sur des points � l'int�rieur du lit actif (limites exclues).
C si MPC est choisie, on recherche, apr�s calcul de contrainte,les points situ�s dans le lit actif
      IF(YINTER(I).GT.EPSY) THEN
      IF(CHOIXC.NE.2) THEN
C        J=XNCMOAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section : debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
C        DO WHILE(J.LT.XNCMOAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau
         IF(J.GT.XNCMMAG(I))THEN
           IF(J.LT.XNCMMAD(I))THEN
             JDEF=JDEF+1
             DEFORM(JDEF)=J
           ENDIF
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
      ELSEIF(CHOIXC.EQ.2)THEN
         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
         JSOUE=0
        DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
      ENDIF
      ELSE
      JSOUEMAX=0
      JDEFMAX=0
      ENDIF

C kamal 2005
C si bief sec ou peu de points sous la surface d'eau on ne doit pas venir dans ce sous programme
      IF(JDEFMAX.LE.1)THEN
c       Write(*,*)'peu de points sous la surface d eau, intermaille ', I
c       write(*,*)'nombre de points  ',JSOUEMAX
c       print*,'Tn+1/2=',0.5*(TNP1+TN)
       CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c       ENCORE=.FALSE.
C      ENDIF
C plusieurs points sous l'eau
C mais si contrainte faible, appel a depot uniforme
        ELSEIf(contrainte.lt.eps)THEN
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
C plusieurs points sous l'eau
       ELSE
C      IF(JSOUEMAX.GT.3)THEN
      IF(CHOIXC.NE.2) THEN
      ROGRAVJ=CONTRAINTE/RH      ! calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
C      CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC) ! pour tous les points de la section I situ�s sous l'eau
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
            TO(J)=TOMPC(J)
         ELSE
            TO(J)=COEFC*CONTRAINTE !si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
         ENDIF
      ENDDO

        ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
       ENDDO
      ENDIF
      IF(TCPENTE) THEN
C calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)  ! pour tous les points de la section I situ�s sous l'eau
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*SHIELDS(DDEP,JRH(I),RH) ! s�diments couche active seront depos�s
        ENDDO
      ELSE
       DO JDEF=1,JDEFMAX
       J=DEFORM(JDEF)
       TC(J)=SHIELDS(DDEP,JRH(I),RH)
       ENDDO
      ENDIF
      J1=DEFORM(1) ! l'objectif est de calculer une valeur de gamma pour que (TC-TO) soit toujours trictements positif
      B1=TC(J1)/TO(J1)
      JDEF=2
      DO WHILE(JDEF.LE.JDEFMAX)
      J=DEFORM(JDEF)
      B2=TC(J)/TO(J)
      IF(B1.NE.0.) THEN
         IF(B2.NE.0.AND.B2.LE.B1) THEN
          C=B2
            B1=C
         ENDIF
      ELSEIF(B1.EQ.0)THEN
         IF(B2.NE.0) THEN
          C=B2
            B1=C
         ENDIF
      ENDIF
          JDEF=JDEF+1
      ENDDO
      GAMA=0.5*B1   !B1 est la valeur minimal de TC/TO, pour gamme on prend 0.5*B1


C calcul de la r�partition du depot en fonction de (contraintre critique-Gama*contrainte)^(1.5)

      MRESTE=MBADEP
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TFAC=TC(J) ! TFAC ici est la contrainte de fin de mouvement, suppos�e �gale � la contrainte de mise en mouvement
      IF(TFAC.EQ.0.)THEN
C      IF(TC(J).EQ.0.)THEN !pas de d�pot sur les points ou la contrainte critique est nulle (i.e paroies verticales, points instables)
        PARTICIP(JDEF)=0.
      ELSE
          PARTICIP(JDEF)=XDYA(J)*(TFAC-GAMA*TO(J))**PUIDEPOT
C fin du if sur tc(j) nul
      ENDIF
      SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO
      IF(SPARTICIP.GT.EPSS)THEN
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
                IF(PARTICIP(JDEF).LT.EPS)THEN
                     dm=0.
                  dz=0.
                ELSE
          DM=MBADEP*PARTICIP(JDEF)/SPARTICIP
C          DZ=(DM*POR1/ROS)/(DXMAIL(I)*XDYA(J))
C modif du 24 avril 2003 par Kamal pour ne pas faire de mixage si DM=0
C rappel: DM=0 signifie que le point ne subit pas de d�formation, donc pas besoin de faire le mixage
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
C AP

          IF(DM.GT.0.)THEN
          DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
C limitation de dz � la hauteur d'eau
          ZBA=XZCSP1(J,1)+DZ
          IF(ZBA.GT.ZABS)THEN
            DZ=ZABS-XZCSP1(J,1)
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF(DZ.GT.DZMAX)THEN
c            write(*,*)'fort depot',' section ',I,' temps ',TN
            DZ=DZMAX
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsec4mba',mba
c                   pause
                 mba=0.
              endif
            CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)
            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
C creation d une nouvelle couche si pas encore au nombre maximal
C de couches et sediemnst pas proches
C ou si le nombre de couches est 1
            ZBA=XZCSP1(J,1)+DZ
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
            ENDIF
C AP
C 300      CONTINUE
C fin du if sur dm=0
          ENDIF
C fin du if sur sparticip=0
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MRESTE=MRESTE-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C fin du if sur sparticip nul
      ENDIF
CAP inutile      IF(JDEFMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
C      IF(MRESTE.LE.EPSM*MBADEP) ENCORE=.FALSE.
      MBADEP=MRESTE
C AP      IF(MBADEP.LE.EPS**2.) ENCORE=.FALSE.
C AP sparticip nul ne doit jamais arriver
C      IF(SPARTICIP.LE.0.) ENCORE=.FALSE.
      IF(SPARTICIP.LE.0.)THEN
           Write(*,*)'erreur: depot 4 impossible, intermaille ', I
           write(*,*)'abscisse  ',xtmail(i)
       CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
c           pause
c          stop
      ENDIF
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
C      ENCORE=.FALSE.
C      ENDDO
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC5(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie sur le pourcours de la section:
C la d�formation est PROPORTIONNELLE A L'ECART (CONTRAINTE CRITIQUE-gamma*CONTRAINTE)^(1.5)
C Gamma est calcul� dans sous programme en d�treminant la valeur MOYENNE du rapport CONTRAINTE CRITIQUE/CONTRAINTE
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C la contrainte critique est calcul�e pour la couche ACTIVE soit par Shields soit par CK*Shields.
C le depot a lieu dans les le lit actif uniquement.
C-----------------------------------------------------------------------
C D�p�t de s�diments dans le lit actif
C Entr�e: I,JRH,RH,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C RH,rayon hydraulique
C JRH, pente energie*rayon hydraulique
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
C hypoth�se: contrainte de de de mouvement est �gale � la contrainte de mise en mouvement

      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J
      DOUBLE PRECISION MBADEP,MRESTE,DDEP,SDEP,ZBA,MBA,DBA,SBA,ZABS
     &  ,DZ,DM,SPARTICIP,PARTICIP(NCMAX),TFAC !TFAC contrainte de fin de mouvement
     &  ,CONTRAINTE,ROGRAVJ,RH,B1,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,TN,EPSS,DTN,TNP1
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER CHOIXC
      LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION COEFC,GAMA,PUIdepot ! GAMA est un coefficient que j'utilise pour que le terme (contraintre critique-GAMA*contrainte) soit toujours strictement positif
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &    ,PSOUE(LNCMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX),SXDYA
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &,TO(LNCMAX)
     : ,DZMAX
     ;,JRH(LMAX),KS1(LMAX)
      DOUBLE PRECISION TOCMM,SHIELDS
      LOGICAL PROCHE
      EXTERNAL PROCHE
      EXTERNAL TOCMM,SHIELDS

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC ! si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/Pdepot/PUIdepot
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/EPSS/EPSS
      COMMON/SOLIDE/JRH,KS1

      IF(JRH(I).LT.EPSS)THEN
            CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
            RETURN
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
C      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      CONTRAINTE=ROGRAV*JRH(I)
C on fait toujours un seul passage = la variable encore pas utilisee
C       DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau si MPC: sont mis dans PSOUE(JSOUE)
C si choixc = 2 (contrainte uniforme RO*GRAV*RH*J)on recherche les points que dans le lit actif: sont mis dans DEFORM(JDEF)
C on suppose que le dep�t s'effecutera sur des points � l'int�rieur du lit actif (limites exclues).
C si MPC est choisie, on recherche, apr�s calcul de contrainte,les points situ�s dans le lit actif
      IF(YINTER(I).GT.EPSY) THEN
      IF(CHOIXC.NE.2) THEN
C        J=XNCMOAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section : debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
C        DO WHILE(J.LT.XNCMOAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau
         IF(J.GT.XNCMMAG(I))THEN
           IF(J.LT.XNCMMAD(I))THEN
             JDEF=JDEF+1
             DEFORM(JDEF)=J
            ENDIF
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
      ELSEIF(CHOIXC.EQ.2)THEN
         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
         JSOUE=0
        DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
      ENDIF
      ELSE
      JSOUEMAX=0
      JDEFMAX=0
      ENDIF
C ap
C si bief sec on ne doit pas venir dans ce sous programme
      IF(JDEFMAX.LE.1)THEN
c       Write(*,*)'peu de points sous la surface d eau, intermaille ', I
c       write(*,*)'nombre de points  ',JSOUEMAX
c       print*,'Tn+1/2=',0.5*(TNP1+TN)
c       write(*,*)'entree5 depsec'
       CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c       write(*,*)'sortie5 depsec'
c       ENCORE=.FALSE.
c      ENDIF
c      IF(JSOUEMAX.GT.3)THEN
C plusieurs points sous l'eau
C mais si contrainte faible, appel a depot uniforme
        ELSEIf(contrainte.lt.eps)THEN
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
C plusieurs points sous l'eau
       else
       IF(CHOIXC.NE.2) THEN
       ROGRAVJ=CONTRAINTE/RH      ! calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
c       CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC) ! pour tous les points de la section I situ�s sous l'eau
        DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
            TO(J)=TOMPC(J)
         ELSE
            TO(J)=COEFC*CONTRAINTE !si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
         ENDIF
        ENDDO

       ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
        ENDDO
       ENDIF
       IF(TCPENTE) THEN
C calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)  ! pour tous les points de la section I situ�s sous l'eau
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*SHIELDS(DDEP,JRH(I),RH) ! s�diments couche active seront depos�s
        ENDDO
       ELSE
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TC(J)=SHIELDS(DDEP,JRH(I),RH)
        ENDDO
       ENDIF
      B1=0.
C on calcule la valeur moyenne pond�r�e du rapport (TC(J)/TO(J))
      JDEF=1
      SXDYA=0.
C somme des distances horizontales XDYA(J)
      DO WHILE(JDEF.LE.JDEFMAX)
      J=DEFORM(JDEF)
c      IF(TO(J).LT.EPS)THEN
c         write(*,*)'to',to(j)
c      endif
      B1=XDYA(J)*(TC(J)/TO(J))+B1
      SXDYA=XDYA(J)+SXDYA
      JDEF=JDEF+1
      ENDDO
      if(sxdya.LT.EPS)then
        write(*,*)'sxdya',sxdya,jdefmax
C        pause
         gama=1.
      else
      GAMA=0.5*B1/SXDYA  ! moitie de la moyenne pond�r�e de  (TC(J)/TO(J))
      endif
C calcul de la r�partition du depot en fonction de (contraintre critique-Gama*contrainte)^(PUI)

      MRESTE=MBADEP
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TFAC=TC(J) ! TFAC ici est la contrainte de fin de mouvement, suppos�e �gale � la contrainte de mise en mouvement
      IF(TFAC.EQ.0.)THEN
C      IF(TC(J).EQ.0.)THEN !pas de d�pot sur les points ou la contrainte critique est nulle (i.e paroies verticales, points instables)
        PARTICIP(JDEF)=0.
      ELSEIF(TFAC.LE.GAMA*TO(J))THEN
C      ELSEIF(TC(J).LE.GAMA*TO(J))THEN
       PARTICIP(JDEF)=0.
      ELSEIF(TFAC.GT.GAMA*TO(J))THEN
C      ELSEIF(TC(J).GT.GAMA*TO(J))THEN
          PARTICIP(JDEF)=XDYA(J)*(TFAC-GAMA*TO(J))**PUIDEPOT
C fin du if sur tc(j) nul
      ENDIF
      SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO
      IF(SPARTICIP.GT.EPSS)THEN
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
                IF(PARTICIP(JDEF).LT.EPS)THEN
                     dm=0.
                  dz=0.
                ELSE
          DM=MBADEP*PARTICIP(JDEF)/SPARTICIP
C          DZ=(DM*POR1/ROS)/(DXMAIL(I)*XDYA(J))
C modif du 24 avril 2003 par Kamal pour ne pas faire de mixage si DM=0
C rappel: DM=0 signifie que le point ne subit pas de d�formation, donc pas besoin de faire le mixage
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
C AP
C          IF(DM.EQ.0.) GOTO 300
          IF(DM.GT.0.)THEN
          DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
C limitation de dz � la hauteur d'eau
          ZBA=XZCSP1(J,1)+DZ
          IF(ZBA.GT.ZABS)THEN
            DZ=ZABS-XZCSP1(J,1)
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF(DZ.GT.DZMAX)THEN
c            write(*,*)'fort depot',' section ',I,' temps ',TN
            DZ=DZMAX
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsec5mba',mba
                 mba=0.
              endif
            CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)

            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
C creation d une nouvelle couche si pas encore au nombre maximal
C de couches et sediemnst pas proches
C ou si le nombre de couches est 1
            ZBA=XZCSP1(J,1)+DZ
c             write(*,*)'creeba'
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
c             write(*,*)'fcreeba'
            ENDIF
C AP
C fin du if sur dm=0
          ENDIF
C fin du if sur sparticip=0
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MRESTE=MRESTE-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C fin du if sur sparticip nul
      ENDIF
CAP inutile      IF(JDEFMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
C      IF(MRESTE.LE.EPSM*MBADEP) ENCORE=.FALSE.
      MBADEP=MRESTE
C AP      IF(MBADEP.LE.EPS**2.) ENCORE=.FALSE.
C AP sparticip nul ne doit jamais arriver
C      IF(SPARTICIP.LE.0.) ENCORE=.FALSE.
      IF(SPARTICIP.LE.0.)THEN
           Write(*,*)'erreur: depot 5 impossible, intermaille ', I
           write(*,*)'abscisse  ',xtmail(i)
       CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
c             stop
      ENDIF
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.FALSE.
c      ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC6(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie sur le pourcours de la section:
C la d�formation est PROPORTIONNELLE A CONTRAINTE)^(1.5)
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C le depot a lieu dans le lit actif uniquement.
C-----------------------------------------------------------------------
C D�p�t de s�diments dans le lit actif
C Entr�e: I,JRH,RH,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C RH,rayon hydraulique
C JRH, pente energie*rayon hydraulique
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
C hypoth�se: contrainte de de de mouvement est �gale � la contrainte de mise en mouvement

      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      LOGICAL TRASED,CDCHAR,CGEOM
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J
      DOUBLE PRECISION MBADEP,MRESTE,DDEP,SDEP,ZBA,MBA,DBA,SBA,ZABS
     &  ,DZ,DM,SPARTICIP,PARTICIP(NCMAX)
C     :,TFAC !TFAC contrainte de fin de mouvement
     &  ,CONTRAINTE,ROGRAVJ,RH,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,TN,EPSS,DTN,TNP1
C      DOUBLE PRECISION B1,B2,C ! deux variables utiliser pour calculer min(TC/TO)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER CHOIXC
      DOUBLE PRECISION COEFC,PUIdepot ! GAMA est un coefficient que j'utilise pour que le terme (contraintre critique-GAMA*contrainte) soit toujours strictement positif
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &    ,PSOUE(LNCMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
C     :,TC(LNCMAX)
c     :,CK(LNCMAX)
     &,TO(LNCMAX)
     : ,DZMAX
     :,JRH(LMAX),KS1(LMAX)
      LOGICAL PROCHE
      EXTERNAL PROCHE
      DOUBLE PRECISION TOCMM,SHIELDS
      EXTERNAL TOCMM,SHIELDS

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC ! si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/Pdepot/PUIdepot
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/EPSS/EPSS
      COMMON/SOLIDE/JRH,KS1

C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      CONTRAINTE=ROGRAV*JRH(I)
C on fait toujours un seul passage = la variable encore pas utilisee
c       DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau si MPC: sont mis dans PSOUE(JSOUE)
C si choixc = 2 (contrainte uniforme RO*GRAV*RH*J)on recherche les points que dans le lit actif: sont mis dans DEFORM(JDEF)
C on suppose que le dep�t s'effecutera sur des points � l'int�rieur du lit actif (limites exclues).
C si MPC est choisie, on recherche, apr�s calcul de contrainte,les points situ�s dans le lit actif
      IF(YINTER(I).GT.EPSY) THEN
       IF(CHOIXC.NE.2) THEN
C        J=XNCMOAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section : debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
C        DO WHILE(J.LT.XNCMOAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau
         IF(J.GT.XNCMMAG(I))THEN
           IF(J.LT.XNCMMAD(I))THEN
             JDEF=JDEF+1
             DEFORM(JDEF)=J
           ENDIF
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
       ELSEIF(CHOIXC.EQ.2)THEN
         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
         JSOUE=0
        DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
      ENDIF
      ELSE
      JSOUEMAX=0
      JDEFMAX=0
      ENDIF
C ap
C si bief sec on ne doit pas venir dans ce sous programme
      IF(JDEFMAX.LE.1)THEN
         CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c         ENCORE=.FALSE.
C      ENDIF
C plusieurs points sous l'eau
C mais si contrainte faible, appel a depot uniforme
        ELSEIf(contrainte.lt.eps)THEN
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
C plusieurs points sous l'eau
       ELSE
C      IF(JSOUEMAX.GT.3)THEN
      IF(CHOIXC.NE.2) THEN
      ROGRAVJ=CONTRAINTE/RH      ! calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
C      CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC) ! pour tous les points de la section I situ�s sous l'eau
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
            TO(J)=TOMPC(J)
         ELSE
            TO(J)=COEFC*CONTRAINTE !si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
         ENDIF
      ENDDO

       ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
       ENDDO
      ENDIF

C calcul de la r�partition du depot en fonction de (Gama*contrainte)^(PUI)

      MRESTE=MBADEP
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
c        TFAC=TC(J) ! TFAC ici est la contrainte de fin de mouvement, suppos�e �gale � la contrainte de mise en mouvement
c      IF(TC(J).EQ.0.)THEN !pas de d�pot sur les points ou la contrainte critique est nulle (i.e paroies verticales, points instables)
c        PARTICIP(JDEF)=0.
c      ELSE
          PARTICIP(JDEF)=XDYA(J)*TO(J)**PUIDEPOT
C fin du if sur tc(j) nul
c      ENDIF
      SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO

      IF(SPARTICIP.GT.EPSS)THEN
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
                IF(PARTICIP(JDEF).LT.EPS)THEN
                     dm=0.
                  dz=0.
                ELSE
          DM=MBADEP*PARTICIP(JDEF)/SPARTICIP
C          DZ=(DM*POR1/ROS)/(DXMAIL(I)*XDYA(J))
C modif du 24 avril 2003 par Kamal pour ne pas faire de mixage si DM=0
C rappel: DM=0 signifie que le point ne subit pas de d�formation, donc pas besoin de faire le mixage
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
C AP
C          IF(DM.EQ.0.) GOTO 300
          IF(DM.GT.0.)THEN
          DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
C limitation de dz � la hauteur d'eau
          ZBA=XZCSP1(J,1)+DZ
          IF(ZBA.GT.ZABS)THEN
            DZ=ZABS-XZCSP1(J,1)
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF(DZ.GT.DZMAX)THEN
c            write(*,*)'fort depot',' section ',I,' temps ',TN
            DZ=DZMAX
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsec6mba',mba
                 mba=0.
              endif
            CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)

            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
C creation d une nouvelle couche si pas encore au nombre maximal
C de couches et sediemnst pas proches
C ou si le nombre de couches est 1
            ZBA=XZCSP1(J,1)+DZ
c        IF(XNBCSP1(J).GT.1)THEN
c             DZ=ZBA-XZCSp1(j,1)
c              IF(ABS(DM-DZ*ROS*XDYA(J)*DXMAIL(I)/POR1).GT.EPS)THEn
c               write(*,*)'cre',j,dm,dz
c              endif
c         endif
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
            ENDIF
C AP
C fin du if sur dm=0
          ENDIF
C fin du if sur sparticip=0
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MRESTE=MRESTE-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C fin du if sur sparticip nul
      ENDIF
CAP inutile      IF(JDEFMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
C      IF(MRESTE.LE.EPSM*MBADEP) ENCORE=.FALSE.
      MBADEP=MRESTE
C AP      IF(MBADEP.LE.EPS**2.) ENCORE=.FALSE.
C AP sparticip nul ne doit jamais arriver
C      IF(SPARTICIP.LE.0.) ENCORE=.FALSE.
      IF(SPARTICIP.LE.0.)THEN
           Write(*,*)'erreur: depot 6 impossible, intermaille ', I
           write(*,*)'abscisse  ',xtmail(i)
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
c             stop
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
c      IF(SPARTICIP.LE.EPSS)THEN
c           ENCORE=.FALSE.

C      ENDIF
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
C      ENCORE=.FALSE.
C      ENDDO
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE DEPSEC7(I,MBADEP,DDEP,SDEP,DZMAX)
C VERSION DE DEPSEC AVEC DEPOT r�partie sur le pourtour de la section:
C la d�formation est PROPORTIONNELLE A L'ECART (CONTRAINTE CRITIQUE-CONTRAINTE CRITIQUE)^(PUI)
c PUI d�pend de la formule de capacit� solide: =1.( si MPM ou englund, =1 si Bagnold
C ou donne par utilisateur
C la contrainte est calcul�e soit par ROGRAVJRH, soit par MPC
C la contrainte critique est calcul�e pour la couche ACTIVE soit par Shields soit par CK*Shields.
C le depot a lieu dans les le lit actif uniquement.
C-----------------------------------------------------------------------
C D�p�t de s�diments dans le lit actif
C Entr�e: I,JRH,RH,MBADEP,DDEP,SDEP
C Modifie: g�om�trie abscisse-cote et s�diments � l'intermaille dans /GEOP1/
C-----------------------------------------------------------------------
C I num�ro de l'intermaille � modifier
C RH,rayon hydraulique
C JRH, pente energie*rayon hydraulique
C MBADEP masse apport�e au compartiment Ba >0
C DDEP,SDEP caract�ristiques des s�diments d�pos�s
C ZABS cote absolue de la surface libre
C TO contrainte effective
C TC contrainte critique
C CK coefficent IKEDA
C hypoth�se: contrainte de de de mouvement est �gale � la contrainte de mise en mouvement

      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NCMAX=1000)
c      LOGICAL ENCORE
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J,J1
      DOUBLE PRECISION MBADEP,MRESTE,DDEP,SDEP,ZBA,MBA,DBA,SBA,ZABS
     &  ,DZ,DM,SPARTICIP,PARTICIP(NCMAX),TFAC !TFAC contrainte de fin de mouvement
     &  ,CONTRAINTE,ROGRAVJ,RH,TBA
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,TN,DTN,TNP1
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER CHOIXC
      LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION COEFC,PUIdepot,EPSS
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &    ,PSOUE(LNCMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &,TO(LNCMAX)
     : ,DZMAX
     :,JRH(LMAX),KS1(LMAX)
      DOUBLE PRECISION TOCMM,SHIELDS

      LOGICAL PROCHE
      EXTERNAL PROCHE
      EXTERNAL TOCMM,SHIELDS

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC ! si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/PDEPOT/PUIdepot
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/EPSS/EPSS
      COMMON/SOLIDE/JRH,KS1

      IF(JRH(I).LT.EPSS)THEN
            CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
            RETURN
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
c      ENCORE=.TRUE.
      RH=RHINTER(I)
      ZABS=XCTDF(I)+YINTER(I)
      CONTRAINTE=ROGRAV*JRH(I)

C on fait toujours un seul passage = la variable encore pas utilisee
c       DO WHILE(ENCORE)
C Recherche du [des] point[s] situ�s sous la surface d'eau si MPC: sont mis dans PSOUE(JSOUE)
C si choixc = 2 (contrainte uniforme RO*GRAV*RH*J)on recherche les points que dans le lit actif: sont mis dans DEFORM(JDEF)
C on suppose que le dep�t s'effecutera sur des points � l'int�rieur du lit actif (limites exclues).
C si MPC est choisie, on recherche, apr�s calcul de contrainte,les points situ�s dans le lit actif
      IF(YINTER(I).GT.EPSY) THEN
      IF(CHOIXC.NE.2) THEN
C        J=XNCMOAG(I)+1
C on recherche tous les points sous l'eau a l'exclusion des extremites section : debordement pas pris en compte
         J=XNC(I-1)+2
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
C        DO WHILE(J.LT.XNCMOAD(I))
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau
         IF(J.GT.XNCMMAG(I))THEN
           IF(J.LT.XNCMMAD(I))THEN
             JDEF=JDEF+1
             DEFORM(JDEF)=J
           ENDIF
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
      ELSEIF(CHOIXC.EQ.2)THEN
         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
         JSOUE=0
        DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
      ENDIF
      ELSE
      JSOUEMAX=0
      JDEFMAX=0
      ENDIF

C kamal 2005
C si bief sec ou peu de points sous la surface d'eau on ne doit pas venir dans ce sous programme
      IF(JDEFMAX.LE.1)THEN
c       Write(*,*)'peu de points sous la surface d eau, intermaille ', I
c       write(*,*)'nombre de points  ',JSOUEMAX
c       print*,'Tn+1/2=',0.5*(TNP1+TN)
       CALL DEPSEC(I,MBADEP,DDEP,SDEP,DZMAX)
c       ENCORE=.FALSE.
C      ENDIF
C plusieurs points sous l'eau
C mais si contrainte faible, appel a depot uniforme
        ELSEIf(contrainte.lt.eps)THEN
         CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
C plusieurs points sous l'eau
       ELSE
C      IF(JSOUEMAX.GT.3)THEN
      IF(CHOIXC.NE.2) THEN
      ROGRAVJ=CONTRAINTE/RH      ! calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
C      CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC) ! pour tous les points de la section I situ�s sous l'eau
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
            TO(J)=TOMPC(J)
         ELSE
            TO(J)=COEFC*CONTRAINTE !si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
         ENDIF
      ENDDO

        ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
       ENDDO
      ENDIF
      IF(TCPENTE) THEN
C calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)  ! pour tous les points de la section I situ�s sous l'eau
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*SHIELDS(DDEP,JRH(I),RH) ! s�diments couche active seront depos�s
        ENDDO
      ELSE
       DO JDEF=1,JDEFMAX
       J=DEFORM(JDEF)
       TC(J)=SHIELDS(DDEP,JRH(I),RH)
       ENDDO
      ENDIF


C calcul de la r�partition du depot en fonction de (contraintre -contrainte critique)^(1.5)

      MRESTE=MBADEP
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TFAC=TC(J) ! TFAC ici est la contrainte de fin de mouvement, suppos�e �gale � la contrainte de mise en mouvement
      IF(TFAC.ge.TO(J))THEN
C      IF(TC(J).EQ.0.)THEN !pas de d�pot sur les points ou la contrainte critique est nulle (i.e paroies verticales, points instables)
        PARTICIP(JDEF)=0.
      ELSE
          PARTICIP(JDEF)=XDYA(J)*(TO(J)-tfac)**PUIDEPOT
C fin du if sur tfac/to(j)
      ENDIF
      SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO
C si contrainte trop faible on depose a epaisseur constante
      IF(SPARTICIP.LE.EPSS) THEN
      SPARTICIP=0.
      DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
         PARTICIP(JDEF)=XDYA(J)
         SPARTICIP=SPARTICIP+PARTICIP(JDEF)
      ENDDO
C fin du if sur sparticip =0
          ENDIF
      IF(SPARTICIP.GT.EPSS)THEN
C Application de ce d�p�t
C Si la granulo incidente est "proche" de la granulo d�j� en place (et que la strate sup�rieure n'est pas la seule),
C on l'int�gre � la strate sup�rieure du lit; sinon on d�pose les s�diemnts incidents dans une nouvelle strate.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          MBA=XMCSP1(J,1)
          DBA=XDCSP1(J,1)
          SBA=XSCSP1(J,1)
          TBA=XTMCSP1(J,1)
                IF(PARTICIP(JDEF).LT.EPS)THEN
                     dm=0.
                  dz=0.
                ELSE
          DM=MBADEP*PARTICIP(JDEF)/SPARTICIP
C          DZ=(DM*POR1/ROS)/(DXMAIL(I)*XDYA(J))
C modif du 24 avril 2003 par Kamal pour ne pas faire de mixage si DM=0
C rappel: DM=0 signifie que le point ne subit pas de d�formation, donc pas besoin de faire le mixage
C modif du 20 juin pour que mixage si CSMAX atteint
C         IF(PROCHE(MBA,DBA,SBA,DM,DDEP,SDEP).AND.XNBCSP1(J).GT.1)THEN
C AP

          IF(DM.GT.0.)THEN
          DZ=DM*POR1/(ROS*DXMAIL(I)*XDYA(J))
C limitation de dz � la hauteur d'eau
          ZBA=XZCSP1(J,1)+DZ
          IF(ZBA.GT.ZABS)THEN
            DZ=ZABS-XZCSP1(J,1)
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF(DZ.GT.DZMAX)THEN
c            write(*,*)'fort depot',' section ',I,' temps ',TN
            DZ=DZMAX
            DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c            ENCORE=.FALSE.
          ENDIF
          IF((PROCHE(DBA,SBA,TBA,DDEP,SDEP).AND.XNBCSP1(J).GT.1)
     :.OR.(XNBCSP1(J).EQ. CSMAX))  THEN
              IF(MBA.LT.0.)THEN
                  write(*,*)'depsec4mba',mba
c                   pause
                 mba=0.
              endif
            CALL MIXAGE(MBA,DBA,SBA,DM,DDEP,SDEP)
            XZCSP1(J,1)=XZCSP1(J,1)+DZ
            XMCSP1(J,1)=MBA
            XDCSP1(J,1)=DBA
            XSCSP1(J,1)=SBA
          ELSE
C creation d une nouvelle couche si pas encore au nombre maximal
C de couches et sediemnst pas proches
C ou si le nombre de couches est 1
            ZBA=XZCSP1(J,1)+DZ
            CALL CREEBA1(J,ZBA,DM,DDEP,SDEP)
            ENDIF
C AP
C 300      CONTINUE
C fin du if sur dm=0
          ENDIF
C fin du if sur sparticip=0
          ENDIF
          XZCOUP1(J)=XZCSP1(J,1)
          MRESTE=MRESTE-DM
        ENDDO

C Fin du traitement avec �ventuellement un passage suppl�mentaire
C fin du if sur sparticip nul
      ENDIF
CAP inutile      IF(JDEFMAX.EQ.0) ENCORE=.FALSE.                               ! Pas de d�p�t
C on fait toujours un seul passage = la variable encore pas utilisee
C      IF(MRESTE.LE.EPSM*MBADEP) ENCORE=.FALSE.
      MBADEP=MRESTE
C AP      IF(MBADEP.LE.EPS**2.) ENCORE=.FALSE.
C AP sparticip nul ne doit jamais arriver
C      IF(SPARTICIP.LE.0.) ENCORE=.FALSE.
      IF(SPARTICIP.LE.0.)THEN
           Write(*,*)'erreur: depot 7 impossible, intermaille ', I
           write(*,*)'abscisse  ',xtmail(i)
       CALL DEPSEC2(I,MBADEP,DDEP,SDEP,DZMAX)
c           pause
c          stop
      ENDIF
      ENDIF
C on fait toujours un seul passage = la variable encore pas utilisee
C      ENCORE=.FALSE.
C      ENDDO
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE CREEBA1(J,Z,M,D,S)
C-----------------------------------------------------------------------
C Cr�e un nouveau compartiment de base sup�rieur
C-----------------------------------------------------------------------
C J num�ro du point
C Z,M,D,S cote du to�t, masse et caract�ristiques du nouveau compartiment
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10)
      INTEGER J,NUMCS
      DOUBLE PRECISION Z,M,D,S
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
          logical proche
          double precision cdispdepo

      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
          common/cdispdepo/cdispdepo

          EXTERNAL Proche

C      Write(*,*)'Creation d''un nouveau compartiment sedimentaire J=',J
C if inutile apres modif de depsec du 20 juin
      IF(XNBCSP1(J).GE.CSMAX)THEN
        WRITE(*,*)'CREEBA1: point ',J,' dernier compartiment perdu'
                RETURN
C si les deux couches sous jacentes sont proches
C et celle du dessus fine(epaisseur<cdispdepo*d84) on les melange
C on pourrait ici utiliser optmac (epaisseur <optmac*d84)
      elseIF(M.LT.0.)THEN
        WRITE(*,*)'CREEBA1: point ',J,' masse couche negative',M
                m=0.
                RETURN
      ELSEIF(XNBCSP1(J).GT.2)then
            IF(PROCHE(XDCSP1(J,1),XSCSP1(J,1),XTMCSP1(J,2),
     :XDCSP1(J,2),XSCSP1(J,2)).AND.(xzcsp1(j,1)-xzcsp1(j,2)).LT.
     :cdispdepo*XDCSP1(J,1)*XSCSP1(J,1))THEN
C on melange
         xzcsp1(j,2)=XZCSP1(J,1)
         XTMCSP1(J,2)=0.
c melanges des compartiments 1 et 2 dans 2
                 call mixage(XMCSP1(J,2),XDCSP1(J,2),XSCSP1(J,2),
     :XMCSP1(J,1),XDCSP1(J,1),XSCSP1(J,1))
         XZCSP1(J,1)=Z
C supprime car mis au debut de la subroutine
c         If (m.lt.0.)then
c          write(*,*)'creeba', m
c         endif
         XMCSP1(J,1)=M
         XDCSP1(J,1)=D
         XSCSP1(J,1)=S
         XTMCSP1(J,1)=0.
         RETURN
C on ne melange pas
        ELSE
          XNBCSP1(J)=XNBCSP1(J)+1
        ENDIF
C cas ou on a une ou deux couches
      ELSE
        XNBCSP1(J)=XNBCSP1(J)+1
      ENDIF
      DO NUMCS=XNBCSP1(J),2,-1
        XZCSP1(J,NUMCS)=XZCSP1(J,NUMCS-1)
        XMCSP1(J,NUMCS)=XMCSP1(J,NUMCS-1)
        XDCSP1(J,NUMCS)=XDCSP1(J,NUMCS-1)
        XSCSP1(J,NUMCS)=XSCSP1(J,NUMCS-1)
        XTMCSP1(J,NUMCS)=XTMCSP1(J,NUMCS-1)
      ENDDO
      XZCSP1(J,1)=Z
C supprime car mis au debut de la subroutine
c      If (m.lt.0.)then
c          write(*,*)'creeba', m
c      endif
      XMCSP1(J,1)=M
      XDCSP1(J,1)=D
      XSCSP1(J,1)=S
      XTMCSP1(J,1)=0.

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE SUPPBA1(J)
C-----------------------------------------------------------------------
C Supprime le compartiment de base sup�rieur (appel � partir de EROSEC)
C-----------------------------------------------------------------------
C J num�ro du point
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10)
      INTEGER J,NUMCS
C ,I,DM,DZ
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX)
c      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
c      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
c      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
c      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1

      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
c      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
c      COMMON/XGEOMT/XTMAIL,XCTDF
c      COMMON/XLGEO/DXMAIL,XDYA
c      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

C      Write(*,*)'Suppression d''un compartiment sedimentaire J=',J
      IF(XNBCSP1(J).GT.1)THEN
        XNBCSP1(J)=XNBCSP1(J)-1
        DO NUMCS=1,XNBCSP1(J)
          XZCSP1(J,NUMCS)=XZCSP1(J,NUMCS+1)
          XMCSP1(J,NUMCS)=XMCSP1(J,NUMCS+1)
          XDCSP1(J,NUMCS)=XDCSP1(J,NUMCS+1)
          XSCSP1(J,NUMCS)=XSCSP1(J,NUMCS+1)
          XTMCSP1(J,NUMCS)=XTMCSP1(J,NUMCS+1)
        ENDDO
c        IF(XNBCSP1(J).GT.1)THEN
c             DZ=XZCSp1(j,1)-XZCSp1(j,2)
c              DM=DZ*ROS*XDYA(J)*DXMAIL(I)/POR1
c              IF(ABS(DM-XMCSp1(J,1)).GT.EPS)THEn
c               write(*,*)'sup',j,dm,xmcsp1(j,1)
c              endif
c         endif
      ELSE
        WRITE(*,*)'SUPPBA1: UN SEUL COMPARTIMENT AU POINT J=',J
        STOP
      ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE MIXAGE(M1,D1,S1,M2,D2,S2)
C-----------------------------------------------------------------------
C Mixe les s�diments des compartiments 1 et 2
C et met le r�sultat dans le compartiment 1
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION M1,D1,S1,M2,D2,S2
      DOUBLE PRECISION M,D,S
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

C Contr�le des entr�es
C-----------------------------------------------------------------------
C Passage de param�tres non physiques
c      IF((M1.LT.0.).OR.(M2.LT.0.).OR.(D2.LE.0.).OR.(S2.LT.1.)
c     &   .OR.(D1.LE.0.).OR.(S1.LT.1.))THEN
c        if(d1.GT.100..OR.D2.GT.100.)THEN
c         write(*,*)'mixage1',m1,d1,s1,m2,d2,s2
c         endif
        IF(M1.LT.0.)THEN
         IF(M1.LT.-EPSM)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           M1=0.
C           STOP
         ELSE
           M1=0.
         ENDIF
       ENDIF
       IF(M2.LT.0.)THEN
         IF(M2.LT.-EPSM)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           M2=0.
C           STOP
         ELSE
           M2=0.
         ENDIF
       ENDIF
C Compartiment 2 n�gligeable (mais on ne perd pas sa masse)
      IF(M2.LE.0.01*M1)THEN
        M1=M1+M2
        RETURN
      ENDIF
C Compartiment 1 n�gligeable (mais on ne perd pas sa masse)
      IF(M1.LE.0.01*M2)THEN
        M1=M1+M2
        D1=D2
        S1=S2
        RETURN
      ENDIF
       IF(D1.LT.0.)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           STOP
       ENDIF
       IF(D2.LT.0.)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           STOP
       ENDIF
       IF(S1.LT.1.)THEN
         IF(S1.LT.1.-EPS)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           STOP
         ELSE
           S1=1.
         ENDIF
       ENDIF
       IF(S2.LT.1.)THEN
         IF(S2.LT.1.-EPS)THEN
           WRITE(*,*)' '
           WRITE(*,*)'PROBLEME lors de l''appel a MIXAGE('
     &    ,M1,',',D1,',',S1,',',M2,',',D2,',',S2,')'
           WRITE(*,*)' '
           STOP
         ELSE
           S2=1.
         ENDIF
       ENDIF

C Calcul du compartiment mix�
C-----------------------------------------------------------------------
CC Moyenne arithm�tique: solution de base, provisoire
C      M=M1+M2
C      D=(D1*M1+D2*M2)/(M1+M2)
C      S=(S1*M1+S2*M2)/(M1+M2)
CC Moyenne g�om�trique corrig�e: attention, non associatif
C      M=M1+M2
C      D=(D1**(M1/M))*(D2**(M2/M))
C      IF(D1.GT.D2)THEN
C        S=(S1**(M1/M))*(S2**(M2/M))*((D1/D2)**(1.0*SQRT(M1*M2)/M))
C      ELSE
C        S=(S1**(M1/M))*(S2**(M2/M))*((D2/D1)**(1.0*SQRT(M1*M2)/M))
C      ENDIF

C Moyenne g�om�trique simple: approximation au premier ordre
      M=M1+M2
C      D=(D1**(M1/M))*(D2**(M2/M))
C      S=(S1**(M1/M))*(S2**(M2/M))
C modif pour eviter deux exponentielles
      D=D1*(D2/D1)**(M2/M)
      S=S1*(S2/S1)**(M2/M)
c      IF(D*S.gt.D1*S1)THEN
c         S=d1*s1/d
c      endif
c      IF(D*S.gt.D2*S2)THEN
c         S=d2*s2/d
c      endif
c      IF(S.gt.D*s1/d1)THEN
c         S=d*s1/d1
c      endif
c     IF(S.gt.D*s2/d2)THEN
c         S=d*s2/d2
c      endif
      If(S.lt.1.)then
         s=1.
      endif

C Affectation au compartiment 1
C-----------------------------------------------------------------------
      M1=M
      D1=D
      S1=S

C 100  CONTINUE
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE DEMIXAGE(M,D,S,M1,D1,S1,M2,D2,S2,DX)
C-----------------------------------------------------------------------
C D�mixe les s�diments des compartiments 1 et 2:
C on rentre avec les maasses des deux compartiments
C et les autres carcteristiques identisques et egales a celles du tout
C on sort les carcteristiques des deux compartiments
C VERSION du 03/01/05
C modif du 23/01/12
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER DEMIX
      DOUBLE PRECISION M1,D1,S1,M2,D2,S2,s20
      DOUBLE PRECISION DX,a,a2,b,b2
C compartiment d'origine
      DOUBLE PRECISION M,D,S
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION DCHARD,DCHARS
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/DCHARSED/DCHARD,DCHARS
      COMMON/DEMIX/DEMIX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
c       write(*,*)'demixage1',m,d,s,dx

C Contr�le des entr�es
C-----------------------------------------------------------------------
       IF(M.GT.EPSM)THEN
        IF(S.LT.1.)then
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur S lors de l''appel a DEMIXAGE'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
          S=1.000000
        endif
        IF(D.LT.EPS)then
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur D lors de l''appel a DEMIXAGE'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
          D=0.000001
        endif
        elseIF(M.LT.-EPSM)THEN
        WRITE(*,*)' '
        WRITE(*,*)'PROBLEME sur M lors de l''appel a DEMIXAGE'
     &    ,'masse ',M,' diametre ',D,' etendue ',S
        WRITE(*,*)' '
c        PAUSE
        STOP
C cas ou masse proche de 0
        ELSE
        IF(S.LT.1.)then
          S=1.000000
        endif
        IF(D.LT.EPSY)then
          D=EPSY
        endif
        IF(M.lT.0.)THEN
          M=0.
          M1=0.
          M2=0.
          D2=D
          S2=S
          D1=D
          S1=S
          RETURN
	  ENDIF
C fin if sur M
        ENDIF
      IF(M1.LT.0.)THEN
        IF(M1.LT.-EPSM)THEN
        WRITE(*,*)'PROBLEME sur M1 lors de l''appel a DEMIXAGE'
     &    ,M,D,S,' masse 1 ',M1,' masse 2 ',M2
        WRITE(*,*)' '
c        PAUSE
        STOP
        ELSE
          M1=0.
          M2=M
          D2=D
          S2=S
          D1=D
          S1=S
          RETURN
        ENDIF
C fin du if sur m1
              ENDIF
      IF(M2.LT.0.)THEN
        IF(M2.LT.-EPSM)THEN
        WRITE(*,*)'PROBLEME sur M2 lors de l''appel a DEMIXAGE'
     &    ,M,D,S,' masse 1 ',M1,' masse 2 ',M2
        WRITE(*,*)' '
c        PAUSE
        STOP
        ELSE
          M2=0.
          M1=M
          D2=D
          S2=S
          D1=D
          S1=S
          RETURN
        ENDIF
C fin du if sur m2
              ENDIF
C DEMIX=0, diam�tre identique pour les deux compartiments 1 et 2
      IF(DEMIX.EQ.0)THEN
        D1=D
        S1=S
        D2=D
        S2=S
      ELSEIF(DEMIX.EQ.1)THEN
c cas ou la masse du compartiment M2 est n�gligeable devant la masse de M1
        IF(M2.LE.EPSY*M1) THEN
          D1=D
          S1=S
c on suppose que le compartiment 2 garde les caract�ristiques du compartiment d'origine
c c'est uniquement pour des raisons num�riques, la masse M2 est nulle
          D2=D
          S2=S
c cas ou la masse du compartiment M1 est n�gligeable devant la masse de M2
        ELSEIF(M1.LE.EPSY*M2) THEN
          D2=D
          S2=S
c on suppose que le compartiment 1 garde les caract�ristiques du compartiment d'origine
c c'est uniquement pour des raisons num�riques, la masse M1 est nulle
          D1=D
          S1=S
c cas ou le materiau est deja uniforme
        ELSEIF(S.LE.1.+EPSY) THEN
          D2=D
          S2=S
          D1=D
          S1=S
C on traite le cas ou les deux masses ne sont pas nulles
        ELSE
c les coefficients Dchard et DcharS ne doivent pas �tre petits
        IF(Dchard.LE.dx)THEN
          a=(S-1.)/S*M2/M
          a2=(S-1.)/S*M1/M
        else
          a=DX/Dchard*(S-1.)/S*M2/M
          a2=DX/Dchard*(S-1.)/S*M1/M
        endif
        IF(Dchars.LE.dx)THEN
          b=(S-1.)/S*M2/M
          b2=(S-1.)/S*M1/M
        else
          b=DX/Dchars*(S-1.)/S*M2/M
          b2=DX/Dchars*(S-1.)/S*M1/M
        endif

c        IF(Dchard.LE.0.)THEN
c          WRITE(*,*)'Attention, Dchard est nul'
c          PAUSE
c          STOP
c        ENDIF
c        IF(DcharS.LE.0.)THEN
c          WRITE(*,*)'Attention, DcharS est nul'
c          PAUSE
c          STOP
c        ENDIF
C le passage de la forme exponentielle a la forme lineaire est mise � 0,1 (5% erreur)
        If(A.lt.0.1)then
C        If(A.lt.0.32)then
          d1=d*(1.-A)
        else
          d1=d*exp(-A)
        endif
        If(A2.lt.0.1)then
C        If(A2.lt.0.32)then
          d2=d*(1.+A2)
        else
          d2=d*exp(A2)
        endif
C on inverse s grand pour D petit
C modif du 23/01/12
C on a s petit pour D petit donc meme formule
         If(B.lt.0.1)then
C         If(B.lt.0.32)then
            S1=s*(1.-B)
          else
            s1=s*exp(-B)
          endif
C on inverse s petit pour D grand
C modif du 23/1/12 :s grand pour D grand
          If(B2.lt.0.1)then
            s2=s*(1.+B2)
          else
            s2=s*exp(B2)
          endif
C a priori d1 et S1 plus petits que d et S donc inutile
c       if(d1*s1.gt.d*s)then
c            s1=(d*s)/d1
cC d1 inferieur a D donc S1 superieur a S
cc            if (s1.lt.1.)then
cc               d1=d*s
cc               s1=1.
cc            endif
C ajout du 23/1/12
        if(s1.lt.1.)then
            s1=1.
        endif
C fin du if sur masses trop petites
      endif
C fin du if sur demix=0
      endif

C04      IF((D2.LE.0.).OR.(S2.LT.1.).OR.(D1.LE.0.).OR.(S1.LT.1.))THEN
C04        WRITE(*,*)' '
C04        WRITE(*,*)'PROBLEME sur D ou S en sortie de DEMIXAGE'
C04     &    ,M,D,S,M1,D1,S1,M2,D2,S2
C04        WRITE(*,*)' '
C04        STOP
C04      ENDIF


      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE RECENV

C-----------------------------------------------------------------------
C Recherche en chaque centremaille les YMAX,VMAX,QMAX ainsi que des temps correspondants
C Recherche en chaque intermaille les ZMAX,ZFMAX,ZFMIN ainsi que des temps correspondants
C-----------------------------------------------------------------------
C Entr�es: TN,YN1D,VN,QN,YINTER,XCTDF
C Sorties: YMAX,VMAX,QMAX,TYMAX,TVMAX,TQMAX
C          ZMAX,ZFMAX,ZFMIN,TZMAX,TZFMAX,TZFMIN
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,NBB,ib
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION YMAX(LMAX),VMAX(LMAX),QMAX(LMAX)
     &  ,TYMAX(LMAX),TVMAX(LMAX),TQMAX(LMAX)
      DOUBLE PRECISION ZMAX(LMAX),ZFMAX(LMAX),ZFMIN(LMAX)
     &  ,TZMAX(LMAX),TZFMAX(LMAX),TZFMIN(LMAX)
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION YN1D(LMAX),VN(LMAX),RHN(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/PARMAX/YMAX,VMAX,QMAX,TYMAX,TVMAX,TQMAX
      COMMON/PARSMAX/ZMAX,ZFMAX,ZFMIN,TZMAX,TZFMAX,TZFMIN
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
         COMMON/NBIEF/NBB

C Recherche des enveloppes aux centremailles
C-----------------------------------------------------------------------
      DO I=1,LM(NBB)
        IF(YN1D(I).GT.YMAX(I))THEN
          TYMAX(I)=TN
          YMAX(I)=YN1D(I)
        ENDIF
        IF(VN(I).GT.VMAX(I).AND.YN1D(I).GT.0.1)THEN
          TVMAX(I)=TN
          VMAX(I)=VN(I)
        ENDIF
        IF(QN(I).GT.QMAX(I))THEN
          TQMAX(I)=TN
          QMAX(I)=QN(I)
        ENDIF
      ENDDO

C Recherche des enveloppes aux intermailles
C-----------------------------------------------------------------------
        DO ib=1,nbb
        DO I=lm(ib-1)+1,Lm(ib)-1
        IF(YINTER(I)+XCTDF(I).GT.ZMAX(I))THEN
          TZMAX(I)=TN
          ZMAX(I)=YINTER(I)+XCTDF(I)
        ENDIF
        IF(XCTDF(I).GT.ZFMAX(I))THEN
          TZFMAX(I)=TN
          ZFMAX(I)=XCTDF(I)
        ENDIF
        IF(XCTDF(I).LT.ZFMIN(I))THEN
          TZFMIN(I)=TN
          ZFMIN(I)=XCTDF(I)
        ENDIF
      ENDDO
         ENDDO

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE RECFRONT
C-----------------------------------------------------------------------
C Recherche les temps d'arriv�e des trois fronts de d�bits
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBHYPR,NBMAX,NBB,ib
      PARAMETER(LMAX=3000,NBHYPR=100,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I,J,JQ1(nbmax),JQ2(nbmax),JQ3(nbmax)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION TFDEB1(LMAX),TFDEB2(LMAX),TFDEB3(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/MAILTN/SN,QN
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/TFDEB/TFDEB1,TFDEB2,TFDEB3
         COMMON/NBIEF/NBB

      SAVE JQ1,JQ2,JQ3
      DATA JQ1,JQ2,JQ3/nbmax*1,nbmax*1,nbmax*1/

         do ib=1,nbb
C JQn, est le num�ro de la premi�re maille o� le front n n'est pas arriv�
      J=JQ1(ib)-1
      DO I=JQ1(ib),LM(ib)
        IF(QN(I).GT.FDEB1)THEN
          IF(TFDEB1(I).LT.EPS) TFDEB1(I)=TN
        ELSE
          IF(J.LT.JQ1(ib)) J=I
        ENDIF
      ENDDO
      IF(J.GE.JQ1(ib)) JQ1(ib)=J

      J=JQ2(ib)-1
      DO I=JQ2(ib),LM(iB)
        IF(QN(I).GT.FDEB2)THEN
          IF(TFDEB2(I).LT.EPS) TFDEB2(I)=TN
        ELSE
          IF(J.LT.JQ2(ib)) J=I
        ENDIF
      ENDDO
      IF(J.GE.JQ2(ib)) JQ2(ib)=J

      J=JQ3(ib)-1
      DO I=JQ3(ib),LM(iB)
        IF(QN(I).GT.FDEB3)THEN
          IF(TFDEB3(I).LT.EPS) TFDEB3(I)=TN
        ELSE
          IF(J.LT.JQ3(ib)) J=I
        ENDIF
      ENDDO
      IF(J.GE.JQ3(ib)) JQ3(ib)=J
c fin boucle sur bief
      enddo
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE SORTIDS(TN)
C-----------------------------------------------------------------------
C Sortie de fichiers de sauvegarde et de visualisation
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION TN
      CHARACTER NOMFIC*40,ETUDE*20
      LOGICAL TRASED,CDCHAR,CGEOM
c      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

      COMMON/NOMETU/ETUDE
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM

C Fichiers de test 'TestCTDF','TestXLARCOT','TestLARCOT'
C      CALL ECRTEST

C Fichier 'geomac.etude': g�om�trie
      NOMFIC='geomac.'//ETUDE
      CALL ECRGEOMAC(TN,NOMFIC)

C Fichier 'condhyd.etude': variables hydrauliques
      NOMFIC='condhyd.'//ETUDE
      CALL ECRCONDHYD(TN,NOMFIC)

C Fichier 'condsol.etude': variables s�dimentaires en mouvement
      IF(TRASED)THEN
        NOMFIC='condsol.'//ETUDE
        CALL ECRCONDSOL(TN,NOMFIC)
      ENDIF

C Fichier 'visu' de suivi de variables
c     NOMFIC='visu'
c      CALL ECRVISU(TN,NOMFIC)

C Fichier 'visu.dxf'
C      CALL ECRVISUDXF(TN)

C Fichier 'visu.wrl': affichage VRML
c      CALL ECRVISUVRML(TN)

C Affichage de confirmation
      WRITE(*,'(A,F16.3,A)')' Mise a jour a Tn=',TN,' s'

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE SAVEFH(DTA)
C-----------------------------------------------------------------------
C Sauvegarde des hydrogrammes et des lignes d'eau au cours du temps
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBHYPR
      PARAMETER(NBHYPR=100)
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
      CHARACTER TF(10)*1
C      CHARACTER NOMFIC*40
      DOUBLE PRECISION DTA
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION TN,DTN,TNP1
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/TSTFIL/TF

C Sauvegarde pour les abscisses fix�es dans 'abshyd.etude' au pas de temps PSAVE
C-----------------------------------------------------------------------
      IF(TF(2).EQ.'O')THEN
      IF((TN.EQ.tinit1D).OR.(INT(TN/PSAVE).GT.INT((TN-DTA)/PSAVE)))THEN

C �criture dans 'hydlim.etude'
          CALL ECRHYDLIM(TN,54)

C �criture dans 'hydlims.etude'
          IF(TRASED)THEN
            CALL ECRHYDLIMS(TN,50)
          ENDIF

        ENDIF
      ENDIF

C Sauvegarde des valeurs � pas de temps constant DTSAUV
C-----------------------------------------------------------------------
      IF(TF(5).EQ.'O')THEN
       IF((TN.EQ.tinit1D)
     :.OR.(INT(TN/DTSAUV).GT.INT((TN-DTA)/DTSAUV)))THEN

C �criture dans 'lindo.etude'
C transforme en ecriture dans profil
C �criture dans 'profil.etude'
                            CALL ECRPROFIL(TN,42,'        ')

C �criture dans 'profils.etude'
            IF(TRASED)THEN
               CALL ECRPROFILS(TN,41,'        ')
            ENDIF

        ENDIF
      ENDIF


      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE SAVEVISU(TN)
C-----------------------------------------------------------------------
C Sauvegarde aux temps de tnprof
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBHYPR
      PARAMETER(NBHYPR=100)
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR),DEFOND
c        :,ITSAV1
      CHARACTER TF(10)*1
      CHARACTER NOMFIC*40
C      DOUBLE PRECISION DTA
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE,TN
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
         CHARACTER*20 ETUDE
         logical volqouv

      COMMON/NOMETU/ETUDE
      COMMON/DEFOND/DEFOND
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
c      COMMON/TREEL/TN,DTN,TNP1
      COMMON/TSTFIL/TF
c      COMMON/ITSAV1/ITSAV1    ! initialis� a 1 dans le programme principal
      common/volqouv/volqouv

C Sauvegarde pour les temps fix�s dans 'tnprof.etude'
C-----------------------------------------------------------------------
c      IF((TF(4).EQ.'O').OR.(TF(7).EQ.'O'))THEN
        IF(ITSAV.LE.NTSOR)THEN
          IF(TN.GE.TSOR(ITSAV))THEN

C �criture dans 'profil.etude'
            IF(TF(4).EQ.'O') Then
                            CALL ECRPROFIL(TN,42,'  tnprof')

C �criture dans 'profils.etude'
            IF(TRASED)THEN
               CALL ECRPROFILS(TN,41,'  tnprof')
            ENDIF
C fin du if sur tf(4)
                     endif

         IF(TF(7).EQ.'O')THEN
                  if(volqouv)then
                       call ecrvolqouv(TN,35)
                     endif
c            IF(TRASED)THEN
c                     if(defond.ne.0)THEN
C �criture dans 'visu-ITSAV'
            IF(ITSAV.LE.9)THEN
              WRITE(NOMFIC,'(A5,I1,A1,A20)')'visu-',ITSAV,'.',ETUDE
            ELSEIF(ITSAV.GT.99)THEN
              WRITE(NOMFIC,'(A5,I3,A1,A20)')'visu-',ITSAV,'.',ETUDE
            ELSE
             WRITE(NOMFIC,'(A5,I2,A1,A20)')'visu-',ITSAV,'.',ETUDE
            ENDIF
            CALL ECRVISU(TN,NOMFIC)
            IF(TRASED)THEN
c                     if(defond.ne.0)THEN
C �criture dans 'TC-ITSAV' donne les valeurs de contraintes critiques en tout point du lit
            IF(ITSAV.LE.9)THEN
             WRITE(NOMFIC,'(A3,I1,A1,A20)')'TC-',ITSAV,'.',ETUDE
            ELSEIF(ITSAV.GT.99)THEN
              WRITE(NOMFIC,'(A3,I3,A1,A20)')'TC-',ITSAV,'.',ETUDE
           ELSE
              WRITE(NOMFIC,'(A3,I2,A1,A20)')'TC-',ITSAV,'.',ETUDE
            ENDIF
            CALL ECRTC(TN,NOMFIC)
C �criture dans 'TO-ITSAV'  donne les valeurs de contraintes hydrodYNamiques
C par MPC en tout point du lit
            IF(ITSAV.LE.9)THEN
              WRITE(NOMFIC,'(A3,I1,A1,A20)')'TO-',ITSAV,'.',ETUDE
            ELSEIF(ITSAV.GT.99)THEN
              WRITE(NOMFIC,'(A3,I3,A1,A20)')'TO-',ITSAV,'.',ETUDE
            ELSE
             WRITE(NOMFIC,'(A3,I2,A1,A20)')'TO-',ITSAV,'.',ETUDE
           ENDIF
            CALL ECRTO(TN,NOMFIC)
C �criture dans 'geomac-ITSAV'
                     if(defond.ne.0)THEN
            IF(ITSAV.LE.9)THEN
              WRITE(NOMFIC,'(A7,I1,A1,A20)')'geomac-',ITSAV,'.',ETUDE
            ELSEIF(ITSAV.GT.99)THEN
              WRITE(NOMFIC,'(A7,I3,A1,A20)')'geomac-',ITSAV,'.',ETUDE
            ELSE
              WRITE(NOMFIC,'(A7,I2,A1,A20)')'geomac-',ITSAV,'.',ETUDE
            ENDIF
            CALL ECRGEOMAC(TN,NOMFIC)
C fin du if sur defond
            ENDIF
C fin du if sur trased
            ENDIF

C �criture dans 'largeur.etude'
             CALL ECRLARGEUR(TN,43)
C fin du if sur tf(7)
         ENDIF
C Passage � l'it�ration suivante
            ITSAV=ITSAV+1
          ENDIF
        ENDIF
C      ENDIF

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE ECRENV
C-----------------------------------------------------------------------
C Sauvegarde des enveloppes et des temps d'arriv�e des fronts � la fin de la simulation
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBHYPR
      PARAMETER(NBHYPR=100)
      CHARACTER ETUDE*20,NOMFIC*40
      CHARACTER TF(10)*1
c      DOUBLE PRECISION DTA
c      DOUBLE PRECISION TN,DTN,TNP1
c      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
c      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
c     &  ,DELIMAI(NBHYPR)
      LOGICAL TRASED,CDCHAR,CGEOM
c      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

c      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NOMETU/ETUDE
      COMMON/TSTFIL/TF
c      COMMON/TREEL/TN,DTN,TNP1
c      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
c      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI

C ecriture pour sauuvegarde reguliere, pour reprise ou temps final
c      IF((INT(TN/DTSAUV).GT.INT((TN-DTA)/DTSAUV))
c     &    .OR.(((TS.NE.0.).AND.(TS.GT.(TN-DTN)).AND.(TS.LE.TN))
c     &    .OR.(TN.GE.tmax1D)))THEN
C Traitement des enveloppes
C-----------------------------------------------------------------------
      IF(TF(3).EQ.'O')THEN
          NOMFIC='envlop.'//ETUDE
          CALL ECRENVLOP(NOMFIC)
          if(trased)then
             NOMFIC='envlops.'//ETUDE
             CALL ECRENVLOPS(NOMFIC)
          endif
      ENDIF

C Traitement des temps d'arriv�e des fronts de d�bits
C-----------------------------------------------------------------------
      IF(TF(6).EQ.'O')THEN
C        IF(((TS.NE.0.).AND.(TS.GT.(TN-DTN)).AND.(TS.LE.TN))
C     &    .OR.(TN.GE.tmax1D))THEN
          NOMFIC='trajec.'//ETUDE
          CALL ECRTRAJEC(NOMFIC)
      ENDIF
C fin du if sur temps
c      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE SAUVRE(TS)
C-----------------------------------------------------------------------
C Sauvegardes au temps TS pour une reprise ult�rieure
C G�om�trie, variables hydrauliques (format identique � 'condin.etude'), solides en mouvement (format identique � 'condins.etude')
C-----------------------------------------------------------------------
      IMPLICIT NONE
      CHARACTER*40 NOMFIC,ETUDE*20
         INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION TS
      DOUBLE PRECISION tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      INTEGER LM(0:NBMAX),LL,NBB,I,DEFOND
      LOGICAL XMODSEC(LMAX),MODSEC
      LOGICAL TRASED,CDCHAR,CGEOM,reprise
C      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

C      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NOMETU/ETUDE
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DEFOND/DEFOND
      COMMON/MODIFSEC/XMODSEC,MODSEC
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
C modif du 18/10/2021 pour eviter passage dans sauvre plusieurs fois
      DATA reprise/.TRUE./
      save reprise

      IF(.NOT.reprise)then
         return
      else
         reprise=.FALSE.
C Fichier 'tpss.etude': variables s�dimentaires en mouvement
      IF(TRASED)THEN
              IF(DEFOND.EQ.0)THEN
                 IF(ABS(tmax1D-TS).GT.EPS)THEN
      write(*,*)'reprise demandee avant temps final'
      write(*,*)'incoherence : la geometrie est modifiee a',TS
                 ENDIF
                 DO I=1,LM(NBB)
                   XMODSEC(I)=.TRUE.
                 ENDDO
                 CALL TRACLC
           CALL INCMLC
           CALL ACTVARHYD
C fin du if sur defond
              ENDIF

        NOMFIC='tpss.'//ETUDE
        CALL ECRCONDSOL(TS,NOMFIC)
      ENDIF
C Fichier 'visu-r': variables de visualisation
        NOMFIC='visu-r.'//ETUDE
        CALL ECRVISU(TS,NOMFIC)
C      ENDIF

C Fichier 'tps.etude': variables hydrauliques
      NOMFIC='tps.'//ETUDE
      CALL ECRCONDHYD(TS,NOMFIC)
C Fichier 'geomac-r.etude': g�om�trie lit (topo. + compo. s�d.)
        NOMFIC='geomac-r.'//ETUDE
        CALL ECRGEOMAC(TS,NOMFIC)


C Affichage de confirmation
      WRITE(*,'(A,A,F15.3,A)')' Sauvegarde pour reprise eventuelle '

     &  ,'a Tn=',TS,' s'
C fin du if sur reprise
      endif
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRTEST
C-----------------------------------------------------------------------
C �crit les fichiers de test du maillage et de la g�om�trie (utiles pour d�bug)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX),NBB
c      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      CHARACTER NOMFIC*40,ETUDE*20
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)

      COMMON/PHYS/LM,LL
      COMMON/NOMETU/ETUDE
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
C      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
         COMMON/NBIEF/NBB

C Fichier de test du maillage
C----------------------------
      NOMFIC='TestCTDF.'//ETUDE
      OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(56,*)'Centremaille           | Intermaille'
      WRITE(56,'(F9.4,1X,F6.2,1X,F7.3,A,F9.4,1X,F6.2)')
     &  TMAIL(1),CTDF(1),PEN(1),'| ',XTMAIL(1),XCTDF(1)
      DO I=2,LL-1
        WRITE(56,'(F9.4,1X,F6.2,1X,F7.3,A)') TMAIL(I),CTDF(I),PEN(I)
     &    ,'| '
        WRITE(56,'(A,F9.4,1X,F6.2)') '                        | '
     &    ,XTMAIL(I),XCTDF(I)
      ENDDO
      WRITE(56,'(F9.4,1X,F6.2,1X,F7.3,A)') TMAIL(LL),CTDF(LL),PEN(LL)
     &  ,'| '
      WRITE(56,'(F9.4,1X,F6.2,1X,F7.3,A,F9.4,1X,F6.2)') TMAIL(LM(nBB))
     &  ,CTDF(LM(nbb)),PEN(LM(nbb)),'| ',XTMAIL(LL),XCTDF(LL)
      CLOSE(56)

C Fichier de test de la g�om�trie largeur-cote aux intermailles
C--------------------------------------------------------------
      NOMFIC='TestXLARCOT.'//ETUDE
      OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
      DO I=1,LL
        WRITE(56,'(I5,I5)') XNC(I),XNCMO(I)
        DO J=XNC(I-1)+1,XNC(I)
          WRITE(56,'(I3,6F12.5)')
     &     J,XLISEC(J),XYISEC(J),XLYSEC(J),XSECUM(J),XPICUM(J),XPECUM(J)
        ENDDO
      ENDDO
      CLOSE(56)

C Fichier de test de la g�om�trie largeur-cote aux centremailles
C---------------------------------------------------------------
      NOMFIC='TestLARCOT.'//ETUDE
      OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
      DO I=1,LL+1
        WRITE(56,'(I5,I5)') NC(I),NCMO(I)
        DO J=NC(I-1)+1,NC(I)
          WRITE(56,'(I3,5F12.5)')
     &      J,LISEC(J),YISEC(J),LYSEC(J),SECUM(J),PECUM(J)
        ENDDO
      ENDDO
      CLOSE(56)

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRHYDLIM(TN,NFIC)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'hydlim.etude'
C Q,V,H � une s�rie d'abscisses fix�es dans 'abshyd.etude' � pas de temps r�gulier PSAVE
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBHYPR,LNCMAX,NBMAX
      PARAMETER(LMAX=3000,NBHYPR=100,LNCMAX=130000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER I,N
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION Q(NBHYPR),H(NBHYPR),V(NBHYPR)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)

      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/DDNARF/NREFA

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          QN(I)=QN(I-1)
          VN(I)=VN(I-1)
          YN1D(I)=YN1D(I-1)+CTDF(I-1)-CTDF(I)
        ENDIF
      ENDDO

C Calcul des variables aux points souhait�s
C-----------------------------------------------------------------------
      DO N=1,NBSSAV
        I=NBMAIL(N)
              IF(I.NE.0)THEN
          Q(N)=QN(I)+(QN(I+1)-QN(I))*DELMAI(N)
          V(N)=VN(I)+(VN(I+1)-VN(I))*DELMAI(N)
          H(N)=YN1D(I)+(YN1D(I+1)-YN1D(I))*DELMAI(N)
              ELSE
             Q(N)=0.
             V(N)=0.
                H(N)=0.
           ENDIF
C        CT(N)=CTDF(I)+(CTDF(I+1)-CTDF(I))*DELMAI(N)
      ENDDO

C �criture dans l'unit� NFIC au format du fichier 'hydlim.etude'
C-----------------------------------------------------------------------
      WRITE(NFIC,*)TN
      WRITE(NFIC,101)(Q(N),N=1,NBSSAV)
      WRITE(NFIC,102)(V(N),N=1,NBSSAV)
      WRITE(NFIC,102)(H(N),N=1,NBSSAV)
C      WRITE(NFIC,102)(CT(N),N=1,NBSSAV)

      RETURN
 101  FORMAT(20F16.6)
 102  FORMAT(20F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRHYDLIMS(TN,NFIC)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'hydlims.etude'
C Zf[centremailles],Dact,Sact � une s�rie d'abscisses fix�es dans 'abshyd.etude' � pas de temps r�gulier PSAVE
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBHYPR,LNCMAX,NBMAX
      PARAMETER(LMAX=3000,NBHYPR=100,LNCMAX=130000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER I,N
      INTEGER NBSSAV,NTSOR,ITSAV,NBMAIL(NBHYPR)
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION CT(NBHYPR),D(NBHYPR),S(NBHYPR)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF


      COMMON/PHYS/LM,LL
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/NSORC/NBSSAV,NTSOR,NBMAIL,ITSAV
      COMMON/DDNARF/NREFA
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF

C Calcul des variables aux points souhait�s
C-----------------------------------------------------------------------
      DO N=1,NBSSAV
        I=NBMAIL(N)
              IF(I.NE.0)THEN
        CT(N)=CTDF(I)+(CTDF(I+1)-CTDF(I))*DELMAI(N)
C logigique suite � la modif pour etude Ain
        D(N)=DMOB(I)+(DMOB(I+1)-DMOB(I))*DELMAI(N)
C        D(N)=DACT(I)+(DACT(I+1)-DACT(I))*DELIMAI(N)
C modif pour etude Ain
c        S(N)=SACT(I)+(SACT(I+1)-SACT(I))*DELIMAI(N)
C qsr et Qn aux centres de maille
        IF(UNISOL.EQ.2)THEN
          IF(ABS(QN(I+1)).GT.0.001.AND.ABS(QN(I)).GT.0.001)THEN
            S(N)=QSR(I)/QN(I)+(QSR(I+1)/QN(I+1)-QSR(I)/QN(I))*DELMAI(N)
          ELSE
            S(N)=0.
          ENDIF
C cas unisol=1
        ELSE
          S(N)=QSR(I)+(QSR(I+1)-QSR(I))*DELMAI(N)
        ENDIF
C cas ou on a choisi une abscisse hors domaine
         ELSE
             CT(N)=0.
             D(N)=0.
                S(N)=0.
           ENDIF
c fin boucle sur n
      ENDDO

C �criture dans l'unit� NFIC au format du fichier 'hydlims.etude'
C-----------------------------------------------------------------------
      WRITE(NFIC,*)TN
      WRITE(NFIC,102)(CT(N),N=1,NBSSAV)
      WRITE(NFIC,101)(D(N),N=1,NBSSAV)
      WRITE(NFIC,101)(S(N),N=1,NBSSAV)

      RETURN
 101  FORMAT(20F16.6)
 102  FORMAT(20F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRPROFIL(TN,NFIC,CHAINE)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'profil.etude'
C Q,V,H,Zf aux centremailles � une s�rie de temps fix�s dans 'tnprof.etude'
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
         CHARACTER*8 Chaine

      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/DDNARF/NREFA

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          YN1D(I)=YN1D(I-1)+CTDF(I-1)-CTDF(I)
          VN(I)=VN(I-1)
          QN(I)=QN(I-1)
        ENDIF
      ENDDO

C �criture dans l'unit� NFIC au format du fichier 'profil.etude'
C-----------------------------------------------------------------------
      WRITE(NFIC,*)TN,CHAINE
      DO I=1,LL+1
        WRITE(NFIC,103)YN1D(I),VN(I),QN(I),CTDF(I)
      ENDDO

      RETURN
 103  FORMAT(F16.4,F16.4,F16.6,F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRPROFILS(TN,NFIC,CHAINE)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'profils.etude'
C Qs,Dmob,Smob aux centremailles � une s�rie de temps fix�s dans 'tnprof.etude'
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
     :  ,tmax1D,DTSAUV,tinit1D,DT1D,PSAVE
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
         CHARACTER*8 Chaine

      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/DDNARF/NREFA
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/TEMPS/tmax1D,DTSAUV,tinit1D,DT1D,PSAVE

C �criture dans l'unit� NFIC au format du fichier 'profils.etude'
C-----------------------------------------------------------------------
      WRITE(NFIC,*)TN,CHAINE
      IF(TN.EQ.tinit1D)THEN
      DO I=1,LL+1
      WRITE(NFIC,104)99999.000,99999.000,99999.000
      ENDDO
      ELSEIF(UNISOL.EQ.1)THEN
      DO I=1,LL+1
        WRITE(NFIC,104)QSR(I),DMOB(I),SMOB(I)
      ENDDO
C unisol=2 concentrations
      ELSE
      DO I=1,LL+1
        IF(ABS(QN(I)).GT.0.001)THEN
          WRITE(NFIC,104)QSR(I)/QN(I),DMOB(I),SMOB(I)
        ELSE
          WRITE(NFIC,104)0.,DMOB(I),SMOB(I)
        ENDIF
      ENDDO
      ENDIF
      RETURN
 104  FORMAT(F16.4,F16.6,F16.6)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRENVLOP(NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('envlop.etude')
C Vmax,Hmax,Qmax aux centremailles
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      INTEGER NREFA(LMAX)
      CHARACTER NOMFIC*40
      DOUBLE PRECISION YMAX(LMAX),VMAX(LMAX),QMAX(LMAX)
     &  ,TYMAX(LMAX),TVMAX(LMAX),TQMAX(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/PARMAX/YMAX,VMAX,QMAX,TYMAX,TVMAX,TQMAX
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/DDNARF/NREFA

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          TYMAX(I)=TYMAX(I-1)
          YMAX(I)=YMAX(I-1)+CTDF(I-1)-CTDF(I)
          TVMAX(I)=TVMAX(I-1)
          VMAX(I)=VMAX(I-1)
          TQMAX(I)=TQMAX(I-1)
          QMAX(I)=QMAX(I-1)
        ENDIF
      ENDDO

C �criture de NOMFIC au format du fichier 'envlop.etude'
C-----------------------------------------------------------------------
      OPEN(52,FILE=NOMFIC,STATUS='OLD')
      WRITE(52,'(3A)')' CentreMaille T(Hmax)          Hmax   '
     &  ,'      T(Vmax)            Vmax   '
     &  ,'      T(Qmax)            Qmax   '
      DO I=1,LL+1
        WRITE(52,99)I,TYMAX(I),YMAX(I),TVMAX(I),VMAX(I),TQMAX(I),QMAX(I)
      ENDDO
      CLOSE(52)

      RETURN
 99   FORMAT(1X,I4,1X,6F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRENVLOPS(NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('envlops.etude')
C Zmax,Zfmax,Zfmin aux intermailles
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,ib,nbb
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      CHARACTER NOMFIC*40
      DOUBLE PRECISION ZMAX(LMAX),ZFMAX(LMAX),ZFMIN(LMAX)
     &  ,TZMAX(LMAX),TZFMAX(LMAX),TZFMIN(LMAX)

      COMMON/NBIEF/NBB
      COMMON/PHYS/LM,LL
      COMMON/PARSMAX/ZMAX,ZFMAX,ZFMIN,TZMAX,TZFMAX,TZFMIN

C �criture de NOMFIC au format du fichier 'envlops.etude'
C-----------------------------------------------------------------------
      OPEN(49,FILE=NOMFIC,STATUS='OLD')
      WRITE(49,'(3A)')' InterMaille  T(Zmax)          Zmax   '
     &  ,'      T(Zfmax)           Zfmax  '
     &  ,'      T(Zfmin)           Zfmin  '
      DO Ib=1,nbb
      DO I=lm(ib-1)+1,Lm(ib)-1
        WRITE(49,99)I,TZMAX(I),ZMAX(I),TZFMAX(I),ZFMAX(I)
     &    ,TZFMIN(I),ZFMIN(I)
      ENDDO
      ENDDO
      CLOSE(49)

      RETURN
 99   FORMAT(1X,I4,1X,6F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRLINDO(TN,NFIC)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'lindo.etude'
C V,H,Zf aux centremailles � pas de temps r�gulier DTSAUV
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      INTEGER NREFA(LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/DDNARF/NREFA

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          YN1D(I)=YN1D(I-1)+CTDF(I-1)-CTDF(I)
          VN(I)=VN(I-1)
        ENDIF
      ENDDO

C �criture dans l'unit� NFIC au format du fichier 'lindo.etude'
C-----------------------------------------------------------------------
      WRITE(NFIC,105) TN,' YN1D   ','           VN   ','           ZfN'
      DO I=1,LL+1
        WRITE(NFIC,106)I,YN1D(I),VN(I),CTDF(I)
      ENDDO
      RETURN
 105  FORMAT(F16.3,3A)
 106  FORMAT(1X,I4,1X,F16.4,F16.4,F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRTRAJEC(NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('trajec.etude')
C Temps d'arriv�e des trois fronts de d�bits
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBHYPR,NBMAX
      PARAMETER(LMAX=3000,NBHYPR=100,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I
      INTEGER NREFA(LMAX)
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TS,FDEB1,FDEB2,FDEB3,TSOR(NBHYPR),DELMAI(NBHYPR)
     &  ,DELIMAI(NBHYPR)
      DOUBLE PRECISION TFDEB1(LMAX),TFDEB2(LMAX),TFDEB3(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/TFDEB/TFDEB1,TFDEB2,TFDEB3
      COMMON/DDNARF/NREFA

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          TFDEB1(I)=TFDEB1(I-1)
          TFDEB2(I)=TFDEB2(I-1)
          TFDEB3(I)=TFDEB3(I-1)
        ENDIF
      ENDDO

C �criture de NOMFIC au format du fichier 'trajec.etude'
C-----------------------------------------------------------------------
      OPEN(53,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(53,111)' FDEB1=',FDEB1,' FDEB2=',FDEB2,' FDEB3=',FDEB3
      WRITE(53,'(2A)')' CentreMaille   TFDEB1          TFDEB2'
     &  ,'          TFDEB3'
      DO I=1,LL+1
        WRITE(53,112)I,TFDEB1(I),TFDEB2(I),TFDEB3(I)
      ENDDO
      CLOSE(53)

      RETURN
 111  FORMAT(3(A7,F16.3))
 112  FORMAT(1X,I4,1X,3(F16.3))
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRLARGEUR(TN,NFIC)
C-----------------------------------------------------------------------
C Continue l'�criture dans l'unit� NFIC � TN au format du fichier 'largeur.etude'
C Diverses informations, dont le Froude
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER NFIC
      INTEGER LM(0:NBMAX),LL
      INTEGER J,IJ
      INTEGER NREFA(LMAX)
      INTEGER LDETYJ
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION TN,LARG,PMOU,S,RH,CONTRA,FROUDE
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX),SMAJ,SMIN
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX),VMIN,VMAJ,QMIN,QMAJ
      DOUBLE PRECISION SN(LMAX),QN(LMAX),FR1(LMAX),FRLM(LMAX)
      DOUBLE PRECISION DETL,DETSN,DETPM,SMBQ2,DETVMI,DETSMI,DETPMI
      DOUBLE Precision RHMIN,RHMAJ,PMIN,CONTRMIN,CONTRMAJ

      EXTERNAL LDETYJ,DETL,DETPM,DETSN,SMBQ2,DETVMI,DETSMI,DETPMI

      COMMON/PHYS/LM,LL
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/DDNARF/NREFA
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM

C En-t�te
C-----------------------------------------------------------------------
      WRITE(NFIC,*)TN
      J=1
      IF(YN1D(J).GT.EPSY)THEN
        IJ=LDETYJ(0,YN1D(J),J)
        LARG=DETL(0,YN1D(J),IJ)
        S=DETSN(0,YN1D(J),IJ)
        PMOU=DETPM(0,YN1D(J),IJ,J)
        RH=S/PMOU
C La masse volumique est 1000 et la contrainte vaut -ro*smbq2/pmou
        CONTRA=-1000.*SMBQ2(0,VN(J),J,YN1D(J))/PMOU
        FROUDE=VN(J)/SQRT(GRAV*S/LARG)
        VMIN=DETVMI(0,Vn(j),YN1D(j),IJ,J)
        SMIN=DETSMI(0,YN1D(j),IJ,J)
        PMIN=DETPMI(0,YN1D(j),IJ,J)
        RHMIN=SMIN/Pmin
        QMIn=VMIN*smin
        QMAJ=QN(j)-QMIN
        SMAJ=s-smin
        IF(smaj.GT.EPSY)THEN
          vmaj=qmaj/smaj
              rhmaj=SMAJ/(PMOU-PMIN)
          CONTRMaj=1000.*grav*vmaj*2/(rhmaj**0.3333*frlm(j)**2)
        ELSE
          VMAJ=0.
          QMAJ=0.
          rhmaj=0.
                  contrmaj=0.
        ENDIF
c T = rho*g*Rh*j avec j = V�/(Ks�*Rh^(4/3))
       CONTRMIN=1000.*grav*vmin**2/(rhmin**0.3333*fr1(j)**2)
      ELSE
        LARG=0.
        RH=0.
        CONTRA=0.
        FROUDE=0.
        vmin=0.
        vmaj=0.
        qmin=0.
        qmaj=0.
        rhmin=0.
        rhmaj=0.
        contrmin=0.
        contrmaj=0.
      ENDIF
      WRITE(NFIC,107)TMAIL(J),YN1D(J)+CTDF(J),LARG,RH,CONTRA,FROUDE
     :,VMIN,VMAJ,QMIN,QMAJ,FR1(J),FRLM(J),rhmin,rhmaj,contrmin,contrmaj

C Coeur
C-----------------------------------------------------------------------
      DO J=2,LL
        IF(YN1D(J).GT.EPSY)THEN
          IJ=LDETYJ(0,YN1D(J),J)
          LARG=DETL(0,YN1D(J),IJ)
          S=DETSN(0,YN1D(J),IJ)
          PMOU=DETPM(0,YN1D(J),IJ,J)
          RH=S/PMOU
C La masse volumique est 1000 et la contrainte vaut -ro*smbq2/pmou
          CONTRA=-1000.*SMBQ2(0,VN(J),J,YN1D(J))/PMOU
          FROUDE=VN(J)/SQRT(GRAV*S/LARG)
        VMIN=DETVMI(0,Vn(j),YN1D(j),IJ,J)
        SMIN=DETSMI(0,YN1D(j),IJ,J)
          PMIN=DETPMI(0,YN1D(j),IJ,J)
          RHMIN=SMIN/Pmin
        QMIn=VMIN*smin
        QMAJ=QN(j)-QMIN
              SMAJ=s-smin
        IF(smaj.GT.EPSY)THEN
          vmaj=qmaj/smaj
              rhmaj=SMAJ/(PMOU-PMIN)
          CONTRMaj=1000.*grav*vmaj*2/(rhmaj**0.3333*frlm(j)**2)
        ELSE
          VMAJ=0.
          QMAJ=0.
          rhmaj=0.
                  contrmaj=0.
        ENDIF
c T = rho*g*Rh*j avec j = V�/(Ks�*Rh^(4/3))
       CONTRMIN=1000.*grav*vmin**2/(rhmin**0.3333*fr1(j)**2)

        ELSE
          LARG=0.
          RH=0.
          CONTRA=0.
          FROUDE=0.
        vmin=0.
        vmaj=0.
        qmin=0.
        qmaj=0.
        rhmin=0.
        rhmaj=0.
        contrmin=0.
        contrmaj=0.
        ENDIF
        WRITE(NFIC,107)TMAIL(J),YN1D(J)+CTDF(J),LARG,RH,CONTRA,FROUDE
     :,VMIN,VMAJ,QMIN,QMAJ,FR1(J),FRLM(J),rhmin,rhmaj,contrmin,contrmaj
      ENDDO

C Fin
C-----------------------------------------------------------------------
      J=LL+1
      IF(YN1D(J).GT.EPSY)THEN
        IJ=LDETYJ(0,YN1D(J),J)
        LARG=DETL(0,YN1D(J),IJ)
        S=DETSN(0,YN1D(J),IJ)
        PMOU=DETPM(0,YN1D(J),IJ,J)
        RH=S/PMOU
C La masse volumique est 1000 et la contrainte vaut -ro*smbq2/pmou
        CONTRA=-1000.*SMBQ2(0,VN(J),J,YN1D(J))/PMOU
        FROUDE=VN(J)/SQRT(GRAV*S/LARG)
        VMIN=DETVMI(0,Vn(j),YN1D(j),IJ,J)
        SMIN=DETSMI(0,YN1D(j),IJ,J)
        PMIN=DETPMI(0,YN1D(j),IJ,J)
        RHMIN=SMIN/Pmin
        QMIn=VMIN*smin
        QMAJ=QN(j)-QMIN
        SMAJ=s-smin
        IF(smaj.GT.EPSY)THEN
          vmaj=qmaj/smaj
              rhmaj=SMAJ/(PMOU-PMIN)
          CONTRMaj=1000.*grav*vmaj*2/(rhmaj**0.3333*frlm(j)**2)
        ELSE
          VMAJ=0.
          QMAJ=0.
          rhmaj=0.
                  contrmaj=0.
        ENDIF
c T = rho*g*Rh*j avec j = V�/(Ks�*Rh^(4/3))
       CONTRMIN=1000.*grav*vmin**2/(rhmin**0.3333*fr1(j)**2)
      ELSE
        LARG=0.
        RH=0.
        CONTRA=0.
        FROUDE=0.
        vmin=0.
        vmaj=0.
        qmin=0.
        qmaj=0.
        rhmin=0.
        rhmaj=0.
        contrmin=0.
        contrmaj=0.
      ENDIF
      WRITE(NFIC,107)TMAIL(J),YN1D(J)+CTDF(J),LARG,RH,CONTRA,FROUDE
     :,VMIN,VMAJ,QMIN,QMAJ,FR1(J),FRLM(J),rhmin,rhmaj,contrmin,contrmaj

      RETURN
 107  FORMAT(F16.3,15F13.6)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRGEOMAC(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('geomac.etude' ou 'geomac-r.etude') � TN
C G�om�trie
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
      INTEGER I,J,NUMCS,IB,NBB
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      CHARACTER NOMFIC*40,XCCOU*1,FINLIGNE*600
      DOUBLE PRECISION TN
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)

      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/NC/NC,XNC
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS

C      NOMFIC='geomac.'//ETUDE
      OPEN(24,FILE=NOMFIC,STATUS='UNKNOWN',FORM='FORMATTED')
         DO IB=1,NBB
      WRITE(24,'(I4,1X,F15.3)')LM(IB)-LM(IB-1)-1,TN
      DO I=LM(IB-1)+1,LM(IB)-1
        WRITE(24,9991)I-LM(IB-1),XTMAIL(I),XNC(I)-XNC(I-1)
        DO J=XNC(I-1)+1,XNC(I)
          IF(J.EQ.XNCMOAG(I))THEN
            XCCOU='G'
          ELSEIF(J.EQ.XNCMOAD(I))THEN
            XCCOU='D'
          ELSEIF(J.EQ.XNCMMAG(I))THEN
            XCCOU='g'
          ELSEIF(J.EQ.XNCMMAD(I))THEN
            XCCOU='d'
          ELSE
            XCCOU=' '
          ENDIF
C Description sedimentaire point par point
          WRITE(FINLIGNE,9995)(XZCS(J,NUMCS),XDCS(J,NUMCS)
     &      ,XSCS(J,NUMCS),XTMCS(J,NUMCS),NUMCS=1,XNBCS(J))
          WRITE(24,9994)XCCOU,XYCOU(J),FINLIGNE
C fin boucle sur J
        ENDDO
C fin boucle sur I
      ENDDO
C fin boucle sur IB
      ENDDO
      CLOSE(24)
 9991 FORMAT(I4,1X,F11.3,1X,I4)
 9994 FORMAT(A1,1X,F11.5,A600)
 9995 FORMAT(10(F13.5,F15.9,F15.9,F15.5))
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRCONDHYD(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('tps.etude' ou 'condhyd.etude') � TN
C Variables hydrauliques
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I
      INTEGER NREFA(LMAX)
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TN
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/DDNARF/NREFA
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/NBIEF/NBB

C Traitement �ventuel du cas des ouvrages
C-----------------------------------------------------------------------
      DO I=2,LL
        IF(NREFA(I-1).EQ.-2.AND.NREFA(I).EQ.-2)THEN
          YN1D(I)=YN1D(I-1)+CTDF(I-1)-CTDF(I)
          VN(I)=VN(I-1)
        ENDIF
      ENDDO

C �criture de NOMFIC au format du fichier 'condin.etude'
C-----------------------------------------------------------------------
      OPEN(40,FILE=NOMFIC,STATUS='UNKNOWN')
         do ib=1,nbb
      WRITE(40,'(F15.3,A30)')TN,'  NoCM   YN1D(m)      VN(m/s) '
      DO I=lm(ib-1)+1,Lm(ib)
        WRITE(40,121)I,YN1D(I),VN(I)
      ENDDO
         enddo
      CLOSE(40)

      RETURN
 121  FORMAT(1X,I4,1X,F16.4,F16.4)
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRCONDSOL(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('tpss.etude' ou 'condsol.etude') � TN
C Variables solides de la couche active
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,IB,NBB
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TN,XVN,QSACT,CSACT
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/MAILTN/SN,QN
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/NBIEF/NBB

      OPEN(40,FILE=NOMFIC,STATUS='UNKNOWN')
C On sort les solides dans la m�me unit� que les entr�es
      IF(UNISOL.EQ.1)THEN
                do ib=1,nbb
        WRITE(40,'(F15.3,A45)')
     :TN,'  NoIM   Qs(kg/s)      D(m)            S(m/m)'
        DO I=lm(ib-1)+1,Lm(ib)-1
          XVN=VN(I)+(VN(I+1)-VN(I))*(XTMAIL(I)-TMAIL(I))
     &      /(TMAIL(I+1)-TMAIL(I))
          QSACT=MACT(I)*XVN/DXMAIL(I)
          WRITE(40,122)I,QSACT,DACT(I),SACT(I)
        ENDDO
              enddo
      ELSEIF(UNISOL.EQ.2)THEN
                do ib=1,nbb
        WRITE(40,'(F15.3,A45)')
     :TN,'  NoIM   Cs(kg/m3)     D(m)            S(m/m)'
        DO I=lm(ib-1)+1,Lm(ib)-1
          CSACT=MACT(I)/(SN(I)*(TMAIL(I+1)-TMAIL(I))
     &      +(SN(I+1)-SN(I))*(XTMAIL(I)-TMAIL(I)))
          WRITE(40,122)I,CSACT,DACT(I),SACT(I)
        ENDDO
              enddo
      ENDIF
      CLOSE(40)

      RETURN
 122  FORMAT(1X,I4,1X,F16.4,F16.6,F16.6)
      END

C-----------------------------------------------------------------------
      SUBROUTINE ECRVISU(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('visu' ou 'visu-r' ou 'visu-'ITSAV) � TN
C S�lection de variables hydrauliques et s�dimentaires
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX,NBB
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J
      INTEGER LM(0:NBMAX),LL
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TN
      DOUBLE PRECISION SSOL,CS,ZFMOY(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),LACT(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     &  ,TFACT(LMAX),SSOL2(LMAX),XLFON,cotfon(lmax)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY

      COMMON/PHYS/LM,LL
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/MAILTN/SN,QN
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
         COMMON/NBIEF/NBB
         COMMON/NC/NC,XNC
      COMMON/XLACT/LACT
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

C modif du 6 mars 2012 xctdf remplace par cotfon=min (xzcou)

C Calcul de la cote moyenne du fond comprise entre les marques 'g' et 'd'
C-----------------------------------------------------------------------
      DO I=1,LL
        SSOL=0.
                COTFON(I)=xzcou(xncmmad(i)-1)
        DO J=XNCMMAG(I)+1,XNCMMAD(I)-2
          SSOL=SSOL+0.5*(XYCOU(J+1)-XYCOU(J))*(XZCOU(J)+XZCOU(J+1))
                  cotfon(i)=min(cotfon(i),xzcou(j))
        ENDDO
                XLFON=XYCOU(XNCMMAD(I)-1)-XYCOU(XNCMMAG(I)+1)
                IF(XLFON.GT.EPS)THEN
           ZFMOY(I)=SSOL/XLFON
            else
          ZFMOY(I)=XZCOU(XNCMMAG(I))
                ENDIF

      ENDDO

C Format du fichier selon le type d'informations
C-----------------------------------------------------------------------
      IF(TRASED)THEN
C calcul section solide
      DO I=1,LL
        SSOL2(I)=0.
        DO J=XNC(I-1)+1,XNC(I)-1
          SSOL2(i)=SSOL2(i)+0.5*(XYCOU(J+1)-XYCOU(J))*
     :(XZCOU(J)+XZCOU(J+1))
        ENDDO
      ENDDO
        IF(UNISOL.EQ.1)THEN
          GOTO 200
        ELSEIF(UNISOL.EQ.2)THEN
          GOTO 300
        ELSE
          WRITE(*,*)'ECRVISU: PROBLEME POUR visu CAR UNISOL=',UNISOL
          STOP
        ENDIF
      ELSE
        GOTO 100
      ENDIF

C Pas de transport solide
C-----------------------------------------------------------------------
 100  CONTINUE
      OPEN(39,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(39,'(F20.5)')TN
      WRITE(39,'(1X,6A)')'     Absc.IM','  CoteMinFond '
     &  ,' CoteMoyFond  ','    CoteEau   ','     VitLiq   '
     &  ,'    SurfMou   '
      DO I=1,LL
c        WRITE(39,'(1X,F12.2,5F14.6)')XTMAIL(I),XCTDF(I)
c     &    ,ZFMOY(I),YINTER(I)+XCTDF(I),VINTER(I)
        WRITE(39,'(1X,F12.2,5F14.6)')XTMAIL(I),COTFON(I)
     &    ,ZFMOY(I),YINTER(I)+COTFON(I),VINTER(I)
     &    ,SINTER(I)
      ENDDO
      CLOSE(39)
      RETURN

C Calcul avec transport solide, d�bits solides en kg/s
C-----------------------------------------------------------------------
 200  CONTINUE
      OPEN(39,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(39,'(F20.5)')TN
      WRITE(39,'(1X,18A)')'     Absc.IM','  CoteMinFond '
     &  ,'  CoteMoyFond ','    CoteEau   ','     VitLiq   '
     :  ,'    SurfMou   ','   ToR�elle   '
     &  ,'     CapSol   ','    LargAct   ','   DistCharg  '
     &  ,'        KS1   '
     &  ,'       Mact   ','       Dact   ','       Sact   '
     :  ,'       Ssol   '
     &  ,'         Qs   ','       Dmob   ','       Smob   '
      DO I=1,LL
c        WRITE(39,'(1X,F12.2,17F14.6)')XTMAIL(I),XCTDF(I)
c     &    ,ZFMOY(I),YINTER(I)+XCTDF(I),VINTER(I),SINTER(I)
        WRITE(39,'(1X,F12.2,17F14.6)')XTMAIL(I),COTFON(I)
     &    ,ZFMOY(I),YINTER(I)+COTFON(I),VINTER(I),SINTER(I)
     :    ,ROGRAV*JRH(I)
     &    ,CAPSOL(I),LACT(I),DCHAR(I)
     &    ,KS1(I)
     &    ,MACT(I),DACT(I),SACT(I),SSOL2(I)
     &    ,QSR(I),DMOB(I),SMOB(I)
      ENDDO
      WRITE(39,'(209X,3F14.6)')QSR(LM(nbb)),DMOB(LM(nbb)),SMOB(LM(nbb))
      CLOSE(39)
      RETURN

C Calcul avec d�formation, concentrations solides en kg/m3
C-----------------------------------------------------------------------
 300  CONTINUE
      OPEN(39,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(39,'(F15.3)')TN
      WRITE(39,'(1X,18A)')'     Absc.IM','  CoteMinFond '
     &  ,'  CoteMoyFond ','    CoteEau   ','     VitLiq   '
     :  ,'    SurfMou   ','   ToR�elle   '
     &  ,'     CapSol   ','    LargAct   ','   DistCharg  '
     &  ,'        KS1   '
     &  ,'       Mact   ','       Dact   ','       Sact   '
     :  ,'       Ssol   '
     &  ,'         Cs   ','       Dmob   ','       Smob   '
      DO I=1,LL
C Attention, on utilise le d�bit � Tn et non � Tn+1/2 comme le voudrait la logique, car QND non calcul�
        IF(QN(I).NE.0.)THEN
          CS=QSR(I)/QN(I)
        ELSE
          CS=0.
        ENDIF
C        WRITE(39,'(1X,F12.2,17F14.6)')XTMAIL(I),XCTDF(I)
C     &    ,ZFMOY(I),YINTER(I)+XCTDF(I),VINTER(I),SINTER(I)
        WRITE(39,'(1X,F12.2,17F14.6)')XTMAIL(I),COTFON(I)
     &    ,ZFMOY(I),YINTER(I)+COTFON(I),VINTER(I),SINTER(I)
     :    ,ROGRAV*JRH(I)
     &    ,CAPSOL(I),LACT(I),DCHAR(I)
     &    ,KS1(I)
     &    ,MACT(I),DACT(I),SACT(I),SSOL2(I)
     &    ,CS,DMOB(I),SMOB(I)
      ENDDO
      IF(QN(LM(NBB)).NE.0.)THEN
        CS=QSR(LM(NBB))/QN(LM(NBB))
      ELSE
        CS=0.
      ENDIF
      WRITE(39,'(209X,3F14.6)')CS,DMOB(LM(NBB)),SMOB(LM(NBB))
      CLOSE(39)
      WRITE(*,'(A,F20.6,A)')' ecriture visu a Tn=',TN,' s'
c      pause
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE ECRTC(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('TC-'ITSAV) � TN
C sortie ajout�e par kamal septembre 2003 pour voir si le code calcule bien la contrainte critique
C attention: dans ce sous-programmes, on calcule la contrainte critique
C en tout point de la section en travers, m�me ceux situ�s au dessus de la surface d'eau
C dans depsec3 , depsec4 et eorsec ou on calcule la contrainte critique
C au temps tn+1/2 UNIQUMENT pour les points du lit situ�s sous la surface d'eau et dans le lit actif
C -----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J6
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER LM(0:NBMAX),LL
      INTEGER XNBCS(LNCMAX)
      INTEGER JTC,JDEFMAX,DEFORM(LNCMAX)
      LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TN
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX),XZ(LNCMAX,CSMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
      DOUBLE PRECISION TC(LNCMAX),CK(LNCMAX)
C     &      ,TETA(LNCMAX),TETA2(LNCMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)

      DOUBLE PRECISION TOCMM,SHIELDS
      EXTERNAL TOCMM,SHIELDS
      COMMON/PHYS/LM,LL
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/MAILTN/SN,QN
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NC/NC,XNC
      COMMON/DEFORM/DEFORM
      COMMON/PDEFOR/JDEFMAX
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS


C-----------------------------------------------------------------------
      OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(56,'(I4,1X,F12.3)')LL,TN

      DO I=1,LL       ! pour toutes les intermailles I
      WRITE(56,'(1X,3A)')'     Absc.Y   ','  CoteZ   '
     &  ,' TC  '
      DO J6=XNC(I-1)+1,XNC(I)
      XZ(J6,1)=XZCOU(J6) !XZ va utiliser pour l'appel de COEFFICIENTCKIKEDA(XZ,CK)
      ENDDO

      JTC=0
        DO J6=XNC(I-1)+2,XNC(I)-1 ! on exlut les deux points hauts berges car calcul de contrainte en ces deux poins
          JTC=JTC+1             ! n'a pas �t� pr�vu dans le sous programme COEFFICIENTCKIKEDA(I,JDEFMAX,XZ,CK)
          DEFORM(JTC)=J6         ! on s'interesse donc a tous les autres points, pas uniquement ceux sous la surface d'eau
        ENDDO
        JDEFMAX=JTC    !cette valeur est �gale � XNC(I)-XNC(I-1)-2

      IF(TCPENTE) THEN
C calcul de la contrainte critique avec facteur CK
        CALL COEFFICIENTCKIKEDA(XZ,CK)
        DO JTC=1,JDEFMAX
           J6=DEFORM(JTC)
           TC(J6)=CK(J6)*SHIELDS(XDCS(J6,1),JRH(I),RHINTER(I))  !D et S des s�diments de la premi�re couche composant le lit
        ENDDO                               ! ici on calcule la contrainte critique pour les s�didments composant le lit
       ELSE
C cas ou la pente locale n'est pas prise en compte CK=1
       DO J6=XNC(I-1)+2,XNC(I)-1
          TC(J6)=SHIELDS(XDCS(J6,1),JRH(I),RHINTER(I))
      ENDDO
      ENDIF
      WRITE(56,*)I,XTMAIL(I)
      DO J6=XNC(I-1)+2,XNC(I)-1
      WRITE(56,'(1X,4F12.6)')XYCOU(J6),XZCOU(J6),TC(J6)
      ENDDO
      ENDDO
      CLOSE(56)
      RETURN
      END
C-----------------------------------------------------------------------
      SUBROUTINE ECRTO(TN,NOMFIC)
C-----------------------------------------------------------------------
C �crit le fichier NOMFIC ('TO-'ITSAV) � TN
C sortie ajout�e par kamal septembre 2003 pour voir si le code calcule bien la contrainte
C attention: dans ce sous-programme, on calcule la contrainte hydrodYNamique
C en tout point de la section en travers situ� au dessous de la surface d'eau

C -----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER I,J6,J8
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER LM(0:NBMAX),LL
      INTEGER XNBCS(LNCMAX)
      INTEGER CHOIXC
      INTEGER JSOUE,JSOUEMAX,PSOUE(LNCMAX)
      CHARACTER NOMFIC*40
      DOUBLE PRECISION TN
      DOUBLE PRECISION COEFC
      DOUBLE PRECISION ZABS,ROGRAVJ,CONTRAINTE
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX),XZ(LNCMAX,CSMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION JRH(LMAX),QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
     &  ,KS1(LMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
     & ,TO(LNCMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION FR1(LMAX),TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      COMMON/PHYS/LM,LL
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/SOLIDE/JRH,KS1
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/MAILTN/SN,QN
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NC/NC,XNC
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/FROTMT/FR1
      COMMON/CHOIXC/CHOIXC ! si choix = 1, calcul de contrainte par MPC, si choix =2, contrainte par formule r�gime uniforme
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/COEFC/COEFC

C-----------------------------------------------------------------------
      OPEN(56,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(56,'(I4,1X,F12.3)')LL,TN

      WRITE(56,'(1X,3A)')'     Absc.Y   ','  CoteZ   '
     &  ,' contrainte  '
      DO I=1,LL
C pour toutes les intermailles I
      ZABS=XCTDF(I)+YINTER(I)

C     Recherche du [des] point[s] situ�s sous la surface d'eau,
C le[s] points situ�s sous l'eau sont mis dans PSOUE(JSOUE)
      DO J6=XNC(I-1)+1,XNC(I)
         XZ(J6,1)=XZCOU(J6)
      ENDDO
      J8=XNC(I-1)+2 ! on recherche les points situ�s sous l'eau
      JSOUE=0
      DO WHILE(J8.LT.XNC(I))
          IF(XZ(J8,1).GE.ZABS) GOTO 600
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J8
C PSOUE= points sous eau
 600      CONTINUE
          J8=J8+1
        ENDDO
        JSOUEMAX=JSOUE
      IF(JSOUEMAX.GT.0)THEN
      IF(CHOIXC.NE.2) THEN
      CONTRAINTE=RO*GRAV*JRH(I)
      ROGRAVJ=CONTRAINTE/RHINTER(I)

C calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZ,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZ,ROGRAVJ,TOMPC)
         ENDIF
C      CALL CONTRAINTEMPC(I,XZ,ROGRAVJ,TOMPC)
      DO JSOUE=1,JSOUEMAX
        J8=PSOUE(JSOUE)
C if mis en commentaire le 29/11/2010
c        IF(TOMPC(J8).LT.COEFC*CONTRAINTE)THEN
          TO(J8)=TOMPC(J8)
c        ELSE
C si contrainte MPC est sup�rieure � COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
c          TO(J8)=COEFC*CONTRAINTE
c        ENDIF
      ENDDO
      ELSEIF(CHOIXC.EQ.2)THEN
        CONTRAINTE=RO*GRAV*JRH(I)
        DO JSOUE=1,JSOUEMAX
          J8=PSOUE(JSOUE)
          TO(J8)=CONTRAINTE
        ENDDO
      ENDIF
      WRITE(56,*)I,XTMAIL(I)
      DO JSOUE=1,JSOUEMAX
        J8=PSOUE(JSOUE)
        WRITE(56,'(1X,3F12.6)')XYCOU(J8),XZCOU(J8),TO(J8)
      ENDDO
      ELSE
      GOTO 700
      ENDIF

700      CONTINUE
      ENDDO
      CLOSE(56)
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRVISUVRML(TN)
C-----------------------------------------------------------------------
C �crit le fichier 'visu.wrl' de visualisation de la g�om�trie et de la ligne d'eau � TN
C Version avec localisation planim�trique �ventuelle
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J,IBLEU,JMIN
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      INTEGER XNCBINI(LMAX),XNCBDUR(LMAX)
      DOUBLE PRECISION XZINI(LNCMAX),XZDUR(LNCMAX)
     &  ,XPLANI(LNCMAX),YPLANI(LNCMAX)
      LOGICAL TROUVIMAX
      INTEGER OGEOD,OGEOI,OGEOL,OSULIT,OSULIB,OSUMOIR
      INTEGER IMIN,IMAX
      CHARACTER NOMFIC*40,COMMENT1*15,COMMENT2*60
      DOUBLE PRECISION XZSL,Z,TEXTSIZE
      DOUBLE PRECISION XMIN,XMAX,DIRVUE,PTVUEX,PTVUEY,PTVUEZ,LARVUE
      DOUBLE PRECISION XXSULIG(LMAX),XXSULID(LMAX),XYSULIG(LMAX)
     &  ,XYSULID(LMAX),XZSULI(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/XGEOVISU/XNCBINI,XNCBDUR,XZINI,XZDUR
         common/XYPLANI/XPLANI,YPLANI

C R�cup�ration des options de sortie du fichier
C-----------------------------------------------------------------------
C Ces options de config sont relues � chaque fois afin de pouvoir changer
C les param�tres de visualisation en cours de calcul
      NOMFIC='config-visu'
      OPEN(37,FILE=NOMFIC,STATUS='OLD',ERR=100)
      READ(37,*) XMIN
      READ(37,*) XMAX
      READ(37,*) OGEOD
      READ(37,*) OGEOI
      READ(37,*) OGEOL
      READ(37,*) OSULIT
      READ(37,*) OSULIB,OSUMOIR
      READ(37,*) DIRVUE
      READ(37,*) PTVUEX,PTVUEY,PTVUEZ
      READ(37,*) LARVUE
      READ(37,'(A60)',ERR=200) COMMENT2
      IF(XMIN.LT.XTMAIL(1)) XMIN=XTMAIL(1)
      IF(XMAX.GT.XTMAIL(LL)) XMAX=XTMAIL(LL)
      GOTO 300
 100  CONTINUE
      XMIN=XTMAIL(1)
      XMAX=XTMAIL(LL)
      OGEOD=1
      OGEOI=1
      OGEOL=1
      OSULIT=0
      OSULIB=1
      OSUMOIR=1
      DIRVUE=0.00
      PTVUEX=00.
      PTVUEY=10.
      PTVUEZ=00.
      LARVUE=0.35
 200  CONTINUE
      COMMENT2=''
 300  CONTINUE
      WRITE(COMMENT1,'(A,F10.1,A)') 'Tn=',TN,' s'
      CLOSE(37)

C Calcul des intersections
C-----------------------------------------------------------------------
      IMIN=1
      IMAX=LL
      TROUVIMAX=.FALSE.
      DO I=1,LL
        IF(XTMAIL(I).LT.XMIN)THEN
          IMIN=I+1
          GOTO 600
        ENDIF
        IF(XTMAIL(I).GT.XMAX)THEN
          IF(.NOT.TROUVIMAX)THEN
            IMAX=I-1
            TROUVIMAX=.TRUE.
          ENDIF
          GOTO 600
        ENDIF
C XXSULIG(I),XXSULID(I),XYSULIG(I),XYSULID(I) abscisses et ordonn�es
C de la surfacce libre � gauche/droite de la section
C XZSULI(I) cote de la surface libre
        XZSL=YINTER(I)+XCTDF(I)
        XZSULI(I)=XZSL
        IF(XZSL.GE.XZCS(XNC(I-1)+1,1))THEN
          XXSULIG(I)=XPLANI(XNC(I-1)+1)
          XYSULIG(I)=YPLANI(XNC(I-1)+1)
          GOTO 500
        ENDIF
        DO J=XNC(I-1)+2,XNC(I)
          IF(XZSL.GE.XZCS(J,1))THEN
            XXSULIG(I)=XPLANI(J-1)+(XPLANI(J)-XPLANI(J-1))
     &        *(XZSL-XZCS(J-1,1))/(XZCS(J,1)-XZCS(J-1,1))
            XYSULIG(I)=YPLANI(J-1)+(YPLANI(J)-YPLANI(J-1))
     &        *(XZSL-XZCS(J-1,1))/(XZCS(J,1)-XZCS(J-1,1))
            GOTO 500
          ENDIF
        ENDDO
        XXSULIG(I)=XPLANI(XNCBINI(I))
        XYSULIG(I)=YPLANI(XNCBINI(I))
 500    CONTINUE
        IF(XZSL.GE.XZCS(XNC(I),1))THEN
          XXSULID(I)=XPLANI(XNC(I))
          XYSULID(I)=YPLANI(XNC(I))
          GOTO 600
        ENDIF
        DO J=XNC(I)-1,XNC(I-1)+1,-1
          IF(XZSL.GE.XZCS(J,1))THEN
            XXSULID(I)=XPLANI(J)+(XPLANI(J+1)-XPLANI(J))
     &        *(XZSL-XZCS(J,1))/(XZCS(J+1,1)-XZCS(J,1))
            XYSULID(I)=YPLANI(J)+(YPLANI(J+1)-YPLANI(J))
     &        *(XZSL-XZCS(J,1))/(XZCS(J+1,1)-XZCS(J,1))
            GOTO 600
          ENDIF
        ENDDO
        XXSULID(I)=XPLANI(XNCBINI(I))
        XYSULID(I)=YPLANI(XNCBINI(I))
 600    CONTINUE
      ENDDO
      JMIN=XNC(IMIN-1)+1
C      Write(*,*)'IMIN=',IMIN,' XTMAIL(IMIN)=',XTMAIL(IMIN),' XMIN=',XMIN
C      Write(*,*)'IMAX=',IMAX,' XTMAIL(IMAX)=',XTMAIL(IMAX),' XMAX=',XMAX

C �criture de 'visu.wrl' au format VRML V2.0
C-----------------------------------------------------------------------
C En-t�te et points de vue
      I=INT(0.5*(IMIN+IMAX))
      NOMFIC='visu.wrl'
      OPEN(36,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(36,'(A)')'#VRML V2.0 utf8'
      WRITE(36,'(A)')'NavigationInfo {type "FLY" , headlight TRUE}'
      WRITE(36,'(A,A)')'PointLight {ambientIntensity 0.8, '
C Eclairage vertical (Soleil au z�nith)
     &  ,'location 0 1000 0}'
      WRITE(36,'(A,A)')'Background {skyColor [1.0 1.0 1.0]}'
C Fond ambiant tout blanc (d�faut=noir)
      WRITE(36,'(A,F4.2,A,A,F5.2,A,F14.4,1X,F14.4,1X,F14.4,A)')
C Point de vue par d�faut, choisi par l'utilisateur
     &  'Viewpoint {fieldOfView ',LARVUE,', jump TRUE, '
     &  ,'orientation 0 1 0 ',DIRVUE,', position ',PTVUEX,PTVUEZ
     &  ,-PTVUEY,', description "Vue personalis�e"}'
      WRITE(36,'(A,A,F14.4,1X,F14.4,1X,F14.4,A)')
C Point de vue depuis l'amont
     &  'Viewpoint {fieldOfView 0.35, jump TRUE, '
     &  ,'orientation 0 1 0 -1.57, position ',XPLANI(XNC(IMIN))
     &  -0.5*(XPLANI(XNC(IMAX))-XPLANI(XNC(IMIN))),XZINI(XNCBINI(IMIN))
     &  ,-(0.5*(YPLANI(XNC(IMIN-1)+1)+YPLANI(XNC(IMIN))))
     &  ,', description "Vue de l''amont"}'
      WRITE(36,'(A,A,F14.4,1X,F14.4,1X,F14.4,A)')
C Point de vue style profil en long
     &  'Viewpoint {fieldOfView 0.50, jump TRUE, orientation 0 1 0 '
     &  ,' 0.00, position ',XPLANI(XNC(I)),XZINI(XNCBINI(I))
     &  ,-(YPLANI(XNC(I))+1.0*(YPLANI(XNC(I))-YPLANI(XNC(I-1)+1)))
     &  ,', description "Vue de cot� droit"}'
      TEXTSIZE=0.05*(XZINI(XNC(IMIN))-XZINI(XNCBDUR(IMIN)))
      IF(TEXTSIZE.LT.0.001) TEXTSIZE=1.

C D�finition des collections de points
      WRITE(36,'(A)')'# D�finition de collection de points lit courant'
      WRITE(36,'(A)')'DEF lit_courant Coordinate {point ['
      DO I=IMIN,IMAX
        DO J=XNC(I-1)+1,XNC(I)
          WRITE(36,'(F14.4,1X,F14.4,1X,F14.4)')
     &      XPLANI(J),XZCS(J,1),-YPLANI(J)
        ENDDO
      ENDDO
      WRITE(36,'(A)')']}'
      WRITE(36,'(A)')'# D�finition de collection de points lit initial'
      WRITE(36,'(A)')'DEF lit_initial Coordinate {point ['
      DO I=IMIN,IMAX
        DO J=XNC(I-1)+1,XNC(I)
          WRITE(36,'(F14.4,1X,F14.4,1X,F14.4)')
     &      XPLANI(J),XZINI(J),-YPLANI(J)
        ENDDO
      ENDDO
      WRITE(36,'(A)')']}'
      WRITE(36,'(A)')'# D�finition de collection de points du fond dur'
      WRITE(36,'(A)')'DEF geom_dure Coordinate {point ['
      DO I=IMIN,IMAX
        DO J=XNC(I-1)+1,XNC(I)
          WRITE(36,'(F14.4,1X,F14.4,1X,F14.4)')
     &      XPLANI(J),XZDUR(J),-YPLANI(J)
        ENDDO
      ENDDO
      WRITE(36,'(A)')']}'
      WRITE(36,'(A)')'# D�finition de collection de points surf. libre'
      WRITE(36,'(A)')'DEF surface_libre Coordinate {point ['
      DO I=IMIN,IMAX
        Z=XZSULI(I)
        WRITE(36,'(F14.4,1X,F14.4,1X,F14.4)')XXSULIG(I),Z,-XYSULIG(I)
        WRITE(36,'(F14.4,1X,F14.4,1X,F14.4)')XXSULID(I),Z,-XYSULID(I)
      ENDDO
      WRITE(36,'(A)')']}'

C Surface du lit courant
      IF(OSULIT.EQ.1)THEN
        WRITE(36,'(A)')'# Surface du lit courant'
        WRITE(36,'(A,A)')'Shape{appearance Appearance{material Material'
     &    ,' {emissiveColor  0.8 0.8 0.0, transparency 0.8}}'
        WRITE(36,'(A,A)')'  geometry IndexedFaceSet {solid FALSE, '
     &    ,'coord USE lit_courant, coordIndex ['
        JMIN=XNC(IMIN-1)+1
        DO I=IMIN,IMAX-1
          WRITE(36,*)(J-JMIN,J=XNC(I-1)+1,XNC(I))
     &      ,(J-JMIN,J=XNC(I+1),XNC(I)+1,-1),-1
        ENDDO
        WRITE(36,'(A)')']}}'
      ENDIF

C G�om�trie du lit
      IF(OGEOL.EQ.1)THEN
        WRITE(36,'(A)')'# G�om�trie du lit courant'
        WRITE(36,'(A,A)')'Shape {appearance Appearance {material '
     &    ,'Material {emissiveColor  0.8 0.8 0.0}}'
        WRITE(36,'(A,A)')'  geometry IndexedLineSet {'
     &    ,'coord USE lit_courant, coordIndex ['
        DO I=IMIN,IMAX
          WRITE(36,*)(J-JMIN,J=XNC(I-1)+1,XNC(I)),-1
        ENDDO
        WRITE(36,'(A)')']}}'
      ENDIF

C G�om�trie initiale
      IF(OGEOI.EQ.1)THEN
        WRITE(36,'(A)')'# G�om�trie initiale'
        WRITE(36,'(A,A)')'Shape {appearance Appearance {material '
     &    ,'Material {emissiveColor  0.5 0.7 0.5}}'
        WRITE(36,'(A,A)')'  geometry IndexedLineSet {'
     &    ,'coord USE lit_initial, coordIndex ['
        WRITE(36,'(A)')'# G�om�trie initiale - sections'
        DO I=IMIN,IMAX
          WRITE(36,*)(J-JMIN,J=XNC(I-1)+1,XNC(I)),-1
        ENDDO
        WRITE(36,'(A)')'# G�om�trie initiale - lignes directrices'
        WRITE(36,*)(XNCMOAG(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,*)(XNCBINI(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,*)(XNCMOAD(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,'(A)')']}}'
        WRITE(36,'(A)')'# G�om�trie initiale - texte abscisses en long'
        DO I=IMIN,IMAX
          WRITE(36,'(A,F14.4,1X,F14.4,1X,F14.4)')
     &      'Transform {rotation 0 1 0 -1.57, translation '
     &      ,XPLANI(XNCBINI(I)),XZINI(XNCBINI(I))-2.*TEXTSIZE
     &      ,-YPLANI(XNCBINI(I))
          WRITE(36,'(A,A)')'  children [ Shape {appearance Appearance {'
     &      ,'material Material {diffuseColor  0.5 0.7 0.5}}'
          WRITE(36,'(A,A,F4.1,A,F12.2,A)')'    geometry Text {'
     &      ,'fontStyle FontStyle {justify "MIDDLE", size ',TEXTSIZE
     &      ,'}, string ["',XTMAIL(I),'"]}'
          WRITE(36,'(A)')'}]}'
        ENDDO
      ENDIF

C G�om�trie dure
      IF(OGEOD.EQ.1)THEN
        WRITE(36,'(A)')'# G�om�trie dure'
        WRITE(36,'(A,A)')'Shape {appearance Appearance {material '
     &    ,'Material {emissiveColor  0.7 0.5 0.5}}'
        WRITE(36,'(A,A)')'  geometry IndexedLineSet {'
     &    ,'coord USE geom_dure, coordIndex ['
        WRITE(36,'(A)')'# G�om�trie dure - sections'
        DO I=IMIN,IMAX
          WRITE(36,*)(J-JMIN,J=XNC(I-1)+1,XNC(I)),-1
        ENDDO
        WRITE(36,'(A)')'# G�om�trie dure - lignes directrices'
        WRITE(36,*)(XNCMOAG(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,*)(XNCBDUR(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,*)(XNCMOAD(I)-JMIN,I=IMIN,IMAX),-1
        WRITE(36,'(A)')']}}'
      ENDIF

C Surface libre
      IF(OSULIB.EQ.1)THEN
        WRITE(36,'(A)')'# Surface libre'
        WRITE(36,'(A,A,A)')'Shape {appearance Appearance {material '
     &    ,'Material {emissiveColor  0.0 0.5 1.0, '
     &    ,'emissiveColor 0.0 0.2 0.5, transparency 0.2}}'
        WRITE(36,'(A,A)')'  geometry IndexedFaceSet {solid FALSE, '
     &    ,'coord USE surface_libre, coordIndex ['
        DO I=0,IMAX-IMIN-1
          WRITE(36,*)2*I,2*I+1,2*I+3,2*I+2,-1
        ENDDO
        WRITE(36,'(A)')']'
C Aspect moir� de la surface de l'eau
        IF(OSUMOIR.EQ.1)THEN
          WRITE(36,'(A)')'  color Color {color['
          IBLEU=1
          DO I=1,2*(IMAX-IMIN+1)
            IF(I.EQ.IBLEU)THEN
              WRITE(36,'(A)')'    0.0 0.5 1.0'
              IF(I/2.EQ.(I+1)/2)THEN
                IBLEU=IBLEU+3
              ELSE
                IBLEU=IBLEU+5
              ENDIF
            ELSE
              WRITE(36,'(A)')'    0.0 0.2 0.5'
            ENDIF
          ENDDO
          WRITE(36,'(A)')']}'
        ENDIF
        WRITE(36,'(A)')'}}'
      ENDIF

C Texte de titre
      I=INT(0.5*(IMIN+IMAX))
      WRITE(36,'(A)')'# Titre du dessin'
      WRITE(36,'(A,F12.2,1X,F12.2,1X,F12.2)')'Transform {translation '
     &  ,XPLANI(XNC(I)),XZINI(XNCBINI(I))
     &  ,-(YPLANI(XNC(I))+0.5*(YPLANI(XNC(I))-YPLANI(XNC(I-1)+1)))
      WRITE(36,'(A,A,A)')'  children [ Shape {appearance Appearance {'
     &  ,'material Material {diffuseColor  0.0 0.5 1.0,'
     &  ,' emissiveColor 0.0 0.2 0.5, transparency 0.2}}'
      WRITE(36,'(A,A,F4.1,A,A,A,A,A)')'    geometry Text {'
     &  ,'fontStyle FontStyle {justify "MIDDLE", size ',6.0*TEXTSIZE
     &  ,'}, string ["',COMMENT1,'", "',COMMENT2,'"]}'
      WRITE(36,'(A)')'}]}'
      WRITE(36,'(A,A,F5.2,A,F12.2,1X,F12.2,1X,F12.2)')'Transform '
     &  ,'{rotation 0 1 0 ',DIRVUE,', translation ',PTVUEX-10.*TEXTSIZE
     &  *SIN(DIRVUE),PTVUEZ,-(PTVUEY-10.*TEXTSIZE*COS(DIRVUE))
      WRITE(36,'(A,A,A)')'  children [ Shape {appearance Appearance {'
     &  ,'material Material {diffuseColor  0.0 0.5 1.0,'
     &  ,' emissiveColor 0.0 0.2 0.5, transparency 0.2}}'
      WRITE(36,'(A,A,F4.1,A,A,A,A,A)')'    geometry Text {'
     &  ,'fontStyle FontStyle {justify "MIDDLE", size ',TEXTSIZE
     &  ,'}, string ["',COMMENT1,'", "',COMMENT2,'"]}'
      WRITE(36,'(A)')'}]}'

      CLOSE(36)

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE ECRVISUDXF(TN)
C-----------------------------------------------------------------------
C �crit le fichier 'visu.dxf' de visualisation de la g�om�trie et de la ligne d'eau � TN
C - sur la couche GEOM_DURE le compartiment g�om�trique inf�rieur
C - sur la couche GEOM_LIT le compartiment g�om�trique sup�rieur
C - sur la couche SURF_LIBRE la ligne d'eau
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      INTEGER LM(0:NBMAX),LL,I,J
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
      DOUBLE PRECISION TN
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      INTEGER OGEOD,OGEOL,OSULI
      CHARACTER NOMFIC*40,COMMENT1*15,COMMENT2*60,COUCHE*10
      DOUBLE PRECISION XMIN,XMAX,DIRVUEX,DIRVUEY,DIRVUEZ,PTVUEX,PTVUEY
     &  ,PTVUEZ,HAUTIMG
      DOUBLE PRECISION XZBAS,XZSL,X,XP1,Z,H
      DOUBLE PRECISION XYSULIG(LMAX),XYSULID(LMAX),XZSULI(LMAX)
      DOUBLE PRECISION XYGEODG(LMAX),XZGEODG(LMAX),XYGEODD(LMAX)
     &  ,XZGEODD(LMAX),XYGEODB(LMAX),XZGEODB(LMAX)

      COMMON/PHYS/LM,LL
      COMMON/NC/NC,XNC
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS

C R�cup�ration des options de sortie du fichier
C-----------------------------------------------------------------------
C Ces options de config sont relues � chaque fois afin de pouvoir changer
C les param�tres de visualisation en cours de calcul
      NOMFIC='config-dxf'
      OPEN(37,FILE=NOMFIC,STATUS='OLD',ERR=100)
      READ(37,*) XMIN
      READ(37,*) XMAX
      READ(37,*) OGEOD
      READ(37,*) OGEOL
      READ(37,*) OSULI
      READ(37,*) DIRVUEX,DIRVUEY,DIRVUEZ
      READ(37,*) PTVUEX,PTVUEY,PTVUEZ
      READ(37,*) HAUTIMG
      READ(37,'(A60)',ERR=200) COMMENT2
      CLOSE(37)
      GOTO 300
 100  CONTINUE
      XMIN=XTMAIL(1)
      XMAX=XTMAIL(LL)
      OGEOD=1
      OGEOL=1
      OSULI=1
 200  CONTINUE
      COMMENT2=''
 300  CONTINUE
      WRITE(COMMENT1,'(A,F10.1,A)') 'Tn=',TN,' s'
C      Write(*,*)' Xmin=',XMIN,' Xmax=',XMAX
C      Write(*,*)' OGEOD=',OGEOD,' OGEOL=',OGEOL,' OSULI=',OSULI
C      Write(*,*)COMMENT1
C      Write(*,*)' Comment2=',COMMENT2

C Calcul des intersections
C-----------------------------------------------------------------------
      DO I=1,LL
C XYGEODG(I),XZGEODG(I) limite mineur/Moyen de la g�om�trie dure � gauche
C XYGEODD(I),XZGEODD(I) limite mineur/Moyen de la g�om�trie dure � droite
        J=XNCMOAG(I)
        XYGEODG(I)=XYCOU(J)
        XZGEODG(I)=XZCS(J,XNBCS(J))
        J=XNCMOAD(I)
        XYGEODD(I)=XYCOU(J)
        XZGEODD(I)=XZCS(J,XNBCS(J))
C XYGEODB(I),XZGEODB(I) point bas de la g�om�trie dure
        J=XNC(I-1)+1
        XZBAS=XZCS(J,XNBCS(J))
        DO J=XNC(I-1)+2,XNC(I)
          IF(XZCS(J,XNBCS(J)).LT.XZBAS)THEN
            XZBAS=XZCS(J,XNBCS(J))
            XYGEODB(I)=XYCOU(J)
            XZGEODB(I)=XZBAS
          ENDIF
        ENDDO
C XYSULIG(I),XYSULID(I) ordonn�es de la surfacce libre � gauche/droite de la section
C XZSULI(I) cote de la surface libre
        XZSL=YINTER(I)+XCTDF(I)
        XZSULI(I)=XZSL
        IF(XZSL.GE.XZCS(XNC(I-1)+1,1))THEN
          XYSULIG(I)=XYCOU(XNC(I-1)+1)
          GOTO 500
        ENDIF
        DO J=XNC(I-1)+2,XNC(I)
          IF(XZSL.GE.XZCS(J,1))THEN
            XYSULIG(I)=XYCOU(J-1)+(XYCOU(J)-XYCOU(J-1))
     &        *(XZSL-XZCS(J-1,1))/(XZCS(J,1)-XZCS(J-1,1))
            GOTO 500
          ENDIF
        ENDDO
        XYSULIG(I)=XYGEODB(I)
 500    CONTINUE
        IF(XZSL.GE.XZCS(XNC(I),1))THEN
          XYSULID(I)=XYCOU(XNC(I))
          GOTO 600
        ENDIF
        DO J=XNC(I)-1,XNC(I-1)+1,-1
          IF(XZSL.GE.XZCS(J,1))THEN
            XYSULID(I)=XYCOU(J)+(XYCOU(J+1)-XYCOU(J))
     &        *(XZSL-XZCS(J,1))/(XZCS(J+1,1)-XZCS(J,1))
            GOTO 600
          ENDIF
        ENDDO
        XYSULID(I)=XYGEODB(I)
 600    CONTINUE
      ENDDO

C �criture de 'visu.dxf' au format DXF
C-----------------------------------------------------------------------
      NOMFIC='visu.dxf'
      OPEN(38,FILE=NOMFIC,STATUS='UNKNOWN')
C �criture des sections HEADER, TABLES, BLOCKS
C---------------------------------------------
C Section Header
      WRITE(38,'(A,/,A)')'  0','SECTION'
      WRITE(38,'(A,/,A)')'  2','HEADER'
      WRITE(38,'(A,/,A)')'  9','$CECOLOR'
      WRITE(38,'(A,/,A)')' 62','256'
      WRITE(38,'(A,/,A)')'  9','$TDCREATE'
      WRITE(38,'(A,/,A)')' 40','2451792.61004'
C date de cr�ation bidon: 4sept2000
      WRITE(38,'(A,/,A)')'  0','ENDSEC'
C Section Tables
      WRITE(38,'(A,/,A)')'  0','SECTION'
      WRITE(38,'(A,/,A)')'  2','TABLES'
      WRITE(38,'(A,/,A)')'  0','TABLE'
C Vue en perspective
      WRITE(38,'(A,/,A)')'  2','VPORT'
      WRITE(38,'(A,/,A)')'  0','VPORT'
      WRITE(38,'(A,/,A)')'  2','*ACTIVE'
      WRITE(38,'(A,/,A)')' 70','0'
      WRITE(38,'(A,/,A)')' 10','0.0'
      WRITE(38,'(A,/,A)')' 20','0.0'
      WRITE(38,'(A,/,A)')' 11','1.0'
      WRITE(38,'(A,/,A)')' 21','1.0'
      WRITE(38,'(A,/,A)')' 12','0.0'
      WRITE(38,'(A,/,A)')' 22','0.0'
      WRITE(38,'(A,/,A)')' 13','0.0'
      WRITE(38,'(A,/,A)')' 23','0.0'
      WRITE(38,'(A,/,A)')' 14','1.0'
      WRITE(38,'(A,/,A)')' 24','1.0'
      WRITE(38,'(A,/,A)')' 15','0.0'
      WRITE(38,'(A,/,A)')' 25','0.0'
      WRITE(38,'(A,/,F10.4)')' 16',DIRVUEX
      WRITE(38,'(A,/,F10.4)')' 26',DIRVUEZ
      WRITE(38,'(A,/,F10.4)')' 36',DIRVUEY
      WRITE(38,'(A,/,F10.4)')' 17',PTVUEX
      WRITE(38,'(A,/,F10.4)')' 27',PTVUEZ
      WRITE(38,'(A,/,F10.4)')' 37',PTVUEY
      WRITE(38,'(A,/,F10.4)')' 40',HAUTIMG
      WRITE(38,'(A,/,A)')' 41','1.0'
      WRITE(38,'(A,/,A)')' 42','50.0'
      WRITE(38,'(A,/,A)')' 43','0.0'
      WRITE(38,'(A,/,A)')' 44','0.0'
      WRITE(38,'(A,/,A)')' 50','0.0'
      WRITE(38,'(A,/,A)')' 51','270.0'
      WRITE(38,'(A,/,A)')' 71','0'
      WRITE(38,'(A,/,A)')' 72','100'
      WRITE(38,'(A,/,A)')' 73','1'
      WRITE(38,'(A,/,A)')' 74','1'
      WRITE(38,'(A,/,A)')' 75','0'
      WRITE(38,'(A,/,A)')' 76','0'
      WRITE(38,'(A,/,A)')' 77','0'
      WRITE(38,'(A,/,A)')' 78','0'
      WRITE(38,'(A,/,A)')'  0','ENDTAB'
      WRITE(38,'(A,/,A)')'  0','TABLE'
C Couche GEOM_DURE
      WRITE(38,'(A,/,A)')'  2','LAYER'
      WRITE(38,'(A,/,A)')' 70','225'
      WRITE(38,'(A,/,A)')'  0','LAYER'
      WRITE(38,'(A,/,A)')'  2','GEOM_DURE'
      WRITE(38,'(A,/,A)')'  6','CONTINUOUS'
      WRITE(38,'(A,/,A)')' 62','9'
      WRITE(38,'(A,/,A)')' 70','64'
      WRITE(38,'(A,/,A)')'  0','ENDTAB'
      WRITE(38,'(A,/,A)')'  0','TABLE'
C Couche GEOM_LIT
      WRITE(38,'(A,/,A)')'  2','LAYER'
      WRITE(38,'(A,/,A)')' 70','225'
      WRITE(38,'(A,/,A)')'  0','LAYER'
      WRITE(38,'(A,/,A)')'  2','GEOM_LIT'
      WRITE(38,'(A,/,A)')'  6','CONTINUOUS'
      WRITE(38,'(A,/,A)')' 62','2'
      WRITE(38,'(A,/,A)')' 70','64'
      WRITE(38,'(A,/,A)')'  0','ENDTAB'
      WRITE(38,'(A,/,A)')'  0','TABLE'
C Couche SURF_LIBRE
      WRITE(38,'(A,/,A)')'  2','LAYER'
      WRITE(38,'(A,/,A)')' 70','225'
      WRITE(38,'(A,/,A)')'  0','LAYER'
      WRITE(38,'(A,/,A)')'  2','SURF_LIBRE'
      WRITE(38,'(A,/,A)')'  6','CONTINUOUS'
      WRITE(38,'(A,/,A)')' 62','4'
      WRITE(38,'(A,/,A)')' 70','64'
      WRITE(38,'(A,/,A)')'  0','ENDTAB'

      WRITE(38,'(A,/,A)')'  0','ENDSEC'
      WRITE(38,'(A,/,A)')'  0','SECTION'
      WRITE(38,'(A,/,A)')'  2','BLOCKS'
      WRITE(38,'(A,/,A)')'  0','ENDSEC'

C �criture de la section ENTITIES
C--------------------------------
      WRITE(38,'(A,/,A)')'  0','SECTION'
      WRITE(38,'(A,/,A)')'  2','ENTITIES'

      DO I=1,LL
        X=XTMAIL(I)
        IF(X.LT.XMIN) GOTO 800
        IF(X.GT.XMAX) GOTO 800
        IF(OGEOD.EQ.1)THEN
C Sections en travers g�om�trie dure
          DO J=XNC(I-1)+1,XNC(I)-1
            COUCHE='GEOM_DURE'
            CALL VISUDXFSEGM(COUCHE,X,XYCOU(J),XZCS(J,XNBCS(J))
     &        ,X,XYCOU(J+1),XZCS(J+1,XNBCS(J+1)))
          ENDDO
        ENDIF
        IF(OGEOL.EQ.1)THEN
C Sections en travers lit
          DO J=XNC(I-1)+1,XNC(I)-1
            COUCHE='GEOM_LIT'
            CALL VISUDXFSEGM(COUCHE,X,XYCOU(J),XZCS(J,1)
     &        ,X,XYCOU(J+1),XZCS(J+1,1))
          ENDDO
        ENDIF
        IF(OSULI.EQ.1)THEN
C Ligne d'eau dans la section
          COUCHE='SURF_LIBRE'
          CALL VISUDXFSEGM(COUCHE,X,XYSULIG(I),XZSULI(I)
     &      ,X,XYSULID(I),XZSULI(I))
        ENDIF
        IF(I.EQ.LL) GOTO 800
        XP1=XTMAIL(I+1)
        IF(XP1.GT.XMAX) GOTO 800
        IF(OGEOD.EQ.1)THEN
C Profil en long g�om�trie dure
          COUCHE='GEOM_DURE'
          CALL VISUDXFSEGM(COUCHE,X,XYGEODG(I),XZGEODG(I)
     &      ,XP1,XYGEODG(I+1),XZGEODG(I+1))
          CALL VISUDXFSEGM(COUCHE,X,XYGEODD(I),XZGEODD(I)
     &      ,XP1,XYGEODD(I+1),XZGEODD(I+1))
          CALL VISUDXFSEGM(COUCHE,X,XYGEODB(I),XZGEODB(I)
     &      ,XP1,XYGEODB(I+1),XZGEODB(I+1))
        ENDIF
        IF(OSULI.EQ.1)THEN
C Profil en long de la ligne d'eau
          COUCHE='SURF_LIBRE'
          CALL VISUDXFSEGM(COUCHE,X,XYSULIG(I),XZSULI(I)
     &      ,XP1,XYSULIG(I+1),XZSULI(I+1))
          CALL VISUDXFSEGM(COUCHE,X,XYSULID(I),XZSULI(I)
     &      ,XP1,XYSULID(I+1),XZSULI(I+1))
        ENDIF
 800    CONTINUE
      ENDDO

      Z=0.5*(XZCS(1,XNBCS(1))+XZCS(LL,XNBCS(LL)))
      H=0.2*(XZCS(1,1)-XZGEODB(1))
      WRITE(38,'(A,/,A)')'  0','TEXT'
      WRITE(38,'(A,/,A)')'  8','GEOM_LIT'
      WRITE(38,'(A,/,F10.4)')'  10',0.5*(XMIN+XMAX)
      WRITE(38,'(A,/,F10.4)')'  20',Z+1.25*H
      WRITE(38,'(A,/,F10.4)')'  30',XYCOU(LL)
      WRITE(38,'(A,/,F10.4)')'  40',H
      WRITE(38,'(A,/,A)')'  1',COMMENT1
      WRITE(38,'(A,/,A)')'  0','TEXT'
      WRITE(38,'(A,/,A)')'  8','GEOM_LIT'
      WRITE(38,'(A,/,F10.4)')'  10',0.5*(XMIN+XMAX)
      WRITE(38,'(A,/,F10.4)')'  20',Z-1.25*H
      WRITE(38,'(A,/,F10.4)')'  30',XYCOU(LL)
      WRITE(38,'(A,/,F10.4)')'  40',H
      WRITE(38,'(A,/,A)')'  1',COMMENT2


      WRITE(38,'(A,/,A)')'  0','ENDSEC'

C �criture de la fin du fichier et fermeture
C-------------------------------------------
      WRITE(38,'(A,/,A)')'  0','EOF'
      CLOSE(38)

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE VISUDXFSEGM(COUCHE,X1,Y1,Z1,X2,Y2,Z2)
C-----------------------------------------------------------------------
C �crit dans le fichier '38' un bloc d'instructions DXF pour tracer un
C segment (1,2) sur la couche COUCHE
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER C
      CHARACTER*10 COUCHE
      DOUBLE PRECISION  X1,Y1,Z1,X2,Y2,Z2

      DO C=1,10
        IF(COUCHE(C:C).EQ.' ') GOTO 100
      ENDDO
 100  CONTINUE

      WRITE(38,'(A,/,A)')'  0','LINE'
      WRITE(38,'(A)')'  8'
      WRITE(38,'(A)')COUCHE(1:C-1)
      WRITE(38,'(A)')'  10'
      WRITE(38,*)X1
      WRITE(38,'(A)')'  20'
      WRITE(38,*)Z1
      WRITE(38,'(A)')'  30'
      WRITE(38,*)Y1
      WRITE(38,'(A)')'  11'
      WRITE(38,*)X2
      WRITE(38,'(A)')'  21'
      WRITE(38,*)Z2
      WRITE(38,'(A)')'  31'
      WRITE(38,*)Y2

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE ACTVARHYD
C-----------------------------------------------------------------------
C actualise les variables hyrauliques dans le cas d'un calcul avec transport solide
c  ----------------------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBMAX,J,LMAX,IK,LDETYJ,LDETSJ
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LM(0:NBMAX),LL
         INTEGER DEFOND,VARCONS
      INTEGER IJ,NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
     &  ,SNP1(LMAX),QNP1(LMAX),YNP1(LMAX),RHNP1(LMAX),VNP1(LMAX)
     :,VIT
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION DTN,TN,TNP1
      DOUBLE PRECISION CTDFTN(LMAX)
      DOUBLE PRECISION DETSN,DETYN

      EXTERNAL LDETSJ,DETYN,LDETYJ,DETSN

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/NC/NC,XNC
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/CTDFTN/CTDFTN
      COMMON/MALTNP/SNP1,QNP1
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/VITTNP/VNP1,YNP1,RHNP1
      COMMON/PHYS/LM,LL
      COMMON/DEFOND/DEFOND
      COMMON/VARCONS/VARCONS


C      Write(*,*)'ACTVARHYD: entree'
      IF(VARCONS.EQ.1)THEN
C conservation de S et Q
        DO J=1,LL+1
          IJ=LDETSJ(0,SNP1(J),J)
C          IF(IJ.EQ.NC(J))WRITE(*,*)'Debordement maille ',J
          YNP1(J)=DETYN(0,SNP1(J),IJ)
          IF(YNP1(J).LT.EPSY)  YNP1(J)=0.
        ENDDO
      ELSE
C      ELSEIF(VARCONS.EQ.2.OR.VARCONS.EQ.3)THEN
C conservation de la cote
c d�termination des hauteur d'eau YNP1(J)
        DO J=1,LL+1
          IF(YNP1(J).LT.EPSY) THEN
            YNP1(J)=0.
            SNP1(J)=0
            VNP1(J)=0
            QNP1(J)=0
          ELSE
            YNP1(J)=(YNP1(J)+CTDFTN(J))-CTDF(J)
            IF(YNP1(J).LT.EPSY) THEN
              YNP1(J)=0.
              SNP1(J)=0
              VNP1(J)=0
              QNP1(J)=0
            ELSE
C vnp1 n'a pas encore ete actualisee
                vnp1(j)=QNP1(J)/SNP1(J)
              IK=LDETYJ(0,YNP1(J),J)
C          IF(IK.EQ.NC(J)) WRITE(*,*)'Debordement maille ',J
              SNP1(J)=DETSN(0,YNP1(J),IK)
              IF(VARCONS.EQ.2) THEN
C conservation du debit si vitesse non acrrue sinon de la vitesse
                VIT=QNP1(J)/SNP1(J)
                IF(VNP1(J).GT.VIT)THEN
                  VNP1(J)=VIT
                ELSE
                  QNP1(J)=VNP1(J)*SNP1(J)
                ENDIF
              ELSE
C conservation de la vitesse
                QNP1(J)=VNP1(J)*SNP1(J)
C fin du if sur varcons=2
              ENDIF
            ENDIF
          ENDIF
C fin boucle sur J
        ENDDO
C fin du if sur varcons=3 et =2
      ENDIF
      RETURN
      END

C-----------------------------------------------------------------------
C-----------------------------------------------------------------------
      SUBROUTINE SC2(SN1,YN1D1,QN1,J)
C-----------------------------------------------------------------------
C Calcule le terme de frottements (� Tn+1)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER J
C     :,LMAX
c      PARAMETER(LMAX=3000)
c      DOUBLE PRECISION  VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION DTN,TN,TNP1
      DOUBLE PRECISION YN1D1,QN1,AD,BD,CD,SN1,V1
      DOUBLE PRECISION SMBQ2
      EXTERNAL SMBQ2

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1
C      COMMON/VITTN/VN,YN1D,RHN

c      Write(*,*)'Entree SC2 J=',J
      IF(YN1D1.GT.EPSY)THEN
C dsmq1 correspond aux termes de geometrie
C       QN1=QN1+DSMQ1
C dsmq2 correspond au 1/2 frottement
C question : pour stabilite un seul frottement?
c        IF(ABS(QN1).GT.ABS(DSMQ2))THEN
c          QN1=QN1+DSMQ2
c        ELSE
c          QN1=0.
c        ENDIF
        V1=1.
        AD=-DTN*SMBQ2(0,V1,J,YN1D1)/SN1**2
C        AD=-0.5*DTN*SMBQ2(0,V1,J,YN1D1)/SN1**2
        CD=-(QN1)
        BD=AD*CD
        IF(ABS(BD).LT.EPS)THEN
          QN1=-CD
        ELSEIF(BD.LT.0.)THEN
          QN1=(-1.+SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
        ELSE
          QN1=(1.-SQRT(1.+4.*AD*ABS(CD)))/(2.*AD)
        ENDIF
      ELSE
        QN1=0.
      ENDIF

c      Write(*,*)'Sortie SC2'
c      Write(*,*)' '
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE TRACLC
C-----------------------------------------------------------------------
C Transforme une section intermaille d'abscisse-cote en largeur-cote,
C i.e. calcule les tableaux de XTBGEO � partir de la g�om�trie abs-cot
C-----------------------------------------------------------------------
C XLISEC: largeur au  miroir         XYISEC: hauteur d'eau
C XLYSEC: gradient de largeur        XSECUM: section mouill�e
C XPICUM: pression hydrostatique     XPECUM: perim�tre mouill�
      IMPLICIT NONE
      INTEGER I,J,K,DERTRI,J1,J2,jk,ik
      INTEGER LMAX,LNCMAX,NBHYPR,CSMAX,NBMAX,IB,NBB
      PARAMETER(LMAX=3000,LNCMAX=130000,NBHYPR=100,CSMAX=10,NBMAX=150)
      INTEGER NC(0:LMAX),XNC(0:LMAX),LL,LM(0:NBMAX)
      INTEGER XNCMO(0:LMAX),NCMO(0:LMAX)
      LOGICAL EXPART
      DOUBLE PRECISION LI,YI,ZPERMUT,XYPART1,XZPART1,MINXYISEC
C      ,XYPART2,XZPART2
      DOUBLE PRECISION XZCOUP1(LNCMAX)
      DOUBLE PRECISION XYCOU(LNCMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX),YMOG
     :, XSECMI(LNCMAX),SECMIN(LNCMAX)

      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX),ZMINMAJ(lmax)
         INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
         INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      LOGICAL XMODSEC(LMAX),MODSEC,MOYEN,CHANGE
            LOGICAL DEBORD,STOCKAGE
C variables pour fente de preisman
         logical iffente
         double precision dlfente
         integer ncfente(lmax),xncfente(lmax)
                 logical encore

C variables pour fente de preisman
         COMMON/IFFENTE/iffente
         COMMON/LFENTE/dlfente
         COMMON/NCXNCFENTE/ncfente,xncfente

      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/XGEOMACY/XYCOU
      COMMON/GEOCP1/XZCOUP1
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/MODIFSEC/XMODSEC,MODSEC
      COMMON/NBIEF/NBB
      COMMON/SPMM/XSECMI,SECMIN
      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/NCMM/NCMO,XNCMO
          COMMON/DEBORS/DEBORD,STOCKAGE

      DO IB=1,NBB
      DO I=LM(IB-1)+1,LM(IB)-1
C      DO I=1,LL
      IF(XMODSEC(I))THEN
C Classement par cotes croissantes
C---------------------------------
C K est l'indice des couples ainsi tri�s
C ATTENTION, pour l'instant XYISEC contient des cotes absolues
C Ensuite, apr�s les avoir copi�s dans XYISEC, on range ce tableau
C        XYISEC(XNC(I-1)+1)=XZCOUP1(XNC(I-1)+1)
C        DO K=XNC(I-1)+2,XNC(I)
        ZMINMAJ(I)=MIN(XZCOUP1(XNCMoAG(I)),XZCOUP1(XNCMOAD(I)))
                minxyisec=zminmaj(i)
C zmin maj sert a relever le fond du lit majeur car on suppose
C que le lit majeur ne coule pas au dessous de la berge la plus basse
      If(STOCKAGE)THEN
        DO K=XNC(I-1)+1,XNC(I)
                IF(K.lt.XNCMOAG(I))THEN
            XYISEC(K)=max(XZCOUP1(K),ZMINMAJ(i))
                ELSEIF(K.GT.XNCMOAD(I))THEN
            XYISEC(K)=max(XZCOUP1(K),ZMINMAJ(i))
                ELSEIF(K.EQ.XNCMOAG(I))THEN
            XYISEC(K)=XZCOUP1(K)
                ELSEIF(K.EQ.XNCMOAD(I))THEN
            XYISEC(K)=XZCOUP1(K)
                ELSE
            XYISEC(K)=XZCOUP1(K)
                        minxyisec=min(minxyisec,xyisec(k))
             ENDIF
        ENDDO
C else du if sur stockage
        else
        DO K=XNC(I-1)+1,XNC(I)
                IF(K.lE.XNCMOAG(I))THEN
             XYISEC(K)=XZCOUP1(K)
                ELSEIF(K.GE.XNCMOAD(I))THEN
            XYISEC(K)=XZCOUP1(K)
                ELSE
            XYISEC(K)=XZCOUP1(K)
                        minxyisec=min(minxyisec,xyisec(k))
             ENDIF
        ENDDO
C fin du if sur stockage
         endif
C si point bas lit mineur plus haut que berge basse
        If(minxyisec.Gt.zminmaj(i)-epsy)then
C si a gauche plus bas on bouge limite
                  IF(XYISEC(XNCMOAG(I)).lt.minxyisec+epsy)then
                     change=.false.
                     do ik=1,xncmoag(i)-xnc(i-1)-1
                         if(.not.change)then
      if(xyisec(xncmoag(i)-ik).gt.minxyisec+epsy)then
                change=.true.
c                        write(*,*)'chg',i,minxyisec,epsy,xyisec(xncmoag(i))
                        If(xncmoag(i).ne.xncmmag(i))then
                           xncmoag(i)=xncmoag(i)-ik
                        else
C si les deux limites sont identiques, on les bouge ensemble
                           xncmoag(i)=xncmoag(i)-ik
                           xncmmag(i)=xncmmag(i)-ik
                        endif
                        endif
                        endif
                        enddo
C fin du if sur xyisec(xncmoag)
            endif
C si a droite plus bas on bouge limite
                  IF(XYISEC(XNCMOAD(I)).lt.minxyisec+epsy)then
                     change=.false.
                     do ik=1,xnc(i)-xncmoaD(i)
                         if(.not.change)then
      if(xyisec(xncmoad(i)+ik).gt.minxyisec+epsy)then
                change=.true.
c                        write(*,*)'chd',i,minxyisec,epsy,xyisec(xncmoad(i))
                        If(xncmoad(i).ne.xncmmad(i))then
                            xncmoad(i)=xncmoad(i)+ik
                        else
C si les deux limites sont identiques, on les bouge ensemble
                           xncmoad(i)=xncmoad(i)+ik
                           xncmmad(i)=xncmmad(i)+ik
                        endif
                        endif
                        endif
                        enddo
C fin du if sur xyisec(xncmoad)
            endif
C fin du if sur minxyisec
            endif
        ZMINMAJ(I)=MIN(XZCOUP1(XNCMoAG(I)),XZCOUP1(XNCMOAD(I)))
c                minxyisec=zminmaj(i)
        DO DERTRI=XNC(I)-1,XNC(I-1)+1,-1
          DO K=XNC(I-1)+1,DERTRI
            IF(XYISEC(K).GT.XYISEC(K+1))THEN
              ZPERMUT=XYISEC(K+1)
              XYISEC(K+1)=XYISEC(K)
              XYISEC(K)=ZPERMUT
                  ENDIF
          ENDDO
        ENDDO

C on met une petite difference sur les xyisec pour calculer
C les largeurs quand deux valeurs de xyisec egales
C on remettra la difference a zero apres
C si plusieurs cotes egales toutes sauf la derniere
C sont d�cal�es a la meme valeur
c on pose       EPS=0.0001 :faux
C en fait eps est tr�s petit car eps en common
C non mis en commentaire le 14 septembre 2011
C sert a avoir deux largeurs differentes pour la meme cote
        ZPERMUT=XYISEC(XNC(I-1)+1)
        DO K=XNC(I-1)+2,XNC(I)
C on ne fait pas le decalage pour le fond si egalite
          IF(XYISEC(K).GT.ZPERMUT)THEN
            IF((XYISEC(K)-XYISEC(K-1)).LT.EPS)THEN
              XYISEC(K-1)=XYISEC(K-1)-0.5*EPS
C nota : si plusieurs fois la meme cote, seule la derniere a ula valeur initiale
C les autres sont a -1/2 eps
            ENDIF
          ENDIF
        ENDDO
C Pour chaque hauteur d'eau remarquable, on parcourt la section
C d'un bord � l'autre et on calcule XLISEC,XPECUM
C--------------------------------------------------------------
C EXPART note si le segment entre deux points est en partie en eau
C XYPART1, XZPART1 sont les coordonn�es du 'point amont' � consid�rer pour les calculs dans le segment suivant
C ATTENTION, pour l'instant XYISEC contient des cotes absolues
        DO K=XNC(I-1)+1,XNC(I)
          XLISEC(K)=0.
          XPECUM(K)=0.
          IF(XYISEC(K).LT.ZMINMAJ(I))THEN
C on reduit la largeur a celle du lit mineur
             J1=XNCMOAG(I)
             J2=XNCMOAD(I)
          ELSE
             J1=XNC(I-1)+1
             J2=XNC(I)
          ENDIF
C !Traitement � part du premier point de la section pour avoir les premi�res donn�es amont
C          J=XNC(I-1)+1
          J=J1
          IF(XZCOUP1(J) .LT. XYISEC(K))THEN
                  EXPART=.TRUE.
                     IF (XZCOUP1(J+1) .GT. XYISEC(K))THEN
                       XYPART1=XYCOU(J+1)+(XYCOU(J)-XYCOU(J+1))
     &        *(XYISEC(K)-XZCOUP1(J+1))/(XZCOUP1(J)-XZCOUP1(J+1))
              XZPART1=XYISEC(K)
                     ELSE
                       XYPART1=XYCOU(J)
              XZPART1=XZCOUP1(J)
                     ENDIF
             ELSEIF(XZCOUP1(J) .EQ. XYISEC(K))THEN
                  IF(XZCOUP1(J+1) .LE. XYISEC(K))THEN
                         EXPART=.TRUE.
                         XYPART1=XYCOU(J)
                XZPART1=XZCOUP1(J)
                  ELSE
                         EXPART=.FALSE.
                  ENDIF
                ELSE
                       IF(XZCOUP1(J+1) .LT. XYISEC(K))THEN
                         EXPART=.TRUE.
                         XYPART1=XYCOU(J+1)+(XYCOU(J)-XYCOU(J+1))
     &        *(XYISEC(K)-XZCOUP1(J+1))/(XZCOUP1(J)-XZCOUP1(J+1))
                XZPART1=XYISEC(K)
                       ELSE
                            EXPART=.FALSE.
                       ENDIF
                ENDIF
c boucle de calcul sur les segments (on a un segment de moins que de point)
          DO J=J1+1,J2-1
C          DO J=XNC(I-1)+2,XNC(I)
c on calcul les composantes de XLISEC et XPECUM au segment J (entre les points J-1 et J)
                     IF(EXPART)THEN
                       IF(XZCOUP1(J) .LE. XYISEC(K))THEN
C                         XLISEC(K)=XLISEC(K)+ABS(XYCOU(J)-XYPART1)
                         XLISEC(K)=XLISEC(K)+(XYCOU(J)-XYPART1)
                XPECUM(K)=XPECUM(K)
     &        +SQRT((XYCOU(J)-XYPART1)**2+(XZCOUP1(J)-XZPART1)**2)
                       ELSE
                         XLISEC(K)=XLISEC(K)-(XYCOU(J-1)-XYPART1)
C                         XLISEC(K)=XLISEC(K)+ABS(XYCOU(J-1)-XYPART1)
                XPECUM(K)=XPECUM(K)
     &        +SQRT((XYCOU(J-1)-XYPART1)**2+(XZCOUP1(J-1)-XZPART1)**2)
                       ENDIF
                     ENDIF
c !sortie pour J=XNC(I) ie pas besoin de d�finir les informations amonts car il n'y a plus de segment aval
C                     IF(J .NE. XNC(I))THEN
c on �tablit les informations n�cessaires � la prochaine �tape(diff�rence selon le placement des points par rapport � la ligne d'eau)
                     IF(XZCOUP1(J) .LT. XYISEC(K))THEN
                       EXPART=.TRUE.
                       IF (XZCOUP1(J+1) .GT. XYISEC(K))THEN
                         XYPART1=XYCOU(J+1)+(XYCOU(J)-XYCOU(J+1))
     &        *(XYISEC(K)-XZCOUP1(J+1))/(XZCOUP1(J)-XZCOUP1(J+1))
                XZPART1=XYISEC(K)
                       ELSE
                         XYPART1=XYCOU(J)
                XZPART1=XZCOUP1(J)
                       ENDIF
                     ELSEIF(XZCOUP1(J) .EQ. XYISEC(K))THEN
                       IF(XZCOUP1(J+1) .LE. XYISEC(K))THEN
                         EXPART=.TRUE.
                         XYPART1=XYCOU(J)
                XZPART1=XZCOUP1(J)
                       ELSE
                         EXPART=.FALSE.
                       ENDIF
                     ELSE
                       IF(XZCOUP1(J+1) .LT. XYISEC(K))THEN
                         EXPART=.TRUE.
                         XYPART1=XYCOU(J+1)+(XYCOU(J)-XYCOU(J+1))
     &        *(XYISEC(K)-XZCOUP1(J+1))/(XZCOUP1(J)-XZCOUP1(J+1))
                XZPART1=XYISEC(K)
                       ELSE
                         EXPART=.FALSE.
                       ENDIF
                     ENDIF
C fin du if sur j=xnc(i)
C         ENDIF
C fin du do sur j
        ENDDO
        J=J2
c on calcul les composantes de XLISEC et XPECUM au segment J (entre les points J-1 et J)
                     IF(EXPART)THEN
                       IF(XZCOUP1(J) .LE. XYISEC(K))THEN
C                         XLISEC(K)=XLISEC(K)+ABS(XYCOU(J)-XYPART1)
                         XLISEC(K)=XLISEC(K)+(XYCOU(J)-XYPART1)
                XPECUM(K)=XPECUM(K)
     &        +SQRT((XYCOU(J)-XYPART1)**2+(XZCOUP1(J)-XZPART1)**2)
                       ELSE
C                         XLISEC(K)=XLISEC(K)+ABS(XYCOU(J-1)-XYPART1)
                         XLISEC(K)=XLISEC(K)-(XYCOU(J-1)-XYPART1)
                XPECUM(K)=XPECUM(K)
     &        +SQRT((XYCOU(J-1)-XYPART1)**2+(XZCOUP1(J-1)-XZPART1)**2)
                       ENDIF
                     ENDIF
c !sortie pour J=J2 ie pas besoin de d�finir les informations amonts car il n'y a plus de segment aval
C fin du do sur k
        ENDDO

C Calcul de XYISEC,XLYSEC,XSECUM,XPICUM
C--------------------------------------
C XYISEC est converti en cotes relatives dans la section
        K=XNC(I-1)+1
        XSECUM(K)=0.
        XPICUM(K)=0.
        XCTDF(I)=XYISEC(K)
        XYISEC(K)=0.
        DO K=XNC(I-1)+2,XNC(I)
          XYISEC(K)=XYISEC(K)-XCTDF(I)
          LI=XLISEC(K)-XLISEC(K-1)
          YI=XYISEC(K)-XYISEC(K-1)
C on remet les deux cotes s�par�es de EPS � egalit�
C si plus de deux cotes on garde deux valeurs differentes de xyisec
C mais des valeurs identiques de xpicum et xsecum
          IF(YI.LT.EPS)THEN
            XYISEC(K-1)=XYISEC(K)
            XLYSEC(K-1)= 0.
            XSECUM(K)=XSECUM(K-1)
            XPICUM(K)=XPICUM(K-1)
          ELSE
            XLYSEC(K-1)=LI/YI
            XSECUM(K)=XSECUM(K-1)+0.5*YI*(XLISEC(K-1)+XLISEC(K))
            XPICUM(K)=XPICUM(K-1)
     &      +YI*(XSECUM(K-1)+YI*(0.5*XLISEC(K-1)+LI/6.))
          ENDIF
C fin de la boucle sur k de xnc(i-1)+2 a xnc(i)
        ENDDO
        XLYSEC(XNC(I))=XLYSEC(XNC(I)-1)
C correction si couple du haut double
        IF(ABS(LI).LT.EPS)THEN
        IF(YI.LT.EPS)THEN
        XLYSEC(XNC(I))=XLYSEC(XNC(I)-2)
        ENDIF
        ENDIF
C        XLYSEC(XNC(I))=XLYSEC(XNC(I)-1)
C fin du if sur xmodsec
      ENDIF
C fente de preisman : on change xpecum
        k=xnc(i)+1
        xncfente(i)=xnc(i)+1
                encore=.true.
        do j=xnc(i), xnc(i-1)+3,-1
                  if(encore)then
                   if(xlisec(j).lt.dlfente)then
                      k=j
                   else
                           encore=.false.
                   endif
                 endif
c fin du do sur j
        enddo
                if(k.lt.xnc(i))then
          xncfente(i)=k
          iffente=.TRUE.
              do j=k+1,xnc(i)
                    xpecum(j)=xpecum(k)
                  enddo
        endif
C fin de la boucle sur i de 1 a lm
      ENDDO
C fin de la boucle sur ib de 1 a nbb
      ENDDO
c      XNCMOG(0)=0
c      XNCMOD(0)=0
      XNCMO(0)=0
c      MOYENG=.FALSE.
      DO IB=1,NBB
      DO I=LM(IB-1)+1,LM(IB)-1
      IF(XMODSEC(I))THEN
C      DO I=1,LL
c        YMOG=XZCOU(XNCMOAG(I))-XCTDF(I)
c        YMOD=XZCOU(XNCMOAD(I))-XCTDF(I)c
              YMOG=ZMINMAJ(I)-XCTDF(I)
C              MIN(YMOG,YMOD)
c        XNCMOG(I)=XNC(I)
c        XNCMOD(I)=XNC(I)
        Moyen=.false.
C on part de +3 et pas de +2 pour avoir au moins 2
        DO J=XNC(I-1)+3,XNC(I)
          If(.NOT.moyen)THEN
C            IF(XYISEC(J).GT.YMOG)THEN
C on ajoute eps pour avoir le dernier couple correspondant a ymog
            IF(XYISEC(J).GT.YMOG+EPSY)THEN
              MOYEn=.TRUE.
C  ! On prend la valeur la plus proche
              IF(ABS(XYISEC(J-1)-YMOG).GT.ABS(XYISEC(J)-YMOG))THEN
                XNCMO(I)=J
C on rajoute un test pour eviter un lit mineur de hauteur nulle
C par exemple quand le lit mineur a ete bouche par les sediments
              ELSEIF(xyisec(j-1).GT.EPSY)then
                XNCMO(I)=J-1
C on cherche le numero le plus bas si plusieurs egaux
C afin d'avoir la largeur la plus faible pour le lit mineur
                IF(XNCmo(I).LT.XNC(I))THEN
                     do jk=XNCMO(i)-1,XNC(I-1)+1,-1
                    IF(abs(XYisec(Jk)-YMOG).lt.eps)THEN
                            xncmo(i)=Jk
                       ENDIF
                     enddo
                ENDIF
                          else
                XNCMO(I)=J
              ENDIF
            ENDIF
          ENDIF
        ENDDO
             IF(.NOT.Moyen)then
C si xncmo est reste a 0 cela signifie que pas de lit majeur
                     xncmo(i)=xnc(i)
              endif
C fin du if sur xmodsec
      ENDIF
C fin boucle sur I
      ENDDO
      XNCMO(LM(IB))=XNC(LM(IB))
C fin boucle sur IB
      ENDDO
C      ENDDO
C detrmination de sections et perimetre mouille du plein bord
      DO IB=1,NBB
      DO I=LM(IB-1)+1,LM(IB)-1
      IF(XMODSEC(I))THEN
C         DO I=1,LL
            DO J=XNC(I-1)+1,XNC(I)
              IF(XNCMO(I).GE.J)THEN
                XSECMI(J)=XSECUM(J)
              ELSE
                XSECMI(J)=XSECMI(J-1)+(XYISEC(J)-XYISEC(J-1))
     :*XLISEC(XNCMO(I))
              ENDIF
C boucle sur j
                       ENDDO
C fin du if sur xmodsec
      ENDIF
c boucle sur I
          ENDDO
c                do j=xnc(lm(ib)-2)+1,xnc(lm(ib)-1)
c                   write(*,*)'j=',j,xsecmi(j)
c                enddo
c boucle sur ib
                ENDDO
C Fin du traitement
C------------------
C Si une seule section de la g�om�trie a chang�, il faut recalculer tous les centremailles
C car les limites NC(I) peuvent avoir chang�
C On rebascule l'interrupteur des intermailles car ils ont maintenant �t� mis � jour
      MODSEC=.FALSE.
      DO I=1,LL
        MODSEC=.NOT.((.NOT.MODSEC).AND.(.NOT.XMODSEC(I)))
        XMODSEC(I)=.FALSE.
      ENDDO
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE INCMLC
C-----------------------------------------------------------------------
C Interpole en largeur-cote une section centremaille � partir des
C intermailles adjacentes, i.e. calcule les tableaux NC,NCMO,
C CTDF,PEN et ceux de TABGEO � partir de ceux de XTBGEO si MODSEC=.TRUE.
C-----------------------------------------------------------------------
C LISEC: largeur au  miroir         YISEC: hauteur d'eau
C LYSEC: gradient de largeur        SECUM: section mouill�e
C PECUM: perim�tre mouill�
      IMPLICIT NONE
      INTEGER NBMAX,LMAX,LNCMAX,I,J,J1,J2,JJ,IB,NBB,K
      PARAMETER(LMAX=3000,LNCMAX=130000,NBMAX=150)
      INTEGER NC(0:LMAX),XNC(0:LMAX),NCMO(0:LMAX),XNCMO(0:LMAX)
     :,LL,LM(0:NBMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
C      DOUBLE PRECISION XLIINTER,XPEINTER
     :,YM,YI
C        ,XYI
C         ,RI
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
     :  ,XSECMI(LNCMAX),SECMIN(LNCMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX),LI
      LOGICAL XMODSEC(LMAX),MODSEC
         DOUBLE PRECISION DETL
         EXTERNAL DETL
C variables pour fente de preisman
         logical iffente
         double precision dlfente
         integer ncfente(lmax),xncfente(lmax)
C variables pour fente de preisman
         COMMON/IFFENTE/iffente
         COMMON/LFENTE/dlfente
         COMMON/NCXNCFENTE/ncfente,xncfente

      COMMON/PHYS/LM,LL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/MODIFSEC/XMODSEC,MODSEC
      COMMON/SPMM/XSECMI,SECMIN
         COMMON/NBIEF/NBB

C Pr�liminaires
C--------------
C Si aucun point de la g�om�trie n'a chang�, pas besoin de r�interpoler les centremailles
C En revanche, si une seule section a chang�, il faut recalculer tous les centremailles
C car les limites NC(I) peuvent avoir chang�
      IF(.NOT.MODSEC) RETURN
c      print*,' entree incmlc='
c      pause

C Cote du fond et pente aux centremailles
      DO IB=1,NBB
C calcul des pentes du fond (des mailles)
          DO I=LM(IB-1)+2,LM(IB)-1
             PEN(I) = (XCTDF(I-1)-XCTDF(I))/(XTMAIL(I)-
     +     XTMAIL(I-1))
          ENDDO
          PEN(LM(IB-1)+1)=PEN(LM(IB-1)+2)
          PEN(LM(IB))=PEN(LM(IB)-1)
C CALCUL COTE FOND CENTRE DE MAILLE A PARTIR INTERFACE
          DO 6 I=LM(IB-1)+2,LM(IB)-1
             CTDF(I)=0.5*(XCTDF(I)+XCTDF(I-1))
 6        CONTINUE
          CTDF(LM(IB))=XCTDF(LM(IB)-1)
          CTDF(LM(IB-1)+1)=XCTDF(LM(IB-1)+1)
C fin boucle sur IB
      ENDDO

C Initialisations pointeurs de couples
      NC(0)=0
      NCMO(0)=0
      DO IB=1,NBB
         IF(IB.EQ.1)THEN
           YISEC(1)=0.
           LISEC(1)=XLISEC(1)
           SECUM(1)=0.
           LYSEC(1)=XLYSEC(1)
           PECUM(1) = LISEC(1)
           NCMO(1)=XNCMO(1)
        DO 8253 J1=2,XNC(1)
           YISEC(J1)=XYISEC(J1)
           LISEC(J1)=XLISEC(J1)
           SECUM(J1)=XSECUM(J1)
           LYSEC(J1)=XLYSEC(J1)
           LI = LISEC(J1)-LISEC(J1-1)
           YI = YISEC(J1)-YISEC(J1-1)
           PECUM(J1) = PECUM(J1-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(1).GE.J1)THEN
                SECMIN(J1)=SECUM(J1)
C                PECMIN(J1)=PECUM(J1)
              ELSE
                SECMIN(J1)=SECMIN(J1-1)+YI*LISEC(NCMO(1))
C                PECMIN(J1)=PECMIN(J1-1)
              ENDIF
 8253   CONTINUE
        NC(1)=XNC(1)
C si IB different de 1
        ELSE
C J2 est la difference d indice entre intermailles et centres
           J2=XNC(LM(IB-1))-NC(LM(IB-1))
           YISEC(NC(LM(IB-1))+1)=0.
           LISEC(NC(LM(IB-1))+1)=XLISEC(XNC(LM(IB-1))+1)
           SECUM(NC(LM(IB-1))+1)=0.
           LYSEC(NC(LM(IB-1))+1)=XLYSEC(XNC(LM(IB-1))+1)
           PECUM(NC(LM(IB-1))+1) = LISEC(NC(LM(IB-1))+1)
        NCMO(LM(IB-1)+1)=XNCMO(LM(IB-1)+1)-XNC(LM(IB-1)-1)+NC(LM(IB-1))
        DO 9253 J1=NC(LM(IB-1))+2,
     :XNC(LM(IB-1)+1)-XNC(LM(IB-1)-1)+NC(LM(IB-1))
           YISEC(J1)=XYISEC(J1+J2)
           LISEC(J1)=XLISEC(J1+J2)
           SECUM(J1)=XSECUM(J1+J2)
           LYSEC(J1)=XLYSEC(J1+J2)
           LI = LISEC(J1)-LISEC(J1-1)
           YI = YISEC(J1)-YISEC(J1-1)
           PECUM(J1) = PECUM(J1-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(LM(IB-1)+1).GE.J1)THEN
                SECMIN(J1)=SECUM(J1)
C                PECMIN(J1)=PECUM(J1)
              ELSE
                SECMIN(J1)=SECMIN(J1-1)+YI*LISEC(NCMO(1))
C                PECMIN(J1)=PECMIN(J1-1)
              ENDIF
 9253   CONTINUE
        NC(LM(IB-1)+1)=XNC(LM(IB-1)+1)-XNC(LM(IB-1)-1)+NC(LM(IB-1))
C fin if sur IB
        ENDIF
        DO 2253 I=LM(IB-1)+2,LM(IB)-1
C on met le min pour le cas o� il n'y a pas de lit majeur d'un cote
C dans le cas ou il n' y a aucun lit majeur une correction est faite apr
c           YM=0.5*(XYISEC(MIN(XNCMO(I-1),XNC(I-1)))
c     :     +XYISEC(MIN(XNCMO(I),XNC(I))))
C modif du 19/10/21
c           YM=0.5*(XYISEC(XNCmo(I-1))
c     :     +XYISEC(XNCmo(I)))
           ym=min(XYISEC(XNCmo(I-1)),XYISEC(XNCmo(I)))
           NCMO(I)=0
           J1=XNC(I-2)+1
           J2=XNC(I-1)+1
           JJ=NC(I-1)+1
           YISEC(JJ)=0.
           SECUM(JJ)=0.
             LISEC(JJ)=0.5*(XLISEC(J1)+XLISEC(J2))
             PECUM(JJ)=LISEC(JJ)
           JJ=JJ+1
           J1=J1+1
           J2=J2+1
 8255      IF(XYISEC(J1).LT.XYISEC(J2)-EPS)THEN
               YISEC(JJ)=XYISEC(J1)
               LISEC(JJ)=0.5*(XLISEC(J1)+DETL(1,YISEC(JJ),J2-1))
               J1=J1+1
           ELSE IF(XYISEC(J1).GT.XYISEC(J2)+EPS)THEN
               YISEC(JJ)=XYISEC(J2)
               LISEC(JJ)=0.5*(XLISEC(J2)+DETL(1,YISEC(JJ),J1-1))
               J2=J2+1
           ELSE
             YISEC(JJ)=0.5*(XYISEC(J1)+XYISEC(J2))
             LISEC(JJ)=0.5*(XLISEC(J1)+XLISEC(J2))
               J1=J1+1
               J2=J2+1
           ENDIF
              IF(NCMO(I).EQ.0)THEN
C modif du 19/10/21
                IF(YISEC(JJ).GT.YM-eps)THEN
c                  IF(ABS(YISEC(JJ)-YM).GT.ABS(YISEC(JJ-1)-YM))THEN
c                    NCMO(I)=JJ-1
c                  ELSE
                    NCMO(I)=JJ
c                  ENDIF
                ENDIF
              ENDIF
              LI = LISEC(JJ)-LISEC(JJ-1)
              YI = YISEC(JJ)-YISEC(JJ-1)
C              LYSEC(JJ-1) = LI/YI
                       IF(YI.GT.EPS)THEN
                LYSEC(JJ-1) = LI/YI
                    ELSE
                            LYSEC(JJ-1)=0.
                       ENDIF
              SECUM(JJ) = SECUM(JJ-1)+YI*LISEC(JJ-1)
     :+LI*YI*0.5
              PECUM(JJ) = PECUM(JJ-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(I).EQ.0)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSEIF(NCMO(I).EQ.JJ)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSE
                SECMIN(JJ)=SECMIN(JJ-1)+YI*LISEC(NCMO(I))
              ENDIF
           JJ=JJ+1
C...............ARRET QUAND ON ATTEINT LE HAUT DE LA MAILLE I OU I-1

C modification : on interpole tant qu on peut
c   IF(J2.LT.XNC(I)+1.AND.J1.LT.XNC(I-1)+1)GO TO 8255
                IF(J2.EQ.XNC(I)+1)THEN
                              DO J=J1,XNC(I-1)
               YISEC(JJ)=0.5*(XYISEC(J2-1)+XYISEC(J))
               LISEC(JJ)=0.5*(XLISEC(J2-1)+XLISEC(J))
              IF(NCMO(I).EQ.0)THEN
C modif du 19/10/21
                IF(YISEC(JJ).GT.YM-eps)THEN
c                  IF(ABS(YISEC(JJ)-YM).GT.ABS(YISEC(JJ-1)-YM))THEN
c                    NCMO(I)=JJ-1
c                  ELSE
                    NCMO(I)=JJ
c                  ENDIF
                ENDIF
              ENDIF
              LI = LISEC(JJ)-LISEC(JJ-1)
              YI = YISEC(JJ)-YISEC(JJ-1)
                       IF(YI.GT.EPS)THEN
                LYSEC(JJ-1) = LI/YI
                    ELSE
                            LYSEC(JJ-1)=0.
                       ENDIF
              SECUM(JJ) = SECUM(JJ-1)+YI*LISEC(JJ-1)
     :+LI*YI*0.5
              PECUM(JJ) = PECUM(JJ-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(I).EQ.0)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSEIF(NCMO(I).EQ.JJ)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSE
                SECMIN(JJ)=SECMIN(JJ-1)+YI*LISEC(NCMO(I))
              ENDIF
              JJ=JJ+1
C fin boucle sur J
                  ENDDO
                            ELSEIF(J1.EQ.XNC(I-1)+1)THEN
                              DO J=J2,XNC(I)
               YISEC(JJ)=0.5*(XYISEC(J1-1)+XYISEC(J))
               LISEC(JJ)=0.5*(XLISEC(J1-1)+XLISEC(J))
              IF(NCMO(I).EQ.0)THEN
C modif du 19/10/21
                IF(YISEC(JJ).GT.YM-eps)THEN
c                  IF(ABS(YISEC(JJ)-YM).GT.ABS(YISEC(JJ-1)-YM))THEN
c                    NCMO(I)=JJ-1
c                  ELSE
                    NCMO(I)=JJ
c                  ENDIF
                ENDIF
              ENDIF
              LI = LISEC(JJ)-LISEC(JJ-1)
              YI = YISEC(JJ)-YISEC(JJ-1)
                       IF(YI.GT.EPS)THEN
                LYSEC(JJ-1) = LI/YI
                    ELSE
                            LYSEC(JJ-1)=0.
                       ENDIF
              SECUM(JJ) = SECUM(JJ-1)+YI*LISEC(JJ-1)
     :+LI*YI*0.5
              PECUM(JJ) = PECUM(JJ-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(I).EQ.0)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSEIF(NCMO(I).EQ.JJ)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSE
                SECMIN(JJ)=SECMIN(JJ-1)+YI*LISEC(NCMO(I))
              ENDIF
              JJ=JJ+1
C fin boucle sur J
                       ENDDO
C on continue car pas atteint xnc
                            ELSE
                              GO TO 8255
                         ENDIF
C on vient de finir interpolation en centre maille
c           LYSEC(JJ-1)=0.
           LYSEC(JJ-1)=0.5*(XLYSEC(XNC(I))+XLYSEC(XNC(I-1)))
           NC(I)=JJ-1
C cas ou il n'y a pas de lit majeur sur les 2 interfaces
           IF(XNCMO(I-1).EQ.XNC(I-1).AND.
     :     XNCMO(I).EQ.XNC(I))NCMO(I)=NC(I)
C ce cas ne se produit normalement pas
           IF(NCMO(I).EQ.0)NCMO(I)=NC(I)
 2253     CONTINUE
C traitement derniere intermaille=derniere section
           JJ=1+NC(LM(ib)-1)
           J1=XNC(LM(ib)-2)+1
           YISEC(JJ)=0.
           LISEC(JJ)=XLISEC(J1)
           SECUM(JJ)=0.
           LYSEC(JJ)=XLYSEC(J1)
           PECUM(JJ) =LISEC(JJ)
           JJ=JJ+1
           NCMO(LM(ib))=XNCMO(Lm(ib)-1)-XNC(Lm(ib)-2)+NC(LM(IB)-1)
        DO 8256 J1=XNC(LM(ib)-2)+2,XNC(LM(ib)-1)
           JJ=J1-XNC(LM(ib)-2)+NC(LM(ib)-1)
           YISEC(JJ)=XYISEC(J1)
           LISEC(JJ)=XLISEC(J1)
           SECUM(JJ)=XSECUM(J1)
           LYSEC(JJ)=XLYSEC(J1)
           LI = LISEC(JJ)-LISEC(JJ-1)
           YI = YISEC(JJ)-YISEC(JJ-1)
           PECUM(JJ) = PECUM(JJ-1)+2.*SQRT(.25*(LI**2.)+YI**2.)
              IF(NCMO(LM(ib)).GE.JJ)THEN
                SECMIN(JJ)=SECUM(JJ)
              ELSE
                SECMIN(JJ)=SECMIN(JJ-1)+YI*LISEC(NCMO(LM(IB)))
              ENDIF
           JJ=JJ+1
 8256   CONTINUE
        NC(LM(ib))=JJ-1
C fin boucle sur IB
      ENDDO
c         do I=LM(2),LM(3)
c           do jj=nc(i-1)+1,nc(i)
c
c           write(*,*)i,jj,lisec(jj),yisec(jj),secum(jj)
c              enddo
c         enddo
        IF(NC(LM(nbb)).GT.LNCMAX)THEN
          WRITE(*,*)'TROP DE COUPLES LARGEUR-COTES'
          WRITE(*,*)'NC=',NC(LM(nbb)),' >',LNCMAX
          STOP
         ENDIF

C fente de preisman : on change pecum
      if(iffente)then
      DO IB=1,NBB
        DO I=LM(IB-1)+1,LM(IB)
        k=nc(i)+1
                ncfente(i)=k
        do j=nc(i)-1, nc(i-1)+3,-1
                  if(lisec(j).lt.dlfente)then
                   if(lisec(j+1).lt.dlfente)then
                     If(k.eq.nc(i)+1)then
                         k=j
                   elseif(k.eq.j+1)then
                           k=j
                   endif
           endif
          endif
c fin du do sur j
        enddo
                if(k.lt.nc(i))then
          ncfente(i)=k
              do j=k+1,nc(i)
                    pecum(j)=pecum(k)
                  enddo
        endif
c fin du do sur I
        enddo
c fin du do sur ib
        enddo
C fin du if sur iffente
       endif

      RETURN
      END

C************************************************************************
      SUBROUTINE LECONF
C************************************************************************
C      SON ROLE : LECTURE DU FICHIER CONFL CONTENANT LES INDICATIONS
C      SUR LES CONFLUENCES
C***********************************************************************
      INTEGER NBMAX

      PARAMETER(NBMAX=150)

      CHARACTER*20 ETUDE
      INTEGER NCONF,CONFLU(NBMAX,3),I,NCONF2,NBB,ll,LM(0:NBMAX)
      DOUBLE PRECISION ALP1(NBMAX),ALP2(NBMAX),ALP3(NBMAX)
      DOUBLE PRECISION ALP10(NBMAX),ALP20(NBMAX),ALP30(NBMAX)

      COMMON/NCONFL/NCONF,NCONF2,CONFLU
      COMMON/NOMETU/ETUDE
      COMMON/ALP/ALP1,ALP2,ALP3
      COMMON/NBIEF/NBB
      COMMON/PHYS/LM,LL

cliq         NBB=1
cliq         RETURN
      OPEN(50,FILE = 'confl.'//ETUDE,STATUS='OLD',ERR=111)
      READ(50,*,ERR=111)NBB,NCONF2,NCONF
      DO 1 I=1,NCONF2
        READ(50,*)CONFLU(I,1),CONFLU(I,2),CONFLU(I,3)
        READ(50,*)ALP10(I),ALP20(I),ALP30(I)
        ALP1(I)=COS((ALP20(I)-ALP30(I))*3.14159/180.)
        ALP2(I)=COS((ALP20(I)-ALP10(I))*3.14159/180.)
        ALP3(I)=COS((ALP30(I)-ALP10(I))*3.14159/180.)
        WRITE(*,*) 'CONFLUENCE ',I
        WRITE(*,*) 'BIEF AVAL ', CONFLU(I,1)
        WRITE(*,*) 'BIEFS AMONT ', CONFLU(I,2),CONFLU(I,3)
 1    CONTINUE
      DO 2 I=NCONF2+1,NCONF2+NCONF
        READ(50,*)CONFLU(I,1),CONFLU(I,2),CONFLU(I,3)
        READ(50,*)ALP10(I),ALP20(I),ALP30(I)
        ALP1(I)=COS((ALP20(I)-ALP30(I))*3.14159/180.)
        ALP2(I)=COS((ALP20(I)-ALP10(I))*3.14159/180.)
        ALP3(I)=COS((ALP30(I)-ALP10(I))*3.14159/180.)
        WRITE(*,*) 'DEFLUENCE ',I
        WRITE(*,*) 'BIEF AMONT ', CONFLU(I,1)
        WRITE(*,*) 'BIEFS AVAL ', CONFLU(I,2),CONFLU(I,3)
 2    CONTINUE
      NCONF=NCONF+NCONF2
      CLOSE (50)
      RETURN
 111  NBB=1
      RETURN
      END
C***********************************************************************
      SUBROUTINE CONFLUE
C***********************************************************************
C                                                                      C
C   SON ROLE : INTRODUCTION AU PROBLEME DE RIEMANN EN CONFLUENCE       C
C ici vmd,vpd,ymd,ypd sont a tn+1/2 depuis le 11 mars 2011
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C

      INTEGER NBMAX
      INTEGER LMAX,LL,J1,J2,J3
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LDETYJ,LM(0:NBMAX),LDETSJ
      DOUBLE PRECISION V10,V20,V30,Y10,Y20,Y30
     +    ,VMD(LMAX),VPD(LMAX)
     +    ,FLS(LMAX),FLQ(LMAX)

      INTEGER IJ1,IJ2,IJ3
      INTEGER CONFLU(NBMAX,3),IB,I,NCONF,NCONF2
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION SNP1(LMAX),QNP1(LMAX)
     :,SC1,SC2,YMD(LMAX),YPD(LMAX)
      DOUBLE PRECISION ALP1(NBMAX),ALP2(NBMAX),ALP3(NBMAX)
      DOUBLE PRECISION DETSN,DETP,DETYN
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
         DOUBLE PRECISION SC3,VMAX,coef,V0,coef2
C         DOUBLE PRECISION FLQ1,FLQ2,FLQ3,SC3,VMAX,coef,V0,coef2
      DOUBLE PRECISION ZEROD

      EXTERNAL LDETYJ,LDETSJ
      EXTERNAL DETSN,DETP,DETYN

      COMMON/VITTD/VMD,VPD
      COMMON/DDFLUX/FLS,FLQ
      COMMON/PHYS/LM,LL
      COMMON/MALTNP/SNP1,QNP1
      COMMON/MAILTN/SN,QN
      COMMON/YMPD/YMD,YPD
      COMMON/NCONFL/NCONF,NCONF2,CONFLU
      COMMON/ALP/ALP1,ALP2,ALP3
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      ZEROD=0.D0

C alp1 = 2-3 alp2 = 2-1 alp3 = 3-1

      DO 1 I=1,NCONF2
C IB numero du bief aval conflunece
       IB=CONFLU(I,1)
       J1=LM(IB-1)+1
       J2=LM(CONFLU(I,2))-1
       J3=LM(CONFLU(I,3))-1
C les variables sont a tn+1
       Y10 = YPD(J1)
       V10 = VPD(J1)
       Y20 = YMD(J2)
       V20 = VMD(J2)
       Y30 = YMD(J3)
       V30 = VMD(J3)
c       V10 = VPD(J1)*ALP1(I)
c       V20 = VMD(J2)*ALP2(I)
c       V30 = VMD(J3)*ALP3(I)
C       FLQ1=FLQ(J1)
C          FLQ2=FLQ(J2)
C          FLQ3=FLQ(J3)
C modification du 25/8/16 : vitesse anormale portee de 2 a 20=2*10 m/s
          VMAX=2.*max(abs(v10),abs(v20),abs(v30),10.D0)

C030111       IF(V10*V20.GE.0.)THEN
C030111         IF(V10*V30.GE.0.) THEN
C030111C vitesses de meme signe= aval reste 1
c                 write(*,*)'i cas 1c=',i,j1,j2,j3,y10,y20,y30
c           write(*,*)'1',I,Y10,Y20,Y30,V10,V20,V30
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y20,V20,Y10+xctdf(J1)-xctdf(J2),V10*ALP2(I),SC2)
      CALL RICONFL(J3,Y30,V30,Y10+xctdf(J1)-xctdf(J3),V10*ALP3(I),SC3)
c      CALL RICONFL(J2,Y20,V20,Y10+xctdf(J1)-xctdf(J2),V10,SC2)
c      CALL RICONFL(J3,Y30,V30,Y10+xctdf(J1)-xctdf(J3),V10,SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,Y20,V20,xctdf(J1)-xctdf(J2),ZEROD,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,Y30,V30,xctdf(J1)-xctdf(J3),ZEROD,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
C fin du if sur y10
               endif
           IJ2=LDETSJ(1,SC2,J2)
           Y20=DETYN(1,SC2,IJ2)
           IJ3=LDETSJ(1,SC3,J3)
           Y30=DETYN(1,SC3,IJ3)
              if(Y20.LT.EPSY)THEN
                if(Y30.LT.EPSY)THEN
                  Y10=0.
                ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
                endif
              elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
              else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
              endif
              IF(y10.lt.epsy)then
                FLS(J1)=0.
                IF(fls(j2)*fls(j3).GT.0.)then
                  fls(j2)=0.
                  fls(j3)=0.
                  flq(j2)=detp(Y20,IJ2)
                  flq(j3)=detp(y30,ij3)
                elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                  coef=abs(fls(j3))/abs(fls(J2))
                  flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                  fls(j2)=coef*fls(j2)
                elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                   coef=abs(fls(j2))/abs(fls(J3))
                   flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                   fls(j3)=coef*fls(j3)
                endif
                sc1=0
                flq(j1)=0.
C si y10 non nul
          else
            IJ1=LDETYJ(1,Y10,J1)
            SC1=DETSN(1,Y10,IJ1)
            FLS(J1)=FLS(J2)+FLS(J3)
            V0=abs(fls(j1)/sc1)
            if(v0.gt.vmax)Then
              coef=vmax/v0
              IF(fls(j2)*fls(j3).GT.0.)then
                fls(j2)=coef*fls(j2)
                fls(j3)=coef*fls(j3)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
              elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                coef2=1.+(coef-1.)*fls(j1)/fls(j2)
c               fls(j2)=fls(j2)+(coef-1.)*fls(j1)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                fls(j2)=coef2*fls(j2)
              elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                coef2=1.+(coef-1.)*fls(j1)/fls(j3)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                fls(j3)=coef2*fls(j3)
              endif
             fls(j1)=coef*fls(j1)
C fin du if sur v0
           endif
             FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,ALP2(I)*FLQ(J2)+ALP3(I)*FLQ(J3)
     :-ALP2(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
C150413                   if(2.*sc2.gt.sn(j2+1))then
                   if(sc2.gt.eps)then
C150413                         SNP1(J2+1)=2.*SC2-sn(j2+1)
C150413                     QNP1(J2+1)=2.*FLS(J2)-qn(j2+1)
                         SNP1(J2+1)=SC2
                     QNP1(J2+1)=FLS(J2)
C150413                         if(qnp1(j2+1)*fls(j2).lt.0.)then
C150413                                 qnp1(j2+1)=0.
C150413                             fls(j2)=0.5*qn(j2+1)
C150413                     endif
                   else
                          snp1(j2+1)=0.
                      qnp1(j2+1)=0.
C150413                          fls(j2)=0.5*qn(j2+1)
              fls(j2)=0.
                          flq(j2)=0.
                        endif
c           SNP1(J2+1)=SC2
c           QNP1(J2+1)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
                   if(sc3.gt.eps)then
                         SNP1(J3+1)=SC3
                     QNP1(J3+1)=FLS(J3)
C150413                   if(2.*sc3.gt.sn(j3+1))then
C150413                         SNP1(J3+1)=2.*SC3-sn(j3+1)
C150413                     QNP1(J3+1)=2.*FLS(J3)-qn(j3+1)
C150413                         if(qnp1(j3+1)*fls(j3).lt.0.)then
C150413                                 qnp1(j3+1)=0.
C150413                             fls(j3)=0.5*qn(j3+1)
C150413                     endif
                   else
                          snp1(j3+1)=0.
                      qnp1(j3+1)=0.
C150413                          fls(j3)=0.5*qn(j3+1)
                          fls(j3)=0.
                          flq(j3)=0.
                        endif
C           SNP1(J3+1)=SC3
C           QNP1(J3+1)=FLS(J3)
C                 FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
C                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
                   if(sc1.gt.eps)then
                         SNP1(J1)=SC1
                     QNP1(J1)=FLS(J1)
C150413                   if(2.*sc1.gt.sn(j1))then
C150413                         SNP1(J1)=2.*SC1-sn(j1)
C150413                     QNP1(J1)=2.*FLS(J1)-qn(j1)
C150413                         if(qnp1(j1)*fls(j1).lt.0.)then
C150413                                 qnp1(j1)=0.
C150413                             fls(j1)=0.5*qn(j1)
C150413                     endif
                   else
                          snp1(j1)=0.
                      qnp1(j1)=0.
C150413                          fls(j1)=0.5*qn(j1)
                          fls(j1)=0.
                          flq(j1)=0.
                        endif
C           SNP1(J1)=SC1
C           QNP1(J1)=FLS(J1)
C           FLS(J1)=0.5*(FLS(J1)+QN(J1))
C                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
c           write(*,*)'1b',J1,J2,J3,Y10,Y20,Y30,V10,V20,V30
c           write(*,*)SC1,SC2,SNP1(J1),FLS(J2),FLS(J3),FLQ(J2),FLQ(J3)
c           write(*,*)FLS(J1),FLQ(J1),DETP(Y20,IJ2),DETP(Y30,IJ3)
c                 write(*,*)sn(j1),sn(j2+1),sn(j3+1),DETP(Y10,IJ)
c                 write(*,*)alp2(I),alp3(i)
c                 write(*,*)ypd(J1),ymd(j2),ymd(J3),vpd(J1),vmd(j2),vmd(J3)
c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111         ELSE
C030111C v30 De signe different = defluence amont =2
C030111c                 write(*,*)'i cas 2c=',i,j1,j2,j3,y10,y20,y30
C030111c           write(*,*)'2',I,Y10,Y20,Y30,V10,V20,V30
C030111           if(Y20.GT.EPSY)THEN
C030111      CALL RICONFL(J1,Y20+xctdf(J2)-xctdf(J1),V20,Y10,V10,SC1)
C030111             V30=-V30
C030111      CALL RICONFL(J3,Y20+xctdf(J2)-xctdf(J3),V20,Y30,V30,SC3)
C030111             FLS(J3)=-FLS(J3)
C030111           else
C030111                if(Y10.GT.xctdf(J2)-xctdf(J1))then
C030111      CALL RICONFL(J1,xctdf(J2)-xctdf(J1),ZEROD,Y10,V10,SC1)
C030111             else
C030111      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
C030111                endif
C030111                if(Y30.GT.xctdf(J2)-xctdf(J3))then
C030111               V30=-V30
C030111      CALL RICONFL(J3,xctdf(J2)-xctdf(J3),ZEROD,Y30,V30,SC3)
C030111               FLS(J3)=-FLS(J3)
C030111             else
C030111      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
C030111                endif
C030111C fin du if sur Y20
C030111               endif
C030111           IJ1=LDETSJ(1,SC1,J1)
C030111           IJ3=LDETSJ(1,SC3,J3)
C030111           Y10=DETYN(1,SC1,IJ1)
C030111           Y30=DETYN(1,SC3,IJ3)
C030111           if(Y10.LT.EPSY)THEN
C030111                 IF(Y30.LT.EPSY)then
C030111                      Y20=0.
C030111                    else
C030111                Y20=Y30+xctdf(J3)-xctdf(J2)
C030111                 endif
C030111              elseif(Y30.LT.EPSY)THEN
C030111              Y20=Y10+xctdf(J1)-xctdf(J2)
C030111              else
C030111      Y20=(sc1*(Y10+xctdf(J1))+sc3*(Y30+xctdf(J3)))/(sc1+sc3)-xctdf(J2)
C030111              endif
C030111                 IF(y20.lt.epsy)then
C030111                        FLS(J2)=0.
C030111                     IF(fls(j1)*fls(j3).LT.0.)then
C030111                             fls(j1)=0.
C030111                          fls(j3)=0.
C030111                             flq(j1)=detp(Y10,IJ1)
C030111                             flq(j3)=detp(y30,ij3)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j3)))then
C030111                             coef=abs(fls(j3))/abs(fls(J1))
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             fls(j1)=coef*fls(j1)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j1)))then
C030111                             coef=abs(fls(j1))/abs(fls(J3))
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                             fls(j3)=coef*fls(j3)
C030111                        endif
C030111                     sc2=0
C030111                        flq(j2)=0.
C030111C si y20 non nul
C030111                 else
C030111             FLS(J2)=FLS(J1)-FLS(J3)
C030111             IJ2=LDETYJ(1,Y20,J2)
C030111                   SC2=DETSN(1,Y20,IJ2)
C030111                      V0=abs(fls(j2)/sc2)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j1)*fls(j3).lT.0.)then
C030111                             fls(j1)=coef*fls(j1)
C030111                          fls(j3)=coef*fls(j3)
C030111                             flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j3)))then
C030111                             coef2=1.+(coef-1.)*fls(j2)/fls(j1)
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
C030111                             fls(j1)=coef2*fls(j1)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j1)))then
C030111                             coef2=1.+(coef-1.)*fls(j2)/fls(j3)
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
C030111                             fls(j3)=coef2*fls(j3)
C030111                        endif
C030111             fls(j2)=coef*fls(j2)
C030111C fin du if sur v0
C030111                        endif
C030111             FLQ(J2)=DETP(Y20,IJ2)+
C030111     :MAX(ZEROD,FLQ(J1)+FLQ(J3)
C030111     :-DETP(Y30,IJ3)-DETP(Y10,IJ1))
C030111c fin du if sur y20=0.
C030111           ENDIF
C030111      FLQ(J3)=FLQ(J3)-(1.-ALP1(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
C030111      FLQ(J1)=FLQ(J1)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J1)-DETP(Y10,IJ1))
C030111           SNP1(J2+1)=SC2
C030111           QNP1(J2+1)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J3+1)=SC3
C030111           QNP1(J3+1)=FLS(J3)
C030111                 FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111           SNP1(J1)=SC1
C030111           QNP1(J1)=FLS(J1)
C030111           FLS(J1)=0.5*(FLS(J1)+QN(J1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111c           write(*,*)'2b',Y10,Y20,Y30,V10,V20,V30
C030111c           write(*,*)SC1,SC2,SNP1(J2+1),FLS(J1),FLS(J3),FLQ(J1),FLQ(J3)
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111         ENDIF
C030111       ELSEIF(V10*V30.GT.0.) THEN
C030111C v20 de signe different = defluence amont =3
C030111c                 write(*,*)'i cas 3c=',i,j1,j2,j3,y10,y20,y30
C030111c          write(*,*)'3',I,Y10,Y20,Y30,V10,V20,V30
C030111         if(Y30.GT.EPSY)THEN
C030111      CALL RICONFL(J1,Y30+xctdf(J3)-xctdf(J1),V30,Y10,V10,SC1)
C030111           V20=-V20
C030111      CALL RICONFL(J2,Y30+xctdf(J3)-xctdf(J2),V30,Y20,V20,SC2)
C030111           FLS(J2)=-FLS(J2)
C030111         else
C030111              if(Y10.GT.xctdf(J3)-xctdf(J1))then
C030111      CALL RICONFL(J1,xctdf(J3)-xctdf(J1),ZEROD,Y10,V10,SC1)
C030111           else
C030111      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
C030111              endif
C030111              if(Y20.GT.xctdf(J3)-xctdf(J2))then
C030111             V20=-V20
C030111      CALL RICONFL(J2,xctdf(J3)-xctdf(J2),ZEROD,Y20,V20,SC2)
C030111             FLS(J2)=-FLS(J2)
C030111           else
C030111      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
C030111              endif
C030111C fn du if sur y30
C030111         endif
C030111c           FLQ(J2)=-FLQ(J2)
C030111           IJ2=LDETSJ(1,SC2,J2)
C030111           IJ1=LDETSJ(1,SC1,J1)
C030111           Y20=DETYN(1,SC2,IJ2)
C030111           Y10=DETYN(1,SC1,IJ1)
C030111           if(Y10.LT.EPSY)THEN
C030111                IF(Y20.LT.EPSY)THEN
C030111                  Y30=0.
C030111                else
C030111               Y30=Y20+xctdf(J2)-xctdf(J3)
C030111                endif
C030111              elseif(y20.LT.EPSY)THEN
C030111             Y30=Y10+xctdf(J1)-xctdf(J3)
C030111              else
C030111      Y30=(sc1*(Y10+xctdf(J1))+sc2*(Y20+xctdf(J2)))/(sc1+sc2)-xctdf(J3)
C030111              endif
C030111                 IF(y30.lt.epsy)then
C030111                        FLS(J3)=0.
C030111                     IF(fls(j1)*fls(j2).LT.0.)then
C030111                             fls(j1)=0.
C030111                          fls(j2)=0.
C030111                             flq(j1)=detp(Y10,IJ1)
C030111                             flq(j2)=detp(y20,ij2)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j2)))then
C030111                             coef=abs(fls(j2))/abs(fls(J1))
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             fls(j1)=coef*fls(j1)
C030111                       elseif(abs(fls(j2)).gt.abs(fls(j1)))then
C030111                             coef=abs(fls(j1))/abs(fls(J2))
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             fls(j2)=coef*fls(j2)
C030111                        endif
C030111                     sc3=0.
C030111                        flq(j3)=0.
C030111C si y20 non nul
C030111                 else
C030111             FLS(J3)=-FLS(J2)+FLS(J1)
C030111             IJ3=LDETYJ(1,Y30,J3)
C030111                   sc3=DETSN(1,Y30,IJ3)
C030111                      V0=abs(fls(j3)/sc3)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j1)*fls(j2).lT.0.)then
C030111                             fls(j1)=coef*fls(j1)
C030111                          fls(j2)=coef*fls(j2)
C030111                             flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j2)))then
C030111                             coef2=1.+(coef-1.)*fls(j3)/fls(j1)
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
C030111                             fls(j1)=coef2*fls(j1)
C030111                       elseif(abs(fls(j2)).gt.abs(fls(j1)))then
C030111                             coef2=1.+(coef-1.)*fls(j3)/fls(j2)
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
C030111                             fls(j2)=coef2*fls(j2)
C030111                        endif
C030111             fls(j3)=coef*fls(j3)
C030111C fin du if sur v0
C030111                        endif
C030111             FLQ(J3)=DETP(Y30,IJ3)+
C030111     :MAX(ZEROD,FLQ(J1)+FLQ(J2)
C030111     :-DETP(Y20,IJ2)-DETP(Y10,IJ1))
C030111c fin du if sur y30=0.
C030111           ENDIF
C030111           FLQ(J2)=FLQ(J2)-(1.-ALP1(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
C030111           FLQ(J1)=FLQ(J1)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J1)-DETP(Y10,IJ1))
C030111           SNP1(J2+1)=SC2
C030111           QNP1(J2+1)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J1)=SC1
C030111           QNP1(J1)=FLS(J1)
C030111                 FLS(J1)=0.5*(FLS(J1)+QN(J1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111           SNP1(J3+1)=SC3
C030111           QNP1(J3+1)=FLS(J3)
C030111           FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111c           write(*,*)'3b',Y10,Y20,Y30,V10,V20,V30
C030111c           write(*,*)SC1,SC2,SNP1(J3+1),FLS(J2),FLS(J1),FLQ(J2),FLQ(J1)
C030111       ELSE
C030111C v10 de signe different = situation transitoire  = aval reste 1
C030111c           write(*,*)'4',I,Y10,Y20,Y30,V10,V20,V30
C030111c                 write(*,*)'i cas 4c=',i,j1,j2,j3,y10,y20,y30
C030111           if(Y10.GT.EPSY)THEN
C030111      CALL RICONFL(J2,Y20,V20,Y10+xctdf(J1)-xctdf(J2),V10,SC2)
C030111      CALL RICONFL(J3,Y30,V30,Y10+xctdf(J1)-xctdf(J3),V10,SC3)
C030111           else
C030111                if(Y20.GT.xctdf(J1)-xctdf(J2))then
C030111      CALL RICONFL(J2,Y20,V20,xctdf(J1)-xctdf(J2),ZEROD,SC2)
C030111             else
C030111      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
C030111                endif
C030111                if(Y30.GT.xctdf(J1)-xctdf(J3))then
C030111      CALL RICONFL(J3,Y30,V30,xctdf(J1)-xctdf(J3),ZEROD,SC3)
C030111             else
C030111      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
C030111                endif
C030111C fin du if sur y10
C030111               endif
C030111           IJ2=LDETSJ(1,SC2,J2)
C030111           IJ3=LDETSJ(1,SC3,J3)
C030111           Y20=DETYN(1,SC2,IJ2)
C030111           Y30=DETYN(1,SC3,IJ3)
C030111              if(Y20.LT.EPSY)THEN
C030111                if(Y30.LT.EPSY)THEN
C030111                  Y10=0.
C030111                ELSE
C030111               Y10=Y30+xctdf(J3)-xctdf(J1)
C030111                endif
C030111              elseif(y30.LT.EPSY)then
C030111             Y10=Y20+xctdf(J2)-xctdf(J1)
C030111              else
C030111      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
C030111              endif
C030111                 IF(y10.lt.epsy)then
C030111                        FLS(J1)=0.
C030111                     IF(fls(j2)*fls(j3).GT.0.)then
C030111                             fls(j2)=0.
C030111                          fls(j3)=0.
C030111                             flq(j2)=detp(Y20,IJ2)
C030111                             flq(j3)=detp(y30,ij3)
C030111                        elseif(abs(fls(j2)).gt.abs(fls(j3)))then
C030111                             coef=abs(fls(j3))/abs(fls(J2))
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             fls(j2)=coef*fls(j2)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j2)))then
C030111                             coef=abs(fls(j2))/abs(fls(J3))
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                             fls(j3)=coef*fls(j3)
C030111                        endif
C030111                     sc1=0
C030111                        flq(j1)=0.
C030111C si y10 non nul
C030111                 else
C030111             IJ1=LDETYJ(1,Y10,J1)
C030111                SC1=DETSN(1,Y10,IJ1)
C030111             FLS(J1)=FLS(J2)+FLS(J3)
C030111                      V0=abs(fls(j1)/sc1)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j2)*fls(j3).GT.0.)then
C030111                             fls(j2)=coef*fls(j2)
C030111                          fls(j3)=coef*fls(j3)
C030111                             flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                        elseif(abs(fls(j2)).gt.abs(fls(j3)))then
C030111                             coef2=1.+(coef-1.)*fls(j1)/fls(j2)
C030111c                             fls(j2)=fls(j2)+(coef-1.)*fls(j1)
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
C030111                             fls(j2)=coef2*fls(j2)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j2)))then
C030111                             coef2=1.+(coef-1.)*fls(j1)/fls(j3)
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
C030111                             fls(j3)=coef2*fls(j3)
C030111                        endif
C030111             fls(j1)=coef*fls(j1)
C030111C fin du if sur v0
C030111                        endif
C030111             FLQ(J1)=DETP(Y10,IJ1)+
C030111     :MAX(ZEROD,ALP2(I)*FLQ(J2)+ALP3(I)*FLQ(J3)
C030111     :-ALP2(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y30,IJ3))
C030111c fin du if sur y10=0.
C030111           ENDIF
C030111           SNP1(J2+1)=SC2
C030111           QNP1(J2+1)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J3+1)=SC3
C030111           QNP1(J3+1)=FLS(J3)
C030111                 FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111           SNP1(J1)=SC1
C030111           QNP1(J1)=FLS(J1)
C030111           FLS(J1)=0.5*(FLS(J1)+QN(J1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111c           write(*,*)'4b',Y10,Y20,Y30,V10,V20,V30
C030111c           write(*,*)SC1,SC2,SNP1(J1),FLS(J2),FLS(J3),FLQ(J2),FLQ(J3)
C030111c FIN DU IF SUR LES CAS DE CONFLUENCE
C030111       ENDIF
 1    CONTINUE

C traitement des d�fluences
c       write(*,*)'conflue fin confluences'

      DO 2 I=NCONF2+1,NCONF
C IB numero du bief amont deflunece
       IB=CONFLU(I,1)
       J1=LM(IB)-1
       J2=LM(CONFLU(I,2)-1)+1
       J3=LM(CONFLU(I,3)-1)+1
       Y10 = YMD(J1)
       V10 = VMD(J1)
c       V10 = VMD(J1)*ALP1(I)
       Y20 = YPD(J2)
       V20 = VPD(J2)
c       V20 = VPD(J2)*ALP2(I)
       Y30 = YPD(J3)
       V30 = VPD(J3)
c       V30 = VPD(J3)*ALP3(I)
C       FLQ1=FLQ(J1)
C          FLQ2=FLQ(J2)
C          FLQ3=FLQ(J3)
C modification du 25/8/16 : vitesse anormale portee de 2 a 20=2*10 m/s
          VMAX=2.*max(abs(v10),abs(v20),abs(v30),10.D0)

C030111       IF(V10*V20.GE.0.)THEN
C030111         IF(V10*V30.GE.0.) THEN
C vitesses de meme signe= amont reste 1
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y10+xctdf(J1)-xctdf(J2),V10*ALP2(I),Y20,V20,SC2)
      CALL RICONFL(J3,Y10+xctdf(J1)-xctdf(J3),V10*ALP3(I),Y30,V30,SC3)
c      CALL RICONFL(J2,Y10+xctdf(J1)-xctdf(J2),V10,Y20,V20,SC2)
c      CALL RICONFL(J3,Y10+xctdf(J1)-xctdf(J3),V10,Y30,V30,SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,xctdf(J1)-xctdf(J2),ZEROD,Y20,V20,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,xctdf(J1)-xctdf(J3),ZEROD,Y30,V30,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
              endif
           IJ2=LDETSJ(1,SC2,J2)
           IJ3=LDETSJ(1,SC3,J3)
           Y20=DETYN(1,SC2,IJ2)
           Y30=DETYN(1,SC3,IJ3)
           if(Y20.LT.EPSY)THEN
             if(Y30.LT.EPSY)THEN
               Y10=0.
             ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
             endif
           elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
           else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
           endif
           IF(y10.lt.epsy)then
             FLS(J1)=0.
             IF(fls(j2)*fls(j3).GT.0.)then
               fls(j2)=0.
               fls(j3)=0.
               flq(j2)=detp(Y20,IJ2)
               flq(j3)=detp(y30,ij3)
             elseif(abs(fls(j2)).gt.abs(fls(j3)))then
               coef=abs(fls(j3))/abs(fls(J2))
               flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
               fls(j2)=coef*fls(j2)
             elseif(abs(fls(j3)).gt.abs(fls(j2)))then
               coef=abs(fls(j2))/abs(fls(J3))
               flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               fls(j3)=coef*fls(j3)
             endif
             sc1=0
             flq(j1)=0.
C si y10 non nul
           else
             IJ1=LDETYJ(1,Y10,J1)
             SC1=DETSN(1,Y10,IJ1)
             FLS(J1)=FLS(J2)+FLS(J3)
             V0=abs(fls(j1)/sc1)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j2)*fls(j3).GT.0.)then
                 fls(j2)=coef*fls(j2)
                 fls(j3)=coef*fls(j3)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j2)
c              fls(j2)=fls(j2)+(coef-1.)*fls(j1)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                 fls(j2)=coef2*fls(j2)
               elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j3)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                 fls(j3)=coef2*fls(j3)
               endif
               fls(j1)=coef*fls(j1)
C fin du if sur v0
             endif
           FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,FLQ(J2)+FLQ(J3)
     :-DETP(Y20,IJ2)-DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
           FLQ(J2)=FLQ(J2)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
           FLQ(J3)=FLQ(J3)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
                   if(sc2.gt.eps)then
                         SNP1(J2)=SC2
                     QNP1(J2)=FLS(J2)
C150413                   if(2.*sc2.gt.sn(j2))then
C150413                         SNP1(J2)=2.*SC2-sn(j2)
C150413                     QNP1(J2)=2.*FLS(J2)-qn(j2)
C150413                         if(qnp1(j2)*fls(j2).lt.0.)then
C150413                                 qnp1(j2)=0.
C150413                             fls(j2)=0.5*qn(j2)
C150413                     endif
                   else
                          snp1(j2)=0.
                      qnp1(j2)=0.
C150413                          fls(j2)=0.5*qn(j2)
                          fls(j2)=0.
                          flq(j2)=0.
                        endif
                   if(sc3.gt.eps)then
                         SNP1(J3)=SC3
                     QNP1(J3)=FLS(J3)
C150413                   if(2.*sc3.gt.sn(j3))then
C150413                         SNP1(J3)=2.*SC3-sn(j3)
C150413                     QNP1(J3)=2.*FLS(J3)-qn(j3)
C150413                         if(qnp1(j3)*fls(j3).lt.0.)then
C150413                                 qnp1(j3)=0.
C150413                             fls(j3)=0.5*qn(j3)
C150413                     endif
                   else
                          snp1(j3)=0.
                      qnp1(j3)=0.
                          fls(j3)=0.
C150413                          fls(j3)=0.5*qn(j3)
                          flq(j3)=0.
                        endif
                   if(sc1.gt.eps)then
                         SNP1(J1+1)=SC1
                     QNP1(J1+1)=FLS(J1)
C150413                   if(2.*sc1.gt.sn(j1+1))then
C150413                         SNP1(J1+1)=2.*SC1-sn(j1+1)
C150413                     QNP1(J1+1)=2.*FLS(J1)-qn(j1+1)
C150413                         if(qnp1(j1+1)*fls(j1).lt.0.)then
C150413                                 qnp1(j1+1)=0.
C150413                             fls(j1)=0.5*qn(j1+1)
C150413                     endif
                   else
                          snp1(j1+1)=0.
                      qnp1(j1+1)=0.
                          fls(j1)=0.
C150413                          fls(j1)=0.5*qn(j1+1)
                          flq(j1)=0.
                        endif
C           SNP1(J2)=SC2
C           QNP1(J2)=FLS(J2)
C           FLS(J2)=0.5*(FLS(J2)+QN(J2))
C           IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C           SNP1(J3)=SC3
C           QNP1(J3)=FLS(J3)
C           FLS(J3)=0.5*(FLS(J3)+QN(J3))
C           IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C           SNP1(J1+1)=SC1
C           QNP1(J1+1)=FLS(J1)
C           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
C           IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111         ELSE
C030111C v30 De signe different = confluence avec aval 2
C030111           if(Y20.GT.EPSY)THEN
C030111      CALL RICONFL(J1,Y10,V10,Y20+xctdf(J2)-xctdf(J1),V20,SC1)
C030111             V30=-V30
C030111      CALL RICONFL(J3,Y30,V30,Y20+xctdf(J2)-xctdf(J3),V20,SC3)
C030111             FLS(J3)=-FLS(J3)
C030111           else
C030111                IF(Y10.GT.xctdf(J2)-xctdf(J1))THEN
C030111      CALL RICONFL(J1,Y10,V10,xctdf(J2)-xctdf(J1),ZEROD,SC1)
C030111             ELSE
C030111      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
C030111                endif
C030111             V30=-V30
C030111                IF(Y30.GT.xctdf(J2)-xctdf(J3))THEN
C030111      CALL RICONFL(J3,Y30,V30,xctdf(J2)-xctdf(J3),ZEROD,SC3)
C030111             ELSE
C030111      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
C030111                endif
C030111             FLS(J3)=-FLS(J3)
C030111C fin du if sur y20
C030111              endif
C030111           IJ1=LDETSJ(1,SC1,J1)
C030111           IJ3=LDETSJ(1,SC3,J3)
C030111           Y10=DETYN(1,SC1,IJ1)
C030111           Y30=DETYN(1,SC3,IJ3)
C030111           if(Y10.LT.EPSY)THEN
C030111                IF(Y30.LT.EPSY)then
C030111                     Y20=0.
C030111                   else
C030111               Y20=Y30+xctdf(J3)-xctdf(J2)
C030111                endif
C030111              elseif(Y30.LT.EPSY)THEN
C030111             Y20=Y10+xctdf(J1)-xctdf(J2)
C030111              else
C030111      Y20=(sc1*(Y10+xctdf(J1))+sc3*(Y30+xctdf(J3)))/(sc1+sc3)-xctdf(J2)
C030111              endif
C030111                 IF(y20.lt.epsy)then
C030111                        FLS(J2)=0.
C030111                     IF(fls(j1)*fls(j3).LT.0.)then
C030111                             fls(j1)=0.
C030111                          fls(j3)=0.
C030111                             flq(j1)=detp(Y10,IJ1)
C030111                             flq(j3)=detp(y30,ij3)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j3)))then
C030111                             coef=abs(fls(j3))/abs(fls(J1))
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             fls(j1)=coef*fls(j1)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j1)))then
C030111                             coef=abs(fls(j1))/abs(fls(J3))
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                             fls(j3)=coef*fls(j3)
C030111                        endif
C030111                     sc2=0
C030111                        flq(j2)=0.
C030111C si y20 non nul
C030111                 else
C030111             FLS(J2)=FLS(J1)-FLS(J3)
C030111             IJ2=LDETYJ(1,Y20,J2)
C030111             SC2=DETSN(1,Y20,IJ2)
C030111                      V0=abs(fls(j2)/sc2)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j1)*fls(j3).lT.0.)then
C030111                             fls(j1)=coef*fls(j1)
C030111                          fls(j3)=coef*fls(j3)
C030111                             flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j3)))then
C030111                             coef2=1.+(coef-1.)*fls(j2)/fls(j1)
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
C030111                             fls(j1)=coef2*fls(j1)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j1)))then
C030111                             coef2=1.+(coef-1.)*fls(j2)/fls(j3)
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
C030111                             fls(j3)=coef2*fls(j3)
C030111                        endif
C030111             fls(j2)=coef*fls(j2)
C030111C fin du if sur v0
C030111                        endif
C030111             FLQ(J2)=DETP(Y20,IJ2)+
C030111     :MAX(ZEROD,ALP2(I)*FLQ(J1)+ALP1(I)*FLQ(J3)
C030111     :-ALP2(I)*DETP(Y10,IJ1)-ALP1(I)*DETP(Y30,IJ3))
C030111c fin du if sur y20=0.
C030111           ENDIF
C030111           SNP1(J2)=SC2
C030111           QNP1(J2)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J3)=SC3
C030111           QNP1(J3)=FLS(J3)
C030111                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111           SNP1(J1+1)=SC1
C030111           QNP1(J1+1)=FLS(J1)
C030111           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111         ENDIF
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111       ELSEIF(V10*V30.GT.0.) THEN
C030111C v20 de signe different = confluence aval =3
C030111         if(Y30.GT.EPSY)THEN
C030111      CALL RICONFL(J1,Y10,V10,Y30+xctdf(J3)-xctdf(J1),V30,SC1)
C030111           V20=-V20
C030111      CALL RICONFL(J2,Y20,V20,Y30+xctdf(J3)-xctdf(J2),V30,SC2)
C030111           FLS(J2)=-FLS(J2)
C030111         else
C030111              if(Y10.GT.xctdf(J3)-xctdf(J1))THEN
C030111        CALL RICONFL(J1,Y10,V10,xctdf(J3)-xctdf(J1),ZEROD,SC1)
C030111              else
C030111           call riconfl(J1,Y10,ZEROD,Y10,ZEROD,SC1)
C030111              endif
C030111           V20=-V20
C030111              if(y20.GT.xctdf(J3)-xctdf(J2))THEN
C030111      CALL RICONFL(J2,Y20,V20,xctdf(J3)-xctdf(J2),ZEROD,SC2)
C030111           else
C030111          call riconfl(J2,Y20,ZEROD,Y20,ZEROD,SC2)
C030111              endif
C030111           FLS(J2)=-FLS(J2)
C030111C fin du if sur Y30
C030111            endif
C030111C           FLQ(J2)=-FLQ(J2)
C030111           IJ2=LDETSJ(1,SC2,J2)
C030111           IJ1=LDETSJ(1,SC1,J1)
C030111           Y20=DETYN(1,SC2,IJ2)
C030111           Y10=DETYN(1,SC1,IJ1)
C030111           if(Y10.LT.EPSY)THEN
C030111                IF(Y20.LT.EPSY)THEN
C030111                  Y30=0.
C030111                else
C030111               Y30=Y20+xctdf(J2)-xctdf(J3)
C030111                endif
C030111              elseif(y20.LT.EPSY)THEN
C030111             Y30=Y10+xctdf(J1)-xctdf(J3)
C030111              else
C030111      Y30=(sc2*(Y20+xctdf(J2))+sc1*(Y10+xctdf(J1)))/(sc2+sc1)-xctdf(J3)
C030111              endif
C030111                 IF(y30.lt.epsy)then
C030111                        FLS(J3)=0.
C030111                     IF(fls(j1)*fls(j2).LT.0.)then
C030111                             fls(j1)=0.
C030111                          fls(j2)=0.
C030111                             flq(j1)=detp(Y10,IJ1)
C030111                             flq(j2)=detp(y20,ij2)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j2)))then
C030111                             coef=abs(fls(j2))/abs(fls(J1))
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             fls(j1)=coef*fls(j1)
C030111                       elseif(abs(fls(j2)).gt.abs(fls(j1)))then
C030111                             coef=abs(fls(j1))/abs(fls(J2))
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             fls(j2)=coef*fls(j2)
C030111                        endif
C030111                     sc3=0.
C030111                        flq(j3)=0.
C030111C si y20 non nul
C030111                 else
C030111             FLS(J3)=-FLS(J2)+FLS(J1)
C030111             IJ3=LDETYJ(1,Y30,J3)
C030111                   sc3=DETSN(1,Y30,IJ3)
C030111                      V0=abs(fls(j3)/sc3)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j1)*fls(j2).lT.0.)then
C030111                             fls(j1)=coef*fls(j1)
C030111                          fls(j2)=coef*fls(j2)
C030111                             flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
C030111                             flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                        elseif(abs(fls(j1)).gt.abs(fls(j2)))then
C030111                             coef2=1.+(coef-1.)*fls(j3)/fls(j1)
C030111                           flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
C030111                             fls(j1)=coef2*fls(j1)
C030111                       elseif(abs(fls(j2)).gt.abs(fls(j1)))then
C030111                             coef2=1.+(coef-1.)*fls(j3)/fls(j2)
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
C030111                             fls(j2)=coef2*fls(j2)
C030111                        endif
C030111             fls(j3)=coef*fls(j3)
C030111C fin du if sur v0
C030111                        endif
C030111           FLQ(J3)=DETP(Y30,IJ3)+
C030111     :MAX(ZEROD,ALP1(I)*FLQ(J2)+ALP3(I)*FLQ(J1)
C030111     :-ALP1(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y10,IJ1))
C030111c fin du if sur y30=0.
C030111            ENDIF
C030111           SNP1(J2)=SC2
C030111           QNP1(J2)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J3)=SC3
C030111           QNP1(J3)=FLS(J3)
C030111                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111           SNP1(J1+1)=SC1
C030111           QNP1(J1+1)=FLS(J1)
C030111           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111       ELSE
C030111C v10 de signe different = situation transitoire  = amont reste 1
C030111           if(Y10.GT.EPSY)THEN
C030111      CALL RICONFL(J2,Y10+xctdf(J1)-xctdf(J2),V10,Y20,V20,SC2)
C030111      CALL RICONFL(J3,Y10+xctdf(J1)-xctdf(J3),V10,Y30,V30,SC3)
C030111           else
C030111                if(Y20.GT.xctdf(J1)-xctdf(J2))then
C030111      CALL RICONFL(J2,xctdf(J1)-xctdf(J2),ZEROD,Y20,V20,SC2)
C030111             else
C030111      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
C030111                endif
C030111                if(Y30.GT.xctdf(J1)-xctdf(J3))then
C030111      CALL RICONFL(J3,xctdf(J1)-xctdf(J3),ZEROD,Y30,V30,SC3)
C030111             else
C030111      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
C030111                endif
C030111C fin du if sur y10
C030111              endif
C030111           IJ2=LDETSJ(1,SC2,J2)
C030111           IJ3=LDETSJ(1,SC3,J3)
C030111           Y20=DETYN(1,SC2,IJ2)
C030111           Y30=DETYN(1,SC3,IJ3)
C030111              if(Y20.LT.EPSY)THEN
C030111                if(Y30.LT.EPSY)THEN
C030111                  Y10=0.
C030111                ELSE
C030111               Y10=Y30+xctdf(J3)-xctdf(J1)
C030111                endif
C030111              elseif(y30.LT.EPSY)then
C030111             Y10=Y20+xctdf(J2)-xctdf(J1)
C030111              else
C030111      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
C030111              endif
C030111                 IF(y10.lt.epsy)then
C030111                        FLS(J1)=0.
C030111                     IF(fls(j2)*fls(j3).GT.0.)then
C030111                             fls(j2)=0.
C030111                          fls(j3)=0.
C030111                             flq(j2)=detp(Y20,IJ2)
C030111                             flq(j3)=detp(y30,ij3)
C030111                        elseif(abs(fls(j2)).gt.abs(fls(j3)))then
C030111                             coef=abs(fls(j3))/abs(fls(J2))
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             fls(j2)=coef*fls(j2)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j2)))then
C030111                             coef=abs(fls(j2))/abs(fls(J3))
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                             fls(j3)=coef*fls(j3)
C030111                        endif
C030111                     sc1=0
C030111                        flq(j1)=0.
C030111C si y10 non nul
C030111                 else
C030111             IJ1=LDETYJ(1,Y10,J1)
C030111                SC1=DETSN(1,Y10,IJ1)
C030111             FLS(J1)=FLS(J2)+FLS(J3)
C030111                      V0=abs(fls(j1)/sc1)
C030111                      if(v0.gt.vmax)Then
C030111                        coef=vmax/v0
C030111                     IF(fls(j2)*fls(j3).GT.0.)then
C030111                             fls(j2)=coef*fls(j2)
C030111                          fls(j3)=coef*fls(j3)
C030111                             flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
C030111                             flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
C030111                        elseif(abs(fls(j2)).gt.abs(fls(j3)))then
C030111                             coef2=1.+(coef-1.)*fls(j1)/fls(j2)
C030111c                             fls(j2)=fls(j2)+(coef-1.)*fls(j1)
C030111                           flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
C030111                             fls(j2)=coef2*fls(j2)
C030111                       elseif(abs(fls(j3)).gt.abs(fls(j2)))then
C030111                             coef2=1.+(coef-1.)*fls(j1)/fls(j3)
C030111                           flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
C030111                             fls(j3)=coef2*fls(j3)
C030111                        endif
C030111             fls(j1)=coef*fls(j1)
C030111C fin du if sur v0
C030111                        endif
C030111           FLQ(J1)=DETP(Y10,IJ1)+
C030111     :MAX(ZEROD,FLQ(J2)+FLQ(J3)
C030111     :-DETP(Y20,IJ2)-DETP(Y30,IJ3))
C030111c fin du if sur y10=0.
C030111           ENDIF
C030111           FLQ(J2)=FLQ(J2)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
C030111           FLQ(J3)=FLQ(J3)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
C030111           SNP1(J2)=SC2
C030111           QNP1(J2)=FLS(J2)
C030111                 FLS(J2)=0.5*(FLS(J2)+QN(J2))
C030111                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
C030111           SNP1(J3)=SC3
C030111           QNP1(J3)=FLS(J3)
C030111                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
C030111                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
C030111           SNP1(J1+1)=SC1
C030111           QNP1(J1+1)=FLS(J1)
C030111           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
C030111                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C030111c                 write(*,*)fls(j1),fls(j2),fls(j3),flq(j1),flq(j2),flq(j3)
C030111C fin du if sur le type de defluence
C030111       ENDIF
 2    CONTINUE
c       write(*,*)'conflue fin'
      RETURN
      END

C***********************************************************************
      SUBROUTINE CONFLUE2
C***********************************************************************
C                                                                      C
C   SON ROLE : INTRODUCTION AU PROBLEME DE RIEMANN EN CONFLUENCE       C
C ici vmd,vpd,ymd,ypd sont a tn+1/2 depuis le 11 mars 2011
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C

      INTEGER NBMAX
      INTEGER LMAX,LL,J1,J2,J3
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER LDETYJ,LM(0:NBMAX),LDETSJ
      DOUBLE PRECISION V10,V20,V30,Y10,Y20,Y30
     +    ,VMD(LMAX),VPD(LMAX)
     +    ,FLS(LMAX),FLQ(LMAX)

      INTEGER IJ1,IJ2,IJ3
      INTEGER CONFLU(NBMAX,3),IB,I,NCONF,NCONF2
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION SNP1(LMAX),QNP1(LMAX)
     :,SC1,SC2,YMD(LMAX),YPD(LMAX)
      DOUBLE PRECISION ALP1(NBMAX),ALP2(NBMAX),ALP3(NBMAX)
      DOUBLE PRECISION DETSN,DETP,DETYN
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
         DOUBLE PRECISION SC3,VMAX,coef,V0,coef2
C         DOUBLE PRECISION FLQ1,FLQ2,FLQ3,SC3,VMAX,coef,V0,coef2
      DOUBLE PRECISION ZEROD

      EXTERNAL LDETYJ,LDETSJ
      EXTERNAL DETSN,DETP,DETYN

      COMMON/VITTD/VMD,VPD
      COMMON/DDFLUX/FLS,FLQ
      COMMON/PHYS/LM,LL
      COMMON/MALTNP/SNP1,QNP1
      COMMON/MAILTN/SN,QN
      COMMON/YMPD/YMD,YPD
      COMMON/NCONFL/NCONF,NCONF2,CONFLU
      COMMON/ALP/ALP1,ALP2,ALP3
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      ZEROD=0.D0

C alp1 = 2-3 alp2 = 2-1 alp3 = 3-1

      DO 1 I=1,NCONF2
C IB numero du bief aval conflunece
       IB=CONFLU(I,1)
       J1=LM(IB-1)+1
       J2=LM(CONFLU(I,2))-1
       J3=LM(CONFLU(I,3))-1
C les variables sont a tn+1
       Y10 = YPD(J1)
       V10 = VPD(J1)
       Y20 = YMD(J2)
       V20 = VMD(J2)
       Y30 = YMD(J3)
       V30 = VMD(J3)
C modification du 25/8/16 : vitesse anormale portee de 2 a 20=2*10 m/s
          VMAX=2.*max(abs(v10),abs(v20),abs(v30),10.D0)

       IF(V10*V20.GE.0.)THEN
         IF(V10*V30.GE.0.) THEN
C vitesses de meme signe= aval reste 1
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y20,V20,Y10+xctdf(J1)-xctdf(J2),V10*ALP2(I),SC2)
      CALL RICONFL(J3,Y30,V30,Y10+xctdf(J1)-xctdf(J3),V10*ALP3(I),SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,Y20,V20,xctdf(J1)-xctdf(J2),ZEROD,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,Y30,V30,xctdf(J1)-xctdf(J3),ZEROD,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
C fin du if sur y10
               endif
           IJ2=LDETSJ(1,SC2,J2)
           Y20=DETYN(1,SC2,IJ2)
           IJ3=LDETSJ(1,SC3,J3)
           Y30=DETYN(1,SC3,IJ3)
              if(Y20.LT.EPSY)THEN
                if(Y30.LT.EPSY)THEN
                  Y10=0.
                ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
                endif
              elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
              else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
              endif
              IF(y10.lt.epsy)then
                FLS(J1)=0.
                IF(fls(j2)*fls(j3).GT.0.)then
                  fls(j2)=0.
                  fls(j3)=0.
                  flq(j2)=detp(Y20,IJ2)
                  flq(j3)=detp(y30,ij3)
                elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                  coef=abs(fls(j3))/abs(fls(J2))
                  flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                  fls(j2)=coef*fls(j2)
                elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                   coef=abs(fls(j2))/abs(fls(J3))
                   flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                   fls(j3)=coef*fls(j3)
                endif
                sc1=0
                flq(j1)=0.
C si y10 non nul
          else
            IJ1=LDETYJ(1,Y10,J1)
            SC1=DETSN(1,Y10,IJ1)
            FLS(J1)=FLS(J2)+FLS(J3)
            V0=abs(fls(j1)/sc1)
            if(v0.gt.vmax)Then
              coef=vmax/v0
              IF(fls(j2)*fls(j3).GT.0.)then
                fls(j2)=coef*fls(j2)
                fls(j3)=coef*fls(j3)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
              elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                coef2=1.+(coef-1.)*fls(j1)/fls(j2)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                fls(j2)=coef2*fls(j2)
              elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                coef2=1.+(coef-1.)*fls(j1)/fls(j3)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                fls(j3)=coef2*fls(j3)
              endif
             fls(j1)=coef*fls(j1)
C fin du if sur v0
           endif
             FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,ALP2(I)*FLQ(J2)+ALP3(I)*FLQ(J3)
     :-ALP2(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
C150413                   if(2.*sc2.gt.sn(j2+1))then
                   if(sc2.gt.eps)then
C150413                         SNP1(J2+1)=2.*SC2-sn(j2+1)
C150413                     QNP1(J2+1)=2.*FLS(J2)-qn(j2+1)
                         SNP1(J2+1)=SC2
                     QNP1(J2+1)=FLS(J2)
C150413                         if(qnp1(j2+1)*fls(j2).lt.0.)then
C150413                                 qnp1(j2+1)=0.
C150413                             fls(j2)=0.5*qn(j2+1)
C150413                     endif
                   else
                          snp1(j2+1)=0.
                      qnp1(j2+1)=0.
C150413                          fls(j2)=0.5*qn(j2+1)
              fls(j2)=0.
                          flq(j2)=0.
                        endif
                   if(sc3.gt.eps)then
                         SNP1(J3+1)=SC3
                     QNP1(J3+1)=FLS(J3)
C150413                   if(2.*sc3.gt.sn(j3+1))then
C150413                         SNP1(J3+1)=2.*SC3-sn(j3+1)
C150413                     QNP1(J3+1)=2.*FLS(J3)-qn(j3+1)
C150413                         if(qnp1(j3+1)*fls(j3).lt.0.)then
C150413                                 qnp1(j3+1)=0.
C150413                             fls(j3)=0.5*qn(j3+1)
C150413                     endif
                   else
                          snp1(j3+1)=0.
                      qnp1(j3+1)=0.
C150413                          fls(j3)=0.5*qn(j3+1)
                          fls(j3)=0.
                          flq(j3)=0.
                        endif
                   if(sc1.gt.eps)then
                         SNP1(J1)=SC1
                     QNP1(J1)=FLS(J1)
C150413                   if(2.*sc1.gt.sn(j1))then
C150413                         SNP1(J1)=2.*SC1-sn(j1)
C150413                     QNP1(J1)=2.*FLS(J1)-qn(j1)
C150413                         if(qnp1(j1)*fls(j1).lt.0.)then
C150413                                 qnp1(j1)=0.
C150413                             fls(j1)=0.5*qn(j1)
C150413                     endif
                   else
                          snp1(j1)=0.
                      qnp1(j1)=0.
C150413                          fls(j1)=0.5*qn(j1)
                          fls(j1)=0.
                          flq(j1)=0.
                        endif
         ELSE
C v30 De signe different = defluence amont =2
           if(Y20.GT.EPSY)THEN
      CALL RICONFL(J1,Y20+xctdf(J2)-xctdf(J1),V20,Y10,V10,SC1)
             V30=-V30
      CALL RICONFL(J3,Y20+xctdf(J2)-xctdf(J3),V20,Y30,V30,SC3)
             FLS(J3)=-FLS(J3)
           else
                if(Y10.GT.xctdf(J2)-xctdf(J1))then
      CALL RICONFL(J1,xctdf(J2)-xctdf(J1),ZEROD,Y10,V10,SC1)
             else
      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
                endif
                if(Y30.GT.xctdf(J2)-xctdf(J3))then
               V30=-V30
      CALL RICONFL(J3,xctdf(J2)-xctdf(J3),ZEROD,Y30,V30,SC3)
               FLS(J3)=-FLS(J3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
C fin du if sur Y20
               endif
           IJ1=LDETSJ(1,SC1,J1)
           IJ3=LDETSJ(1,SC3,J3)
           Y10=DETYN(1,SC1,IJ1)
           Y30=DETYN(1,SC3,IJ3)
           if(Y10.LT.EPSY)THEN
                 IF(Y30.LT.EPSY)then
                      Y20=0.
                    else
                Y20=Y30+xctdf(J3)-xctdf(J2)
                 endif
              elseif(Y30.LT.EPSY)THEN
              Y20=Y10+xctdf(J1)-xctdf(J2)
              else
      Y20=(sc1*(Y10+xctdf(J1))+sc3*(Y30+xctdf(J3)))/(sc1+sc3)-xctdf(J2)
              endif
             IF(y20.lt.epsy)then
               FLS(J2)=0.
               IF(fls(j1)*fls(j3).LT.0.)then
                 fls(j1)=0.
                 fls(j3)=0.
                 flq(j1)=detp(Y10,IJ1)
                 flq(j3)=detp(y30,ij3)
               elseif(abs(fls(j1)).gt.abs(fls(j3)))then
                 coef=abs(fls(j3))/abs(fls(J1))
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 fls(j1)=coef*fls(j1)
               elseif(abs(fls(j3)).gt.abs(fls(j1)))then
                 coef=abs(fls(j1))/abs(fls(J3))
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                 fls(j3)=coef*fls(j3)
               endif
               sc2=0
               flq(j2)=0.
C si y20 non nul
                 else
             FLS(J2)=FLS(J1)-FLS(J3)
             IJ2=LDETYJ(1,Y20,J2)
             SC2=DETSN(1,Y20,IJ2)
             V0=abs(fls(j2)/sc2)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j1)*fls(j3).lT.0.)then
                 fls(j1)=coef*fls(j1)
                 fls(j3)=coef*fls(j3)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               elseif(abs(fls(j1)).gt.abs(fls(j3)))then
                 coef2=1.+(coef-1.)*fls(j2)/fls(j1)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
                 fls(j1)=coef2*fls(j1)
               elseif(abs(fls(j3)).gt.abs(fls(j1)))then
                 coef2=1.+(coef-1.)*fls(j2)/fls(j3)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                 fls(j3)=coef2*fls(j3)
               endif
             fls(j2)=coef*fls(j2)
C fin du if sur v0
                        endif
             FLQ(J2)=DETP(Y20,IJ2)+
     :MAX(ZEROD,FLQ(J1)+FLQ(J3)
     :-DETP(Y30,IJ3)-DETP(Y10,IJ1))
c fin du if sur y20=0.
           ENDIF
      FLQ(J3)=FLQ(J3)-(1.-ALP1(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
      FLQ(J1)=FLQ(J1)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J1)-DETP(Y10,IJ1))
           SNP1(J2+1)=SC2
           QNP1(J2+1)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J3+1)=SC3
           QNP1(J3+1)=FLS(J3)
c                 FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
           SNP1(J1)=SC1
           QNP1(J1)=FLS(J1)
c           FLS(J1)=0.5*(FLS(J1)+QN(J1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
         ENDIF
       ELSEIF(V10*V30.GT.0.) THEN
C v20 de signe different = defluence amont =3
         if(Y30.GT.EPSY)THEN
      CALL RICONFL(J1,Y30+xctdf(J3)-xctdf(J1),V30,Y10,V10,SC1)
           V20=-V20
      CALL RICONFL(J2,Y30+xctdf(J3)-xctdf(J2),V30,Y20,V20,SC2)
           FLS(J2)=-FLS(J2)
         else
              if(Y10.GT.xctdf(J3)-xctdf(J1))then
      CALL RICONFL(J1,xctdf(J3)-xctdf(J1),ZEROD,Y10,V10,SC1)
           else
      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
              endif
              if(Y20.GT.xctdf(J3)-xctdf(J2))then
             V20=-V20
      CALL RICONFL(J2,xctdf(J3)-xctdf(J2),ZEROD,Y20,V20,SC2)
             FLS(J2)=-FLS(J2)
           else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
              endif
C fn du if sur y30
         endif
c           FLQ(J2)=-FLQ(J2)
           IJ2=LDETSJ(1,SC2,J2)
           IJ1=LDETSJ(1,SC1,J1)
           Y20=DETYN(1,SC2,IJ2)
           Y10=DETYN(1,SC1,IJ1)
           if(Y10.LT.EPSY)THEN
                IF(Y20.LT.EPSY)THEN
                  Y30=0.
                else
               Y30=Y20+xctdf(J2)-xctdf(J3)
                endif
              elseif(y20.LT.EPSY)THEN
             Y30=Y10+xctdf(J1)-xctdf(J3)
              else
      Y30=(sc1*(Y10+xctdf(J1))+sc2*(Y20+xctdf(J2)))/(sc1+sc2)-xctdf(J3)
              endif
              IF(y30.lt.epsy)then
                FLS(J3)=0.
                IF(fls(j1)*fls(j2).LT.0.)then
                  fls(j1)=0.
                  fls(j2)=0.
                  flq(j1)=detp(Y10,IJ1)
                  flq(j2)=detp(y20,ij2)
                elseif(abs(fls(j1)).gt.abs(fls(j2)))then
                  coef=abs(fls(j2))/abs(fls(J1))
                  flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                  fls(j1)=coef*fls(j1)
                elseif(abs(fls(j2)).gt.abs(fls(j1)))then
                  coef=abs(fls(j1))/abs(fls(J2))
                  flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                  fls(j2)=coef*fls(j2)
                endif
                sc3=0.
                flq(j3)=0.
C si y20 non nul
                 else
             FLS(J3)=-FLS(J2)+FLS(J1)
             IJ3=LDETYJ(1,Y30,J3)
             sc3=DETSN(1,Y30,IJ3)
             V0=abs(fls(j3)/sc3)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j1)*fls(j2).lT.0.)then
                 fls(j1)=coef*fls(j1)
                 fls(j2)=coef*fls(j2)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
               elseif(abs(fls(j1)).gt.abs(fls(j2)))then
                 coef2=1.+(coef-1.)*fls(j3)/fls(j1)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
                 fls(j1)=coef2*fls(j1)
               elseif(abs(fls(j2)).gt.abs(fls(j1)))then
                 coef2=1.+(coef-1.)*fls(j3)/fls(j2)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                 fls(j2)=coef2*fls(j2)
               endif
             fls(j3)=coef*fls(j3)
C fin du if sur v0
             endif
             FLQ(J3)=DETP(Y30,IJ3)+
     :MAX(ZEROD,FLQ(J1)+FLQ(J2)
     :-DETP(Y20,IJ2)-DETP(Y10,IJ1))
c fin du if sur y30=0.
           ENDIF
           FLQ(J2)=FLQ(J2)-(1.-ALP1(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
           FLQ(J1)=FLQ(J1)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J1)-DETP(Y10,IJ1))
           SNP1(J2+1)=SC2
           QNP1(J2+1)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J1)=SC1
           QNP1(J1)=FLS(J1)
c                 FLS(J1)=0.5*(FLS(J1)+QN(J1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
           SNP1(J3+1)=SC3
           QNP1(J3+1)=FLS(J3)
c           FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
       ELSE
C v10 de signe different = situation transitoire  = aval reste 1
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y20,V20,Y10+xctdf(J1)-xctdf(J2),V10,SC2)
      CALL RICONFL(J3,Y30,V30,Y10+xctdf(J1)-xctdf(J3),V10,SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,Y20,V20,xctdf(J1)-xctdf(J2),ZEROD,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,Y30,V30,xctdf(J1)-xctdf(J3),ZEROD,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
C fin du if sur y10
               endif
           IJ2=LDETSJ(1,SC2,J2)
           IJ3=LDETSJ(1,SC3,J3)
           Y20=DETYN(1,SC2,IJ2)
           Y30=DETYN(1,SC3,IJ3)
              if(Y20.LT.EPSY)THEN
                if(Y30.LT.EPSY)THEN
                  Y10=0.
                ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
                endif
              elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
              else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
              endif
                 IF(y10.lt.epsy)then
                   FLS(J1)=0.
                   IF(fls(j2)*fls(j3).GT.0.)then
                     fls(j2)=0.
                     fls(j3)=0.
                     flq(j2)=detp(Y20,IJ2)
                     flq(j3)=detp(y30,ij3)
                   elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                     coef=abs(fls(j3))/abs(fls(J2))
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                     fls(j2)=coef*fls(j2)
                   elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                     coef=abs(fls(j2))/abs(fls(J3))
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                     fls(j3)=coef*fls(j3)
                   endif
                     sc1=0
                        flq(j1)=0.
C si y10 non nul
                 else
             IJ1=LDETYJ(1,Y10,J1)
                SC1=DETSN(1,Y10,IJ1)
             FLS(J1)=FLS(J2)+FLS(J3)
             V0=abs(fls(j1)/sc1)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j2)*fls(j3).GT.0.)then
                 fls(j2)=coef*fls(j2)
                 fls(j3)=coef*fls(j3)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j2)
               flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                 fls(j2)=coef2*fls(j2)
               elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j3)
               flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                 fls(j3)=coef2*fls(j3)
               endif
             fls(j1)=coef*fls(j1)
C fin du if sur v0
             endif
             FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,ALP2(I)*FLQ(J2)+ALP3(I)*FLQ(J3)
     :-ALP2(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
           SNP1(J2+1)=SC2
           QNP1(J2+1)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2+1))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J3+1)=SC3
           QNP1(J3+1)=FLS(J3)
c                 FLS(J3)=0.5*(FLS(J3)+QN(J3+1))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
           SNP1(J1)=SC1
           QNP1(J1)=FLS(J1)
c           FLS(J1)=0.5*(FLS(J1)+QN(J1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
c FIN DU IF SUR LES CAS DE CONFLUENCE
       ENDIF
 1    CONTINUE

C traitement des d�fluences
c       write(*,*)'conflue fin confluences'

      DO 2 I=NCONF2+1,NCONF
C IB numero du bief amont deflunece
       IB=CONFLU(I,1)
       J1=LM(IB)-1
       J2=LM(CONFLU(I,2)-1)+1
       J3=LM(CONFLU(I,3)-1)+1
       Y10 = YMD(J1)
       V10 = VMD(J1)
       Y20 = YPD(J2)
       V20 = VPD(J2)
       Y30 = YPD(J3)
       V30 = VPD(J3)
C modification du 25/8/16 : vitesse anormale portee de 2 a 20=2*10 m/s
          VMAX=2.*max(abs(v10),abs(v20),abs(v30),10.D0)

       IF(V10*V20.GE.0.)THEN
         IF(V10*V30.GE.0.) THEN
C vitesses de meme signe= amont reste 1
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y10+xctdf(J1)-xctdf(J2),V10*ALP2(I),Y20,V20,SC2)
      CALL RICONFL(J3,Y10+xctdf(J1)-xctdf(J3),V10*ALP3(I),Y30,V30,SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,xctdf(J1)-xctdf(J2),ZEROD,Y20,V20,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,xctdf(J1)-xctdf(J3),ZEROD,Y30,V30,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
              endif
           IJ2=LDETSJ(1,SC2,J2)
           IJ3=LDETSJ(1,SC3,J3)
           Y20=DETYN(1,SC2,IJ2)
           Y30=DETYN(1,SC3,IJ3)
           if(Y20.LT.EPSY)THEN
             if(Y30.LT.EPSY)THEN
               Y10=0.
             ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
             endif
           elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
           else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
           endif
           IF(y10.lt.epsy)then
             FLS(J1)=0.
             IF(fls(j2)*fls(j3).GT.0.)then
               fls(j2)=0.
               fls(j3)=0.
               flq(j2)=detp(Y20,IJ2)
               flq(j3)=detp(y30,ij3)
             elseif(abs(fls(j2)).gt.abs(fls(j3)))then
               coef=abs(fls(j3))/abs(fls(J2))
               flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
               fls(j2)=coef*fls(j2)
             elseif(abs(fls(j3)).gt.abs(fls(j2)))then
               coef=abs(fls(j2))/abs(fls(J3))
               flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               fls(j3)=coef*fls(j3)
             endif
             sc1=0
             flq(j1)=0.
C si y10 non nul
           else
             IJ1=LDETYJ(1,Y10,J1)
             SC1=DETSN(1,Y10,IJ1)
             FLS(J1)=FLS(J2)+FLS(J3)
             V0=abs(fls(j1)/sc1)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j2)*fls(j3).GT.0.)then
                 fls(j2)=coef*fls(j2)
                 fls(j3)=coef*fls(j3)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j2)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                 fls(j2)=coef2*fls(j2)
               elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                 coef2=1.+(coef-1.)*fls(j1)/fls(j3)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                 fls(j3)=coef2*fls(j3)
               endif
               fls(j1)=coef*fls(j1)
C fin du if sur v0
             endif
           FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,FLQ(J2)+FLQ(J3)
     :-DETP(Y20,IJ2)-DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
           FLQ(J2)=FLQ(J2)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
           FLQ(J3)=FLQ(J3)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
                   if(sc2.gt.eps)then
                         SNP1(J2)=SC2
                     QNP1(J2)=FLS(J2)
C150413                   if(2.*sc2.gt.sn(j2))then
C150413                         SNP1(J2)=2.*SC2-sn(j2)
C150413                     QNP1(J2)=2.*FLS(J2)-qn(j2)
C150413                         if(qnp1(j2)*fls(j2).lt.0.)then
C150413                                 qnp1(j2)=0.
C150413                             fls(j2)=0.5*qn(j2)
C150413                     endif
                   else
                          snp1(j2)=0.
                      qnp1(j2)=0.
C150413                          fls(j2)=0.5*qn(j2)
                          fls(j2)=0.
                          flq(j2)=0.
                        endif
                   if(sc3.gt.eps)then
                         SNP1(J3)=SC3
                     QNP1(J3)=FLS(J3)
C150413                   if(2.*sc3.gt.sn(j3))then
C150413                         SNP1(J3)=2.*SC3-sn(j3)
C150413                     QNP1(J3)=2.*FLS(J3)-qn(j3)
C150413                         if(qnp1(j3)*fls(j3).lt.0.)then
C150413                                 qnp1(j3)=0.
C150413                             fls(j3)=0.5*qn(j3)
C150413                     endif
                   else
                          snp1(j3)=0.
                      qnp1(j3)=0.
                          fls(j3)=0.
C150413                          fls(j3)=0.5*qn(j3)
                          flq(j3)=0.
                        endif
                   if(sc1.gt.eps)then
                         SNP1(J1+1)=SC1
                     QNP1(J1+1)=FLS(J1)
C150413                   if(2.*sc1.gt.sn(j1+1))then
C150413                         SNP1(J1+1)=2.*SC1-sn(j1+1)
C150413                     QNP1(J1+1)=2.*FLS(J1)-qn(j1+1)
C150413                         if(qnp1(j1+1)*fls(j1).lt.0.)then
C150413                                 qnp1(j1+1)=0.
C150413                             fls(j1)=0.5*qn(j1+1)
C150413                     endif
                   else
                          snp1(j1+1)=0.
                      qnp1(j1+1)=0.
                          fls(j1)=0.
C150413                          fls(j1)=0.5*qn(j1+1)
                          flq(j1)=0.
                        endif
         ELSE
C v30 De signe different = confluence avec aval 2
           if(Y20.GT.EPSY)THEN
      CALL RICONFL(J1,Y10,V10,Y20+xctdf(J2)-xctdf(J1),V20,SC1)
             V30=-V30
      CALL RICONFL(J3,Y30,V30,Y20+xctdf(J2)-xctdf(J3),V20,SC3)
             FLS(J3)=-FLS(J3)
           else
                IF(Y10.GT.xctdf(J2)-xctdf(J1))THEN
      CALL RICONFL(J1,Y10,V10,xctdf(J2)-xctdf(J1),ZEROD,SC1)
             ELSE
      CALL RICONFL(J1,Y10,ZEROD,Y10,ZEROD,SC1)
                endif
             V30=-V30
                IF(Y30.GT.xctdf(J2)-xctdf(J3))THEN
      CALL RICONFL(J3,Y30,V30,xctdf(J2)-xctdf(J3),ZEROD,SC3)
             ELSE
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
             FLS(J3)=-FLS(J3)
C fin du if sur y20
              endif
           IJ1=LDETSJ(1,SC1,J1)
           IJ3=LDETSJ(1,SC3,J3)
           Y10=DETYN(1,SC1,IJ1)
           Y30=DETYN(1,SC3,IJ3)
           if(Y10.LT.EPSY)THEN
                IF(Y30.LT.EPSY)then
                     Y20=0.
                   else
               Y20=Y30+xctdf(J3)-xctdf(J2)
                endif
              elseif(Y30.LT.EPSY)THEN
             Y20=Y10+xctdf(J1)-xctdf(J2)
              else
      Y20=(sc1*(Y10+xctdf(J1))+sc3*(Y30+xctdf(J3)))/(sc1+sc3)-xctdf(J2)
              endif
                 IF(y20.lt.epsy)then
                   FLS(J2)=0.
                   IF(fls(j1)*fls(j3).LT.0.)then
                     fls(j1)=0.
                     fls(j3)=0.
                     flq(j1)=detp(Y10,IJ1)
                     flq(j3)=detp(y30,ij3)
                   elseif(abs(fls(j1)).gt.abs(fls(j3)))then
                     coef=abs(fls(j3))/abs(fls(J1))
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                     fls(j1)=coef*fls(j1)
                   elseif(abs(fls(j3)).gt.abs(fls(j1)))then
                     coef=abs(fls(j1))/abs(fls(J3))
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                     fls(j3)=coef*fls(j3)
                   endif
                     sc2=0
                        flq(j2)=0.
C si y20 non nul
                 else
             FLS(J2)=FLS(J1)-FLS(J3)
             IJ2=LDETYJ(1,Y20,J2)
             SC2=DETSN(1,Y20,IJ2)
             V0=abs(fls(j2)/sc2)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j1)*fls(j3).lT.0.)then
                 fls(j1)=coef*fls(j1)
                 fls(j3)=coef*fls(j3)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
               elseif(abs(fls(j1)).gt.abs(fls(j3)))then
                 coef2=1.+(coef-1.)*fls(j2)/fls(j1)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
                 fls(j1)=coef2*fls(j1)
               elseif(abs(fls(j3)).gt.abs(fls(j1)))then
                  coef2=1.+(coef-1.)*fls(j2)/fls(j3)
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                 fls(j3)=coef2*fls(j3)
               endif
             fls(j2)=coef*fls(j2)
C fin du if sur v0
                        endif
             FLQ(J2)=DETP(Y20,IJ2)+
     :MAX(ZEROD,ALP2(I)*FLQ(J1)+ALP1(I)*FLQ(J3)
     :-ALP2(I)*DETP(Y10,IJ1)-ALP1(I)*DETP(Y30,IJ3))
c fin du if sur y20=0.
           ENDIF
           SNP1(J2)=SC2
           QNP1(J2)=FLS(J2)
c           FLS(J2)=0.5*(FLS(J2)+QN(J2))
c           IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J3)=SC3
           QNP1(J3)=FLS(J3)
c                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
           SNP1(J1+1)=SC1
           QNP1(J1+1)=FLS(J1)
c           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
         ENDIF
       ELSEIF(V10*V30.GT.0.) THEN
C v20 de signe different = confluence aval =3
         if(Y30.GT.EPSY)THEN
      CALL RICONFL(J1,Y10,V10,Y30+xctdf(J3)-xctdf(J1),V30,SC1)
           V20=-V20
      CALL RICONFL(J2,Y20,V20,Y30+xctdf(J3)-xctdf(J2),V30,SC2)
           FLS(J2)=-FLS(J2)
         else
              if(Y10.GT.xctdf(J3)-xctdf(J1))THEN
        CALL RICONFL(J1,Y10,V10,xctdf(J3)-xctdf(J1),ZEROD,SC1)
              else
           call riconfl(J1,Y10,ZEROD,Y10,ZEROD,SC1)
              endif
           V20=-V20
              if(y20.GT.xctdf(J3)-xctdf(J2))THEN
      CALL RICONFL(J2,Y20,V20,xctdf(J3)-xctdf(J2),ZEROD,SC2)
           else
          call riconfl(J2,Y20,ZEROD,Y20,ZEROD,SC2)
              endif
           FLS(J2)=-FLS(J2)
C fin du if sur Y30
            endif
           IJ2=LDETSJ(1,SC2,J2)
           IJ1=LDETSJ(1,SC1,J1)
           Y20=DETYN(1,SC2,IJ2)
           Y10=DETYN(1,SC1,IJ1)
           if(Y10.LT.EPSY)THEN
                IF(Y20.LT.EPSY)THEN
                  Y30=0.
                else
               Y30=Y20+xctdf(J2)-xctdf(J3)
                endif
              elseif(y20.LT.EPSY)THEN
             Y30=Y10+xctdf(J1)-xctdf(J3)
              else
      Y30=(sc2*(Y20+xctdf(J2))+sc1*(Y10+xctdf(J1)))/(sc2+sc1)-xctdf(J3)
              endif
             IF(y30.lt.epsy)then
               FLS(J3)=0.
               IF(fls(j1)*fls(j2).LT.0.)then
                 fls(j1)=0.
                 fls(j2)=0.
                 flq(j1)=detp(Y10,IJ1)
                 flq(j2)=detp(y20,ij2)
               elseif(abs(fls(j1)).gt.abs(fls(j2)))then
                 coef=abs(fls(j2))/abs(fls(J1))
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 fls(j1)=coef*fls(j1)
               elseif(abs(fls(j2)).gt.abs(fls(j1)))then
                 coef=abs(fls(j1))/abs(fls(J2))
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                 fls(j2)=coef*fls(j2)
               endif
                     sc3=0.
                        flq(j3)=0.
C si y20 non nul
                 else
             FLS(J3)=-FLS(J2)+FLS(J1)
             IJ3=LDETYJ(1,Y30,J3)
             sc3=DETSN(1,Y30,IJ3)
             V0=abs(fls(j3)/sc3)
             if(v0.gt.vmax)Then
               coef=vmax/v0
               IF(fls(j1)*fls(j2).lT.0.)then
                 fls(j1)=coef*fls(j1)
                 fls(j2)=coef*fls(j2)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef**2+detp(y10,ij1)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
               elseif(abs(fls(j1)).gt.abs(fls(j2)))then
                 coef2=1.+(coef-1.)*fls(j3)/fls(j1)
                 flq(j1)=(flq(j1)-detp(y10,ij1))*coef2**2+detp(y10,ij1)
                 fls(j1)=coef2*fls(j1)
               elseif(abs(fls(j2)).gt.abs(fls(j1)))then
                 coef2=1.+(coef-1.)*fls(j3)/fls(j2)
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                 fls(j2)=coef2*fls(j2)
               endif
             fls(j3)=coef*fls(j3)
C fin du if sur v0
                        endif
           FLQ(J3)=DETP(Y30,IJ3)+
     :MAX(ZEROD,ALP1(I)*FLQ(J2)+ALP3(I)*FLQ(J1)
     :-ALP1(I)*DETP(Y20,IJ2)-ALP3(I)*DETP(Y10,IJ1))
c fin du if sur y30=0.
            ENDIF
           SNP1(J2)=SC2
           QNP1(J2)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J3)=SC3
           QNP1(J3)=FLS(J3)
c                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
           SNP1(J1+1)=SC1
           QNP1(J1+1)=FLS(J1)
c           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
       ELSE
C v10 de signe different = situation transitoire  = amont reste 1
           if(Y10.GT.EPSY)THEN
      CALL RICONFL(J2,Y10+xctdf(J1)-xctdf(J2),V10,Y20,V20,SC2)
      CALL RICONFL(J3,Y10+xctdf(J1)-xctdf(J3),V10,Y30,V30,SC3)
           else
                if(Y20.GT.xctdf(J1)-xctdf(J2))then
      CALL RICONFL(J2,xctdf(J1)-xctdf(J2),ZEROD,Y20,V20,SC2)
             else
      CALL RICONFL(J2,Y20,ZEROD,Y20,ZEROD,SC2)
                endif
                if(Y30.GT.xctdf(J1)-xctdf(J3))then
      CALL RICONFL(J3,xctdf(J1)-xctdf(J3),ZEROD,Y30,V30,SC3)
             else
      CALL RICONFL(J3,Y30,ZEROD,Y30,ZEROD,SC3)
                endif
C fin du if sur y10
              endif
           IJ2=LDETSJ(1,SC2,J2)
           IJ3=LDETSJ(1,SC3,J3)
           Y20=DETYN(1,SC2,IJ2)
           Y30=DETYN(1,SC3,IJ3)
              if(Y20.LT.EPSY)THEN
                if(Y30.LT.EPSY)THEN
                  Y10=0.
                ELSE
               Y10=Y30+xctdf(J3)-xctdf(J1)
                endif
              elseif(y30.LT.EPSY)then
             Y10=Y20+xctdf(J2)-xctdf(J1)
              else
      Y10=(sc2*(Y20+xctdf(J2))+sc3*(Y30+xctdf(J3)))/(sc2+sc3)-xctdf(J1)
              endif
             IF(y10.lt.epsy)then
               FLS(J1)=0.
               IF(fls(j2)*fls(j3).GT.0.)then
                 fls(j2)=0.
                 fls(j3)=0.
                 flq(j2)=detp(Y20,IJ2)
                 flq(j3)=detp(y30,ij3)
               elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                 coef=abs(fls(j3))/abs(fls(J2))
                 flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                 fls(j2)=coef*fls(j2)
               elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                 coef=abs(fls(j2))/abs(fls(J3))
                 flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                 fls(j3)=coef*fls(j3)
               endif
               sc1=0
               flq(j1)=0.
C si y10 non nul
                 else
             IJ1=LDETYJ(1,Y10,J1)
                SC1=DETSN(1,Y10,IJ1)
             FLS(J1)=FLS(J2)+FLS(J3)
             V0=abs(fls(j1)/sc1)
             if(v0.gt.vmax)Then
                coef=vmax/v0
                IF(fls(j2)*fls(j3).GT.0.)then
                  fls(j2)=coef*fls(j2)
                  fls(j3)=coef*fls(j3)
                  flq(j2)=(flq(j2)-detp(y20,ij2))*coef**2+detp(y20,ij2)
                  flq(j3)=(flq(j3)-detp(y30,ij3))*coef**2+detp(y30,ij3)
                elseif(abs(fls(j2)).gt.abs(fls(j3)))then
                   coef2=1.+(coef-1.)*fls(j1)/fls(j2)
                flq(j2)=(flq(j2)-detp(y20,ij2))*coef2**2+detp(y20,ij2)
                   fls(j2)=coef2*fls(j2)
                 elseif(abs(fls(j3)).gt.abs(fls(j2)))then
                   coef2=1.+(coef-1.)*fls(j1)/fls(j3)
                flq(j3)=(flq(j3)-detp(y30,ij3))*coef2**2+detp(y30,ij3)
                   fls(j3)=coef2*fls(j3)
                 endif
             fls(j1)=coef*fls(j1)
C fin du if sur v0
                        endif
           FLQ(J1)=DETP(Y10,IJ1)+
     :MAX(ZEROD,FLQ(J2)+FLQ(J3)
     :-DETP(Y20,IJ2)-DETP(Y30,IJ3))
c fin du if sur y10=0.
           ENDIF
           FLQ(J2)=FLQ(J2)-(1.-ALP2(I))*MAX(ZEROD,FLQ(J2)-DETP(Y20,IJ2))
           FLQ(J3)=FLQ(J3)-(1.-ALP3(I))*MAX(ZEROD,FLQ(J3)-DETP(Y30,IJ3))
           SNP1(J2)=SC2
           QNP1(J2)=FLS(J2)
c                 FLS(J2)=0.5*(FLS(J2)+QN(J2))
c                 IF(ABS(FLQ2).GT.EPS)FLQ(J2)=0.5*(FLQ2+FLQ(J2))
           SNP1(J3)=SC3
           QNP1(J3)=FLS(J3)
c                 FLS(J3)=0.5*(FLS(J3)+QN(J3))
c                 IF(ABS(FLQ3).GT.EPS)FLQ(J3)=0.5*(FLQ3+FLQ(J3))
           SNP1(J1+1)=SC1
           QNP1(J1+1)=FLS(J1)
c           FLS(J1)=0.5*(FLS(J1)+QN(J1+1))
c                 IF(ABS(FLQ1).GT.EPS)FLQ(J1)=0.5*(FLQ1+FLQ(J1))
C fin du if sur le type de defluence
       ENDIF
 2    CONTINUE
c       write(*,*)'conflue fin'
      RETURN
      END

C***********************************************************************
      SUBROUTINE RICONFL(J,Y1,V1,Y2,V2,S1)
C***********************************************************************
C                                                                      C
C   SON ROLE : RESOLUTION DU PROBLEME DE RIEMAN PAR ROE ET CORRECTION LEVEQUE
C               POUR UNE CONFLUENCE
C++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++C

      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER LDETYJ



      INTEGER IJ1,IJ2,J,IJ
      INTEGER LDETSJ
      DOUBLE PRECISION S1,V1,S2,V2
     +    ,Y1,Y2
     +    ,VC,CC,CC1,CC2,Y,B,S
     +    ,FLS(LMAX),FLQ(LMAX)
     +    ,L1,L2,L1C,L2C
     +    ,MA11,MA12,MA21,MA22
     +,intermediaire

      DOUBLE PRECISION B1,B2
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION DETL,DETFL,DETP,DETYN,DETBET,DETSN

      EXTERNAL DETL,DETFL,DETP,DETYN,DETBET,DETSN
      EXTERNAL LDETSJ,LDETYJ



      COMMON/DDFLUX/FLS,FLQ
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

c        write(*,*)'entree riconfl'
      IF(Y1.GT.EPSY)THEN
         IJ1 = LDETYJ(1,Y1,J)
         S1 = DETSN(1,y1,IJ1)
         B1=DETBET(1,Y1,IJ1,J)
      ELSE
               S1=0.
         IJ1 = LDETSJ(1,S1,J)
            V1=0.
               B1=1.
      ENDIF
         IF(Y2.GT.EPSY)THEN
        IJ2 = LDETYJ(1,Y2,J)
        S2 = DETSN(1,y2,IJ2)
        B2=DETBET(1,Y2,IJ2,J)
      ELSE
        S2=0.
        IJ2 = LDETSJ(1,S2,J)
           V2=0.
              B2=0.
         ENDIF
C      write(*,*)'riconfl',j,s1,s2,v1,V2,Y1,Y2,b1,b2
      IF (S1.LE.EPS) THEN
        CC1=0.
        IF(S2.LE.EPS)THEN
          CC = 0.
        ELSE
          CC2 = SQRT(GRAV*S2/DETL(1,Y2,IJ2))
          VC = B2*V2
                INTERMEDIAIRE=DETP(Y2,IJ2)/S2-VC*(1.-B2)*V2
             if(intermediaire.GT.EPS)then
                  cc=sqrt(intermediaire)
             else
                  cc=0.
             endif

C           CC = SQRT(DETP(Y2,IJ2)/S2-VC*(1.-B2)*V2)
C fin du if sur S2=0
         ENDIF
C si S1 non nul
       ELSE
         CC1 = SQRT(GRAV*S1/DETL(1,Y1,IJ1))
         IF(S2.LE.EPS)THEN
           CC2=0.
           VC = B1*V1
                 INTERMEDIAIRE=DETP(Y1,IJ1)/S1-VC*(1.-B1)*V1
              if(intermediaire.GT.EPS)then
                   cc=sqrt(intermediaire)
              else
                   cc=0.
              endif
C           CC = SQRT(DETP(Y1,IJ1)/S1-VC*(1.-B1)*V1)
C s2 non nul
         ELSE
c            write(*,*)'avant cc2'
                 CC2 = SQRT(GRAV*S2/DETL(1,Y2,IJ2))
           VC = (V1*S1**.5*B1**.5+V2*S2**.5*B2**.5)/
     : ((S1**.5/B1**.5)+(S2**.5/B2**.5))
C modif du 06 aout 2010
C s au lieu de Y, epsy au lieu de eps dans formule
           IF (ABS(S1-S2).LT.EPSY) THEN
             S=0.5*(S1+S2)
             IJ=LDETSJ(1,S,J)
             Y=DETYN(1,S,IJ)
             B=DETBET(1,Y,IJ,J)
C modif du 06 aout 2010
                   intermediaire=CC1**2+VC**2*(1.-
     :(1.-S*(B1-B)/(B*EPSY))/B)
                if(intermediaire.GT.EPS)then
                     cc=sqrt(intermediaire)
                else
                     cc=0.
                endif
c               CC=SQRT(B1**2*V1**2+CC1**2-B1*V1**2*
c     :(1.-S1*(B1-B)/(B1*EPS)))
C else du if sur y1=Y2
           ELSE
C modif du 06 aout 2010
             intermediaire = (DETP(Y2,IJ2)-DETP(Y1,IJ1))/(S2-S1)
     :+VC**2*(1.-((S1/B1-S2/B2)/(S1-S2)))
                if(intermediaire.GT.EPS)then
                     cc=sqrt(intermediaire)
                else
                     cc=0.
                endif
C             IF((s1.gt.s2.and.detp(y2,ij2).gt.detp(y1,ij1)).or.
C     :(s2.gt.s1.and.detp(y1,ij1).gt.detp(y2,ij2)))then
C             write(*,*)'confl', cc, s1,s2,detp(y2,ij2),detp(y1,ij1)
C             endif
C fin du if sur y1=Y2
            ENDIF
C fin du if sur s2 nul
          ENDIF
C fin du if sur s1 nul
        ENDIF
c               write(*,*)'apres cc'
        IF (CC.lt.eps) THEN
          FLS(J) = (V2*S2+V1*S1)/2.
          FLQ(J) = (DETFL(S1,V1,IJ1,J)+DETFL(S2,V2,IJ2,J))/2.
          S1=(0.5*(sqrt(S1)+sqrt(S2)))**2
C si cc non nul
        ELSE
          L1 = VC-CC
          L2 = VC+CC
          IF (((B1*V1-CC1).LT.0.).AND.(0.LT.(B2*V2-CC2)))THEN
            L1C=(B2*V2-CC2)*(L1-B1*V1+CC1)/(B2*V2-CC2-B1*V1+CC1)
            IF (((B1*V1+CC1).LT.0.).AND.(0.LT.(B2*V2+CC2)))THEN
              L2C=(B2*V2+CC2)*(L2-B1*V1-CC1)/(B2*V2+CC2-B1*V1-CC1)
            ELSE
              L2C=.5*(ABS(L2)+L2)
            ENDIF
            MA11=L1C*L2-L2C*L1
            MA12=L2C-L1C
            MA22=L2*L2C-L1*L1C
          ELSE
            IF (((B1*V1+CC1).LT.0.).AND.(0.LT.(B2*V2+CC2)))THEN
              L1C = .5*(ABS(L1)+L1)
              L2C=(B2*V2+CC2)*(L2-B1*V1-CC1)/(B2*V2+CC2-B1*V1-CC1)
              MA11=L1C*L2-L2C*L1
              MA12=L2C-L1C
              MA22=L2*L2C-L1*L1C
            ELSE
              MA12=.5*(L2+ABS(L2)-L1-ABS(L1))
              MA22=.5*((L2+ABS(L2))*L2-(L1+ABS(L1))*L1)
              IF(L1*L2.GT.0.)THEN
                MA11=0.
              ELSE
                MA11=ABS(L1)*L2
C fin du if sur l1 * L2 positif
              ENDIF
C fin du if sur b1*v1+cc1
            ENDIF
C fin du if sur b1*v1-cc1
          ENDIF
c          write(*,*)'avant ma21'
          MA21 = -L1*L2*MA12
          FLS(J) = V2*S2-(MA11*(S2-S1)+MA12*(V2*S2-V1*S1))/(2.*CC)
          FLQ(J) = DETFL(S2,V2,IJ2,J)-(MA21*(S2-S1)
     +                               +MA22*(V2*S2-V1*S1))/(2.*CC)
          S1=(0.5*(sqrt(S1)+sqrt(S2)))**2
C fin du if sur cc=0
        ENDIF
c        if(j.eq.18.or.j.eq.90.or.j.eq.92)then
c      write(*,*)'sortie riconfl',j,y1,y2,v1,v2,s1,fls(j),flq(j)
c        endif
      RETURN
      END



C**********************************************************************
              SUBROUTINE DCTMMS(QINP1,S2,V2,S,I)
C---------------------------------------------------------------------C
C                                                                     C
C       RESOLUTION PAR DICHOTOMIE DE L'EQUATION EN S , C'EST A DIRE   C
C       DETERMINATION DE S* INTERSECTION DES COURBES V+C ET V-C       C
C     CONSTANTE DE CHOC ET DE DETENTE DANS LE CAS D'UN DEBIT QINP1    C
C                   IMPOSE EN CONDITION LIMITE AMONT                  C
C                                                                     C
C       CALCUL D'UNE SECTION A PARTIR D'UN DEBIT IMPOSE               C
C       ENTREES : QINP1                       SORTIE : S              C
C                 S2,V2                                               C
C=====================================================================C
C       EPS EST LA PRECISION DEMANDEE                                 C
C       NITER LE NOMBRE D'ITERATIONS FAITES                           C
C=====================================================================C


      INTEGER NITER,N,LDETSJ,LMAX,N2,NMAX,NMIN,LNCMAX,I
C     : ,NBMAX
      PARAMETER(LMAX=3000,LNCMAX=130000)
C      PARAMETER(LMAX=3000,NBMAX=150,NCMAX=20)
C      INTEGER LM(0:NBMAX) ,LL,NBB

      INTEGER NC(0:LMAX),XNC(0:LMAX)
     :,icompteur,icompteur2

      DOUBLE PRECISION QINP1,S2,H2,V2,Y2,S,S3
      DOUBLE PRECISION EPS,A,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION DETYN,DETH
     +    ,XLISEC(LNCMAX)
     +    ,XYISEC(LNCMAX)
     +    ,XLYSEC(LNCMAX)
     +    ,XSECUM(LNCMAX)
     +    ,XPICUM(LNCMAX)
     +    ,XPECUM(LNCMAX)
     +    ,VM,HM,QM,YM,SM,SN,SP
     +    ,VNPLUS,HNPLUS,QNPLUS,YN1DPLUS,SNPLUS

      EXTERNAL LDETSJ
      EXTERNAL DETYN,DETH

      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
C      COMMON/PHYS/LM,LL
C      COMMON/NBIEF/NBB
      COMMON/NC/NC,XNC

         data icompteur,icompteur2/0,0/
         save icompteur,icompteur2

C        I=1
        IF(S2.LE.0.)THEN
            IF(ABS(QINP1).LT.EPS)THEN
              S=0.
              RETURN
            ENDIF
            S2=0.
              ELSEIF(ABS(QINP1).LT.EPS)THEN
              S=S2
              RETURN
        ENDIF
        N2=LDETSJ(1,S2,I)
        Y2=DETYN(1,S2,N2)
        H2=DETH(Y2,N2,I)
        NMIN=XNC(I-1)
C        NMIN=XNC(I-1)+1
        NMAX=XNC(I)
        N = NMIN



1       N = N+1
        SNPLUS=XSECUM(N)
        YN1DPLUS=XYISEC(N)
        HNPLUS=DETH(YN1DPLUS,N,I)
        VNPLUS=V2-H2+HNPLUS
        QNPLUS=VNPLUS*SNPLUS


        IF (QNPLUS.LT.QINP1)THEN
           NMIN=N
           IF (N.LT.XNC(I))GO TO 1
        ENDIF
        NMAX=N
C cas ou meme pour le premier couple non verifie
              IF(NMIN.EQ.XNC(I-1))THEN
                S=S2
             RETURN
              ELSE
          SN = XSECUM(NMIN)
           ENDIF
C......DANS LE CAS OU ON SE TROUVE AU DELA DE LA PLUS HAUTE COTE,
C.......ON PREND DEUX  FOIS LA SECTION MAXIMUM POUR LIMITE SUPERIEURE
        IF (NMIN.EQ.XNC(I))THEN
C          SP=2.*SN
C augmente a 10 dans le cas ou la cote amont maximale topo est trop faible
              SP=10.*SN
           if(icompteur.lt.100)then
        WRITE(*,*)'SP DCTMMS:NMIN=NCMAX,COTE MAXI DONNEE TROP FAIBLE'
              icompteur=icompteur+1
              endif
        ELSE
            SP = XSECUM(NMAX)
        ENDIF
        S3=MAX(SP,S2)
        NITER=1


10      SM = (SN+SP)/2.
        A = ABS(SP-SN)
C        IF (A.LT.EPS**2)GO TO 100
        IF(A.LT.EPS*S3)GO TO 100
        YM=DETYN(1,SM,NMIN)
        HM=DETH(YM,NMIN,I)
        VM = V2-H2+HM
        QM = VM*SM
        IF (QM.LT.QINP1)THEN
              SN = SM
        ELSE
              SP = SM
        ENDIF
        NITER = NITER+1
        IF(NITER.GE.50)THEN
           if(icompteur2.lt.100)then
             WRITE(*,*)'SP DCTMMS NON CONVERGENT EN 50 ITERATIONS'
             WRITE(*,*)'QINP1=',QINP1,'QM=',QM ,'SN=',SN,'SP=',SP
                      icompteur2=icompteur2+1
        endif
             GO TO 100

        ENDIF
        GOTO 10

100     S = SM
        IF (ABS(S2-S).LT.EPS*S3)S=S2
        RETURN
        END




C**********************************************************************
              SUBROUTINE DCHAMV(S,S2,V2,IJ,I)
C---------------------------------------------------------------------C
C                                                                     C
C       RESOLUTION PAR DICHOTOMIE DE L'EQUATION EN S :                C
C           (S-S2)*(P(S)-P(S2))-S*S2*V2**2=0                          C
C         pour choc en condition amont ou aval (Q=0)                  C
C       ENTREES : IJ                        SORTIE : S                C
C                 S2,V2                                               C
C=====================================================================C
C       EPSY EST LA PRECISION DEMANDEE SUR Y(S)                       C
C       NITER LE NOMBRE D'ITERATIONS FAITES                           C
C=====================================================================C


      INTEGER NITER,LDETSJ,I,IJ,IJ1
     :,icompteur
      DOUBLE PRECISION S2,V2,SV2,Y2,S
      DOUBLE PRECISION EPS,A,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION Y,DETYN,DETP
     +    ,SN,SP,P,P2,YN1D,YP

      EXTERNAL LDETSJ
      EXTERNAL DETYN,DETP

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

         data icompteur/0/
         save icompteur


      Y2=DETYN(1,S2,IJ)
      IF(Y2.LT.EPSY)THEN
        S=0.
        RETURN
      ENDIF
      P2=DETP(Y2,IJ)
      SV2=S2*V2**2
C ON COMMENCE PAR CHERCHER UN MULTIPLE DE S POUR LEQUEL LA FONCTION EST
C POSITIVE
      S=S2
      NITER=1
 1    S=2.*S
      NITER=NITER+1
      IJ1=LDETSJ(1,S,I)
      Y=DETYN(1,S,IJ1)
      P=DETP(Y,IJ1)
      A=(S-S2)*(P-P2)-S*SV2
      IF(A.GT.0.)THEN
         SP=S
         SN=.5*S
      ELSEIF(NITER.LT.50)THEN
         GO TO 1
      ELSE
         WRITE(*,*)'SP DCHAMV:ERREUR MAILLE   ',I
         write(*,*)'s=',s,s2,v2,ij,i
         STOP
      ENDIF
        NITER=1
      IJ1=LDETSJ(1,SN,I)
      YN1D=DETYN(1,SN,IJ1)
      IJ1=LDETSJ(1,SP,I)
      YP=DETYN(1,SP,IJ1)
10      S = 0.5*(SN+SP)
        A = ABS(YP-YN1D)
        IF (A.LT.EPSY)GO TO 100
        IF(A.LT.EPS*ABS(YN1D))GO TO 100
      IJ1=LDETSJ(1,S,I)
      Y=DETYN(1,S,IJ1)
      P=DETP(Y,IJ1)
      A=(S-S2)*(P-P2)-S*SV2
      IF(A.GT.0.)THEN
              SP = S
              YP = Y
        ELSE
              SN = S
              YN1D = Y
        ENDIF
        NITER = NITER+1
        IF(NITER.GT.50)THEN
           if(icompteur.lt.100)then
             WRITE(*,*)'SP DCHAMV NON CONVERGENT EN 50 ITERATIONS'
             WRITE(*,*)'SN=',SN,'SP=',SP,' MAILLE   ',I
                      icompteur=icompteur+1
              endif
        ELSE
          GOTO 10
        ENDIF

100     RETURN
        END
C**********************************************************************
              SUBROUTINE QAMONT(S2,V2,Q,S,I)
C---------------------------------------------------------------------C
C       CHOC EN CONDITION AMONT  (GEOM.REELLE)                        C
C       RESOLUTION PAR DICHOTOMIE DE L'EQUATION EN S :                C
C           (S-S2)*(P(S)-P(S2))-S2/S*(Q-S*V2)**2=0                    C
C       ENTREES : Q                        SORTIE : S                 C
C                 S2,V2                                               C
C=====================================================================C
C       EPSY EST LA PRECISION DEMANDEE SUR Y(S)
C       NITER LE NOMBRE D'ITERATIONS FAITES                           C
C=====================================================================C


      INTEGER NITER,LDETSJ,IJ,IJ2,I
      DOUBLE PRECISION S2,V2,Q,Y2,S,Y1,Y3
      DOUBLE PRECISION EPS,A,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION Y,DETYN,DETP
     +    ,SN,SP,P,P2

      EXTERNAL LDETSJ
      EXTERNAL DETYN,DETP

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IJ2=LDETSJ(1,S2,I)
      Y2=DETYN(1,S2,IJ2)
      P2=DETP(Y2,IJ2)
C ON COMMENCE PAR REGARDER LA POSITION RESPECTIVE DE Q ET S2*V2
      IF(V2.EQ.0.)THEN
        IF(S2.EQ.0.)THEN
          S=0
          GO TO 100
        ENDIF
C ON COMMENCE PAR CHERCHER UN MULTIPLE DE S POUR LEQUEL LA FONCTION EST
C POSITIVE
      S=S2
      NITER=1
 1    S=2.*S
      NITER=NITER+1
      IJ=LDETSJ(1,S,I)
      Y=DETYN(1,S,IJ)
      P=DETP(Y,IJ)
      A=(S-S2)*(P-P2)-S2/S*(Q-S*V2)**2
      IF(A.GT.0.)THEN
         SP=S
         SN=.5*S
      ELSEIF(NITER.LT.50)THEN
         GO TO 1
      ELSE
         WRITE(*,*)'SP QAMONT:ERREUR'
         STOP
      ENDIF
C SI V2 NON NULLE
C POUR S=S2 A TOUJOURS NEGATIF DONC SN=S2
C POUR Q=S*V2 A TOUJOURS POSITF DONC SP
C      ELSEIF(Q.GT.S2*V2)THEN
       ELSE
         SN=S2
         SP=Q/V2
C      ELSE
C         SN=Q/V2
C         SP=S2
      ENDIF
C APRES AVOIR TROUVE L'INTERVALLE, ON COMMENCE LA DICHOTOMIE
        NITER=1
      IJ=LDETSJ(1,SP,I)
      Y1=DETYN(1,SP,IJ)
      IJ=LDETSJ(1,SN,I)
      Y3=DETYN(1,SN,IJ)
10      S = .5*(SN+SP)
        A = ABS(Y3-Y1)
        IF (A.LT.EPSY)GO TO 100
        IF(A.LT.EPS**2*ABS(Y3))GO TO 100
      IJ=LDETSJ(1,S,I)
      Y=DETYN(1,S,IJ)
      P=DETP(Y,IJ)
      A=(S-S2)*(P-P2)-S2/S*(Q-S*V2)**2
      IF(A.LT.0.)THEN
              SN = S
              Y3=Y
        ELSE
              SP = S
              Y1=Y
        ENDIF
        NITER = NITER+1
        IF(NITER.GT.50)THEN
             WRITE(*,*)'SP QAMONT NON CONVERGENT EN 50 ITERATIONS'
             WRITE(*,*)'SN=',SN,'SP=',SP
        ELSE
          GOTO 10
        ENDIF

100     RETURN
        END

C**********************************************************************
              SUBROUTINE QAVAL(S,Y,IJ,V,S2,Y2,IJ2,V2)
C---------------------------------------------------------------------C
C       CHOC EN CONDITION AVAL(GEOM.REELLE)                           C
C       RESOLUTION DE L'EQUATION EN V :                               C
C           (S-S2)*(P(S)-P(S2))-S2*S*(V-V2)**2=0                      C
C       ENTREES : S,Y,IJ                        SORTIE : V            C
C                 S2,V2,Y2,IJ2                                        C
C=====================================================================C


      INTEGER IJ,IJ2
      DOUBLE PRECISION S2,V2,Y2,S,V,Y
      DOUBLE PRECISION A
      DOUBLE PRECISION DETP,P,P2

      EXTERNAL DETP


C      IF(V2.LT.0.)THEN
      P2=DETP(Y2,IJ2)
      P=DETP(Y,IJ)
      A=(P-P2)*(S-S2)
      A=SQRT(A/(S2*S))
      IF(S.GT.S2)THEN
        V=V2-A
c        V=V2+A
      ELSE
        V=V2+A
c        V=V2-A
      ENDIF
C        ELSE
C CAS OU V2 EST POSITIVE ET OU CE N'EST PAS LA BONNE SOLUTION
C          WRITE(*,*)'CONDITION AVAL APPROXIMATIVE'
C        ENDIF
        RETURN
        END
C**********************************************************************
      SUBROUTINE QBOUVR1D(IOUV,IBOUV,TOUV)
C---------------------------------------------------------------------C
C     CALCULE LES DEBITS TRANSITANT PAR LES OUVRAGES B
C=====================================================================C

      INTEGER NOB1DMAX
      PARAMETER (NOB1DMAX=10)
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax,NOUnoe1Dmax
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000
     :,NOUnoe1Dmax=nou1Dmax*noe1Dmax)
      INTEGER LMAX
      PARAMETER(LMAX=3000)

      DOUBLE PRECISION Z1,Z2,ZG,ZD,Q
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax)
     :     ,NOUV(nou1Dmax),NBOUV
      DOUBLE PRECISION VOLOUV(nou1Dmax),TOUV
C      CHARACTER*1 TYPOUV(nou1Dmax,noe1Dmax)

      INTEGER IOUV,I(NOB1DMAX),IBOUV

      INTEGER IT(NOB1DMAX),NT(NOB1DMAX)
C      LOGICAL TOTAL
      LOGICAL KAPPA(NOB1DMAX)

      DOUBLE PRECISION NU(NOB1DMAX),KA(NOB1DMAX)
      DOUBLE PRECISION TPREC(NOB1DMAX)

      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),C1(NOB1DMAX),C2(NOB1DMAX)
     :,DT2(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX)
      DOUBLE PRECISION Q2
      DOUBLE PRECISION QL(ntr1Dmax,NOB1DMAX),QS(ntr1Dmax,NOB1DMAX)
     :,DBR(ntr1Dmax,NOB1DMAX)
     :,Z(ntr1Dmax,NOB1DMAX),ZAV(ntr1Dmax,NOB1DMAX)
     :,ZBR(ntr1Dmax,NOB1DMAX)
      DOUBLE PRECISION TN,DTN,TNP1
     :,DBMAX(NOB1DMAX),TRECT(NOB1DMAX),TRUP(NOB1DMAX)
     :,CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDVOLOU/VOLOUV
      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDRESUL/QL,QS,DBR,Z,ZAV,ZBR
      COMMON/DDTINIB/TRUP
      COMMON/DDLAG/KAPPA
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/TREEL/TN,DTN,TNP1

      DATA I/NOB1DMAX*1/

      SAVE TPREC,I
      IF(TOUV.LT.TRUP(IBOUV))THEN
        RETURN
      ELSE
C---- DEFINITION DE LA COTE GAUCHE ET DROITE---
         Z1=HOUV(IA1(IOUV),1)+XCTDF(IA1(IOUV))
         IF(IA2(IOUV).GT.0)THEN
           Z2=HOUV(IA2(IOUV),2)+XCTDF(IA2(IOUV))
         ELSE
           Z2=-99999.999
         ENDIF
         IF(Z1.GT.Z2+EPSY)THEN
           IF(HOUV(IA1(IOUV),1).GT.EPSY)THEN
             ZG=Z1
             ZD=Z2
           ELSE
             RETURN
           ENDIF
         ELSEIF(Z2.GT.Z1+EPSY)THEN
           IF(HOUV(IA2(IOUV),2).GT.EPSY)THEN
             ZG=Z2
             ZD=Z1
           ELSE
             RETURN
           ENDIF
         ELSE
           RETURN
         ENDIF

C    BOUCLE DE CALCUL
C  (INITIALISATION DES VARIABLES)
      IF(I(IBOUV).EQ.1)THEN
        ZAV(I(IBOUV),IBOUV)=ZD
        Z(I(IBOUV),IBOUV)=ZG
        DBR(I(IBOUV),IBOUV)=DB(IBOUV)
        ZBR(I(IBOUV),IBOUV)=ZB(IBOUV)
        QS(I(IBOUV),IBOUV)=0.
        QL(I(IBOUV),IBOUV)=0.
        TPREC(IBOUV)=TRUP(IBOUV)
        I(IBOUV)=2
        ZAV(I(IBOUV),IBOUV)=ZD
        Z(I(IBOUV),IBOUV)=ZG
        DBR(I(IBOUV),IBOUV)=DB(IBOUV)
        ZBR(I(IBOUV),IBOUV)=ZB(IBOUV)
        QS(I(IBOUV),IBOUV)=0.
        QL(I(IBOUV),IBOUV)=0.
      ENDIF

      CALL CALCULB(ZG,Q,Q2,ZD,IBOUV)
C  (BOUCLE DE CHANGEMENT DE GEOMETRIE D''EROSION)
      IF(TOUV.GT.TPREC(IBOUV))THEN
        ZAV(I(IBOUV),IBOUV)=ZD
        Z(I(IBOUV),IBOUV)=ZG
        ZBR(I(IBOUV),IBOUV)=ZB(IBOUV)
        DBR(I(IBOUV),IBOUV)=DB(IBOUV)
        QL(I(IBOUV),IBOUV)=Q
        QS(I(IBOUV),IBOUV)=Q2
C renard
        IF(IT(IBOUV).EQ.0)THEN
C effondrement voute
          IF(DB(IBOUV).GE.0.66666667*(ZC(IBOUV)-ZB0(IBOUV)))THEN
            KAPPA(IBOUV)=.TRUE.
            IT(IBOUV)=1
            IF(YM(IBOUV).GT.0.)THEN
            DB(IBOUV)=SM(IBOUV)/YM(IBOUV)
            ENDIF
            DBR(I(IBOUV),IBOUV)=DB(IBOUV)
            WRITE(*,*)
            WRITE(*,120)
     :'PASSAGE EN ELARGISSEMENT RECTANGULAIRE AU TEMPS',TOUV
  120       FORMAT(A40,F10.0)
            WRITE(*,*)'OUVRAGE ',IOUV,' breche ',IBOUV
            TRECT(IBOUV)=TOUV
            WRITE(*,*)
          ENDIF
C surverse en approfondissement
        ELSEIF(IT(IBOUV).LT.0)THEN
C           IF(R(I).LT.ZP)R(I)=ZP
C basculement en elargissement ramene de SP calcul
            IF(ZB(IBOUV).LT.ZP(IBOUV))THEN
              IT(IBOUV)=-IT(IBOUV)
              ZB(IBOUV)=ZP(IBOUV)
              ZBR(I(IBOUV),IBOUV)=ZB(IBOUV)
c            WRITE(*,*) TOUV, TRUP(IBOUV)
            WRITE(*,120)
     :'PASSAGE EN ELARGISSEMENT RECTANGULAIRE AU TEMPS',TOUV
            WRITE(*,*)'OUVRAGE ',IOUV,' breche ',IBOUV
            TRECT(IBOUV)=TOUV
            WRITE(*,*)
            ENDIF
C     si(IT.GT.0)RIEN
        ENDIF
        IF(TOUV.GT.FLOAT(I(IBOUV)-1)*DT2(IBOUV)+TRUP(IBOUV))THEN
C     :.AND.T.GT.TPREC(IBOUV)
C     :)THEN
C  (BOUCLE D''INCREMENTATION)
C on enregistre jusqua ntr1Dmax et pas NT pour le cas de fausse manoeuvre
          IF(I(IBOUV).LT.ntr1Dmax)THEN
            I(IBOUV)=I(IBOUV)+1
            DBR(I(IBOUV),IBOUV)=DBR(I(IBOUV)-1,IBOUV)
            ZBR(I(IBOUV),IBOUV)=ZBR(I(IBOUV)-1,IBOUV)
          ENDIF
        ENDIF
        TPREC(IBOUV)=TOUV
C else du if sur t et tprec pour changement geometrie
C dans ce cas on revient en arriere
      ELSE
        ZB(IBOUV)=ZBR(I(IBOUV),IBOUV)
        DB(IBOUV)=DBR(I(IBOUV),IBOUV)
C fin du if sur t et tprec pour changement geometrie
      ENDIF

C      TPREC(IBOUV)=TOUV
C enregistrement du debit Q calcule
         IF(Z1.GT.Z2)THEN
            VOLOUV(IOUV)=VOLOUV(IOUV)+Q
            QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
            IF(IA2(IOUV).GT.0)THEN
             QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q
            ENDIF
C SI Z2 >Z1
           ELSE
            VOLOUV(IOUV)=VOLOUV(IOUV)-Q
            QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)-Q
            IF(IA2(IOUV).GT.0)THEN
             QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)-Q
           ENDIF
C fin du if sur Z1/Z2
           ENDIF
C FIn DU IF sur touv superieur a trup
      ENDIF
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE QOUVR1D
C-----------------------------------------------------------------------
C Calcul des d�bits transitant par les ouvrages
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax,NOUnoe1Dmax,NOB1DMAX
         PARAMETER(NOB1DMAX=10)
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000
     :,NOUnoe1Dmax=nou1Dmax*noe1Dmax)
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      DOUBLE PRECISION RAC2G,Z1,Z2,ZG,ZD,Q,GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)
      INTEGER NBMOVY(nou1Dmax,noe1Dmax)
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax),NBOUV
     :  ,NBCOU2(nou1Dmax,noe1Dmax),NBCOU1(nou1Dmax,noe1Dmax)
      DOUBLE PRECISION LONG(nou1Dmax,noe1Dmax),ZDEV(nou1Dmax,noe1Dmax)
     :,HAUT(nou1Dmax,noe1Dmax),COEF(nou1Dmax,noe1Dmax),VOLOUV(nou1Dmax)
     :  ,ZOUV(nou1Dmax,noe1Dmax),COEFIN(nou1Dmax,noe1Dmax)
     :  ,ZFERM(nou1Dmax,noe1Dmax),COEFAC(nou1Dmax,noe1Dmax)
     :,QCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
     :,ZCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
      DOUBLE PRECISION TN,DTN,TNP1
C         ,TPREC(NOB1DMAX)
     :,T,TOUV,QPRIME
      CHARACTER*1 TYPOUV(nou1Dmax,noe1Dmax)
      INTEGER IOUV,J,I,IOUB(nou1Dmax,noe1Dmax),NOB,IBOUV,K,IMP
C IFB donne la forme de la breche (rectangulaire=1, trapezoidale=2)
      LOGICAL DEBUT1D
c      INTEGER IT,NT,K,IMP
c      DOUBLE PRECISION NU
c      DOUBLE PRECISION KA,DT2
c      DOUBLE PRECISION ZC,ZP,ZB,ZB0,Z0,ALP,ALC,RHO,PHI,DB0,DB,D50
c      DOUBLE PRECISION ETA,C1,C2,DBMAX,TRUP
c      DOUBLE PRECISION YM,SM,PM,RHM,ALM
c      DOUBLE PRECISION QL(ntr1Dmax),QS(ntr1Dmax),R(ntr1Dmax),Z(ntr1Dmax)
c     :  ,ZAV(ntr1Dmax),TAU(ntr1Dmax),Q2,TAUI
c      LOGICAL TOTAL,KAPPA
C       LOGICAL TOTAL

      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DDHQOUV/HOUV,QOUV
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDCOUVRA/LONG,ZDEV,HAUT,COEF
      COMMON/DDTOUVRA/TYPOUV
      COMMON/DDVOLOU/VOLOUV
      COMMON/YOUVRA/COEFIN,ZOUV,ZFERM
      COMMON/DDNOUVRA/NBCOU1,NBCOU2
      COMMON/DDZOUVRA/QCOUP,ZCOUP
      COMMON/TREEL/TN,DTN,TNP1
c      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
c      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
c      COMMON/DDMAXBRE/DBMAX,TRECT
c      COMMON/DDRESUL/QL,QS,DBR,Z,ZAV,ZBR
c      COMMON/DDTINIB/TRUP
c      COMMON/DDELAPPR/ELAP
c      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
c      COMMON/DDNCONST/NT
c      COMMON/DDBRECHE/ZB,DB,IT
c      COMMON/DDLAG/KAPPA
      COMMON/DDIOBREC/IOUB,NOB

C      SAVE TOTAL,I,IMP,NBMOVY
      SAVE I,IMP,NBMOVY
      SAVE COEFAC,DEBUT1D,RAC2G
C      DATA TOTAL/.TRUE./
      DATA DEBUT1D/.TRUE./
      DATA I/1/
      DATA IMP/2/
      DATA NBMOVY/NOUnoe1Dmax*0/

C      Write(*,*)'Qouvr: entr�e'
C TOTAL = VRAI si on n'est pas � un pas de temps intermediaire
c      TOUV=TNP1-0.5*DTN
      TOUV=TN
c      TOTAL=.NOT.TOTAL
C a deplacer dans lecture ouvrage car initialisation
      IF(DEBUT1D)THEN
        DEBUT1D=.NOT.DEBUT1D
        RAC2G=SQRT(2.*GRAV)
        DO IOUV=1,NBOUV
          DO J=1,NOUV(IOUV)
            COEFAC(IOUV,J)=COEFIN(IOUV,J)
          ENDDO
        ENDDO
      ENDIF
C modif du 18/3/2014 pour separer remise a zero et calcul qouv
C car une arete peut appartenir a plusieurs ouvrages
      DO IOUV=1,NBOUV
        VOLOUV(IOUV)=0.
        QOUV(IA1(IOUV),1)=0.
        QOUV(IA2(IOUV),2)=0.
      enddo
      DO IOUV=1,NBOUV
        DO J=1,NOUV(IOUV)

C Ouvrage de type D
C-----------------------------------------------------------------------
          IF(TYPOUV(IOUV,J).EQ.'D')THEN
            Z1=HOUV(IA1(IOUV),1)+XCTDF(IA1(IOUV))
            IF(IA2(IOUV).GT.0)THEN
              Z2=HOUV(IA2(IOUV),2)+XCTDF(IA2(IOUV))
            ELSE
              Z2=-99999.999
            ENDIF
            IF(Z1.GT.Z2+EPSY)THEN
              IF(HOUV(IA1(IOUV),1).GT.EPSY)THEN
                ZG=Z1
                ZD=Z2
              ELSE
                GOTO 2
              ENDIF
            ELSEIF(Z2.GT.Z1+EPSY)THEN
              IF(HOUV(IA2(IOUV),2).GT.EPSY)THEN
                ZG=Z2
                ZD=Z1
              ELSE
                GOTO 2
              ENDIF
            ELSE
              GOTO 2
            ENDIF

C R�gime d'�coulement noy� / d�noy�
C----------------------------------
            IF(ZD.GT.0.6667*ZG+0.3333*ZDEV(IOUV,J))THEN
              IF(ZD.LT.ZDEV(IOUV,J)+0.58095*HAUT(IOUV,J))THEN
C D�versoir noy�
                IF(ZD.GT.ZDEV(IOUV,J)+EPSY)THEN
                  Q=2.59808*COEF(IOUV,J)*LONG(IOUV,J)*RAC2G
     &              *(ZD-ZDEV(IOUV,J))*SQRT(ZG-ZD)
                ELSE
                  Q=0.
                ENDIF
              ELSE
C Orifice noy�
                Q=1.50935*COEF(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *HAUT(IOUV,J)*SQRT(ZG-ZD)
              ENDIF
            ELSE
              IF(ZG.LT.ZDEV(IOUV,J)+1.125*HAUT(IOUV,J))THEN
C D�versoir d�noy�
                IF(ZG.GT.ZDEV(IOUV,J)+EPSY)THEN
                  Q=COEF(IOUV,J)*LONG(IOUV,J)*RAC2G
     &              *(ZG-ZDEV(IOUV,J))**1.5
                ELSE
                  Q=0.
                ENDIF
              ELSEIF(ZD.LT.ZDEV(IOUV,J)+0.5*HAUT(IOUV,J))THEN
C Orifice d�noy�
                Q=1.50935*COEF(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *SQRT(ZG-ZDEV(IOUV,J)-0.5*HAUT(IOUV,J))*HAUT(IOUV,J)
              ELSE
C Orifice noy�
                Q=1.50935*COEF(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *HAUT(IOUV,J)*SQRT(ZG-ZD)
              ENDIF
            ENDIF

            IF(Z1.GT.Z2)THEN
              VOLOUV(IOUV)=VOLOUV(IOUV)+Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q
            ELSE
              VOLOUV(IOUV)=VOLOUV(IOUV)-Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)-Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)-Q
            ENDIF

C Ouvrage de type Y (vanne se fermant et s'ouvrant selon  2 cotes amont)
C-----------------------------------------------------------------------
          ELSEIF(TYPOUV(IOUV,J).EQ.'Y')THEN
C      Write(*,*)'Qouvr: d�but vanne Y; IOUV=',IOUV,' J=',J
C Z1 / Z2: cote absolue de l'eau � l'amont / � l'aval de la vanne d'un point de vue g�ographique
C ZG / ZD: cote absolue de l'eau � l'amont / � l'aval de la vanne d'un point de vue hydraulique
            Z1=HOUV(IA1(IOUV),1)+XCTDF(IA1(IOUV))
            IF(IA2(IOUV).NE.0)THEN
C l'interface aval de l'ouvrage existe
              Z2=HOUV(IA2(IOUV),2)+XCTDF(IA2(IOUV))
            ELSE
C d�versement de l'ouvrage vers l'ext�rieur du mod�le
              Z2=-99999.999
            ENDIF
C Mouvements de la vanne
C-----------------------
C Si Z2 > Z1 ou si hauteur amont nulle ou si sous la cote de fermeture, la vanne se ferme
C Si au dessus de la cote d'ouverture, la vanne s'ouvre
C Sinon elle reste dans l'�tat dans lequel elle �tait
            IF(Z1.LT.Z2)THEN
              IF(COEFAC(IOUV,J).NE.0.)THEN
                NBMOVY(IOUV,J)=NBMOVY(IOUV,J)+1
                WRITE(*,'(A,I2,A,I2,A,I2,A,F8.3,A,F8.3,A,F11.2)')
     &            'VanneY [',IOUV,';',J,'], manoeuvre ',NBMOVY(IOUV,J)
     &            ,': fermeture Z1=',Z1,' Z2=',Z2,' a Tn=',TN
              ENDIF
              COEFAC(IOUV,J)=0.
            ELSEIF(HOUV(IA1(IOUV),1).LT.EPSY)THEN
              IF(COEFAC(IOUV,J).NE.0.)THEN
                NBMOVY(IOUV,J)=NBMOVY(IOUV,J)+1
                WRITE(*,'(A,I2,A,I2,A,I2,A,I2,A,F8.3,A,F11.2)')
     &            'VanneY [',IOUV,';',J,'], manoeuvre ',NBMOVY(IOUV,J)
     &            ,': fermeture HOUV(',IA1(IOUV),',1)='
     &            ,HOUV(IA1(IOUV),1),' a Tn=',TN
              ENDIF
              COEFAC(IOUV,J)=0.
            ELSEIF(Z1.LT.ZFERM(IOUV,J))THEN
              IF(COEFAC(IOUV,J).NE.0.)THEN
                NBMOVY(IOUV,J)=NBMOVY(IOUV,J)+1
                WRITE(*,'(A,I2,A,I2,A,I2,A,F8.3,A,F8.3,A,F11.2)')
     &            'VanneY [',IOUV,';',J,'], manoeuvre ',NBMOVY(IOUV,J)
     &            ,': fermeture Z1=',Z1,' CotFer=',ZFERM(IOUV,J)
     &            ,' a Tn=',TN
              ENDIF
              COEFAC(IOUV,J)=0.
            ELSEIF(Z1.GT.ZOUV(IOUV,J))THEN
              IF(COEFAC(IOUV,J).EQ.0.)THEN
                NBMOVY(IOUV,J)=NBMOVY(IOUV,J)+1
                WRITE(*,'(A,I2,A,I2,A,I2,A,F8.3,A,F8.3,A,F11.2)')
     &            'VanneY [',IOUV,';',J,'], manoeuvre ',NBMOVY(IOUV,J)
     &            ,': ouverture Z1=',Z1,' Zouv=',ZOUV(IOUV,J)
     &            ,' a Tn=',TN
              ENDIF
              COEFAC(IOUV,J)=COEF(IOUV,J)
            ENDIF
            IF(COEFAC(IOUV,J).EQ.0.)THEN
              ZG=XCTDF(IA1(IOUV))
              ZD=XCTDF(IA2(IOUV))
            ELSE
              ZG=Z1
              ZD=Z2
            ENDIF
C R�gime d'�coulement noy� / d�noy�
C----------------------------------
            IF(ZD.GT.0.66667*ZG+0.33333*ZDEV(IOUV,J))THEN
              IF(ZD.LT.ZDEV(IOUV,J)+0.58095*HAUT(IOUV,J))THEN
C D�versoir noy�
                IF(ZD.GT.ZDEV(IOUV,J)+EPSY)THEN
                  Q=2.59808*COEFAC(IOUV,J)*LONG(IOUV,J)*RAC2G
     &              *(ZD-ZDEV(IOUV,J))*SQRT(ZG-ZD)
                ELSE
                  Q=0.
                ENDIF
              ELSE
C Orifice noy�
                Q=1.50935*COEFAC(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *HAUT(IOUV,J)*SQRT(ZG-ZD)
              ENDIF
            ELSE
              IF(ZG.LT.ZDEV(IOUV,J)+1.125*HAUT(IOUV,J))THEN
C D�versoir d�noy�
                IF(ZG.GT.ZDEV(IOUV,J)+EPSY)THEN
                  Q=COEFAC(IOUV,J)*LONG(IOUV,J)*RAC2G
     &              *(ZG-ZDEV(IOUV,J))**1.5
                ELSE
                  Q=0.
                ENDIF
              ELSEIF(ZD.LT.ZDEV(IOUV,J)+0.5*HAUT(IOUV,J))THEN
C Orifice d�noy�
                Q=1.50935*COEFAC(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *SQRT(ZG-ZDEV(IOUV,J)-0.5*HAUT(IOUV,J))*HAUT(IOUV,J)
              ELSE
C Orifice noy�
                Q=1.50935*COEFAC(IOUV,J)*LONG(IOUV,J)*RAC2G
     &            *HAUT(IOUV,J)*SQRT(ZG-ZD)
              ENDIF
            ENDIF
C Calcul du d�bit d'ouvrage
C--------------------------
            IF(Z1.GT.Z2)THEN
              VOLOUV(IOUV)=VOLOUV(IOUV)+Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q
            ELSE
              VOLOUV(IOUV)=VOLOUV(IOUV)-Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)-Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)-Q
            ENDIF
C      Write(*,*)'Qouvr: fin vanne Y; IOUV=',IOUV,' J=',J

C Ouvrage de type O orifice circulaire
C-----------------------------------------------------------------------

           ELSEIF(TYPOUV(IOUV,J).EQ.'O')THEN
            Z1=HOUV(IA1(IOUV),1)+XCTDF(IA1(IOUV))
            IF(IA2(IOUV).GT.0)THEN
              Z2=HOUV(IA2(IOUV),2)+XCTDF(IA2(IOUV))
            ELSE
              Z2=-99999.999
            ENDIF
            IF(Z1.GT.Z2+EPSY)THEN
              IF(HOUV(IA1(IOUV),1).GT.EPSY)THEN
                ZG=Z1
                ZD=Z2
              ELSE
                GOTO 2
              ENDIF
            ELSEIF(Z2.GT.Z1+EPSY)THEN
              IF(HOUV(IA2(IOUV),2).GT.EPSY)THEN
                ZG=Z2
                ZD=Z1
              ELSE
                GOTO 2
              ENDIF
            ELSE
              GOTO 2
            ENDIF

C si submersionde l'entree
                IF(ZG-ZDEV(IOUV,J).GT.HAUT(IOUV,J))THEN
              IF(ZG.GT.ZDEV(IOUV,J)+EPSY)THEN
C DEVERSOIR DENOYE
               Q=(ZG-ZDEV(IOUV,J))**1.5
C perte de charge singuliere de coefficient 0.5
C materiau suppose beton strickler de 90
C coef standard suppose egal a 0.3
C 10.472=pi/0.3
C 25 septembre 2019 modification 2.618 = pi/0.3/4 au lieu de 10.472
C conduite noyee
               Qprime=2.618*HAUT(iouv,j)*
     :SQRT((ZG-ZD)/(1.5+0.000610*Grav
     :*LONG(IOUV,J)*HAUT(IOUV,J)**(-1.333)))
               Q=min(Q,Qprime)*HAUT(IOUV,J)*COEF(IOUV,J)*RAC2G
              ELSE
               Q=0.
              ENDIF
C si non submersion de l'entree
                          ELSE
C DEVERSOIR DENOYE = seul cas pris en compte
              IF(ZG.GT.ZDEV(IOUV,J)+EPSY)THEN
               Q=COEF(IOUV,J)*HAUT(IOUV,J)**0.6*RAC2G*
     :(ZG-ZDEV(IOUV,J))**1.9
              ELSE
               Q=0.
              ENDIF
C fin du if sur submersion entree
                          ENDIF
C Calcul du d�bit d'ouvrage
C--------------------------
            IF(Z1.GT.Z2)THEN
              VOLOUV(IOUV)=VOLOUV(IOUV)+Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q
            ELSE
              VOLOUV(IOUV)=VOLOUV(IOUV)-Q
              QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)-Q
              IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)-Q
            ENDIF


C Ouvrage de type Z
C-----------------------------------------------------------------------
          ELSEIF(TYPOUV(IOUV,J).EQ.'Z')THEN
            Z1=HOUV(IA1(IOUV),1)+XCTDF(IA1(IOUV))
            DO 3 K=NBCOU1(IOUV,J),NBCOU2(IOUV,J)
              IF(ZCOUP(K).GT.Z1)THEN
                IF(K.EQ.NBCOU1(IOUV,J))THEN
                  Q=QCOUP(K)
                ELSE
                  Q=QCOUP(K-1)+(Z1-ZCOUP(K-1))/(ZCOUP(K)-ZCOUP(K-1))
     &              *(QCOUP(K)-QCOUP(K-1))
                ENDIF
                GOTO 4
              ENDIF
 3          CONTINUE
            Q=QCOUP(NBCOU2(IOUV,J))
 4          VOLOUV(IOUV)=VOLOUV(IOUV)+Q
            QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
            IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q

C Ouvrage de type Q
C-----------------------------------------------------------------------
          ELSEIF(TYPOUV(IOUV,J).EQ.'Q')THEN
c            IF(TOTAL)THEN
c              T=TNP1
c            ELSE
c              T=0.5*(TN+TNP1)
c            ENDIF
            T=TOUV
            DO 103 K=NBCOU1(IOUV,J),NBCOU2(IOUV,J)
              IF(ZCOUP(K).GT.T)THEN
                IF(K.EQ.NBCOU1(IOUV,J))THEN
                  Q=QCOUP(K)
                ELSE
                  Q=QCOUP(K-1)+(T-ZCOUP(K-1))/(ZCOUP(K)-ZCOUP(K-1))
     &              *(QCOUP(K)-QCOUP(K-1))
                ENDIF
                GOTO 104
              ENDIF
 103        CONTINUE
            Q=QCOUP(NBCOU2(IOUV,J))
 104        VOLOUV(IOUV)=VOLOUV(IOUV)+Q
            QOUV(IA1(IOUV),1)=QOUV(IA1(IOUV),1)+Q
            IF(IA2(IOUV).GT.0) QOUV(IA2(IOUV),2)=QOUV(IA2(IOUV),2)+Q

          ELSEIF(TYPOUV(IOUV,J).EQ.'B')THEN
            IBOUV=IOUB(IOUV,J)
            CALL QBOUVR1D(IOUV,IBOUV,TOUV)

C Fin du IF sur le type d'ouvrage
          ENDIF

    2     CONTINUE
        ENDDO
C      Write(*,*)'Tn=',TN,' QOUV(',IA1(IOUV),',1)=',QOUV(IA1(IOUV),1)
C     &  ,' QOUV(',IA2(IOUV),',2)=',QOUV(IA2(IOUV),2)

C    1   CONTINUE
      ENDDO

C      Write(*,*)'Qouvr: sortie'
      END


C-----------------------------------------------------------------------
      SUBROUTINE QYAV(Y1,S1,V1,H1,V2,S2,YL2,IJ,LL2,IB)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NBMAX,LL2,IB
      INTEGER IJ,LMAX,LNCMAX,NITER,I,NCLMAX
      PARAMETER(LNCMAX=130000,LMAX=3000,NCLMAX=1000,NBMAX=150)
      INTEGER NC(0:LMAX),XNC(0:LMAX),NT2(0:NBMAX)
      DOUBLE PRECISION YH(NCLMAX),QYH(NCLMAX)
      DOUBLE PRECISION S1,V1,H1,Y1,Q,S2,V2,YLP,YLM,YL2,Q2,YL,DQ
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      INTEGER LDETSJ,LDETYJ
      DOUBLE PRECISION DETYN,DETSN,DETH
      EXTERNAL DETYN,DETSN,DETH,LDETSJ,LDETYJ

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
C     COMMON/PHYS/LM,LL
      COMMON/HYDAV/YH,QYH
      COMMON/NHYDA/NT2
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NC/NC,XNC
      COMMON/GEOMT/TMAIL,CTDF,PEN

      IF(NT2(IB)-NT2(IB-1).EQ.1)THEN
        YL2=YH(NT2(IB))-CTDF(LL2+1)
        IJ=LDETYJ(1,YL2,LL2)
        S2=DETSN(1,YL2,IJ)
C      IF(NT2.EQ.1)THEN
c        YL2=YH(1)-CTDF(LM)
c        IJ=LDETYJ(1,YL2,LL)
c        S2=DETSN(1,YL2,IJ)
C DETENTE
C        IF(IDS.EQ.1)THEN
        V2=V1+H1-DETH(YL2,IJ,LL2)
CC CHOC APPROCHE
C        ELSEIF(S2.GT.0.)THEN
C          V2=(V1*S1+H1*(S2-S1))/S2
C        ELSE
C          V2=0.
C        ENDIF
C if sur une seule valeur dans loi de tarage
      ELSE
C introduction du cas ou pour q=0 y non nul
        YL2=YH(NT2(IB-1)+1)-CTDF(LL2+1)
              if( abs(QYH(nt2(ib-1)+1)).LT.EPS)THEN
          YL2=YH(NT2(IB-1)+1)-CTDF(LL2+1)
                  if (yl2.gt.epsy)then
             IJ=LDETYJ(1,YL2,LL2)
                   v2=v1+h1-DETH(YL2,IJ,LL2)
                      if(v2.lt.0.)then
C cas ou la loi de tarage ne peut etre satisfaite
C on impose cote donnee et Q=0
               S2=DETSN(1,YL2,IJ)
                        V2=0.
                        return
             endif
                 endif
              endif
        Q=S1*V1
        YLM=0.
        YLP=XYISEC(XNC(LL2))
C        YLP=0.
        YL2=Y1
        NITER=0
150     IF(Q.LT.0.)Q=0.
C-----ON CHERCHE LA VALEUR DE QYH IMMEDIATEMENT SUPERIEURE A Q
       DO 140  I=NT2(IB-1)+1,NT2(IB)
         IF(Q .LT.QYH(I))THEN
          IF(I.EQ.NT2(IB-1)+1)THEN
           YL=YH(I)-CTDF(LL2+1)
          ELSE
           YL=YH(I)+(YH(I)-YH(I-1))/(QYH(I)-QYH(I-1))*(Q-QYH(I))
           YL=YL-CTDF(LL2+1)
          ENDIF
            GOTO 130
          ENDIF
140     CONTINUE
        YL=YH(NT2(IB))-CTDF(LL2+1)
130     IF(YL.GT.YL2)THEN
          YLM=YL2
        ELSE
          YLP=YL2
        ENDIF
        DQ=Q-V2*S2
        IF(ABS(Q).LT.EPS.AND.ABS(V2).LT.EPS)DQ=0.
C------ON ARRETE LE CALCUL DES QU'ON A TROUVE UNE VALEUR  A EPS PRES
C        IF(YLP.LE.0.)THEN
C          YL2=YL
C        ELSE
        YL2=.5*(YLP+YLM)
C        ENDIF
        IJ=LDETYJ(1,YL2,LL2)
        S2=DETSN(1,YL2,IJ)
C        IF(IDS.EQ.1)THEN
        V2=V1+H1-DETH(YL2,IJ,LL2)
        Q=V2*S2
C        ELSE IF(S2.GT.0.)THEN
C          Q=S1*V1+H1*(S2-S1)
C          V2=Q/S2
C        ELSE
C          Q=0.
C          V2=0.
C        ENDIF
C        IF(ABS(YLP-YLM).LT.EPSY)RETURN
        IF(ABS(YLP-YLM).LT.EPSY.AND.ABS(DQ).LE.EPS*Q)RETURN
        NITER=NITER+1
        IF(NITER.GT.50)THEN
          WRITE(*,*)'CONDITION AVAL NON VERIFIEE'
        Q2=QYH(I)-(YH(I)-CTDF(LL2+1)-YL2)*
     :(QYH(I)-QYH(I-1))/(YH(I)-YH(I-1))
          WRITE(*,*)'Q OBTENU=',Q,'Q THEORIQUE=',Q2
          WRITE(*,*)'Y MAX=',YLP,'Y MIN=',YLM
        ELSE
          GOTO 150
        ENDIF
c fin du if sur une seule valeur dans loi de tarage
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE QYAVUN(Y1,V1,H1,V2,S2,YL2,IJ,LL2)
C-----------------------------------------------------------------------
C Calcul d'une detente en condition limite aval dans le cas de regime uniforme
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL2,LNCMAX
      INTEGER IJ,LMAX,NITER
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION Y1,V1,H1,V2,S2,YL2,Q,YLP,YLM,DQ,RACIN
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     +  ,XLYSEC(LNCMAX)
     +  ,XSECUM(LNCMAX),XPICUM(LNCMAX),XPECUM(LNCMAX)
      INTEGER LDETYJ
      DOUBLE PRECISION FR1(LMAX),TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION DETSN,DETH,DETPM,SMBQ2
      EXTERNAL DETSN,DETH,LDETYJ,DETPM,SMBQ2

      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NC/NC,XNC
      COMMON/FROTMT/FR1

      YLM=0.
      YLP=MAX(XYISEC(XNC(LL2)),2.*Y1)
      YL2=Y1
      NITER=0
      GOTO 150
 130  CONTINUE
      IF(DQ.LT.0.)THEN
        YLM=YL2
      ELSE
        YLP=YL2
      ENDIF
      YL2=.5*(YLP+YLM)
 150  CONTINUE
      IJ=LDETYJ(1,YL2,LL2)
      S2=DETSN(1,YL2,IJ)
C Q contient en fait la vitesse pour que y=0 ne soit pas solution
c      Write(*,*)'QYAVUN PEN(LL2): PEN(',LL2,')=',PEN(LL2)
      IF(PEN(LL2).LE.EPS)THEN
C le r�gime uniforme existe seulement pour pente positive
              Q=0.
      ELSE
         V2=1.
         RACIN=SMBQ2(1,V2,LL2,YL2)
         IF(RACIN.LT.0.)THEN
           RACIN=-GRAV*S2*PEN(LL2)/RACIN
           IF(RACIN.GT.eps)THEN
             Q=SQRT(RACIN)
           ELSE
             Q=0.
           ENDIF
         ELSE
           Q=0.
         ENDIF
C        Q=FR1(LL2)*(S2/DETPM(1,YL2,IJ,LL2))**0.666667*SQRT(PEN(LL2))
      ENDIF
      V2=V1+H1-DETH(YL2,IJ,LL2)
C DQ est une fonction croissante de YL2 negative pour yl2=0
      DQ=Q-V2
      IF(ABS(YLP-YLM).LT.EPSY.AND.ABS(DQ).LE.EPS*Q)RETURN
        NITER=NITER+1
        IF(NITER.GT.50)THEN
C          WRITE(*,*)'CONDITION AVAL NON VERIFIEE',YLP-YLM
C          WRITE(*,*)'Q OBTENU=',Q,'Q THEORIQUE=',V2*S2
C          WRITE(*,*)'Y MAX=',YLP,'Y MIN=',YLM
        ELSE
          GOTO 130
        ENDIF

      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE QYAVCR(Y1,V1,H1,V2,S2,YL2,IJ,LL2)
C-----------------------------------------------------------------------
C Calcul d'une detente en condition limite aval dans le cas de regime critique
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LL2,LNCMAX
      INTEGER IJ,LMAX,NITER
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION Y1,V1,H1,V2,S2,YL2,Q,YLP,YLM,DQ
      DOUBLE PRECISION EPS,GRAV,CHEZY,EPSY,EPSM
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     +  ,XLYSEC(LNCMAX)
     +  ,XSECUM(LNCMAX),XPICUM(LNCMAX),XPECUM(LNCMAX)
      INTEGER LDETYJ
      DOUBLE PRECISION FR1(LMAX),TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION DETSN,DETH,DETPM,DETCN
      EXTERNAL DETSN,DETH,LDETYJ,DETPM,DETCN

      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NC/NC,XNC
      COMMON/FROTMT/FR1

      YLM=0.
      YLP=MAX(XYISEC(XNC(LL2)),2.*Y1)
      YL2=Y1
      NITER=0
      GOTO 150
 130  CONTINUE
      IF(DQ.LT.0.)THEN
        YLM=YL2
      ELSE
        YLP=YL2
      ENDIF
      YL2=.5*(YLP+YLM)
 150  CONTINUE
      IJ=LDETYJ(1,YL2,LL2)
      S2=DETSN(1,YL2,IJ)
C Q contient en fait la vitesse pour que y=0 ne soit pas solution
C      Write(*,*)'QYAVUN PEN(LL2): PEN(',LL2,')=',PEN(LL2)
      Q=DETCN(1,YL2,IJ)
      V2=V1+H1-DETH(YL2,IJ,LL2)
C DQ est une fonction croissante de YL2 negative pour yl2=0
      DQ=Q-V2
      IF(ABS(YLP-YLM).LT.EPSY.AND.ABS(DQ).LE.EPS*Q)RETURN
        NITER=NITER+1
        IF(NITER.GT.50)THEN
C          WRITE(*,*)'CONDITION AVAL NON VERIFIEE',YLP-YLM
C          WRITE(*,*)'Q OBTENU=',Q,'Q THEORIQUE=',V2*S2
C          WRITE(*,*)'Y MAX=',YLP,'Y MIN=',YLM
        ELSE
          GOTO 130
        ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE TRXYI (X,I,nb,TROUVE)
C-----------------------------------------------------------------------
C V�rifie que les coordonn�es des interfaces d'un ouvrage sont bonnes
C Entr�e: X abscisse de l'interface portant l'ouvrage
C Sortie: I numero de l'interface
C         TROUVE variable logique indiquant si l'interface est trouv�e
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX,NBB,M,nb
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER I,LM(0:NBMAX),LL
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION X
      LOGICAL TROUVE

      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB

      IF(NB.LT.1.OR.NB.GT.NBB)THEN
      DO 3 M=1,NBB
      DO 1 I=LM(M-1)+1,LM(M)-1
C 0.0006 CAR LES COORDONNEES SONT EN F11.3
C mis a 0.0011 pour permettre une erreur de 1 mm (pamhyr)
      IF(ABS(X-XTMAIL(I)).LT.0.0011)THEN
             NB=M
          TROUVE=.TRUE.
          RETURN
C        ENDIF
      ENDIF
   1  CONTINUE
 3    CONTINUE
C bief designe
      ELSE
              m=nb
      DO I=LM(M-1)+1,LM(M)-1
C 0.0006 CAR LES COORDONNEES SONT EN F11.3
C mis a 0.0011 pour permettre une erreur de 1 mm (pamhyr)
      IF(ABS(X-XTMAIL(I)).LT.0.0011)THEN
          TROUVE=.TRUE.
          RETURN
      ENDIF
      ENDDO
c fin du if sur nb
      endif
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE DEBSAM(T,IB)
C-----------------------------------------------------------------------
C Calcule le d�bit solide � la condition limite amont au temps T (en l'occurence Tn+1/2)
C Renvoie le d�bit solide QS, le diam�tre D et l'�tendue S
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NCLMAX,NBMAX
      PARAMETER(LMAX=3000,NCLMAX=1000,NBMAX=150)
      INTEGER NTHSAM(0:NBMAX),I,IB,NBB
      DOUBLE PRECISION T,QS,D,S
      DOUBLE PRECISION THSAM(NCLMAX),QTHSAM(NCLMAX),DTHSAM(NCLMAX)
     &  ,STHSAM(NCLMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION QSR(LMAX),DCHAR(LMAX),DDCHAR(LMAX)
     &  ,VCHUT(LMAX),CAPSOL(LMAX),DMOB(LMAX),SMOB(LMAX)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER LM(0:NBMAX)
     +       ,LL,CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      LOGICAL HYDSAM(NBMAX),TROUVE
         INTEGER CONFLU(NBMAX,3),J,NCONF,NCONF2,IB0


      COMMON/MAILTN/SN,QN
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NSAM/NTHSAM
      COMMON/QSAM/THSAM,QTHSAM,DTHSAM,STHSAM
      COMMON/HSA/HYDSAM
      COMMON/NBIEF/NBB
      COMMON/PHYS/LM,LL
      COMMON/CONDLI/CONDAM,CONDAV,REGIME
      COMMON/SOLID/QSR,DCHAR,DDCHAR,VCHUT,CAPSOL,DMOB,SMOB
      COMMON/NCONFL/NCONF,NCONF2,CONFLU

      IF(HYDSAM(IB))THEN
      IF((NTHSAM(IB).EQ.NTHSAM(IB-1)+1)
     :.OR.(T.LE.THSAM(NTHSAM(IB-1)+1)))THEN
        QS=QTHSAM(NTHSAM(IB-1)+1)
        D=DTHSAM(NTHSAM(IB-1)+1)
        S=STHSAM(NTHSAM(IB-1)+1)
      ELSEIF(T.GE.THSAM(NTHSAM(IB)))THEN
        QS=QTHSAM(NTHSAM(IB))
        D=DTHSAM(NTHSAM(IB))
        S=STHSAM(NTHSAM(IB))
      ELSE
        DO I=NTHSAM(IB-1)+2,NTHSAM(IB)
          IF(T.LE.THSAM(I)) GOTO 100
        ENDDO
 100    CONTINUE
        QS=QTHSAM(I-1)+(T-THSAM(I-1))
     &    *(QTHSAM(I)-QTHSAM(I-1))/(THSAM(I)-THSAM(I-1))
        D=DTHSAM(I-1)+(T-THSAM(I-1))
     &    *(DTHSAM(I)-DTHSAM(I-1))/(THSAM(I)-THSAM(I-1))
        S=STHSAM(I-1)+(T-THSAM(I-1))
     &    *(STHSAM(I)-STHSAM(I-1))/(THSAM(I)-THSAM(I-1))
      ENDIF

C Si le fichier d'entr�e �tait en concentrations, on transforme QS en d�bit solide
C Attention, on utilise le d�bit � Tn et non � Tn+1/2 comme le voudrait la logique, car QND non calcul�
      IF(UNISOL.EQ.2)THEN
        QS=QS*QN(LM(IB-1)+1)
      ENDIF
         QSR(LM(ib-1)+1)=QS
         DMOB(LM(ib-1)+1)=D
         SMOB(LM(ib-1)+1)=S
C cas ou hydsam faux (confluence)
C le cas de inversion vitesse pas traite
C on melange les deux biefs amont
      ELSEIf(condam(ib).eq.4)THEN
              TROUVE=.FALSE.
        DO I=1,NCONF2
                IF(.NOT.TROUVE)THEN
C IB numero du bief aval confluence
C suppose que ib est superieur aux numeros biefs amont
C sinon les valeurs sont les valeurs anterieures??
          IF(IB.EQ.CONFLU(I,1))THEN
               qs=QSR(LM(conflu(i,2)))
               d=dmob(LM(conflu(i,2)))
               s=smob(LM(conflu(i,2)))
                     call Mixage(Qs,d,s,qsr(LM(conflu(i,3)))
     :,dmob(LM(conflu(i,3))),smob(LM(conflu(i,3))))
               QSR(LM(ib-1)+1)=QS
               DMOB(LM(ib-1)+1)=D
               SMOB(LM(ib-1)+1)=S
                     TROUVE=.TRUE.
C fin du fi sur ib
                     ENDIF
C fin if sur trouve
                ENDIF
C fin boucle sur I
                ENDDO
        DO I=NCONF2+1,NCONF
                IF(.NOT.TROUVE)THEN
C IB0 numero du bief amont deflunece
       IB0=CONFLU(I,1)
        DO  J=2,3
         IF(.NOT.TROUVE)THEN
C IB numero du bief aval confluence
C cela ne donne les bonnes valeurs que si ib0<ib
C sinon valeur anterieure??
          IF(IB.EQ.CONFLU(I,J))THEN
             TROUVE=.TRUE.
             IF(QN(lm(ib-1)+1).gt.0.)THEN
               qsr(lm(ib-1)+1)=qsr(lm(ib0))/qn(lm(ib0))*qn(lm(ib-1)+1)
               dmob(lm(ib-1)+1)=dmob(lm(ib0))
               smob(lm(ib-1)+1)=smob(lm(ib0))
             ELSE
C cas debit nul ou inversion vitesse
                 QSR(LM(ib-1)+1)=0.
                 DMOB(LM(ib-1)+1)=1.
                 SMOB(LM(ib-1)+1)=1.
C fin du if sur QN
             ENDIF
C fin du if sur IB
           ENDIF
C fin du if sur not trouve
         ENDIF
C fin boucle sur j
        ENDDO
C fin du if sur not trouve
        ENDIF
C boucle sur i
        ENDDO
C cas ou hydsam faux (condam nul)
C cas du couplage (condam =6) traite ailleurs : qsr calcule dans module couplage
      ELSEIF(CONDAM(ib).EQ.0)THEN
           QSR(LM(ib-1)+1)=0.
           DMOB(LM(ib-1)+1)=1.
           SMOB(LM(ib-1)+1)=1.
      ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE FINSAUV
C-----------------------------------------------------------------------
C Fermeture des fichiers de sortie
C-----------------------------------------------------------------------
      CHARACTER TF(10)*1
      LOGICAL TRASED,CDCHAR,CGEOM

      COMMON/TSTFIL/TF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM

C 'hydlim.etude' et 'hydlims.etude'
      IF(TF(2).EQ.'O')THEN
        CLOSE(54)
           IF(TRaSED)THEN
           CLOSE(50)
           ENDIF
      ENDIF

C 'profil.etude' et 'profils.etude'
      IF(TF(4).EQ.'O')THEN
        CLOSE(42)
           IF(TRaSED)THEN
        CLOSE(41)
        ENDIF
      ENDIF

C 'lindo.etude'
      IF(TF(5).EQ.'O')THEN
        CLOSE(55)
      ENDIF

C 'largeur.etude'
      IF(TF(7).EQ.'O')THEN
        CLOSE(43)
      ENDIF

      RETURN
      END


C---------------------------------------------------------
      SUBROUTINE IMPDONB(IOUV,IBOUV)
C impression des donnees
      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax,NAMAX
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000,NAMAX=900000)

      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax),IOUV
     :     ,NBOUV
c        :,IAMONT

      INTEGER IT(NOB1DMAX)
      INTEGER IMP(NOB1DMAX)

      DOUBLE PRECISION KA(NOB1DMAX),K1
C        :,CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),C1(NOB1DMAX),C2(NOB1DMAX)
     :,DT2(NOB1DMAX)
     :,DBMAX(NOB1DMAX),TRECT(NOB1DMAX),TRUP(NOB1DMAX)
c      DOUBLE PRECISION ZFA(NAMAX),ZFM,LARGMAIL(NOB1DMAX)
c     :,TRECT(NOB1DMAX),TINI(NOB1DMAX)
      LOGICAL ELAP(NOB1DMAX)

      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
c      COMMON/ZARE/ZFA
c      COMMON/ZBAS/ZFM
C      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDTINIB/TRUP
      COMMON/DDELAPPR/ELAP

      IMP(IBOUV)=60+IBOUV
C on suppose que l'amont etait bien l'amont de l'ouvrage
c      IAMONT=IA1(IOUV)
c      Z0(IBOUV)=ZFA(IAMONT)

C  IMPRESSION ET TEST DES DONNEES
      WRITE(IMP(IBOUV),'(A,I2)')' IT=',IT(IBOUV)
      IF (IT(IBOUV).LT.0) WRITE(IMP(IBOUV),'(A)')
     :' RUPTURE PAR SUBMERSION'
      IF (IT(IBOUV).GE.0) WRITE(IMP(IBOUV),'(A)')
     :' RUPTURE PAR RENARD'
      IF(ELAP(IBOUV))THEN
        WRITE(IMP(IBOUV),'(A)')'AVEC CONTRAINTE REDUITE'
      ELSE
        WRITE(IMP(IBOUV),'(A)')'PAR METHODE SIMPLE'
      ENDIF
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),'(A,F10.2)')'  COTE EN CRETE =', ZC(IBOUV)
C      WRITE(IMP(IBOUV),'(A,F10.2)')'  COTE EN CRETE =', ZC(IBOUV)+ZFM
      WRITE(IMP(IBOUV),'(A,F10.2)')'  LARGEUR EN CRETE =',ALC(IBOUV)
      WRITE(IMP(IBOUV),'(A,F10.2)')'  COTE EN PIED =',ZP(IBOUV)
C      WRITE(IMP(IBOUV),'(A,F10.2)')'  COTE EN PIED =',ZP(IBOUV)+ZFM
      WRITE(IMP(IBOUV),'(A,F10.2)')'  LARGEUR EN PIED =',ALP(IBOUV)
      K1=21./D50(IBOUV)**(1./6.)
      WRITE(IMP(IBOUV),'(A,F10.8)')
     :'  DIAMETRE DES GRAINS (D50) =',D50(IBOUV)
      WRITE(IMP(IBOUV),'(A,F4.2)')'  POROSITE =',PHI(IBOUV)
      WRITE(IMP(IBOUV),'(A,F5.0)')'  MASSE VOLUMIQUE =',RHO(IBOUV)
      WRITE(IMP(IBOUV),'(A,F5.1)')'  STRICKLER DONNE =',KA(IBOUV)
      WRITE(IMP(IBOUV),'(A,F5.1)')'  STRICKLER CALCULE =',K1
      WRITE(IMP(IBOUV),*)'LARGEUR MAXIMALE DE BRECHE = '
     :, DBMAX(IBOUV)
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),*)'TEMPS DE DEBUT DE RUPTURE = '
     :, TRUP(IBOUV), ' s'
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),'(A,F8.2)')
     :'  COTE DU TERRAIN NATUREL =',Z0(IBOUV)
C     :'  COTE DU TERRAIN NATUREL =',Z0(IBOUV)+ZFM
      WRITE(IMP(IBOUV),'(A,F8.2)')
     :'  COTE DE LA BRECHE =',ZB0(IBOUV)
C     :'  COTE DE LA BRECHE =',ZB0(IBOUV)+ZFM
      WRITE(IMP(IBOUV),*)
      IF (IT(IBOUV).LT.0) THEN
        WRITE(IMP(IBOUV),'(A,F11.5)')
     :'  DIMENSION DE LA BRECHE =',DB0(IBOUV)
      ELSEIF (IT(IBOUV).GE.0)THEN
        WRITE(IMP(IBOUV),'(A,F11.5)')'  RAYON DU RENARD =',DB0(IBOUV)
      ENDIF
      IF ((ZC(IBOUV).LE.ZP(IBOUV)).OR.(ALP(IBOUV).LE.ALC(IBOUV)))
     : WRITE(IMP(IBOUV),'(A)')
     :' ANOMALIE SUR LA DIGUE'
      IF ((ZB0(IBOUV).GE.ZC(IBOUV)).OR.(ZB0(IBOUV).LT.ZP(IBOUV)))
     :WRITE(IMP(IBOUV),'(A)')
     :' ANOMALIE SUR LA COTE INITIALE DE LA BRECHE'
      Write(IMP(IBOUV),*)
      Write(IMP(IBOUV),*)
      END
C---------------------------------------------------------
      SUBROUTINE OUVERB(IBOUV)

      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
      CHARACTER*40 XLIS,ETUDE*20,XBOUV*3
      INTEGER IMP(NOB1DMAX)

      COMMON/NOMETU/ETUDE

C   OUVERTURE DU FICHIER de resultat en rupture progressive
      IMP(IBOUV)=60+IBOUV
      IF(IBOUV.EQ.1)THEN
        XLIS='ruptur.'//ETUDE
C        XLIS=ETUDE//'.res'
      ELSE
        IF(IBOUV.LT.10)THEN
          WRITE(XBOUV,'(I1,A2)')IBOUV,'  '
        XLIS='ruptur'//XBOUV(1:1)//'.'//ETUDE
        ELSEIF(IBOUV.LT.100)THEN
          WRITE(XBOUV,'(I2,A1)')IBOUV,' '
        XLIS='ruptur'//XBOUV(1:2)//'.'//ETUDE
        ELSEIF(IBOUV.LT.1000)THEN
          WRITE(XBOUV,'(I3)')IBOUV
        XLIS='ruptur'//XBOUV//'.'//ETUDE
        ENDIF
C        XLIS=ETUDE//'.res'//XBOUV
      ENDIF
      OPEN(IMP(IBOUV),FILE=XLIS,STATUS='UNKNOWN')
      END

C-----------------------------------------------------------------------
      SUBROUTINE LECTESB(IOUV,IBOUV)
C-----------------------------------------------------------------------
      IMPLICIT NONE
            INTEGER NOB1DMAX
            PARAMETER (NOB1DMAX=10)
            INTEGER IT(NOB1DMAX),NT(NOB1DMAX)
      DOUBLE PRECISION NU(NOB1DMAX)
      INTEGER LEC,IBOUV
      INTEGER nou1Dmax,noe1Dmax,ntr1Dmax,LMAX
      PARAMETER(nou1Dmax=50,noe1Dmax=10,ntr1Dmax=9000,LMAX=3000)
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax),IOUV,NBOUV
     :,IAMONT
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION KA(NOB1DMAX)
     :,GRAV,CHEZY,EPS,EPSY,EPSM
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX)
     :,Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX),RHO(NOB1DMAX)
     :,PHI(NOB1DMAX),DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),C1(NOB1DMAX),C2(NOB1DMAX)
     :,DT2(NOB1DMAX)
     :,DBMAX(NOB1DMAX),TRUP(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX)
      DOUBLE PRECISION QL(ntr1Dmax,NOB1DMAX),QS(ntr1Dmax,NOB1DMAX)
     :,ZBR(ntr1Dmax,NOB1DMAX),DBR(ntr1Dmax,NOB1DMAX)
     :,Z(ntr1Dmax,NOB1DMAX),ZAV(ntr1Dmax,NOB1DMAX),TRECT(NOB1DMAX)
      CHARACTER*20 ETUDE,LIGNE*30

      COMMON/NOMETU/ETUDE
      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDRESUL/QL,QS,DBR,Z,ZAV,ZBR
      COMMON/DDTINIB/TRUP
C      COMMON/DDELAPPR/ELAP
      COMMON/DDBRECHE/ZB,DB,IT
C      COMMON/ZBAS/ZFM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF

C LEC=IDON de LOUVR
      LEC=9
C  LECTURE DU FICHIER DES CARACTERISTIQUES DE LA DIGUE
      READ(LEC,'(4F10.2)',ERR=5) D50(IBOUV),KA(IBOUV)
     : ,RHO(IBOUV),PHI(IBOUV)
            IF(D50(IBOUV).LT.EPS)THEN
              write(*,*)'d50 = ',D50(IBOUV), 'ouvrage ',IBOUV
              STOP
            ENDIF
            IF(rho(IBOUV).LT.EPS)THEN
              write(*,*)'masse vol. = ',RHO(IBOUV), 'ouvrage ',IBOUV
               stop
            ENDIF
      READ(LEC,'(A)',ERR=5) LIGNE
      IF(LIGNE(21:30).EQ.'          ')THEN
        READ(LIGNE(1:20),'(2F10.2)',ERR=5) ZB0(IBOUV),DB0(IBOUV)
        DBMAX(IBOUV)=9999999.9
      ELSE
        READ(LIGNE,'(3F10.2)',ERR=5) ZB0(IBOUV),DB0(IBOUV),DBMAX(IBOUV)
      ENDIF
      ZB(IBOUV)=ZB0(IBOUV)
C D50 ET DB0 EN MM PASSES en M
      D50(IBOUV)=D50(IBOUV)*0.001
      DB0(IBOUV)=DB0(IBOUV)*0.001
      DBMAX(IBOUV)=DBMAX(IBOUV)
C  CARACTERISTIQUES DU CALCUL
      READ(LEC,103) DT2(IBOUV),ETA(IBOUV),IT(IBOUV),NT(IBOUV)
 103  FORMAT(2F6.2,I2,I4)
C  DT2 EST LE PAS DE STOCKAGE DES VARIABLES D''EROSION DE LA DIGUE
C  NT EST LE NB DE PAS DE TEMPS POUR LES DONNEES D''EROSION ENREGISTREES
      IF(NT(IBOUV).GT.ntr1Dmax)THEN
         WRITE (*,*) 'Le nombre de pas de temps de stockage demande est
     : trop grand'
         STOP
       ENDIF

C on suppose que l'amont etait bien l'amont de l'ouvrage
      IAMONT=IA1(IOUV)
      Z0(IBOUV)=XCTDF(IAMONT)
C      Z0=CTDF(IAMONT)+YN1D(IAMONT)

C    LES CONSTANTES
      C2(IBOUV)=0.001*RHO(IBOUV)-1.
      C1(IBOUV)=8.*SQRT(GRAV)/(C2(IBOUV)*KA(IBOUV)**3)
      C2(IBOUV)=0.047*C2(IBOUV)*D50(IBOUV)*KA(IBOUV)**2
      ALM(IBOUV)=ALC(IBOUV)+(ALP(IBOUV)-ALC(IBOUV))
     : *(ZB0(IBOUV)-ZC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV))
C----------------------------------------------------------------
C            MODIFICATION : PROBLEME DE COMMON; INITIALISATION DB
C----------------------------------------------------------------
      IF (IT(IBOUV).EQ.0) THEN
C en circulaire on suppose eau au dessus du renard
        RHM(IBOUV)=0.5*DB0(IBOUV)
C db0 contenait le rayon mais DB contient le diametre
        DB(IBOUV)=2.*DB0(IBOUV)
        NU(IBOUV)=2*GRAV*ALM(IBOUV)/(KA(IBOUV)**2*RHM(IBOUV)**1.3333333)
      ELSE
        IF(Z0(IBOUV).GT.ZB0(IBOUV))THEN
          RHM(IBOUV)=DB0(IBOUV)*(Z0(IBOUV)-ZB0(IBOUV))
     :/(DB0(IBOUV)+2.*(Z0(IBOUV)-ZB0(IBOUV)))
      NU(IBOUV)=2.*GRAV*ALM(IBOUV)/(KA(IBOUV)**2*RHM(IBOUV)**1.3333333)
        ELSE
          NU(IBOUV)=0.
          RHM(IBOUV)=0.
        ENDIF
        DB(IBOUV)=DB0(IBOUV)
      ENDIF

      RETURN
    5 WRITE (*,*)'ERREUR DANS LA LECTURE DU FICHIER ouvrag.',ETUDE
      STOP
      END

C----------------------------------------------------------------------------------------------------------
      SUBROUTINE CALCULB(Z,Q,QS,ZD,IBOUV)

      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
      DOUBLE PRECISION GRAV,CHEZY,EPS,EPSY,EPSM

      INTEGER IT(NOB1DMAX),NT(NOB1DMAX)
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),KA(NOB1DMAX),
     :C1(NOB1DMAX),C2(NOB1DMAX),DT2(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX),NU(NOB1DMAX)
C      DOUBLE PRECISION Q2
      DOUBLE PRECISION Z,QS,TN,DTN,TNP1

      DOUBLE PRECISION S2,V0,BETA,S3,RTAU
C beta vaut PI/4
      PARAMETER (BETA=0.7854)
      DOUBLE PRECISION ZD,Q
     :,DBMAX(NOB1DMAX),TRECT(NOB1DMAX),TRUP(NOB1DMAX)
C     :,TM,TR,tinit1D,tmax1D
      LOGICAL ELAP(NOB1DMAX)

      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDELAPPR/ELAP
      COMMON/DDTINIB/TRUP
      COMMON/TREEL/TN,DTN,TNP1
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
c      COMMON/TENV/TM,TR,tinit1D,tmax1D

C surface erosion berges utilisee uniquement en premiere phase de surverse (IT negatif)
      S3=0.
C rapport des tau-tauc **1.5 entre fond et berges = rapport d'epaisseur erosion
      RTAU=0.
C  CALCUL DE L''ELARGISSEMENT DE LA BRECHE
      CALL DEBITB(Z,Q,QS,ZD,RTAU,IBOUV)
      CALL SURFB(S2,S3,IBOUV)
C calcul elargissement et approfondissement
      IF(IT(IBOUV).LT.0)THEN
C si elargissement et approfondissement
        IF(ELAP(IBOUV))THEN
          V0=QS/(S2+RTAU*S3)
          DB(IBOUV)=DB(IBOUV)+2.*RTAU*V0*DTN
        ELSE
          V0=QS/S2
C          DB(IBOUV)=DB(IBOUV)
        ENDIF
        ZB(IBOUV)=ZB(IBOUV)-V0*DTN
C calcul elargissement renard ou breche rectangulaire
      ELSE
C      IF(IT.GE.0)
        V0=QS/S2
        DB(IBOUV)=DB(IBOUV)+2.*V0*DTN
      ENDIF
C on limite la largeur de la breche soit rectangulaire soit diametre renard
       IF(DB(IBOUV).GT.DBMAX(IBOUV))THEN
         DB(IBOUV)=DBMAX(IBOUV)
C approximation grossiere
         QS=0.
       ENDIF
C  CALCUL DE NU A LA FIN DU PAS DE TEMPS
      IF(RHM(IBOUV).GT.0.)THEN
        NU(IBOUV)=2.*GRAV*ALM(IBOUV)/(KA(IBOUV)**2
     :*RHM(IBOUV)**1.3333333)
      ELSE
        NU(IBOUV)=0.
      ENDIF
      RETURN
      END

C----------------------------------------------------------
      SUBROUTINE DEBITB(Z,Q,Q2,ZD,RTAU,IBOUV)
C retourne Q, Q2=QS,RTAU et ZD si on obtient un ZAVI<ZD
      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      INTEGER IT(NOB1DMAX),NT(NOB1DMAX),I
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX),DBTOTAL
      DOUBLE PRECISION ETA(NOB1DMAX),KA(NOB1DMAX),
     :C1(NOB1DMAX),C2(NOB1DMAX),DT2(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX),NU(NOB1DMAX)
      DOUBLE PRECISION BETA
      PARAMETER (BETA=3.1416*0.25)

      DOUBLE PRECISION Z,H,Q,Q2,Y,Y0,Y1,Y2,YE,R,S,SE,SR,TETA,AL
      DOUBLE PRECISION DF,F,ZAVI,F1,C,ZD,RTAU,QS1,QS2
      LOGICAL ELAP(NOB1DMAX)

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDELAPPR/ELAP

C   CE PROGRAMME CALCULE LES DEBITS LIQUIDE ET SOLIDE
C   AINSI QUE LES VALEURS MOYENNES CARACTERISANT
C   L ECOULEMENT
C    VARIABLES
C   IT=0 CIRCULAIRE              DB=DIAMETRE
C   IT=1 OU -1 RECTANGULAIRE  DB=LARGEUR
C   ZB=COTE DE LA BRECHE
C   Z=COTE DU PLAN D EAU
C   Q=DEBIT LIQUIDE
C   Q2=DEBIT SOLIDE
C   YM,SM,PM,RHM,ALM,NU;HAUTEUR,SECTION,PERIMETRE MOUILLE
C   RAYON HYDRAULIQUE,LONGUEUR D ECOULEMENT,COEFFICIENT DE
C   PERTE DE CHARGE EN VALEURS MOYENNES

C    on ne fait pas la correction pour ne pas interferer avec le chnagement dans IT
C      IF(ZB(IBOUV).LE.ZP(IBOUV))ZB(IBOUV)=ZP(IBOUV)
      H=Z-ZB(IBOUV)
      IF(H.LE.0.)THEN
        Q=0.
        Q2=0.
        Z=ZB(IBOUV)
        YM(IBOUV)=0.
        RETURN
      ELSE
        IF(IT(IBOUV).NE.0)THEN
C  CALCUL EN RECTANGULAIRE
          Y=0.66666666667*H
C   CALCUL DE Y PAR UNE METHODE DE NEWTON
          DO 10 I=1,100
            YM(IBOUV)=0.5*(Y+H)
            C=1+(ETA(IBOUV)/(H*H)+NU(IBOUV)/
     :(YM(IBOUV)**2))*Y**2
            F=Y+0.5*Y*C-H
            DF=1.5*C-0.5*NU(IBOUV)*Y**3/
     :(YM(IBOUV)**3)
            Y1=Y-F/DF
            IF(Y1.LT.0.)Y1=0.
            IF(ABS(Y-Y1).LT.0.001)GO TO 11
            Y=Y1
   10     CONTINUE
   11     Y=Y1
C   CALCUL DES CARACTERISTIQUES MOYENNES
          YM(IBOUV)=0.5*(Y+H)
          F1=1+Y*Y*(ETA(IBOUV)/(H*H)+NU(IBOUV)/
     :(YM(IBOUV)*YM(IBOUV)))
          SM(IBOUV)=DB(IBOUV)*YM(IBOUV)
          PM(IBOUV)=DB(IBOUV)+2.*YM(IBOUV)
          RHM(IBOUV)=SM(IBOUV)/PM(IBOUV)
          IF(ZB(IBOUV)+YM(IBOUV).LT.ZC(IBOUV))THEn
            ALM(IBOUV)=ALC(IBOUV)+((ALP(IBOUV)-ALC(IBOUV))
     :/(ZP(IBOUV)-ZC(IBOUV)))*(ZB(IBOUV)+YM(IBOUV)-ZC(IBOUV))
          ELSE
            ALM(IBOUV)=ALC(IBOUV)
          ENDIF
          S=DB(IBOUV)*Y
        ELSE
C  CALCUL EN CIRCULAIRE
          R=0.5*DB(IBOUV)
C  CALCUL DE LA SECTION D ENTREE
          YE=2.*R
          SE=3.1416*R*R
          IF(H.GE.2.*R)GO TO 21
          YE=H
          TETA=ACOS(1.-H/R)
          SE=R*R*(TETA-SIN(TETA)*COS(TETA))
   21     Y1=YE
          Y0=0.
C   CALCUL DE DICHOTOMIE POUR TROUVER Y AVAL
          DO 23 I=1,20
            Y=0.5*(Y1+Y0)
            TETA=ACOS(1.-Y/R)
            S=R*R*(TETA-SIN(TETA)*COS(TETA))
            AL=2.*R*SIN(TETA)
            SM(IBOUV)=0.5*(SE+S)
            F1=1+S*S*(ETA(IBOUV)/(SE*SE)+NU(IBOUV)/
     :(SM(IBOUV)*SM(IBOUV)))
            F=Y-H+0.5*F1*S/AL
            IF(F.EQ.0.)GO TO 24
            IF(F.LT.0.)GO TO 22
            Y1=Y
            GO TO 23
   22       Y0=Y
   23     CONTINUE
   24     SM(IBOUV)=0.5*(S+SE)
C   CALCUL DES CARACTERISTIQUES MOYENNES
          SR=SM(IBOUV)/(R*R)
          IF(SR.GE.3.1416)GO TO 26
          Y1=1.
          DO 25 I=1,100
            TETA=ACOS(1.-Y1)
            F=TETA-SIN(TETA)*COS(TETA)-SR
            Y2=Y1-F/(2.*SIN(TETA))
            IF(Y2.GE.2.)GO TO 26
            IF(Y2.LE.0.)Y2=0.01
            IF(ABS(Y1-Y2).LT.0.001)GO TO 27
            Y1=Y2
   25     CONTINUE
   26     YM(IBOUV)=2.*R
          GO TO 28
   27     YM(IBOUV)=Y1*R
   28     TETA=ACOS(1.-YM(IBOUV)/R)
          PM(IBOUV)=2.*R*TETA
          SM(IBOUV)=R*R*(TETA-SIN(TETA)*COS(TETA))
          RHM(IBOUV)=SM(IBOUV)/PM(IBOUV)
          IF(ZB(IBOUV)+YM(IBOUV).LT.ZC(IBOUV))THEn
            ALM(IBOUV)=ALC(IBOUV)+((ALP(IBOUV)-ALC(IBOUV))
     :/(ZP(IBOUV)-ZC(IBOUV)))*(ZB(IBOUV)+YM(IBOUV)-ZC(IBOUV))
          ELSE
            ALM(IBOUV)=ALC(IBOUV)
          ENDIF
C fin du if sur rectangulaire/circulaire
        ENDIF
        ZAVI=Y+ZB(IBOUV)

C   ON REPREND LE CALCUL si z a droite est plus grand que z aval (critique)

        IF(ZAVI.LT.ZD)THEN
          IF(IT(IBOUV).NE.0)THEN
C  CALCUL EN RECTANGULAIRE
            Y=ZD-ZB(IBOUV)
            IF(Y.LE.0.)THEN
              ZD=ZB(IBOUV)
              Q=0.
              Q2=0.
              RETURN
            ENDIF
C   CALCUL DES CARACTERISTIQUES MOYENNES
            YM(IBOUV)=0.5*(Y+H)
            SM(IBOUV)=DB(IBOUV)*YM(IBOUV)
            PM(IBOUV)=DB(IBOUV)+2.*YM(IBOUV)
            RHM(IBOUV)=SM(IBOUV)/PM(IBOUV)
          IF(ZB(IBOUV)+YM(IBOUV).LT.ZC(IBOUV))THEn
            ALM(IBOUV)=ALC(IBOUV)+((ALP(IBOUV)-ALC(IBOUV))
     :/(ZP(IBOUV)-ZC(IBOUV)))*(ZB(IBOUV)+YM(IBOUV)-ZC(IBOUV))
          ELSE
            ALM(IBOUV)=ALC(IBOUV)
          ENDIF
            S=DB(IBOUV)*Y
          ELSE
C  CALCUL EN CIRCULAIRE
            R=0.5*DB(IBOUV)
C  CALCUL DE LA SECTION D ENTREE
            YE=2.*R
            IF(H.LT.2.*R)YE=H
            Y=ZD-ZB(IBOUV)
            IF(Y.LT.0.)THEN
              ZD=ZB(IBOUV)
              Q=0.
              Q2=0.
              RETURN
            ENDIF
            IF(Y.GE.2.*R)Y=2.*R
C   CALCUL DES CARACTERISTIQUES MOYENNES
            YM(IBOUV)=0.5*(Y+YE)
            IF(YM(IBOUV).LT.2.*R)THEN
              TETA=ACOS(1.-YM(IBOUV)/R)
              PM(IBOUV)=2.*R*TETA
              SM(IBOUV)=R*R*(TETA-SIN(TETA)*COS(TETA))
            ELSE
              YM(IBOUV)=2.*R
              PM(IBOUV)=6.2832*R
              SM(IBOUV)=3.1416*R*R
            ENDIF
            RHM(IBOUV)=SM(IBOUV)/PM(IBOUV)
          IF(ZB(IBOUV)+YM(IBOUV).LT.ZC(IBOUV))THEn
            ALM(IBOUV)=ALC(IBOUV)+((ALP(IBOUV)-ALC(IBOUV))
     :/(ZP(IBOUV)-ZC(IBOUV)))*(ZB(IBOUV)+YM(IBOUV)-ZC(IBOUV))
          ELSE
            ALM(IBOUV)=ALC(IBOUV)
          ENDIF
C FIN DU IF SUR RECTANGULAIRE/CIRCULAIRE
          ENDIF
C ELSE DU IF sur zavi>zd
C on enregistre dans ZD la cote aval breche (critique)
        ELSE
          ZD=ZAVI
C FIN DU IF sur zavi>zd
        ENDIF

C  CALCUL DES DEBITS
        IF(Y.GT.H)Y=H
        Q=S*SQRT(2.*GRAV*(H-Y)/F1)
        QS1=Q/(SM(IBOUV)*RHM(IBOUV)**(1./6.))
C        TAUI(IBOUV)=QS1*QS1/C2*0.047*(RHO(IBOUV)-1000.)*G*D50(IBOUV)
        Q2=QS1*QS1-C2(IBOUV)
        IF(Q2.LE.0.)THEN
          Q2=0.
        ELSEIF(IT(IBOUV).EQ.0)THEN
C circulaire
          Q2=C1(IBOUV)*PM(IBOUV)*Q2**1.5
        ELSEIF(.NOT.ELAP(IBOUV))THEN
C mode Approfondissement seul et contrainte non reduite
          Q2=C1(IBOUV)*PM(IBOUV)*Q2**1.5
        ELSE
C QS1 debit sur le fond
C QS2 debit sur les cotes
C on r�duit la contrainte laterale a 0.6 contrainte moyenne pour largeur infinie
C article de Knight et formulation simplifiee
C          DBTOTAL=DB(IBOUV)+DBPREC(IBOUV)
          DBTOTAL=DB(IBOUV)
          IF(DBTOTAL.LT.2.*YM(IBOUV))THEN
            QS2=Q2
          ELSEIF(0.083*DBTOTAL.LT.YM(IBOUV))THEN
            QS2=(1.06-0.03*DB(IBOUV)/YM(IBOUV))*QS1*QS1-C2(IBOUV)
          ELSE
            QS2=(0.6+YM(IBOUV)/(0.83*DB(IBOUV)))*QS1*QS1-C2(IBOUV)
          ENDIF
          IF (QS2.LE.0.) QS2=0.
          IF(IT(IBOUV).LT.0)THEN
            QS1=QS1*QS1-C2(IBOUV)
            IF (QS1.LE.0.)THEN
              QS1=0.
              RTAU=0.
            ELSE
              RTAU=(QS2/QS1)**1.5
            ENDIF
C si on a atteint le fond, le debit unitaire est pris uniquement sur les cotes
          ELSE
            QS1=0.
          ENDIF
          Q2=C1(IBOUV)*(DB(IBOUV)*QS1**1.5+2.*YM(IBOUV)*QS2**1.5)
C fin du if sur type de calcul du debit
        ENDIF
        RETURN
C fin du if sur h positif
      ENDIF
      END

C
C---------------------------------------------------------------
      SUBROUTINE SURFB(S2,S3,IBOUV)

      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
      INTEGER IT(NOB1DMAX)
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION S2,S3
      LOGICAL ELAP(NOB1DMAX)

      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDELAPPR/ELAP

C  CE SOUS PROGRAMME CALCULE LA SUPERFICIE ERODABLE (S2)
C  POUR ZB ET DB DONNES
      IF(IT(IBOUV).LT.0)THEN
C  CALCUL EN RECTANGULAIRE (SUBMERSION, EROSION VERS LE BAS et eventuellement sur le cote)
        S2=(ALC(IBOUV)-(ZC(IBOUV)-ZB(IBOUV))
     :*(ALP(IBOUV)-ALC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))*DB(IBOUV)
        IF(ELAP(IBOUV))THEN
C erosion berges
          S3=(ALC(IBOUV)+0.5*(ZB(IBOUV)-ZC(IBOUV))
     :*(ALP(IBOUV)-ALC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))
     :*(ZC(IBOUV)-ZB(IBOUV))
C on met dans S3 la surface des deux cotes
          S3=2.*S3
        ELSE
          S3=0.
        ENDIF
      ELSEIF(IT(IBOUV).GT.0)THEN
C  CALCUL EN RECTANGULAIRE (RENARD OU SUBMERSION, ELARGISSEMENT BRECHE)
        S2=(ALC(IBOUV)+0.5*(ZB(IBOUV)-ZC(IBOUV))
     :*(ALP(IBOUV)-ALC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))
     :*(ZC(IBOUV)-ZB(IBOUV))
C on met dans S2 la surface des deux cotes (=surface erosion comme en renard)
          S2=2.*S2
        S3=0.
      ELSE
C  CALCUL EN CIRCULAIRE (-RENARD-)AVEC DB diametre
        S2=3.1416*DB(IBOUV)*(ALP(IBOUV)+(ZB(IBOUV)-ZP(IBOUV))
     :*(ALC(IBOUV)-ALP(IBOUV))/(ZC(IBOUV)-ZP(IBOUV))
     :+0.75*DB(IBOUV)*(ALC(IBOUV)-ALP(IBOUV))/(ZC(IBOUV)-ZP(IBOUV)))
C valeur 0.75 et pas 0.5 ??: valeur differente dans le bilan
        S3=0.
C fin du if sur forme breche
      ENDIF
      S2=S2*(1.-PHI(IBOUV))
      S3=S3*(1.-PHI(IBOUV))
      RETURN
      END

C********************************************************************
      SUBROUTINE IMPRIB(IBOUV)

      INTEGER NOB1DMAX,IBOUV
      PARAMETER (NOB1DMAX=10)
       INTEGER ntr1Dmax
       PARAMETER (ntr1Dmax=9000)

       INTEGER IMP(NOB1DMAX)

       INTEGER M1,M2,M3,I

       DOUBLE PRECISION QST,T

      INTEGER IT(NOB1DMAX),NT(NOB1DMAX)
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),KA(NOB1DMAX),
     :C1(NOB1DMAX),C2(NOB1DMAX),DT2(NOB1DMAX)
      DOUBLE PRECISION YM(NOB1DMAX),SM(NOB1DMAX),PM(NOB1DMAX)
     :,RHM(NOB1DMAX),ALM(NOB1DMAX),NU(NOB1DMAX)
      DOUBLE PRECISION QL(ntr1Dmax,NOB1DMAX),QS(ntr1Dmax,NOB1DMAX)
     :,DBR(ntr1Dmax,NOB1DMAX)
     :,Z(ntr1Dmax,NOB1DMAX),ZAV(ntr1Dmax,NOB1DMAX)
     :,ZBR(ntr1Dmax,NOB1DMAX)
      DOUBLE PRECISION DBMAX(NOB1DMAX),TRECT(NOB1DMAX)
       DOUBLE PRECISION LBEF,DBF,TRUP(NOB1DMAX)

       LOGICAL KAPPA(NOB1DMAX)

      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0
     :,D50,RHO,PHI,DB0,ZB0
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDMOYEN/YM,SM,PM,RHM,ALM,NU
      COMMON/DDRESUL/QL,QS,DBR,Z,ZAV,ZBR
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/DDLAG/KAPPA
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDTINIB/TRUP

      IMP(IBOUV)=60+IBOUV
      QST=0.
      LBEF=0.
      IF (TRECT(IBOUV).GT.TRUP(IBOUV))THEN
        WRITE(IMP(IBOUV),*)
     :'PASSAGE EN ELARGISSEMENT RECTANGULAIRE AU TEMPS ',TRECT(IBOUV)
      ENDIF
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),*)
      WRITE(IMP(IBOUV),'(A)')
     :'   HR   MN    S      COTE EAU  COTE BRECHE  LARG BRECHE
     :   DEBIT LIQ   DEBIT SOL'
      DO 5 I=1,NT(IBOUV)
      T=FLOAT(I-1)*DT2(IBOUV)+TRUP(IBOUV)
C      IF(T.GE.TINI(IBOUV).AND.T.LT.TM)THEN
        M1=INT(T/3600.)
        M2=INT((T-3600.*FLOAT(M1))/60.)
        M3=INT(T-3600.*FLOAT(M1)-60.*FLOAT(M2))

        WRITE(IMP(IBOUV),101)M1,M2,M3,Z(I,IBOUV),ZBR(I,IBOUV)
C        WRITE(IMP(IBOUV),101)M1,M2,M3,Z(I,IBOUV)+ZFM,ZBR(I,IBOUV)
     :,DBR(I,IBOUV),QL(I,IBOUV),QS(I,IBOUV)
  101   FORMAT(2X,I3,2X,I3,2X,I3,5X,5F10.4)
C On cherche la largeur breche apres effondrement renard pour calcul volume
        IF(T.LT.TRECT(IBOUV))LBEF=DBR(I,IBOUV)
C calcul du volume erode
        QST=QST+QS(I,IBOUV)
C      ENDIF
    5 CONTINUE
      QST=QST*DT2(IBOUV)
      WRITE(IMP(IBOUV),'(A,F10.0)')' VOLUME ERODE =', QST

C ---- CALCUL  (VOLUME INITIAL DIGUE - VOLUME FINAL DIGUE = VOLUME PARTI) -----
      IF(IT(IBOUV).NE.0)THEN
C PREMIER CAS : SI EFFONDREMENT DU RENARD
      IF(KAPPA(IBOUV))THEN
C dans ce cas, le volume calcule est faux car on ne tient pas compte du volume
C lors de l'effondrement
C      QST=DB*(ALC+.5*(ALP-ALC)*(ZB-ZC)/(ZP-ZC))*(ZC-ZB)*(1.-PHI)
C      QST=QST-3.1416*DB0*DB0*(ALC+(ALP-ALC)*(ZB0+DB0-ZC)/(ZP-ZC))*
C     :(1-PHI)
C nouvelle methode doit etre verifiee
C aclcul volume pour le renard jusqu'a effondrement
      DBF=(ZC(IBOUV)-ZP(IBOUV))/3.
      QST=3.1416*DBF*DBF*(ALC(IBOUV)+(ALP(IBOUV)-ALC(IBOUV))
     :*(ZB(IBOUV)+DBF-ZC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))*(1.-PHI(IBOUV))
      QST=QST-3.1416*DB0(IBOUV)*DB0(IBOUV)*(ALC(IBOUV)+
     ;(ALP(IBOUV)-ALC(IBOUV))*(ZB0(IBOUV)+DB0(IBOUV)-ZC(IBOUV))/
     :(ZP(IBOUV)-ZC(IBOUV)))*(1.-PHI(IBOUV))
C puis ajout du volume en rectangulaire   allant jusqu au fond
      QST=QST+(DB(IBOUV)-LBEF)*(ALC(IBOUV)+.5*(ALP(IBOUV)-ALC(IBOUV)))
     :*(ZC(IBOUV)-ZP(IBOUV))*(1.-PHI(IBOUV))
C SECOND CAS : SI SURVERSE
       ELSE
      QST=DB(IBOUV)*(ALC(IBOUV)+.5*(ALP(IBOUV)-ALC(IBOUV))
     :*(ZB(IBOUV)-ZC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))
     :*(ZC(IBOUV)-ZB(IBOUV))*(1.-PHI(IBOUV))
      QST=QST-DB0(IBOUV)*(ALC(IBOUV)+.5*(ALP(IBOUV)-ALC(IBOUV))
     :*(ZB0(IBOUV)-ZC(IBOUV))/(ZP(IBOUV)-ZC(IBOUV)))
     :*(ZC(IBOUV)-ZB0(IBOUV))*(1.-PHI(IBOUV))
       ENDIF
C TROISIEME CAS RENARD SANS EFFONDREMENT
      ELSE
      QST=0.7854*DB(IBOUV)*DB(IBOUV)*(ALC(IBOUV)+
     :(ALP(IBOUV)-ALC(IBOUV))*(ZB(IBOUV)+0.5*DB(IBOUV)-ZC(IBOUV))
     :/(ZP(IBOUV)-ZC(IBOUV)))*(1.-PHI(IBOUV))
      QST=QST-3.1416*DB0(IBOUV)*DB0(IBOUV)*(ALC(IBOUV)
     :+(ALP(IBOUV)-ALC(IBOUV))*(ZB0(IBOUV)+DB0(IBOUV)-ZC(IBOUV))/
     :(ZP(IBOUV)-ZC(IBOUV)))*(1.-PHI(IBOUV))
      ENDIF
      WRITE (IMP(IBOUV),'(A)') ' QU IL FAUT COMPARER AVEC'
      WRITE (IMP(IBOUV),'(A,F10.0)')' VOLUME PARTI =',QST
      CLOSE(IMP(IBOUV))
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE COEFFICIENTCKIKEDA(Z,CK)
C-----------------------------------------------------------------------
C CALCUL DU FACTEUR CK INFLUENCANT LA CONTRAINTE CRITIQUE (Said.KH): ICI LA FORMULE
C de IKDEA EST UTILISEE(Kamal MARS 2003)
C---------------------------------------------------------------------------------
C JDEFMAX:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU dans le lit actif, pour l'intermaille IS
C JRH:PENTE LIGNE DENREGIE*RAYON HYDRAULIQUE dans l'intermaille IS
C CK:FACTEUR DE IKDEA,INTERVIENT POUR LE CALCUL DE CONTRAINTE CRITIQUE SUR LES BERGES
C---------------------------------------------------------------------------------
C ALPHA: rapport de portance � la train�e, il est pris �gale � 0.85
C TETA:angle de pente de berge
C BMIU:tangente angle de frottement interne des s�diments
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: facteur CK
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10)
C      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION CK(LNCMAX),TETA
      DOUBLE PRECISION TETA1,TETA2
      DOUBLE PRECISION ALPHA,BMIU,PSTAB
      DOUBLE PRECISION ALPHA2,BMIU2,COEF
      DOUBLE PRECISION EPSTETA
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
     :,tanteta,costeta,sinteta
      INTEGER XNBCSP1(LNCMAX)
      INTEGER J5,J2,JDEFMAX,DEFORM(LNCMAX)
c      INTEGER XNCMOAG(0:LMAX),XNCMOAD(0:LMAX)
C      INTEGER NT
      LOGICAL CKZERO
      COMMON/BMIU/BMIU,PSTAB
C      COMMON/XNCMOA/XNCMOAG,XNCMOAD
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/DEFORM/DEFORM
C      COMMON/PSOUE/PSOUE
C      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
C      COMMON/NC/NC,XNC

       DATA EPSTETA/0.0001/
       DATA ALPHA/0.85/
       DATA ALPHA2/0.7225/
       SAVE EPSTETA
       SAVE ALPHA,ALPHA2

c      Write(*,*)'entree ck'
C      EPSTETA=0.0001
C      ALPHA=0.85
c      BMIU=0.5661
      BMIU2=BMIU*BMIU
C      ALPHA2=ALPHA*ALPHA
C variable pour mettre ck a zero en evitant calculs
      CKZERO=.FALSE.
      DO 50 J2=1,JDEFMAX
        J5=DEFORM(J2)
C condition toujours verifiee
C      IF(J5.LT.XNC(I).AND.J5.GT.XNC(I-1)+1))THEN
C je traite le cas ou les segements (J-1)-J et (J)-(J+1) sont pas verticales
        IF(Z(J5,1).GT.Z(J5+1,1).AND.Z(J5,1).LE.Z(J5-1,1))THEN
          IF(XYCOU(J5+1).NE.XYCOU(J5))THEN
            TETA=ATAN((Z(J5,1)-Z(J5+1,1))/(XYCOU(J5+1)-XYCOU(J5)))
C cas berges verticales
C          ELSEIF(XYCOU(J5-1).EQ.XYCOU(J5))THEN
          ELSE
            CKZERO=.TRUE.
          ENDIF
        ELSEIF(Z(J5,1).LE.Z(J5+1,1).AND.Z(J5,1).GT.Z(J5-1,1))THEN
          IF(XYCOU(J5).NE.XYCOU(J5-1))THEN
            TETA=ATAN((Z(J5,1)-Z(J5-1,1))/(XYCOU(J5)-XYCOU(J5-1)))
C cas berges verticales
C          ELSEIF(XYCOU(J5-1).EQ.XYCOU(J5))THEN
          ELSE
            CKZERO=.TRUE.
          ENDIF
        ELSEIF(Z(J5,1).GE.Z(J5+1,1).AND.Z(J5,1).GE.Z(J5-1,1))THEN
          IF(XYCOU(J5).NE.XYCOU(J5-1).AND.XYCOU(J5).NE.XYCOU(J5+1))THEN
           TETA1=ABS(ATAN((Z(J5,1)-Z(J5-1,1))/(XYCOU(J5)-XYCOU(J5-1))))
           TETA2=ABS(ATAN((Z(J5,1)-Z(J5+1,1))/(XYCOU(J5+1)-XYCOU(J5))))
           TETA=MAX(TETA1,TETA2)
C cas berges verticales
C       ELSEIF(XYCOU(J5).NE.XYCOU(J5-1).OR.XYCOU(J5).NE.XYCOU(J5+1))THEN
C l'angle maximal est PI/2, donc ck=0
          ELSE
            CKZERO=.TRUE.
          ENDIF
        ELSEIF(Z(J5,1).LT.Z(J5+1,1).AND.Z(J5,1).LT.Z(J5-1,1))THEN
          TETA=0.
        ELSEIF(Z(J5,1).EQ.Z(J5+1,1).AND.Z(J5,1).LT.Z(J5-1,1))THEN
          TETA=0.
        ELSEIF(Z(J5,1).EQ.Z(J5-1,1).AND.Z(J5,1).LT.Z(J5+1,1))THEN
          TETA=0.
        ELSE
          Write(*,*)'CK non defini'
C           Pause
           Stop
        ENDIF

C condition toujours verifiee

c      ELSEIF(J5.EQ.XNC(I))THEN !pour pouvoir calculer la contrainte critique le long de tout le p�rim�tre de la section
c        WRITE(*,*)'erreur dernier point de la section est sous l eau'
c        WRITE(*,*) 'abscisse ',xtmail(I)
cc        PAUSE
c        STOP
c      ENDIF


        IF(CKZERO)THEN
          CK(J5)=0.
          CKZERO=.FALSE.
        ELSEIF(ABS(TETA).LE.EPSTETA)THEN
          CK(J5)=1.
        ELSE
          TANTETA=TAN(TETA)
          IF(BMIU.LE.TANTETA)THEN
            CK(J5)=0.
          ELSE
            COSTETA=COS(TETA)
            SINTETA=TANTETA*COSTETA
            COEF=BMIU2*COSTETA**2+(ALPHA2*BMIU2-1.)
     &           *SINTETA**2
            IF(COEF.LE.0.)THEN
              CK(J5)=0.
            ELSE
c            CK(J5)=((-1)*ALPHA*BMIU2*COS(TETA)+COEF**.5)
c     :              /(BMIU-ALPHA*BMIU2)
              CK(J5)=(SQRT(COEF)-ALPHA*BMIU2*COSTETA)
     :              /(BMIU-ALPHA*BMIU2)
            ENDIF
          ENDIF
        ENDIF
 50   CONTINUE
      RETURN
      END

C
C-----------------------------------------------------------------------
      SUBROUTINE CONTRAINTEMPC(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCULE DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NCMAX,NPMAX,LMAX,CSMAX,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,AREA,PER
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS

      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

           IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
            JJ=J3-NOMBRE(II,J3)
C2270        JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA=0.5*(A1+A2+A3+A4)
        PER=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
           If(per.gt.EPS)then
          TOMPC(J1)=COEFFIC*ABS(AREA)/PER
              elseif(J0.gt.NEAUC(K))THEN
                   TOMPC(J1)=TOMPC(PSOUE(J0-1))
              else
             TOMPC(J1)=0.
              endif
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE CONTMPCKI(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCULE DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES avec avec proportionalite a diametre a la puissance 1/3
C pour prendre en compte rugosite variable
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NCMAX,NPMAX,LMAX,CSMAX,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,PER
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS
      DOUBLE PRECISION FRII(LNCMAX)
      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

C pour calculer la contrainte en fonction du diamettre de surface
      DOUBLE PRECISION TOTALAREA,TOTALCOEF,COEFDSURF(LNCMAX)
     :,AREA(LNCMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G
         COMMON/FRI/FRII

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

            IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
C2270        JJ=J3-NOMBRE(II,J3)
            JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
           TOTALAREA=0.
              TOTALCOEF=0.
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA(J1)=ABS(0.5*(A1+A2+A3+A4))
C on suppose que la contrainte est proportionelle a 1/KS**3/2
        COEFDSURF(J1)=AREA(J1)*FRII(J1)**(-1.5)
C fin boucle sur J0
      ENDDO
C calcul de la contribution totale pour la ramener a la contribution initiale
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        TOTALAREA=TOTALAREA+AREA(J1)
        TOTALCOEF=TOTALCOEF+COEFDSURF(J1)
C fin boucle sur J0
      ENDDO
      TOTALAREA=TOTALAREA/TOTALCOEF
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        PER=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
           If(per.gt.EPS)then
           TOMPC(J1)=COEFFIC*TOTALAREA*COEFDSURF(J1)/PER
              elseif(J0.gt.NEAUC(K))THEN
                   TOMPC(J1)=TOMPC(PSOUE(J0-1))
              else
             TOMPC(J1)=0.
              endif
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE CONTMPCD(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCULE DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES avec avec proportionalite a diametre a la puissance 1/3
C pour prendre en compte rugosite variable
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LNCMAX,NPMAX,LMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,PER
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS

      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

C pour calculer la contrainte en fonction du diamettre de surface
      DOUBLE PRECISION TOTALAREA,TOTALCOEF,COEFDSURF(LNCMAX)
     :,AREA(LNCMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

            IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
C2270        JJ=J3-NOMBRE(II,J3)
            JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
           TOTALAREA=0.
              TOTALCOEF=0.
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA(J1)=ABS(0.5*(A1+A2+A3+A4))
C on suppose que la contrainte est proprotionelle a 1/KS**2
C et que KS est proprotionnel a D90**-1/6
        COEFDSURF(J1)=AREA(J1)*(XDCSP1(J1,1)*XSCSP1(J1,1))**0.333
C fin boucle sur J0
      ENDDO
C calcul de la contribution totale pour la ramener a la contribution initiale
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        TOTALAREA=TOTALAREA+AREA(J1)
        TOTALCOEF=TOTALCOEF+COEFDSURF(J1)
C fin boucle sur J0
      ENDDO
      TOTALAREA=TOTALAREA/TOTALCOEF
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        PER=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
           If(per.gt.EPS)then
           TOMPC(J1)=COEFFIC*TOTALAREA*COEFDSURF(J1)/PER
              elseif(J0.gt.NEAUC(K))THEN
                   TOMPC(J1)=TOMPC(PSOUE(J0-1))
              else
             TOMPC(J1)=0.
              endif
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE CONTMPCMOY3(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCULE DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES moyenne sur 3 segments
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LNCMAX,NPMAX,LMAX,CSMAX,NCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,AREA(NCMAX),PER(NCMAX)
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS

      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

            IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
            JJ=J3-NOMBRE(II,J3)
C2270        JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA(J0)=0.5*(A1+A2+A3+A4)
        PER(J0)=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
c        TOMPC(J1)=COEFFIC*ABS(AREA)/PER
C fin boucle sur J0
      ENDDO
C nouvelle boucle pour que la contrainte soit lissee
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        IF(J0.EQ.NEAUC(K))THEN
          TOMPC(J1)=COEFFIC*
     :(ABS(AREA(J0))+ABS(AREA(J0))+ABS(AREA(J0+1)))
     : /(PER(J0)+PER(J0)+PER(J0+1))
        ELSEIF(J0.EQ.NEAUF(K))THEN
          TOMPC(J1)=COEFFIC*
     :(ABS(AREA(J0-1))+ABS(AREA(J0))+ABS(AREA(J0)))
     : /(PER(J0-1)+PER(J0)+PER(J0))
        ELSE
          TOMPC(J1)=COEFFIC*
     :(ABS(AREA(J0-1))+ABS(AREA(J0))+ABS(AREA(J0+1)))
     : /(PER(J0-1)+PER(J0)+PER(J0+1))
        ENDIF
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CONTMPCMOYH(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCUL DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES moyennee sur n segments de longueur h avec n egal au maximum a nsegmax mis a 9
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NCMAX,NPMAX,LMAX,CSMAX,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,AREA(NCMAX),PER(NCMAX)
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS
      INTEGER NSEGMAX
      DOUBLE PRECISION AREAJ1,HAUTEURJ1,PERJ1

      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

            IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
            JJ=J3-NOMBRE(II,J3)
C2270        JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA(J0)=0.5*(A1+A2+A3+A4)
        PER(J0)=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
c        TOMPC(J1)=COEFFIC*ABS(AREA)/PER
C fin boucle sur J0
      ENDDO
C nouvelle boucle pour que la contrainte soit lissee
      Nsegmax=9
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        hauteurj1=zsurf-z(j1,1)
        areaj1=abs(area(j0))
        perj1=per(j0)
        IF(J0.EQ.NEAUC(K))THEN
          do j4=1,nsegmax
          IF(perj1.lt.hauteurj1)then
             if(j0+j4.lt.neauf(k))then
             perj1=perj1+per(j0+J4)
             areaj1=areaj1+ABS(AREA(J0+J4))
             endif
          endif
          enddo
        ELSEIF(J0.EQ.NEAUF(K))THEN
          do j4=1,nsegmax
          IF(perj1.lt.hauteurj1)then
             if(j0-j4.gt.neauc(k))then
             perj1=perj1+per(j0-J4)
             areaj1=areaj1+ABS(AREA(J0-j4))
             endif
          endif
          enddo
        ELSE
          do j4=1,nsegmax
          IF(perj1.lt.hauteurj1)then
             if(j0-j4.gt.neauc(k))then
               if(j0+j4.lt.neauf(k))then
                 perj1=perj1+per(j0+j4)+per(j0-j4)
                 areaj1=areaj1+ABS(AREA(J0-j4))+ABS(AREA(J0+j4))
               else
                 perj1=perj1+per(j0-J4)
                 areaj1=areaj1+ABS(AREA(J0-j4))
               endif
             elseif(j0+j4.lt.neauf(k))then
               perj1=perj1+per(j0+J4)
               areaj1=areaj1+ABS(AREA(J0+J4))
             endif
          endif
C fin boucle sur nsegmax
          enddo
C fin du if sur debut et fin de zone en eau
        ENDIF
        TOMPC(J1)=COEFFIC*AREAJ1/PERJ1
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CONTMPCPENTEMOY3(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCULE DE LA CONTRAINTE AU FOND ET SUR LES BERGES PAR LA METHODE DES PERPENDICULAIRES
C CONFONDUES avec pente du segment moyenne sur 3 segments
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e par la MPC
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NCMAX,NPMAX,LMAX,CSMAX,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION ANGL(NPMAX,0:NCMAX),ANGL1
      DOUBLE PRECISION YY(NPMAX,0:NCMAX),ZZ(NPMAX,0:NCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION Y1,Y2,Z1,Z2
      DOUBLE PRECISION A1,A2,A3,A4,AREA,PER
      DOUBLE PRECISION PENII,AN,AN1,AN2,TA1,TA2
      DOUBLE PRECISION INFINI,PI,COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION PENL,V2CO,YMIL,INFINIM,ZSURF
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      INTEGER NOMBRE(NPMAX,0:NCMAX),NOMBRE1,IM
      INTEGER I,I0,I1,II,III,J3,J,J0,J1,J4,J2,JJ,JJJ
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS

      LOGICAL INTERSECTION,POINBA

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
C      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
C      COMMON/PDEFOR/JDEFMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

C      DATA V2CO,YMIL/0.D0,0.D0/
      DATA INFINI,INFINIM,PI/1000000000.D0,-1000000000.D0,3.141592654D0/
C      SAVE V2CO,YMIL
      SAVE INFINI,INFINIM,PI

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
c      DO K=1,KEAU
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
c      ENDDO
c      write(*,*) 'fin boucle sur J2'
c      pause
c      write(*,*) 'KEAU',K
C pour tous les points on definit le milieu du segment de coordonnees
C YY(1,j) et ZZ(1,j)
C puis la perpendiculaire jusqu a la surface
c d'angle avec l horizontale angl(1,j)
C angle (i,j) est l'angle avec l'horizontale du
C troncon i de la ligne j et compris entre 0 et PI
C angle compt� positivement dans le sens aiguilles montre
C nombre(i,j) est le nombre de lignes confondues
C sur le troncon i de la ligne j
C boucle sur les zones en eau

      DO K=1,KEAU
C J'utilise la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2)THEN
c      write(*,*) 'K',K
c      write(*,*) 'NEAUC',NEAUC(K)
c      write(*,*) 'NEAUF',NEAUF(K)
      DO 50 J2=NEAUC(K),NEAUF(K)-1
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
        NOMBRE(1,J2)=1
C la modif de contpentemoy3 est de prendre la pente entre 3 points
C attention : pas de modif en extremite
c attention : modif pas repercutee en cas de poinba donc
c possibilit� d'incompatibilite
        IF(ABS(XYCOU(J4+2)-XYCOU(J4-1)).LE.EPS)THEN
          IF(Z(J4-1,1).GE.Z(J4+2,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+2,1)-Z(J4-1,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+2)-XYCOU(J4-1))
     :/(Z(J4-1,1)-Z(J4+2,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4-1,1)-Z(J4+2,1))/(XYCOU(J4+2)-XYCOU(J4-1))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1
C fin boucle sur J2
50    CONTINUE
c      write(*,*) 'fin boucle sur J2 apres continue 50'

C AP2 on rajoute en exxtremite deux droites fictives
C on recalcule de meme maniere sauf si point hors eau
      J2=NEAUF(K)
        J4=PSOUE(J2)
        NP(J2)=2
        YY(1,J2)=0.5*(XYCOU(J4)+XYCOU(J4+1))
        ZZ(1,J2)=0.5*(Z(J4,1)+Z(J4+1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2-1)+(ZZ(1,J2-1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2-1))
     &    /(ZZ(1,J2-1)-0.5*(Z(J4,1)+Z(J4+1,1)))
      ENDIF

        NOMBRE(1,J2)=1
        IF(ABS(XYCOU(J4+1)-XYCOU(J4)).LE.EPS)THEN
          IF(Z(J4,1).GE.Z(J4+1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.D0
            ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
C pour les perpendiculaires horizontales, on met egalite cotes
C et pas deuxieme point en surface
          ZZ(2,J2)=ZZ(1,J2)
        ELSE
          IF(ABS(Z(J4+1,1)-Z(J4,1)).LE.EPS)THEN
            ANGL(1,J2)=0.5*PI
          ELSE
            ANGL(1,J2)=ATAN((XYCOU(J4+1)-XYCOU(J4))/(Z(J4,1)-Z(J4+1,1)))
            IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
          ENDIF
          PENL=(Z(J4,1)-Z(J4+1,1))/(XYCOU(J4+1)-XYCOU(J4))
          CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)
        ENDIF
        ANGL(2,J2)=ANGL(1,J2)
        NOMBRE(2,J2)=1

      J=PSOUE(NEAUC(K))
      J2=NEAUC(K)-1
      NP(j2)=2
      YY(1,J2)=0.5*(XYCOU(J)+XYCOU(J-1))
      ZZ(1,J2)=0.5*(Z(J,1)+Z(J-1,1))
C si point au dessus eau       on le ramene au dessous
      IF(ZZ(1,J2).GT.ZSURF) THEN
      ZZ(1,J2)=ZSURF-EPS
      YY(1,J2)=YY(1,J2+1)+(ZZ(1,J2+1)-ZZ(1,J2))*(YY(1,J2)-YY(1,J2+1))
     &    /(ZZ(1,J2+1)-0.5*(Z(J,1)+Z(J-1,1)))
      ENDIF

      IF(ABS(XYCOU(J-1)-XYCOU(J)).LE.EPS)THEN
          IF(Z(J,1).LE.Z(J-1,1))THEN
            YY(2,J2)=INFINI
            ANGL(1,J2)=0.
          ELSE
            YY(2,J2)=INFINIM
            ANGL(1,J2)=PI
          ENDIF
          ZZ(2,J2)=ZZ(1,J2)
      ELSE
        IF(ABS(Z(J-1,1)-Z(J,1)).LE.EPS)THEN
          ANGL(1,J2)=0.5*PI
        ELSE
          ANGL(1,J2)=ATAN((XYCOU(J-1)-XYCOU(J))/(Z(J,1)-Z(J-1,1)))
        ENDIF
        IF(ANGL(1,J2).LT.0.)ANGL(1,J2)=PI+ANGL(1,J2)
        PENL=      (Z(J,1)-Z(J-1,1))/(XYCOU(J-1)-XYCOU(J))
        CALL CRSURF(YY(1,J2),ZZ(1,J2),PENL,ZSURF
     :            ,YY(2,J2),ZZ(2,J2),V2CO,YMIL)

      ENDIF
      ANGL(2,J2)=ANGL(1,J2)
      NOMBRE(1,J2)=1
      NOMBRE(2,J2)=1
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K'
c      DO K=1,KEAU
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
c      ENDDO
c AP
c on cherche maintenant les points d'intersection des perpendiculaires
C on utilise l'indice de la seconde perpendiculaire
C AP2 on s arrete a JSOUEMAX-1 et on commence a 2
C c est a dire que pour la derniere JSOUEMAX et la premiere 0 perpendiculaires
C les intersections ne sont pas recherchees
C modif car pour bord vertical il faut calulcer
C les intersections de 0 a JSOUEMAX
      DO K=1,KEAU
C! je ne calcule par la MPC que si j'ai au moins 3 points sous la surface d'eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO 2000 J3=NEAUC(K),NEAUF(K)
        J4=PSOUE(J3)
        II=1
        JJ=J3-1
C pour que la ligne J3 croise une ligne anterieure,
C il faut que a la surface son point soit positionne a gauche
2300    IF(YY(NP(J3),J3).LT.YY(NP(JJ),JJ))THEN
        INTERSECTION=.FALSE.
C np(jj)-1 car on regarde intersection sur I0, I0+1
        DO 2250 I0=1,NP(JJ)-1
c         write(*,*) 'entree 2250'
C on ne continue la ligne jj que si pas encore intersection
        IF(.NOT.INTERSECTION)THEN
c        write(*,*) 'II',II
c        write(*,*) 'I0',I0
c        write(*,*) 'J3',J3
c        write(*,*) 'not intersection 2250'
          Y1=YY(II,J3)
          Z1=ZZ(II,J3)
          ANGL1=ANGL(II,J3)
          NOMBRE1=NOMBRE(II,J3)
C          IF(ANGL1.LT.0.) ANGL1=PI+ANGL1
c          IF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
c            PEN1=INFINI
c          ELSE
c            PEN1=TAN(ANGL1)
c          ENDIF
C          IF(ANGL(I0,JJ).LT.0.) ANGL(I0,JJ)=PI+ANGL(I0,JJ)
c          IF(ABS(ANGL(I0,JJ)-0.5*PI).LE.EPS)THEN
c            PEN2=INFINI
c          ELSE
c            PEN2=TAN(ANGL(I0,JJ))
c          ENDIF
          POINBA=.FALSE.
          IF(I0.EQ.1)THEN
            IF(Z(J4,1).LT.Z(J4-1,1))THEN
              IF(Z(J4,1).LT.Z(J4+1,1))THEN
                TA1=SQRT((XYCOU(J4)-XYCOU(J4-1))**2
     &              +(Z(J4,1)-Z(J4-1,1))**2)
                TA2=SQRT((XYCOU(J4+1)-XYCOU(J4))**2
     &              +(Z(J4+1,1)-Z(J4,1))**2)
C AN1 et AN2 sont les angles du fond avec horizontale
                IF(XYCOU(J4).EQ.XYCOU(J4-1))THEN
                  AN1=0.5*PI
                ELSE
                  AN1=ATAN((Z(J4,1)-Z(J4-1,1))
     &                  /(XYCOU(J4)-XYCOU(J4-1)))
                ENDIF
                IF(XYCOU(J4+1).EQ.XYCOU(J4))THEN
                  AN2=0.5*PI
                ELSE
                  AN2=ATAN((Z(J4+1,1)-Z(J4,1))
     &                  /(XYCOU(J4+1)-XYCOU(J4)))
                ENDIF
                IF(AN1.LT.0)AN1=AN1+PI
                IF(AN2.LT.0)AN2=AN2+PI
                AN=AN1-AN2
                IF(AN.LT.0.5*PI)THEN
c si angle saillant on confond intersection
C avec pied perpendiculaire d'un cote
C et seulement si difference de longueur importante
C car dans ce cas intersection sous le fond
                IF(TA1.GT.TA2/COS(AN))THEN
                  YY(2,J3)=YY(1,J3-1)
                  ZZ(2,J3)=ZZ(1,J3-1)
                  ANGL(2,J3)=ANGL(1,J3-1)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception1',i0,jj,J3
C                 pause
                ELSEIF(TA2.GT.TA1/COS(AN))THEN
                  YY(2,J3)=YY(1,J3)
                  ZZ(2,J3)=ZZ(1,J3)
                  ANGL(2,J3)=ANGL(1,J3)
                  II=2
                  Y2=YY(2,J3)
                  Z2=ZZ(2,J3)
                  POINBA=.TRUE.
                  INTERSECTION=.TRUE.
C              write(*,*) 'exception2',i0,jj,J3
C                 pause

C fin if sur placement ta1 et TA2
                ENDIF
c      write(*,*) 'fin if sur placement ta1 et TA2'
c fin if sur difference infeieure a PI/2
                ENDIF
c      write(*,*) 'fin if sur difference infeieure a PI/2'
c fin if cas normal ou i0 differente de 1, etc
              ENDIF
c      write(*,*) 'fin cas normal ou i0 differente de 1, etc'
              ENDIF
          ENDIF
c      DO J2=NEAUC(K)-1,NEAUF(K)
c      write(*,*) 'ANGL(1,',J2,')',ANGL(1,J2)
c      write(*,*) 'ANGL(2,',J2,')',ANGL(2,J2)
c      ENDDO
          IF(.NOT.POINBA)THEN
          CALL CROISEE(Y1,Z1,ANGL1,YY(I0,JJ),ZZ(I0,JJ),ANGL(I0,JJ)
     :,Y2,Z2)
C on ne prend en compte intersection que si appartient au segment
            IF(Z2.LT.ZZ(I0+1,JJ)+EPS)THEN
              IF(Z2.GT.ZZ(I0,JJ)-EPS)THEN
                INTERSECTION=.TRUE.
                 endif
            endif
            IF(INTERSECTION)THEN
              II=II+1
              YY(II,J3)=Y2
              ZZ(II,J3)=Z2
              ANGL(II,J3)=(NOMBRE1*ANGL1+NOMBRE(I0,JJ)*ANGL(I0,JJ))
     &      /(NOMBRE1+NOMBRE(I0,JJ))

C controle inutil car les deux angles sont deja entre 0 et PI
C            IF(ANGL(II,J3).LT.0.) ANGL(II,J3)=PI+ANGL(II,J3)
            ENDIF
c fin du if sur poinba
          endif
c      write(*,*) 'fin sur poinba'
          IF(INTERSECTION)THEN
c      write(*,*) 'entree intersection'
              NOMBRE(II,J3)=NOMBRE1+NOMBRE(I0,JJ)
C on en prend pas en compte le cas ou la nouvelle ligne
C serait horizontale car ne peut arriver que si corniche
            IF(ABS(ANGL(II,J3)-PI).LE.EPS)THEN
              PENII=INFINI
            ELSEIF(ABS(ANGL(II,J3)).LE.EPS)THEN
              PENII=INFINI
            ELSE
c      write(*,*) 'II',II
c      write(*,*) 'ANGL(',J3,')',ANGL(II,J3)
               PENII=1/TAN(ANGL(II,J3))
c      write(*,*) 'apres PENII'

            ENDIF
            NP(J3)=II+1
            CALL CRSURF(YY(II,J3),ZZ(II,J3),PENII,ZSURF
     :            ,YY(II+1,J3),ZZ(II+1,J3),V2CO,YMIL)
            ANGL(II+1,J3)=ANGL(II,J3)
            NOMBRE(II+1,J3)=NOMBRE(II,J3)
c on egale a la ligne J3 toutes celles avant
C qui etaient normalement un seul et meme segment
            DO 2252 JJJ=J3-1,jj+1,-1
c             write(*,*) 'entree boucle 2252'
                III=NP(JJJ)-1
                YY(III+1,JJJ)=YY(II,J3)
                ZZ(III+1,JJJ)=ZZ(II,J3)
                ANGL(III+1,JJJ)=ANGL(II,J3)
                NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
                NP(JJJ)=III+2
                ZZ(III+2,JJJ)=ZZ(II+1,J3)
                YY(III+2,JJJ)=YY(II+1,J3)
                ANGL(III+2,JJJ)=ANGL(II,J3)
                NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
2252        CONTINUE
c      write(*,*) 'apres 2252'

            IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
C2245        IF((ABS(YY(I0,JJ)-Y2).LT.EPS)
     &      .AND.(ABS(ZZ(I0,JJ)-Z2).LT.EPS))THEN
C si intersection pres de i0,jj  on supprime ce point
              IM=I0
C              write(10,*) 'exception5',i0,jj,J3
C                 pause
            ELSE
              IM=I0+1
            ENDIF
C on egale la ligne jj et la ligne J3
C au dela de IM et II respectivement
c ce qui evut dire np(jj)-IM+1 points supprimes
            NPS=NP(JJ)-IM+1
C on egale aussi les lignes confondues avec jj
C modif du 28 septembre  2010
C pour eviter de prendre NP (< 0) mais sans comprendre l'erreur
               if(J3.LT.nombre(II,J3)-1)then
                          nombre(II,J3)=j3+1
                        endif
            DO 2255 JJJ=JJ,j3-nombre(II,J3)+1,-1
C III est indice du dernier point conserve
              III=NP(JJJ)-NPS
              YY(III+1,JJJ)=YY(II,J3)
              ZZ(III+1,JJJ)=ZZ(II,J3)
              ANGL(III+1,JJJ)=ANGL(II,J3)
              NOMBRE(III+1,JJJ)= NOMBRE(II,J3)
              ZZ(III+2,JJJ)=ZZ(II+1,J3)
              YY(III+2,JJJ)=YY(II+1,J3)
              ANGL(III+2,JJJ)= ANGL(II,J3)
              NOMBRE(III+2,JJJ)=NOMBRE(II,J3)
              NP(JJJ)=III+2
2255        CONTINUE
c      write(*,*) 'apres 2255'
C on continue a remonter le long de la ligne J3
C maintenant confondue avec nombre-1 lignes a partir de II
             JJ=J3-NOMBRE(II,J3)
C2270        JJ=J3-NOMBRE(II,J3)
C on termine c'est a dire on passe a J3+1 (2000)
C si plus de points a gauche      car on a eu intersection
C sinon on recommence pour voir si la nouvelle ligne
C  intersecte la nouvelle jj
            IF(JJ.GT.NEAUC(K)-2)GOTO 2300
C fin du if sur intersection
          ENDIF
C fin du if sur pas intersection
        ENDIF
c      write(*,*) 'sur pas intersection'
C passage au I0 suivant sur la ligne jj
2250    CONTINUE
C fin du if sur possibilite intersection avec lignes precedentes
        ENDIF
C passage a J3 suivant
2000  CONTINUE
      ENDIF
C fin de boucle sur K
      ENDDO
c      write(*,*) 'fin boucle sur K apres 2000'
C on calcule maintenant les aires qui donnent les contraintes
C boucle sur nombre de zones en eau independantes
      DO K=1,KEAU
C seulement si plus de 3 points en eau
      IF(NEAUF(K)-NEAUC(K).GT.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
        A1=0.
        A2=0.
        A3=0.
        A4=0.
C on calcule des aires jusqu'au zero des altitudes (sans incidence en double precision)
C A1 et A2 sont des aires sous la perpendiculaire
C A2 est l'oppos� du A1 du I1 precedent sauf au premier point
        DO I1=1,NP(J0)-1
          A1=A1+(YY(I1+1,J0)-YY(I1,J0))*(ZZ(I1+1,J0)+ZZ(I1,J0))
        ENDDO
        DO I1=1,NP(J0-1)-1
          A2=A2+(YY(I1,J0-1)-YY(I1+1,J0-1))*(ZZ(I1,J0-1)+ZZ(I1+1,J0-1))
        ENDDO
C A3 est aire sous le fond
        A3=(XYCOU(J1)-YY(1,J0-1))*(Z(J1,1)+ZZ(1,J0-1))
     &    +(YY(1,J0)-XYCOU(J1))*(ZZ(1,J0)+Z(J1,1))
C A4 est un rectangle depuis la surface,
C nul si perpendiculaires confondues  quelque part
        A4=(YY(NP(J0-1),J0-1)-YY(NP(J0),J0))
     &    *(ZZ(NP(J0-1),J0-1)+ZZ(NP(J0),J0))
        AREA=0.5*(A1+A2+A3+A4)
        PER=SQRT((YY(1,J0)-XYCOU(J1))**2+(ZZ(1,J0)-Z(J1,1))**2)
     &    +SQRT((XYCOU(J1)-YY(1,J0-1))**2
     &     +(Z(J1,1)-ZZ(1,J0-1))**2)
           If(per.gt.EPS)then
          TOMPC(J1)=COEFFIC*ABS(AREA)/PER
              elseif(J0.gt.NEAUC(K))THEN
                   TOMPC(J1)=TOMPC(PSOUE(J0-1))
              else
             TOMPC(J1)=0.
              endif
C fin boucle sur J0
      ENDDO
        ELSE
C si 3 ou moins points en eau
C      ELSEIF(NEAUF(K)-NCEAU(K).LE.2) THEN
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C        TOMPC(J1)=COEFFIC*(ZSURF-Z(J1,1))
C expression en theorie valable uniquement si un seul lit
c        TOMPC(J1)=COEFFIC*RHINTER(I)
C correction du 3 mai 2006 au cas o� plusieurs lits
        TOMPC(J1)=COEFFIC*MIN(RHINTER(I),ZSURF-Z(J1,1))
      ENDDO
      ENDIF
C fin boucle sur K
      ENDDO
c      write(*,*)'sortiecontrainte'
c      pause
      RETURN
      END

C
C-----------------------------------------------------------------------
      SUBROUTINE CONTHAUT(I,Z,COEFFIC,TOMPC)
C-----------------------------------------------------------------------
C CALCUL DE LA CONTRAINTE AU FOND proportionellement a hauteur eau
C-----------------------------------------------------------------------
C IS: NUMERO INTERMAILLE
C NT:NOMBRE DE POINTS SITUES SOUS LA SURFACE D'EAU
C Zsurf:cote de la surface libre de l'intermaille IS
C COEFFIC: ROGRAV*J
C T0: contrainte locale calcul�e
C---------------------------------------------------------------------------------
C ENTREE: g�ometrie,cote d'eau, points situ�s sous la surface d'eau
C SORTIE: contrainte T0MPC
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NCMAX,NPMAX,LMAX,CSMAX,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300,NCMAX=1000)
      DOUBLE PRECISION XYCOU(LNCMAX),XZCOU(LNCMAX)
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
     &  ,XZCOUP1(LNCMAX),MACTP1(LMAX),DACTP1(LMAX),SACTP1(LMAX)
     &  ,TMACTP1(LMAX),TFACTP1(LMAX),Z(LNCMAX,CSMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      INTEGER XNBCSP1(LNCMAX)
      DOUBLE PRECISION TOMPC(LNCMAX)
      DOUBLE PRECISION COEFFIC
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION V2CO,YMIL,ZSURF
      INTEGER I,J,J0,J1,J2
     &      ,PSOUE(LNCMAX)
C     &      ,PSOUE(LNCMAX),DEFORM(LNCMAX)
      INTEGER JSOUEMAX,NP(0:NCMAX)
      INTEGER NEAUC(NCMAX),NEAUF(NCMAX),K,KEAU,NPS

C cour2g contient inverse du produit rayon de courbure par 2G
      DOUBLE PRECISION COUR2G(LMAX)

      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMACZ/XZCOU
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/GEOACP1/MACTP1,DACTP1,SACTP1,TMACTP1,TFACTP1
      COMMON/GEOCP1/XZCOUP1
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
C      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/COURB/COUR2G

      ZSURF=XCTDF(I)+YINTER(I)
c      IF(
      V2CO=VINTER(I)**2*COUR2G(I)
      YMIL=0.5*(XYCOU(PSOUE(1))+XYCOU(PSOUE(JSOUEMAX)))
c      ENDIF
C AP
C      DEBUT=.TRUE.
C chaque zone en eau K est limitee par neauc(k) et neauf(k)
      NEAUC(1)=1
      K=1

      DO J2=1,JSOUEMAX-1
        J=PSOUE(J2)
c          IF(DEBUT)THEN
C si il y a un point hors d'eau on saute de zone
            IF(J+1.LT.PSOUE(J2+1))THEN
               NEAUF(K)=J2
               K=K+1
               NEAUC(K)=J2+1
c               DEBUT=.FALSE.
            ENDIF
C si pas debut, on cherche le point au dessous du niveau eau
c          ELSE
c               NEAUC(K)=J2
c               DEBUT=.TRUE.
c          ENDIF
C fin boucle sur J2
      ENDDO
      NEAUF(K)=JSOUEMAX
      KEAU=K
      DO K=1,KEAU
      DO J0=NEAUC(K),NEAUF(K)
        J1=PSOUE(J0)
C on multiplie par la hauteur au lieu du rapport surface/perimetre
C si courbure on tient compte dela pente transversale
        TOMPC(J1)=COEFFIC*(ZSURF+V2CO*(xYcou(j1)-YMIL)-Z(J1,1))
      ENDDO
C fin boucle sur K
      ENDDO
      RETURN
      END


C-----------------------------------------------------------------------
      SUBROUTINE CRSURF(Y1,Z1,PENL,ZSURF,Y3,Z3,V2CO,YMIL)
C-----------------------------------------------------------------------
C TROUVE LEs coordonnes du POINT INTERSECTION DE
C une ligne et la surface libre
C SOUS PROGRAMME UTILISE DANS CONTRAINTEMPC
C ENTREE: un point de la ligne Y1,Z1, sa pente,
C la cote moyenne de la surface libre
C et V2CO carr� de la vitesse moyenne       multiplie par
C 1/2*G*Rayon de courbure
C Ymil milieu de la largeur en eau
C SORTIE: Coordonnees du point intersection Y3,Z3
C-----------------------------------------------------------------------
      IMPLICIT NONE
c      INTEGER NCMAX,NPMAX,LMAX,CSMAX
c      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NPMAX=300)
c      DOUBLE PRECISION Y2(NPMAX,NCMAX),Z2(NPMAX,NCMAX)

      DOUBLE PRECISION Y1,Y3,Z1,Z3,PENL,ZSURF
      DOUBLE PRECISION V2CO,YMIL
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

c      write(*,*) 'entree srsurf'
C normalement cas ou pas de courbure
      IF(V2CO.LT.EPS)THEN
        Z3=ZSURF
C on limite le produit v2co*Penl � 0.9
C ce qui veut dire que si penl est fort donc perpendiculaire
C quasi horizontale et de meme sens que la surface libre
C on suppose V2CO=0.9/PENL
      ELSEIF(V2CO*PENL.GT.0.9)THEN
        Z3=10.*(ZSURF-0.9*Z1+0.9*(Y1-YMIL)/PENL)
      ELSE
        Z3=(ZSURF+V2CO*(Y1-YMIL-PENL*Z1))/(1.-V2CO*PENL)
      ENDIF
      Y3=Y1+PENL*(Z3-Z1)
c      write(*,*) 'sortie srsurf'
      RETURN
      END

C-----------------------------------------------------------------------
      SUBROUTINE CROISEE(Y1,Z1,ANGL1,Y2,Z2,ANGL2,Y3,Z3)
C-----------------------------------------------------------------------
C TROUVE LE POINT INTERSECTION DE DEUX LIGNES
C SOUS PROGRAMMES UTILISE DANS CONTRAINTEMPC
C ENTREE: Caracteristiques des deux lignes
C SORTIE: Coordonnes du point intersection
C-----------------------------------------------------------------------

      IMPLICIT NONE
      DOUBLE PRECISION Y1,Y2,Y3,INFINI,PI
      DOUBLE PRECISION Z1,Z2,Z3,PENT1,PENT2,ANGL1,ANGL2
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      DATA INFINI,PI/1000000000.D0,3.141592654D0/
      SAVE INFINI,PI

      IF(ABS(ANGL1-ANGL2).LE.EPS)THEN
        Y3=INFINI
        Z3=INFINI
       RETURN
      ELSEIF(ABS(ANGL1-0.5*PI).LE.EPS)THEN
        Y3=Y1
        IF(ANGL2.LT.EPS)THEN
          Z3=Z2
        ELSEIF(ANGL2.GT.PI-EPS)THEN
          Z3=Z2
        ELSE
          Z3=TAN(ANGL2)*(Y3-Y2)+Z2
        ENDIF
        RETURN
      ELSEIF(ABS(ANGL2-0.5*PI).LE.EPS)THEN
        Y3=Y2
        IF(ANGL1.LT.EPS)THEN
          Z3=Z1
        ELSEIF(ANGL1.GT.PI-EPS)THEN
          Z3=Z1
        ELSE
          Z3=TAN(ANGL1)*(Y3-Y1)+Z1
        ENDIF
        RETURN
      ELSE
        PENT1=TAN(ANGL1)
        PENT2=TAN(ANGL2)
        Y3=(PENT1*Y1-PENT2*Y2+(Z2-Z1))/(PENT1-PENT2)
        Z3=PENT1*(Y3-Y1)+Z1
C       Z3=0.5*(PENT2*(Y3-Y2)+Z2+PENT1*(Y3-Y1)+Z1)
      ENDIF
      RETURN
      END
C-----------------------------------------------------------------------
C***********************************************************************
C                          LISTE  DES  FONCTIONS
C***********************************************************************

C-----------------------------------------------------------------------
      LOGICAL FUNCTION PROCHE(D1,S1,T1,D2,S2)
C-----------------------------------------------------------------------
C Indique si les caract�ristiques des s�diments 1 et 2 sont proches
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION D1,S1,D2,S2,DL1,DL2,DL3,DL4,T1
          DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM,COEFPROCHE
c      DOUBLE PRECISION SERTJUSTEAEVITERLESWARNIGS
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
          COMMON/CPROCHE/COEFPROCHE

C Version provisoire: on pourrait tester les contraintes de mise en mouvement
C le 25/5/2020 ajout de T1 contrainte couche 1
      IF (T1.GT.900.)then
         Proche=.FALSE.
         return
      endif

      PROCHE=.TRUE.
C      EPS=0.1
      DL1=D2*(1.+COEFPROCHE*(S2-1.))*(1.+EPS)
          DL2=D1/(1.+COEFPROCHE*(S1-1.))*(1.-EPS)
      DL3=D2/(1.+COEFPROCHE*(S2-1.))*(1.-EPS)
          DL4=D1*(1.+COEFPROCHE*(S1-1.))*(1.+EPS)
C      IF(D1.GT.D2*S2*(1.+EPS).AND.(D2.LT.D1/S1*(1.-EPS))) THEN
      IF(D1.GT.DL1.AND.D2.LT.DL2) THEN
        PROCHE=.FALSE.
C      ELSEIF(D1.LT.D2/S2*(1.-EPS).AND.(D2.GT.D1*S1*(1.+EPS)))THEN
      ELSEIF(D1.LT.DL3.AND.D2.GT.DL4)THEN
        PROCHE=.FALSE.
      ENDIF
C      IF (.not.Proche)then
C      write(*,*)'proche',d1,s1,d2,s2
C      endif
c      SERTJUSTEAEVITERLESWARNIGS=M1+M2

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION MBA1(I)
C-----------------------------------------------------------------------
C Renvoie la masse du compartiment de base Ba sup�rieur
C-----------------------------------------------------------------------
C I num�ro de l'intermaille
C DX longueur de la maille consid�r�e
C XLISEC(XNCMO(I)) largeur (maximale) du lit mineur
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX
      DOUBLE PRECISION MBADDEF
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10)
      PARAMETER (MBADDEF=999999.)
      INTEGER I
      DOUBLE PRECISION DX,LARG,EPAI
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPECUM(LNCMAX)
     &  ,XPICUM(LNCMAX)
      INTEGER XNBCS(LNCMAX)
      DOUBLE PRECISION XZCS(LNCMAX,CSMAX),XMCS(LNCMAX,CSMAX)
     &  ,XDCS(LNCMAX,CSMAX),XSCS(LNCMAX,CSMAX)
     &  ,XTMCS(LNCMAX,CSMAX),XTFCS(LNCMAX,CSMAX)
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)

      COMMON/NCMM/NCMO,XNCMO
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/COMSED/XNBCS,XZCS,XMCS,XDCS,XSCS,XTMCS,XTFCS
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/GEOMT/TMAIL,CTDF,PEN

      IF(XNBCS(I).LE.1)THEN
        WRITE(*,*)'MBA1: un seul compartiment a l''intermaille ',I
        MBA1=MBADDEF
      ELSE
        DX=TMAIL(I+1)-TMAIL(I)
        LARG=XLISEC(XNCMO(I))
        EPAI=XZCS(I,1)-XZCS(I,2)
        MBA1=EPAI*LARG*DX*ROS/POR1
      ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION TOCMM(D,S,JRH,RH)
C-----------------------------------------------------------------------
C Renvoie la contrainte critique de mise en mouvement
C-----------------------------------------------------------------------
C D diam�tre repr�sentatif (diam�tre m�dian)
C S param�tre d'�tendue granulom�trique
C JRH pseudo-contrainte sur la section (J.Rh)
      IMPLICIT NONE
      DOUBLE PRECISION D,S,JRH,RH
c      DOUBLE PRECISION SERTJUSTEAEVITERLESWARNIGS

      DOUBLE PRECISION SHIELDS
      EXTERNAL SHIELDS

C Dans cette version du code la contrainte seuil de mise en mouvement
C est identique � la contrainte seuil de fin de mouvement
      TOCMM=SHIELDS(D,JRH,RH)
c      SERTJUSTEAEVITERLESWARNIGS=S

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION SHIELDS(D,JRH,RH)
C-----------------------------------------------------------------------
C Renvoie la contrainte critique de Shields (seuil de fin de mouvement)
C-----------------------------------------------------------------------
C D diam�tre repr�sentatif (diam�tre m�dian)
C JRH pseudo-contrainte sur la section (J.Rh)
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      DOUBLE PRECISION D,JRH,RENPAR,TCADI,RH,DGR,ROM1
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      LOGICAL TRASED,CDCHAR,CGEOM
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF

      COMMON/ROSRO/ROM1
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
C modif 11/06/02 pour calculer avec contrainte critique fixe
C et donnee par utilisateur
      IF(OPTS.EQ.7.OR.OPTS.EQ.15.OR.OPTS.EQ.27.OR.OPTS.EQ.28)THEN
        TCADI=TCADIM
      ELSEIF(OPTS.EQ.9)THEN
                IF(RH.LT.EPS)THEN
                  TCADI=TCADIM
                ELSEIF(JRH.GT.0.7*RH)THEN
                     TCADI=0.
                ELSE
           TCADI=SQRT(1./(1.+(JRH/RH)**2))*(1.-(JRH/RH)/0.7)*TCADIM
             ENDIF
      ELSEIF(OPTS.EQ.16)THEN
                DGR=sqrt(grav*D**3)/visc
             dgr=(dgr*sqrt(rom1))**(-0.6)
                tcadi=0.22*dgr+0.06*10.**(-7.7*dgr)
         ELSEIF(OPTS.EQ.8.OR.OPTS.EQ.2.OR.OPTS.EQ.3.OR.OPTS.EQ.13
     :.OR.OPTS.EQ.18.OR.OPTS.EQ.19.OR.OPTS.EQ.22)THEN
C cas EH,Bagnold,AckersWhite, sato,yang,karim car pas de contrainte critique
                     TCADI=0.
      else
C cas ou opts est egal a 1,10,11,12,14,17,20,21
       if(.NOT.TCSHIELDS)THEN
               TCADI=TCADIM
      ELSEIF(opts.eq.17.OR.opts.eq.20.OR.opts.eq.21)then
C courbe de shields par van rijn
         DGR=D*(grav*ROM1/visc**2)**0.333333
               IF(DGR.LT.4.)then
           TCADI=0.24/DGR
               ELSEIF(DGR.LT.10.)then
           TCADI=0.14*DGR**(-0.64)
               elseIF(DGR.LT.20.)then
           TCADI=0.04*DGR**(-0.1)
               elseIF(DGR.LT.150.)then
           TCADI=0.013*DGR**(-0.29)
               ELSE
           TCADI=0.056
            endif
          else
        RENPAR=(D*SQRT(GRAV*JRH))/VISC
        IF(RENPAR.LE.0.2)THEN
          TCADI=0.575
        ELSEIF(RENPAR.LT.2.)THEN
          TCADI=0.115/RENPAR
        ELSEIF(RENPAR.LT.5.)THEN
          TCADI=0.081317279*RENPAR**(-0.5)
        ELSEIF(RENPAR.LT.10.)THEN
          TCADI=0.050175514*RENPAR**(-0.2)
        ELSEIF(RENPAR.LT.40.)THEN
          TCADI=0.025147327*RENPAR**(0.1)
        ELSEIF(RENPAR.LT.490.)THEN
          TCADI=0.017389449*RENPAR**(0.2)
        ELSE
          TCADI=0.060
        ENDIF
C fin du if sur tcshields
       ENDIF
C fin du if sur opts
      ENDIF
      SHIELDS=TCADI*D*GRAV*(ROS-RO)

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DICHAR(I,D)
C-----------------------------------------------------------------------
C Renvoie la distance de chargement [m]
C-----------------------------------------------------------------------
C CDCHAR .TRUE. si calcul (ici, charg. et d�charg. idem), .FALSE. si constante
C JRH pseudo-contrainte dans la section (J.Rh) [m]
C D diam�tre repr�sentatif du compartiment s�dimentaire consid�r� [m]
      IMPLICIT NONE
      INTEGER LMAX,I
      PARAMETER(LMAX=3000)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF,ODCHAR
      DOUBLE PRECISION JRH(LMAX),KS1(LMAX),D
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
         Double precision XL1(LMAX),VINTER(LMAX),SINTER(LMAX)
     :,YINTER(LMAX),RHINTER(LMAX)

      DOUBLE PRECISION VITCHUT
      EXTERNAL VITCHUT

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/SOLIDE/JRH,KS1
         COMMON/ODCHAR/ODCHAR

      IF(CDCHAR)THEN
        IF(ODCHAR.EQ.1)THEN
          DICHAR=HALFA*SQRT(GRAV*JRH(I))/VITCHUT(D)
C odchar=2
        ELSE
          DICHAR=MAX(DCHARG,YINTER(I)*VINTER(I)/(HALFA*VITCHUT(D)))
        ENDIF
      ELSE
        DICHAR=DCHARG
      ENDIF

      RETURN
      END

C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DEBSOL(I,D,TCFM,SACT,LACT)
C-----------------------------------------------------------------------
C Calcule la capacit� de transport solide [kg/s]
C ATTENTION, changement d'unit� depuis ruts9
C-----------------------------------------------------------------------
C JRH pseudo-contrainte dans la section (J.Rh) [m]
C KS1: coefficent de correction de la contrainte efficace []
C D diam�tre repr�sentatif du compartiment s�dimentaire consid�r� [m]
C TCFM contrainte critique de fin de mouvement [N/m2]
C LACT largeur active [m]
C VITLIQ vitesse d�bitante du liquide [m/s]
C SACT etendue granulometrique
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER J,I,CAPASOL
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      INTEGER JDEF,JDEFMAX,DEFORM(LNCMAX),JSOUE,JSOUEMAX
     &      ,PSOUE(LNCMAX)
      INTEGER CHOIXC
         LOGICAL TCPENTE,TCSHIELDS,TAUEFFICACE
      DOUBLE PRECISION JRH(LMAX),KS1(LMAX)
     :,D,TCFM,LACT,VITLIQ,ZABS,ROGRAVJ
     &,CONTRAINTE,COEFC,SACT,RH,YH,TCFM1,CONTRAINTEEFF,JRH2
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      DOUBLE PRECISION XYCOU(LNCMAX)
      DOUBLE PRECISION VICHUT,DSTAR,TAU0
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER XNCMMAG(0:LMAX),XNCMMAD(0:LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      DOUBLE PRECISION DXMAIL(LMAX),XDYA(LNCMAX)
      INTEGER XNBCSP1(LNCMAX),IJ
      DOUBLE PRECISION XZCSP1(LNCMAX,CSMAX),XMCSP1(LNCMAX,CSMAX)
     &  ,XDCSP1(LNCMAX,CSMAX),XSCSP1(LNCMAX,CSMAX)
     &  ,XTMCSP1(LNCMAX,CSMAX),XTFCSP1(LNCMAX,CSMAX)
           DOUBLE PRECISION TO(LNCMAX),TC(LNCMAX),CK(LNCMAX)
     &   ,TOMPC(LNCMAX),TOEFF(LNCMAX)
     :,SIGNE
      DOUBLE PRECISION DG,Na,Ma,Aa,Caa,Ca,Fa,Ba,Da,Ga,EPSS,ROM1
     :,EF,BETA,PHI,ZED,VFR,AFOND,FROUDE,COEF,S,LARG,COEF2,renpar
     :,FR1(LMAX),coef3,powerstar,conc,vichutstar,ustar,vitliqcr
     :,ustargrain,powercr,ustarc
C        :,vicsm,mustar
C les 3 lignes suivantes sont pour debsol 29 et 30
      double precision debit,dmstar,FRPEAU,coefdm
     :,pente99,puissancen,Qcrit,puissancem,reynolds,coefz,qstar
     :,gfonctiondeqstar,dm,q,cor

      INTEGER LDETYJ
         DOUBLE PRECISION VITCHUT,DETL,DETSN
      EXTERNAL VITCHUT,DETL,DETSN,LDETYJ


      COMMON/NC/NC,XNC
      COMMON/XNCMMA/XNCMMAG,XNCMMAD
      COMMON/XGEOMACY/XYCOU
      COMMON/XGEOMT/XTMAIL,XCTDF
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER
      COMMON/XLGEO/DXMAIL,XDYA
      COMMON/GEOP1/XNBCSP1,XZCSP1,XMCSP1,XDCSP1,XSCSP1,XTMCSP1,XTFCSP1
      COMMON/DEFORM/DEFORM
      COMMON/PSOUE/PSOUE
      COMMON/PJSOUE/JSOUEMAX
      COMMON/PDEFOR/JDEFMAX
      COMMON/CHOIXC/CHOIXC
      COMMON/DDOPTION/TCPENTE,TCSHIELDS,TAUEFFICACE
      COMMON/COEFC/COEFC
Csi contrainte MPC > COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/SOLIDE/JRH,KS1
      COMMON/CAPASOL/CAPASOL
      COMMON/EPSS/EPSS
      COMMON/ROSRO/ROM1
      COMMON/FROTMT/FR1

c       nu=0.000001
        DEBSOL=0.
C gestion des vitesse negatives
       IF(VINTER(I).GT.EPSY)THEN
          VITLIQ=VINTER(I)
          SIGNE=1.
       ELSEIF(VINTER(I).GT.-EPSY)THEN
c          DEBSOL=0.
          RETURN
       ELSE
          VITLIQ=-VINTER(I)
          SIGNE=-1.
       ENDIF
       JRH2=JRH(I)
       YH=YINTER(I)
       RH=RHINTER(I)
c          write(*,*)'debsol',i,vitliq,yh,D,JRH2,xlact,tcfm
       IF(YH.LT.D)THEN
C       IF(YH.LT.3.*D)THEN
c             debsol=0.
c             write(*,*)'debsol',i,yh,D,debsol
         return
       endif
       if(opts.ne.29.and.opts.ne.30)then
C formule de lefort 2007 pour opts=29
C formule de lefort 2015 pour opts=30
C si pas lefort on continue sinon on va a la fin du Sous programme
c             write(*,*)'debsol2',i,yh,D,debsol
c       IF(VITLIQ.GT.10..OR.YH.LT.3.*D)THEN
c          VITLIQ=0.
c          JRH2=0.
c          YH=0.
c       ENDIF
      CONTRAINTE=ROGRAV*JRH2

      IF(CAPASOL.NE.1)THEN
      ZABS=XCTDF(I)+YINTER(I)
        IF(CHOIXC.NE.2) THEN
         J=XNC(I-1)+2     ! on recherche tous les points sous l'eau
         JDEF=0
         JSOUE=0 ! SOUE= sous l'eau
         DO WHILE(J.LT.XNC(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 100     ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1
          PSOUE(JSOUE)=J ! PSOUE= points sous eau, on traite toute la section lit majeur+litmineur
         IF(J.GE.XNCMMAG(I)+1.AND.J.LT.XNCMMAD(I))THEN
          JDEF=JDEF+1
          DEFORM(JDEF)=J
         ENDIF
 100      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JDEF
        ELSEIF(CHOIXC.EQ.2)THEN
C cas de contrainte uniforme dans la section
         J=XNCMMAG(I)+1 ! on recherche les points sous l'eau directement dans le lit actif
         JSOUE=0
        DO WHILE(J.LT.XNCMMAD(I))
          IF(XZCSP1(J,1).GE.ZABS) GOTO 110      ! Cas o� point hors de l'eau
          JSOUE=JSOUE+1 ! ces points vont �tre �ventuellement d�form�s
          PSOUE(JSOUE)=J
          JDEF=JSOUE
          DEFORM(JDEF)=PSOUE(JSOUE) ! varibale introduite uniquement pour coh�rence avec la MPC
 110      CONTINUE
          J=J+1
        ENDDO
        JSOUEMAX=JSOUE
        JDEFMAX=JSOUEMAX
C fin du if sur choixc=2
              ENDIF

      IF(JDEFMAX.GT.0)then
        ROGRAVJ=CONTRAINTE/RHINTER(I)
              If(rogravj.gt.eps)then
        IF(CHOIXC.NE.2) THEN
C calcul de la contrainte selon choix: MPC ou formule r�gime uniforme
         IF(CHOIXC.EQ.1)THEN
           CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.3)THEN
           CALL CONTMPCMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.4)THEN
           CALL CONTMPCPENTEMOY3(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.5)THEN
           CALL CONTMPCMOYH(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.6)THEN
           CALL CONTMPCD(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.7)THEN
           CALL CONTMPCKI(I,XZCSP1,ROGRAVJ,TOMPC)
         ELSEIF(CHOIXC.EQ.8)THEN
           CALL CONTHAUT(I,XZCSP1,ROGRAVJ,TOMPC)
         ENDIF
C        CALL CONTRAINTEMPC(I,XZCSP1,ROGRAVJ,TOMPC)
        DO JDEF=1,JDEFMAX
         J=DEFORM(JDEF)
        IF(TOMPC(J).LT.COEFC*CONTRAINTE)THEN
         TO(J)=TOMPC(J)
        ELSE
        TO(J)=COEFC*CONTRAINTE
C si contrainte MPC > COEFC*ROGRAVRHJ, alors contrainte= COEFC*ROGRAVRHJ
        ENDIF
           IF(TAUEFFICACE)THEN
          to(j)=to(j)*KS1(i)
        endif
C fin boucle sur jdef
              ENDDO
      ELSEIF(CHOIXC.EQ.2)THEN
        DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        TO(J)=CONTRAINTE
           IF(TAUEFFICACE)THEN
          to(j)=to(j)*KS1(i)
        endif
        ENDDO
C fin sur choixc=2
      ENDIF

       IF(TCPENTE) THEN
C calcul de la contrainte critique selon option: avec ou sans facteur CK
        CALL COEFFICIENTCKIKEDA(XZCSP1,CK)
        DO JDEF=1,JDEFMAX
           J=DEFORM(JDEF)
           TC(J)=CK(J)*TCFM
        ENDDO
       ELSE
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          TC(J)=TCFM
       ENDDO
C fin du if sur option
          ENDIF
c else du if sur contrainte nulle
          else
       DEBSOL=0.
       Return
c fin du if sur contrainte nulle
       ENDIF
C else du if sur jdefmax= 0
      ELSE
       DEBSOL=0.
       Return
      ENDIF

C         ELSE
C cas de capasol =1
C deux largeurs actives introduites
C pour que la valeur calculee lact ne remonte pas
C                LACT=XLACT
C fin du if sur capasol different de 1
      ENDIF

      IF(CAPASOL.EQ.2)THEN
C KE je calcule la largeur active LACT par segments
        LACT=0.
c je ne traite pas les deux points extr�mes
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          IF(TO(J).GT.TC(J))LACT=LACT+XDYA(J)
        ENDDO
        IF(LACT.GT.XL1(I))LACT=XL1(I)
C fin du if sur capasol=2
      ENDIF
C kamal Avril
c calcul de la largeur active: pour un point de la la section J, situ� dans le lit actif,
c la largeur �l�mentaire XDYA qui lui est associ� est calcul� par 0.5*(Y(J+1)-Y(J-1))
c si contrainte (J) est > contrainte critique (J), cette largeur intervient dans la largeur
c active
C      CAPSOL3=0.
      IF(CAPASOL.EQ.3)THEN
c      DEBSOL=0.

      IF(OPTS.EQ.1)THEN
c      Write(*,*)' entre debsol3',I

C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.TC(J))THEN
C          DEBSOL=XDYA(J)*ROS*MUCASO*ALF*(TOEFF(J)-TC(J))**1.5+DEBSOL
          DEBSOL=XDYA(J)*ROS*ALF*(TO(J)-TC(J))**1.5+DEBSOL
        ENDIF
       ENDDO

c      ELSEIF(OPTS.EQ.4.OR.OPTS.EQ.6)THEN
C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C avec correction de la contrainte (contrainte efficace)
c        DO JDEF=1,JDEFMAX
c          J=DEFORM(JDEF)
c        TOEFF(J)=KS1(I)*TO(J)
c        IF(TOEFF(J).GT.TC(J))THEN
c          DEBSOL=XDYA(J)*ROS*MUCASO*ALF*(TOEFF(J)-TC(J))**1.5+DEBSOL
c        ENDIF
c       ENDDO

      ELSEIF(OPTS.EQ.2)THEN
C Capacit� solide par formule de transport total de Engelund & Hansen
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
c        TOEFF(J)=TO(J)
c        IF(TOEFF(J).GT.TC(J))THEN
        IF(TO(J).GT.TC(J))THEN
        DEBSOL=XDYA(J)*ROS*RO**2*0.05*VITLIQ**2
     &        *(TO(J)/ROGRAV)**1.5/(SQRT(GRAV)*D*(ROS-RO)**2)+DEBSOL
C     &        *(TOEFF(J)/ROGRAV)**1.5/(SQRT(GRAV)*D*(ROS-RO)**2)+DEBSOL
        ENDIF
       ENDDO

      ELSEIF(OPTS.EQ.3)THEN
C Capacit� solide par formule de transport total de Bagnold
        VICHUT=VITCHUT(D)
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
C        IF(TOEFF(J).GT.TC(J))THEN
        IF(TO(J).GT.TC(J))THEN
C modif du 17 mai 2011
C ros remplace par ro/rom1
                DEBSOL=XDYA(J)*RO/ROM1*VITLIQ*(TO(J)/ROGRAV)
C                DEBSOL=XDYA(J)*ROS*VITLIQ*(TO(J)/ROGRAV)
     &   *(0.17+0.01*VITLIQ/VICHUT)+DEBSOL
        ENDIF
       ENDDO


      ELSEIF(OPTS.EQ.7)THEN
C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C modifiee Pologne
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C          TOEFF(J)=TO(J)
        IF(TO(J).GT.TC(J))THEN
C        IF(TOEFF(J).GT.TC(J))THEN
C le facteur 8 inclus dans alf passe � 1/0,6**3/2 soit 2,152
C donc facteur multiplicateur 2.152/8=0.269
      DEBSOL=XDYA(J)*ROS*0.269*ALF*(TO(J)-TC(J))**1.5
C      DEBSOL=XDYA(J)*ROS*0.269*MUCASO*ALF*(TOEFF(J)-TC(J))**1.5
     &      +DEBSOL
        ENDIF
        ENDDO
       ELSEIF(OPTS.EQ.9)THEN
C Capacit� solide par formule de Smart et Jaeggi
        DEBSOL=0.
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
          CONTRAINTEEFF=TO(J)
C            TCFM1=SQRT(1./(1.+(JRH2/RH)**2))*(1.-(JRH2/RH)/0.7)*TC(J)
C modif car changement dans shields pour opts=9
            TCFM1=TC(J)
        IF(CONTRAINTEEFF.GT.TCFM1)THEN
c modif ap du 13/9/06 en remplacant d90/d30 par s**2
       DEBSOL=DEBSOL+4.*SACT**0.4*XDYA(J)
     :      *VITLIQ*YH*ROS*(JRH2/RH)**1.6/(ROM1)
     &              *(1.-(TCFM1/CONTRAINTEEFF))
         endif
         ENDDO
      ELSEIF(OPTS.EQ.10)THEN
C Capacit� solide par formule de van Rijn
C sans correction de la contrainte (contrainte efficace)
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.TC(J))THEN
c        CONTRAINTEEFF=ROGRAV*JRH2
c        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(TO(J)/TC(J))-1.
C          TAU0=(TOEFF(J)/TC(J))-1.
C modif du 17 mai 2011 : tauo>3 :formule differente
           IF(TAU0.LT.3.)THEN
          DEBSOL=DEBSOL+XDYA(J)*0.053*TAU0**2.1*DSTAR**(-0.3)
           ELSE
          DEBSOL=DEBSOL+XDYA(J)*0.10*TAU0**1.5*DSTAR**(-0.3)
           ENDIF
c        ELSE
c          DEBSOL=0.
         ENDIF
C fin de la boucle sur jdef
                ENDDO
         DEBSOL=DEBSOL*SQRT(grav*rom1*D**3)
C on calcule la suspension globalement
        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(CONTRAINTEEFF/TCFM)-1.
C CALCUL DES ASPERITES
                    AFOND=MAX(0.01*YH,3.*D)
C CALCUL DE LA CONCENTRATION DU FOND
                    CA=0.015*D*TAU0**1.5/AFOND/DSTAR**0.3
                    AFOND=AFOND/YH
C                DIAMS=D*(1.+0.011*(SIGS-1.)*(TRANSP-25.)
C CALCUL DU PARAMETRE DE SUSPENSION
C formule vitesse de chute pour D>1mm
                    VICHUT=VITCHUT(D)
C VFR vitesse de frottement
                    VFR=SQRT(grav*JRH2)
                    BETA=1.+2.*(ViCHut/VFR)**2
                    PHI=2.5*(ViCHut/VFR)**0.8*(CA/0.65)**0.4
c                    ZED=2.5*ViCHut/VFR/BETA
                    ZED=ViCHut/(VFR*BETA*yh*afond)
                    ZED=ZED+PHI
C si ce parametre est trop grand
                    IF(ZED.GT.1.)ZED=1.
C CALCUL DE LA CONCENTRATINO D'EQUILIBRE EN m3/m3
C si les asperites sont suffisamment petites
                      EF=(AFOND**ZED-AFOND**1.2)/(1.-AFOND)**ZED
     :                 /(1.2-ZED)
C ca est en m3/m3
          DEBSOL=DEBSOL+LACT*CA*EF*VITLIQ*YH
C fin du if sur contrainteeff pour suspension
                ENDIF
C cette ligne me semble a mettre en commentaire
                DEBSOL=ROS*DEBSOL
      ELSEIF(OPTS.EQ.25)THEN
C Capacit� solide par formule de van Rijn suspension
C sans correction de la contrainte (contrainte efficace)
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
C on calcule la suspension globalement
        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(CONTRAINTEEFF/TCFM)-1.
C CALCUL DES ASPERITES
                    AFOND=MAX(0.01*YH,3.*D)
C CALCUL DE LA CONCENTRATION DU FOND
                    CA=0.015*D*TAU0**1.5/AFOND/DSTAR**0.3
                    AFOND=AFOND/YH
C                DIAMS=D*(1.+0.011*(SIGS-1.)*(TRANSP-25.)
C CALCUL DU PARAMETRE DE SUSPENSION
C formule vitesse de chute pour D>1mm
                    VICHUT=VITCHUT(D)
C VFR vitesse de frottement
                    VFR=SQRT(grav*JRH2)
                    BETA=1.+2.*(ViCHut/VFR)**2
                    PHI=2.5*(ViCHut/VFR)**0.8*(CA/0.65)**0.4
c                    ZED=2.5*ViCHut/VFR/BETA
                    ZED=ViCHut/(VFR*BETA*yh*afond)
                    ZED=ZED+PHI
C si ce parametre est trop grand
                    IF(ZED.GT.1.)ZED=1.
C CALCUL DE LA CONCENTRATINO D'EQUILIBRE EN m3/m3
C si les asperites sont suffisamment petites
                      EF=(AFOND**ZED-AFOND**1.2)/(1.-AFOND)**ZED
     :                 /(1.2-ZED)
C ca est en m3/m3
          DEBSOL=LACT*CA*EF*VITLIQ*YH
C fin du if sur contrainteeff pour suspension
                ENDIF
C cette ligne me semble a mettre en commentaire
                DEBSOL=ROS*DEBSOL
      ELSEIF(OPTS.EQ.26)THEN
C Capacit� solide par formule de van Rijn charriage
C sans correction de la contrainte (contrainte efficace)
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.TC(J))THEN
c        CONTRAINTEEFF=ROGRAV*JRH2
c        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(TO(J)/TC(J))-1.
C          TAU0=(TOEFF(J)/TC(J))-1.
C modif du 17 mai 2011 : tauo>3 :formule differente
           IF(TAU0.LT.3.)THEN
          DEBSOL=DEBSOL+XDYA(J)*0.053*TAU0**2.1*DSTAR**(-0.3)
           ELSE
          DEBSOL=DEBSOL+XDYA(J)*0.10*TAU0**1.5*DSTAR**(-0.3)
           ENDIF
c        ELSE
c          DEBSOL=0.
         ENDIF
C fin de la boucle sur jdef
                ENDDO
         DEBSOL=DEBSOL*SQRT(grav*rom1*D**3)
C cette ligne me semble a mettre en commentaire
                DEBSOL=ROS*DEBSOL
      ELSEIF(OPTS.EQ.11)THEN
C Capacit� solide par formule de Rickenmann
C sans correction de la contrainte (contrainte efficace)
          IJ=LDETYJ(1,YH,I)
          LARG=DETL(1,YH,IJ)
          S=SINTER(I)
          FROUDE=VITLIQ/SQRT(GRAV*S/LARG)
c coef vaut (d90/d30)**0.2
                coef=sact**0.4
                IF(coef.lt.1.05)coef=1.05
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.TC(J))THEN
C 0.125 car alf contient 8
          DEBSOL=DEBSOL+XDYA(J)*
C     :(CONTRAINTEEFF-TCFM)*SQRT(CONTRAINTEEFF/ROM1)
     :(To(j)-tc(j))*SQRT(to(j)/ROM1)
           ENDIF
C fin de la boucle sur jdef
              ENDDO
              DEBSOL=DEBSOL*ROS*ALF*0.125*3.1*coef*FRoude**1.1

              ELSEIF(OPTS.EQ.12)THEN
C Capacit� solide par formule de Camenen et Larson
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.eps)THEN
C        IF(TOEFF(J).GT.eps)THEN
          DEBSOL=XDYA(J)*
     :TO(J)**1.5*exp(-4.5*TC(J)/TO(J))+DEBSOL
C     :TOEFF(J)**1.5*exp(-4.5*TC(J)/TOEFF(J))+DEBSOL
        ENDIF
       ENDDO
C *1.5 pour passer de 8 a 12
          DEBSOL=DEBSOL*ROS*ALF*1.5
              ELSEIF(OPTS.EQ.13)THEN
C Capacit� solide par formule de Schoklitsch
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
        COEF=vitliq*(JRH2/RH)**1.5
c d40 approche par d50*(0.75+0.25/s)
         COEF2=0.26*rom1*(JRH2/RH)**0.3333*(0.25*d*(3.+1./sact))**1.5
C la hauteur est ici le rayon hydraulique local
C soit to/rogravj
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
        IF(TO(J).GT.eps)THEN
C        IF(TOEFF(J).GT.eps)THEN
                coef3=coef*to(j)/rogravj-coef2
C                coef3=coef*toeff(j)/rogravj-coef2
             if(coef3.gt.eps)then
            DEBSOL=XDYA(J)*coef3+DEBSOL
             endif
        ENDIF
       ENDDO
           DEBSOL=DEBSOL*2.5*RO/ROS
              ELSEIF(OPTS.EQ.14)THEN
C Capacit� solide par formule de Sato
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
C        IF(TOEFF(J).GT.TC(J))THEN
        IF(TO(J).GT.TC(J))THEN
          DEBSOL=XDYA(J)*
     :TO(J)**1.5*(1.-TC(J)/TO(J))+DEBSOL
C     :TOEFF(J)**1.5*(1.-TC(J)/TOEFF(J))+DEBSOL
        ENDIF
       ENDDO
C *1.25 pour passer de 8 a 10
          DEBSOL=DEBSOL*ROS*ALF*1.25
          if(FR1(I).GT.40.)THEN
                 DEBSOL=DEBSOL*(FR1(I)/40.)**3.5
          ENDIF
              ELSEIF(OPTS.EQ.15)THEN
C Capacit� solide par formule de Recking
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation
C        COEF=D*GRAV*(ROS-RO)
        COEF=D*ROGRAV*ROM1
              COEF2=coef*0.65*(JRH2/RH)**0.41
              coef3=(JRH2/RH)**0.275
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
        IF(TO(J).GT.coef2)THEN
C forte contrainte
C *1.75 pour passer de 8 a 14
          DEBSOL=XDYA(J)*1.75*
     :TO(J)**2.45*COEF**(-0.95)+DEBSOL
C     :TOEFF(J)**2.45*COEF**(-0.95)+DEBSOL
        elseif(to(j).gt.eps)then
                     if(to(j).gt.TC(j)*coef3)THEN
C *1.95 pour passer de 8 a 15.6
          DEBSOL=XDYA(J)*1.95*
     :(TO(J)-TC(J)*coef3)**2*COEF**(-0.5)+DEBSOL
C     :(TOEFF(J)-TC(J)*coef3)**2*COEF**(-0.5)+DEBSOL
                endif
        ENDIF
       ENDDO
          DEBSOL=DEBSOL*ROS*ALF
              ELSEIF(OPTS.EQ.27)THEN
C Capacit� solide par formule de recking 2016
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation faisant intervenir d84
              COEF=D*ROGRAV*ROM1*sact
              COEF2=(D*sact)**3*grav*rom1
C si tcadim est egal a 0.26 la contrainte critique vaut
C tc multiplie par coef3 et par sact etendue
              coef3=(JRH2/RH)**0.3
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
c valeur 0.1 arbitraire pour �viter division par 0
           if(to(j).gt.0.1*TC(j)*coef3)THEN
          DEBSOL=XDYA(J)*(to(j)/coef)**2.5
     :/(1+(tc(j)*coef3*sact/to(j))**10)+DEBSOL
           endif
       ENDDO
          DEBSOL=14*sqrt(coef2)*DEBSOL*ROS
              ELSEIF(OPTS.EQ.28)THEN
C Capacit� solide par formule de piton 2017
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation faisant intervenir d84
              COEF=D*ROGRAV*ROM1*sact
              COEF2=(D*sact)**3*grav*rom1
C si tcadim est egal a 1.5 la contrainte critique vaut
C tc multiplie par coef3 et par sact etendue
              coef3=(JRH2/RH)**0.75
       DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
c valeur 0.1 arbitraire pour �viter division par 0
           if(to(j).gt.0.1*TC(j)*coef3)THEN
          DEBSOL=XDYA(J)*(to(j)/coef)**2.5
     :/(1+(tc(j)*coef3*sact/to(j))**4)+DEBSOL
           endif
       ENDDO
          DEBSOL=14*sqrt(coef2)*DEBSOL*ROS
              ELSEIF(OPTS.EQ.20)THEN
C Capacit� solide par formule de Karim etKennedy
       DO JDEF=1,JDEFMAX
        J=DEFORM(JDEF)
        IF(TO(J).GT.TC(J)+EPS)THEN
          ustar=sqrt(to(j)/ro)
          ustarc=sqrt(tc(j)/ro)
          na=sqrt(grav*rom1*d)
          fa=dlog10(vitliq/na)
          na=dlog10((ustar-ustarc)/na)
          ga=-2.279+2.972*fa+1.06*fa*na+0.299*dlog10(YH/D)*na
          ga=10.**ga*sqrt(grav*rom1*d**3)
          DEBSOL=XDYA(J)*ros*ga+debsol
        ENDIF
       ENDDO
              ELSEIF(OPTS.EQ.21)THEN
C Capacit� solide par formule de Yang et lim
C sans correction de la contrainte (contrainte efficace)
        ustargrain=vitliq/(2.5*DLOG(11*RH/2.*D))
              vichut=vitchut(d)

C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
        DO JDEF=1,JDEFMAX
          J=DEFORM(JDEF)
C        TOEFF(J)=TO(J)
C        IF(TOEFF(J).GT.TC(J))THEN
          ustar=sqrt(tc(j)/ro)
        IF(ustargrain.GT.ustar)THEN
          DEBSOL=XDYA(J)*
     :TO(J)*(ustargrain**2-ustar**2)+DEBSOL
C     :TOEFF(J)**1.5*(1.-TC(J)/TOEFF(J))+DEBSOL
        ENDIF
       ENDDO
          DEBSOL=DEBSOL*12.5*ROS/(RO*vichut*rom1)
      ELSE
        WRITE(*,*)'DEBSOL: loi ',OPTS,' non disponible'
c        PAUSE
        STOP
      ENDIF
C else du if sur capasol.eq.3
      ELSE
        CONTRAINTEEFF=ROGRAV*JRH2
           IF(TAUEFFICACE)THEN
           CONTRAINTEEFF=CONTRAINTEEFF*KS1(I)
           ENDIF
        IF(OPTS.EQ.1)THEN
C        IF(OPTS.EQ.1.OR.OPTS.EQ.5)THEN
C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C sans correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          DEBSOL=LACT*ROS*ALF*(CONTRAINTEEFF-TCFM)**1.5
c        ELSE
c          DEBSOL=0.
        ENDIF
c      ELSEIF(OPTS.EQ.4.OR.OPTS.EQ.6)THEN
C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C avec correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=KS1(I)*ROGRAV*JRH2
c        IF(CONTRAINTEEFF.GT.TCFM)THEN
c          DEBSOL=LACT*ROS*MUCASO*ALF*(CONTRAINTEEFF-TCFM)**1.5
cc        ELSE
cc          DEBSOL=0.
c        ENDIF
      ELSEIF(OPTS.EQ.2)THEN
        IF(YH.GT.EPSY.AND.VITLIQ.GT.EPSY) THEN
C Capacit� solide par formule de transport total de Engelund & Hansen
        DEBSOL=LACT*ROS*RO**2
     &    *0.05*VITLIQ**2*JRH2**1.5/(SQRT(GRAV)*D*(ROS-RO)**2)
c        ELSE
c       DEBSOL=0.
        ENDIF

      ELSEIF(OPTS.EQ.3)THEN
C Capacit� solide par formule de transport total de Bagnold
        VICHUT=VITCHUT(D)
C modif du 17 mai 2011
C ros remplace par ro/rom1
C        DEBSOL=LACT*ROS*VITLIQ*JRH2*(0.17+0.01*VITLIQ/VICHUT)
        DEBSOL=LACT*RO/ROM1*VITLIQ*JRH2*(0.17+0.01*VITLIQ/VICHUT)

      ELSEIF(OPTS.EQ.7)THEN
C Capacit� solide par formule de charriage de Meyer-Peter & M�ller
C modifiee Pologne
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
C le facteur 8 inclus dans alf passe � 1/0,6**3/2 soit 2,152
C donc facteur multiplicateur 2.152/8=0.269
          DEBSOL=LACT*ROS*0.269*ALF*(CONTRAINTEEFF-TCFM)**1.5
c        ELSE
c          DEBSOL=0.
        ENDIF
      ELSEIF(OPTS.EQ.8)THEN

c capacit� solide par Ackers White(1973)
c      IF(YH.LE.EPSY.OR.JRH2.LE.EPSS.OR.VITLIQ.LE.EPSY) THEN
        IF(YH.gt.EPSY.and.JRH2.gt.EPSS.and.VITLIQ.gt.EPSY) THEN
c       DEBSOL=0.
c       ELSE
          Dg=D*(GRAV*ROM1/VISC**2)**(0.333)
                IF(dg.GT.60.)then
                       na=0.
                    ma=1.50
                       aa=0.17
                       ca=0.025
                else
          Na=1-0.56*DLOG10(Dg)
          Ma=1.34+(9.66/Dg)
          Aa=0.14+(0.23/SQRT(Dg))
          Caa=-3.53+2.86*DLOG10(Dg)-(DLOG10(Dg))**2
          Ca=exp(2.3*Caa)
          endif
          Ba=(GRAV*JRH2)**(Na*0.5)/SQRT(GRAV*D*ROM1)
          Fa=Ba*(VITLIQ/(5.66*DLOG10(10*YH/D)))**(1-Na)
          Da=(Fa/Aa)-1
          IF(Da.LE.0)THEN
            Ga=0.
          ELSE
            Ga=Ca*Da**(Ma)
          ENDIF
          DEBSOL=(ROS*Ga*D*(VITLIQ*SINTER(I)/YH))
     & *(VITLIQ/(GRAV*JRH2)**0.5)**(Na)
        ENDIF

      ELSEIF(OPTS.EQ.9)THEN
C Capacit� solide par formule de Smart et Jaeggi
c        CONTRAINTEEFF=ROGRAV*JRH2
C modif car changement dans shields pour opts=9
c         TCFM1=SQRT(1./(1.+(JRH2/RH)**2))*(1.-(JRH2/RH)/0.7)*TCFM
        TCFM1=TCFM
        IF(CONTRAINTEEFF.GT.TCFM1)THEN
c modif ap du 13/9/06 en remplacant d90/d30 par s**2
          DEBSOL=4.*SACT**0.4*LACT
     :      *VITLIQ*YH*ROS*(JRH2/RH)**1.6/(ROM1)
     &              *(1.-(TCFM1/CONTRAINTEEFF))
c      DEBSOL=4.3*LACT*VITLIQ*YH*ROS*(JRH2/RH)**1.6/((ROS/RO)-1.)
c     &              *MUCASO*(1.-(TCFM1/CONTRAINTEEFF))
c        ELSE
c          DEBSOL=0.
        ENDIF

        ELSEIF(OPTS.EQ.10)THEN
C Capacit� solide par formule de van Rijn
C sans correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(CONTRAINTEEFF/TCFM)-1.
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
C modif du 17 mai 2011 : tauo>3 :formule differente
           IF(TAU0.LT.3.)THEN
          DEBSOL=0.053*TAU0**2.1*DSTAR**(-0.3)
           ELSE
          DEBSOL=0.10*TAU0**1.5*DSTAR**(-0.3)
           ENDIF
                DEBSOL=DEBSOL*SQRT(grav*rom1*D**3)
C CALCUL DES ASPERITES
                    AFOND=MAX(0.01*YH,3.*D)
C CALCUL DE LA CONCENTRATION DU FOND
                    CA=0.015*D*TAU0**1.5/AFOND/DSTAR**0.3
                    AFOND=AFOND/YH
C                DIAMS=D*(1.+0.011*(SIGS-1.)*(TRANSP-25.)
C CALCUL DU PARAMETRE DE SUSPENSION
C formule vitesse de chute pour D>1mm
                    VICHUT=VITCHUT(D)
C VFR vitesse de frottement
                    VFR=SQRT(grav*JRH2)
                    BETA=1.+2.*(ViCHut/VFR)**2
                    PHI=2.5*(ViCHut/VFR)**0.8*(CA/0.65)**0.4
                    ZED=ViCHut/(VFR*BETA*yh*afond)
                    ZED=ZED+PHI
C si ce parametre est trop grand
                    IF(ZED.GT.1.)ZED=1.
C CALCUL DE LA CONCENTRATINO D'EQUILIBRE EN m3/m3
C si les asperites sont suffisamment petites
                      EF=(AFOND**ZED-AFOND**1.2)/(1.-AFOND)**ZED
     :                 /(1.2-ZED)
C ca est en m3/m3
          DEBSOL=DEBSOL+CA*EF*VITLIQ*YH
                DEBSOL=LACT*ROS*DEBSOL
c        ELSE
c          DEBSOL=0.
        ENDIF
        ELSEIF(OPTS.EQ.25)THEN
C Capacit� solide par formule de van Rijn suspension
C sans correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(CONTRAINTEEFF/TCFM)-1.
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
C CALCUL DES ASPERITES
                    AFOND=MAX(0.01*YH,3.*D)
C CALCUL DE LA CONCENTRATION DU FOND
                    CA=0.015*D*TAU0**1.5/AFOND/DSTAR**0.3
                    AFOND=AFOND/YH
C                DIAMS=D*(1.+0.011*(SIGS-1.)*(TRANSP-25.)
C CALCUL DU PARAMETRE DE SUSPENSION
C formule vitesse de chute pour D>1mm
                    VICHUT=VITCHUT(D)
C VFR vitesse de frottement
                    VFR=SQRT(grav*JRH2)
                    BETA=1.+2.*(ViCHut/VFR)**2
                    PHI=2.5*(ViCHut/VFR)**0.8*(CA/0.65)**0.4
                    ZED=ViCHut/(VFR*BETA*yh*afond)
                    ZED=ZED+PHI
C si ce parametre est trop grand
                    IF(ZED.GT.1.)ZED=1.
C CALCUL DE LA CONCENTRATINO D'EQUILIBRE EN m3/m3
C si les asperites sont suffisamment petites
                      EF=(AFOND**ZED-AFOND**1.2)/(1.-AFOND)**ZED
     :                 /(1.2-ZED)
C ca est en m3/m3
          DEBSOL=CA*EF*VITLIQ*YH
                DEBSOL=LACT*ROS*DEBSOL
c        ELSE
c          DEBSOL=0.
        ENDIF
        ELSEIF(OPTS.EQ.26)THEN
C Capacit� solide par formule de van Rijn charriage
C sans correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          TAU0=(CONTRAINTEEFF/TCFM)-1.
          Dstar=D*(GRAV*ROM1/visc**2)**0.33333
C modif du 17 mai 2011 : tauo>3 :formule differente
           IF(TAU0.LT.3.)THEN
          DEBSOL=0.053*TAU0**2.1*DSTAR**(-0.3)
           ELSE
          DEBSOL=0.10*TAU0**1.5*DSTAR**(-0.3)
           ENDIF
                DEBSOL=DEBSOL*SQRT(grav*rom1*D**3)
                DEBSOL=LACT*ROS*DEBSOL
c        ELSE
c          DEBSOL=0.
        ENDIF
        ELSEIF(OPTS.EQ.11)THEN
C Capacit� solide par formule de Rickenmann
C sans correction de la contrainte (contrainte efficace)
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.TCFM)THEN
          IJ=LDETYJ(1,YH,I)
          LARG=DETL(1,YH,IJ)
          S=SINTER(I)
          FROUDE=VITLIQ/SQRT(GRAV*S/LARG)
c coef vaut (d90/d30)**0.2
                coef=sact**0.4
                IF(coef.lt.1.05)coef=1.05
C 0.125 car alf contient 8
          DEBSOL=LACT*ROS*ALF*0.125*3.1*coef*
     :(CONTRAINTEEFF-TCFM)*SQRT(CONTRAINTEEFF/ROM1)*FRoude**1.1
           ENDIF
      ELSEIF(OPTS.EQ.12)THEN


C Capacit� solide par formule de Camenen et Larson
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
c        CONTRAINTEEFF=ROGRAV*JRH2
        IF(CONTRAINTEEFF.GT.eps)THEN
C *1.5 pour passer de 8 a 12
          DEBSOL=LACT*ROS*ALF*1.5*
     :CONTRAINTEEFF**1.5*exp(-4.5*TCFM/CONTRAINTEEFF)
c        ELSE
c          DEBSOL=0.
        ENDIF
              ELSEIF(OPTS.EQ.13)THEN
C Capacit� solide par formule de Schoklitsch
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
        COEF=vitliq*(JRH2/RH)**1.5
c d40 approche par d50/etendue granulo**-0.2
C erronne �a modifier
              COEF2=0.26*rom1*(JRH2/RH)**0.3333*(d*sact**(-0.2))**1.5
C la hauteur est ici le rayon hydraulique local
C soit to/rogravj
        IF(CONTRAINTEEFF.GT.eps)THEN
                coef3=coef*CONTRAINTEEFF/rogravj-coef2
             if(coef3.gt.eps)then
            DEBSOL=LACT*coef3
             endif
        ENDIF
           DEBSOL=DEBSOL*2.5*RO/ROS
              ELSEIF(OPTS.EQ.14)THEN
C Capacit� solide par formule de Sato
C sans correction de la contrainte (contrainte efficace)
C       DO JDEF=2,JDEFMAX-1 ! je ne traite pas les deux points extr�mes
        IF(CONTRAINTEEFF.GT.TCFM)THEN
C *1.25 pour passer de 8 a 10
          DEBSOL=LACT*ROS*ALF*1.25*
     :CONTRAINTEEFF**1.5*(1.-TCFM/CONTRAINTEEFF)
        ENDIF
          if(FR1(I).GT.40.)THEN
                 DEBSOL=DEBSOL*(FR1(I)/40.)**3.5
          ENDIF
              ELSEIF(OPTS.EQ.15)THEN
C Capacit� solide par formule de Recking 2008
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation
C        COEF=D*GRAV*(ROS-RO)
        COEF=D*ROGRAV*ROM1
              COEF2=coef*0.65*(JRH2/RH)**0.41
              coef3=(JRH2/RH)**0.275
        IF(CONTRAINTEEFF.GT.coef2)THEN
C forte contrainte
C *1.75 pour passer de 8 a 14
          DEBSOL=lact*1.75*
     :CONTRAINTEEFF**2.45*COEF**(-0.95)
        elseif(CONTRAINTEEFF.gt.eps)then
                     if(CONTRAINTEEFF.gt.TCFM*coef3)THEN
C *1.95 pour passer de 8 a 15.6
          DEBSOL=lact*1.95*
     :(CONTRAINTEEFF-TCFM*coef3)**2*COEF**(-0.5)
             endif
         ENDIF
          DEBSOL=DEBSOL*ROS*ALF
              ELSEIF(OPTS.EQ.27)THEN
C Capacit� solide par formule de recking 2016
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation faisant intervenir d84
              COEF=D*ROGRAV*ROM1*sact
              COEF2=(D*sact)**3*grav*rom1
C si tcadim est egal a 0.26 la contrainte critique vaut
C tcfm multiplie par coef3 et par sact etendue
              coef3=(JRH2/RH)**0.3
c valeur 0.1 arbitraire pour �viter division par 0
           if(contrainteeff.gt.0.1*tcfm*coef3)THEN
              debsol=14*sqrt(coef2)*(contrainteeff/coef)**2.5
     :/(1+(tcfm*coef3*sact/contrainteeff)**10)
           endif
          DEBSOL=DEBSOL*ROS*lact
              ELSEIF(OPTS.EQ.28)THEN
C Capacit� solide par formule de piton 2017
C sans correction de la contrainte (contrainte efficace)
C coefficient d'adimensionnalisation faisant intervenir d84
              COEF=D*ROGRAV*ROM1*sact
              COEF2=(D*sact)**3*grav*rom1
C si tcadim est egal a 1.5 la contrainte critique vaut
C tcfm multiplie par coef3 et par sact etendue
              coef3=(JRH2/RH)**0.75
c valeur 0.1 arbitraire pour �viter division par 0
           if(contrainteeff.gt.0.1*tcfm*coef3)THEN
               debsol=14*sqrt(coef2)*(contrainteeff/coef)**2.5
     :/(1+(tcfm*coef3*sact/contrainteeff)**4)
             endif
          DEBSOL=DEBSOL*ROS*lact
      ELSEIF(OPTS.EQ.16)THEN
c capacit� solide par Brownlie (1981)
          FA=vitliq/sqrt(rom1*grav*d)
          GA=4.596*(JRH2/RH)**(-0.1405)*sact**(-0.1606)
          ga=ga*tcfm**0.5293
          if(fa.lt.GA)then
             debsol=0.
          else
            conc=7115.*(FA-GA)**1.978*(JRH2/RH)**0.6601
     :*(RH/D)**(-0.3301)
C 17 mai 2011 : multiplication par 0.000001 car formule donne en ppm
C coefficient multiplicatif :1.268 en rivi�res et 1 en canal de labo
C pas int�gr�
            DEBSOL=0.000001*conc*VITLIQ*SINTER(I)*ro
c            DEBSOL=conc*VITLIQ*SINTER(I)*ros
          ENDIF
      ELSEIF(OPTS.EQ.17)THEN
c capacit� solide par Laursen (1958)
        ma=0.01724*ro*vitliq**2*(D/YH)**0.33333
C ma contrainte calculee specifiquement (grain)
        IF(ma.gt.tcfm)then
            ustar=sqrt(grav*jrh2)
            ga=ustar/VITCHUT(D)
            na=dlog10(ga)
            if(ga.lt.0.2)then
              fa=1.025+0.245*na
            elseif(ga.lt.2.8)then
              fa=1.162+0.767*na+1.014*na**2+0.789*na**3
            elseif(ga.LT.20.)then
              fa=0.785+2.202*na
            elseif(ga.lt.200.)then
              fa=-2.430+8.271*na-3.370*na**2+0.476*na**3
            else
              fa=3.988+0.250*na
            endif
              conc=0.01*ro*grav*(D/yh)**1.16667*
     :(ma-tcfm)/tcfm*fa
              DEBSOL=conc*VITLIQ*SINTER(I)
c         else
c              debsol=0.
         endif
      ELSEIF(OPTS.EQ.18)THEN
c capacit� solide par Ackers White(1990)
c      IF(YH.LE.EPSY.OR.JRH2.LE.EPSS.OR.VITLIQ.LE.EPSY) THEN
        IF(YH.gt.EPSY.and.JRH2.gt.EPSS.and.VITLIQ.gt.EPSY) THEN
c       DEBSOL=0.
c       ELSE
          Dg=D*(GRAV*ROM1/VISC**2)**(0.333)
                IF(dg.GT.60.)then
                    na=0.
                    ma=1.78
                    aa=0.17
                    ca=0.025
                else
          Na=1-0.56*DLOG10(Dg)
          Ma=1.67+(6.83/Dg)
          Aa=0.14+(0.23/SQRT(Dg))
          Caa=-3.46+2.79*DLOG10(Dg)-0.98*(DLOG10(Dg))**2
          Ca=exp(2.3*Caa)
                endif
          Ba=(GRAV*JRH2)**(Na*0.5)/SQRT(GRAV*D*ROM1)
          Fa=Ba*(VITLIQ/(5.66*DLOG10(10*YH/D)))**(1-Na)
          Da=(Fa/Aa)-1
          IF(Da.LE.0)THEN
            Ga=0.
          ELSE
            Ga=Ca*Da**(Ma)
          ENDIF
          DEBSOL=(ROS*Ga*D*(VITLIQ*SINTER(I)/YH))
     & *(VITLIQ/(GRAV*JRH2)**0.5)**(Na)
        ENDIF
      ELSEIF(OPTS.EQ.19)THEN
c capacit� solide par Karim(1998)
            ustar=sqrt(contrainteeff/ro)
            ga=ustar/VITCHUT(D)
            na=sqrt(grav*rom1*d**3)
            fa=vitliq/sqrt(grav*rom1*d)
            debsol=0.00139*na*fa**2.97*ga**1.47
            debsol=lact*ros*debsol
              ELSEIF(OPTS.EQ.20)THEN
C Capacit� solide par formule de Karim etKennedy
        IF(contrainteeff.GT.TCfm+eps)THEN
               ustar=sqrt(contrainteeff/ro)
               ustarc=sqrt(tcfm/ro)
               na=sqrt(grav*rom1*d)
               fa=dlog10(vitliq/na)
               na=dlog10((ustar-ustarc)/na)
               ga=-2.279+2.972*fa+1.06*fa*na+0.299*dlog10(YH/D)*na
               ga=10.**ga*sqrt(grav*rom1*d**3)
            DEBSOL=lact*ros*ga
        ENDIF
              ELSEIF(OPTS.EQ.21)THEN
C Capacit� solide par formule de Yang et lim
        ustargrain=vitliq/(2.5*DLOG(11*RH/2.*D))
              vichut=vitchut(d)

          ustar=sqrt(tcfm/ro)
        IF(ustargrain.GT.ustar)THEN
          DEBSOL=lact*
     :contrainteeff*(ustargrain**2-ustar**2)
          DEBSOL=DEBSOL*12.5*ros/(vichut*rom1*RO)
        ENDIF

              ELSEIF(OPTS.EQ.22)THEN
C Capacit� solide par formule de Yang (1984)
C sans correction de la contrainte (contrainte efficace)
        vichut=vitchut(D)
C              vichut=vichut*(1.-conc)**0.
C              roliq=ro+(ros-Ro)*conc
C              mustar=exp(5.06*conc)
C              viscm=visc*mustar*ro/roliq
              ustar=sqrt(contrainteeff/ro)
              renpar=ustar*d/visc
              if(renpar.gt.70.)then
                     vitliqcr=2.05
              else
                     vitliqcr=0.66+2.5/(dlog10(ustar*d/visc)-0.06)
              endif
              vichutstar=vichut*d/visc
              powerstar=vitliq*(jrh2/rh)/vichut
              powercr=vitliqcr*(jrh2/rh)
              if(powerstar.GT.powercr)then
      conc=6.681-0.633*dlog10(vichutstar)-4.816*dlog10(ustar/vichut)
     :+(2.784-0.305*dlog10(vichutstar)-0.282*dlog10(ustar/vichut))*
     :dlog10(powerstar-powercr)
           conc=0.000001*10.**conc
           debsol=conc*vitliq*sinter(I)*RO
c              else
c                     debsol=0.
              endif
              ELSEIF(OPTS.EQ.23)THEN
C Capacit� solide par formule de Yang (1979)
C sans correction de la contrainte (contrainte efficace)
        vichut=vitchut(D)
C              vichut=vichut*(1.-conc)**0.
C              roliq=ro+(ros-Ro)*conc
C              mustar=exp(5.06*conc)
C              viscm=visc*mustar*ro/roliq
              ustar=sqrt(contrainteeff/ro)
              renpar=ustar*d/visc
              if(renpar.gt.70.)then
                     vitliqcr=2.05
              else
                     vitliqcr=0.66+2.5/(dlog10(ustar*d/visc)-0.06)
              endif
              vichutstar=vichut*d/visc
              powerstar=vitliq*(jrh2/rh)/vichut
              powercr=vitliqcr*(jrh2/rh)
              if(powerstar.GT.powercr)then
      conc=5.165-0.153*dlog10(vichutstar)-0.297*dlog10(ustar/vichut)
     :+(1.780-0.360*dlog10(vichutstar)-0.480*dlog10(ustar/vichut))*
     :dlog10(powerstar-powercr)
           conc=0.000001*10.**conc
           debsol=conc*vitliq*sinter(I)*RO
c              else
c                     debsol=0.
              endif
              ELSEIF(OPTS.EQ.24)THEN
C Capacit� solide par formule de Yang (1973)
C sans correction de la contrainte (contrainte efficace)
        vichut=vitchut(D)
              ustar=sqrt(contrainteeff/ro)
              renpar=ustar*d/visc
              if(renpar.gt.70.)then
                     vitliqcr=2.05
              else
                     vitliqcr=0.66+2.5/(dlog10(ustar*d/visc)-0.06)
              endif
              vichutstar=vichut*d/visc
              powerstar=vitliq*(jrh2/rh)/vichut
              powercr=vitliqcr*(jrh2/rh)
              if(powerstar.GT.powercr)then
      conc=5.435-0.286*dlog10(vichutstar)-0.457*dlog10(ustar/vichut)
     :+(1.799-0.409*dlog10(vichutstar)-0.314*dlog10(ustar/vichut))*
     :dlog10(powerstar-powercr)
           conc=0.000001*10.**conc
           debsol=conc*vitliq*sinter(I)*RO
c              else
c                     debsol=0.
              endif
      ELSE
        WRITE(*,*)'DEBSOL: loi ',OPTS,' non disponible'
        STOP
      ENDIF

C fin du if sur capasol.eq.3
      ENDIF
      ELSEif(opts.eq.29)then
C cas de opts=29 formule de Lefort 2007
        debit=vitliq*sinter(i)
        if(debit.gt.eps)then
        pente99=max(JRH2/RH,0.0001)
c valeur de dm fantaisiste
        dm=d*(sact+1.)*0.5
        dmstar=dm*(grav*rom1/visc**2)**0.33333
        FRPEAU=21.1*Dm**(-0.1666666667)
        if(d.LT.0.008)then
        coefdm=0.0269+0.532/(1.1+dmstar)-0.0589*exp(-d/60)
        else
        coefdm=0.00269
        endif
        puissancen=-1.725-0.09*dlog10(pente99)
        coef1=fr1(i)/frpeau
        Qcrit=sqrt(grav*D**5)*coefdm*rom1**1.6667*(lact/d)**0.66667
     :*(coef1)**(-0.42)*pente99**puissancen
        puissancem=1.887+0.09*dlog10(pente99)
        reynolds=vitliq*yh/visc
        coefz=0.78+1.53*reynolds**0.14*dmstar**(-0.78)
C if a priori inutile
        if(Qcrit.LT.0.000001)then
c           write(*,*)'Qcrit=',Qcrit
           debsol=0.
        else
        qstar=debit/Qcrit
        if(qstar.GT.2.5)then
          gfonctiondeqstar=3.88*(1.-(0.75/qstar)**0.25)**1.66667
        else
          gfonctiondeqstar=0.4*(qstar/0.25)**(6.25*(1.-0.37*qstar))
        endif
c calcul de cor
        cor=1.
        if(Dm.lt.0.0005)then
          if(vitliq/sqrt(grav*rh).lt.0.5)then
            if(coef1.lt.0.6)then
              cor=1.-0.9*exp(-0.08*qstar*coef1**2.4)
C              cor=1.-1.4*exp(-0.9*coef1**2*sqrt(coef2))
            endif
          endif
        endif
C final avec cor
        debsol=3176*debit*cor*sact**0.21*(rom1+1.)*rom1**(-1.38)
     :*pente99**puissancem*gfonctiondeqstar**coefz
c fin du if sur qcrit
          endif
c fin du if sur debit >0
          endif
      ELSEif(opts.eq.30)then
C cas de opts=30 formule de Lefort 2015 (non fini)
        debit=vitliq*sinter(i)
        if(debit.gt.eps)then
        q=debit/lact
        pente99=max(JRH2/RH,0.0001)
c valeur de dm fantaisiste
        dm=d*(sact+1.)*0.5
        dmstar=dm*(grav*rom1/visc**2)**0.33333
c calcul de coef1
        qstar=q/sqrt(grav*pente99*(dm)**3)
        if(qstar.lt.200.)then
          coef1=0.75*(qstar/200.)**0.23
          coef3=(qstar+2.5)/200.
        else
          coef1=0.75
          coef3=1.
        endif
C si graviers avec limite � 8 mm arbitraire
        if(dm.gt.0.008)then
             coefz=1.
             coefdm=0.0444
          else
          coefz=1.+(0.38*(q/sqrt(grav*dm**3))**0.192*dmstar**(-0.45))
        coefdm=0.0444*(1.+(15./(1.+dmstar))-1.5*exp(-dmstar/75))
          endif
        COEF=sqrt((rom1*dm)**3*grav)
        qcrit=coef*coefdm*(dm/lact)**0.3333/
     :(sqrt(coef1)*pente99**(1.6+0.06*log(pente99)))
c calcul de coef2
C if a priori inutile
       If(qcrit.lt.0.0001)then
           debsol=0.
       else
         coef2=q/qcrit
         if(q.LT.qcrit)then
          gfonctiondeqstar=0.06*coef3*coef2
        else
       gfonctiondeqstar=(6.1*(1.-0.938*coef2**(-0.284))**1.66)**coefz
        endif
c calcul de cor
        cor=1.
        if(Dm.lt.0.0006)then
          if(dmstar.lt.14)then
            if(coef1.lt.0.63)then
              cor=1.-1.4*exp(-0.9*coef1**2*sqrt(coef2))
            endif
          endif
        endif
C formule finale
        puissancem=1.8+0.08*log(pente99)
        debsol=1700*debit*sact**0.2*(rom1+1)*rom1**(-1.65)
     :*pente99**puissancem*cor*gfonctiondeqstar
c fin du if sur qcrit
       endif
c fin du if sur debit>0
       endif
C fin du if sur opts=29 et 30
      endif

c      write(*,*)'debsol',I,capasol, debsol,contrainteeff,rograv,mucaso
      IF(DEBSOL.LT.EPS)THEN
        DEBSOL=0.
      ELSE
        DEBSOL=DEBSOL*SIGNE*MUCASO
      ENDIF

      RETURN
      END
C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION LCHARG(QSAM,CAPSOL,DCHAR,DX)
C-----------------------------------------------------------------------
C Estime le d�bit solide en appliquant la loi de chargement [kg/s]
C-----------------------------------------------------------------------
C QSAM d�bit solide � l'amont (entrant = venant de la maille pr�c�dente + apport lat�ral) [kg/s]
C CAPSOL capacit� de transport sur la maille consid�r�e [kg/s]
C DCHAR distance de chargement [m]
C DX longueur de la maille consid�r�e [m]
      IMPLICIT NONE
      DOUBLE PRECISION QSAM,CAPSOL,DCHAR,DX

C      Write(*,*)'Entr�e LCHARG, QSAM=',QSAM,' CAPSOL=',CAPSOL,' DCHAR='
C     &  ,DCHAR,' DX=',DX
C Test sur la distance de chargement
      IF(DCHAR.LE.0.)THEN
C        WRITE(*,*)'Attention, distance de chargement nulle'
        LCHARG=CAPSOL
        RETURN
      ENDIF

C Loi de chargement lin�aire propos�e par Andr�
C      LCHARG=(CAPSOL*DX+QSAM*DCHAR)/(DX+DCHAR)

C Loi de chargement de type Daubert&Lebreton
C discr�tis�e diff�remment selon le pas d'espace relatif
C modif du 16/5/05 sur les coefficients qui sont arbitraires
C mis � 0,1 au lieu de 0,32 et 10 au lieu de5
      IF(DX.LE.0.1*DCHAR)THEN
          LCHARG=QSAM-(DX/DCHAR)*(QSAM-CAPSOL)
C Forme diff�rentielle: �quation lin�aire
      ELSEIF(DX.GT.10.*DCHAR)THEN
C Forme int�gr�e: exponentielle amortie
          LCHARG=CAPSOL
      ELSE
          LCHARG=CAPSOL+(QSAM-CAPSOL)*EXP(-DX/DCHAR)
      ENDIF

C      IF(LCHARG.LT.0.) LCHARG=0.

C      Write(*,*)'LCHARG=',LCHARG
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION VITCHUT(D)
C-----------------------------------------------------------------------
C Calcule la vitesse de chute (m/s) en fonction du diam�tre D
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION D,DGR
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
c      DOUBLE PRECISION POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      DOUBLE PRECISION ROM1

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM
c      COMMON/CONDEP/POR1,ALF,ROGRAV,DCHARG,RO,ROS,COEF0,COEF1
      COMMON/ROSRO/ROM1
C van Rijn, 1990
c              IF(D.LT.0.0001)THEN !
c        VITCHUT=ROM1*GRAV*D**2/(18.*VISC)
c        ELSEIF(D.LT.0.001)THEN
c        VITchut=10.*VISC/D*
c     & (SQRT(1.+0.01*ROM1*GRAV*D**3/(VISC**2))-1.)
c          ELSE
c          VITchut=1.1*SQRT(ROM1*GRAV*D)
c        ENDIF

C Fisher, 1995
          DGR=d**3*grav*rom1/visc**2
              IF(Dgr.LT.16.187)THEN !
        VITCHUT=ROM1*GRAV*D**2/(18.*VISC)
        ELSEIF(DGR.LT.16187)THEN
        VITchut=10.*VISC/D*
     & (SQRT(1.+0.01*ROM1*GRAV*D**3/(VISC**2))-1.)
          ELSE
          VITchut=1.1*SQRT(ROM1*GRAV*D)
        ENDIF

C Loi de Stockes (�coulement laminaire # d < 0,06 mm)
C       VITCHUT=((ROS-RO)*GRAV*D**2)/(18.*0.0000013*RO)
c      VITCHUT=((ROS-RO)*GRAV*D**2)/(18.*VISC*RO)

C Formule de Lefort (0,04 < d < 4 mm)
C      VITCHUT=1.837*SQRT(DELTA*GRAV*D)
C     &  *(1-EXP(-(D*(DELTA*GRAV/VISC**2)**(1./3.))/10.33))**1.5

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION LARACT(I)
C-----------------------------------------------------------------------
C Calcule la largeur active � l'intermaille
C-----------------------------------------------------------------------
C XL(I)  largeur active lue dans 'litactif.etude', �ventuellement limit�e par la largeur du plein bord (limite min/Moy)
C XL1(I) largeur en eau dans le lit actif � un instant donn�
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER I,LMAX
      PARAMETER(LMAX=3000)
      DOUBLE PRECISION XL(LMAX)
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)

      COMMON/XL/XL
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER

      LARACT=MIN(XL(I),XL1(I))

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETB(Y,J,YF)
C-----------------------------------------------------------------------
C Calcule la pression lat�rale B en Y
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER J,LMAX,LDETYJ,LNCMAX,IJ1,IJ2
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION P1,P2,YF,DETP,Y1,Y2,Y,DETL,DELX
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      DOUBLE PRECISION XTMAIL(LMAX),XCTDF(LMAX)
      EXTERNAL DETP,LDETYJ,DETL

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/XGEOMT/XTMAIL,XCTDF

      IF(Y.LT.EPSY)THEN
        IF(YF.GT.0.)YF=0.
        Y1=YF+CTDF(J)-XCTDF(J-1)
        Y2=YF+CTDF(J)-XCTDF(J)
        IF(Y1*Y2.LT.0.)THEN
          IF(Y1.GT.EPSY)THEN
            P2=0.
            IJ1=LDETYJ(1,Y1,J-1)
            P1=DETP(Y1,IJ1)
          ELSE
            P1=0.
            IJ2=LDETYJ(1,Y2,J)
            P2=DETP(Y2,IJ2)
          ENDIF
          DETB=(P2-P1)/(XTMAIL(J)-XTMAIL(J-1))
        ELSE
          DETB=0.
        ENDIF
      ELSE
C Cas Y non nul
        Y1=Y+CTDF(J)-XCTDF(J-1)
        Y2=Y+CTDF(J)-XCTDF(J)
        IF(Y1.LT.EPSY)THEN
          Y1=0.
          P1=0.
          DELX=(2.*Y+XCTDF(J-1)-XCTDF(J))**2/(8.*Y*PEN(J))
          IJ2=LDETYJ(1,Y2,J)
          P2=DETP(Y2,IJ2)
        ELSE
          IJ1=LDETYJ(1,Y1,J-1)
          P1=DETP(Y1,IJ1)
          IF(Y2.LT.EPSY)THEN
            Y2=0.
            P2=0.
            DELX=-(2.*Y+XCTDF(J)-XCTDF(J-1))**2/(8.*Y*PEN(J))
          ELSE
            IJ2=LDETYJ(1,Y2,J)
            P2=DETP(Y2,IJ2)
            DELX=XTMAIL(J)-XTMAIL(J-1)
          ENDIF
        ENDIF
        DETB=(P2-P1)/DELX
      ENDIF

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETBET(IDS,Y,IJ,I)
C-----------------------------------------------------------------------
C Calcule le coefficient de Boussinesq
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,IJ,IDS
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX),NCM
      DOUBLE PRECISION S,Y,PMOU,SMIN,PMIN,RHMIN,SMOY,RHMOY,COEFA
     :,FR1(LMAX),FRLM(LMAX),HACH,RACIN
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
          DOUBLE PRECISION COEFA1(LMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
          LOGICAL DEBORD,STOCKAGE

      INTEGER LDETYJ
      DOUBLE PRECISION DETSN,DETPM,DETSMI,DETPMI
      EXTERNAL DETSN,DETPM,DETPMI,DETSMI,LDETYJ

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/MAILTN/SN,QN
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/COEFA12/COEFA1
          COMMON/DEBORS/DEBORD,STOCKAGE

      IF(IDS.EQ.0)THEN
        NCM=NCMO(I)
      ELSE
        NCM=XNCMO(I)
      ENDIF
      IF(IJ.LT.NCM)THEN
C Pas de lit Moyen
        DETBET=1.
      ELSE
C mineur + Moyen : correction sur la quantit� de mouvement
        SMIN=DETSMI(IDS,Y,IJ,I)
        IF(SMIN.LT.EPS)THEN
          DETBET=1.
          RETURN
        ENDIF
        S=DETSN(IDS,Y,IJ)
        PMOU=DETPM(IDS,Y,IJ,I)
        PMIN=DETPMI(IDS,Y,IJ,I)
        RHMIN=SMIN/PMIN
        SMOY=S-SMIN
        IF(SMOY.LT.EPS)THEN
          DETBET=1.
          RETURN
        ENDIF
        RHMOY=SMOY/(PMOU-PMIN)
                IF(DEBORD)THEn
c        COEFA1(I)=0.9*(FR1(I)/FRLM(I))**(-0.16667)
        IF(RHMOY.LT.0.3*RHMIN)THEN
          COEFA=0.5*((1.-COEFA1(i))*COS(3.14159/0.3*RHMOY/RHMIN)+
     :    (1.+COEFA1(I)))
        ELSE
                   COEFA=COEFA1(I)
        ENDIF
        RACIN=SMOY**2+SMIN*SMOY*(1.-COEFA**2)
        IF(RACIN.GT.EPS)THEN
          HACH=FRLM(I)*RHMOY**0.6667*SQRT(RACIN)/
     :  (FR1(I)*SMIN*COEFA*RHMIN**0.6667)
        ELSE
          HACH=0.
        ENDIF
        DETBET=S*(1./SMIN+HACH**2/SMOY)/(1.+HACH)**2
C si pas debord
                ELSE
                  if(smoy.gt.EPS)then
          HACH=FRLM(I)*RHMOY**0.6667*SMOY/
     :  (FR1(I)*SMIN*RHMIN**0.6667)
        DETBET=S*(1./SMIN+HACH**2/SMOY)/(1.+HACH)**2
C else du if sur smoy=0
           else
                      detbet=1.
C fin du if sur smoy=0
        ENDIF
C fin du if sur debord
        ENDIF
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETCN(IDS,Y,IJ)
C-----------------------------------------------------------------------
C     Calcul de la c�l�rite des ondes de gravit�
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER IJ,IDS
      DOUBLE PRECISION Y,S
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      DOUBLE PRECISION DETL,DETSN
      EXTERNAL DETL,DETSN

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      S=DETSN(IDS,Y,IJ)
      IF(S.LE.0.)THEN
        DETCN = 0.
        RETURN
      ENDIF
      DETCN = SQRT(GRAV*S/DETL(IDS,Y,IJ))
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETFL(S,V,IJ,I)
C-----------------------------------------------------------------------
C Calcul du flux associ� � la quantit� de mouvement
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER IJ,LMAX,LNCMAX,I
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION S,V,Y
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      DOUBLE PRECISION DETP,DETYN,DETBET
      EXTERNAL DETP,DETYN,DETBET

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      Y=DETYN(1,S,IJ)
      DETFL=DETBET(1,Y,IJ,I)*V*V*S+DETP(Y,IJ)
C      DETFL = V*V*S+DETP(Y,IJ)
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETH(Y,N,M)
C-----------------------------------------------------------------------
C
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,N,N1,K,I,M,NREST
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION Y,DY1,HR,LI,SI,DLDY,DY,NUM,DET
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NC/NC,XNC

      HR=0.
c                                      write(*,*)'entre deth'
c        pause

      IF(Y.LE.0.) GOTO 100
      N1=N
      IF(N.EQ.XNC(M)) N1=XNC(M)-1
      IF(N.GT.XNC(M-1)+1)THEN
        DO I=XNC(M-1)+1,N-1
          LI=XLISEC(I)
          SI=XSECUM(I)
          DLDY=XLYSEC(I)
          DY=(XYISEC(I+1)-XYISEC(I))/100.
          IF(SI.GT.EPS*2)THEN
            HR=HR+.5*DY*SQRT(GRAV*LI/SI)
          ELSE
            NUM=LI+DLDY*(.11*DY)
            DET=SI+LI*(.11*DY)+.00605*DLDY*DY**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
            ENDIF
          ENDIF
          DO K=1,99
            NUM=LI+DLDY*(FLOAT(K)*DY)
            DET=SI+LI*(FLOAT(K)*DY)+0.5*DLDY*(FLOAT(K)*DY)**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+DY*SQRT(GRAV*NUM/DET)
            ENDIF
c            HR=HR+DY*SQRT(GRAV*NUM/DET)
          ENDDO
          NUM=LI+DLDY*100.*DY
          DET=SI+LI*(100.*DY)+DLDY*5000.*DY**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
            ENDIF
C          HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
        ENDDO
      ENDIF
        DY=(XYISEC(N1+1)-XYISEC(N1))/100.
        IF(DY.LT.EPSY)THEN
              NREST=0
            ELSE
              NREST=INT((Y-XYISEC(N))/DY)
            ENDIF
        LI=XLISEC(N)
        SI=XSECUM(N)
        DLDY=XLYSEC(N)

      IF(NREST.NE.0)THEN
        IF(SI.GT.EPS*2)THEN
          HR=HR+.5*DY*SQRT(GRAV*LI/SI)
        ELSE
          NUM=LI+DLDY*(.11*DY)
          DET=SI+LI*(.11*DY)+0.00605*DLDY*DY**2
          IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
            ENDIF
C         HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
        ENDIF
        DO K=1,NREST-1
          NUM=LI+DLDY*(FLOAT(K)*DY)
          DET=SI+LI*(FLOAT(K)*DY)+0.5*DLDY*(FLOAT(K)*DY)**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+DY*SQRT(GRAV*NUM/DET)
            ENDIF
C          HR=HR+DY*SQRT(GRAV*NUM/DET)
        ENDDO
        NUM=LI+DLDY*(NREST*DY)
        DET=SI+LI*(NREST*DY)+.5*DLDY*(NREST*DY)**2
C        HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
              HR=HR+.5*DY*SQRT(GRAV*NUM/DET)
            ENDIF
      ENDIF

      DY1=Y-XYISEC(N)
      IF(N.EQ.(XNC(M-1)+1).AND.NREST.EQ.0)THEN
        IF(SI.GT.EPS*2)THEN
          HR=HR+.5*DY1*SQRT(GRAV*LI/SI)
        ELSE
          NUM=LI+DLDY*(.11*DY1)
          DET=SI+LI*(.11*DY1)+0.00605*DLDY*DY1**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
          HR=HR+.5*DY1*SQRT(GRAV*NUM/DET)
            ENDIF
        ENDIF
        NUM=LI+DLDY*DY1
        DET=SI+LI*DY1+0.5*DLDY*DY1**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
        HR=HR+.5*DY1*SQRT(GRAV*NUM/DET)
            ENDIF
      ELSE
        NUM=LI+DLDY*DY1
        DET=SI+LI*DY1+0.5*DLDY*DY1**2
            IF(DET.GT.EPS.AND.NUM.GT.EPS)THEN
        HR=HR+.5*(DY1-NREST*DY)*SQRT(GRAV*NUM/DET)
            ENDIF
      ENDIF

100   DETH=HR

      RETURN
      END



C--------------------------------------------------
      DOUBLE PRECISION FUNCTION DETL(IDS,Y,IJ)
C--------------------------------------------------
C Renvoie la largeur L(Y)
C--------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION Y
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IF(IDS.EQ.0)THEN
         DETL=LISEC(IJ)+(Y-YISEC(IJ))*LYSEC(IJ)
      ELSE
         DETL=XLISEC(IJ)+(Y-XYISEC(IJ))*XLYSEC(IJ)
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETLMI(IDS,Y,IJ,I)
C-----------------------------------------------------------------------
C D�termination de la largeur L(Y) du lit mineur
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS,I
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION Y
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NCMM/NCMO,XNCMO
      COMMON/NC/NC,XNC

      IF(IDS.EQ.0)THEN
        IF(IJ.LT.NCMO(I))THEN
          DETLMI = LISEC(IJ)+(Y-YISEC(IJ))*LYSEC(IJ)
        ELSE
          IF(NCMO(I).EQ.NC(I))THEN
            DETLMI = LISEC(IJ)+(Y-YISEC(IJ))*LYSEC(IJ)
          ELSE
            DETLMI=LISEC(NCMO(I))
          ENDIF
        ENDIF
      ELSE
        IF(IJ.LT.XNCMO(I))THEN
          DETLMI = XLISEC(IJ)+(Y-XYISEC(IJ))*XLYSEC(IJ)
        ELSE
          IF(XNCMO(I).EQ.XNC(I))THEN
            DETLMI = XLISEC(IJ)+(Y-XYISEC(IJ))*XLYSEC(IJ)
          ELSE
            DETLMI=XLISEC(XNCMO(I))
          ENDIF
        ENDIF
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETP(Z,J)
C-----------------------------------------------------------------------
C D�termination de la pression
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,J
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION Z,LI,YI,SI,PCUM,DLDYI
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      YI=XYISEC(J)
      LI=XLISEC(J)
      SI=XSECUM(J)
      DLDYI=XLYSEC(J)
      PCUM=XPICUM(J)
      DETP=GRAV*(PCUM+0.5*LI*(Z-YI)**2+(DLDYI*((Z-YI)**3)/6.)+(Z-YI)*SI)

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETPM(IDS,Y,IJ,I)
C-----------------------------------------------------------------------
C Renvoie Pm (P�riMouill�) en fonction du tirant d'eau Y et du
C num�ro du couple juste sous la surface IJ
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS,I
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION DELYN,DELLN,Y
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
C variables pour fente de preisman
         logical iffente
         double precision dlfente
         integer ncfente(lmax),xncfente(lmax)

C variables pour fente de preisman
         COMMON/IFFENTE/iffente
         COMMON/LFENTE/dlfente
         COMMON/NCXNCFENTE/ncfente,xncfente

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM

      IF(IDS.EQ.1)THEN
        DELYN=Y-XYISEC(IJ)
        DELLN=DELYN*XLYSEC(IJ)
        DETPM=XPECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
                IF(IFFENTE)THEN
                  if(IJ.GT.xncfente(i))then
                    detpm=xpecum(xncfente(i))
                  endif
C fin du if sur iffente
         endif
      ELSE
        DELYN=Y-YISEC(IJ)
        DELLN=DELYN*LYSEC(IJ)
        DETPM=PECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
                IF(IFFENTE)THEN
                  if(IJ.GT.ncfente(i))then
                    detpm=pecum(ncfente(i))
                  endif
C fin du if sur iffente
         endif
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETPMI(IDS,Y,IJ,I)
C-----------------------------------------------------------------------
C CALCUL DU PERIMETRE MOUILLE du mineur
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS,I
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION DELYN,DELLN,Y
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
C variables pour fente de preisman
         logical iffente
         integer ncfente(lmax),xncfente(lmax)
C variables pour fente de preisman
         COMMON/IFFENTE/iffente
         COMMON/NCXNCFENTE/ncfente,xncfente

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NCMM/NCMO,XNCMO
      COMMON/NC/NC,XNC

      IF(IDS.EQ.1)THEN
        IF(IJ.LT.XNCMO(I))THEN
          DELYN=Y-XYISEC(IJ)
          DELLN=DELYN*XLYSEC(IJ)
          DETPMI = XPECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
        ELSE
          IF(XNC(I).EQ.XNCMO(I))THEN
            DELYN=Y-XYISEC(IJ)
            DELLN=DELYN*XLYSEC(IJ)
            DETPMI = XPECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
          ELSE
            DETPMI=XPECUM(XNCMO(I))
          ENDIF
        ENDIF
                IF(IFFENTE)THEN
                  if(IJ.GT.xncfente(i))then
                    detpmi=min(xpecum(xncfente(i)),xpecum(xncmo(i)))
                  endif
C fin du if sur iffente
         endif
      ELSE
        IF(IJ.LT.NCMO(I))THEN
          DELYN=Y-YISEC(IJ)
          DELLN=DELYN*LYSEC(IJ)
          DETPMI = PECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
        ELSE
          IF(NC(I).EQ.NCMO(I))THEN
            DELYN=Y-YISEC(IJ)
            DELLN=DELYN*LYSEC(IJ)
            DETPMI = PECUM(IJ)+2.*SQRT(.25*DELLN**2+DELYN**2)
          ELSE
            DETPMI=PECUM(NCMO(I))
          ENDIF
        ENDIF
                IF(IFFENTE)THEN
                  if(IJ.GT.ncfente(i))then
                    detpmi=min(pecum(ncfente(i)),pecum(ncmo(i)))
                  endif
C fin du if sur iffente
         endif
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETSMI(IDS,Y,IJ,I)
C-----------------------------------------------------------------------
C Calcul de S du mineur en fonction de Y
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS,I
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION Y,DELYN
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XSECUM(LNCMAX),XLYSEC(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX)
      INTEGER NC(0:LMAX),XNC(0:LMAX)

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NCMM/NCMO,XNCMO
      COMMON/NC/NC,XNC

      IF(IDS.EQ.0)THEN
        IF(IJ.LT.NCMO(I))THEN
          DELYN=Y-YISEC(IJ)
          DETSMI=SECUM(IJ)+DELYN*LISEC(IJ)+0.5*LYSEC(IJ)*DELYN*DELYN
        ELSE
          IF(NC(I).EQ.NCMO(I))THEN
            DELYN=Y-YISEC(IJ)
            DETSMI=SECUM(IJ)+DELYN*LISEC(IJ)+0.5*LYSEC(IJ)*DELYN*DELYN
          ELSE
            DELYN=Y-YISEC(NCMO(I))
            DETSMI=SECUM(NCMO(I))+LISEC(NCMO(I))*DELYN
          ENDIF
        ENDIF
      ELSE
        IF(IJ.LT.XNCMO(I))THEN
          DELYN=Y-XYISEC(IJ)
          DETSMI=XSECUM(IJ)+DELYN*XLISEC(IJ)+0.5*XLYSEC(IJ)*DELYN*DELYN
        ELSE
          IF(XNC(I).EQ.XNCMO(I))THEN
            DELYN=Y-XYISEC(IJ)
            DETSMI=XSECUM(IJ)+DELYN*XLISEC(IJ)+.5*XLYSEC(IJ)*DELYN*DELYN
          ELSE
            DELYN=Y-XYISEC(XNCMO(I))
            DETSMI=XSECUM(XNCMO(I))+XLISEC(XNCMO(I))*DELYN
          ENDIF
        ENDIF
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETSN(IDS,Y,IJ)
C-----------------------------------------------------------------------
C Renvoie S en fonction du tirant d'eau Y et du num�ro du couple
C juste sous la surface IJ
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,IJ,IDS
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION Y,DELYN
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XSECUM(LNCMAX),XLYSEC(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM

      IF(IDS.EQ.0)THEN
        DELYN=Y-YISEC(IJ)
        DETSN=SECUM(IJ)+DELYN*LISEC(IJ)+0.5*LYSEC(IJ)*DELYN*DELYN
      ELSE
        DELYN=Y-XYISEC(IJ)
        DETSN=XSECUM(IJ)+DELYN*XLISEC(IJ)+0.5*XLYSEC(IJ)*DELYN*DELYN
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETYRH(IDS,Y,I)
C-----------------------------------------------------------------------
C Renvoie Rh en fonction de Y dans la section I
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER IDS,I,IJ
      INTEGER LDETYJ
      DOUBLE PRECISION Y
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
      DOUBLE PRECISION DETSN,DETPM
      EXTERNAL DETSN,DETPM,LDETYJ

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IF(Y.GT.EPSY)THEN
        IJ=LDETYJ(IDS,Y,I)
        DETYRH=DETSN(IDS,Y,IJ)/DETPM(IDS,Y,IJ,I)
      ELSE
        DETYRH=0.
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETVMI(IDS,V,Y,IJ,I)
C-----------------------------------------------------------------------
C  vitesse dans le lit mineur
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,IJ,IDS,LDETYJ
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX),NCM
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION S,Y,PMOU,SMIN,PMIN,RHMIN,SMOY,RHMOY,COEFA
     :,FR1(LMAX),FRLM(LMAX),HACH,V,RACIN
      DOUBLE PRECISION COEFA1(LMAX)
      DOUBLE PRECISION DETSN,DETPM,DETSMI,DETPMI
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM
          LOGICAL DEBORD,STOCKAGE
      EXTERNAL DETSN,DETPM,DETPMI,DETSMI,LDETYJ

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/NCMM/NCMO,XNCMO
      COMMON/NC/NC,XNC
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/COEFA12/COEFA1
          COMMON/DEBORS/DEBORD,STOCKAGE

C      IJ=LDETYJ(IDS,Y,I)
      IF(IDS.EQ.1)THEN
        NCM=XNCMO(I)
        IF(NCM.EQ.XNC(I))NCM=NCM+1
      ELSE
        NCM=NCMO(I)
        IF(NCM.EQ.NC(I))NCM=NCM+1
      ENDIF
      IF(IJ.LT.NCM)THEN
        DETVMI=V
        RETURN
      ELSE
        SMIN=DETSMI(IDS,Y,IJ,I)
        IF(SMIN.LT.EPS)THEN
          DETVMI=V
          RETURN
        ENDIF
        S=DETSN(IDS,Y,IJ)
        PMOU=DETPM(IDS,Y,IJ,I)
        PMIN=DETPMI(IDS,Y,IJ,I)
        RHMIN=SMIN/PMIN
        SMOY=S-SMIN
        IF(PMOU.LE.PMIN)THEN
          DETVMI=V
          RETURN
        ELSE
          RHMOY=SMOY/(PMOU-PMIN)
        ENDIF
                IF(DEBORD)THEN
C        COEFA=0.9*(FR1(I)/FRLM(I))**(-0.16667)
        IF(RHMOY.LT.0.3*RHMIN)THEN
          COEFA=0.5*((1.-COEFA1(i))*COS(3.14159/0.3*RHMOY/RHMIN)
     :+(1.+COEFA1(I)))
        ELSE
           COEFA=COEFA1(i)
        ENDIF
C HACH est l'inverse du eta de la formule
        RACIN=SMOY**2+SMIN*SMOY*(1.-COEFA**2)
        IF(RACIN.GT.EPS)THEN
          HACH=FRLM(I)*RHMOY**0.6667*
     :  SQRT(RACIN)/
     :  (FR1(I)*SMIN*COEFA*RHMIN**0.6667)
        ELSE
          HACH=0.
        ENDIF
C else de debord
        else
          HACH=FRLM(I)*RHMOY**0.6667*SMOY/
     :  (FR1(I)*SMIN*RHMIN**0.6667)
c endif de debord
        endif
        DETVMI=V*S/SMIN/(HACH+1.)
C        IF(DETVMI.GT.1.5*V)THEN
C          write(*,*)'detvmi',detvmi,v, y,i
C        ENDIF
        ENDIF
      RETURN
      END

C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DETYN(IDS,S,J)
C-----------------------------------------------------------------------
C D�termination de la hauteur en fonction de la section S
C-----------------------------------------------------------------------
C IDS=0 si centremaille, =1 si intermaille
      IMPLICIT NONE
      INTEGER J,LMAX,IDS ,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000)
      DOUBLE PRECISION LI,YI,S,DELTA,DLDY,SCUM,INTERMEDIAIRE
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IF(S.LT.EPS)THEN
        DETYN=0.
        RETURN
      ENDIF
      IF(IDS.EQ.0)THEN
        LI = LISEC(J)
        YI = YISEC(J)
        SCUM = SECUM(J)
        DLDY = LYSEC(J)
      ELSE
        LI = XLISEC(J)
        YI = XYISEC(J)
        SCUM = XSECUM(J)
        DLDY = XLYSEC(J)
      ENDIF

      IF(ABS(DLDY).GT.EPSY)THEN
              INTERMEDIAIRE=(LI**2)+2.*DLDY*(S-SCUM)
           if(intermediaire.gt.eps)then
        DELTA = SQRT(INTERMEDIAIRE)
        DETYN = YI+((DELTA-LI)/DLDY)
              else
               DETYN = YI+((S-SCUM)/LI)
           endif
c        DELTA = SQRT((LI**2)+2.*DLDY*(S-SCUM))
c        DETYN = YI+((DELTA-LI)/DLDY)
      ELSE
        DETYN = YI+((S-SCUM)/LI)
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION FQINP1(T,IB)
C-----------------------------------------------------------------------
C Calcule le d�bit � la condition limite amont (d�bit impos�)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER NTHMAX,NCLMAX,NBMAX
      PARAMETER(NTHMAX=20000,NCLMAX=1000,NBMAX=150)
      INTEGER NT1(0:NBMAX),NT3(0:NBMAX),I,IB

      DOUBLE PRECISION T
      DOUBLE PRECISION TH(NTHMAX),QTH(NTHMAX),TA(NCLMAX),HIMP(NCLMAX)

      COMMON/HYDAM/TH,QTH
      COMMON/NHYDM/NT1,NT3
      COMMON/HHAM/TA,HIMP

       IF((NT1(IB).EQ.NT1(IB-1)+1).OR.(T.LE.TH(NT1(IB-1)+1))) THEN
         FQINP1=QTH(NT1(IB-1)+1)
       ELSE
           DO 1  I=NT1(IB-1)+2,NT1(IB)
             IF (T.LE.TH(I)) GOTO 10
1          CONTINUE
           FQINP1=QTH(NT1(IB))
           GOTO 101
10     FQINP1=QTH(I-1)+(T-TH(I-1))*(QTH(I)-QTH(I-1))/(TH(I)-TH(I-1))
       ENDIF
101     RETURN
        END


C-----------------------------------------------------------------------
      INTEGER FUNCTION LDETSJ(IDS,S,I)
C-----------------------------------------------------------------------
C Renvoie le num�ro du J�me couple, juste inf�rieur � la surface S
C dans la section I (IDS=0 au centremaille, IDS=1 � l'intermaille)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER I,LMAX,N,IDS,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION DELTA,S
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NC/NC,XNC

      IF(IDS.EQ.0)THEN
        DO 2 N=NC(I-1)+2,NC(I)
          DELTA=S-SECUM(N)
          IF(DELTA.LT.0.)THEN
            LDETSJ=N-1
            RETURN
          ENDIF
2       CONTINUE
        LDETSJ=NC(I)
      ELSE
        DO 3 N=XNC(I-1)+2,XNC(I)
          DELTA=S-XSECUM(N)
          IF(DELTA.LT.0.)THEN
            LDETSJ=N-1
            RETURN
          ENDIF
3       CONTINUE
        LDETSJ = XNC(I)
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      INTEGER FUNCTION LDETYJ(IDS,Y,I)
C-----------------------------------------------------------------------
C Renvoie le num�ro du J�me couple, juste inf�rieur � la cote Y
C dans la section I (IDS=0 au centremaille, IDS=1 � l'intermaille)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER N,LMAX,I,IDS,LNCMAX
      PARAMETER(LMAX=3000,LNCMAX=130000)
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      DOUBLE PRECISION DELTA,Y
      DOUBLE PRECISION LISEC(LNCMAX),YISEC(LNCMAX)
     &  ,LYSEC(LNCMAX),SECUM(LNCMAX),PECUM(LNCMAX)
      DOUBLE PRECISION XLISEC(LNCMAX),XYISEC(LNCMAX)
     &  ,XLYSEC(LNCMAX),XSECUM(LNCMAX),XPICUM(LNCMAX)
     &  ,XPECUM(LNCMAX)

      COMMON/TABGEO/LISEC,YISEC,LYSEC,SECUM,PECUM
      COMMON/XTBGEO/XLISEC,XYISEC,XLYSEC,XSECUM,XPICUM,XPECUM
      COMMON/NC/NC,XNC

      IF(IDS.EQ.0)THEN
        DO 2 N=NC(I-1)+2,NC(I)
          DELTA = Y-YISEC(N)
          IF(DELTA.LT.0.)THEN
            LDETYJ = N-1
            RETURN
          ENDIF
 2      CONTINUE
        LDETYJ = NC(I)
      ELSE
        DO 3 N=XNC(I-1)+2,XNC(I)
          DELTA = Y-XYISEC(N)
          IF (DELTA.LT.0.) THEN
            LDETYJ = N-1
            RETURN
          ENDIF
 3      CONTINUE
        LDETYJ = XNC(I)
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION QSA(I,T,DAL,LACT,QLAT,SACT)
C Calcule le d�bit solide lat�ral
C QSA > 0 si apport; < 0 si soutirage
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,I,J
      PARAMETER(LMAX=3000)
      INTEGER NMU(0:LMAX)
      DOUBLE PRECISION T
     :,DAL,TFAV,LACT,QLAT
     :,JRH(LMAX),KS1(LMAX),SACT
C      INTEGER CAPASOL
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION TSMU(LMAX*20),QSMU(LMAX*20)
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY

      DOUBLE PRECISION DEBSOL,SHIELDS
      DOUBLE PRECISION XL1(LMAX),VINTER(LMAX),SINTER(LMAX),YINTER(LMAX)
     &  ,RHINTER(LMAX)
      EXTERNAL DEBSOL,SHIELDS

      COMMON/MAILTN/SN,QN
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/NTMU/NMU
      COMMON/QTSM/QSMU,TSMU
C      COMMON/CAPASOL/CAPASOL
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/SOLIDE/JRH,KS1
      COMMON/INTER/XL1,VINTER,SINTER,YINTER,RHINTER

      IF(T.LT.TSMU(NMU(I-1)+1))THEN
        QSA=QSMU(NMU(I-1)+1)
      ELSE
        DO J=NMU(I-1)+2,NMU(I)
          IF(T.LT.TSMU(J))THEN
            QSA=QSMU(J-1)*(T-TSMU(J))+QSMU(J)*(TSMU(J-1)-T)
            QSA=QSA/(TSMU(J-1)-TSMU(J))
            GOTO 100
          ENDIF
        ENDDO
        QSA=QSMU(NMU(I))
      ENDIF
 100  CONTINUE
C si QSA est negatif on interprete comme si on entre la capacite solide
C calculee a partir du diametre lateral et de l'ecoulement principal
C et reduite en fonction du rpport des debits lateral/principal
c (si Qlat est negatif on suppose un soutirage a concentration interne dans canoge
c  et on n'entre pas dans cette fonction)
      IF(QSA.LT.-EPS)THEN
        TFAV=SHIELDS(DAL,JRH(I),RHINTER(I))
c        IF(CAPASOL.EQ.1)THEN
          QSA=DEBSOL(I,DAL,TFAV,SACT,LACT)
c        ELSEIF(CAPASOL.EQ.2)THEN
c          QSA=DEBSOL(I,DAL,TFAV,SACT)
c        ELSEIF(CAPASOL.EQ.3)THEN
c          QSA=DEBSOL(I,DAL,TFAV,SACT)
c        ENDIF
        IF(QN(I).GT.EPS)THEN
          QSA=QLAT*QSA/QN(I)
        ELSE
          QSA=0.
        ENDIF
      ELSEIF(QSA.LT.EPS)THEN
        QSA=0.
      ELSE
C Si le fichier d'entr�e �tait en concentrations, on transforme QSA en d�bit solide/m
C car qlat est un debit lineique
        IF(UNISOL.EQ.2)THEN
          QSA=QSA*QLAT
        ENDIF
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
COU1D      DOUBLE PRECISION FUNCTION SGN(X)
C-----------------------------------------------------------------------
C Renvoie de signe d'un nombre
C-----------------------------------------------------------------------
COU1D      IMPLICIT NONE
COU1D      DOUBLE PRECISION X

COU1D      IF(X.LT.0.) SGN=-1.
COU1D      IF(X.EQ.0.) SGN=0.
COU1D      IF(X.GT.0.) SGN=1.
COU1D      RETURN
COU1D      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION SMBQ2(IDS,VVN,I,Y)
C-----------------------------------------------------------------------
C Calcul du second membre frottement
C-----------------------------------------------------------------------
C -SMBQ2 est le frottement(en Strickler sauf si le rayon hydraulique est faible)
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,IJ,IDS,LDETYJ
      DOUBLE PRECISION S,Y,PMOU,SMIN,PMIN,RHMIN,SMOY,RHMOY,COEFA
     :,FR1(LMAX),FRLM(LMAX),DEB,VVN,CHR,RACIN
      DOUBLE PRECISION DETSN,DETPM,DETSMI,DETPMI
      DOUBLE PRECISION CHEZY
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM
      INTEGER NCMO(0:LMAX),XNCMO(0:LMAX),NCM
      INTEGER NC(0:LMAX),XNC(0:LMAX)
      EXTERNAL DETSN,DETPM,DETPMI,DETSMI,LDETYJ
      LOGICAL TRASED,CDCHAR,CGEOM
      INTEGER OPTS,UNISOL,TYPREP,TYPDEF
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     :,TFACT(LMAX)
C D�claration de la constante OptFPc pour le choix de la formule de perte de charge
      INTEGER OptFPC
      DOUBLE PRECISION FR1EQ
      DOUBLE PRECISION COEFA1(LMAX)
          LOGICAL DEBORD,STOCKAGE

      EXTERNAL FR1EQ

C      DOUBLE PRECISION AAA
      COMMON/NC/NC,XNC
      COMMON/NCMM/NCMO,XNCMO
      COMMON/FROTMT/FR1
      COMMON/FROT2/FRLM
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/OSOLID/OPTS,UNISOL,TYPREP,TYPDEF
      COMMON/LSOLID/TRASED,CDCHAR,CGEOM
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/FRMLPC/OptFPC
      COMMON/COEFA12/COEFA1
      COMMON/DEBORS/DEBORD,STOCKAGE


c      Write(*,*)'Entr�e SMBQ2(',IDS,',',VVN,',',I,',',Y,')'
      IF(Y.GT.EPSY)THEN
        IJ=LDETYJ(IDS,Y,I)
        IF(IDS.EQ.1)THEN
          NCM=XNCMO(I)
          IF(NCM.EQ.XNC(I))NCM=NCM+1
        ELSE
          NCM=NCMO(I)
C coorection du 11/3/2005
          IF(NCM.EQ.NC(I))NCM=NCM+1
C          IF(NCM.EQ.XNC(I))NCM=NCM+1
        ENDIF
c      Write(*,*)' marque 1'
        S=DETSN(IDS,Y,IJ)
        PMOU=DETPM(IDS,Y,IJ,I)
        IF(PMOU.LT.EPS)THEN
          SMBQ2=0.
C          WRITE(*,*)'Incoherence maille ',I,' PM=',PMOU,' Y=',Y
          RETURN
        ENDIF
        IF(S.LT.EPS)THEN
          SMBQ2=0.
C          WRITE(*,*)'Incoherence maille ',I,' S=',S,' Y=',Y
          RETURN
        ENDIF
C modification strickler lit mineur en fonction dimaetre
C modif de MPM modifiee POLOGNE
        IF(OPTS.EQ.7)THEN
            FR1(I)=54.58*DACT(I)**(4.53*DACT(I))
        ENDIF
c      Write(*,*)' marque 2'
C
        IF(IJ.GE.NCM)THEN
          PMIN=DETPMI(IDS,Y,IJ,I)
          SMIN=DETSMI(IDS,Y,IJ,I)
          SMOY=S-SMIN
          IF(PMOU-PMIN.LT.EPSY.OR.PMIN.LT.EPSY.OR
     :    .SMIN.LT.EPS.OR.SMOY.LT.EPS)THEN
            RHMIN=S/PMOU
c*************************************************************************
c la fonction de strickler �quivalent FR1EQ n'est appell�e que si OptFPC est non nul
C et si on est au centre de maille ids=0
        if (OptFPC.NE.0)THEN
         IF(IDS.EQ.0)THEN
          FR1(I)=FR1EQ(IDS,I,IJ,S,Y,RHMIN)
         ENDIF
       ENDIF
            CHR=MAX(CHEZY**2,FR1(I)**2*RHMIN**0.333333)
            SMBQ2=S/(CHR*RHMIN)
          ELSE
            RHMIN=SMIN/PMIN
c*************************************************************************
c la fonction de strickler �quivalent FR1EQ n'est appell�e que si OptFPC est non nul
C et si on est au centre de maille ids=0
        if (OptFPC.NE.0)THEN
         IF(IDS.EQ.0)THEN
          FR1(I)=FR1EQ(IDS,I,IJ,S,Y,RHMIN)
         ENDIF
       ENDIF
            RHMOY=SMOY/(PMOU-PMIN)
       IF(DEBORD)THEN
            COEFA1(I)=0.9*(FR1(I)/FRLM(I))**(-0.16667)
            IF(RHMOY.LT.0.3*RHMIN)THEN
          COEFA=0.5*((1.-COEFA1(I))*COS(3.14159/0.3*RHMOY/RHMIN)
     :+(1.+COEFA1(i)))
                else
              COEFA=COEFA1(I)
            ENDIF
            RACIN=SMOY**2+SMIN*SMOY*(1.-COEFA**2)
            IF(RACIN.GT.EPS)THEN
              DEB=FR1(I)*SMIN*COEFA*RHMIN**0.6667+FRLM(I)*RHMOY**0.6667
     :      *SQRT(RACIN)
              SMBQ2=S**3/DEB**2
            ELSE
              CHR=MAX(CHEZY**2,FR1(I)**2*RHMIN**0.333333)
              SMBQ2=S/(CHR*RHMIN)
            ENDIF
C else de debord
                else
            IF(SMOY.GT.EPS)THEN
              DEB=FR1(I)*SMIN*RHMIN**0.6667+FRLM(I)*RHMOY**0.6667
     :      *SMOY
              SMBQ2=S**3/DEB**2
            ELSE
              CHR=MAX(CHEZY**2,FR1(I)**2*RHMIN**0.333333)
              SMBQ2=S/(CHR*RHMIN)
            ENDIF
C endif de debord
        endif

          ENDIF
C Il n'y que le lit mineur
        ELSE
          RHMIN=S/PMOU
c*************************************************************************
c la fonction de strickler �quivalent FR1EQ n'est appell�e que si OptFPC est non nul
C et si on est au centre de maille ids=0
        if (OptFPC.NE.0)THEN
         IF(IDS.EQ.0)THEN
          FR1(I)=FR1EQ(IDS,I,IJ,S,Y,RHMIN)
         ENDIF
       ENDIF
          CHR=MAX(CHEZY**2,FR1(I)**2*RHMIN**0.333333)
          SMBQ2=S/(CHR*RHMIN)
        ENDIF
c      Write(*,*)' marque 11'
        SMBQ2=-GRAV*VVN*ABS(VVN)*SMBQ2
C Y est nul a epsy pr�s
      ELSE
        SMBQ2=0.
      ENDIF
      RETURN
      END



C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION SMBS(MUI,Y,ZYDI,EXPOSI,I,T)
C-----------------------------------------------------------------------
C Calcul du second membre (S) avec deversoir
C-----------------------------------------------------------------------
C Modif PiB6mars2001: ZYDI est une cote (plus une hauteur) afin de traiter le cas o� la prise est ensabl�e
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      DOUBLE PRECISION MUI,Y,ZYDI,GRAV,EPS,EPSY,EPSM,CHEZY,EXPOSI,QMUI
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)
      INTEGER K,I,NMU(0:LMAX)
      DOUBLE PRECISION TMU(LMAX*20),QMU(LMAX*20),T,R2G,R32,UND
      DOUBLE PRECISION QLAT(LMAX)
      DOUBLE PRECISION OUV(LMAX),LOND(LMAX),COEFA(LMAX),COEFB(LMAX)
     &  ,EFFPRI(LMAX),OUVI
      INTEGER NITER
      DOUBLE PRECISION Q1,Q2,H1,H2,H2N,H2M,H2P,MU0,MUF
      DOUBLE PRECISION SMBS1

      DOUBLE PRECISION DEBIP1,DEBIP2
      EXTERNAL DEBIP1,DEBIP2

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/TQMU/TMU,QMU
      COMMON/NTMU/NMU
C QLAT debit evacue par les prises
      COMMON/QL/QLAT
      COMMON/PRISE/OUV,LOND,COEFA,COEFB,EFFPRI

      R2G=SQRT(2.*GRAV)
      R32=1.5*SQRT(3.)
      UND=1.5

C      IF(ABS(EXPOSI).LT.EPS)THEN
C On a un d�bit lin�aire d'apport fonction ou non du temps
C        QLAT(I)=0.
        IF(NMU(I).EQ.NMU(I-1))THEN
          SMBS=0.
        ELSE
          DO K=NMU(I-1)+1,NMU(I)
            IF(T.LT.TMU(K))THEN
              IF(K.NE.NMU(I-1)+1)THEN
          QMUI=QMU(K-1)+(T-TMU(K-1))*(QMU(K)-QMU(K-1))/(TMU(K)-TMU(K-1))
              ELSE
                QMUI=QMU(K)
              ENDIF
              GOTO 2
            ENDIF
          ENDDO
          QMUI=QMU(NMU(I))
 2        CONTINUE
          SMBS=QMUI
c          QLAT(I)=SMBS
        ENDIF

C Ouvrage de prise T1
C--------------------
c      ELSEIF(ABS(EXPOSI+1.).LT.EPS)THEN
      IF(ABS(EXPOSI+1.).LT.EPS)THEN
        H1=Y+CTDF(I)-ZYDI
        OUVI=OUV(I)
C        IF(CTDF(I).GE.(ZYDI+OUVI))THEN
CC Ensablement complet de la prise
C          SMBS=0.
C          QLAT(I)=0.
C          RETURN
C        ELSEIF(CTDF(I).GE.ZYDI)THEN
CC Ensablement partiel de la prise
C          OUVI=OUVI-(CTDF(I)-ZYDI)
C        ENDIF
C D�bit maximum
        Q1=MUI*R2G*H1**UND
        H2P=COEFA(I)*Q1**COEFB(I)
        H2M=0.
        NITER=0
  3     CONTINUE
        IF(NITER.LT.20)THEN
          NITER=NITER+1
          H2N=0.5*(H2M+H2P)
          Q2=DEBIP1(R2G,R32,UND,H1,H2N,MUI,OUVI)
          H2=COEFA(I)*Q2**COEFB(I)
          IF(ABS(Q1-Q2).LT.EPS)THEN
            SMBS1=0.5*(Q1+Q2)
          ELSE
            Q1=Q2
            IF(H2.GT.H2N)THEN
              H2M=H2N
            ELSE
              H2P=H2N
            ENDIF
            GOTO 3
          ENDIF
        ELSE
          SMBS1=0.5*(Q1+Q2)
        ENDIF
        SMBS=SMBS-SMBS1*LOND(I)
c        QLAT(I)=SMBS

C Ouvrage de prise T2
C--------------------
      ELSEIF(ABS(EXPOSI+2.).LT.EPS)THEN
        H1=Y+CTDF(I)-ZYDI
        OUVI=OUV(I)
C        IF(CTDF(I).GE.(ZYDI+OUVI))THEN
CC Ensablement complet de la prise
C          SMBS=0.
C          QLAT(I)=0.
C          RETURN
C        ELSEIF(CTDF(I).GE.ZYDI)THEN
CC Ensablement partiel de la prise
C          OUVI=OUVI-(CTDF(I)-ZYDI)
C        ENDIF
        MU0=2.*MUI/3.
        MUF=MU0-0.08
C debit maximum
        Q1=MUF*R2G*H1**UND
        H2P=COEFA(I)*Q1**COEFB(I)
        H2M=0.
        NITER=0
  4     CONTINUE
        IF(NITER.LT.20)THEN
          NITER=NITER+1
          H2N=0.5*(H2M+H2P)
          Q2=DEBIP2(R2G,UND,H1,H2N,MU0,OUVI)
          H2=COEFA(I)*Q2**COEFB(I)
          IF(ABS(Q1-Q2).LT.EPS)THEN
            SMBS1=0.5*(Q1+Q2)
          ELSE
            Q1=Q2
            IF(H2.GT.H2N)THEN
              H2M=H2N
            ELSE
              H2P=H2N
            ENDIF
            GOTO 4
          ENDIF
        ELSE
          SMBS1=0.5*(Q1+Q2)
        ENDIF
        SMBS=SMBS-SMBS1*LOND(I)
c        QLAT(I)=SMBS

C D�versoir classique
      ELSEIF(EXPOSI.GT.EPS)THEN
                IF(Y.GT.(ZYDI-CTDF(I)))THEN
        SMBS=SMBS-MUI*((Y+CTDF(I)-ZYDI)**EXPOSI)*(R2G)
             endif
         else
C mui contient ici le debit lateral
        SMBS=SMBS-MUI

C        QLAT(I)=0.
c        QLAT(I)=SMBS
c      ELSE
c        SMBS=0.
c        QLAT(I)=0.
      ENDIF
      QLAT(I)=SMBS

      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DEBIP1(R2G,R32,UND,H1,H2,MUI,OUVI)
C-----------------------------------------------------------------------
C D�bit aux prises T1
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION R2G,R32,UND,H1,H2,MUI,OUVI
      LOGICAL SURFLIB,DENOYE,PARTIEL

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IF(OUVI.LT.EPS)THEN
         DEBIP1=0.
         RETURN
      ELSEIF(H2.GE.H1)THEN
         DEBIP1=0.
         RETURN
      ELSEIF(H1.LT.EPS)THEN
         DEBIP1=0.
         RETURN
      ELSEIF(H1.LE.OUVI)THEN
         SURFLIB=.TRUE.
      ELSE
         SURFLIB=.FALSE.
      ENDIF
      IF(H2*UND.LT.H1)THEN
         DENOYE=.TRUE.
      ELSE
         DENOYE=.FALSE.
         IF(H2*UND.LT.H1+0.5*OUVI)THEN
           PARTIEL=.TRUE.
         ELSE
           PARTIEL=.FALSE.
         ENDIF
      ENDIF
C calcul du debit
      IF(SURFLIB.AND.DENOYE)THEN
        DEBIP1=MUI*R2G*H1**UND
      ELSEIF(SURFLIB.AND..NOT.DENOYE)THEN
        DEBIP1=R32*MUI*R2G*H2*SQRT(H1-H2)
      ELSEIF(.NOT.SURFLIB.AND.DENOYE)THEN
        DEBIP1=MUI*R2G*(H1**UND-(H1-OUVI)**UND)
      ELSEIF(.NOT.SURFLIB.AND..NOT.DENOYE)THEN
        IF(PARTIEL)THEN
          DEBIP1=MUI*R2G*(R32*H2*SQRT(H1-H2)-(H1-OUVI)**UND)
        ELSE
          DEBIP1=R32*MUI*R2G*OUVI*SQRT(H1-H2)
        ENDIF
      ENDIF
      RETURN
      END


C-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION DEBIP2(R2G,UND,H1,H2,MUI,OUVI)
C-----------------------------------------------------------------------
C D�bit aux prises T2
C-----------------------------------------------------------------------
      IMPLICIT NONE
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY
      DOUBLE PRECISION R2G,UND,H1,H2,MUI,OUVI
      DOUBLE PRECISION MUF,ALFA,MU,MU1,BETA,KF,ALFA1,X,KF1
      LOGICAL SURFLIB,DENOYE,PARTIEL

      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

      IF(OUVI.LT.EPS)THEN
        DEBIP2=0.
        RETURN
      ELSEIF(H2.GE.H1)THEN
        DEBIP2=0.
        RETURN
      ELSEIF(H1.LT.EPS)THEN
        DEBIP2=0.
        RETURN
      ELSEIF(H1.LE.OUVI)THEN
        SURFLIB=.TRUE.
        MUF=MUI-0.08
        ALFA=0.75
      ELSE
        SURFLIB=.FALSE.
        MU=MUI-0.08*OUVI/H1
        MU1=MUI-0.08*OUVI/(H1-OUVI)
        ALFA=1.-0.14*H2/OUVI
        IF(ALFA.LT.0.4)THEN
          ALFA=0.4
        ELSEIF(ALFA.GT.0.75)THEN
          ALFA=0.75
        ENDIF
      ENDIF
      IF(H2.LT.ALFA*H1)THEN
        DENOYE=.TRUE.
      ELSE
        DENOYE=.FALSE.
        X=SQRT(1.-h2/H1)
        BETA=-2.*ALFA+2.6
        IF(X.GT.0.2)THEN
          KF=1.-(1. - X/SQRT(1.-ALFA))**BETA
        ELSE
          KF=5.*x*(1.-(1.-0.2/SQRT(1.-ALFA))**BETA )
        ENDIF
        ALFA1=1.-0.14*(H2-OUVI)/OUVI
        IF(ALFA1.LT.0.4)THEN
          ALFA1=0.4
        ELSEIF(ALFA1.GT.0.75)THEN
          ALFA1=0.75
        ENDIF
        IF(H2.LT.ALFA1*H1+(1.-ALFA1)*OUVI)THEN
          PARTIEL=.TRUE.
        ELSE
          PARTIEL=.FALSE.
        ENDIF
      ENDIF
C calcul du debit
      IF(SURFLIB.AND.DENOYE)THEN
        DEBIP2=MUF*R2G*H1**UND
      ELSEIF(SURFLIB.AND..NOT.DENOYE)THEN
        DEBIP2=KF*MUF*R2G*H1**UND
      ELSEIF(.NOT.SURFLIB.AND.DENOYE)THEN
        DEBIP2=R2G*(MU*H1**UND-MU1*(H1-OUVI)**UND)
      ELSEIF(.NOT.SURFLIB.AND..NOT.DENOYE)THEN
        IF(PARTIEL)THEN
          DEBIP2=R2G*(KF*MU*H1**UND-MU1*(H1-OUVI)**UND)
        ELSE
          X=SQRT(1.-(H2-OUVI)/(H1-OUVI))
          BETA=-2.*ALFA1+2.6
          IF(X.GT.0.2)THEN
            KF1=1.-(1. - X/SQRT(1.-ALFA1))**BETA
          ELSE
            KF1=5.*x*(1.-(1.-0.2/SQRT(1.-ALFA1))**BETA )
          ENDIF
          DEBIP2=R2G*(KF*MU*H1**UND-KF1*MU1*(H1-OUVI)**UND)
        ENDIF
      ENDIF
      RETURN
      END
c-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION FR1EQ(IDS,I,IJ,S,Y,RHMIN)
C-----------------------------------------------------------------------
C     Calcul de Strickler �quivalent
C on appelle cette fonction que si Y est superieur a EPSY
C et si IDS est nul (centre maille)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NBMAX
      PARAMETER(LMAX=3000,NBMAX=150)
      INTEGER I,IJ,IDS
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION DETSN,DETPM,DETSMI,DETPMI,DETL,SENG,FRDE
      DOUBLE PRECISION S,Y,RHMIN, SENG2
     :,FR1(LMAX)
      DOUBLE PRECISION MACT(LMAX),DACT(LMAX),SACT(LMAX),TMACT(LMAX)
     :,TFACT(LMAX)
      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION T0,LrgM,Frdg,AlphaLR,AlphaBR
      DOUBLE PRECISION VITCHUT,INTERMEDIAIRE

         DOUBLE PRECISION HALFA,MUCASO,VISC,TCADIM
         DOUBLE PRECISION GRAV,EPS,EPSY,EPSM, CHEZY
      INTEGER OptFPC
      INTEGER LM(0:NBMAX),LL
      DOUBLE PRECISION TMAIL(LMAX),CTDF(LMAX),PEN(LMAX)

      DOUBLE PRECISION Frd,Frd1,Frd2,Karim,karimtr,Dg,Tc,Tg,T,J,V
      DOUBLE PRECISION Ki,Qci,LimtA,LimtB,Lamda
      DOUBLE PRECISION DM,segma,D84,DETVMI,ROM1,KA,FR1EQ1,CGR,FP

      EXTERNAL DETSN,DETPM,DETPMI,DETSMI,DETL,DETVMI
      EXTERNAL VITCHUT,SENG,SENG2

      COMMON/PHYS/LM,LL
      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/FROTMT/FR1
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM
      COMMON/MODGEO/MACT,DACT,SACT,TMACT,TFACT
      COMMON/FRMLPC/OptFPC
      COMMON/rosro/rom1
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/ABN/HALFA,MUCASO,VISC,TCADIM

c       IJ=LDETYJ(IDS,Y,I)
c       S=DETSN(IDS,Y,IJ)
       DM=DACT(I)
       SEGMA=SACT(I)
C mis en commentaire ici
C on utilise plus loin la fonction detvmi
c       if(IDS.EQ.0)then
c       V=VN(I)
c       elseif(I.EQ.1)then
c       V=VN(I)
c       elseif(I.EQ.LL)then
c       V=VN(LM)
c       ELSE
c       V=0.5*(QN(I)+QN(I+1))/S
c       endif

c*****************************************************************************
c optFPC=1: correspond � la formule de Brownlie 1983
       if(OptFPC.EQ.1)then
C nombre de froude associ� au grain solide tq roM1=(ros-Ro)/Ro
       V=abs(DETVMI(IDS, VN(I),Y,IJ,I))
       Frdg =V/SQRT(ROM1*GRAV*DM)
C la pente de frottement calcule par Strickler precedent
       J=SENG2(IDS,I,RHMIN)
C la pente d'energie calculee par la difference de charge
c        J=SENG(I)
C (To=Fg*) qui limite les deux r�gimes
C on introduit une transition sur J entre 0.006 et 0.007
C       If(J.LT.0.006) then
       If(J.LT.0.007) then
         T0=1.74*J**(-0.33333)
         If(Frdg.LT.T0) then
       FR1EQ=6.9791*(GRAV**0.643095)*(RHMIN**0.014)/
     &((Segma**0.20653)*(DM**0.03766)*(V**0.28619))
C transition
       If(J.GT.0.006) then
      FR1EQ1=30.87145/((RHMIN**0.01451)*
     & (Segma**0.13926)*(DM**0.10914)*(V**0.08585))
       FR1EQ=1000.*(FR1EQ*(J-0.006)+FR1EQ1*(0.007-J))
       ENDIF
C si frdg plus grand que to
                 ELSE
                 KA=FRDG/T0
C ka compris entre 1 et 1.1 : rajoute pour continuite
           IF(KA.LT.1.1)THEN
       FR1EQ=6.9791*(GRAV**0.643095)*(RHMIN**0.014)/
     &((Segma**0.20653)*(DM**0.03766)*(V**0.28619))
      FR1EQ1=30.87145/((RHMIN**0.01451)*
     & (Segma**0.13926)*(DM**0.10914)*(V**0.08585))
          FR1EQ=10.*(FR1EQ*(1.1-KA)+(KA-1.)*FR1EQ1)
C transition
       If(J.GT.0.006) then
       FR1EQ=1000.*(FR1EQ*(J-0.006)+FR1EQ1*(0.007-J))
       ENDIF
C si ka superieur a 1.1
                else
      FR1EQ=30.87145/((RHMIN**0.01451)*
     & (Segma**0.13926)*(DM**0.10914)*(V**0.08585))
C fin du if sur ka
              endif
C fin du if sur frdg et to
         endif
C si j plus grand que 0.007
       else
      FR1EQ=30.87145/((RHMIN**0.01451)*
     & (Segma**0.13926)*(DM**0.10914)*(V**0.08585))
       endif
c*****************************************************************************
c optFPC=2: correspond � la formule de Griffiths 1981  fond mobile
      ELSEIf(OptFPC.EQ.2)then
       V=abs(DETVMI(IDS, VN(I),Y,IJ,I))
      FR1EQ=13.2796*(V**0.34)/((RHMIN**0.1666)*
     &(DM**0.17))
c*****************************************************************************
c optFPC=3: correspond � la formule de Karim 1995
      elseIf(OptFPC.EQ.3)then
C nombre de Froude lit mineur
              Frd =abs(FRDE(IDS,I,Y,S,IJ))
C limite sup�rieure du lower regime
        Frd1=2.716*(Y/DM)**(-0.25)
C limite inf�rieure  du upper regime
        Frd2=4.785*(Y/DM)**(-0.27)

              If(Frd.GT.Frd2)then
C pour le upper regime la hauteur relative des formes est nulle f/f0=1.2
          karim=1.20
              else
C calcul de la vitesse de chute de d50 remplace par appel fonction
c              IF(DM.LT.0.0001)THEN !
c        Uchut=(1.65)*GRAV*DM**2/(18.*0.0000013) ! vitesse de chute
c        ELSEIF(DM.LT.0.001)THEN
c        Uchut=10.*0.0000013/DM*
c     & (SQRT(1.+0.01*(1.65)*GRAV*DM**3/(0.0000013**2))-1.)
c          ELSE
c          Uchut=1.1*SQRT((1.65)*GRAV*DM)
c        ENDIF
C la pente de frottement calcule par Strickler precedent
       J=SENG2(IDS,I,RHMIN)
C la pente d'energie calculee par la difference de charge
c        J=SENG(I)
        T0=SQRT(GRAV*RHMIN*J)/VITCHUT(DM)
              If((T0.LT.3.665).AND.(T0.GT.0.136))then
C              If((T0.LT.3.64).AND.(T0.GT.0.15))then
      Karim=1.20+8.92*(-0.04+0.294*T0+0.00316*T0**2-0.0319*T0**3+
     &  0.00272*T0**4)
              else
              karim=1.20
C fin du if sur T0
              endif
              if (Frd.GT.Frd1.and.frd.LT.frd2) then
C karimtr c'est le f/f0 calc par la relation lineaire
                karimtr=1.20+8.92*(0.20*((Frd2-Frd)/(Frd2-Frd1)))
C f/f0 est pris le minimum entre la karimtr et karim(T0)
                if (karimtr.LT.karim)karim =karimtr
C fin du if sur frd1
              Endif
C fin du if sur frd2
        endif
              FR1EQ=1./(0.037*(DM**0.126)*karim**0.465)
c*****************************************************************************
c optFPC=4: correspond � la formule de Wu et Wang 1999
              elseIf (optFPC.EQ.4)then
C nombre de Froude lit mineur
        Frd =abs(FRDE(IDS,I,Y,S,IJ))
C diam�tre adimensionnel des grains
        Dg=DM*((ROM1*GRAV)/(0.000001**2))**(0.3333)
c calcul de la contrainte critique divisee par 1000 grav
               if(Dg.LT.1.5)then
               Tc=ROM1*DM*(0.126*Dg**(-0.44))
               elseif(Dg.LT.10)then
               Tc=ROM1*DM*(0.131*Dg**(-0.55))
               elseif(Dg.LT.20)then
                Tc=ROM1*DM*(0.0685*Dg**(-0.27))
               elseif (Dg.LT.40)then
               Tc=ROM1*DM*(0.0173*Dg**(0.19))
               elseif (Dg.LT.150) then
               Tc=ROM1*DM*(0.0115*Dg**(0.30))
C               elseif(Dg.GT.150.)then
            else
               Tc=ROM1*DM*(0.052)
               endif
C la pente de frottement calcule par Strickler precedent
       J=SENG2(IDS,I,RHMIN)
C la pente d'energie calculee par la difference de charge
c        J=SENG(I)
c         if (J.GT.PEN(I))J=PEN(I)
C contrainte de frottement relative aux grains divisee par 1000 grav
         Tg=(((Fr1(i)*DM**(0.1666))/21.)**(1.5))*J*RHMIN
         T=Tg/Tc
c        WRITE (*,*)'FR=',Frd,'V=',v,'k=',fr1(i),'j=',J
c        WRITE (*,*)'Tg',Tg,'  i=',i,'  J',J,'  Rh',RHMIN,'V=',v
c        if ((T.GT.1).and.(T.LT.55))then
        if (T.GT.1)then
        FR1EQ=((SQRT(GRAV)*Frd**(0.3333))/(DM**(0.1666)))*(10**
     & (0.911-0.273*dlog10(T)-0.051*(dlog10(T))**2+
     & 0.135*(dlog10(T))**3))
        else
        FR1EQ=((SQRT(GRAV)*Frd**(0.3333))/(DM**(0.1666)))*10**
     & 0.911
C  formule de strickler pour le frottement de grain
c        FR1EQ=21./(DM**0.166666)
C fin du if sur T
        endif
c        WRITE (*,*)'T=',T,'  Kr',Fr1(i),'  Keq',FR1EQ,'   a=',a
c*****************************************************************************
c optFPC=5: correspond � la formule de Yu et Lim 2003
      elseIf(OptFPC.EQ.5)then
C la pente de frottement calcule par Strickler precedent
       J=SENG2(IDS,I,RHMIN)
C la pente d'energie calculee par la difference de charge
c        J=SENG(I)
C  diam�tre adimensionnel des grains
      Dg=DM*((ROM1*GRAV)/(0.000001**2))**(0.3333)
C param�tre de Shields (J est prise �gale � la pente g�om�trique)
      T=RHMIN*J/(ROM1*DM)
C param�tre critique de Shield reformul� par Yu et Lim 2003
      Tc=0.056-0.033*exp(-0.0115*Dg)+0.1*exp(-0.25*Dg)+exp(-2*Dg)
C      Ki=log(T/Tc)
c si ki<0 le fond est plat sans transport, le strcikler ne change pas
c      if (ki.LE.0)then
      if(T.le.tc)then
      FR1EQ=21/DM**0.16666
      else
      Ki=dlog(T/Tc)
c calcul de Qci
c      If ((RHMIN/DM).LE.(300*Ki).and.Ki.GT.0) then
      If ((RHMIN/DM).LE.(300.*Ki)) then
      Qci=(300.*Ki*DM/RHMIN)**0.1
      else
      Qci=1.
      endif
C largeur au miroir
       LrgM=DETL(IDS,Y,IJ)
c calcul de lamda pour chaque r�gime d'�coulement
       V=abs(DETVMI(IDS, VN(I),Y,IJ,I))
      LimtA=1000*J*(V*S/(LrgM*sqrt(ROM1*GRAV*DM**3)))**0.2*segma**0.2
      if (Dg.LT.14.)then
      LimtB=0.2413*Dg**2-2.385*Dg+17.52
      else
      LimtB=3.805*(Dg-14.)**0.2+30.44
      endif
      If (LimtA.LE.LimtB.or.ki.lT.0) then
      Lamda=-0.0044*Ki**3+0.0661*Ki**2-0.352*Ki+1
      elseif (LimtA.GT.LimtB.and.ki.GT.2)then
      Lamda=0.0337*Ki**3-0.4687*Ki**2+1.916*Ki-1.644
C normalement ce cas n'existe pas
      else
      lamda=1
      endif
C le strickler est corrig� par Qci et Lamda
      FR1EQ=Qci*Lamda*21/DM**0.16666
C fin du if sur ki negatif
      endif
c*****************************************************************************
c optFPC=6: correspond � la formule de Recking et al. 2008
      elseIf(OptFPC.EQ.6)then
       D84=DM*SEGMA
C param�tre critique de Shield reformul� Recking et al. en fonction du pente
      IF(PEN(I).GT.0.001)THEN
      Tc=0.15*(PEN(I)**0.275)
      ELSE
      TC=0.02244
      ENDIF
C la pente de frottement calcule par Strickler precedent
       J=SENG2(IDS,I,RHMIN)
C la pente d'energie calculee par la difference de charge
c        J=SENG(I)
C param�tre de Shields
      T=(J*RHMIN)/(ROM1*D84)
      IF(RHMIN.GT.D84)THEN
      AlphaLR=4*((RHMIN/D84)**(-0.43))
      IF(ALPHALR.LT.1.)ALPHALR=1.
C si rhmin plus petit que d84
      ELSE
      ALPHALR=4.
      ENDIF
      if(T.LT.Tc)then
         AlphaBR=1.
      elseif (T.LT.2.5*Tc)then
C      AlphaBR=7*(RHMIN/D84)*(PEN(i)**0.85)
        AlphaBR=7*(RHMIN/D84)*(J**0.85)
        IF(ALPHABR.LT.1.)THEN
          ALPHABR=1.
        ELSEIF(ALPHABR.GT.2.6)THEN
          ALPHABR=2.6
        ENDIF
C si T superieur a 2.5 TC
      else
        AlphaBR=2.6
      endif
c      FR1EQ=SQRt(GRAV*RHMIN**(-0.3333))*(6.25+5.75*dLOG10(RHMIN/
c     &(AlphaLR*AlphaBR*D84)))

      Intermediaire=6.25+5.75*dLOG10(RHMIN/(AlphaLR*AlphaBR*D84))
          if(intermediaire.GT.EPS)then
           FR1EQ=max(1.D0,INTERMEDIAIRE*SQRT(GRAV*RHMIN**(-0.3333)))
          else
         fr1eq=1.
              endif
c optFPC=7: correspond � la formule de garde et raju 1966
      elseIf(OptFPC.EQ.7)then
        V=abs(DETVMI(IDS, VN(I),Y,IJ,I))
        FP=v/SQRT(grav*ROM1*DM)
        IF(FP.GT.0.33)THEN
          CGR=0.028
        ELSE
          CGR=0.098
        ENDIF
        FR1EQ=SQRT(GRAV/CGR)*DM**(-0.166667)
C fin du if sur OptFPC
      endif

      RETURN
      END
c-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION SENG(I)
C-----------------------------------------------------------------------
C     Calcul de pente de la ligne d'�nergie
C on appelle cette fonction que si Y est superieur a EPSY
C et si IDS est nul (centre maille)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,I2
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
      DOUBLE PRECISION TMAIL(LMAX),PEN(LMAX),CTDF(LMAX)
     :,chamont,charge
      DOUBLE PRECISION CHEZY,GRAV,EPS,EPSY,EPSM

      COMMON/VITTN/VN,YN1D,RHN
      COMMON/GEOMT/TMAIL,CTDF,PEN
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM

       IF(I.EQ.1)THEN
          I2=2
       ELSE
          I2=I
       ENDIF
       chamont=ctdf(i2-1)+YN1D(i2-1)+0.5*vn(i2-1)**2/grav
       charge=ctdf(i2)+YN1D(i2)+0.5*vn(i2)**2/grav
       seng=(chamont-charge)/(tmail(i2)-tmail(i2-1))
       IF(SENG.LT.0.0001)SENG=0.0001
       RETURN
       END

c-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION SENG2(IDS,I,RHMIN)
C-----------------------------------------------------------------------
C     Calcul de pente de la ligne d'�nergie
C on appelle cette fonction que si Y est superieur a EPSY
C et si IDS est nul (centre maille)
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,IDS
c      INTEGER LM,LL
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
c      DOUBLE PRECISION DETSN,DETPM,DETSMI,DETPMI,DETL
      DOUBLE PRECISION RHMIN
     :,FR1(LMAX)
c      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION V

C      COMMON/PHYS/LM,LL
c      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/FROTMT/FR1

c      if(IDS.EQ.0)then
       V=VN(I)
c       elseif(I.EQ.1)then
c       V=VN(I)
c       elseif(I.EQ.LL)then
c       V=VN(LM)
c       ELSE
c       V=(QN(I)+QN(I+1))/(2*S)
c       endif
       SENG2=(V**2)/((Fr1(i)**2)*RHMIN**1.33333)
       RETURN
       END

c-----------------------------------------------------------------------
      DOUBLE PRECISION FUNCTION FRDE(IDS,I,Y,S,IJ)
C-----------------------------------------------------------------------
C     Calcul du nombre de Froude du lit mineur
C  IDS vaut toujours 0 (centre de maille)
C Y est toujours superieur a epsy

         IMPLICIT NONE
      INTEGER LMAX
      PARAMETER(LMAX=3000)
      INTEGER I,IJ,IDS
C      INTEGER LM,LL
      DOUBLE PRECISION VN(LMAX),YN1D(LMAX),RHN(LMAX)
C      DOUBLE PRECISION DETSN
      DOUBLE PRECISION S,Y
C      DOUBLE PRECISION SN(LMAX),QN(LMAX)
      DOUBLE PRECISION DETLMI,DETVMI,V
      DOUBLE PRECISION GRAV,EPS,EPSY,EPSM,CHEZY

      EXTERNAL DETVMI,DETLMI

C      COMMON/PHYS/LM,LL
C      COMMON/MAILTN/SN,QN
      COMMON/VITTN/VN,YN1D,RHN
      COMMON/PARNUM/CHEZY,GRAV,EPS,EPSY,EPSM


c       IF(Y.GT.EPSY)THEN
c       IJ=LDETYJ(IDS,Y,I)
c       S=DETSN(IDS,Y,IJ)
c       if(IDS.EQ.0)then
       V=VN(I)
c       elseif(I.EQ.1)then
c       V=VN(I)
c       elseif(I.EQ.LL)then
c       V=VN(LM)
c       ELSE
c       V=0.5*(QN(I)+QN(I+1))/S
cC fin du if sur IDS
c       endif
       FRDE=DETVMI(IDS,V,Y,IJ,I)/SQRT(GRAV*S/DETLMI(IDS,Y,IJ,I))
C fin du if y=0
c       ELSE
c       FRDE=0.
c       ENDIF
       RETURN
       END
C-----------------------------------------------------------------------
       SUBROUTINE ORDRBIEF
C-----------------------------------------------------------------------

        integer num,i,j,k,J1,J2,IB,nbb,nbmax
              parameter(NBMAX=150)
              logical encore,encoret
      INTEGER NCONF,CONFLU(NBMAX,3),NCONF2,NIB(NBMAX)
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)

         COMMON/NBIEF/NBB
      COMMON/NCONFL/NCONF,NCONF2,CONFLU
         common/NIBIEF/nib
      COMMON/CONDLI/CONDAM,CONDAV,REGIME

         num=1
            encore=.true.
            encoret=.true.
            do ib=1,nbb
                 if(condam(ib).NE.4)then
                       nib(num)=ib
                    num=num+1
           endif
               enddo
         DO k=1,NBB
c on repasse jusqua avoir tous les biefs nbb fois au maximum
               IF(encoret)then
c on traite les defluences
                      do I=nconf2+1,NCOnF
                 do j=1,NUm-1
                   if(conflu(i,1).EQ.nib(J))THEN
                 do j1=1,NUm-1
                   if(conflu(i,2).EQ.nib(J1))THEN
                             encore=.false.
                      endif
c fin boucle sur J1
               enddo
                       if(encore)then
                            nib(num)=conflu(i,2)
                         num=num+1
                            nib(num)=conflu(i,3)
                         num=num+1
                       endif
                       encore=.true.
                      endif
c fin boucle sur J
                 enddo
c fin boucle sur I
               enddo
C on traite les confluences
            do I=1,NCOnF2
                 do j=1,NUm-1
                   if(conflu(i,2).EQ.nib(J))THEN
                 do j1=1,NUm-1
                   if(conflu(i,3).EQ.nib(J1))THEN
                 do j2=1,NUm-1
                   if(conflu(i,2).EQ.nib(J2))THEN
                             encore=.false.
                      endif
c fin boucle sur J2
                 enddo
                 if(encore)then
                            nib(num)=conflu(i,1)
                         num=num+1
                 endif
                       encore=.true.
                      endif
c fin boucle sur J1
                 enddo
                      endif
c fin boucle sur J
                 enddo
c fin boucle sur I
               enddo
            if(num.gt.nbb)encoret=.false.
               endif
c fin boucle sur K
               enddo
               return
               end

C-----------------------------------------------------------------------
      SUBROUTINE ECRVOLQOUV(TN,NFIC)
C-----------------------------------------------------------------------
C �crit le fichier VOLQOUV � TN
C -----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,LNCMAX,CSMAX,NBMAX,NFIC,nou1Dmax,noe1Dmax
      PARAMETER(LMAX=3000,LNCMAX=130000,CSMAX=10,NBMAX=150)
      DOUBLE PRECISION TN
      PARAMETER(nou1Dmax=50,noe1Dmax=10)
      INTEGER IOUV
      CHARACTER ETUDE*20
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax)
     :,NBOUV,NREFA(LMAX)
      DOUBLE PRECISION HOUV(LMAX,2),QOUV(LMAX,2)

      COMMON/NOMETU/ETUDE
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDNARF/NREFA
      COMMON/DDHQOUV/HOUV,QOUV


C-----------------------------------------------------------------------
C      OPEN(NFIC,FILE='volqouv.'//ETUDE,STATUS='UNKNOWN')
      DO IOUV=1,NBOUV
           IF(NREFA(IA1(iouv)).EQ.-1)THEN
        WRITE(NFIC,'(I4,1X,F12.3,1X,F12.3)')IOUV,TN,QOUV(IA1(IOUV),1)
              ENDIF
      ENDDO
         return
         end
C-----------------------------------------------------------------------
      SUBROUTINE ECROUV
C-----------------------------------------------------------------------
C ecrit un fichier ouvrag-r pour reprise des B
C-----------------------------------------------------------------------
      IMPLICIT NONE
      INTEGER LMAX,NOB1DMAX,NBHYPR,NBMAX
      PARAMETER(LMAX=3000,NOB1DMAX=10,NBHYPR=100,NBMAX=150)
      INTEGER LM(0:NBMAX),LL

      INTEGER nou1Dmax,noe1Dmax,M,NBB
      PARAMETER(nou1Dmax=50,noe1Dmax=10)
      INTEGER IOUV,IDON,I,K,IBOUV,NOB
      CHARACTER ETUDE*20,NOMFIC*40
      INTEGER IA1(nou1Dmax),IA2(nou1Dmax),NOUV(nou1Dmax)
     :,NBOUV,NREFA(LMAX)
     &  ,NBCOUP,NBCOU2(nou1Dmax,noe1Dmax),NBCOU1(nou1Dmax,noe1Dmax)
      DOUBLE PRECISION LONG(nou1Dmax,noe1Dmax),ZDEV(nou1Dmax,noe1Dmax)
     &  ,HAUT(nou1Dmax,noe1Dmax),COEF(nou1Dmax,noe1Dmax)
     &  ,ZOUV(nou1Dmax,noe1Dmax),COEFIN(nou1Dmax,noe1Dmax)
     &  ,ZFERM(nou1Dmax,noe1Dmax)
     &  ,QCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
     :,ZCOUP(nou1Dmax*noe1Dmax*noe1Dmax)
      CHARACTER*1 TYPOUV(nou1Dmax,noe1Dmax)
      INTEGER IT(NOB1DMAX),NT(NOB1DMAX),IOUB(nou1Dmax,noe1Dmax),NB1,NB2
      DOUBLE PRECISION KA(NOB1DMAX),DT2(NOB1DMAX)
      DOUBLE PRECISION ZC(NOB1DMAX),ZP(NOB1DMAX),ZB(NOB1DMAX)
     :,ZB0(NOB1DMAX),Z0(NOB1DMAX),ALP(NOB1DMAX),ALC(NOB1DMAX)
     :,RHO(NOB1DMAX),PHI(NOB1DMAX)
     :,DB0(NOB1DMAX),DB(NOB1DMAX),D50(NOB1DMAX)
      DOUBLE PRECISION ETA(NOB1DMAX),C1(NOB1DMAX),C2(NOB1DMAX)
     :,DBMAX(NOB1DMAX),TRUP(NOB1DMAX)
     :,TS,FDEB1,FDEB2,FDEB3
     :,TSOR(NBHYPR),DELMAI(NBHYPR),DELIMAI(NBHYPR)
     :,xtmail(lmax),xctdf(lmax),TR

      DOUBLE PRECISION TRECT(NOB1DMAX)
      LOGICAL ELAP(NOB1DMAX)

      COMMON/PHYS/LM,LL
      COMMON/NBIEF/NBB
      COMMON/NOMETU/ETUDE
      COMMON/DDIOUVRA/IA1,IA2,NOUV,NBOUV
      COMMON/DDCOUVRA/LONG,ZDEV,HAUT,COEF
      COMMON/DDTOUVRA/TYPOUV
      COMMON/DDNARF/NREFA
      COMMON/YOUVRA/COEFIN,ZOUV,ZFERM
      COMMON/DDNOUVRA/NBCOU1,NBCOU2
      COMMON/DDZOUVRA/QCOUP,ZCOUP
      COMMON/DDDIGUE/ZC,ZP,ALP,ALC,Z0,D50,RHO,PHI,DB0,ZB0
      COMMON/DDMAXBRE/DBMAX,TRECT
      COMMON/DDTINIB/TRUP
      COMMON/DDELAPPR/ELAP
      COMMON/DDIOBREC/IOUB,NOB
      COMMON/DDCONRUP/ETA,KA,C1,C2,DT2
      COMMON/DDNCONST/NT
      COMMON/DDBRECHE/ZB,DB,IT
      COMMON/SORCAL/TS,FDEB1,FDEB2,FDEB3,TSOR,DELMAI,DELIMAI
      COMMON/XGEOMT/XTMAIL,XCTDF

      IF(NOB.GT.0)THEN
         IBOUV=0
         IDON=9
      NOMFIC='ouvrag-r.'//ETUDE
      OPEN(IDON,FILE=NOMFIC,STATUS='UNKNOWN')
      WRITE(*,'(A,A)')'ecriture fichier ',NOMFIC
      DO IOUV=1,NBOUV
               nb1=0
                     nb2=0
               DO 3 M=1,NBB
                     IF(IA1(IOUV).GT.LM(M-1).AND.ia1(iouv).LT.LM(M))THEN
                            NB1=M
                     endif
                     IF(IA2(IOUV).GT.LM(M-1).AND.ia2(iouv).LT.LM(M))THEN
                            NB2=M
                     endif
 3    CONTINUE

      WRITE(IDON,'(F11.3,2I5)')XTmail(IA1(iouv)),NREFA(IA1(iouv)),NB1
            if(ia2(iouv).gt.0)then
           WRITE(IDON,'(F11.3,2I5)')XTmail(IA2(iouv)),NOUV(IOUV),NB2
           else
             WRITE(IDON,'(F11.3,2I5)')       999999.999,NOUV(IOUV),0
            endif
        DO I=1,NOUV(IOUV)
          IF(TYPOUV(iouv,I).EQ.'Y'.OR.TYPOUV(iouv,I).EQ.'y')THEN
            WRITE(IDON,'(A1,9X,4F10.3)')TYPOUV(iouv,I),LONG(iouv,I)
     :,ZDEV(iouv,I),HAUT(iouv,I)+Zdev(iouv,I),COEF(iouv,I)
            WRITE(IDON,*)COEFIN(IOUV,I),ZOUV(IOUV,I),ZFERM(IOUV,I)
          ELSEIF(TYPOUV(iouv,I).EQ.'D'.OR.TYPOUV(iouv,I).EQ.'d')THEN
            WRITE(IDON,'(A1,9X,4F10.3)')TYPOUV(iouv,I),LONG(iouv,I)
     :,ZDEV(iouv,I),HAUT(iouv,I)+Zdev(iouv,I),COEF(iouv,I)
C O correspond au meme ouvrage que D mais avec une longueur et en circulaire
          ELSEIF(TYPOUV(iouv,I).EQ.'O'.OR.TYPOUV(iouv,I).EQ.'o')THEN
            WRITE(IDON,'(A1,9X,4F10.3)')TYPOUV(iouv,I),LONG(iouv,I)
     :,ZDEV(iouv,I),HAUT(iouv,I),COEF(iouv,I)
          ELSEIF(TYPOUV(iouv,I).EQ.'Z'.OR.TYPOUV(iouv,I).EQ.'z')THEN
            WRITE(IDON,'(A1,I9,4F10.3)')TYPOUV(iouv,I)
     :,NBCOU2(IOUV,I)-NBCOU1(IOUV,I)+1,0.,0.,0.,0.
            DO K=1,NBCOU1(IOUV,I),NBCOU2(IOUV,I)
              WRITE(IDON,'(2F10.3)')ZCOUP(K),QCOUP(K)
            ENDDO
          ELSEIF(TYPOUV(iouv,I).EQ.'Q'.OR.TYPOUV(iouv,I).EQ.'q')THEN
            WRITE(IDON,'(A1,I9,4F10.3)')TYPOUV(iouv,I)
     :,NBCOU2(IOUV,I)-NBCOU1(IOUV,I)+1,0.,0.,0.,0.
            DO K=1,NBCOU1(IOUV,I),NBCOU2(IOUV,I)
              WRITE(IDON,'(2F10.3)')ZCOUP(K),QCOUP(K)
            ENDDO
          ELSEIF(TYPOUV(iouv,I).EQ.'B'.OR.TYPOUV(iouv,I).EQ.'b')THEN
                       ibouv=ibouv+1
                    IF(elap(ibouv))then
                              nbcoup=1
                       else
                              nbcoup=0
                       endif
                       IF(TRup(ibouv).gt.ts)then
                              tr=trup(ibouv)
                       else
                              tr=ts
                       endif
            WRITE(IDON,'(A1,I1,F8.0,4F10.3)')
     :TYPOUV(iouv,I),nbcoup,TR
     :,zc(ibouv),Zp(ibouv),alc(ibouv),alp(ibouv)
            WRITE(IDON,'(4F10.2)')D50(IBOUV)*1000.,KA(IBOUV)
     : ,RHO(IBOUV),PHI(IBOUV)
            WRITE(IDON,'(3F10.2)')ZB(IBOUV),DB(IBOUV)*1000.,DBMAX(IBOUV)
            WRITE(IDON,'(2F6.2,I2,I4)') DT2(IBOUV)
     :,ETA(IBOUV),IT(IBOUV),NT(IBOUV)
C fin du if sur le type d'ouvrage
          ENDIF
C fin du if sur ouvrage elementaire
         ENDDO
C fin du if sur ouvrage
         ENDDO
        CLOSE(IDON)
C fin du if sur NOB
        endif
        RETURN
        END
cccccccccccccccccccccccccccccccccccccccccccccccccc
        subroutine verifconfluence
cccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE

      INTEGER NBMAX
      PARAMETER(NBMAX=150)
C Donn�es de base
C----------------
      CHARACTER ETUDEDD*20
      INTEGER CONDAM(NBMAX),CONDAV(NBMAX),REGIME(NBMAX)
      INTEGER NCONF,CONFLU(NBMAX,3),NCONF2,INDAMONT,IB,ICONF,NBB
          LOGICAL VERIF

C Variables contenues dans 'donnee.etude'
C----------------------------------------
      COMMON/NOMETU/ETUDEDD
      COMMON/CONDLI/CONDAM,CONDAV,REGIME


C 9)VARIABLES NBIEFS
C----------------------------------
      COMMON/NBIEF/NBB
      COMMON/NCONFL/NCONF,NCONF2,CONFLU

          VERIF=.TRUE.
          do ib=1,nbb
            if(condam(ib).eq.4)then
                  indamont=0
C confluence si limite amont du bief aval
              do Iconf=1,nconf2
                    if(conflu(iconf,1).EQ.ib)then
                                indamont=indamont+1
                        endif
                  enddo
C defluence si limite amont d'un des biefs aval
          do Iconf=nconf2+1,nconf
                    if(conflu(iconf,2).EQ.ib)then
                                indamont=indamont+1
                        endif
                    if(conflu(iconf,3).EQ.ib)then
                                indamont=indamont+1
                        endif
                  enddo
C indicateur indamont doit �tre �gal � 1 sinon probleme
           if(indamont.eq.0)then
                      write(*,*)'le bief ',IB,' bief aval confluence',
     :' ou defluence non declare dans confl.',ETUDEDD
              VERIF=.FALSE.
                        elseif(indamont.gt.1)then
                      write(*,*)'le bief ',IB,' bief aval confluence',
     :' ou defluence declare plusieurs fois dans confl.',ETUDEDD
              VERIF=.FALSE.
            endif
C fin du if sur condam(ib)=4
          endif
C on verifie maintenant les conditions aval internes
            if(condav(ib).eq.4)then
                  indamont=0
C confluence si limite aval d'un des biefs amont
              do Iconf=1,nconf2
                    if(conflu(iconf,2).EQ.ib)then
                                indamont=indamont+1
                        endif
                    if(conflu(iconf,3).EQ.ib)then
                                indamont=indamont+1
                        endif
                  enddo
C defluence si limite aval du bief amont
          do Iconf=nconf2+1,nconf
                    if(conflu(iconf,1).EQ.ib)then
                                indamont=indamont+1
                        endif
                  enddo
C indicateur indamont doit �tre �gal � 1 sinon probleme
           if(indamont.eq.0)then
                      write(*,*)'le bief ',IB,' bief amont confluence',
     :' ou defluence non declare dans confl.',ETUDEDD
              VERIF=.FALSE.
                   elseif(indamont.gt.1)then
                      write(*,*)'le bief ',IB,' bief amont confluence',
     :' ou defluence declare plusieurs fois dans confl.',ETUDEDD
              VERIF=.FALSE.
            endif
C fin du if sur condam(ib)=4onfl
          endif
c fin boucle sur IB
         enddo
                 if(.not.verif)then
                         stop
                 endif
                 return
                 end


