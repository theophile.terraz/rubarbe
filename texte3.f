CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C	PROGRAMME D'ECRITURE DES RESULTATS DE RUBAR3.FOR          C 
C                                                                 C
C	LECTURE DES FICHIERS TRAJEC.ETUDE                         C
C			     ENVLOP.ETUDE                         C 
C			     MAIL.ETUDE                           C
C                                                                 C
C       LE 8/7/97               Andre PAQUIER                     C
C lecture geometrie par geomac-i le 15 octobre 2007 corrige le 22/10
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

        INTEGER I,J,LM,LMAX,K,I1,I2,NC,IDPARS,J0,J1,J2

        PARAMETER (LMAX=20000)

        INTEGER ITF1(0:LMAX),ITF10(0:LMAX),ITF2(0:LMAX),
     +           ITF20(0:LMAX),IFDEB,JFDEB

        REAL X1,X2,FDEB1,FDEB2,FDEB3,POUB3,POUB4,POUB5
        REAL TF1(0:LMAX),TF2(0:LMAX),TFDEB1(0:LMAX),
     +        TFDEB2(0:LMAX),TFDEB3(0:LMAX),
     +        TY(0:LMAX),TV(0:LMAX),TQ(0:LMAX),
     +        VM(0:LMAX),QM(0:LMAX),YM(0:LMAX),ZM(0:LMAX),
     +        YM2(0:LMAX),TRAN,PK(0:LMAX),
     +        CTDF(0:LMAX),TMAIL(0:LMAX),DIST(0:LMAX),
     +        YM0(0:LMAX),ZM0(0:LMAX),XTMAIL(0:LMAX),CTDF2(0:LMAX)
     +        ,VM0(0:LMAX),POUB1,POUB2

        CHARACTER*6  ETUDE*20,NOMFIC*38,TIT1,TIT2,TIT3,TIT*1,XCCOU*1
     :,LIGNE*630
        EXTERNAL TRAN

        COMMON/GEO/DIST,TMAIL,K,LM

        WRITE(*,*)'RENTREZ LE NOM DE L''ETUDE'
        READ(*,'(A)')ETUDE

C    LM = NOMBRE DE MAILLES
C Ouverture du fichier de g�om�trie abscisse-cote aux intermailles
C geomac-i 
        NOMFIC='geomac-i.'//ETUDE
        OPEN(24,FILE=NOMFIC,STATUS='OLD',FORM='FORMATTED',ERR=98)
        GO TO 980
 98     WRITE(*,*)'FICHIER geomac-i INEXISTANT'        
 980    READ(24,*) LM
         LM=LM+1
      DO I=1,LM-1
        READ(24,*) II,XTMAIL(I),NC
        CTDF2(I)=9999.99
        DO J=1,NC
C Lecture des couples abscisse-cote
          READ(24,'(A630)') LIGNE
          READ(LIGNE,9994) XCCOU,POUB1,POUB2
          IF(POUB2.LT.CTDF2(I))THEN
            CTDF2(I)=POUB2
          ENDIF
        ENDDO 
      ENDDO
      CLOSE(24)

 9994 FORMAT(A1,1X,F11.5,F13.5)

       CTDF(1)=CTDF2(1)
       TMAIL(1)=XTMAIL(1)
       CTDF(LM)=CTDF2(LM-1)
       TMAIL(LM)=XTMAIL(LM-1)
       DO 2123 I=2,LM-1
          CTDF(I)=0.5*(CTDF2(I-1)+CTDF2(I))
          TMAIL(I)=0.5*(XTMAIL(I-1)+XTMAIL(I))
 2123  CONTINUE


        NOMFIC='envlop.'//ETUDE
        OPEN(10,FILE=NOMFIC,STATUS='OLD')
        READ(10,'(A)')TIT
        DO 3 I=1,LM
          READ(10,*) J,TY(I),YM0(I),TV(I),VM0(I),TQ(I),QM(I)
          ZM0(I)=YM0(I)+CTDF(I)
3       CONTINUE
        CLOSE(10)

        NOMFIC='trajec.'//ETUDE
        OPEN(2,FILE=NOMFIC,STATUS='OLD')
        READ(2,100)TIT1,FDEB1,TIT2,FDEB2,TIT3,FDEB3
100     FORMAT(3(A7,F13.7,5X))
        READ(2,'(A)')TIT
        DO 44 I=1,LM
      	READ(2,*)J,TFDEB1(I),TFDEB2(I),TFDEB3(I)
44      CONTINUE
        CLOSE(2)

        WRITE(*,*)'RENTREZ L''ABSCISSE AMONT POUR RESULTATS'
        READ(*,*)X1
        WRITE(*,*)'RENTREZ L''ABSCISSE AVAL POUR RESULTATS'
        READ(*,*)X2
        K=INT((X2-X1)/500.)
        DO 1 J=0,K
          PK(J)=FLOAT(J)*.5
          DIST(J)=FLOAT(J)*500.+X1
1       CONTINUE

        WRITE(*,*)'LES FRONTS DE DEBIT DISPONIBLES SONT'
        WRITE(*,*)'FDEB1=',FDEB1,'M3/S'
        WRITE(*,*)'FDEB2=',FDEB2,'M3/S'
        WRITE(*,*)'FDEB3=',FDEB3,'M3/S'
        WRITE(*,*)'LE DEBIT MAXIMAL'
        WRITE(*,*)'CHOISISSEZ 2 DEBITS PARMI 1,2,3 OU 4'
        READ (*,*)I1,I2

        IF(I1.EQ.1)THEN
            DO 5001 J=0,K
            TF1(J)=TRAN(TFDEB1,J)
5001        CONTINUE
            IFDEB=NINT(FDEB1)
        ELSE IF(I1.EQ.2)THEN
            DO 5002 J=0,K
            TF1(J)=TRAN(TFDEB2,J)
5002        CONTINUE
            IFDEB=NINT(FDEB2)
        ELSE IF(I1.EQ.3)THEN
            DO 5003 J=0,K
            TF1(J)=TRAN(TFDEB3,J)
5003        CONTINUE
            IFDEB=NINT(FDEB3)
        ELSE
            DO 5004 J=0,K
            TF1(J)=TRAN(TQ,J)
5004        CONTINUE
        ENDIF
          DO 11 J=0,K
            ITF1(J)=INT(TF1(J)/3600.)
            ITF10(J)=INT(TF1(J)/60.)-ITF1(J)*60
11        CONTINUE

        IF(I2.EQ.1)THEN
            DO 5005 J=0,K
            TF2(J)=TRAN(TFDEB1,J)
5005        CONTINUE
            JFDEB=NINT(FDEB1)
        ELSE IF(I2.EQ.2)THEN
            DO 5006 J=0,K
            TF2(J)=TRAN(TFDEB2,J)
5006        CONTINUE
            JFDEB=NINT(FDEB2)
        ELSE IF(I2.EQ.3)THEN
            DO 5007 J=0,K
            TF2(J)=TRAN(TFDEB3,J)
5007        CONTINUE
            JFDEB=NINT(FDEB3)
        ELSE
            DO 5008 J=0,K
            TF2(J)=TRAN(TQ,J)
5008        CONTINUE
        ENDIF
          DO 12 J=0,K
            ITF2(J)=INT(TF2(J)/3600.)
            ITF20(J)=INT(TF2(J)/60.)-ITF2(J)*60
12        CONTINUE

        DO 5009 J=0,K
            ZM(J)=TRAN(ZM0,J)
5009    CONTINUE
        DO 5010 J=0,K
            YM(J)=TRAN(YM0,J)
5010    CONTINUE
        DO 5011 J=0,K
            VM(J)=TRAN(VM0,J)
5011    CONTINUE
        DO 13 J=0,K
c        IF(YM(J).GT.2.)THEN
c         ZM(J)=FLOAT(NINT(ZM(J)))
c         YM(J)=FLOAT(NINT(YM(J)))
c        ELSE
         ZM(J)=FLOAT(NINT(10.*ZM(J)))/10.
         YM(J)=FLOAT(NINT(10.*YM(J)))/10.
c        ENDIF
13      CONTINUE
C------ECRITURE DES VALEURS CALCULEES
        NOMFIC='lst.'//ETUDE
        OPEN(95,FILE=NOMFIC,STATUS='UNKNOWN')
        J1=26
        J0=0
        WRITE(95,'(A)')' '
112     WRITE(95,'(63X,A8,I2)')'TABLEAU ',J0/27+1
        WRITE(95,101)'VALEURS  CALCULEES'
101     FORMAT(20X,A30)
        WRITE(95,103)          'PK',          'TEMPS',         
     +     'TEMPS',          'COTE'          ,'TIRANT','VITESSE'          
103     FORMAT(6X,5X,A2,6X,4X,A5,4X,4X,A5,4X,4X,A4,4X
     +         ,2X,A6,3X,A7)
        WRITE(95,104)          '(DISTANCE',          'D''ARRIVEE',
     +           'D''ARRIVEE',
     +                  'MAXIMALE',          'D''EAU' ,'MAXIMALE'         
104     FORMAT(6X,2X,A9,2X,2X,A9,2X,2X,A9,2X,2X,A8,2X
     +         ,2X,A5,4X,A8)
        WRITE(95,105)          'DEPUIS LE',          'DU DEBIT'
     +          ,'DU DEBIT',          'ATTEINTE'          ,
     +'MAXIMAL','ATTEINTE'          
105     FORMAT(6X,2X,A9,2X,3X,A8,2X,3X,A8,2X,2X,A8,2X
     +         ,2X,A7,2X,A8)
       IF(I1.EQ.4)THEN
        WRITE(95,114)          'BARRAGE)',          'MAXIMAL',         
     + JFDEB,'M3/S',          'Z',          'Y','V'          
114     FORMAT(6X,3X,A8,3X,4X,A7,3X,I5,A4,3X,5X,A1,
     +         6X,6X,A1,10X,A1)
       ELSE IF(I2.EQ.4)THEN
        WRITE(95,115)          'BARRAGE)',          IFDEB,'M3/S',     
     +      'MAXIMAL',          'Z',          'Y' ,'V'         
115     FORMAT(6X,3X,A8,2X,I5,A4,3X,3X,A7,3X,6X,A1,
     +         6X,5X,A1,8X,A1)
       ELSE
        WRITE(95,106)          'BARRAGE)',          IFDEB,'M3/S',    
     +      JFDEB,'M3/S',          'Z',          'Y','V'          
106     FORMAT(6X,3X,A8,2X,I5,A4,3X,I5,A4,3X,6X,A1,
     +         6X,5X,A1,8X,A1)
       ENDIF
        WRITE(95,107)          'en km'          ,'en h et mn',         
     + 'en h et mn',          'en m NGF',          'en m' ,'en m/s'         
107     FORMAT(6X,4X,A5,4X,1X,A10,1X,2X,A10,1X,3X,A8,2X,
     +         4X,A4,4X,A6)
       IF (J1.GT.K)J1=K
       DO 111 I=J0,J1
c       IF(YM(I).GT.2.)THEN
c        WRITE(95,109)          PK(I),          ITF1(I),'h',ITF10(I),'mn'
c     +,          ITF2(I),'h',ITF20(I),'mn',          ZM(I),          
c     +YM(I),VM(I)          
109     FORMAT(6X,F9.1,4X,2(I3,1X,A1,I3,1X,A2,2X),
     +        F8.0,4X,F8.0,1X,F8.1)
c       ELSE
        WRITE(95,110)          PK(I),          ITF1(I),'h',ITF10(I),'mn'
     +,          ITF2(I),'h',ITF20(I),'mn',          ZM(I),          
     +YM(I),VM(I)          
110     FORMAT(6X,F9.1,4X,2(I3,1X,A1,I3,1X,A2,2X),
     +        F9.1,3X,F9.1,F8.1)
c       ENDIF
111    CONTINUE
       J1=J1+27
       J0=J1-26
       IF(J0.GT.K)GO TO 113
       GO TO 112
113    J2=J0/27
C------CALCUL DES VALEURS CTPB
        DO 14 J=0,K
          TF1(J)=TF1(J)*.87
          ITF1(J)=INT(TF1(J)/3600.)
          ITF10(J)=INT(TF1(J)/60.)-ITF1(J)*60
          TF2(J)=TF2(J)*.87
          ITF2(J)=INT(TF2(J)/3600.)
          ITF20(J)=INT(TF2(J)/60.)-ITF2(J)*60
          YM2(J)=YM(J)*1.15
c          IF(YM(J).GT.2.)THEN
c            YM2(J)=FLOAT(NINT(YM2(J)))
c          ELSE
            YM2(J)=FLOAT(NINT(10.*YM2(J)))/10.
c          ENDIF
          IF(YM2(J)-YM(J).LT.1.)THEN
            YM2(J)=YM(J)+1.
          ENDIF
          ZM(J)=ZM(J)-YM(J)+YM2(J)
14     CONTINUE
C------ECRITURE DES VALEURS CTPB
        J1=26
        J0=0
1112    WRITE(95,'(63X,A8,I2)')'TABLEAU ',J0/27+2+J2
        WRITE(95,101)'VALEURS  RECOMMANDEES'
        WRITE(95,103)          'PK',          'TEMPS',         
     +     'TEMPS',          'COTE',          'TIRANT' ,'VITESSE'         
        WRITE(95,104)          '(DISTANCE',          'D''ARRIVEE'
     +,          'D''ARRIVEE',
     +                  'MAXIMALE',          'D''EAU' ,'MAXIMALE'         
        WRITE(95,105)          'DEPUIS LE',          'DU DEBIT'
     +,          'DU DEBIT',          'ATTEINTE',          
     +'MAXIMAL' ,'ATTEINTE'         
       IF(I1.EQ.4)THEN
        WRITE(95,114)          'BARRAGE)',          'MAXIMAL',         
     + JFDEB,'M3/S',          'Z',          'Y' ,'V'         
       ELSE IF(I2.EQ.4)THEN
        WRITE(95,115)          'BARRAGE)',          IFDEB,'M3/S',    
     +      'MAXIMAL',          'Z',          'Y','V'          
       ELSE
        WRITE(95,106)          'BARRAGE)',          IFDEB,'M3/S'     
     +     ,JFDEB,'M3/S',          'Z',          'Y' ,'V'         
       ENDIF
        WRITE(95,107)          'en km',          'en h et mn',         
     + 'en h et mn',          'en m NGF',          'en m' ,'en m/s'         
       IF (J1.GT.K)J1=K
       DO 1111 I=J0,J1
c       IF(YM(I).GT.2.)THEN
c        WRITE(95,109)          PK(I),          ITF1(I),'h',ITF10(I),'mn'
c     +,          ITF2(I),'h',ITF20(I),'mn',          ZM(I),          
c     +YM2(I),VM(I)          
c       ELSE
        WRITE(95,110)          PK(I),          ITF1(I),'h',ITF10(I),'mn'
     +          ,ITF2(I),'h',ITF20(I),'mn'          ,ZM(I)          ,
     +YM2(I),VM(I)          
c       ENDIF
1111   CONTINUE
       J1=J1+27
       J0=J1-26
       IF(J0.GT.K)GO TO 1113
       GO TO 1112
1113   CLOSE(95)
C.....ADDITION A TEXTE30=ECRITURE DES RESULTATS A CHAQUE PAS
        NOMFIC='lis.'//ETUDE
        OPEN(96,FILE=NOMFIC,STATUS='UNKNOWN')
        WRITE(96,*) ' '
        WRITE(96,*) ' '
        WRITE(96,'(14X,A49)')
     + 'ABSCISSE    COTE     HAUTEUR    VITESSE    DEBIT'
        WRITE(96,'(14X,A50)')
     + '          MAXIMALE   MAXIMALE   MAXIMALE   MAXIMAL'
        WRITE(96,*)' '
        J0=1
        J1=-7
        N=-1
        J1=MIN(LM,J1+61)
        DO 1250 I=1,LM
C        DO 1250 I=J0,J1
        WRITE(96,250)TMAIL(I),ZM0(I),YM0(I),VM0(I),QM(I)
1250    CONTINUE
C       WRITE(96,120)
        IF(LM.GT.J1)THEN
          N=N+1
          J0=55+61*N
        ENDIF
250     FORMAT(13X,F8.0,2X,F8.2,3X,F8.3,3X,F8.3,3X,F8.1)
        CLOSE(96)
       STOP
       END

C----------------------------------------------------------
       REAL FUNCTION TRAN(X,J)
       INTEGER LMAX,LM,K,I,J

       PARAMETER(LMAX=20000)

       REAL X(0:LMAX)
       REAL DIST(0:LMAX),TMAIL(0:LMAX)
 
       COMMON/GEO/DIST,TMAIL,K,LM

       DO 2 I=1,LM
         IF (DIST(J).LT.TMAIL(I))THEN
           TRAN=X(I-1)+((DIST(J)-TMAIL(I-1))
     +        /(TMAIL(I)-TMAIL(I-1)))*(X(I)-X(I-1))
           GO TO 1
         ENDIF
2      CONTINUE
       TRAN=X(LM)
1      RETURN 
       END
